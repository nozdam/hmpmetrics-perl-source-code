#!/usr/bin/perl


#----------------------------------------------------------------------------
#  program: Registration.pl
#  author: Jayanth Raj
#----------------------------------------------------------------------------

use strict;
use CGI;
use DBI;

# use Session;

use CGI::Session;
do "common_header.pl";
print "Content-Type: text/html\n\n";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $role = $session->param("role");

my $msg = $cgi->param("msg");

if(!$userID)
{
	&sessionExpire;
}


my $dbh;
my ($sql, $sth, $key);

# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my $defaultYear= $ini_value{'DEFAULT_YEAR'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

$sql = "select * from userdetails where UsersID = '$userID'";
$sth = $dbh->prepare($sql);
$sth->execute();

my $First_Name = "";
my $Middle_Name = "";
my $Last_Name = "";
my $emailID = "";
my $Address1 = "";
my $Address2 = "";
my $Address3 = "";
my $city = "";
my $State = "";
my $zip = "";
my $Title = "";
my $Suffix_Title = "";
my $Phone1 = "";
my $Phone2 = "";
my $CreatedDate = "";
my $ModifiedDate = "";
my $company_name = "";

while (my $row1 = $sth->fetchrow_hashref) {
	$First_Name = $$row1{First_Name};
	$Middle_Name = $$row1{Middle_Name};
	$Last_Name = $$row1{Last_Name};
	$emailID = $$row1{emailID};
	$Address1 = $$row1{Address1};
	$Address2 = $$row1{Address2};
	$Address3 = $$row1{Address3};
	$city = $$row1{city};
	$State = $$row1{State};
	$zip = $$row1{zip};
	$Title = $$row1{Title};
	$Suffix_Title = $$row1{Suffix_Title};
	$Phone1 = $$row1{Phone1};
	$Phone2 = $$row1{Phone2};
	$CreatedDate = $$row1{CreatedDate};
	$ModifiedDate = $$row1{ModifiedDate};
	$company_name = $$row1{company_name};
}	

my %statesDetails = &getStatesDetails();
my %passwordSecurityQry = &getPasswordSecurityQry();




print qq{
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<title>PROJECT MILO Edit Profile...</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="/css/register/main.css" rel="stylesheet" type="text/css">
<link href="/css/register/qm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/JS/common_header/Profile.js"></script> 
</head>
<body>
<center><br>
    };
	&innerHeader();
	print qq{
    <!--content start-->    
    
        <table cellpadding="0" cellspacing="0" width="885px">      
        
            <tr>
                <td valign="top">                	
                    </td>
            <td>	
		<table class="ColrBk02" width="100%" border="0" cellpadding="2" cellspacing="0">
		  <tbody><tr>
			<td><form action="/cgi-bin/ProfileActions.pl" method="post" name="frmProfile" id="frmProfile" onsubmit="return funcValidateForm();">
				<input type="hidden" name="Facility_Other" value="$company_name">
				<table width="100%"  bgcolor="#ffffff" border="0" cellpadding="4" cellspacing="0">
				  <tbody>
				  <tr>
					<td class="ColrBk03" colspan="3"><b><font color="#0066cc">&nbsp;<strong>Update your details</strong>...</font></b></td><td class="ColrBk03" ><font color="#006666"><b>$msg</b></font></td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> First Name</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><select name="txtNameTitle" class="TextBox01" id="txtNameTitle">
						<option value="n">Select Title...</option>
					};
						if($Title eq 'Mr.'){
						  print "<option value=\"Mr.\" selected>Mr.</option>\n";
						}else{
						  print "<option value=\"Mr.\">Mr.</option>\n";
						}
						if($Title eq 'Mrs.'){
						  print "<option value=\"Mrs.\" selected>Mrs.</option>\n";
						}
						else{
						  print"<option value=\"Mrs.\">Mrs.</option>\n";
						}
						if($Title eq 'Ms.'){
						  print "<option value=\"Ms.\" selected>Ms.</option>\n";
						}else{
						  print "<option value=\"Ms.\">Ms.</option>\n";
						}
						if($Title eq 'Dr.'){
						  #print "<option value="\Dr.\" selected>Dr.</option>\n";
						}else{
						  #print "<option value="\Dr.\" selected>Dr.</option>\n";
						}
						if($Title eq 'Prof.'){
						  print "<option value=\"Prof.\" selected>Prof.</option>\n";
						}else{
						  print "<option value=\"Prof.\">Prof.</option>\n";
						}
						if($Title eq 'Rev.'){
						  #print "<option value="\Rev.\" selected>Rev.</option>\n";
						}else{
						  print "<option value=\"Rev.\">Rev.</option>\n";
						}
						if($Title eq ''){
						  print "<option value=\"Other\" selected>Other</option>\n";
						}else{
						  print "<option value=\"Other\">Other</option>\n";
						} 
						
					print qq{
						
						</select> 
						<input name="txtFirstName" class="TextBox01" id="txtFirstName" value="$First_Name" size="30" maxlength="100" type="text">
					  <font color="#ff0000">*</font>
					  <select name="txtNameSuffix" class="TextBox01" id="txtNameSuffix">					  
						<option value="n" >Select Suffix...</option>
					};
						if($Suffix_Title eq 'CEO'){
							print "<option value=\"CEO\" selected>CEO</option>\n";
						}else{
							print "<option value=\"CEO\">CEO</option>\n";
						}
						if($Suffix_Title eq 'Director'){
							print "<option value=\"Director\" selected>Director</option>\n";
						}else{
							print "<option value=\"Director\" >Director</option>\n";
						}
					print qq{				
					  </select>					  
					  </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Middle Name</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtMiddleName" class="TextBox01" id="txtMiddleName" value="$Middle_Name" size="30" maxlength="100" type="text"> <font color="#ff0000"></font> </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Last Name</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtLastName" class="TextBox01" id="txtLastName" value="$Last_Name" size="30" maxlength="100" type="text"> <font color="#ff0000">*</font> </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Company Name </b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td>
						};						
						
						print qq{						
					  		 <input name="txtCompany" class="TextBox01" id="txtCompany" size="30" maxlength="100" type="text" value="$company_name">		  
					  </td>
				  </tr>
				  
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Address</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtAddress1" class="TextBox01" id="txtAddress1" value="$Address1" size="60" maxlength="100" type="text"> <font color="#ff0000"></font> </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtAddress2" class="TextBox01" id="txtAddress2" value="$Address2" size="60" maxlength="100" type="text"> <font color="#ff0000"></font> </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtAddress3" class="TextBox01" id="txtAddress3" value="$Address3" size="60" maxlength="100" type="text"> <font color="#ff0000"></font> </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> City</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtCity" class="TextBox01" id="txtCity" size="30" maxlength="100" type="text" value="$city"> <font color="#ff0000"></font> </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> State</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><select name="txtState" class="TextBox01" id="txtState">
						<option value="">--Select--</option>
						};	               	
						foreach $key(sort {$statesDetails{$a} cmp $statesDetails{$b} } keys %statesDetails){            	
						           if($State eq $key){ 
							    print "<OPTION VALUE=\"$key\" selected>$statesDetails{$key}</OPTION> \n";            	
							   }else{
							    print "<OPTION VALUE=\"$key\">$statesDetails{$key}</OPTION> \n";	
							   } 
							   
						}
						if($State eq 'Other'){
						   	print "<OPTION VALUE=\"Other\" selected>Other</OPTION>\n";	
						}else{
						   	print "<OPTION VALUE=\"Other\">Other</OPTION>\n";	
						}
          					print qq{
						
					  </select> <font color="#ff0000"></font>					  
					  </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Zip Code</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtZip" class="TextBox01" id="txtZip" value="$zip" size="15" maxlength="10" type="text"> <font color="#ff0000"></font> </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Telephone1</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtTelephone1" class="TextBox01" id="txtTelephone1" value="$Phone1" size="15" maxlength="10" type="text"> <font color="#ff0000"> (Land Phone, Mobile, Office)</font></td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif"  width="4" height="7"> Telephone2</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtTelephone2" class="TextBox01" id="txtTelephone2" size="15" maxlength="10" type="text" value="$Phone2"> <font color="#ff0000"> (Land Phone, Mobile, Office)</font></td>
				  </tr>
				  <!-- <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Email ID</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtEmail" class="TextBox01" id="txtEmail" value="$emailID" size="50" maxlength="100" type="text"> <font color="#ff0000"></font></td>
				  </tr> -->
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;Last updated on : $ModifiedDate</td>
				  </tr>
				  <tr>
					<td colspan="4"><table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tbody>
						<tr>
						  <td width="25"><img src="/images/register/pix_trans.gif" width="120" height="5"></td>
						  <td width="25"></td>
						  <td></td>
						</tr>
					  </tbody></table></td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td><input type="hidden" name="saction" value="Profile"><input name="cmdNext" class="TextBox01" id="cmdNext" value="Update" type="submit"> <input name="cmdReset" class="TextBox01" id="cmdReset" onclick="window.location='/cgi-bin/index.pl'" value="Cancel" type="reset"></td>
				  </tr>
				</tbody></table>
			  </form></td>
		  </tr>
		</tbody></table>
		</td>
	</tr>
  </tbody></table>
  </td>
              <td valign="top">
                  </td>
          </tr>
      </table>
    <!--content end-->
    </center>
   </body> 
  </html>
};

#--------------------------------------------------------------------------------------
sub sessionExpire
{

print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};
&headerScript();
print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}


#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#--------------------------------------------------------------------------------
# - Get details from States table. 

sub getStatesDetails
{
	my ($qry, $stmt, $record);
	
	$qry = "select * from states order by state_name";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();   	
   	
   	my %state;   	
   	
	while ($record = $stmt->fetchrow_hashref) {	
		$state{$$record{state}} = $$record{state_name};		
  	}  	
  	return %state;
}

#--------------------------------------------------------------------------------
# - Get details from password_security_qry table. 

sub getPasswordSecurityQry
{
	my ($qry, $stmt, $record);
	
	$qry = "select * from password_security_qry ";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();   	
   	
   	my %state;   	
   	
	while ($record = $stmt->fetchrow_hashref) {	
		$state{$$record{Qry_ID}} = $$record{Query};		
  	}  	
  	return %state;
}

#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach my $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}

#------------------------------------------------------------------------------
