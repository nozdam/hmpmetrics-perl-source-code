#!/usr/bin/perl

use CGI;
use DBI;

use CGI::Session;
use File::Basename;
use CGI::Carp qw ( fatalsToBrowser ); 
do "audit.pl";
my $cgi = new CGI;

my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $facility = $cgi->param("facility");
if(!$userID)
{
	&sessionExpire;
}
my $peerId = "";
my $str = "";
my $errStr = '';
my $saction = $cgi->param('saction'); 
my $msg = $saction;
my $mainId = "";
# Database 

my $dbh;
my ($sql2, $sth2);
my ($sql1, $sth1);
my ($sql, $sth);
my ($sql_r , $sth3,$sql_u,$sth4,$sql_f,$facility_id,$master_table); 
my ($row,@row,@imp_prov,@fac_name,@fac_num,$peerGrpMainId,$key,$s);
# Get Database connection
my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};
my $upload_dir = $ini_value{'PEER_IMPORT_FILE_PATH'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error('ERROR connecting to database', $DBI::errstr);
   exit(1);
}
$sql_f = "select facility_id,master_table from tbl_facility_type where facility_name = '$facility'";
$sth = $dbh->prepare($sql_f);
$sth->execute();
while ($row = $sth->fetchrow_hashref) {
	$facility_id = $$row{facility_id};
	$master_table= $$row{master_table};
}
if($saction eq 'Check' || $saction eq 'CheckSave' || $saction eq 'CheckPeer' || $saction eq 'CheckPeerSave' || $saction eq 'CheckPeerUpload' ){
	my $peerGrpMainName = $cgi->param('peername');
	if($saction eq 'CheckPeer' || $saction eq 'CheckPeerSave' || $saction eq 'CheckPeerUpload'){

		$peerId = $cgi->param('mainID');
	}	
	
	# Check duplicate peer group name
		
	$sql = "select * from peergroup_main where Name = '$peerGrpMainName' and facility_type = $facility_id";
	$sth = $dbh->prepare($sql);
	$sth->execute();
	my $rowcount = $sth->rows();
	if($rowcount>0){
		if($saction eq 'CheckSave' || $saction eq 'Check'){
			$str ="<script>alert(\"Peer group name ' $peerGrpMainName ' already exist.\");document.frmPeerActions.duplicate.value='true';parent.document.peersearch.peergrpname.value='';parent.document.peersearch.save.disabled='true';parent.document.peersearch.peergrpname.focus();</script>";
		}
		if($saction eq 'CheckPeer' || $saction eq 'CheckPeerSave'){
					$str ="<script>alert(\"Peer group name ' $peerGrpMainName ' already exist.\");document.frmPeerActions.duplicate.value='true';parent.document.frmPeerMain.peergrpname.value='';parent.document.frmPeerMain.save.disabled='true';parent.document.frmPeerMain.peergrpname.focus();window.parent.peerframe.location.href='/cgi-bin/PeerDetails.pl?reload=false&mainID=$peerId&facility=$facility';</script>";
		}
	}else{
		if($saction eq 'CheckSave'){			
			$str ="<script>document.frmPeerActions.duplicate.value='false';parent.document.peersearch.save.disabled=false;parent.PeerBuild();</script>";
		}elsif ($saction eq 'CheckPeer'){
			$str ="<script>document.frmPeerActions.duplicate.value='false';parent.document.frmPeerMain.save.disabled=false;window.parent.peerframe.location.href='/cgi-bin/PeerDetails.pl?reload=false&mainID=$peerId&facility=$facility';</script>";
		}elsif ($saction eq 'CheckPeerSave'){
			$str ="<script>document.frmPeerActions.duplicate.value='false';parent.document.frmPeerMain.save.disabled=false;parent.PeerBuild($peerId);</script>";
		}elsif ($saction eq 'CheckPeerUpload'){
			$str ="<script>document.frmPeerActions.duplicate.value='false';parent.document.frmPeerMain.save.disabled=false;parent.PeerUpload($peerId,'$facility');</script>";
		}else{
			$str ="<script>document.frmPeerActions.duplicate.value='false';parent.document.peersearch.save.disabled=false;</script>";
		}
	}
	
}

if($saction eq 'Create' || $saction eq 'CreatePeer'){

	my $peerGrpMainId;
	my $peerGrpMainName = $cgi->param('peername'); 
	my $peerGrpMainDesc = $cgi->param('desc');
	my $lstrpids = $cgi->param('strpids'); # provider numbers separated by :
	my $lstrnames = $cgi->param('strpnames'); # Hospital Names separated by :
	
	#inserting values in audit log details
	if($saction eq 'CreatePeer'){
	&insert("Create","",$peerGrpMainName,"Peer Group");
	}
	if($saction eq 'Create'){
	&insert("Create","",$peerGrpMainName,"Search Page");
	}
	# Search Criteria values
	
	my $foiId = $cgi->param('foiId');		
	#my $s_peergroupname=$session->param("nr_peername");
	my $s_zip=$session->param("nr_zip");
	my $s_county=$session->param("nr_county");
	my $s_name=$session->param("nr_name");
	my $s_address=$session->param("nr_address");
	my $s_city=$session->param("nr_city");
	my $s_prov=$session->param("nr_provider");
	my $s_state=$session->param("nr_state");
	my $s_ctype=$session->param("nr_ctype");
	my $s_msa=	$session->param("nr_msa");
	my $s_system=$session->param("nr_system");
	my $s_htype=$session->param("nr_htype");
	my $s_thosp=$session->param("nr_thosp");
	my $s_geoarea=$session->param("nr_geoarea");
	my $s_bedstart=$session->param("nr_bedstart");
	my $s_bedend=$session->param("nr_bedend");
	my $s_ftestart=$session->param("nr_ftestart");
	my $s_fteend=$session->param("nr_fteend");
	my $s_incomestart=$session->param("nr_incomestart");
	my $s_incomeend=$session->param("nr_incomeend");
	my $s_Zscoremin=$session->param("nr_Zscoremin");
	my $s_Zscoremax=$session->param("nr_Zscoremax");
	my $s_OprMarginmin=$session->param("nr_OprMarginmin");
	my $s_OprMarginmax=$session->param("nr_OprMarginmax");
	my $s_Within=$session->param("nr_Within");
	my $s_c_Miles_Of_zip=$session->param("nr_p_c_Miles_Of_zip");
		
	if($s_zip){}else{
		$s_zip='';
	}
	if($foiId){}else{
		$foiId = 'No';
	 }
	if($s_county){}else{
		$s_county='';
	}
	if($s_name){}else{
		$s_name='';
	}
	if($s_address){}else{
		$s_address='';
	}
	if($s_city){}else{
		$s_city='';
	}
	if($s_prov){}else{
		$s_prov='';
	}
	
	if($s_geoarea){
	}else{
		$s_geoarea=NULL;
	}
	if($s_thosp){
		#$s_thosp=~ s//''/g;
	}else{
		$s_thosp='';
	}
	if($s_htype){}else{
		$s_htype='';
	}
	if($s_system){}else{
		$s_system='';
	}
	if($s_state){
	$s_state=~ s/'/''/g;
	}
	else{
		$s_state='';
	}
	if($s_msa){}else{
		$s_msa='';
	}
	if($s_ctype){}else{
		$s_ctype='';
	}
	
	if($s_Within == ''){
		$s_Within=NULL;
	}
	if($s_c_Miles_Of_zip == ''){
		$s_c_Miles_Of_zip=NULL;
	}
	
	if($s_bedstart == ''){
		$s_bedstart=NULL;
	}
	if($s_bedend == ''){
		$s_bedend=NULL;
	}
	if($s_ftestart == ''){
		$s_ftestart=NULL;
	}
	if($s_fteend == ''){
		$s_fteend=NULL;
	}
	if($s_incomestart == ''){
		$s_incomestart=NULL;
	}
	if($s_incomeend == ''){
		$s_incomeend=NULL;
	}
	if($s_Zscoremin == ''){
		$s_Zscoremin=NULL;
	}
	if($s_Zscoremax == ''){
		$s_Zscoremax=NULL;
	}
	if($s_OprMarginmin == ''){
		$s_OprMarginmin=NULL;
	}
	if($s_OprMarginmax == ''){
		$s_OprMarginmax=NULL;
	}
	
	# Split and store provider numers and Hospital Names in arrays.
	
	my @arrayOfIds = split(/:/,$lstrpids);	
	my @arrayOfNames = split(/:/,$lstrnames);
	
	# Get Database connection	
	
		# Select max(peergroup_mainid) ,increment it by 1 and insert into database. 
	
		$sql = "select max(PeerGroup_ID) from peergroup_main";
		$sth = $dbh->prepare($sql);
		$sth->execute();
		
		#output database results
		@row = $sth->fetchrow_array();
		$peerGrpMainId = @row[0];
		 
		$peerGrpMainId = ($peerGrpMainId+1);
		
		# Prepare query for insertion. 
		 		  
		$sql = "insert into peergroup_main(PeerGroup_ID,Name,CreateDate,Description,CreatedBy,c_Provider_States,c_Control,c_MSA,c_System,c_Hospital_Type,C_Teaching_Hospital,C_Geographical_Area,C_Bed_Start,c_Bed_End,c_FTE_Start,c_FTE_End,c_Income_Start,c_Income_End,c_Zscore_min,c_Zscore_max,c_OprMargin_min,c_OprMargin_max,c_Within,c_Miles_Of_zip,c_Provider_ID,c_Provider_Name,c_Provider_Address,c_Provider_City,c_Provider_zip,c_Provider_country,facility_type) values($peerGrpMainId,'$peerGrpMainName',CURRENT_DATE,'$peerGrpMainDesc',$userID,'$s_state','$s_ctype','$s_msa','$s_system','$s_htype','$s_thosp',$s_geoarea,$s_bedstart,$s_bedend,$s_ftestart,$s_fteend,$s_incomestart,$s_incomeend,$s_Zscoremin,$s_Zscoremax,$s_OprMarginmin,$s_OprMarginmax,$s_Within,$s_c_Miles_Of_zip,'$s_prov','$s_name','$s_address','$s_city','$s_zip','$s_county',$facility_id)";
		$sth = $dbh->prepare($sql);
		$sth->execute(); 	
		
		# Loop through array and insert peer grop details.           
		for(my $i=0;$i<($#arrayOfIds+1);$i++){
		    my $foiValue ='false';
		    if($foiId == @arrayOfIds[$i]){
		    	$foiValue = 'true';
		    }
		    if(@arrayOfIds[$i] eq ''){
			next;
		    }else{		    	
			$sql = "insert into peergroup_details(PeerGroup_Main_ID,Provider_number,ProviderName,FOI) values($peerGrpMainId,'".&trim(@arrayOfIds[$i])."','".&trim(@arrayOfNames[$i])."',$foiValue)";					
			$sth = $dbh->prepare($sql);
			$sth->execute();
		    }
		}
	
		$str ="<script>alert('Peer group created successfully.');window.parent.location.href='/cgi-bin/PeerMain.pl?facility=$facility';</script>";
	
}	
if($saction eq 'Update'){

	my $updPeerName = $cgi->param('txtpeername');
	my $updDesc = $cgi->param('txtdescription');
	my $peerId = $cgi->param('updId');
	my $sqlAud = "select name from  peergroup_main where PeerGroup_ID = $peerId"; 
			
	$sth = $dbh->prepare($sqlAud);

	$sth->execute();

	while (my $recSet = $sth->fetchrow_hashref)
	{
		&update ("Update","",$updPeerName,$$recSet{name},"Peer Group");	
	}
	$sql = "update peergroup_main set  Name ='".$updPeerName."' , Description ='".$updDesc."' , AlterDate = CURRENT_DATE, ModifiedBy = $userID where PeerGroup_ID = $peerId"; 
	$sth = $dbh->prepare($sql);
	$sth->execute();
	
	$str ="<script>alert('Peer group updated successfully.');window.parent.location.href='/cgi-bin/PeerMain.pl?facility=$facility';</script>";
	
	$sth->finish();	
	$dbh->disconnect();
	
}

if($saction eq 'UploadPeer'){
	my $line;
	my @peer,@imp_prov;
	my %seen;
	my @prov_imp,@fac_name,@fac_num;
	my $string;
	my $peerGrpMainDesc = $cgi->param('desc');
	my $peerGrpMainName = $cgi->param('peername');
	my $operator='\r\n';
	my $count=0;
	my $i=0;
	my $log;
	my $p_count=0;	
	my $path = $cgi->param('myfile');
	my $upload_filehandle = $cgi->upload('myfile');

	#inserting values in audit log details
	&insert("Create","Peer import from CSV file",$peerGrpMainName,"Peer Group");
	
	#Upload file to specified directory
	open ( UPLOADFILE, ">$upload_dir/$path" ) or die "$!";  
	binmode UPLOADFILE;  
	while ( <$upload_filehandle> ){  
	 print UPLOADFILE;  
	}   
	close UPLOADFILE;    
		
	# Select max(peergroup_mainid) ,increment it by 1 and insert into database. 
	$sql = "select max(PeerGroup_ID) from peergroup_main";
	$sth1 = $dbh->prepare($sql);
	$sth1->execute();
			
	#output database results
	@row = $sth1->fetchrow_array();
	$peerGrpMainId = @row[0]; 
	$peerGrpMainId = ($peerGrpMainId+1);		
						
			open INPUT, "$upload_dir/$path" or die "can't open Book2.csv";
			while ($line = <INPUT>){
			$line =~ s/\t\n/-/g;
			push(@peer,$line);
			chomp(@peer);
			$count++;
			}
			close INPUT;			
			my $size = $count;	#total number of providers imported
			
			foreach my $peer(@peer){
				$string=@peer[$i];
				$string =~ s/\r|\n//g;
				if($string){
														
					if(length($string) == 6){
					push(@imp_prov,$string);
					}elsif(length($string) != 6){
						unlink("$upload_dir/$path");
						$errStr = "Import file contains invalid \\nprovider numbers ";
						break;
					}
				}	
					$i++;
			}			
			open(J, ">data.txt");
						print J $facility ;
						close J;		
		#store the array in hash with no. of duplicates as key value	  
		for ( my $j = 0; $j <= $#imp_prov ; )
		{
			splice @imp_prov,--$j,1
			if $seen{$imp_prov[$j++]}++;	 
		}

		my $h_size=scalar keys %seen;
		
		foreach $key ( %seen){
			if($seen{$key} !=1 && $seen{$key} != "" ){
				unlink("$upload_dir/$path");
				$errStr = $errStr."Import file contains duplicate \\nprovider numbers";
				$log=0;
				break;
			}elsif($seen{$key} ==1 && $h_size==$size){
				$log=1;
			}
		}
		my $count_p ;
		if($log==1){
			foreach my $imp_prov(@imp_prov){
				$imp_prov =~ s/\r|\n//g;
				$sql = "select Name,providerno from $master_table where providerno = ".$imp_prov."";
				$sth2 = $dbh->prepare($sql);
				$sth2->execute(); 
				
				$count_p = $sth2->rows();
				# stoe the imported provider 
				while ($row = $sth2->fetchrow_hashref){
					push (@fac_name, $$row{Name});
					push(@fac_num,$$row{providerno});
				}	
				
				if($count_p==1){
					$p_count++;
				}else{
					$log=2;
				}
			
			}
		}
		 	 
		if($p_count==$size && $log==1){
			# Prepare query for insertion.		
			$sql_r = "Insert into peergroup_main(PeerGroup_ID,Name,CreateDate,Description,CreatedBy,facility_type) values($peerGrpMainId,'$peerGrpMainName',CURRENT_DATE,'$peerGrpMainDesc',$userID,$facility_id)";
			$sth3 = $dbh->prepare($sql_r);
			$sth3->execute(); 
				
			for my $c (0 .. $#imp_prov) {
				$sql_u="Insert into peergroup_details(PeerGroup_Main_ID,Provider_number,ProviderName) values ($peerGrpMainId,'$fac_num[$c]','$fac_name[$c]'); ";
				$sth4 = $dbh->prepare($sql_u);
				$sth4->execute();				
			}		
				
							unlink("$upload_dir/$path");
				$errStr = "Peer group created successfully.The total number of providers imported is $size "; 		
		}elsif($log==2){
					unlink("$upload_dir/$path");
					$errStr = $errStr."Imported provider number \\nmismatches with facility master table ";
		}
			
			$str = "<script>alert('$errStr');window.parent.location.href='/cgi-bin/PeerMain.pl?facility=$facility';</script>";				
}



if($saction eq 'PeerSearch_Save'){
	my $s_prov_Ids=$cgi->param('prov_Id');
	my $foiId = $cgi->param('foi');				
	my $s_prov_name=$cgi->param('prov_Names');
	# my @arrayOfIds = split(/ /,$s_prov_Ids);	
	# my @arrayOfNames = split(/~/,$s_prov_name);	
	my @arrayOfIds = split(/:/,$s_prov_Ids);	
	my @arrayOfNames = split(/:/,$s_prov_name);
	open(J, ">data.txt");
						print J $foiId;
						close J;
	my $peer_Id = $cgi->param('peer_id');
	my $peer_name=$cgi->param('peer_name');
	my $s_zip=$session->param("nr_zip");
	my $s_county=$session->param("nr_county");
	my $s_name=$session->param("nr_name");
	my $s_address=$session->param("nr_address");
	my $s_city=$session->param("nr_city");
	my $s_prov=$session->param("nr_provider");
	my $s_peergroupname=$session->param("nr_peername");
	my $s_state=$session->param("nr_state");
	my $s_ctype=$session->param("nr_ctype");
	my $s_msa=	$session->param("nr_msa");
	my $s_system=$session->param("nr_system");
	my $s_htype=$session->param("nr_htype");
	my $s_thosp=$session->param("nr_thosp");
	my $s_geoarea=$session->param("nr_geoarea");
	my $s_bedstart=$session->param("nr_bedstart");
	my $s_bedend=$session->param("nr_bedend");
	my $s_ftestart=$session->param("nr_ftestart");
	my $s_fteend=$session->param("nr_fteend");
	my $s_incomestart=$session->param("nr_incomestart");
	my $s_incomeend=$session->param("nr_incomeend");
	my $s_Zscoremin=$session->param("nr_Zscoremin");
	my $s_Zscoremax=$session->param("nr_Zscoremax");
	my $s_OprMarginmin=$session->param("nr_OprMarginmin");
	my $s_OprMarginmax=$session->param("nr_OprMarginmax");
	my $s_Within=$session->param("nr_Within");
	my $s_c_Miles_Of_zip=$session->param("nr_p_c_Miles_Of_zip");
	
	if($s_zip){}else{
		$s_zip='';
	}
	if($s_county){}else{
		$s_county='';
	}
	if($s_name){}else{
		$s_name='';
	}
	if($s_address){}else{
		$s_address='';
	}
	if($s_city){}else{
		$s_city='';
	}
	if($s_prov){}else{
		$s_prov='';
	}
	if($s_geoarea){
	}else{
		$s_geoarea=NULL;
	}
	if($s_thosp){
		#$s_thosp=~ s/'/''/g;
	}else{
		$s_thosp='';
	}
	if($s_htype){}else{
		$s_htype='';
	}
	if($s_system){}else{
		$s_system='';
	}
	if($s_state){
	$s_state=~ s/'/''/g;
	}
	else{
		$s_state='';
	}
	if($s_msa){}else{
		$s_msa='';
	}
	if($s_ctype){}else{
		$s_ctype='';
	}
	if($s_Within == ''){
		$s_Within=NULL;
	}
	if($s_c_Miles_Of_zip == ''){
		$s_c_Miles_Of_zip=NULL;
	}
	
	if($s_bedstart == ''){
		$s_bedstart=NULL;
	}
	if($s_bedend == ''){
		$s_bedend=NULL;
	}
	if($s_ftestart == ''){
		$s_ftestart=NULL;
	}
	if($s_fteend == ''){
		$s_fteend=NULL;
	}
	if($s_incomestart == ''){
		$s_incomestart=NULL;
	}
	if($s_incomeend == ''){
		$s_incomeend=NULL;
	}
	if($s_Zscoremin == ''){
		$s_Zscoremin=NULL;
	}
	if($s_Zscoremax == ''){
		$s_Zscoremax=NULL;
	}
	if($s_OprMarginmin == ''){
		$s_OprMarginmin=NULL;
	}
	if($s_OprMarginmax == ''){
		$s_OprMarginmax=NULL;
	}
	if($foiId){}else{
			$foiId = 'No';
	 }
	$sql="update peergroup_main set Name='".$peer_name."',c_System='".$s_system."',c_Control='".$s_ctype."',c_Hospital_Type='".$s_htype."',c_MSA='".$s_msa."',C_Bed_Start=".$s_bedstart.",c_Bed_End=".$s_bedend.",c_FTE_Start=".$s_ftestart.",c_FTE_End=".$s_fteend.",c_OprMargin_min=".$s_OprMarginmin.",c_OprMargin_max=".$s_OprMarginmax.",C_Teaching_Hospital='".$s_thosp."',C_Geographical_Area=".$s_geoarea.",c_Zscore_min=".$s_Zscoremin.",c_Zscore_max=".$s_Zscoremax.",c_Income_Start=".$s_incomestart.",c_Income_End=".$s_incomeend.",c_Within=".$s_Within.",c_Miles_Of_zip=".$s_c_Miles_Of_zip.",AlterDate = CURRENT_DATE, ModifiedBy = ".$userID.",c_Provider_ID='".$s_prov."',c_Provider_Name='".$s_name."',c_Provider_Address='".$s_address."',c_Provider_City='".$s_city."',c_Provider_zip='".$s_zip."',c_Provider_country='".$s_county."',c_Provider_States='".$s_state."' where PeerGroup_ID =".$peer_Id." and facility_type=".$facility_id."";
	$sth = $dbh->prepare($sql);
	$sth->execute();
	
	# Delete Peer Group Main records.

	$sql1 = "delete from peergroup_details where PeerGroup_Main_ID = ".$peer_Id.""; 	
	$sth1 = $dbh->prepare($sql1);
	$sth1->execute();	
	
	
	# Loop through array and insert peer grop details.           
		for(my $i=0;$i<($#arrayOfIds+1);$i++){
		    my $foiValue ='false';
		    if($foiId == @arrayOfIds[$i]){
		    	$foiValue = 'true';
		    }
		    if(@arrayOfIds[$i] eq ''){
			next;
		    }else{		    	
			$sql2 = "insert into peergroup_details(PeerGroup_Main_ID,Provider_number,ProviderName,FOI) values(".$peer_Id.",'".&trim(@arrayOfIds[$i])."','".&trim(@arrayOfNames[$i])."',$foiValue)";					
			$sth = $dbh->prepare($sql2);
			$sth->execute();
		    }
		}
		&update ("Update","Search criteria updated",$peer_name,$peer_name,"Peer Group");		
	$str ="<script>alert('Peer group updated successfully.');window.parent.location.href='/cgi-bin/PeerMain.pl?facility=$facility';</script>";
	$sth->finish();	
	$dbh->disconnect();

}

if($saction eq 'Delete'){
	$peerId = $cgi->param('delID');
	
	#to insert values in audit log
	my $sqlAud = "select name from  peergroup_main where PeerGroup_ID = $peerId"; 
		
	$sth = $dbh->prepare($sqlAud);
	
	$sth->execute();
	
	while (my $recSet = $sth->fetchrow_hashref)
	{
		&delete ("Delete","",$$recSet{name},"Peer Group");	
	}
	# Delete Peer Group Details records. 
	
	$sql = "delete from peergroup_details where PeerGroup_Main_ID = $peerId"; 
	
	$sth = $dbh->prepare($sql);
	$sth->execute();
	
	# Delete Peer Group Main records.

	$sql1 = "delete from peergroup_main where PeerGroup_ID = $peerId"; 	
	$sth1 = $dbh->prepare($sql1);
	$sth1->execute();	
		
	$str ="<script>alert('Peer group deleted successfully.');window.parent.location.href='/cgi-bin/PeerMain.pl?facility=$facility';</script>";
	
	$sth->finish();
	$sth1->finish();	
	$dbh->disconnect();

}

if($saction eq 'UpdatePeer'){
	my $peerid = $cgi->param('updId');
	my $foiId = $cgi->param('foiId');
	my $lstrpids = $cgi->param('strpids'); 
	my $lstrnames = $cgi->param('strpnames'); 
	my @arrayOfIds = split(/:/,$lstrpids);	

	my @arrayOfNames = split(/:/,$lstrnames);
	for(my $i=0;$i<($#arrayOfIds+1);$i++){
		 my $foiValue ='false';
		    if($foiId == @arrayOfIds[$i]){
		    	$foiValue = 'true';
		    }
		    if(@arrayOfIds[$i] eq ''){
			next;
		    }else{				
			my $psql = "insert into peergroup_details(PeerGroup_Main_ID,Provider_number,ProviderName,FOI) values($peerid,'".&trim(@arrayOfIds[$i])."','".&trim(@arrayOfNames[$i])."',$foiValue)";					
			$sth = $dbh->prepare($psql);
			$sth->execute();	
		    }
		}
			
	my $sqlAud = "select name from  peergroup_main where PeerGroup_ID = $peerid"; 
			
	$sth = $dbh->prepare($sqlAud);

	$sth->execute();

	while (my $recSet = $sth->fetchrow_hashref)
	{
		&update ("Update","Peer providers updated",$$recSet{name},$$recSet{name},"Peer Group");	
	}
			
	$str ="<script>alert('Peer group updated successfully.');window.parent.location.href='/cgi-bin/PeerMain.pl?facility=$facility';</script>";
 } 
 
if($saction eq 'delDetails'){
	$mainId = $cgi->param('i1');
	my $providerno = $cgi->param('i2');

	my $sqlAud = "select name from  peergroup_main where PeerGroup_ID = $mainId"; 
		
	$sth = $dbh->prepare($sqlAud);
	
	$sth->execute();
	
	while (my $recSet = $sth->fetchrow_hashref)
	{
		&delete ("Delete","",$$recSet{name},"Peer Group");	
	}
	# Delete Peer Group Details records. 
	
	$sql = "delete from peergroup_details where PeerGroup_Main_ID = $mainId and Provider_number = $providerno"; 
	
	$sth = $dbh->prepare($sql);
	$sth->execute();
	
	# Update the modified date.
		
	$sql2 ="update peergroup_main set AlterDate = CURRENT_DATE, ModifiedBy = $userID where PeerGroup_ID = $mainId";
	$sth2 = $dbh->prepare($sql2);
	$sth2->execute();
	
	$str ="<script>alert('Peer group details deleted successfully.');window.location.href='/cgi-bin/PeerDetails.pl?reload=true&mainID='+$mainId+'&facility=$facility';</script>";
	
	$sth->finish();	
	$sth2->finish();	
	$dbh->disconnect();

}  
  
print "Content-Type: text/html\n\n";
print <<EOF
<HTML>
<HEAD><TITLE>PEER GROUP DETAILS</TITLE>
<LINK REL="shortcut icon" HREF="/icons/favicon.ico" TYPE="image/x-icon">
<LINK REL="stylesheet" TYPE="text/css" HREF="/css/common_css/rounded_button.css">
<LINK REL="stylesheet" TYPE="text/css" HREF="/css/common_css/milo.css">
<script type="text/javascript" src="/JS/peer_group/peer_main.js"></script>
<script type="text/javascript" src="/JS/peer_group/peersearch.js"></script>
</HEAD>
<BODY>
<FORM name="frmPeerActions">
<input type="hidden" name="duplicate" value="">
<input type="hidden" name="frm_path" value="">
EOF
;    
print "$str";
print qq{
</FORM>
</BODY>
</HTML>
};

#-----------------------------------------------------------------------------


sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#--------------------------------------------------------------------------------------
sub sessionExpire
{

print "Content-Type: text/html\n\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<LINK REL="stylesheet" TYPE="text/css" HREF="/css/common_css/milo.css">
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}
#----------------------------------------------------------------------------

sub trim($)
{
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;

	return $string;
}
#----------------------------------------
sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
<LINK REL="shortcut icon" HREF="/icons/favicon.ico" TYPE="image/x-icon">
<LINK REL="stylesheet" TYPE="text/css" HREF="/css/common_css/rounded_button.css">
<LINK REL="stylesheet" TYPE="text/css" HREF="/css/common_css/milo.css">
</HEAD>
<BODY>
<CENTER>
<BR>
<BR>
<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/cgi-bin/index.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/cr_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="/cgi-bin/cr_batchx.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="/cgi-bin/snf_batch1.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="/cgi-bin/standard_reports.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Standard Reports</div>">
			        <img src="/images/stardardreport_inner_icon.gif" onmouseover="src='/images/stardardreport_inner_icon_hover.gif';"
                                    onmouseout="src='/images/stardardreport_inner_icon.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 102px" align="Center">
                            <a href="/cgi-bin/PeerMain.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
			                                    <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->
    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}