#!/usr/bin/perl -w

#----------------------------------------------------------------------------
#  program: dump.pl
#----------------------------------------------------------------------------

use strict;
use CGI;

my $cgi = new CGI;

my ($name, $value);

my @names = $cgi->param;

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>DUMP</TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css" />
</HEAD>
<BODY>
<CENTER>
<BR>
<BR>
<FONT SIZE="5" COLOR="RED">
<B>CGI PARAM DUMPER</B><BR>
</FONT>
<BR>
<TABLE CLASS="dentry" BORDER="1" CELLPADDING="1" WIDTH="700">
};

my $i = 0;

foreach $name (@names) {
    $value = $cgi->param("$name");
    print "   <TR>\n";
    print "      <TD ALIGN=\"center\">$i</TD>\n";
    print "      <TD ALIGN=\"left\">$name</TD>\n";
    print "      <TD ALIGN=\"left\">$value</TD>\n";
    print "   </TR>\n";
    ++$i;
}

print qq{
</TABLE>
<FORM ACTION="#">
<INPUT TYPE="button" VALUE=" CLOSE " onclick="window.close()">
</FORM>
</CENTER>
</BODY>
</HTML>
};

exit 0;

