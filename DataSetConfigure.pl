#!/usr/bin/perl

use CGI;
use DBI;
use strict;

do "common_header.pl";
print "Content-Type: text/html\n\n";
&headerScript();
my $cgi = new CGI;
do "pagefooter.pl";
use CGI::Session;

my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $orderBy = $cgi->param("orderBy");
if(!$orderBy){
	$orderBy = "reports_name";
}
if(!$userID)
{
	&sessionExpire;
}


my $text = $cgi->param('providers');
my $checkedYear = $cgi->param('yearCheck');
my $datasetIds = $cgi->param('datasetIds');
my $dataElementIds = $cgi->param('dataElementIds');

$datasetIds = substr($datasetIds, 1);
$dataElementIds = substr($dataElementIds, 1);

# Database

my $dbh;
my ($sql, $sth,$sql_f,$sth1,$facility_id,$row,$row1);

my($rpt_id,$rpt_name,$rpt_status,$rpt_createdate,$rpt_createdby,$rpt_alterdate,$rpt_modifiedby,$class1,$s);
# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my $defaultYear= $ini_value{'DEFAULT_YEAR'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

my $facility = $cgi->param("facility");
$sql_f = "select facility_id from tbl_facility_type where facility_name = '$facility'";
$sth1 = $dbh->prepare($sql_f);
$sth1->execute();

while ($row = $sth1->fetchrow_hashref) {
	$facility_id = $$row{facility_id};
}

#$sql = "select * from standard_reports order by $orderBy";
#$sql = "select reports_id,reports_name,reports_status,CreateDate,AlterDate,(select UserName from users where UserID = standard_reports.CreatedBy) as CreatedBy ,(select UserName from users where UserID = standard_reports.ModifiedBy) as ModifiedBy from standard_reports order by $orderBy";
$sql = "select reports_id,reports_name,reports_status,CreateDate,AlterDate,(select concat(First_Name, ' ',Last_Name) as CreatedBy from userdetails where UsersID = standard_reports.CreatedBy) as CreatedBy ,(select concat(First_Name, ' ',Last_Name) as ModifiedBy from userdetails where UsersID = standard_reports.ModifiedBy) as ModifiedBy from standard_reports where facility_type = $facility_id order by $orderBy";
$sth = $dbh->prepare($sql);
$sth->execute();


my $divHeight = 0;
my $rows_returned = $sth->rows();

if($rows_returned<200){
  $divHeight = ($rows_returned * 50) + "px";
}else{
  $divHeight = "200px";
}



print <<EOF
<HTML>
<HEAD><TITLE>Standard Reports</TITLE>

<link href="/css/admin_panel/standard_report/DataSetConfigure.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="/JS/admin_panel/standard_report/DataSetConfigure.js" ></script>
</HEAD>
<BODY onload="ShowDetails('$facility');"><!--<a style="display:scroll;position:fixed;left:5px;bottom:5px;" href="#" title="Feedback Please"><img border=0 src="/images/feedback.png"/></a>-->
<FORM NAME = "frmReportsMain" Action="/cgi-bin/PeerActions.pl" method="post">
<CENTER>
<div id='bodyDiv' style="position:absolute;z-index:100;display:none;background-image:url(/images/images/background-trans.png);height:100%;width:100%;"><div align='center'  id="displaybox" style="display: none;"></div></div>
EOF
;
&innerHeader();
print <<EOF
    <!--content start-->

      <table cellpadding="0" cellspacing="0" width="885px">
        <tr>
            <td valign="top">
                <img src="/images/content_left.gif" /></td>
            <td valign="top" style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; ">

		<table width="100%">
		  <tr>
			<td><table bgColor=\"#B4CDCD\" width=\"100%\" align=\"right\"><tr><td width=\"55%\"><b>Standard Reports :</b></td><td><select onchange=\"SortBy(1,'$facility')\" name=\"SortMain\" id=\"SortMain\"><option value=\"\">Sort By</option><option value=\"reports_name\">Name</option><option value=\"CreateDate\">Created Date</option><option value=\"AlterDate\">Modified Date</option><option value=\"CreatedBy\">Created By</option><option value=\"ModifiedBy\">Modified By</option></select></td><td><span  class=\"button\"><INPUT TYPE=\"button\" VALUE=\" Create \" onClick=\"CallCreateReport('$facility')\"></span></td><td><span  class=\"button\"><INPUT TYPE=\"button\" VALUE=\" Edit \" onClick=\"CallEditReport('$facility')\"></span></td><td><span  class=\"button\"><INPUT TYPE=\"button\" VALUE=\" Delete \" onclick=\"delRep('$facility')\"></span></td></tr></table></td>
		  </tr>
		  <tr>
   			<td>
   				<div style="position:static;overflow:scroll;height:150px;">
				<TABLE class="gridstyle" align='center' CELLPADDING="0" CELLSPACING="0" width="100%">

					<tr class='gridHeader'>
						<td>SlNo</td><td>Name</td><td>Created On</td><td>Created By</td><td>Modified On</td><td>Modified By</td><td>select</td>
					</tr>
EOF
;

				my $i = 0;
				while ($row1 = $sth->fetchrow_hashref) {
					$rpt_id = $$row1{reports_id};
					$rpt_name = $$row1{reports_name};
					$rpt_status = $$row1{reports_status};
					$rpt_createdate = $$row1{CreateDate};
					$rpt_createdby = $$row1{CreatedBy};
					$rpt_alterdate = $$row1{AlterDate};
					$rpt_modifiedby = $$row1{ModifiedBy};
					$class1 = (++$i % 2) ? 'gridrow' : 'gridalternate';
					if($i == 1){
						print "<tr class=\"$class1\"><td>$i</td><td><a href=\"/cgi-bin/ReportDetails.pl?reload=false&mainID=$rpt_id&dtName=$rpt_name&facility=$facility\" target=\"Reportsframe\" onclick=\"CheckIt($i)\">$rpt_name</a></td><td>$rpt_createdate</td><td>$rpt_createdby</td><td>$rpt_alterdate</td><td>$rpt_modifiedby</td><td><input id=\"chk$i\" onclick=\"SetReady(this,$i,$rpt_id,'$rpt_name','$facility')\" type=\"checkbox\" name=\"delReports\" value=\"$rpt_id\" checked></td></tr>\n";
					}else{
						print "<tr class=\"$class1\"><td>$i</td><td><a href=\"/cgi-bin/ReportDetails.pl?reload=false&mainID=$rpt_id&dtName=$rpt_name&facility=$facility\" target=\"Reportsframe\" onclick=\"CheckIt($i)\">$rpt_name</a></td><td>$rpt_createdate</td><td>$rpt_createdby</td><td>$rpt_alterdate</td><td>$rpt_modifiedby</td><td><input id=\"chk$i\" onclick=\"SetReady(this,$i,$rpt_id,'$rpt_name','$facility')\" type=\"checkbox\" name=\"delReports\" value=\"$rpt_id\"></td></tr>\n";
					}
					if($i == 1){
					   	print "<input type=\"hidden\" name=\"firstid\" value=\"$rpt_id\">";
					   	print "<input type=\"hidden\" name=\"firstName\" value=\"$rpt_name\">";
  					}
				}


print "</TABLE>\n";
print "</div>\n";
print "</td>\n";
print " </tr>\n";
print "<tr>\n";
print "<td><table bgColor=\"#B4CDCD\" width=\"100%\" align=\"right\"><tr><td width=\"55%\"><span id=\"dtLabel\" style=\"font-weight:bold;\">Details of : </span><span id=\"dtName\"></span></td><td align=\"left\"><select onchange=\"SortBy(2,'$facility')\" name=\"SortDetail\"><option value=\"\">Sort By</option><option value=\"element\">Element</option><option value=\"Formulae\">Worksheet</option></select></td><td><span  class=\"button\"><INPUT TYPE=\"button\" id=\"btnCancel\" VALUE=\"Cancel\" onclick=\"CallCancel('$facility')\" ></span></td><td><span  class=\"button\"><INPUT TYPE=\"button\" id=\"btnSave\" VALUE=\"Save\" onClick=\"CallSave(1,'$facility')\" disabled></span></td><td><span  class=\"button\"><INPUT TYPE=\"button\" id=\"btnAddMore\" VALUE=\"Add Columns\" onClick=\"CallSave(2)\" disabled></span></td><td></td></tr></table></td>\n";
print "</tr>\n";
print "<tr>\n";
print "<td valign=\"top\">\n";
print "<iframe name=\"Reportsframe\"  width=\"100%\" height=\"230px\" frameborder=0 cellpadding=0 cellspacing=0 ></iframe>\n";
print "</td>\n";
print "</tr>\n";
print "</table>\n";
print "</td>\n";
print "<td valign=\"top\">\n";
print "<img src=\"/images/content_right.gif\" />\n";
print "</td>\n";
print "</tr>\n";
print "</table>\n";
print "</CENTER><!--content end-->\n";
&TDdoc;
print "<!-- Reportdetails on body load will set 'rpt_mainid' value used for Edit functionality-->\n";
print "<input type=\"hidden\" name=\"rpt_mainid\">\n";
print "<input type=\"hidden\" name=\"rpt_name\">\n";
print "<input type=\"hidden\" name=\"saction\">\n";
print "<input type=\"hidden\" name=\"callSave\">\n";
print "<input type=\"hidden\" name=\"facility\" value='$facility'>\n";
print "<input type=\"hidden\" name=\"orderBy\" value='$orderBy'>\n";
print "</FORM>\n";
print "</BODY>\n";
print "</HTML>\n";

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#------------------------------------------------------------------------------
sub get_value
{
   my ($report_id, $worksheet, $line, $column, $fYear) = @_;

   my $yyyy = $fYear;

   my ($sthVal, $value);

   my $sql = qq(
    SELECT CR_VALUE
      FROM CR_ALL_DATA
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?

   );

   unless ($sthVal = $dbh->prepare($sql)) {
       &display_error('Error preparing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sthVal->execute($report_id, $worksheet, $line, $column)) {
       &display_error('Error executing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

   if ($sthVal->rows) {
      $value = $sthVal->fetchrow_array;
   }

   $sthVal->finish();

   return $value;
}
#--------------------------------------------------------------------------------------

sub sessionExpire
{


print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}


print qq{
<HTML>
<HEAD><TITLE>Standard Reports-ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
<BR>
<BR>
<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/cgi-bin/index.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/cr_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="/cgi-bin/cr_batchx.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="/cgi-bin/snf_batch1.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="/cgi-bin/standard_reports.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Standard Reports</div>">
			        <img src="/images/stardardreport_inner_icon.gif" onmouseover="src='/images/stardardreport_inner_icon_hover.gif';"
                                    onmouseout="src='/images/stardardreport_inner_icon.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 102px" align="Center">
                            <a href="/cgi-bin/PeerMain.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
			                                    <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->
    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}
