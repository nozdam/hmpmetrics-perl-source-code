#!/usr/bin/perl
#----------------------------------------------------------------------------
#  program: cr_batchx.pl
#  author: Steve Spicer
#  Written: Wed Oct 24 20:14:32 CDT 2007
#  Revised: Sun Jun 14 22:31:17 CDT 2009
#----------------------------------------------------------------------------

use strict;
use Spreadsheet::WriteExcel;
use Date::Calc qw(Delta_Days);
use CGI;
use DBI;
do "common_header.pl";
do "pagefooter.pl";
use CGI::Session;
do "audit.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");

if(!$userID)
{
	&sessionExpire;
}

my $start_time = time;


my $action    = $cgi->param('action');    
my $text      = $cgi->param('providers'); # aaa.txt bbb.txt
my $show_keys = $cgi->param('showkeys');
my $dir       = $cgi->param('direction');
my $year      = $cgi->param('year');
my $imp       = $cgi->param('imp');       # import peer list  Y/N or nothing;
my @datasets  = $cgi->param('datasets');

my $loadPeer  = $cgi->param('loadPeer');
my $peerID    = $cgi->param('peerID');

my $checkedYear    = $cgi->param('yearCheck');

&view("View","","Hospital spreadsheet generator");
# String holding provider IDs separated by ';'
my $providerIds = $cgi->param('Ids');
my $test = '';
my @arrayOfIds = split(/:/,$providerIds);

my $remote_user  = $ENV{'REMOTE_USER'};

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};

my ($dbh, $sql, $sth,$sql_f,$facility_id,$master_table,$report_table,$data_table,$row);

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error('ERROR connecting to database', $DBI::errstr);
   exit(1);
}
my $facility = $cgi->param("facility");
$sql_f = "select facility_id ,master_table,report_table,data_table from tbl_facility_type where facility_name = '$facility'";
$sth = $dbh->prepare($sql_f);
$sth->execute();

while ($row = $sth->fetchrow_hashref) {
	$facility_id = $$row{facility_id};
	$master_table = $$row{master_table};
	$report_table = $$row{report_table};
	$data_table = $$row{data_table};
}
open(J, ">data.txt");
print J $sql_f;
close J;
if($loadPeer)
{
	@arrayOfIds = &peerGroupDetails($peerID);
}

unless ($action eq 'go') {
   &display_form();
   exit(0);
}

$year = $ini_value{'DEFAULT_YEAR'} unless ($year =~ m/^\d\d\d\d$/);  # avoid any sql injection possibilities

my ($line, @list, $s, $len, $string, $provider, $n, $width, $value, $row, $column, $desc);

my @provider_list;

foreach $s (split /\n/,$text) {
   $s =~ s/[ \r\n]//g;
   $len = length($s);

   if ($s =~ m/^\S{6}[\:\/](\d{4}|ALL)$/) {
      push @provider_list, $s;
   } elsif ($s =~ m/(^\S{6})[\s-\s]/) {
      push @provider_list, $1;
   } elsif ($s =~ m/^\S{6}$/) {
      push @provider_list, $s;
   }
}

$n = @provider_list;

unless ($n) {
   &display_error('YOU MUST PROVIDE ONE OR MORE 6 CHARACTER PROVIDER IDS');
   exit(0);
}



my %mem_provider;
my %mem_rpt_year;
my %mem_fy_bgn_dt;
my %mem_fy_end_dt;
my %mem_fi_num;
my %mem_stus_cd;

my ($key, $yyyy, $rpt_num, $rpt_year, $rpt_stus_cd, $fy_bgn_dt, $fy_end_dt);

my @report_id_list = ();

#foreach $string (@provider_list) {
#   if ($string =~ m/(\S{6})[:\/\t]ALL/i)  {
#      &load_reports('all', $1);
#   } elsif ($string =~ m/(^\S{6})[:\/\t](\d{4})$/)  {
#      &load_reports('peryear',$1,$checkedYear);
#   } elsif ($string =~ m/^(\S{6})$/) {
#      &load_reports('recent',$1);
#   }
#}

# Jayanth - this below code is implemented for selecting multiple years. 

foreach $string (@provider_list) {
	if ($string =~ m/^(\S{6})$/) {
		if($checkedYear != "") {
			&load_reports('peryear',$1,$checkedYear);
		}
		else
		{
			&load_reports('recent',$1);
		}
	} 
}

#----------------------------------------------------------------------------

my @report_definition;

my $fname;

foreach $fname (@datasets) {
   open(INPUT, "./packages/$fname");
   while ($line = <INPUT>) {
     chomp($line);
     push @report_definition, $line;
	
   }
   close INPUT;
}

#----------------------------------------------------------------------------

my $workbook = Spreadsheet::WriteExcel->new('-');
#  $workbook->set_tempdir('/tmp');

my $sheet1 = $workbook->addworksheet('main');

my $amount_format = $workbook->addformat();
   $amount_format->set_num_format(0x2b);           #  (#,##0.00_); (#,##0.00)

my $money_format = $workbook->addformat();
   $money_format->set_num_format(0x2c);            #  (#,##0.00_); (#,##0.00)

my $amount_bold_format = $workbook->addformat();
   $amount_bold_format->set_num_format(0x2c);      #  (#,##0.00_); (#,##0.00)
   $amount_bold_format->set_bold();

my $date_format = $workbook->addformat();
   $date_format->set_num_format(0x0e);             #  m/d/yy
   $date_format->set_align("center");

my $number_format = $workbook->addformat();
   $number_format->set_num_format(0x01);           #  0 integer

my $comma_format = $workbook->addformat();
   $comma_format->set_num_format(0x26);            #  (#,###) red

my $format_bold = $workbook->addformat();
   $format_bold->set_bold();

my $big_format_bold = $workbook->addformat();      
   $big_format_bold -> set_bold();
   $big_format_bold -> set_size(14);

my $format_bold_rotated = $workbook->addformat();
   $format_bold_rotated->set_bold();
   $format_bold_rotated->set_rotation(80);

my $format_bold_right  = $workbook->addformat();
   $format_bold_right->set_bold();
   $format_bold_right->set_align("right");


my $col = 0;
my $row = 0;

$n = @report_id_list;

if ($dir eq 'across') {
   if ($show_keys eq 'on') {
       $sheet1->set_column($col, $col, 24);
       ++$col;
   }
   $sheet1->set_column($col, $col, 40);
   ++$col;
   $sheet1->set_column($col, $col + $n, 15);
}

my $now = localtime();

$sheet1->write_string($row,   0, 'Healthcare Management Partners', $big_format_bold);
$sheet1->write_string(++$row, 0, 'Medicare Cost Report Data extract', $big_format_bold);
$sheet1->write_string(++$row, 0, "GENERATED: $now", $format_bold);

&process_report();

my $elapsed_time = time - $start_time;

# $sheet1->write_number($row, 0, $elapsed_time, $number_format);

$dbh->disconnect();

print "Content-type: application/vnd.ms-excel\n";
print "Content-Disposition: attachment; filename=cost_report_batch.xls\n";
print "Cache-Control: no-cache\n";
print "Pragma: no-cache\n\n";

$workbook->close();

exit(0);


#=============================< SUBROUTINES >================================

# across ... the hospitals left to right columns
# down  .... the hospitals are down the page

sub process_report
{
   $row += 2;
   $col  = 0;
	 &insert("Create","Spreadsheet generated","","Hospital spreadsheet generator");
   my $format = ($dir eq 'across') ? $format_bold : $format_bold_rotated;
   
   if ($show_keys eq 'on') {
      @list = ('KEY', 'DESCRIPTION');
   } else {
      @list = ('DESCRIPTION');
   }

   my $top_row  = $row;

   my ($string, $id, $s);

   foreach $string (@list) {
      $sheet1->write_string($row, $col, $string, $format);
      if ($dir eq 'across') { ++$col; } else { ++$row;}
   }
   
   foreach $id (@report_id_list) {
      $sheet1->write_string($row, $col, $id, $format_bold_right);
      if ($dir eq 'across') { ++$col; } else { ++$row;}
   }
   
   if ($dir eq 'across') { ++$row; } else { ++$col;}

   my ($report_id, $worksheet, $wrow, $wcol, $fmt);
   
   foreach $line (@report_definition) {

      next if ($line =~ m/^\#/);
   
      ($worksheet, $wrow, $wcol, $fmt, $desc) = split /\|/,$line,-1;
   
      if ($dir eq 'across') { $col = 0;} else {$row = $top_row;}
   
      if ($show_keys eq 'on') {
         $sheet1->write_string($row, $col, "$worksheet,$wrow,$wcol", $format);
         if ($dir eq 'across') { ++$col; } else { ++$row;}
      }

      $sheet1->write_string($row, $col, "$desc", $format);
      if ($dir eq 'across') { ++$col; } else { ++$row;}

      foreach $id (@report_id_list) {

         $report_id = $id;
  
         if ($worksheet eq 'SPECIAL') {
            $value = &get_special_value($report_id, $worksheet, $wrow, $wcol);
         } else {
            $value = &get_value($report_id, $worksheet, $wrow, $wcol);
         }
   
         if ($value =~ m/^\-??[\d\.]+$/) {
             if ($fmt eq '$') {
                $sheet1->write_number($row, $col, $value, $money_format);
             } elsif ($fmt eq 'M') {
                $sheet1->write_number($row, $col, $value, $amount_format);
             } elsif ($fmt eq 'C') {
                $sheet1->write_number($row, $col, $value, $comma_format);
             } elsif ($fmt eq 'S') {
                $sheet1->write_string($row, $col, $value);
             } else {
                $sheet1->write_number($row, $col, $value);
             }
         } else {
             if ($fmt eq 'U' && $value =~ m/http:\/\//) {
                $sheet1->write_url($row, $col, $value, 'LINK');
             } else {
                $sheet1->write_string($row, $col, $value);
             }
         }
   
         if ($dir eq 'across') { ++$col } else { ++$row; }
      }
    
      if ($dir eq 'across') { ++$row; } else { ++$col; }
   }
}

#----------------------------------------------------------------------------
#  
#----------------------------------------------------------------------------

sub load_reports
{
   my $view = shift (@_);

   my ($sth, $sql, @sql_args);

   if ($view eq 'all') {

      $sql = qq{
         SELECT RPT_REC_NUM, year(FY_END_DT) as RPT_YEAR, RPT_STUS_CD, FI_NUM, FY_BGN_DT, FY_END_DT
           FROM $report_table
          WHERE PRVDR_NUM = ?
       ORDER BY RPT_REC_NUM ASC
      };

      push @sql_args, @_;  # provider

   } elsif ($view eq 'peryear') {

      $sql = qq{
         SELECT RPT_REC_NUM, year(FY_END_DT) as RPT_YEAR, RPT_STUS_CD, FI_NUM, FY_BGN_DT, FY_END_DT
           FROM $report_table
          WHERE PRVDR_NUM = ?
            AND RPT_YEAR  in (};
      $sql = $sql.pop(@_);
      $sql = $sql.qq{)
       ORDER BY RPT_REC_NUM ASC
      };
			
      push @sql_args, @_;    # provider, year      

   } elsif ($view eq 'recent') {

      $sql = qq{
         SELECT RPT_REC_NUM, year(FY_END_DT) as RPT_YEAR, RPT_STUS_CD, FI_NUM, FY_BGN_DT, FY_END_DT
           FROM $report_table
          WHERE PRVDR_NUM = ?
       ORDER BY RPT_REC_NUM DESC
        LIMIT 1
      };

      push @sql_args, @_;    # provider;
   }

   unless ($sth = $dbh->prepare($sql)) {
       &display_error('ERROR preparing SQL: ', $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sth->execute(@sql_args)) {
      &display_error('ERROR executing SQL: ', $sth->errstr);
      $dbh->disconnect();
      exit(0);
   }
       
   my ($rpt_num, $rpt_year, $rpt_stus_cd, $fi_num, $fy_bgn_dt, $fy_end_dt);

   my $i=0;
   while (($rpt_num, $rpt_year, $rpt_stus_cd, $fi_num, $fy_bgn_dt, $fy_end_dt) = $sth->fetchrow_array) {
      $mem_provider{$rpt_num}   = $provider;
      $mem_stus_cd{$rpt_num}    = $rpt_stus_cd;
      $mem_rpt_year{$rpt_num}   = $rpt_year;
      $mem_fy_bgn_dt{$rpt_num}  = $fy_bgn_dt;
      $mem_fy_end_dt{$rpt_num}  = $fy_end_dt;
      push @report_id_list, $rpt_num;
      $i++;
   }
   
    

}

#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
<BR>
<BR>
};
&innerHeader();

print qq{
    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}

#------------------------------------------------------------------------------

sub get_value_by_address
{
    my ($report_id, $address) = @_;

    my ($worksheet, $line, $column) = split /:/,$address;

    return(&get_value($report_id, $worksheet, $line, $column));
}

#----------------------------------------------------------------------------

sub get_value
{
   my ($report_id, $worksheet, $line, $column) = @_;

   my $yyyy = $mem_rpt_year{$report_id};

   my ($sth, $value);

   my $sql = qq(
    SELECT CR_VALUE         
      FROM $data_table
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
   );

   unless ($sth = $dbh->prepare($sql)) {
       &display_error('Error preparing SQL: ', $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sth->execute($report_id, $worksheet, $line, $column)) {
       &display_error('Error executing SQL: ', $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }
   
   if ($sth->rows) {
      $value = $sth->fetchrow_array;
   }

   $sth->finish();

   return $value;
}

#---------------------------------------------------------------------------

sub get_special_value
{
   my ($report_id, $worksheet, $line, $column) = @_;

   my @status_desc = (
     'void',
     'As Submitted',
     'Settled w/o Audit',
     'Settled with Audit',
     'Reopened',
     'Amended',
   );

   if ($line eq '00100' and $column eq '0100') {
      return $report_id;
   } elsif ($line eq '00100' and $column eq '0200') {
      return $mem_rpt_year{$report_id};
   } elsif ($line eq '00100' and $column eq '0300') {
      return "http://www.spicerware.com/cgi-bin/cr_explore.pl?reportid=$report_id&facility=$facility";
   } elsif ($line eq '00100' and $column eq '0400') {
      return $mem_fi_num{$report_id}
   } elsif ($line eq '00200' and $column eq '0100') {
      return $status_desc[$mem_stus_cd{$report_id}];
   } elsif ($line eq '00300' and $column eq '0100') {
      return $mem_fy_bgn_dt{$report_id};
   } elsif ($line eq '00300' and $column eq '0200') {
      return $mem_fy_end_dt{$report_id};
   } elsif ($line eq '00300' and $column eq '0300') {
      return (&compute_days_in_period($report_id));
   } elsif ($line eq '00400' and $column eq '0100') {   # quick ratio
      return (&compute_quick_ratio($report_id));
   } elsif ($line eq '00500' and $column eq '0100') {   # ebitdar
      return (&compute_ebitdar($report_id));
   } elsif ($line eq '00600' and $column eq '0100') {   # operating margin
      return (&compute_operating_margin($report_id));
   } elsif ($line eq '00700' and $column eq '0100') {   # excess margin
      return (&compute_excess_margin($report_id));
   } elsif ($line eq '00800' and $column eq '0100') {   # days in patient ar
      return (&compute_days_in_patient_ar($report_id));
   } elsif ($line eq '00900' and $column eq '0100') {   # return on assets
      return (&compute_return_on_assets($report_id));
   } elsif ($line eq '01000' and $column eq '0100') {   # return on equity
      return (&compute_return_on_equity($report_id));
   } elsif ($line eq '01100' and $column eq '0100') {   # Personnel expense percent of totoal operating revenue
      return (&compute_personnel_expense_pct_tor($report_id));
   }
}

#----------------------------------------------------------------------------

sub compute_days_in_period
{
   my ($report_id) = @_;

   my ($a_yyyy, $a_mm, $a_dd, $b_yyyy, $b_mm, $b_dd);

   if ($mem_fy_bgn_dt{$report_id} =~ m/(\d\d\d\d)\-(\d\d)\-(\d\d)/) {
      ($a_yyyy, $a_mm, $a_dd) = ($1,$2,$3);
   } else {
      return ('');
   }
   if ($mem_fy_end_dt{$report_id} =~ m/(\d\d\d\d)\-(\d\d)\-(\d\d)/) {
      ($b_yyyy, $b_mm, $b_dd) = ($1,$2,$3);
   } else {
      return ('');
   }

   my $days = &Delta_Days($a_yyyy, $a_mm, $a_dd, $b_yyyy, $b_mm, $b_dd);
   return ($days + 1);
}

#---------------------------------------------------------------------------

sub compute_quick_ratio
{
   my ($report_id) = @_;

   my $total_current_assets         = &get_value_by_address($report_id, 'G000000:01100:0100');
   my $total_current_liabilities    = &get_value_by_address($report_id, 'G000000:03600:0100');
   my $inventory                    = &get_value_by_address($report_id, 'G000000:00700:0100');

   my $quick_ratio = 'NA';

   if ($total_current_liabilities) {  # avoid divide by zero
      $quick_ratio               = ($total_current_assets - $inventory) / $total_current_liabilities;
   }
   return $quick_ratio;
}

#---------------------------------------------------------------------------

sub compute_ebitdar
{
   my ($report_id) = @_;

   my $net_income            = &get_value_by_address($report_id, 'G300000:03100:0100');
   my $interest_expense      = &get_value_by_address($report_id, 'A700003:00500:1100');  
   my $depreciation_expense  = &get_value_by_address($report_id, 'A700003:00500:0900');  
   my $lease_cost            = &get_value_by_address($report_id, 'A700003:00500:1000');  

   return($net_income + $interest_expense + $depreciation_expense + $lease_cost);
}

#---------------------------------------------------------------------------
sub compute_operating_margin
{
   my ($report_id) = @_;

   my $operating_revenue     = &get_value_by_address($report_id, 'G300000:00300:0100');
   my $operating_expense     = &get_value_by_address($report_id, 'G300000:00400:0100');

   if ($operating_revenue) {
      return (($operating_revenue - $operating_expense) / $operating_revenue * 100);
   } else {
      return('NA');
   }
}

#---------------------------------------------------------------------------

sub compute_excess_margin
{
   my ($report_id) = @_;

   my $operating_revenue     = &get_value_by_address($report_id, 'G300000:00300:0100');
   my $non_operating_revenue = &get_value_by_address($report_id, 'G300000:02500:0100');
   my $operating_expense     = &get_value_by_address($report_id, 'G300000:00400:0100');

   if ($operating_revenue + $non_operating_revenue) {
      return (($operating_revenue - $operating_expense + $non_operating_revenue) / ($operating_revenue + $non_operating_revenue) * 100);
   } else {
      return('NA');
   }
}


#----------------------------------------------------------------------------

sub compute_days_in_patient_ar
{
   my ($report_id) = @_;

   my $accounts_receivable          = &get_value_by_address($report_id, 'G000000:00400:0100');
   my $allowance_for_uncollectable  = &get_value_by_address($report_id, 'G000000:00600:0100');
   my $operating_revenue            = &get_value_by_address($report_id, 'G300000:00300:0100');

   if ($operating_revenue) {
      return(($accounts_receivable - $allowance_for_uncollectable) / ($operating_revenue / 365));
   } else {
      return 'N/A';
   }

}

#---------------------------------------------------------------------------

sub compute_return_on_equity
{
   my ($report_id) = @_;

   my $net_income         = &get_value_by_address($report_id, 'G300000:03100:0100');
   my $total_assets       = &get_value_by_address($report_id, 'G000000:02700:0100');
   my $total_liabilities  = &get_value_by_address($report_id, 'G000000:04300:0100');

   if ($total_assets - $total_liabilities) {
      return($net_income / ($total_assets - $total_liabilities) * 100);
   } else {
      return 'N/A';
   }
}
#---------------------------------------------------------------------------

sub compute_return_on_assets
{
   my ($report_id) = @_;

   my $net_income         = &get_value_by_address($report_id, 'G300000:03100:0100');
   my $total_assets       = &get_value_by_address($report_id, 'G000000:02700:0100');

   if ($total_assets) {
      return(($net_income / $total_assets) * 100);
   } else {
      return 'N/A';
   }
}

#----------------------------------------------------------------------------

sub compute_personnel_expense_pct_tor
{
   my ($report_id) = @_;

   my $salary_expense     = &get_value_by_address($report_id, 'A000000:10100:0100');
   my $fringe_benefits    = &get_value_by_address($report_id, 'A000000:00500:0200');
   my $operating_revenue  = &get_value_by_address($report_id, 'G300000:00300:0100');

   my $contract_labor     = &get_value_by_address($report_id, 'S300002:00900:0300');
      $contract_labor    += &get_value_by_address($report_id, 'S300002:00901:0300');
      $contract_labor    += &get_value_by_address($report_id, 'S300002:00902:0300');
      $contract_labor    += &get_value_by_address($report_id, 'S300002:01000:0300');
      $contract_labor    += &get_value_by_address($report_id, 'S300002:01001:0300');
      $contract_labor    += &get_value_by_address($report_id, 'S300002:01100:0300');
      $contract_labor    += &get_value_by_address($report_id, 'S300002:01200:0300');
      $contract_labor    += &get_value_by_address($report_id, 'S300002:01201:0300');


   if ($operating_revenue) {
      return(($salary_expense + $contract_labor + $fringe_benefits) / $operating_revenue * 100);
   } else {
      return 'N/A';
   }
}

#----------------------------------------------------------------------------

sub compute_ftes_per_adjusted_occupied_bed
{
   my ($report_id) = @_;

   my $total_ftes         = &get_value_by_address($report_id, 'S300001:02500:1000');

   my $aaa                = &get_value_by_address($report_id, 'S300001:01200:0600'); #   inpatient days
      $aaa               -= &get_value_by_address($report_id, 'S300001:00400:0600'); # - NF swing days
      $aaa               -= &get_value_by_address($report_id, 'S300001:01100:0600'); # - Nursery Days

   my $bbb                = &get_value_by_address($report_id, 'G200001:02500:0300'); #   total patient Revenues

}

#----------------------------------------------------------------------------


sub display_form
{

   my ($year, $year_options, $selected);

   my $package_list = '';

   opendir(DIR,"./packages");
   my @list = readdir(DIR);
   closedir(DIR);

   my ($fname, $editlink, $string); 

   foreach $fname (sort @list) {
      if ($fname =~ m/\.txt$/) {
         $string = uc $fname;
         $string =~ s/^\d+//;  
         $string =~ s/\.txt//i;
         $editlink = "<A HREF=\"#\" onClick=\"MyWindow=window.open('/cgi-bin/setedit.pl?set=$fname&facility=$facility','$fname','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,coyhistory=no,resizable=yes,width=850,height=700,left=25,top=25'); return false;\">$string</A>";
         $package_list .= "<INPUT NAME=\"datasets\" TYPE=\"checkbox\" VALUE=\"$fname\">$editlink<BR>\n";
      }
   }
	
    
   my $line;

   my $default_list = '';

   if ($imp =~ m/^Y/i) {
      if (open INPUT, "userdata/$remote_user.txt") {
         while ($line = <INPUT>) {
            $default_list .= $line;
         }
      }
      close INPUT;
   }
   
   #jayanth - to select the saved peer groups. 
   
   $sql = "select * from peergroup_main";
   $sth = $dbh->prepare($sql);
   $sth->execute();   
   
   
   print "Content-Type: text/html\n";
   print "Cache-Control: no-cache\n";
   print "Pragma: no-cache\n\n";

   print qq{
<HTML>
<HEAD><TITLE>COST REPORTS</TITLE>
<script language="javascript" src="/JS/admin_panel/spreadsheet_generator/cr_batchx.js" ></script>
    </HEAD>
<BODY>
<center><BR>
};
&headerScript();
print qq{
<CENTER>

<FORM NAME="form1" METHOD="POST" ACTION="/cgi-bin/cr_batchx.pl?facility=$facility">
};
&innerHeader();
print qq{
    <!--content start-->
   
    <table cellpadding="0" cellspacing="0" width="885px">
        <tr>
            <td valign="top">
                <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;
                text-align: left; padding: 10px">
<div class="pageheader">COST REPORT SPREADSHEET GENERATOR</div>
<TABLE bordr=1 ALIGN="CENTER" CELLPADDING="0" CELLSPACING="0" WIDTH="100%" class="gridstyle" style="background-color:white">
<Tr>
	<Td valign="top" style="padding:10px">
	  <table class="gridstyle" width="100%">
	     <tr>
	     	<td><strong>REPORT DIRECTION</strong></td>
	     </tr>
	     <tr>
		<td><INPUT NAME="direction" TYPE="radio" VALUE="across"> ACROSS</td>
	     </tr>
	     <tr>
	     	<td><INPUT NAME="direction" TYPE="radio" VALUE="down" CHECKED> DOWN</td>
	     </tr>
	     <tr>
	     	<td><strong>OPTIONS</strong><br></td>
	     </tr>
	     <tr>
		<td><INPUT NAME="showkeys" TYPE="checkbox"> INCLUDE GRID-CODES</td>
	     </tr>	
	     <tr>		
		<td><strong>SELECT DATASETS</strong></td>
	     </tr>
	     <tr>
		<td><span class="gridHlink">$package_list</span></td>
	     </tr>	    
	  </table>
	</td>
	<td valign="top" style="padding:10px">
		<table class="gridstyle" width="100%">
			<tr>
			  <td colspan=2 >
				 <B>PEER GROUP SELECTION</B><BR><BR>
		        	 Cut and paste from provider numbers into area below use a single column <BR><BR>         
        			<select name=peerGroupSelect onChange="selectPeerGroup(this)">
				};
	
					my $providerid = 0;
	
					my $peername = '--Select Peer Group--';
					print "<option value=$providerid>$peername</option>\n";
					while ($row = $sth->fetchrow_hashref) {	
						$providerid = $$row{PeerGroup_ID};
						$peername = $$row{Name};
						if($providerid == $peerID)
						{
							print "<option value=$providerid selected>$peername</option>\n"; 	
						}
						else
						{
							print "<option value=$providerid>$peername</option>\n"; 	
						}
  					}
  
  				print qq{
  				</select>
		 		<br><br>
	    	 	 </td>
			</tr>
			<tr>
			  <td>        			
         			<TEXTAREA NAME="providers" class="textboxstyle" COLS="50" ROWS="20" style="width:100%; height:200px; font-family:Verdana; font-size:10px;">};
					foreach my $hospitalID (@arrayOfIds){print "$hospitalID\n";}print qq{         
        			</TEXTAREA>         
        		  </td>
        		  <td>
        		  	<div style='border:1px solid gray;width:80%; height:200px;position:static;overflow:auto;'>
        	      		  <table width="100%" cellpadding="0" celspacing="0">        	 		        
        	 		        };
        	 		        foreach my $fYears (@fYears)
					{
						if($fYears==$defaultYear)
						{
							print "<tr><td width=\"10px\"><INPUT TYPE='CHECKBOX' NAME='CHK1' value='$fYears' checked></td><td>$fYears</td></tr>";					
						}
						else
						{
							print "<tr><td width=\"10px\"><INPUT TYPE='CHECKBOX' NAME='CHK1' value='$fYears'></td><td>$fYears</td></tr>";					
						}
         				}         				
         				print qq{
         			  </table>
		               </div>	 
		         </td>
	              </tr>
	              <tr>
		      	          	<td >
		      	         		<span class="button"><INPUT TYPE="button" VALUE="CLEAR" onclick="window.document.form1.providers.value=''"></span>
		      	         	</td>         
		      	         	<td>
		       			  	<span class="button"><INPUT TYPE="button" VALUE="CLEAR" onclick="CallClear(this)"></span>
		      	         	</td>
          	      </tr>
         	  </table>
         </td>
</tr>   
<tr>
	<td colspan=4>
		<Div style="padding-top:2px"><INPUT TYPE="hidden" NAME="action" VALUE="go">
				<INPUT TYPE="hidden" NAME="yearCheck" >
		         <span class="button"><INPUT TYPE="button"  VALUE="CLEAR" onclick="ClearDatasets();"></span>
		         <span class="button"><INPUT TYPE="button" VALUE="GENERATE SPREADSHEET" onClick="callsubmit()"></span>
		 	 <span class="button"><INPUT TYPE="button" value="BACK" onClick="history.go(-1)" /></span>
 		</div>
	</td>
</tr>	
</TABLE>
</td>
<td valign="top">
                <img src="/images/content_right.gif" />
</td>
</tr>
</table>
};

&TDdoc;
print qq{
   <INPUT TYPE="hidden" NAME="facility" VALUE="$facility"> 
</FORM>
</CENTER>
</BODY>
</HTML>
   };
}

#-----------------------------------------------------------------------------

sub peerGroupDetails
{
	my ($peerID) = @_;	
	
	$sql = "select pd.Provider_number as Number, th.HospitalName
as Hosp_Name from peergroup_details as pd 
inner join $master_table as th on th.ProviderNo = pd.Provider_number 
where pd.PeerGroup_Main_ID = $peerID 
Order By pd.FOI desc";
	$sth = $dbh->prepare($sql);
	$sth->execute();   	
   	

   	my @provNumber;
   	my $i=0;
   	
	while ($row = $sth->fetchrow_hashref) {	
		$provNumber[$i] = $$row{Number}." - ".$$row{Hosp_Name};
		$i++;
  	}  	
  	return @provNumber;
}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#--------------------------------------------------------------------------------------
sub sessionExpire
{

print "Content-Type: text/html\n\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

#------------------------------------------------------------------------------


#Database: HCRISDB  Table: HS2006_RPT  Wildcard: %
#+--------------------+---------------+-------------------+------+-----+---------+-------+---------------------------------+---------+
#| Field              | Type          | Collation         | Null | Key | Default | Extra | Privileges                      | Comment |
#+--------------------+---------------+-------------------+------+-----+---------+-------+---------------------------------+---------+
#| RPT_REC_NUM        | int(11)       |                   | NO   |     |         |       | select,insert,update,references |         |
#| PRVDR_CTRL_TYPE_CD | char(2)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| PRVDR_NUM          | char(6)       | latin1_swedish_ci | NO   | MUL |         |       | select,insert,update,references |         |
#| NPI                | decimal(10,0) |                   | YES  |     |         |       | select,insert,update,references |         |
#| RPT_STUS_CD        | char(1)       | latin1_swedish_ci | NO   |     |         |       | select,insert,update,references |         |
#| FY_BGN_DT          | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| FY_END_DT          | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| PROC_DT            | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| INITL_RPT_SW       | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| LAST_RPT_SW        | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| TRNSMTL_NUM        | char(2)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| FI_NUM             | char(5)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| ADR_VNDR_CD        | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| FI_CREAT_DT        | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| UTIL_CD            | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| NPR_DT             | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| SPEC_IND           | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| FI_RCPT_DT         | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#+--------------------+---------------+-------------------+------+-----+---------+-------+---------------------------------+---------+
