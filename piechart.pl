#!/usr/bin/perl

#--------------------------------------------------------------------
# Program: piechart.pl
# used by: bill_summary
#  Author: Steve Spicer
#--------------------------------------------------------------------

use strict;
use CGI;
use GD::Graph::pie;

my $cgi = new CGI;

my $label_list = $cgi->param("labels");
my $value_list = $cgi->param("values");
my $title      = $cgi->param("title");

my @labels = split /,/,$label_list;
my @values = split /,/,$value_list;

my @GraphData;

push (@GraphData,\@labels);
push (@GraphData,\@values);

my $my_graph = new GD::Graph::pie(300,200);

$my_graph->set( 
        title => "$title",
        label => "GRAPH LABEL GOES HERE",
        axislabelclr => 'black',
        pie_height => 36,
);

my $image = ($my_graph->plot(\@GraphData))->gif;

print "Content-Type: image/gif\n\n";
print $image;

exit 0;
