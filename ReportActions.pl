#!/usr/bin/perl

use CGI;
use DBI;
use strict;
my $cgi = new CGI;
do "audit.pl";
use CGI::Session;
do "common_header.pl";
print "Content-Type: text/html\n\n";
&headerScript();
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $facility = $cgi->param("facility");
my $saction = $cgi->param("saction");
my $reportname = $cgi->param("txtRptName");
my $element = $cgi->param("txtElement");
my $worksheet = $cgi->param("txtWorksheet");
my $lineno = $cgi->param("txtLineNo");
my $columnno = $cgi->param("txtColumnNo");
my $isFormula = $cgi->param("txtIsFormula");
my $formula = $cgi->param("txtFormula");
my $sumarry = $cgi->param("txtSummary");
my $rpt_mainid = $cgi->param("rpt_mainid");
my $repdetailsid = $cgi->param("repdetailsid");
my $for_value = "$worksheet"."-"."$lineno"."-"."$columnno";
my ($row,@row,$ex_name,$temp,$row1);
if(!$userID)
{
	&sessionExpire;
}

# Database

my $dbh;
my ($sql, $sth,$sql_f);
my ($sql1, $sth1);
my ($sql2, $sth2);
my ($sql3, $sth3);
my ($sql4, $sth4);
my ($sql5, $sth5);
my ($sqlChkDuplicate, $sthDuplicate,$facility_id ,$s);
# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};

my $msg = "";
my $reportId = $cgi->param("rpt_mainid");
my $strDetailsId = $cgi->param("hdnRptDetailsids");
my $strElmnt = $cgi->param("hdnElement");
my $strworksheet = $cgi->param("hdnWorksheet");
my $strLinno = $cgi->param("hdnLineNo");
my $strClmn = $cgi->param("hdnColumnNo");
my $strIsFrmla = $cgi->param("hdnIsFormula");
my $strFrmla = $cgi->param("hdnFormula");
my $strSummary = $cgi->param("hdnSummary");

my @idArray = split(/~/,$strDetailsId);
my @elemntArray = split(/~/,$strElmnt);
my @worksheetArray = split(/~/,$strworksheet);
my @linnoArray = split(/~/,$strLinno);
my @columnArray = split('~',$strClmn);
my @isFormulaArray = split(/~/,$strIsFrmla);
my @formulaArray = split(/~/,$strFrmla);
my @summaryArray = split(/~/,$strSummary);

my $len = @elemntArray;

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error('ERROR connecting to database', $DBI::errstr);
   exit(1);
}
$sql_f = "select facility_id from tbl_facility_type where facility_name = '$facility'";
$sth = $dbh->prepare($sql_f);
$sth->execute();

while ($row = $sth->fetchrow_hashref) {
	$facility_id = $$row{facility_id};
}

if(trim($saction) eq 'CheckReport'){

	$sql = "select * from standard_reports where upper(reports_name) = upper('$reportname')";
	$sth = $dbh->prepare($sql);
	$sth->execute();
	if($sth->rows()>0){
		$msg ="<script>alert('Report name already exists.');window.parent.document.getElementById('btnSave').disabled = true;window.parent.document.getElementById('btnAddMore').disabled = true;window.location.href='/cgi-bin/ReportActions.pl?saction=CreateNew&facility=$facility';</script>";
	}else{
		$msg ="<script>window.parent.document.getElementById('btnSave').disabled = false;window.parent.document.getElementById('btnAddMore').disabled = false;window.location.href='/cgi-bin/ReportActions.pl?saction=CreateNew&txtRptName=$reportname&facility=$facility';</script>";
	}
}
if(trim($saction) eq 'Create'){

	if(!$reportId){
		$sql = "insert into standard_reports(reports_name,reports_status,CreateDate,CreatedBy,facility_type) values('$reportname','1',CURRENT_DATE,'$userID',$facility_id)";
		$sth = $dbh->prepare($sql);
		$sth->execute();

		$sql1 = "select reports_id from standard_reports where reports_name = '$reportname' and facility_type = $facility_id";
		$sth1 = $dbh->prepare($sql1);
		$sth1->execute();

		&insert("Create","",$reportname,"Standard Reports");

		@row = $sth1->fetchrow_array();
		$reportId = @row[0];
		$msg = "<script>alert('Report created successfully.')";
	}else{
		my $sql_d="select reports_name from standard_reports where reports_id = '$reportId' ";
		$sth = $dbh->prepare($sql_d);
		$sth->execute();

		while ($row = $sth->fetchrow_hashref){
				$ex_name=$$row{reports_name};
			}

		&update("Update","","",$ex_name,"Standard Reports");

		$sql = "update standard_reports set AlterDate = CURRENT_DATE ,ModifiedBy = '$userID' where reports_id = '$reportId'";
		$sth = $dbh->prepare($sql);
		$sth->execute();
		$msg = "<script>alert('Report updated successfully.')";
	}
	my $insertFormula="";
		if($isFormula == 1){
			$insertFormula = $formula;
		}
		else
		{
			$insertFormula = $for_value;
		}

	$sqlChkDuplicate = "select element,metrics_id from standard_metrics where replace(Formulae, ' ', '') = replace('$insertFormula', ' ','') and facility_type = $facility_id";
	$sthDuplicate = $dbh->prepare($sqlChkDuplicate);
	$sthDuplicate->execute();
	if($sthDuplicate->rows()>0)
	{
		while($row = $sthDuplicate->fetchrow_hashref)
		{
			$sql3 = "INSERT INTO standard_reports_metrics(metrics_id,reports_id,Summary_Info) values ($$row{metrics_id}, $reportId,'$sumarry')";
			$sth3 = $dbh->prepare($sql3);
			$sth3->execute();
		}
		$msg =$msg.";window.parent.location.href='/cgi-bin/DataSetConfigure.pl?facility=$facility';</script>";
	}
	else
{
	$sql2 = "insert into standard_metrics(element,isFormulae,Formulae,facility_type) values('$element',$isFormula,'$insertFormula',$facility_id)";
	$sth2 = $dbh->prepare($sql2);
		$sth2->execute();
		$saction = "";
		$reportname = "";
		$msg =$msg.";window.parent.location.href='/cgi-bin/DataSetConfigure.pl?facility=$facility';</script>";

		$sql4 = "select metrics_id from standard_metrics where element = '$element'  and  isFormulae = $isFormula and Formulae = '$insertFormula' and facility_type = $facility_id";
		$sth4 = $dbh->prepare($sql4);
		$sth4->execute();
		my $metrics_id = "";
		if($sth4->rows()>0)
	{
		while ($row = $sth4->fetchrow_hashref){
			$metrics_id=$$row{metrics_id};
			$sql3 = "INSERT INTO standard_reports_metrics(metrics_id,reports_id,Summary_Info) values ($metrics_id, $reportId,'$sumarry')";
			$sth3 = $dbh->prepare($sql3);
			$sth3->execute();
	}
	}
	}
	$reportId = "";
	#$msg =$msg.";window.parent.location.href='/cgi-bin/DataSetConfigure.pl';</script>";
}

#--- Displays elements in selected report for editing.

if(trim($saction) eq 'AddMore'){

	if(!$reportId){
		$sql = "insert into standard_reports(reports_name,reports_status,CreateDate,CreatedBy,facility_type) values('$reportname','1',CURRENT_DATE,'$userID',$facility_id)";
		$sth = $dbh->prepare($sql);
		$sth->execute();

		$sql1 = "select reports_id from standard_reports where reports_name = '$reportname' and facility_type = $facility_id";
		$sth1 = $dbh->prepare($sql1);
		$sth1->execute();

		@row = $sth1->fetchrow_array();
		$reportId = @row[0];

	}

	my $insertFormula="";
	if($isFormula == 1){
		$insertFormula = $formula;
	}
	else
	{
		$insertFormula = $for_value;
	}
	$sqlChkDuplicate = "select element,metrics_id from standard_metrics where replace(Formulae,' ','') = replace('$insertFormula', ' ','')";
	$sthDuplicate = $dbh->prepare($sqlChkDuplicate);
	$sthDuplicate->execute();
	if($sthDuplicate->rows()>0)
	{
		while($row = $sthDuplicate->fetchrow_hashref)
		{
			$sql3 = "INSERT INTO standard_reports_metrics(metrics_id,reports_id,Summary_Info) values ($$row{metrics_id}, $reportId,'$sumarry')";
			$sth3 = $dbh->prepare($sql3);
			$sth3->execute();
		}
		$msg ="<script>window.location.href = '/cgi-bin/AddMoreReports.pl?saction=AddMore&rpt_mainid=$reportId&rept_name=$reportname&facility=$facility'</script>";
	}
	else
	{
	$sql2 = "insert into standard_metrics(element,isFormulae,Formulae,facility_type) values('$element',$isFormula,'$insertFormula',$facility_id)";
	$sth2 = $dbh->prepare($sql2);
	$sth2->execute();
	$msg ="<script>window.location.href = '/cgi-bin/AddMoreReports.pl?saction=AddMore&rpt_mainid=$reportId&rept_name=$reportname&facility=$facility'</script>";
		$sql4 = "select metrics_id from standard_metrics where element = '$element'  and  isFormulae = $isFormula and Formulae = '$insertFormula'";
		$sth4 = $dbh->prepare($sql4);
		$sth4->execute();
		my $metrics_id = "";
		while ($row = $sth4->fetchrow_hashref){
			$metrics_id=$$row{metrics_id};
			$sql3 = "INSERT INTO standard_reports_metrics(metrics_id,reports_id,Summary_Info) values ($metrics_id, $reportId,'$sumarry')";
			$sth3 = $dbh->prepare($sql3);
			$sth3->execute();
			}
}
open(J, ">data.txt");
						print J $sql2;
						close J;
	$msg ="<script>window.location.href = '/cgi-bin/AddMoreReports.pl?saction=AddMore&rpt_mainid=$reportId&rept_name=$reportname&facility=$facility'</script>";
}

#--- Displays elements in selecetd report for editing.

if(trim($saction) eq 'Edit'){
	$sql = "select * from standard_metrics sm inner join standard_reports_metrics rm on rm.metrics_id = sm.metrics_id Where rm.reports_id = $reportId";
	$sth = $dbh->prepare($sql);
	$sth->execute();
}

#--- While editing if user click 'update' this will execute.

if(trim($saction) eq 'EditSave'){

	my $sql_d="select reports_name from standard_reports where reports_id = '$reportId'";
	$sth = $dbh->prepare($sql_d);
	$sth->execute();

	while ($row = $sth->fetchrow_hashref){
			$ex_name=$$row{reports_name};
		}

	&update("Update","","",$ex_name,"Standard Reports");

	$sql = "update standard_reports set reports_name = '$reportname', AlterDate = CURRENT_DATE ,ModifiedBy = '$userID' where reports_id = '$reportId'";
	$sth = $dbh->prepare($sql);
	$sth->execute();

	for(my $i = 0;$i<$len; $i++) {
		if(@isFormulaArray[$i] == 0){
			@formulaArray[$i] = @worksheetArray[$i]."-".@linnoArray[$i]."-".@columnArray[$i];
		}
		$sql1 = "update standard_metrics set element = '@elemntArray[$i]',isFormulae = '@isFormulaArray[$i]',Formulae = '@formulaArray[$i]' where metrics_id = '@idArray[$i]'";
		$sth1 = $dbh->prepare($sql1);
		$sth1->execute();

		$sql2 = "update standard_reports_metrics set Summary_Info = '@summaryArray[$i]'  where metrics_id = '@idArray[$i]'";
		$sth2 = $dbh->prepare($sql2);
		$sth2->execute();
	}
	$msg ="<script>alert('Report updated successfully.');window.parent.location.href='/cgi-bin/DataSetConfigure.pl?facility=$facility';</script>";
}

#--- Deletes selected report

if(trim($saction) eq 'delete'){

	my $sql_d="select reports_name from standard_reports where reports_id = '$reportId'";
	$sth = $dbh->prepare($sql_d);
	$sth->execute();

	while ($row = $sth->fetchrow_hashref){
			$ex_name=$$row{reports_name};
		}


	&delete("Delete","",$ex_name,"Standard report");

	$sql = "delete from standard_reports where reports_id = '$reportId'";
	$sth = $dbh->prepare($sql);
	$sth->execute();

	# $sql1 = "DELETE FROM standard_metrics WHERE metrics_id
	# IN (SELECT metrics_id FROM standard_reports_metrics WHERE reports_id = '$reportId')";
	# $sth1 = $dbh->prepare($sql1);
	# $sth1->execute();


	$sql2 = "delete from standard_reports_metrics where reports_id = '$reportId'";
	$sth2 = $dbh->prepare($sql2);
	$sth2->execute();
	$msg ="<script>alert('Report deleted successfully.');window.parent.location.href='/cgi-bin/DataSetConfigure.pl?facility=$facility';</script>";
}

#--- Deletes elements from the selected report.

if(trim($saction) eq 'delDetails'){
	$sql = "delete from standard_reports_metrics where reports_id = '$reportId' and metrics_id = '$repdetailsid'";
	$sth = $dbh->prepare($sql);
	$sth->execute();

	$msg ="<script>alert('Element deleted successfully.');window.location.href='/cgi-bin/ReportDetails.pl?reload=false&mainID=$reportId&dtName=$reportname&reload=true&facility=$facility';</script>";
}


print qq{
<HTML>
<HEAD><TITLE>Standard Reports</TITLE>
 <script type="text/javascript" src="/JS/AjaxScripts/prototype.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/effects.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/controls.js"></script>
 <script language="javascript" src="/JS/admin_panel/standard_report/ReportActions.js" ></script>
 <link href="/css/admin_panel/Exceptions/ExceptionActions.css" rel="stylesheet" type="text/css" />	
</HEAD>
<BODY onload="setCreate()">
<FORM NAME = "frmReportActions" Action="/cgi-bin/ReportActions.pl?facility=$facility" method="post">
<!--<div id='bodyDiv' style="position:absolute;z-index:100;display:none;background-image:url(/images/images/background-trans.png);height:100%;width:100%;"><div align='center'  id="displaybox" style="display: none;"></div></div>-->

<TABLE align="top" border=1 class="gridstyle" CELLPADDING="0" CELLSPACING="0" width="100%">
};
my ($rpt_summary,$rpt_details_id,$rpt_details_element,$rpt_details_worksheet,$rpt_details_line_no,$rpt_details_column_no,$rpt_details_isFormulae,$rpt_details_Formulae);
	if(trim($saction) eq 'Edit'){
		my $i = 0;
		my $isFormula = undef;
		while ($row1 = $sth->fetchrow_hashref) {
			$rpt_details_id = $$row1{metrics_id};
			$rpt_details_element = $$row1{element};
			if($$row1{isFormulae} == 0)
			{
				$temp = ($rpt_details_worksheet,$rpt_details_line_no,$rpt_details_column_no) = split(/-/, $$row1{Formulae});
			}
			$rpt_details_isFormulae = $$row1{isFormulae};
			$rpt_details_Formulae = $$row1{Formulae};
			if($rpt_details_isFormulae eq '1'){
					$isFormula = "True";
					$rpt_details_worksheet = "";
					$rpt_details_line_no = "" ;
					$rpt_details_column_no = "";
				}else{
					$isFormula = "False";
					$rpt_details_Formulae="";
					$temp;
				}
			print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Element</td><td style=\"background-color:#edf2fb\"><input type=\"text\"  id =\"element$i\" name=\"txtElement\" value=\"$rpt_details_element\" size=\"60\" maxLength='50'><input type=\"hidden\" name=\"txtReportdetailsId\" value=\"$rpt_details_id\"></td></tr>\n";
			print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Worksheet</td><td style=\"background-color:#edf2fb\"><input type=\"text\" name=\"txtWorksheet\" value=\"$rpt_details_worksheet\" maxLength=\"7\"></td></tr>\n";
			print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Line No.</td><td style=\"background-color:#edf2fb\"><input type=\"text\" name=\"txtLineNo\" value=\"$rpt_details_line_no\"  maxLength=\"5\"></td></tr>\n";
			print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Column No.</td><td style=\"background-color:#edf2fb\"><input type=\"text\" name=\"txtColumnNo\" value=\"$rpt_details_column_no\" maxLength=\"4\"></td></tr>\n";

			if($rpt_details_isFormulae == "1"){
				print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Add Formulae</td><td style=\"background-color:#edf2fb\"><input type=\"radio\" name=\"optnIsFormula$i\" onClick=\"set(0,$i,this)\" value=\"0\" > No <input type=\"radio\" name=\"optnIsFormula$i\" onClick=\"set(1,$i,this)\" value=\"1\" checked=true> Yes <input type=\"hidden\" name=\"txtIsFormula\" value=\"$rpt_details_isFormulae\"></td></tr>\n";
			}else{
				print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Add Formulae</td><td style=\"background-color:#edf2fb\"><input type=\"radio\" name=\"optnIsFormula$i\" onClick=\"set(0,$i,this)\" value=\"0\" checked=true> No <input type=\"radio\" name=\"optnIsFormula$i\" onClick=\"set(1,$i,this)\" value=\"1\"> Yes <input type=\"hidden\" name=\"txtIsFormula\" value=\"$rpt_details_isFormulae\"></td></tr>\n";
			}
			if($rpt_details_isFormulae eq "1"){
				print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Formulae</td><td nowrap style=\"background-color:#edf2fb\"><input type=\"text\" style=\"height:23px;\" id=\"Formula$i\" name=\"txtFormula\" value=\"$rpt_details_Formulae\"  size=\"80\" maxLength='1000'><span class=\"button\" style=\"valign:top;\"><INPUT TYPE=\"button\" value=\"Formula Builder\" onClick=\"LoadFormulas(1,'Formula$i','element$i')\"/></span></td></tr>\n";
			}else{
				print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Formulae</td><td nowrap style=\"background-color:#edf2fb\"><input type=\"text\" style=\"height:23px;\" id=\"Formula$i\" name=\"txtFormula\" value=\"$rpt_details_Formulae\"  disabled size=\"80\" maxLength='1000'><span class=\"button\" style=\"align:top;\"><INPUT TYPE=\"button\" value=\"Formula Builder\" onClick=\"LoadFormulas(1,'Formula$i','element$i')\"/></span></td></tr>\n";
			}

			print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Report Total</td><td style=\"background-color:#edf2fb\"><select name=\"txtSummary\" >";

			if($rpt_summary eq 'Avg'){
				print"<option value=\"Avg\" selected>Average</option>";
			}else{
				print"<option value=\"Avg\">Average</option>";
			}

			if($rpt_summary eq 'Sum'){
				print"<option value=\"Sum\" selected>Sum</option>";
			}else{
				print"<option value=\"Sum\">Sum</option>";
			}
			if($rpt_summary eq 'NA'){
				print "<option value=\"NA\" selected>No Summary</option>";
			}else{
				if(!($rpt_summary)){
					print "<option value=\"NA\" selected>No Summary</option>";
				}else{
					print "<option value=\"NA\">No Summary</option>";
				}
			}

			print "</select></td></tr>\n";
			print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color::#FFFFFF\" colspan=\"2\">&nbsp;<br></td></tr>\n";
		   $i++;
		}
		$len = $i;
	}else{
		if(trim($saction) eq 'AddMore' || trim($saction) eq 'EditSave' || trim($saction) eq 'delete' || trim($saction) eq 'delDetails' || trim($saction) eq 'CheckReport'){
			print "$msg";
		}else{
			print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Element</td><td style=\"background-color:#edf2fb\"><input type=\"text\" id=\"element0\" name=\"txtElement\" size=\"60\" maxLength=\"50\"></td></tr>\n";
			print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Worksheet</td><td style=\"background-color:#edf2fb\"><input type=\"text\" name=\"txtWorksheet\"  maxLength=\"7\"></td></tr>\n";
			print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Line No.</td><td style=\"background-color:#edf2fb\"><input type=\"text\" name=\"txtLineNo\"  maxLength=\"5\"></td></tr>\n";
			print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Column No.</td><td style=\"background-color:#edf2fb\"><input type=\"text\" name=\"txtColumnNo\" maxLength=\"4\"></td></tr>\n";
			print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Add Formulae</td><td style=\"background-color:#edf2fb\"><input type=\"radio\" name=\"txtIsFormula\" onclick=\"set(0,0,this)\" value=\"0\" checked> No <input type=\"radio\" name=\"txtIsFormula\" onclick=\"set(1,0,this)\" value=\"1\"> Yes</td></tr>\n";
			print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Formulae</td><td valign=\"top\" nowrap style=\"background-color:#edf2fb\"><table><tr><td><input type=\"text\" style=\"height:23px;\" id=\"Formula0\" name=\"txtFormula\"  disabled size=\"80\" maxLength='1000'></td><td valign=\"top\"><span class=\"button\"><INPUT TYPE=\"button\" value=\"Formula Builder\" name=\"btnLoad\" id=\"btnLoad0\" onClick=\"LoadFormulas(1,'Formula0','element0')\" /></span></td></tr></table></td></tr>\n";
			print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Report Total</td><td style=\"background-color:#edf2fb\"><select name=\"txtSummary\" ><option value=\"Avg\">Average</option><option value=\"Sum\">Sum</option><option value=\"NA\" selected>No Summary</option></select></td></tr>\n";
			print "<tr><td></td><td>$msg</td></tr>\n";
		}
	}

print qq{
</TABLE>
<input type="hidden" name="saction" value="$saction">
<input type="hidden" name="txtRptName" value="$reportname">
<input type="hidden" name="rpt_mainid" value="$rpt_mainid">
<input type="hidden" name="facility" value="$facility">
<!-- Below hidden text boxes are used for edit functionality-->
<input type="hidden" name="hdnRptDetailsids" value="">
<input type="hidden" name="hdnElement" value="">
<input type="hidden" name="hdnLineNo" value="">
<input type="hidden" name="hdnWorksheet" value="">
<input type="hidden" name="hdnColumnNo" value="">
<input type="hidden" name="hdnIsFormula" value="">
<input type="hidden" name="hdnFormula" value="">
<input type="hidden" name="hdnSummary" value="">
<input type="hidden" name="hdnFormulaTxtId" value="">
<input type="hidden" name="hdnForm" value="$rpt_details_Formulae">

</FORM>
</BODY>
</HTML>
};
#--------------------------------------------------------------------------------------

sub sessionExpire
{

print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}
#-----------------------------------------------------------------------------


sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}

print qq{
<HTML>
<HEAD><TITLE>Standard Reports-ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
<BR>
<BR>
<FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}

#----------------------------------------------------------------------------

#Perl trim function to remove whitespace from the start and end of the string

sub trim($)
{
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}
