#!/usr/bin/perl

use strict;
use CGI;
use DBI;
do "common_header.pl";
print "Content-Type: text/html\n\n";
&headerScript();
do "pagefooter.pl";
use CGI::Session;
do "audit.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $userID = $session->param("userID");
my $facility = $cgi->param("facility");
if(!$userID)
{
	&sessionExpire;
}

my $heading = uc($facility) . " COST REPORT";

print qq{
<HTML>
<BODY>
<BR>
<CENTER>
};
&innerHeader();
print qq{
  <table cellpadding="0" cellspacing="0" width="100%" align="center"  width="850px">
                    <tr>
                        <td valign="top" align="center">
                            <form method="POST" action="/cgi-bin/choose1.pl?facility=$facility">

								<table bgColor=\"#B4CDCD\"  align=\"centre\" width=\"850px\" >
								<tr>
									<td>
										<font size="2" color="#2e7738"><b>$heading</b><br>
										</font>
									</td>
								</tr>
								</table>
 <table border="0" cellspacing="0" cellpadding="0" align ="center" width="850px">
   <tr>
			    <td valign="top">
                <img src="/images/content_left.gif" />

				</td>
            <td valign="top" style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; ">

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Cost Report</title>
    <link href="/css/tabs.css" rel="stylesheet" type="text/css" />
	<link href="/css/admin_panel/user_management/userManagement.css" rel="stylesheet" type="text/css" />
	<table border="0" cellspacing="0" cellpadding="0" align ="center" width="850px">
                                        <td style="padding-top: 20px" align="center">
                                            <strong>Provider Number:</strong></td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <input type="text" name="provider"  maxlength="6" title="ENTER 6 DIGIT PROVIDER NUMBER"
                                                style="width: 170px">
                                        </td>
                                    </tr>
									<tr><td >&nbsp;</td></tr>
                                    <tr>
                                        <td align="center">
                                            <input type="hidden" name="action" value="go">
											<input type="hidden" name="facility" value="$facility">
                                            <span class="button">
                                                <input type="submit" value=" LIST REPORTS " style="width: 167px"></span>
												</td>
                                        </td>
                            </form>
							</td>
									</tr>
                                </table>
							</tr>
							</table>
							</table>
</CENTER>
</BODY>
</HTML>
};
&TDdoc();
#--------------------------------------------------------------------------------------
sub sessionExpire
{

print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};
print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";
print qq{
</body>
</html>
};
}
