#!/usr/bin/perl

#--------------------------------------------------------------------
# Program: derek4.pl
#  Author: Steve Spicer
# Written: Thu Oct 18 09:07:40 CDT 2007
# Revised: 
#   Notes:
# Depends: DBI.pm, CGI.pm
#--------------------------------------------------------------------

use strict;
use DBI;
use CGI;
use CGI::Session;
do "common_header.pl";
print "Content-Type: text/html\n\n";
&headerScript();
do "pagefooter.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $dataYear=$cgi->param("year");


if(!$userID)
{
	&sessionExpire;
}


my $county_cd  = $cgi->param('county_cd');
my $state_cd   = $cgi->param('state_cd');
my $number=$cgi->param('provider');
my $start_time = time;

my %ini_value;

unless (&read_inifile("milo.ini")) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{"DB_TYPE"};
my $dblogin    = $ini_value{"DB_LOGIN"};
my $dbpasswd   = $ini_value{"DB_PASSWORD"};
my $dbname     = $ini_value{"DB_NAME"};
my $dbserver   = $ini_value{"DB_SERVER"};

my ($sql, $dbh, $sth,$sql_f,$facility_id,$master_table,$report_table,$data_table,$row);
my $facility = $cgi->param("facility");


unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}
$sql_f = "select facility_id ,master_table,report_table,data_table from tbl_facility_type where facility_name = '$facility'";
$sth = $dbh->prepare($sql_f);
$sth->execute();

while ($row = $sth->fetchrow_hashref) {
	$facility_id = $$row{facility_id};
	$master_table = $$row{master_table};
	$report_table = $$row{report_table};
	$data_table = $$row{data_table};
}
$sql = qq{
   SELECT SSA_COUNTY, SSA_STATE
     FROM SSA_COUNTYCODES
    WHERE SSA_STATE_CODE = ?
      AND SSA_CNTY_CD = ?
};

unless ($sth = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL query:",  $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute($state_cd, $county_cd)) {
   &display_error("ERROR executing SQL query:",  $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

my ($title_county, $title_state) = $sth->fetchrow_array;

$sth->finish();

#----------------------------------------------------------------------------

#      AND DRG_CD   = 462

$sql = qq{
  SELECT ld.PROVIDER as PROVIDER, th.Name as HOSP_NAME,th.City as CITY,th.State as STATE, COUNT(*), SUM(PMT_AMT) AS AMT
     FROM LDSM_$dataYear as ld
LEFT JOIN $master_table as th ON ld.PROVIDER = th.providerno
    WHERE STATE_CD = ?
      AND CNTY_CD  = ?
      AND SGMT_NUM = 1
 GROUP BY PROVIDER
 ORDER BY AMT DESC 
};

unless ($sth = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute($state_cd, $county_cd)) {
   &display_error("ERROR executing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

my $now = localtime();

my $rows = $sth->rows;


print qq{
<HTML>
<HEAD><TITLE>DRG ANALYSIS BY PROVIDER</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();

print qq{
<!--content start-->

          <table cellpadding="0" cellspacing="0" width="885px">
            <tr>
                <td valign="top">
                    <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px">
<Table cellpadding="0" cellspacing="0" width="100%">
	<Tr>
		<Td style="padding-bottom:10px">
				<tr><td><span class="pageheader">MEDICARE CLAIMS SUMMARY</span>&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp&nbsp &nbsp<b> Year: </b><font color='#EE0000'>$dataYear</font></td></tr>
				<tr><td><B>FOR PATIENTS RESIDING IN </B></td></tr>
					<tr><td><span class="tahoma12">$title_county COUNTY, $title_state</span></td></tr>

		</td>
		
	</tr>
	<tr align="left">
	<td>
	</br>
	<script>
	function submitYear(value)
	{
		var opt=document.yearSel.year.options[document.yearSel.year.selectedIndex].value;
		window.parent.location.href="/cgi-bin/derek3.pl?provider=$number&facility=$facility&year="+opt+"";
		document.selection.submit();
	}
	</script>
	<form name="yearSel" >
	PATIENT SOURCE : <select name="year" onChange="submitYear(this.value)">
	<option value=''> Select Year </option>
	<option value=2006> 2006</option>
	<option value=2007> 2007</option>
	<option value=2008> 2008</option>
	<option value=2009> 2009</option>
</select>
</form>
</td>
<td align="right" valign="bottom" padding-bottom:10px" class="tahoma12">
$now
</td>
</tr>
</table>
<TABLE CELLPADDING="4" class="gridstyle" WIDTH="100%">
  <TR class="gridheader">
    <TH>PROVIDER<BR>NUMBER</TH>
    <TH>HOSPITAL NAME</TH>
    <TH>CITY, STATE</TH>
    <TH ALIGN="right">CLAIM<BR>COUNT</TH>
    <TH ALIGN="right">TOTAL<BR>AMOUNT PAID</TH>
    <TH ALIGN="right">PERCENT<BR>OF TOTAL</TH>
  </TR>
};

my $total_claims = 0;
my $total_amount = 0;

my $count = 0;

my @buffer;

my ($class, $i, $edit_amount, $edit_pct);
my ($provider, $name, $city, $state);
my ($drg, $desc, $pmt_amt, $claim_count, $amtlink);

while (($provider, $name, $city, $state, $claim_count, $pmt_amt) = $sth->fetchrow_array) {

   $total_claims += $claim_count;
   $total_amount += $pmt_amt;

   push @buffer, [$provider, $name, $city, $state, $claim_count, $pmt_amt];
}

my $link;

my $limit = @buffer;

for ($i=0;$i<$limit;++$i) {

   ($provider, $name, $city, $state, $claim_count, $pmt_amt) = @{$buffer[$i]};

   $edit_amount = &comify($pmt_amt);

   $edit_pct = sprintf ("%.02f", $pmt_amt / $total_amount * 100);

   $class = (++$count % 2) ? "gridrow" : "gridalternate";

   $link = "<A HREF=\"/cgi-bin/drg_mix.pl?provider=$provider&year=$dataYear&facility=$facility\" TITLE=\"DRG BREAKDOWN\">$provider</A>";

   $amtlink = "<A HREF=\"/cgi-bin/drg_mix.pl?provider=$provider&state_cd=$state_cd&county_cd=$county_cd&year=$dataYear&facility=$facility\" TITLE=\"BREAKDOWN OF THIS VALUE\">$edit_amount</A>";

   print "   <TR CLASS=\"$class\">\n";
   print "      <TD ALIGN=\"center\">$link</TD>\n";
   print "      <TD ALIGN=\"left\">$name</TD>\n";
   print "      <TD ALIGN=\"left\">$city, $state</TD>\n";
   print "      <TD ALIGN=\"right\">$claim_count</TD>\n";
   print "      <TD ALIGN=\"right\">$amtlink</TD>\n";
   print "      <TD ALIGN=\"right\">$edit_pct &#37;</TD>\n";
   print "   </TR>\n";
   
}

my $edit_count  = &comify($total_claims);
   $edit_amount = &comify(sprintf("%.02f",$total_amount));

$class = (++$count % 2) ? "odd" : "even";

print "   <TR CLASS=\"$class\">\n";
print "      <TD ALIGN=\"right\" COLSPAN=\"3\"><B>TOTAL:</B></TD>\n";
print "      <TD ALIGN=\"right\"><B>$edit_count</B></TD>\n";
print "      <TD ALIGN=\"right\"><B>$edit_amount</B></TD>\n";
print "      <TD ALIGN=\"left\">&nbsp;</TD>\n";
print "   </TR>\n";

my $elapsed_time = time - $start_time;

print qq{
</TABLE>
$count ROWS IN $elapsed_time SECONDS
</td>
            <td valign="top">
                <img src="/images/content_right.gif" /></td>
        </tr>
    </table>
    <!--content end-->
};
&TDdoc;
print qq{
</CENTER>
</BODY>
</HTML>
};

$sth->finish;
$dbh->disconnect;
   
exit(0);


#------------------------------------------------------------------------------

sub display_error
{
   my $s;


   print qq{
<HTML>
<HEAD><TITLE>ERROR NOTIFICATION</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>

<FONT SIZE="5" COLOR="RED">
<B>ERRORS NOTIFICATION</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" BORDER="1" CELLPADDING="20" WIDTH="600">
<TR><TD>
<UL>
};
   foreach $s (@_) {
      print "$s<BR>";
   }
   print qq{
</TD></TR>
</TABLE>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</FONT>
</CENTER>
</BODY>
</HTML>
};
}

#----------------------------------------------------------------------------

sub comify
{
   local($_) = shift;
   1 while s/^(-?\d+)(\d{3})/$1,$2/;
   return($_);
}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}


#--------------------------------------------------------------------------------------
sub sessionExpire
{

print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

#------------------------------------------------------------------------------

