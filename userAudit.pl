#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: userModify.pl
#  author: Jagadish.M
#----------------------------------------------------------------------------

use strict;
use CGI;
use DBI;
do "common_header.pl";
print "Content-Type: text/html\n\n";
# use Session;
my $cgi = new CGI;
do "pagefooter.pl";
use CGI::Session;
do "audit.pl";
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $saction = $cgi->param('saction');
my $dateFrom = $cgi->param('txtDateFrom');
my $dateTo = $cgi->param('txtDateTo');

my $dbh;
my ($sql, $sth, $key);

# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my $defaultYear= $ini_value{'DEFAULT_YEAR'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

print qq{
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<title>PROJECT MILO User Management</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="/css/register/main.css" rel="stylesheet" type="text/css">
<link href="/css/register/qm.css" rel="stylesheet" type="text/css">
<link href="/css/tabs.css" rel="stylesheet" type="text/css" />   
<link href="/css/admin_panel/user_management/userManagement.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/css/calendar.css">
<script language="JavaScript" src="/JS/calendar_us.js"></script>

</head>
<body leftmargin="0" topmargin="0" marginheight="20" marginwidth="0">
<CENTER>
};
&innerHeader();
print qq{

    <!--content start-->
    
    <table cellpadding="0" cellspacing="0" width="885px">
        <tr>            
            <td valign="top" >
  
	  	
		<ol id="toc">
		    <li><a href="/cgi-bin/userView.pl"><span>View</span></a></li>
		    <li><a href="/cgi-bin/userCreate.pl"><span>Create User</span></a></li>
		    <li><a href="/cgi-bin/userModify.pl"><span>Modify User</span></a></li>	
		    <li class="current"><a href="/cgi-bin/userAudit.pl"><span>Audit Log</span></a></li>
		</ol>
		<div class="content" style="border:0px;padding:0em;">
		<table align="center" class="ColrBk01" width="100%" border="0" cellpadding="4" cellspacing="0">
		  <tbody><tr>
			<td class="Fnt11Pix" background="/images/register/grad_01_hover.gif"><b class="Colr03">Audit log details</b></td>
			<td class="Fnt11Pix" background="/images/register/grad_01_hover.gif" align="right"><b class="Colr03"></b></td>
		  </tr>
		</tbody></table>
		<table class="ColrBk02" width="100%" border="0" cellpadding="2" cellspacing="0">
		  <tbody><tr>
			<td><form action="/cgi-bin/userAudit.pl" method="post" name="frmSingerSLRegistry" id="frmSingerSLRegistry">
				<table width="100%"  bgcolor="#ffffff" border="0" cellpadding="4" cellspacing="0">
				  <tbody>
				  <tr>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Date From :</b></font>
					<input name="txtDateFrom" class="TextBox01" id="txtDateFrom" size="30" maxlength="100" type="text" >
					<script language="JavaScript">
					new tcal ({
						// form name
						'formname': 'frmSingerSLRegistry',
						// input name
						'controlname': 'txtDateFrom'
					});										
					</script>
					</td>
					<td>
					<font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Date To :</b></font>
					<input name="txtDateTo" class="TextBox01" id="txtDateTo" size="30" maxlength="100" type="text" > 
					<script language="JavaScript">
					new tcal ({
						// form name
						'formname': 'frmSingerSLRegistry',
						// input name
						'controlname': 'txtDateTo'
					});										
					</script>
					
					</td>										
					<td><input type="hidden" name="saction" value="search"><input name="cmdNext" class="TextBox01" id="cmdNext" value="Search" type="submit"> <input name="cmdReset" class="TextBox01" id="cmdReset" value="Clear" type="reset"></td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>				  
				</tbody></table>
				
				};								  
				  if($saction eq "search")
				  {					
					my ($qry, $stmt, $record);
					
					my @fromValues = split('/', $dateFrom);
					my @toValues = split('/', $dateTo);
					$dateFrom = $fromValues[2]."-".$fromValues[0]."-".$fromValues[1];
					$dateTo = $toValues[2]."-".$toValues[0]."-".$toValues[1];
					
					$qry = "select u.UserName as UserName, concat(ud.Last_Name,',',ud.First_Name) as fullName, a.user_id as user_id, a.log_date as log_date, a.action as action, a.module as module, a.old_value as old_value, a.new_value as new_value, a.description as description, user_ip as user_ip from audit as a inner join users as u on u.UserID = a.user_id inner join userdetails ud on ud.UsersID = u.UserID where date(log_date) between ? and ? order by log_date desc";
					$stmt = $dbh->prepare($qry);
					$stmt->execute($dateFrom,$dateTo); 
					
					&view("View","Audit log search","User Management");
					
					print "<table width=\"100%\" border=\"0\" > \n";
					
					print "<tr class=\"gridheader\"> \n";
						print "<td> \n";
						print "Date";
						print "</td> \n";

						print "<td> \n";
						print "User Name";
						print "</td> \n";
						
						print "<td> \n";
						print "User IP";
						print "</td> \n";
						
						print "<td> \n";
						print "Action";
						print "</td> \n";

						print "<td> \n";
						print "Module";
						print "</td> \n";

						print "<td> \n";
						print "Old Value";
						print "</td> \n";

						print "<td> \n";
						print "New Value";
						print "</td> \n";

						print "<td> \n";
						print "Description";
						print "</td> \n";							
					print "</tr> \n";
					
					my $count = 0;
					my $class;
					
					while ($record = $stmt->fetchrow_hashref) {	
						$class = (++$count % 2) ? 'gridrow' : 'gridalternate';
						print "<tr class=\"$class\"> \n";
							print "<td> \n";
							print $$record{log_date};
							print "</td> \n";
							
							print "<td> \n";
							print $$record{fullName};
							print "</td> \n";
							
							print "<td> \n";
							print $$record{user_ip};
							print "</td> \n";
							
							print "<td> \n";
							print $$record{action};
							print "</td> \n";
							
							print "<td> \n";
							print $$record{module};
							print "</td> \n";
							
							print "<td> \n";
							print $$record{old_value};
							print "</td> \n";
							
							print "<td> \n";
							print $$record{new_value};
							print "</td> \n";
							
							print "<td> \n";
							print $$record{description};
							print "</td> \n";							
						print "</tr> \n";
					
					
					
					}
					print "</table> \n";
					
					
				  }
								  
				print qq{
				
			  </form>
	             </td>
		  </tr>
		</tbody>
	      </table>
	      </div>
          </td>
       </tr>
  </tbody>
 </table>
 
 </td>
  </tr>
  </table>
  <!--content end-->
  };
  	  &TDdoc;
  
  print qq{
  </CENTER>
  </body>  
  </html>
};

#--------------------------------------------------------------------------------------
sub sessionExpire
{

print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};
&headerScript();
print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach my $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}

#------------------------------------------------------------------------------

