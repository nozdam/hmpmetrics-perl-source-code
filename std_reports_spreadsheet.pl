#!/usr/bin/perl

use CGI;
use DBI;
use Spreadsheet::WriteExcel;
use Date::Calc qw(Delta_Days);
do "common_header.pl";
my $cgi = new CGI;
do "audit.pl";
use CGI::Session;

my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");

if(!$userID)
{
	&sessionExpire;
}


my $spreadHead = $cgi->param('spreadHead');
my $spreadData = $cgi->param('spreadData');


# Database 

my $dbh;
my ($sql, $sth);


# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my $defaultYear= $ini_value{'DEFAULT_YEAR'};




#------------------------------------------------------------------------------------------
# Spreadsheet Generaator

my $workbook = Spreadsheet::WriteExcel->new('-');
#  $workbook->set_tempdir('/tmp');

my $sheet1 = $workbook->addworksheet('main');

my $amount_format = $workbook->addformat();
   $amount_format->set_num_format(0x2b);           #  (#,##0.00_); (#,##0.00)

my $money_format = $workbook->addformat();
   $money_format->set_num_format(0x2c);            #  (#,##0.00_); (#,##0.00)

my $amount_bold_format = $workbook->addformat();
   $amount_bold_format->set_num_format(0x2c);      #  (#,##0.00_); (#,##0.00)
   $amount_bold_format->set_bold();

my $date_format = $workbook->addformat();
   $date_format->set_num_format(0x0e);             #  m/d/yy
   $date_format->set_align("center");

my $number_format = $workbook->addformat();
   $number_format->set_num_format(0x01);           #  0 integer

my $comma_format = $workbook->addformat();
   $comma_format->set_num_format(0x26);            #  (#,###) red

my $format_bold = $workbook->addformat();
   $format_bold->set_bold();

my $big_format_bold = $workbook->addformat();      
   $big_format_bold -> set_bold();
   $big_format_bold -> set_size(14);

my $format_bold_rotated = $workbook->addformat();
   $format_bold_rotated->set_bold();
   $format_bold_rotated->set_rotation(80);

my $format_bold_right  = $workbook->addformat();
   $format_bold_right->set_bold();
   $format_bold_right->set_align("right");


my $col = 0;
my $row = 0;

my $now = localtime();

$sheet1->write_string($row,   0, 'Healthcare Management Partners', $big_format_bold);
$sheet1->write_string(++$row, 0, 'Medicare Cost Report Data extract', $big_format_bold);
$sheet1->write_string(++$row, 0, "GENERATED: $now", $format_bold);


my $elapsed_time = time - $start_time;

# $sheet1->write_number($row, 0, $elapsed_time, $number_format);

++$row;
++$row;
$col = 0;

foreach $s (split /\~/,$spreadHead)
{
	if(!($s eq "NoVal"))

	{
		$sheet1->write_string($row, $col, $s, $format_bold);
		$col++;	
	}
}



foreach $data (split /\:/,$spreadData)
{	
	$col = 0;
	++$row;
	foreach $s (split /\~/,$data)
	{
		if(!($s eq "NoVal"))
		{
			$sheet1->write_string($row, $col, $s);
			$col++;		
		}
	}	
}

&insert("Create","Spreadsheet generated","","Standard Report");

print "Content-type: application/vnd.ms-excel\n";
print "Content-Disposition: attachment; filename=cost_report_batch.xls\n";
print "Cache-Control: no-cache\n";
print "Pragma: no-cache\n\n";

$workbook->close();


#--------------------------------------------------------------------------------------

sub sessionExpire
{

print "Content-Type: text/html\n\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#------------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}
print "Content-Type: text/html\n\n";
&headerScript();
print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();

print qq{
<BR>
<BR>
    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}
