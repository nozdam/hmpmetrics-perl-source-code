#!/usr/bin/perl

#--------------------------------------------------------------------
# Program: derek3.pl
#  Author: Steve Spicer
# Written: Thu Oct 18 09:07:40 CDT 2007
# Revised: Sun Jun  7 17:34:26 CDT 2009
#   Notes:
# Depends: DBI.pm, CGI.pm
#--------------------------------------------------------------------

use strict;
use DBI;
use CGI;
use CGI::Session;
do "pagefooter.pl";
do "common_header.pl";
print "Content-Type: text/html\n\n";
&headerScript();
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");


if(!$userID)
{
	&sessionExpire;
}


my $provider = $cgi->param('provider');

my $start_time = time;

my %ini_value;

unless (&read_inifile("milo.ini")) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my ($sql, $dbh, $sth,$sql_f,$facility_id,$master_table,$report_table,$data_table,$row);

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}
my $facility = $cgi->param("facility");
$sql_f = "select facility_id ,master_table,report_table,data_table from tbl_facility_type where facility_name = '$facility'";
$sth = $dbh->prepare($sql_f);
$sth->execute();

while ($row = $sth->fetchrow_hashref) {
	$facility_id = $$row{facility_id};
	$master_table = $$row{master_table};
	$report_table = $$row{report_table};
	$data_table = $$row{data_table};
}

$sql = qq{
  SELECT th.Name as HOSP_NAME,th.address as STREET_ADDR,
th.City as CITY,th.State as STATE,
th.Zip as ZIP_CODE,th.County as COUNTY 
     FROM $master_table as th
    WHERE th.ProviderNo = ?
};

unless ($sth = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute($provider)) {
   &display_error("ERROR executing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

my ($name, $address, $city, $state, $zip, $county) = $sth->fetchrow_array;

$zip =~ s/-$//;  # crappy data

$sth->finish();

#----------------------------------------------------------------------------
# opportunity to presummarize data by PROVIDER, STATE, COUNTY   PAYMENT AMOUNT, CLAIM COUNT, 
#----------------------------------------------------------------------------

#-------------------- Selection of year in combo-box --------------------------

my $dataYear=2009;
my $year=$cgi->param('year');
if($year!='')
{
	$dataYear=$year;
}
$sql = qq{
   SELECT STATE_CD, CNTY_CD, SSA_COUNTY, SSA_STATE, SUM(PMT_AMT) AS AMT, COUNT(*) AS CNT
     FROM LDSM_$dataYear
LEFT JOIN SSA_COUNTYCODES ON STATE_CD = SSA_STATE_CODE AND CNTY_CD = SSA_CNTY_CD 
    WHERE PROVIDER = ? 
      AND SGMT_NUM = 1
 GROUP BY STATE_CD, CNTY_CD
 ORDER BY AMT DESC
};

unless ($sth = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute($provider)) {
   &display_error("ERROR executing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

my $now = localtime();

my $rows = $sth->rows;



print qq{
<HTML>
<HEAD><TITLE>COUNTY / STATE ANALYSIS BY PROVIDER</TITLE>
<SCRIPT SRC="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<SCRIPT SRC="/JS/dropmenu.js"></SCRIPT>
<link rel="stylesheet" href="/css/dropmenu.css" type="text/css" />
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<form name='selection'>
   <INPUT TYPE="hidden" NAME="facility" VALUE="$facility"> 
    <!--content start-->
         
        <table cellpadding="0" cellspacing="0" width="885px">
            <tr>
                <td valign="top">
                    <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px"  valign="top">
<Table cellpadding="0" cellspacing="0" width="100%">
	<Tr>
		<Td style="padding-bottom:10px">
				<tr><td><span class="pageheader">PROVIDER: $provider</span>&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp&nbsp &nbsp &nbsp &nbsp &nbsp&nbsp &nbsp &nbsp &nbsp &nbsp&nbsp &nbsp &nbsp &nbsp &nbsp&nbsp &nbsp &nbsp &nbsp &nbsp<b> Year: </b><font color='#EE0000'>$dataYear</font></td></tr>
				<tr><td><B>BENEFICIARY COUNTY / STATE ANALYSIS</B></td></tr>
				<tr><td><span class="tahoma12">
				$name<BR>$address<BR>$city, $state, $zip</span></td></tr>

		</td>
		
	</tr>
	<tr><td>
	<br></td></tr>
	<tr align="left">
	<td>
	<script>
	function submitYear(value)
	{
		document.selection.submit();
	}
	</script>
	<input type="hidden" value=$provider name='provider' />
	PATIENT SOURCE : <select name="year" onChange="submitYear(this.value)">
	<option value=''> Select Year </option>
	<option value=2009> 2009</option>
	<option value=2008> 2008</option>
	<option value=2007> 2007</option>
	<option value=2006> 2006</option>
</select>
</td>
<td align="right" valign="bottom" padding-bottom:10px" class="tahoma12">
$now
</td>
</tr>
</table>
<TABLE CLASS="dentry" BORDER="0" CELLPADDING="4" WIDTH="100%" class="gridstyle">
  <TR class="gridheader">
    <TH>COUNTY, STATE</TH>
    <TH>CLAIMS</TH>
    <TH ALIGN="right">PAYMENT AMOUNT</TH>
    <TH ALIGN="right">PERCENT OF TOTAL</TH>
  </TR>
};
#    <TH>COUNTY/STATE<BR>CODE</TH>

my $total_claims = 0;
my $total_amount = 0;

my $count = 0;

my @buffer;

my ($bgcolor, $i, $edit_amount);
my ($drg, $desc, $pmt_amt, $claim_count);
my ($state_cd, $cnty_cd, $pmg_amt, $claim_amt);

while (($state_cd, $cnty_cd, $county, $state, $pmt_amt, $claim_count) = $sth->fetchrow_array) {
   $total_claims += $claim_count;
   $total_amount += $pmt_amt;
   push @buffer, [$state_cd, $cnty_cd, $county, $state, $pmt_amt, $claim_count];
}


my ($class, $pct, $edit_pct, $stlink, $revlink, $amtlink);

my $limit = @buffer;

for ($i=0;$i<$limit;++$i) {

   ($state_cd, $cnty_cd, $county, $state, $pmt_amt, $claim_count) = @{$buffer[$i]};

   $edit_amount = &comify($pmt_amt);

   $edit_pct = sprintf ("%.02f", $pmt_amt / $total_amount * 100);

   $stlink = "<A HREF=\"http://ftp2.census.gov/geo/maps/general_ref/stco_outline/cen2k_pgsz/stco_$state.pdf\" TITLE=\"A COUNTY MAP OF STATE\">$state</A>";

   $amtlink = "<A HREF=\"/cgi-bin/drg_mix.pl?provider=$provider&state_cd=$state_cd&county_cd=$cnty_cd&year=$dataYear&facility=$facility\">$edit_amount</A>";

   $revlink = "<A HREF=\"/cgi-bin/derek4.pl?state_cd=$state_cd&county_cd=$cnty_cd&year=$dataYear&provider=$provider&facility=$facility\" TITLE=\"PATIENTS FROM THIS COUNTY TAKE THE BUSINESS WHERE\">$county, $state</A>";

   $class = (++$count % 2) ? "odd" : "even";

   print "   <TR CLASS=\"$class\">\n";
   print "      <TD ALIGN=\"left\">$revlink</TD>\n";
   print "      <TD ALIGN=\"right\">$claim_count</TD>\n";
   print "      <TD ALIGN=\"right\">$amtlink</TD>\n";
   print "      <TD ALIGN=\"right\">$edit_pct &#37;</TD>\n";
   print "   </TR>\n";
}

my $edit_count = &comify($total_claims);
   $edit_amount = &comify(sprintf("%.02f",$total_amount));

$class = (++$count % 2) ? "gridstyle" : "gridalternate";

print "   <TR CLASS=\"$class\">\n";
print "      <TD ALIGN=\"right\" COLSPAN=\"1\"><B>TOTAL:</B></TD>\n";
print "      <TD ALIGN=\"right\"><B>$edit_count</B></TD>\n";
print "      <TD ALIGN=\"right\"><B>$edit_amount</B></TD>\n";
print "      <TD ALIGN=\"right\">&nbsp;</B></TD>\n";
print "   </TR>\n";

my $elapsed_time = time - $start_time;

print qq{
</TABLE><span class="tahoma12">
$count ROWS IN $elapsed_time SECONDS</span>
</td>
            <td valign="top">
                <img src="/images/content_right.gif" /></td>
        </tr>
    </table>
    <!--content end-->
	};	
&TDdoc;	
	print qq{
</CENTER>
</BODY>
</HTML>
};

$sth->finish;
$dbh->disconnect;
   
exit(0);


#------------------------------------------------------------------------------

sub display_error
{
   my $s;



   print qq{
<HTML>
<HEAD><TITLE>ERROR NOTIFICATION</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>

<FONT SIZE="5" COLOR="RED">
<B>ERRORS</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" BORDER="1" CELLPADDING="20" WIDTH="600">
<TR><TD>
<UL>
};
   foreach $s (@_) {
      print "$s<BR>";
   }
   print qq{
</TD></TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</FONT>
</CENTER>
</BODY>
</HTML>
};
}

#----------------------------------------------------------------------------
sub comify
{
   local($_) = shift;
   1 while s/^(-?\d+)(\d{3})/$1,$2/;
   return($_);
}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}


#--------------------------------------------------------------------------------------
sub sessionExpire
{


print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}
#------------------------------------------------------------------------------

