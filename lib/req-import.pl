
use strict;

require "/var/www/cgi-bin/lib/req-formulas.pl";



sub update_hath {
	my($facility_id_ip, $dbh, $hath_column_name, $is_execute) = @_;

	my($sql, $rs);

	my %temp_tables;
	$sql = "SELECT facility_id, hath_table FROM tbl_facility_type";
	$rs = &run_mysql_fast($sql, 4, $dbh, undef);
	foreach my $k (keys %{$rs}) {
		my $facility_id = $$rs{$k}{'facility_id'};
		my $temp_table = $$rs{$k}{$hath_column_name};
		$temp_tables{$facility_id} = $temp_table;
	}

	my $hath_temp_table = $temp_tables{$facility_id_ip};

	#Get a list of rpt_rec_nums
	my %all_report_ids;
	$sql = "select rpt_rec_num from $hath_temp_table";
	$rs = &run_mysql_fast($sql, 4, $dbh, undef);
	foreach my $k (keys %{$rs}) {
		$all_report_ids{$$rs{$k}{"rpt_rec_num"}} = 1;
	}

	#Update the fields from the hath addresses table.
	$sql = "select db_column, formulae, facility_type, metric_type from $tbl_hath_addresses where facility_type = $facility_id_ip";
	$rs = &run_mysql_fast($sql, 4, $dbh, undef);
	foreach my $k (keys %{$rs}) {
		my $db_column = $$rs{$k}{"db_column"};
		my $formulae = $$rs{$k}{"formulae"};
		my $facility_type = $$rs{$k}{"facility_type"};
		my $metric_type = $$rs{$k}{"metric_type"};

		my $report_ids = &get_all_rpt_id_values_for_single($formulae, $facility_type, $metric_type, undef, undef);

		foreach my $rpt_rec_num (keys %all_report_ids) {
			my $value = $$report_ids{$rpt_rec_num};
			my ($sql2, $rs2);
			if (!$value) {
				$sql2 = "UPDATE $hath_temp_table SET $db_column = NULL WHERE rpt_rec_num = $rpt_rec_num";
			}
			elsif ($metric_type eq 'N') {
				$sql2 = "UPDATE $hath_temp_table SET $db_column = $value WHERE rpt_rec_num = $rpt_rec_num";
			}
			elsif ($metric_type eq 'D') {
				#Match the 4 digit BEFORE the 2 digit for a reason.
				my($day, $month, $year);
				if ( $value =~ /(\d{2})\/(\d{2})\/(\d{4})/ || $value =~ /(\d{2})-(\d{2})-(\d{4})/ ) {
					$month = $1;
					$day = $2;
					$year = $3;
					if ($year < $start_year_4) {
						$year += 2000;
					}
					else {
						$year += 1900;
					}
					$value = "$year-$month-$day";
				}
				elsif ( $value =~ /(\d{2})\/(\d{2})\/(\d{2})/ || $value =~ /(\d{2})-(\d{2})-(\d{2})/ ) {
					my $month = $1;
					my $day = $2;
					my $year = $3;
					if ($year < $start_year_2) {
						$year += 2000;
					}
					else {
						$year += 1900;
					}
					$value = "$year-$month-$day";
				}

				$sql2 = "UPDATE $hath_temp_table SET $db_column = '$value' WHERE rpt_rec_num = $rpt_rec_num";
			} # elsif ($metric_type eq 'D') {
			else {
				$value =~ s/\'/\`/sig;
				$value =~ s/\\$//sig;
				$sql2 = "UPDATE $hath_temp_table SET $db_column = '$value' WHERE rpt_rec_num = $rpt_rec_num";
			}

			if ($is_execute) {
				$rs2 = &run_mysql_fast($sql2, undef, $dbh, undef);
				print"$sql2|$rs2\n";
			}
			else {
				print "$sql2\n";
			}
		} # foreach my $rpt_rec_num (keys %all_report_ids) {
	} # foreach my $k (keys %{$rs}) {


	#Update the zip code to be just 5 digits
	if ($facility_id_ip == 1) {
		$sql = "update $hath_temp_table set hospitalzipcode = substring(hospitalzipcode, 1, 5)";
	}
	else {
		$sql = "update $hath_temp_table set zip = substring(zip, 1, 5)";
	}

	if ($is_execute) {
		$rs = &run_mysql_fast($sql, undef, $dbh, undef);
		print "$sql|$rs\n";
	}
	else {
		print "$sql\n";
	}


	if ($facility_id_ip == 3) {

		$sql = "SELECT rpt_rec_num, system_city FROM $hath_temp_table ORDER BY rpt_rec_num";
		$rs = &run_mysql_fast($sql, 4, $dbh, undef);

		foreach my $k (sort {$a <=> $b} keys %{$rs}) {
			my $rpt_rec_num = $$rs{$k}{'rpt_rec_num'};
			my $city = &trim($$rs{$k}{'system_city'});
			my $state = "";

			if ($city =~ / /) {
				$city =~ s/ +/\|/g;
				my @parts = split(/\|/, $city);
				my $zip = $parts[$#parts];
				$state = $parts[$#parts-1];
				if ($state) {
					$state =~ s/(\W)*//sig;
					$state = substr ($state, 0, 2);
				}
				$city = "";

				for my $a (0 .. $#parts-2){
					$city .= $parts[$a] . " ";
				}
				$city =~ s/ +$//;
			} #if ($city =~ / /)

			if (length($city) > 0) {
				$city = "'$city'";
			}
			else {
				$city = "NULL";
			}


			if (length($state) > 0) {
				$state = "'$state'";
			}
			else {
				$state = "NULL";
			}

			my $sql2 = "UPDATE $hath_temp_table SET system_city = $city, system_state = $state WHERE rpt_rec_num = $rpt_rec_num";

			if ($is_execute) {
				my $rs2 = &run_mysql_fast($sql2, undef, $dbh, undef);
				print"$sql2|$rs2\n";
			}
			else {
				print"$sql2\n";
			}
		} #foreach my $k (sort {$a <=> $b} keys %{$rs})
	} #if ($facility_id_ip == 3)
}



1;