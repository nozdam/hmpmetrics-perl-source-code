
use strict;

require "/var/www/cgi-bin/lib/req-milo.pl";

my $debug = 0;

sub gather_ratios_data
{
	my ($column_name, $sort, $lower, $upper) = @_;
	my $sql = "";
	my $rs = "";

	$column_name = lc($column_name);

	my %return;

	$sql = "SELECT STD($column_name) as std_deviation, AVG($column_name) as average FROM vw_element_income WHERE partial_year <> '1' AND $column_name IS NOT NULL AND hmp_control IS NOT NULL AND hmp_control <> 7 AND hmp_facility_type = 2 AND speciality_bed_ratio < 0.3 AND $column_name >= $lower AND $column_name <= $upper";
	print "STD DEV SQL: $sql\n" if ($debug == 1);
	$rs = &run_mysql($sql,4);
	my $std_deviation = $$rs{0}{'std_deviation'};
	my $mean = $$rs{0}{'average'};
	my $correction_factor_upper = $mean + (3 * $std_deviation);
	my $correction_factor_lower = $mean - (3 * $std_deviation);

	print "STD DEV = $std_deviation, MEAN = $mean, Correction Factor U = $correction_factor_upper, Correction Factor L = $correction_factor_lower\n" if ($debug == 1);
	print "LOWER: $lower, UPPER: $upper\n" if ($debug == 1);


	$sql = "SELECT rpt_rec_num, prvdr_num, year(fy_end_dt) AS rpt_year, $column_name, control_category_short FROM vw_element_income, control_type WHERE hmp_control = control_type_id AND partial_year <> '1' AND $column_name IS NOT NULL AND hmp_control IS NOT NULL AND hmp_control <> 7 AND hmp_facility_type = 2 AND speciality_bed_ratio < 0.3 AND $column_name < $correction_factor_upper AND $column_name > $correction_factor_lower AND $column_name >= $lower AND $column_name <= $upper";
	print "SELECT SQL: $sql\n\n" if ($debug == 1);
	$rs = &run_mysql($sql,4);

	foreach my $k (keys %{$rs}) {
		$return{$$rs{$k}{'rpt_year'}}{$$rs{$k}{'control_category_short'}}{"INCLUDED"}{$$rs{$k}{'prvdr_num'}} = $$rs{$k}{$column_name};
		$return{$$rs{$k}{'rpt_year'}}{"ALL"}{"INCLUDED"}{$$rs{$k}{'prvdr_num'}} += $$rs{$k}{$column_name};
	}


	# Get a list of just the exclusions to be displayed as exclusions.
	# Do NOT list the pre-exclusions

	#Exclusions:
	#	1) Metric is null
	#	2) Metric is exactly zero
	#	3) Metric value NOT between mean-3std_dev and mean+3std_dev
	#	4) Metric value NOT between the lower and upper bounds set per metric manually

	#Pre-exclusions:
	#	1) partial_year <> '1'
	#	2) hmp_control <> 7
	#	3) speciality_bed_ratio < 0.3
	#	4) hmp_facility = 2


	$sql = "SELECT rpt_rec_num, prvdr_num, year(fy_end_dt) AS rpt_year, $column_name, control_category_short, hmp_control, partial_year, speciality_bed_ratio FROM vw_element_income, control_type WHERE hmp_control = control_type_id AND ($column_name IS NULL OR $column_name >= $correction_factor_upper OR $column_name <= $correction_factor_lower OR $column_name > $upper OR $column_name < $lower) AND partial_year <> '1' AND hmp_control IS NOT NULL AND hmp_control <> 7 AND hmp_facility_type = 2 AND speciality_bed_ratio < 0.3";
	print "EXCLUSION SQL: $sql\n\n" if ($debug == 1);
	$rs = &run_mysql($sql,4);

	foreach my $k (keys %{$rs}) {
		$return{$$rs{$k}{'rpt_year'}}{$$rs{$k}{'control_category_short'}}{"EXCLUDED"}{$$rs{$k}{'prvdr_num'}} = $$rs{$k}{$column_name};
		$return{$$rs{$k}{'rpt_year'}}{"ALL"}{"EXCLUDED"}{$$rs{$k}{'prvdr_num'}} += $$rs{$k}{$column_name};
	}

	return \%return;
}



sub parse_ratios_data_report
{
	my ($column_name, $sort, $lower, $upper) = @_;
	my $rs = &gather_ratios_data($column_name, $sort, $lower, $upper);

	my %return;

	foreach my $year (sort {$a <=> $b}keys %{$rs}) {

		foreach my $hmp_control (sort {$a <=> $b}keys %{$$rs{$year}}) {

			my $count = keys %{$$rs{$year}{$hmp_control}{"INCLUDED"}};
			my $i = 1;

			##Sort the providers ascending or descending based on '$sort' according to the column values to put them in their percentiles.
			if ($sort eq 'A') {
				foreach my $provider_num (sort {$$rs{$year}{$hmp_control}{"INCLUDED"}{$a} <=> $$rs{$year}{$hmp_control}{"INCLUDED"}{$b}} keys %{$$rs{$year}{$hmp_control}{"INCLUDED"}}) {

					my $quartile = &calculate_quartile($i, $count);

					$return{$year}{$hmp_control}{$quartile}{"PROVIDERS"}{$provider_num} =  $$rs{$year}{$hmp_control}{"INCLUDED"}{$provider_num};
					$return{$year}{$hmp_control}{$quartile}{"TOTAL"} += $$rs{$year}{$hmp_control}{"INCLUDED"}{$provider_num};
					$return{$year}{$hmp_control}{$quartile}{"COUNT"}++ ;

					$i++;
				}# end of provider_num
			}
			else {
				foreach my $provider_num (sort {$$rs{$year}{$hmp_control}{"INCLUDED"}{$b} <=> $$rs{$year}{$hmp_control}{"INCLUDED"}{$a}} keys %{$$rs{$year}{$hmp_control}{"INCLUDED"}}) {

					my $quartile = &calculate_quartile($i, $count);

					$return{$year}{$hmp_control}{$quartile}{"PROVIDERS"}{$provider_num} =  $$rs{$year}{$hmp_control}{"INCLUDED"}{$provider_num};
					$return{$year}{$hmp_control}{$quartile}{"TOTAL"} += $$rs{$year}{$hmp_control}{"INCLUDED"}{$provider_num};
					$return{$year}{$hmp_control}{$quartile}{"COUNT"}++ ;

					$i++;
				}# end of provider_num
			}

		} #end of hmp_control
	} # end of year



	## EXCLUSIONS AS QUARTILES
	foreach my $year (sort {$a <=> $b}keys %{$rs}) {

		foreach my $hmp_control (sort {$a <=> $b}keys %{$$rs{$year}}) {

			foreach my $provider_num (sort {$$rs{$year}{$hmp_control}{"EXCLUDED"}{$a} <=> $$rs{$year}{$hmp_control}{"EXCLUDED"}{$b}} keys %{$$rs{$year}{$hmp_control}{"EXCLUDED"}}) {
				my $count = keys %{$$rs{$year}{$hmp_control}{"EXCLUDED"}};
				my $quartile = "EXCLUDED";

				$return{$year}{$hmp_control}{$quartile}{"PROVIDERS"}{$provider_num} =  $$rs{$year}{$hmp_control}{"EXCLUDED"}{$provider_num};
				$return{$year}{$hmp_control}{$quartile}{"TOTAL"} += $$rs{$year}{$hmp_control}{"EXCLUDED"}{$provider_num};
				$return{$year}{$hmp_control}{$quartile}{"COUNT"}++ ;
			}
		}
	}

	foreach my $year (sort {$a <=> $b} keys %return) {
		print"YEAR: $year\n\n";
		foreach my $hmp_control (sort {$a cmp $b} keys %{$return{$year}}) {
			print"CONTROL: $hmp_control\n";
			foreach my $quartile (sort {$a cmp $b} keys %{$return{$year}{$hmp_control}}) {
				my @providers = sort {$a <=> $b} keys %{$return{$year}{$hmp_control}{$quartile}{"PROVIDERS"}};
				my $provider_set = join("|", @providers);

				my @data_values = values %{$return{$year}{$hmp_control}{$quartile}{"PROVIDERS"}};
				my $median = &calculate_median(@data_values);
				print "$quartile,$return{$year}{$hmp_control}{$quartile}{'TOTAL'},$return{$year}{$hmp_control}{$quartile}{'COUNT'}," . ($return{$year}{$hmp_control}{$quartile}{'TOTAL'}/$return{$year}{$hmp_control}{$quartile}{'COUNT'}). ",$median,$provider_set\n";
			}
			print "\n";
		}
		print "\n";
	}

	return \%return;
}




sub parse_ratios_data_linear
{
	my ($column_name, $sort, $lower, $upper) = @_;
	my $rs = &gather_ratios_data($column_name, $sort, $lower, $upper);

	my %return;

	foreach my $year (sort {$a <=> $b}keys %{$rs}) {

		foreach my $hmp_control (sort {$a <=> $b}keys %{$$rs{$year}}) {

			my $count = keys %{$$rs{$year}{$hmp_control}{"INCLUDED"}};
			my $i = 1;

			##Sort the providers ascending or descending based on '$sort' according to the column values to put them in their percentiles.
			if ($sort eq 'A') {
				foreach my $provider_num (sort {$$rs{$year}{$hmp_control}{"INCLUDED"}{$a} <=> $$rs{$year}{$hmp_control}{"INCLUDED"}{$b}} keys %{$$rs{$year}{$hmp_control}{"INCLUDED"}}) {

					my $quartile = &calculate_quartile($i, $count);

					$return{$year}{$hmp_control}{$quartile}{"PROVIDERS"}{$provider_num} =  $$rs{$year}{$hmp_control}{"INCLUDED"}{$provider_num};
					$return{$year}{$hmp_control}{$quartile}{"TOTAL"} += $$rs{$year}{$hmp_control}{"INCLUDED"}{$provider_num};
					$return{$year}{$hmp_control}{$quartile}{"COUNT"}++ ;

					$i++;
				}# end of provider_num
			}
			else {
				foreach my $provider_num (sort {$$rs{$year}{$hmp_control}{"INCLUDED"}{$b} <=> $$rs{$year}{$hmp_control}{"INCLUDED"}{$a}} keys %{$$rs{$year}{$hmp_control}{"INCLUDED"}}) {

					my $quartile = &calculate_quartile($i, $count);

					$return{$year}{$hmp_control}{$quartile}{"PROVIDERS"}{$provider_num} =  $$rs{$year}{$hmp_control}{"INCLUDED"}{$provider_num};
					$return{$year}{$hmp_control}{$quartile}{"TOTAL"} += $$rs{$year}{$hmp_control}{"INCLUDED"}{$provider_num};
					$return{$year}{$hmp_control}{$quartile}{"COUNT"}++ ;

					$i++;
				}# end of provider_num
			}

		} #end of hmp_control
	} # end of year



	## EXCLUSIONS AS QUARTILES
	foreach my $year (sort {$a <=> $b}keys %{$rs}) {

		foreach my $hmp_control (sort {$a <=> $b}keys %{$$rs{$year}}) {

			foreach my $provider_num (sort {$$rs{$year}{$hmp_control}{"EXCLUDED"}{$a} <=> $$rs{$year}{$hmp_control}{"EXCLUDED"}{$b}} keys %{$$rs{$year}{$hmp_control}{"EXCLUDED"}}) {
				my $count = keys %{$$rs{$year}{$hmp_control}{"EXCLUDED"}};
				my $quartile = "EXCLUDED";

				$return{$year}{$hmp_control}{$quartile}{"PROVIDERS"}{$provider_num} =  $$rs{$year}{$hmp_control}{"EXCLUDED"}{$provider_num};
				$return{$year}{$hmp_control}{$quartile}{"TOTAL"} += $$rs{$year}{$hmp_control}{"EXCLUDED"}{$provider_num};
				$return{$year}{$hmp_control}{$quartile}{"COUNT"}++ ;
			}
		}
	}

	return \%return;
}

sub run_report {
	my ($metrics) = @_;

	my $bed_cohort = 8;

	print "\"Peer Group\",\"Metric\",\"FY_End\",\"Quartile\",\"BedCohort\",\"Mean\",\"Median\",\"nValue\"\n\n";

	foreach my $k (sort {$a cmp $b} keys %{$metrics}) {

		my $return = &parse_ratios_data_linear($k, $$metrics{$k}{'sort'}, $$metrics{$k}{'lower'}, $$metrics{$k}{'upper'});

		foreach my $year (sort {$a <=> $b} keys %{$return}) {
			foreach my $hmp_control (sort {$a cmp $b} keys %{$$return{$year}}) {
				my $quartile_total_name = "TOTAL";
				my (@quartile_values, @quartile_providers, $quartile_total, $quartile_count, $quartile_provider_set);

				foreach my $quartile (sort {$a cmp $b} keys %{$$return{$year}{$hmp_control}}) {
					my @providers = sort {$a <=> $b} keys %{$$return{$year}{$hmp_control}{$quartile}{"PROVIDERS"}};
					my $provider_set = join("|", @providers);

					my @data_values = values %{$$return{$year}{$hmp_control}{$quartile}{"PROVIDERS"}};
					my $median = &calculate_median(@data_values);

					if ($quartile ne "EXCLUDED") {
						push (@quartile_providers, @providers);
						push (@quartile_values, @data_values);
						$quartile_total += $$return{$year}{$hmp_control}{$quartile}{'TOTAL'};
						$quartile_count += $$return{$year}{$hmp_control}{$quartile}{'COUNT'};
					}


					print "\"$hmp_control\",\"$$metrics{$k}{'name'}\",\"$year\",\"$quartile\",\"$bed_cohort\",\"" . ($$return{$year}{$hmp_control}{$quartile}{'TOTAL'}/$$return{$year}{$hmp_control}{$quartile}{'COUNT'}). "\",\"$median\",\"$$return{$year}{$hmp_control}{$quartile}{'COUNT'}\",\"$provider_set\"\n";
				}

				my $quartile_median = &calculate_median(@quartile_values);
				my $quartile_provider_set = join("|", @quartile_providers);

				print "\"$hmp_control\",\"$$metrics{$k}{'name'}\",\"$year\",\"$quartile_total_name\",\"$bed_cohort\",\"" . ($quartile_total/$quartile_count). "\",\"$quartile_median\",\"$quartile_count\",\"$quartile_provider_set\"\n";
			}
		}

	}
}



sub calculate_mean {
	my(@data_set) = @_;

	my $mean = 0;
	my $count = @data_set;
	my $sum = 0;
	$sum += $_ for @data_set;

	$mean = ($sum/$count) if ($count > 0);

	return $mean;
}


sub calculate_median {
	my(@data_set) = @_;
	#print "ORIG: @data_set\n";
	@data_set =	sort {$a <=> $b} (@data_set);
	#print "SORTED: @data_set\n";

	my $count = @data_set;
	my $median = 0;
	#print "COUNT: $count\n";
	#print "#DATA_SET: $#data_set\n";

	if ($count%2 == 0) {
		#$median = ($data_set[$#data_set] + $data_set[$#data_set+1])/2;
		$median = ($data_set[($count/2 - 1)] + $data_set[($count/2)]) / 2; #If count = 6 then we want count/2 = 3 and 4. (The -1 is coz of the zero-based index).
	}
	else {
		$median = $data_set[($count+1)/2 - 1]; #or just $#data_set/2 coz $#data_set = $count-1.
	}
	#print "MEDIAN: $median\n";

	return $median;
}


sub calculate_quartile {
	my ($index, $count) = @_;
	#Q1
	if ($index <= ($count/4) || ($count < 4 && $index == 1) ) {
		return '1';
	}
	#Q2
	elsif ( ($index > ($count/4) && $index <= ($count/2)) || ($count < 4 && $index == 2) ) {
		return '2';
	}
	#Q3
	elsif ( ($index > ($count/2) && $index <= (3 * $count/4)) || ($count < 4 && $index == 3) ) {
		return '3';
	}
	#Q4
	else {
		return '4';
	}
}




1;