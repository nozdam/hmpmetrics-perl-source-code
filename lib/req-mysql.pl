

use strict;
use DBI;
use DBD::mysql;

sub HOST_DB { return "hmpmetricsdb.cyzljvo74mny.us-east-1.rds.amazonaws.com"; };
sub DB_DEFAULT { return "hcrisdb"; };
sub DB_USER { return "hmpmilo"; };
sub DB_PASS { return "LibertyPlace1650"; };


sub db_open {
	my ($db, $host) = @_;

	my $hostname = &HOST_DB;
	my $database = &DB_DEFAULT;
	if ($db)
	{
		$database = $db;
	}
	if ($host)
	{
		$hostname = $host;
	}

	my $dsn = "DBI:mysql:database=$database;host=$hostname";
	my $user = &DB_USER;
	my $pass = &DB_PASS;

	my $db_handle = DBI->connect( $dsn, $user, $pass, { RaiseError => 0 } ) || return 0;
	return $db_handle;
} #db_open


sub db_close {
	my ($db_handle) = @_;
	$db_handle->disconnect;
} #db_close



sub run_mysql {
	my ($sql, $flag, $db, $host, @value_list) = @_;

	my $db_handle = &db_open($db, $host);
	my $return = &run_mysql_fast($sql, $flag, $db_handle, @value_list);
	&db_close($db_handle);
	return $return;
} # run_mysql


sub run_mysql_fast {
	my ($sql, $flag, $db_handle, @value_list) = @_;

	my ($cursor,$rc,@fields,%records,$i);

	$cursor = $db_handle->prepare($sql);
	if ( (@value_list) && $#value_list>0 ) {
		$cursor->execute(@value_list);
	}
	else {
		$cursor->execute;
	}

	### Get records returns Ref to Hash {key->field_name->field_value} ###
	if ( $flag == 1 )
	{
		while (@fields=$cursor->fetchrow)
		{
			$i = 1;
			while ($i< @fields )
			{
				$records{$fields[0]}{lc($cursor->{'NAME'}->[$i])} = $fields[$i];
				$i++;
			}
		}

		$cursor->finish;
		return \%records;
	}

	### Return single recordset ###
	elsif ( $flag == 2 )
	{
		@fields=$cursor->fetchrow;
		$cursor->finish;
		return \@fields;
	}

   ### Return Hash of Fields ###
	elsif ( $flag == 3 )
	{
		$i = 0;
		while (@fields=$cursor->fetchrow)
		{
			$records{$i} = [ @fields ];
			$i++;
		}
		$cursor->finish;
		return \%records;
	}

	### Return $HASH{number_as_key}{field_name} = value ###
	elsif ( $flag == 4 )
	{
		my $j = 0;
		while (@fields=$cursor->fetchrow)
		{
			$i = 0;
			while ( $i< @fields )
			{
				$records{$j}{lc($cursor->{'NAME'}->[$i])} = $fields[$i];
				$i++;
			}
			$j++;
		}
		$cursor->finish;

		return \%records;
	}


	### Return Number of Rows ###
	else
	{
		$rc = $cursor->rows; # number of rows
		$cursor->finish;
		return $rc;
	}
} # run_mysql_fast







1;
