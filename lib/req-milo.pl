
use strict;

require "/var/www/cgi-bin/lib/req-mysql.pl";

my $cost_report_address_orig = "([A-Za-z0-9]{7})-(\\d{5})-(\\d{4})";
my $cost_report_address_modified = "[A-Za-z0-9]{7}:\\d{5}:\\d{4}";
my $path_logs = "/var/www/cgi-bin/logs/";

my %special_chars;
$special_chars{'\@'} = "at";
$special_chars{'\%'} = "pct";
$special_chars{'\&'} = "and";
$special_chars{'\*'} = "x";
$special_chars{'\+'} = "and";
$special_chars{'\-'} = " ";
$special_chars{'\/'} = "by";

sub trim {
	my ($string) = @_;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}

sub get_pattern_orig_address {
	return $cost_report_address_orig;
}

sub get_pattern_modified_address {
	return $cost_report_address_modified;
}

sub get_special_chars {
	return \%special_chars;
}

sub get_path_logs {
	return $path_logs;
}

sub commafy
{
  local($_) = shift;
  1 while s/^(-?\d+)(\d{3})/$1,$2/;
  return($_);
}


1;