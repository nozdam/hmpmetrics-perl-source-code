
use strict;

require "/var/www/cgi-bin/lib/req-milo.pl";

my $debug = 0;


sub is_debug() {
	return $debug;
}

sub get_all_rpt_ids {
	my ($facility_type, $facility_types) = @_;

	my %report_ids;
	my $rpt_table = $$facility_types{$facility_type}{'REPORT_TABLE'};
	my $sql = "SELECT rpt_rec_num FROM $rpt_table";
	my $rs = &run_mysql($sql,4);
	foreach my $k (keys %{$rs}) {
		$report_ids{$$rs{$k}{"rpt_rec_num"}} = 0;
	}

	return \%report_ids;
}

sub get_all_facility_types {
	my %facility_types;
	my $sql = "SELECT * FROM tbl_facility_type";
	my $rs = &run_mysql($sql,4);
	foreach my $k (keys %{$rs}) {
		$facility_types{$$rs{$k}{'facility_id'}}{"NAME"} = $$rs{$k}{'facility_name'};
		$facility_types{$$rs{$k}{'facility_id'}}{"BASE_TABLE"} = $$rs{$k}{'base_metrics_table'};
		$facility_types{$$rs{$k}{'facility_id'}}{"DERIVED_TABLE"} = $$rs{$k}{'derived_metrics_table'};
		$facility_types{$$rs{$k}{'facility_id'}}{"REPORT_TABLE"} = $$rs{$k}{'report_table'};
		$facility_types{$$rs{$k}{'facility_id'}}{"DATA_TABLE"} = $$rs{$k}{'data_table'};
		$facility_types{$$rs{$k}{'facility_id'}}{"MASTER_TABLE"} = $$rs{$k}{'master_table'};
	}

	return \%facility_types;
}

sub convert_dash_to_colon {
	my ($formula_orig) = @_;

	my $cost_report_address_orig = &get_pattern_orig_address();

	my $formula_modified = $formula_orig;
	$formula_modified =~ s/$cost_report_address_orig/$1:$2:$3/sig;
	return $formula_modified;
}


sub get_metric_table_name {
	my ($metric_column_name, $facility_type, $facility_types) = @_;

	my $table = &get_table_name($metric_column_name, $$facility_types{$facility_type}{"BASE_TABLE"});
	if (!$table) {
		$table = &get_table_name($metric_column_name, $$facility_types{$facility_type}{"DERIVED_TABLE"});
	}
	return $table;
}


sub get_table_name {
	my ($metric_column_name, $table) = @_;

	my $sql = "DESC $table";
	my $rs = &run_mysql($sql,4);
	foreach my $k (keys %{$rs}) {
		if ($metric_column_name eq $$rs{$k}{'field'}) {
			return $table;
		}
	}
	return "";
}

sub get_table_name_by_formula_type {
	my($is_formula, $facility_type, $facility_types) = @_;

	if (!$facility_types) {
		$facility_types = &get_all_facility_types();
	}

	my $table;
	my $base_table = $$facility_types{$facility_type}{"BASE_TABLE"};
	my $derived_table = $$facility_types{$facility_type}{"DERIVED_TABLE"};

	my $table = $base_table;
	if ($is_formula) {
		$table = $derived_table;
	}

	return $table;
}

sub get_all_rpt_id_values_for_metric {

	my ($formula_orig, $facility_type, $facility_types, $report_ids) = @_;

	if (!$facility_types) {
		$facility_types = &get_all_facility_types();
	}

	if (!$report_ids) {
		$report_ids = &get_all_rpt_ids($facility_type, $facility_types);
	}

	my $cost_report_address_final = &get_pattern_modified_address();

	my $formula_modified = &convert_dash_to_colon($formula_orig);

	my $data_table = $$facility_types{$facility_type}{"DATA_TABLE"};

	my %all_reports_ids;
	my @parts = $formula_modified =~ m/$cost_report_address_final/sig;
	my %parts;
	foreach my $part (@parts) {
		$parts{$part} = 1;
	}


	foreach my $address (keys %parts) {
		my ($worksheet, $line, $column) = split (":", $address);

		my $sql = "SELECT cr_rec_num, cr_value FROM $data_table WHERE cr_worksheet = '$worksheet' AND cr_line = '$line' AND cr_column = '$column'";
		print "DATA: $sql\n" if(&is_debug());
		my $rs = &run_mysql($sql,4);

		foreach my $k (keys %{$rs}) {
			$all_reports_ids{$$rs{$k}{'cr_rec_num'}}{$address} = $$rs{$k}{'cr_value'};
		}
	}


	foreach my $report_id (sort {$a <=> $b} keys %{$report_ids}) {
		my $formula = $formula_modified;
		foreach my $address (keys %parts) {
			my $address_value = (0 + $all_reports_ids{$report_id}{$address});

			# Replace the address with the value
			$formula =~ s/$address/$address_value/sig;
		}

		#Once all the cost report addresses are parsed evaluate the result.
		my $final_value = eval($formula);

		$$report_ids{$report_id} = $final_value;
	}

	return $report_ids;
}


sub get_all_rpt_id_values_for_single {

	my ($formula_orig, $facility_type, $metric_type, $facility_types, $report_ids) = @_;

	if (!$facility_types) {
		$facility_types = &get_all_facility_types();
	}

	if (!$report_ids) {
		$report_ids = &get_all_rpt_ids($facility_type, $facility_types);
	}

	my $cost_report_address_final = &get_pattern_modified_address();

	my $formula_modified = &convert_dash_to_colon($formula_orig);

	my $data_table = $$facility_types{$facility_type}{"DATA_TABLE"};

	my %all_reports_ids;
	my @parts = $formula_modified =~ m/$cost_report_address_final/sig;
	my %parts;
	foreach my $part (@parts) {
		$parts{$part} = 1;
	}

	foreach my $address (keys %parts) {
		my ($worksheet, $line, $column) = split (":", $address);

		my $sql = "SELECT cr_rec_num, cr_value FROM $data_table WHERE cr_worksheet = '$worksheet' AND cr_line = '$line' AND cr_column = '$column'";
		print "DATA: $sql\n" if(&is_debug());
		my $rs = &run_mysql($sql,4);

		foreach my $k (keys %{$rs}) {
			$all_reports_ids{$$rs{$k}{'cr_rec_num'}}{$address} = $$rs{$k}{'cr_value'};
		}
	}

	foreach my $report_id (sort {$a <=> $b} keys %{$report_ids}) {
		my $formula = $formula_modified;
		foreach my $address (keys %parts) {
			my $address_value;
			if ($metric_type eq "N") {
				$address_value = (0 + $all_reports_ids{$report_id}{$address});
			}
			else {
				$address_value = $all_reports_ids{$report_id}{$address};
			}

			# Replace the address with the value
			$formula =~ s/$address/$address_value/sig;
		}

		#Once all the cost report addresses are parsed evaluate the result.
		my $final_value;
		if (length($formula_modified) == 18 && $formula_modified =~ /$cost_report_address_final/) {
			$final_value = $formula;
		}
		else {
			$final_value = eval($formula);
		}

		$$report_ids{$report_id} = $final_value;
	}

	return $report_ids;
}

sub update_metric_values {
	my ($metric_column_name, $facility_type, $formula, $is_formulae, $facility_types, $report_ids) = @_;

	my $update_table = &get_metric_table_name($metric_column_name, $facility_type, $facility_types);

	my($sql, $rs);
	my $dbh = &db_open();

	if (!$update_table) {
		# Column not found in either table. We need to create it in one of the 2.
		$update_table = &get_table_name_by_formula_type($is_formulae, $facility_type, $facility_types);

		my $column_type = "DECIMAL(19,4)";
		$sql = "ALTER TABLE $update_table ADD COLUMN $metric_column_name $column_type";
		#print "$sql\n";
		$rs = &run_mysql_fast($sql, undef, $dbh, undef);
		print "$sql|$rs\n" if(&is_debug());
	}

	&populate_metric_values($metric_column_name, $update_table, $dbh, $report_ids);

	$sql = "SELECT count(*) AS counts FROM $update_table WHERE $metric_column_name IS NOT NULL";
	$rs = &run_mysql_fast($sql, 4, $dbh, undef);
	my $counts = $$rs{0}{'counts'};

	if ($counts == 0) {
		print "METRIC: $metric_column_name in TABLE: $update_table has the WRONG FORMULA: $formula for Facility Type: $facility_type\n";
	}

	&db_close($dbh);
}


sub populate_metric_values {
	my ($metric_column_name, $update_table, $dbh, $report_ids) = @_;

	my($sql, $rs);

	foreach my $report_id (sort {$a <=> $b} keys %{$report_ids}) {
		my $value = $$report_ids{$report_id};
		if ($value) {
			$sql = "UPDATE $update_table SET $metric_column_name = $$report_ids{$report_id} WHERE rpt_rec_num = $report_id";
		}
		else {
			$sql = "UPDATE $update_table SET $metric_column_name = NULL WHERE rpt_rec_num = $report_id";
		}
		#print "$sql\n";
		$rs = &run_mysql_fast($sql, undef, $dbh, undef);
		print "$sql|$rs\n" if(is_debug());
	}
}

sub get_metric_column_name {
	my ($field_name, $is_formula, $facility_type, $facility_types) = @_;

	if (!$facility_types) {
		$facility_types = &get_all_facility_types();
	}

	my $base_table = $$facility_types{$facility_type}{"BASE_TABLE"};
	my $derived_table = $$facility_types{$facility_type}{"DERIVED_TABLE"};

	my $table = $base_table;
	if ($is_formula) {
		$table = $derived_table;
	}

	my $column_name = &convert_fieldname_to_columnname($field_name);

	return $column_name;
}

sub validate_metric_column_name {
	my ($metric_column_name, $facility_type, $facility_types) = @_;

	my $table_name = &get_metric_table_name($metric_column_name, $facility_type, $facility_types);

	return 0 if($table_name);

	my $element;
	my $sql = "select element from standard_metrics where quartile_db_column = '$metric_column_name' and facility_type = $facility_type";
	my $rs = &run_mysql($sql,4);
	$element = $$rs{0}{'element'} if($rs);

	if (!$element) {
		return 1;
	}
	return 0;
}

sub should_process_history_record {
	my ($action, $old_value, $new_value) = @_;

	my $should_process = 0;
	#1 - Record SHOULD be processed
	#0 - Record SHOULD NOT be processed just update the processing status to 1.

	my $pattern_address = &get_pattern_orig_address();
	if ($old_value =~ m/$pattern_address/ || $new_value =~ m/$pattern_address/) {
		$should_process = 1;
	}
	if ($action eq "Create" || $action eq "Update") {
		$should_process = 1;
	}

	return $should_process;
}

sub mark_history_record_processed {
	my ($history_id) = @_;

	my $sql = "UPDATE metric_history SET processing_status = 1 WHERE history_id = '$history_id'";
	#print"$sql\n";
	my $rs = &run_mysql($sql);
	print "$sql|$rs\n" if(is_debug());
}

sub convert_fieldname_to_columnname {
	my($field_name) = @_;

	my $special_chars = &get_special_chars();

	my $column_name = $field_name;
	foreach my $char (keys %{$special_chars}) {
		$column_name =~ s/$char/$$special_chars{$char}/eg;
	}
	$column_name =~ s/\s+/_/sig;
	$column_name =~ s/\W//sig;

	if (length($column_name) > 64) {
		$column_name = substr($column_name, 0, 64);
	}

	return lc($column_name);
}


1;