
use strict;
use Spreadsheet::ParseExcel;
use Spreadsheet::WriteExcel;
use Data::Dumper;
use Scalar::Util qw(looks_like_number);


require "/var/www/cgi-bin/lib/req-milo.pl";

my $path = "/var/www/html";
my @align_horizontal = ('', 'left', 'center', 'right', 'fill', 'justify', 'center_across');
my @align_vertical = ('top', 'vcenter', 'bottom', 'vjustify', 'vequal_space');
my %worksheet_inserts;
$worksheet_inserts{"S300001"} = 1;


sub get_worksheet_inserts {
	return \%worksheet_inserts;
}


sub get_cr_path{
	return $path;
}

sub locate_header_row {
	my($sheet) = @_;

	for(my $iR = $sheet->{MinRow}; defined $sheet->{MaxRow} && $iR <= $sheet->{MaxRow}; $iR++) {
		my $value_string = "";
		my %return_hash;
		for(my $iC = $sheet->{MinCol}; defined $sheet->{MaxCol} && $iC <= $sheet->{MaxCol}; $iC++) {
			my $oWkC = $sheet->{Cells}[$iR][$iC];
			if ($oWkC) {
				my $value = "";
				if ($oWkC->{Value}) {
					$value = $oWkC->{Value};
					if (lc($value) eq "general" && $oWkC->{Val}) {
						$value = $oWkC->{Val};
					}
				}
				elsif ($oWkC->{Val}) {
					$value = $oWkC->{Val};
				}

				$value_string .= $value . ",";
				if ($value) {
					$return_hash{$iC} = $value;
				}
			}
		}
		#print "$value_string\n\n";

		if ($value_string =~ /1\,2\,/ || $value_string =~ /1\,1\.01\,/) {
			foreach my $k (sort {$a <=> $b} keys %return_hash) {
				my $v = $return_hash{$k};
				if ($v) {
					$v = &trim($v);
					if ($v && looks_like_number($v)) {
						$return_hash{$k} = sprintf("%04d", ($v*100));
					}
					else {
						delete $return_hash{$k};
					}
				}
			}
			return \%return_hash;
		}
	}
	return;
}

sub get_col_widths {
	my($col_width_default, $col_width) = @_;
	for (my $i=0; $i<=$#{$col_width}; $i++) {
		$$col_width[$i] = $col_width_default if (!$$col_width[$i]);
	}
}

sub get_row_heights {
	my($row_height_default, $row_height) = @_;
	for (my $i=0; $i<=$#{$row_height}; $i++) {
		$$row_height[$i] = $row_height_default if (!$$row_height[$i]);
	}
}

sub create_work_sheet {
	my($spreadsheet_name, $spreadsheet_path, $do_mapping, $dest_book, $worksheet) = @_;

	#open(J, ">data.txt");
	#print J "DO MAPPING: $do_mapping\n" ;
	#Fill the excel sheet with Actual values
	my $oExcel = new Spreadsheet::ParseExcel;
	my $oBook = $oExcel->Parse($spreadsheet_path);
	my($iR, $iC, $oWkS, $oWkC);

	$oWkS = $oBook->{Worksheet}[0];
	$oWkS->{Name} = $spreadsheet_name;
	my(@col_width, @row_height);
	# Row Heights and Column Widths.
	if (exists $oWkS->{ColWidth}) {
		@col_width = @{$oWkS->{ColWidth}};
		my $col_width_default = $oWkS->{DefColWidth};
		&get_col_widths($col_width_default, \@col_width);
	}

	if (exists $oWkS->{RowHeight}) {
		@row_height = @{$oWkS->{RowHeight}};
		my $row_height_default = $oWkS->{DefRowHeight};
		&get_row_heights($row_height_default, \@row_height);
	}

	#Locate header row.
	my $header_hash = &locate_header_row($oWkS);
	my $dest_sheet = $dest_book->addworksheet($oWkS->{Name});
	my $format;

	if ($do_mapping) {
		#print J "DO MAPPING IF BLOCK\n";
		my $row_current = 0;
		my @cr_lines = (sort {$a <=> $b} keys %{$worksheet});
		#print J (join(",", @cr_lines)) . "\n";

		my (%excel_data, %cr_line_col_values, %excel_to_cr, %cr_to_excel, %new_excelline_oldexcelline, %modified);
		for(my $iR = $oWkS->{MinRow}; defined $oWkS->{MaxRow} && $iR <= $oWkS->{MaxRow}; $iR++) {
			my $cr_line = "";
			for(my $iC = $oWkS->{MinCol}; defined $oWkS->{MaxCol} && $iC <= $oWkS->{MaxCol}; $iC++) {

				$oWkC = $oWkS->{Cells}[$iR][$iC];
				if ($oWkC) {
					my $value = "";
					if ($oWkC->{Value}) {
						$value = $oWkC->{Value};
						if (lc($value) eq "general" && $oWkC->{Val}) {
							$value = $oWkC->{Val};
						}
					}
					elsif ($oWkC->{Val}) {
						$value = $oWkC->{Val};
					}

					if ($iC == 0 && looks_like_number($value)) {
						$cr_line = sprintf("%05d", ($value*100));
						#print J "Excel Line: $iR, CR_LINE: $cr_line\n";
					}

					#If we have gotten far enough down the spread sheet so that the numbers have started..
					my $cr_column = "";
					my $cr_value = "";
					if ($cr_line) {
						#get the cr_column based on the spreadsheet column
						$cr_column = $$header_hash{$iC} if($$header_hash{$iC});
						if ($cr_column) {
							$cr_value = $$worksheet{$cr_line}{$cr_column};
							if ($cr_value) {
								$value = $cr_value;
							}
							else {
								$value = undef; #Removed the 'X'
							}
							$cr_line_col_values{$cr_line}{$cr_column} = $value;
						}
						$oWkC->{Val} = $value;
					}

					$value = &trim($value) if($value);
					if ($value && uc($value) eq "X") {
						$value = undef;
					}

					$excel_data{$iR}{$iC} = $value;
					$excel_to_cr{$iR} = $cr_line;

					if ($cr_line) {
						$cr_to_excel{$cr_line} = $iR;
					}
				}
			}
		}

		#Now add the remaining values from the "ALL_DATA" table
		foreach my $cr_line (sort {$a <=> $b} keys %{$worksheet}) {
			foreach my $cr_column (sort {$a <=> $b} keys %{$$worksheet{$cr_line}}) {
				my $cr_value = $$worksheet{$cr_line}{$cr_column};
				$cr_line_col_values{$cr_line}{$cr_column} = $cr_value;
			}
		}

		foreach my $excel_line (sort {$a <=> $b} keys%excel_to_cr) {
			my $cr_line = $excel_to_cr{$excel_line};
			#print J "Excel: $excel_line, CR: $cr_line\n";
		}


		#Now get new excel row numbers
		my $have_cr_lines_started = 0;

		my @all_cr_lines = (sort {$a <=> $b} keys %cr_line_col_values);
		my @excel_cr_lines = (sort {$a <=> $b} keys%cr_to_excel);

		#print J (join(",", @all_cr_lines)) . "\n";
		#print J (join(",", @excel_cr_lines)) . "\n";

		my $index = -1;
		my $cr_index_excel = 0;
		my $cr_index_all = 0;
		foreach my $excel_line (sort {$a <=> $b} keys%excel_to_cr) {
			++$index;
			my $cr_line = $excel_to_cr{$excel_line};
			if ($cr_line) {
				$have_cr_lines_started = 1;
			}

			if (!$have_cr_lines_started) {
				$new_excelline_oldexcelline{$index} = $excel_line;
				foreach my $col (sort {$a <=> $b} keys %{$excel_data{$excel_line}}) {
					$modified{$index}{$col} = $excel_data{$excel_line}{$col};
				}
			}
			else {
				#cr lines have started

				if ($cr_line) {
					#print J "$index, $cr_line, $cr_index_excel, $cr_index_all, $excel_cr_lines[$cr_index_excel], $all_cr_lines[$cr_index_all]\n";
					if ($excel_cr_lines[$cr_index_excel] eq $all_cr_lines[$cr_index_all]) {
						$new_excelline_oldexcelline{$index} = $excel_line;
						++$cr_index_all;

						foreach my $col (sort {$a <=> $b} keys %{$excel_data{$excel_line}}) {
							$modified{$index}{$col} = $excel_data{$excel_line}{$col};
						}
					}
					else {
						#print J "ELSE BLOCK: $cr_index_excel, $cr_index_all\n";
						for (my $j=$cr_index_all; $j<$#all_cr_lines && $all_cr_lines[$j] le $excel_cr_lines[$cr_index_excel]; $j++) {
							#++$index;
							#print J "ELSE BLOCK: $index, $cr_index_excel, $cr_index_all, $j, $all_cr_lines[$j], $excel_cr_lines[$cr_index_excel]\n";
							if ($excel_cr_lines[$cr_index_excel] ne $all_cr_lines[$j]) {
								$new_excelline_oldexcelline{$index} = "";
								$modified{$index}{0} = sprintf ("%2f", ($all_cr_lines[$j]/100));
								$modified{$index}{1} = sprintf ("%05d", $all_cr_lines[$j]);
								foreach my $col (sort {$a <=> $b} keys %{$cr_line_col_values{$all_cr_lines[$j]}}) {
									my $col_2 = ($col/100) + 2;
									#print J "$col, $col_2, $all_cr_lines[$j], $cr_line_col_values{$all_cr_lines[$j]}{$col}\n";
									$modified{$index}{$col_2} =  $cr_line_col_values{$all_cr_lines[$j]}{$col};
								}
								$modified{$index}{2} =  substr($cr_line_col_values{$all_cr_lines[$j]}{'0000'} ,5);
								++$index;
								#$modified{$index}{"CR_LINE"} = $all_cr_lines[$j];
							}
							else {
								$new_excelline_oldexcelline{$index} = $excel_line;
								foreach my $col (sort {$a <=> $b} keys %{$excel_data{$excel_line}}) {
									$modified{$index}{$col} = $excel_data{$excel_line}{$col};
								}
							}
							++$cr_index_all;
						}
					}
					++$cr_index_excel;
				}
				else {
					#This is not a cr_line
					$new_excelline_oldexcelline{$index} = $excel_line;
					foreach my $col (sort {$a <=> $b} keys %{$excel_data{$excel_line}}) {
						$modified{$index}{$col} = $excel_data{$excel_line}{$col};
					}
				}
			}
		}

		my %col_formats;
		foreach my $new_excel_line (sort {$a <=> $b} keys %modified) {
			my $old_excel_line = $new_excelline_oldexcelline{$new_excel_line};

			if ($old_excel_line ) {
				$dest_sheet->set_row($new_excel_line, $row_height[$old_excel_line]) if (exists $oWkS->{RowHeight});
			}
			else {
				$dest_sheet->set_row($new_excel_line, $oWkS->{DefRowHeight}) if (exists $oWkS->{DefRowHeight});
			}

			#if ($new_excel_line == 0 && exists $oWkS->{ColWidth}) {
			#	for(my $iC = $oWkS->{MinCol}; defined $oWkS->{MaxCol} && $iC <= $oWkS->{MaxCol}; $iC++) {
			#		$dest_sheet->set_column($iC, $iC, $col_width[$iC]);
			#	}
			#}

			for(my $excel_col = $oWkS->{MinCol}; defined $oWkS->{MaxCol} && $excel_col <= $oWkS->{MaxCol}; $excel_col++) {
			#foreach my $excel_col (sort {$a <=> $b} keys %{$modified{$new_excel_line}}) {
				my $value = $modified{$new_excel_line}{$excel_col};

				#if ($new_excel_line == 0 && exists $oWkS->{ColWidth}) {
				#	print J join(',', @{$oWkS->{ColWidth}}) . ", $oWkS->{DefColWidth}\n";
				#	print J "$excel_col, $col_width[$excel_col]\n";
				#	$dest_sheet->set_column($excel_col, $excel_col, $col_width[$excel_col]);
				#}

				if ($new_excel_line == 0 && exists $oWkS->{ColWidth}) {
					$dest_sheet->set_column($excel_col, $excel_col, $col_width[$excel_col]);
				}

				if ($old_excel_line) {
					$oWkC = $oWkS->{Cells}[$old_excel_line][$excel_col];
					#print J "$old_excel_line, $new_excel_line, $excel_col, $value\n";
					$format = &add_worksheet_format($value, $dest_book, $oWkC, \@align_horizontal, \@align_vertical);
					$col_formats{$excel_col} = $format;
					$dest_sheet->write($new_excel_line, $excel_col, $value, $format);
				}
				else {
					#print J ",$new_excel_line, $excel_col, $value\n";
					$format = $col_formats{$excel_col};
					if (looks_like_number($value)) {
						&align_cell($value, $format, \@align_horizontal, \@align_vertical, undef);
					}
					$dest_sheet->write($new_excel_line, $excel_col, $value, $format);
				}
			}
		}

		undef %excel_data;
		undef %cr_line_col_values;
		undef %excel_to_cr;
		undef %cr_to_excel;
		undef %new_excelline_oldexcelline;
		undef %modified;
	} #if ($do_mapping)
	else {
		#print J "DO MAPPING ELSE BLOCK\n";
		for(my $iR = $oWkS->{MinRow}; defined $oWkS->{MaxRow} && $iR <= $oWkS->{MaxRow}; $iR++) {
			$dest_sheet->set_row($iR, $row_height[$iR]) if (exists $oWkS->{RowHeight});
			my $cr_line = "";
			for(my $iC = $oWkS->{MinCol}; defined $oWkS->{MaxCol} && $iC <= $oWkS->{MaxCol}; $iC++) {

				if ($iR == 0 && exists $oWkS->{ColWidth}) {
					$dest_sheet->set_column($iC, $iC, $col_width[$iC]);
				}

				$oWkC = $oWkS->{Cells}[$iR][$iC];
				if ($oWkC) {
					my $value = "";
					if ($oWkC->{Value}) {
						$value = $oWkC->{Value};
						if (lc($value) eq "general" && $oWkC->{Val}) {
							$value = $oWkC->{Val};
						}
					}
					elsif ($oWkC->{Val}) {
						$value = $oWkC->{Val};
					}

					if ($iC == 0 && looks_like_number($value)) {
						$cr_line = sprintf("%05d", ($value*100));
					}

					#If we have gotten far enough down the spread sheet so that the numbers have started..
					my $cr_column = "";
					my $cr_value = "";
					if ($cr_line) {
						#get the cr_column based on the spreadsheet column
						$cr_column = $$header_hash{$iC} if($$header_hash{$iC});
						if ($cr_column) {
							$cr_value = $$worksheet{$cr_line}{$cr_column};
							if ($cr_value) {
								$value = $cr_value;
							}
							else {
								$value = undef; #Removed the 'X'
							}
						}
						$oWkC->{Val} = $value;
					}

					$value = &trim($value) if($value);
					if ($value && uc($value) eq "X") {
						$value = undef;
					}

					$format = &add_worksheet_format($value, $dest_book, $oWkC, \@align_horizontal, \@align_vertical);
					if ($format) {
						$dest_sheet->write($iR, $iC, $value, $format);
					}
					else {
						$dest_sheet->write($iR, $iC, $value);
					}
				}
			}
		}
	}

	undef $oExcel;
	#close J;
}


sub modify_line_numbers {
	my($worksheet, $sth, $line_mappings, $worksheet_inserts, $value_hash, $col_hash) = @_;

	my ($row, $col, $value);

	while (($row, $col, $value) = $sth->fetchrow_array) {
		&modify_line_number($worksheet, $row, $col, $value, "single", $line_mappings, $worksheet_inserts, $value_hash, $col_hash);
   }
}

sub modify_line_number {
	my($worksheet, $row, $col, $value, $return_single_multi, $line_mappings, $worksheet_inserts, $value_hash, $col_hash) = @_;

	if ( (keys %{$line_mappings}) && exists $$line_mappings{$row}) {
	   if (exists $$worksheet_inserts{$worksheet} && $row ne $$line_mappings{$row}) {
		   if ($return_single_multi eq "multi") {
			    $$value_hash{$row}{$col} = "";
		   }
		   else {
			    $$value_hash{"$row:$col"} = "";
		   }

		   $row = $$line_mappings{$row};
		   if ($return_single_multi eq "multi") {
			   $$value_hash{$row}{$col} = $value;
		   }
		   else {
			   $$value_hash{"$row:$col"} = $value;
		   }
	   }
	   else {
		   $row = $$line_mappings{$row};
		   if ($return_single_multi eq "multi") {
			   $$value_hash{$row}{$col} = $value;
		   }
		   else {
			   $$value_hash{"$row:$col"} = $value;
		   }
	   }
   }
   else {
	   if ($return_single_multi eq "multi") {
		   $$value_hash{$row}{$col} = $value;
	   }
	   else {
		   $$value_hash{"$row:$col"} = $value;
	   }
   }
   $$col_hash{$col} = 'Y';
}


sub map_lines {
	my($data_table, $reportid, $worksheet, $dbh, $line_mappings, $line_mappings_r) = @_;

	my $sql = "SELECT cr_line, cr_value FROM $data_table WHERE cr_rec_num=$reportid AND cr_worksheet='$worksheet' AND cr_column='0000' AND cr_type='A' AND cr_value IS NOT NULL AND cr_value <> ''";
	my $rs = &run_mysql_fast($sql, 4, $dbh);
	if (keys %{$rs}) {
	   foreach my $k (keys %{$rs}) {
			my $cr_l = $$rs{$k}{'cr_line'};
			my $cr_v = $$rs{$k}{'cr_value'};

			$cr_v = substr($cr_v, 0, 5);

			if (looks_like_number($cr_v)) {
				$$line_mappings{$cr_l} = $cr_v;
				$$line_mappings_r{$cr_v} = $cr_l;
			}
	   }
	}
}


sub add_worksheet_format {
	my($value, $dest_book, $oWkC, $align_horizontal, $align_vertical) = @_;

	my $format;
	if (exists $oWkC->{Format} && defined $oWkC->{Format}) {
		$format = $dest_book->add_format();
		if ( exists ${$oWkC->{Format}}{"BdrColor"} ) {
			$format->set_left_color(${$oWkC->{Format}->{BdrColor}}[0]);
			$format->set_right_color(${$oWkC->{Format}->{BdrColor}}[1]);
			$format->set_top_color(${$oWkC->{Format}->{BdrColor}}[2]);
			$format->set_bottom_color(${$oWkC->{Format}->{BdrColor}}[3]);
		}

		if ( exists ${$oWkC->{Format}}{"BdrStyle"} ) {
			$format->set_left(${$oWkC->{Format}->{BdrStyle}}[0]);
			$format->set_right(${$oWkC->{Format}->{BdrStyle}}[1]);
			$format->set_top(${$oWkC->{Format}->{BdrStyle}}[2]);
			$format->set_bottom(${$oWkC->{Format}->{BdrStyle}}[3]);
		}

		if ( exists ${$oWkC->{Format}}{"Fill"} ) {
			$format->set_pattern(${$oWkC->{Format}->{Fill}}[0]);
			$format->set_fg_color(${$oWkC->{Format}->{Fill}}[1]);
			$format->set_bg_color(${$oWkC->{Format}->{Fill}}[2]);
		}

		if ( exists ${$oWkC->{Format}}{"Font"} ) {
			$format->set_color($oWkC->{Format}->{Font}->{Color});
			$format->set_font($oWkC->{Format}->{Font}->{Name});
			$format->set_size($oWkC->{Format}->{Font}->{Height});

			$format->set_bold($oWkC->{Format}->{Font}->{Bold});
			$format->set_italic($oWkC->{Format}->{Font}->{Italic});
			$format->set_underline($oWkC->{Format}->{Font}->{Underline});
			$format->set_font_strikeout($oWkC->{Format}->{Font}->{Strikeout});
		}

		&align_cell($value, $format, $align_horizontal, $align_vertical, $oWkC);
	}
	return $format;
}


sub align_cell {
	my($value, $format, $align_horizontal, $align_vertical, $oWkC) = @_;

	if ($oWkC) {
		# Align VERTICAL
		$format->set_align($$align_vertical[$oWkC->{Format}->{AlignV}]) if(exists ${$oWkC->{Format}}{"AlignV"});

		# Align HORIZONTAL
		if (looks_like_number($value)) {
			if ($value =~ /\./) {
				$format->set_num_format('#,##0.00');
			}
			else {
				$format->set_num_format('#,##0');
			}
			$format->set_align('right');
		}
		elsif (exists ${$oWkC->{Format}}{"AlignH"}) {
			$format->set_align($$align_horizontal[$oWkC->{Format}->{AlignH}]);
		}
	}
	else {
		# Align HORIZONTAL
		if (looks_like_number($value)) {
			if ($value =~ /\./) {
				$format->set_num_format('#,##0.00');
			}
			else {
				$format->set_num_format('#,##0');
			}
			$format->set_align('right');
		}
	}
}


1;