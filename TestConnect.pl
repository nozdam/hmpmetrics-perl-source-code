#!/usr/bin/perl

use CGI;
use DBI;

my $cgi = new CGI;
my $peerGrpMainId;
my $peerGrpMainName = $cgi->param('peername'); 
my $peerGrpMainDesc = $cgi->param('desc');
my $lstrpids = $cgi->param('strpids'); # provider numbers separated by :
my $lstrnames = $cgi->param('strpnames'); # Hospital Names separated by :

# Database 

my $dbh;
my ($sql, $sth);


# Search Criteria values

my $c_lproviderID = $cgi->param('js_lproviderNum');
my $c_lproviderName = $cgi->param('js_lproviderName'); 
my $c_lAddress = $cgi->param('js_lAddress');
my $c_lCity = $cgi->param('js_lCity');
my $c_lStates = $cgi->param('js_lStates');
my $c_lZip = $cgi->param('js_lZip');
my $c_lCountry = $cgi->param('js_lCountry');
my $c_lSystem = $cgi->param('js_lSystem');
my $c_lToc = $cgi->param('js_lToc ');
my $c_lHosp_Type = $cgi->param('js_lHosp_Type');
my $c_lMsa = $cgi->param('js_lMsa');
my $c_lMin_beds = $cgi->param('js_lMin_beds');
my $c_lMax_beds = $cgi->param('js_lMax_beds');
my $c_lMin_ftes = $cgi->param('js_lMin_ftes');
my $c_lMax_ftes = $cgi->param('js_lMax_ftes');
my $c_lMin_net_income = $cgi->param('js_lMin_net_income');
my $c_lMax_net_income = $cgi->param('js_lMax_net_income');
my $c_lLocation = $cgi->param('js_lLocation');
my $c_lRadius = $cgi->param('js_lRadius');

if($c_lproviderID){}else{
	$c_lproviderID = '';
}
if($c_lproviderName){}else{
	$c_lproviderName = '';
}
if($c_lAddress){}else{
	$c_lAddress = '';
}
if($c_lCity){}else{
	$c_lCity = '';
}
if($c_lStates){}else{
	$c_lStates = '';
}
if($c_lZip){}else{
	$c_lZip = '';
}
if($c_lCountry){}else{
	$c_lCountry = '';
}
if($c_lSystem){}else{
	$c_lSystem = '';
}
if($c_lToc){}else{
	$c_lToc = '';
}
if($c_lHosp_Type){}else{
	$c_lHosp_Type = '';
}
if($c_lMsa){}else{
	$c_lMsa = '';
}
if($c_lMin_beds){}else{
	$c_lMin_beds = NULL;
}
if($c_lMax_beds){}else{
	$c_lMax_beds = NULL;
}
if($c_lMin_ftes){}else{
	$c_lMin_ftes = NULL;
}
if($c_lMax_ftes){}else{
	$c_lMax_ftes = NULL;
}
if($c_lMin_net_income){}else{
	$c_lMin_net_income = NULL;
}
if($c_lMax_net_income){}else{
	$c_lMax_net_income = NULL;
}
if($c_lLocation){}else{
	$c_lMax_net_income = NULL;
}
if($c_lRadius){}else{
	$c_lMax_net_income = NULL;
}
if($peerGrpMainDesc){}else{
	$peerGrpMainDesc = NULL;
}

# Split and store provider numers and Hospital Names in arrays.

my @arrayOfIds = split(/:/,$lstrpids);
my @arrayOfNames = split(/:/,$lstrnames);

# Get Database connection

$dbh = DBI->connect("dbi:mysql:Milo:contentserver",root,root);

# Select max(peergroup_mainid) ,increment it by 1 and insert into database. 

$sql = "select max(PeerGroup_ID) from peergroup_main";
$sth = $dbh->prepare($sql);
$sth->execute();

#output database results
@row = $sth->fetchrow_array();
$peerGrpMainId = @row[0];
 
$peerGrpMainId = ($peerGrpMainId+1);

# Prepare query for insertion.

$sql = "insert into peergroup_main(PeerGroup_ID,Name,c_Provider_ID,c_Provider_Name,c_Provider_Address,c_Provider_City,c_Provider_States,c_Provider_zip,c_Provider_country,c_System,c_Control,c_Hospital_Type,c_MSA,C_Bed_Start,c_Bed_End,c_FTE_Start,c_FTE_End,c_Income_Start,c_Income_End,c_Within,c_Miles_Of_zip,CreateDate,Description) values($peerGrpMainId,'$peerGrpMainName','$c_lproviderID','$c_lproviderName','$c_lAddress','$c_lCity','$c_lStates',
       '$c_lZip','$c_lCountry','$c_lSystem','$c_lToc','$c_lHosp_Type','$c_lMsa',$c_lMin_beds,$c_lMax_beds,$c_lMin_ftes,$c_lMax_ftes,$c_lMin_net_income,$c_lMax_net_income,'$c_lRadius','$c_lLocation',CURRENT_DATE,'$peerGrpMainDesc')";

$sth = $dbh->prepare($sql);

$sth->execute || 
           die "Insert record failed ... maybe invalid?";      

# Loop through array and insert peer grop details.           
for(my $i=0;$i<($#arrayOfIds+1);$i++){
	if(@arrayOfIds[$i] eq ''){
		next;
	}else{
		$sql = "insert into peergroup_details(PeerGroup_Main_ID,Provider_number,ProviderName) values($peerGrpMainId,'@arrayOfIds[$i]','@arrayOfNames[$i]')";
		$sth = $dbh->prepare($sql);
		$sth->execute();
	}
}

print "Content-Type: text/html\n\n";
print qq{
<HTML>
<HEAD><TITLE>MYSQL DATABASE STATUS</TITLE></HEAD>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon">
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
 <link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />

<BODY>
<CENTER>
<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/index.html" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/cr_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="/cgi-bin/cr_batchx.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="/cgi-bin/snf_batch1.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="/cgi-bin/graph.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Graph Utility</div>">
                                <img src="/images/graph_icon_inner.gif" onmouseover="src='/images/graph_icon_inner_hover.gif';"
                                    onmouseout="src='/images/graph_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 102px" align="Center">
                            <a href="/team.html" class="addToolTip" title="<div id='ToolTipTextWrap'>Milo Team</div>">
                                <img src="/images/milo_team_icon_inner.gif" onmouseover="src='/images/milo_team_icon_inner_hover.gif';"
                                    onmouseout="src='/images/milo_team_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->


<!--content start--><br>
    <table cellpadding="0" cellspacing="0" width="885px">
        <tr>
            <td valign="top">
                <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px">

<TABLE align='center' CELLPADDING="0" CELLSPACING="0" width="100%">
<Tr><Td align='center'>
<div class="pageheader">Peer Group created Successfully.</div>
</td></tr>
<tr><td align="center">
	<FORM ACTION="#">
		<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
	</FORM>
</td></tr>
</table>
</CENTER>
</BODY>
</HTML>
};
