#! /usr/bin/perl

use strict;

require "/var/www/cgi-bin/lib/req-import.pl";

my $facility_id_ip = 1;

my $start_year_2 = 60;
my $start_year_4 = 1900 + $start_year_2;

my ($sql, $rs);

my $tbl_hath_addresses = "hath_addresses";
my $dbh = &db_open();


my %temp_tables;
$sql = "SELECT facility_id, hath_temp_table FROM tbl_facility_type";
$rs = &run_mysql_fast($sql, 4, $dbh, undef);
foreach my $k (keys %{$rs}) {
	my $facility_id = $$rs{$k}{'facility_id'};
	my $temp_table = $$rs{$k}{'hath_temp_table'};
	$temp_tables{$facility_id} = $temp_table;
}

my $hath_temp_table = $temp_tables{$facility_id_ip};


if(1 == 2) {


#Create a temp hath table
$sql = "CREATE TABLE $hath_temp_table LIKE hath";
$rs = &run_mysql_fast($sql, undef, $dbh, undef);


#Insert all records into hath temp from the newly imported CR_ALL_RPT
$sql = "INSERT INTO $hath_temp_table(rpt_rec_num, fy_bgn_dt, fy_end_dt, hmp_year, providerno) SELECT rpt_rec_num, fy_bgn_dt, fy_end_dt, year(fy_end_dt), prvdr_num FROM cr_all_rpt";
$rs = &run_mysql_fast($sql, undef, $dbh, undef);


#Delete all records in hath_temp which already exist in hath
$sql = "DELETE FROM $hath_temp_table WHERE rpt_rec_num IN (SELECT rpt_rec_num FROM HATH)";
$rs = &run_mysql_fast($sql, undef, $dbh, undef);


##########################################



####################   FIELDS IN HATH ADDRESSES     ######################
#Get a list of rpt_rec_nums
my %all_report_ids;
$sql = "select rpt_rec_num from $hath_temp_table";
$rs = &run_mysql_fast($sql, 4, $dbh, undef);
foreach my $k (keys %{$rs}) {
	$all_report_ids{$$rs{$k}{"rpt_rec_num"}} = 1;
}

#Update the fields from the hath addresses table.
$sql = "select db_column, formulae, facility_type, metric_type from $tbl_hath_addresses where facility_type = $facility_id_ip";
$rs = &run_mysql_fast($sql, 4, $dbh, undef);
foreach my $k (keys %{$rs}) {
	my $db_column = $$rs{$k}{"db_column"};
	my $formulae = $$rs{$k}{"formulae"};
	my $facility_type = $$rs{$k}{"facility_type"};
	my $metric_type = $$rs{$k}{"metric_type"};

	my $report_ids = &get_all_rpt_id_values_for_single($formulae, $facility_type, $metric_type, undef, undef);

	foreach my $rpt_rec_num (keys %all_report_ids) {
		my $value = $$report_ids{$rpt_rec_num};
		my ($sql2, $rs2);
		if (!$value) {
			$sql2 = "UPDATE $hath_temp_table SET $db_column = NULL WHERE rpt_rec_num = $rpt_rec_num";
		}
		elsif ($metric_type eq 'N') {
			$sql2 = "UPDATE $hath_temp_table SET $db_column = $value WHERE rpt_rec_num = $rpt_rec_num";
		}
		elsif ($metric_type eq 'D') {

			#Match the 4 digit BEFORE the 2 digit for a reason.
			my($day, $month, $year);
			if ( $value =~ /(\d{2})\/(\d{2})\/(\d{4})/ || $value =~ /(\d{2})-(\d{2})-(\d{4})/ ) {
				$month = $1;
				$day = $2;
				$year = $3;
				if ($year < $start_year_4) {
					$year += 2000;
				}
				else {
					$year += 1900;
				}
				$value = "$year-$month-$day";
			}
			elsif ( $value =~ /(\d{2})\/(\d{2})\/(\d{2})/ || $value =~ /(\d{2})-(\d{2})-(\d{2})/ ) {
				my $month = $1;
				my $day = $2;
				my $year = $3;
				if ($year < $start_year_2) {
					$year += 2000;
				}
				else {
					$year += 1900;
				}
				$value = "$year-$month-$day";
			}

			$sql2 = "UPDATE $hath_temp_table SET $db_column = '$value' WHERE rpt_rec_num = $rpt_rec_num";
		}
		else {
			$value =~ s/\'/\`/sig;
			$value =~ s/\\$//sig;
			$sql2 = "UPDATE $hath_temp_table SET $db_column = '$value' WHERE rpt_rec_num = $rpt_rec_num";
		}
		#print "$sql\n";
		$rs2 = &run_mysql_fast($sql2, undef, $dbh, undef);
		print"$sql2|$rs2\n";
	}
}


#Update the zip code to be just 5 digits
if ($facility_id_ip == 1) {
	$sql = "update $hath_temp_table set hospitalzipcode = substring(hospitalzipcode, 1, 5)";
}
else {
	$sql = "update $hath_temp_table set zip = substring(zip, 1, 5)";
}
#print "$sql\n";
$rs = &run_mysql_fast($sql, undef, $dbh, undef);
print "$sql|$rs\n";
##########################################




####################  Error Code PRVDR     ######################
my %hath_street_zip;
$sql = "select distinct providerno, upper(hospitalstreet) as hospitalstreet, upper(shortzip) as hospitalzipcode from hath";
$rs = &run_mysql_fast($sql, 4, $dbh, undef);
foreach my $k (keys %{$rs}) {
	my $street = $$rs{$k}{'hospitalstreet'};
	my $zipcode = $$rs{$k}{'hospitalzipcode'};
	my $providerno = $$rs{$k}{'providerno'};
	my @street_parts = split (/ /, $street);
	$street = $street_parts[0];
	$hath_street_zip{"$street|$zipcode"} = $providerno;
}


$sql = "select rpt_rec_num, providerno, upper(hospitalstreet) as hospitalstreet, upper(hospitalzipcode) as hospitalzipcode from $hath_temp_table";
$rs = &run_mysql_fast($sql, 4, $dbh, undef);
my $review_code = "PRVDR";
foreach my $k (keys %{$rs}) {
	my $street = $$rs{$k}{'hospitalstreet'};
	my $zipcode = $$rs{$k}{'hospitalzipcode'};
	my $providerno = $$rs{$k}{'providerno'};
	my @street_parts = split (/ /, $street);
	$street = $street_parts[0];

	my $providerno_hath = $hath_street_zip{"$street|$zipcode"};

	if ($providerno ne $providerno_hath) {
		my $rpt_rec_num = $$rs{$k}{'rpt_rec_num'};
		&add_error_code($rpt_rec_num, $review_code, $dbh, undef);
	}

}
##########################################



#####################  Error Codes CAH, CTRL and FAC       #####################
my %provider_info;
$sql = "select providerno, hmpfacilitytype, hmpcontrol, cah from tblhathmaster";
$rs = &run_mysql_fast($sql, 4, $dbh, undef);
foreach my $k (keys %{$rs}) {
	my $hmpfacilitytype = $$rs{$k}{'hmpfacilitytype'};
	my $hmpcontrol = $$rs{$k}{'hmpcontrol'};
	my $cah = $$rs{$k}{'cah'};
	my $providerno = $$rs{$k}{'providerno'};
	$provider_info{$providerno}{"facility"} = $hmpfacilitytype;
	$provider_info{$providerno}{"control"} = $hmpcontrol;
	$provider_info{$providerno}{"cah"} = $cah;
}


$sql = "select rpt_rec_num, providerno, facilitytype, typeofcontrol, cah from $hath_temp_table";
$rs = &run_mysql_fast($sql, 4, $dbh, undef);
foreach my $k (keys %{$rs}) {
	my $facilitytype = $$rs{$k}{'facilitytype'};
	my $control = $$rs{$k}{'typeofcontrol'};
	my $cah = $$rs{$k}{'cah'};
	my $providerno = $$rs{$k}{'providerno'};
	my $rpt_rec_num = $$rs{$k}{'rpt_rec_num'};

	my $hmpfacilitytype = $provider_info{$providerno}{'facility'};
	my $hmpcontrol = $provider_info{$providerno}{'control'};
	my $hmpcah = $provider_info{$providerno}{'cah'};

	my($sql2, $rs2);

	if ($facilitytype ne $hmpfacilitytype) {
		&add_error_code($rpt_rec_num, "FAC", $dbh, undef);
	}
	if ($control ne $hmpcontrol) {
		&add_error_code($rpt_rec_num, "CTRL", $dbh, undef);
	}
	if ($cah ne $hmpcah) {
		&add_error_code($rpt_rec_num, "CAH", $dbh, undef);
	}
}
##########################################

################# System ID #########################

$sql = "UPDATE $hath_temp_table ht, tblhathmaster hm SET ht.systemid=hm.systemid WHERE ht.providerno = hm.providerno";
$rs = &run_mysql_fast($sql, undef, $dbh, undef);
print "$sql|$rs\n";
##########################################



################### System Name and Home Office Address #######################
$sql = "select rpt_rec_num from $hath_temp_table where (length(system_name) > 0 OR length(home_office_address) > 0) and length(systemid) = 0";
$rs = &run_mysql_fast($sql, 4, $dbh, undef);
foreach my $k (keys %{$rs}) {
	my $rpt_rec_num = $$rs{$k}{'rpt_rec_num'};
	&add_error_code($rpt_rec_num, "SYS", $dbh, undef);
}
##########################################

}





####################   FIELDS IN HATH ADDRESSES     ######################

my $facility_id_ip = 2;

my %temp_tables;
$sql = "SELECT facility_id, hath_table FROM tbl_facility_type";
$rs = &run_mysql_fast($sql, 4, $dbh, undef);
foreach my $k (keys %{$rs}) {
	my $facility_id = $$rs{$k}{'facility_id'};
	my $temp_table = $$rs{$k}{'hath_table'};
	$temp_tables{$facility_id} = $temp_table;
}

my $hath_temp_table = $temp_tables{$facility_id_ip};

#Get a list of rpt_rec_nums
my %all_report_ids;
$sql = "select rpt_rec_num from $hath_temp_table";
$rs = &run_mysql_fast($sql, 4, $dbh, undef);
foreach my $k (keys %{$rs}) {
	$all_report_ids{$$rs{$k}{"rpt_rec_num"}} = 1;
}

#Update the fields from the hath addresses table.
$sql = "select db_column, formulae, facility_type, metric_type from $tbl_hath_addresses where facility_type = $facility_id_ip";
$rs = &run_mysql_fast($sql, 4, $dbh, undef);
foreach my $k (keys %{$rs}) {
	my $db_column = $$rs{$k}{"db_column"};
	my $formulae = $$rs{$k}{"formulae"};
	my $facility_type = $$rs{$k}{"facility_type"};
	my $metric_type = $$rs{$k}{"metric_type"};

	my $report_ids = &get_all_rpt_id_values_for_single($formulae, $facility_type, $metric_type, undef, undef);

	foreach my $rpt_rec_num (keys %all_report_ids) {
		my $value = $$report_ids{$rpt_rec_num};
		my ($sql2, $rs2);
		if (!$value) {
			$sql2 = "UPDATE $hath_temp_table SET $db_column = NULL WHERE rpt_rec_num = $rpt_rec_num";
		}
		elsif ($metric_type eq 'N') {
			$sql2 = "UPDATE $hath_temp_table SET $db_column = $value WHERE rpt_rec_num = $rpt_rec_num";
		}
		elsif ($metric_type eq 'D') {

			#Match the 4 digit BEFORE the 2 digit for a reason.
			my($day, $month, $year);
			if ( $value =~ /(\d{2})\/(\d{2})\/(\d{4})/ || $value =~ /(\d{2})-(\d{2})-(\d{4})/ ) {
				$month = $1;
				$day = $2;
				$year = $3;
				if ($year < $start_year_4) {
					$year += 2000;
				}
				else {
					$year += 1900;
				}
				$value = "$year-$month-$day";
			}
			elsif ( $value =~ /(\d{2})\/(\d{2})\/(\d{2})/ || $value =~ /(\d{2})-(\d{2})-(\d{2})/ ) {
				my $month = $1;
				my $day = $2;
				my $year = $3;
				if ($year < $start_year_2) {
					$year += 2000;
				}
				else {
					$year += 1900;
				}
				$value = "$year-$month-$day";
			}

			$sql2 = "UPDATE $hath_temp_table SET $db_column = '$value' WHERE rpt_rec_num = $rpt_rec_num";
		}
		else {
			$value =~ s/\'/\`/sig;
			$value =~ s/\\$//sig;
			$sql2 = "UPDATE $hath_temp_table SET $db_column = '$value' WHERE rpt_rec_num = $rpt_rec_num";
		}
		#print "$sql2\n";
		$rs2 = &run_mysql_fast($sql2, undef, $dbh, undef);
		print"$sql2|$rs2\n";
	}
}


#Update the zip code to be just 5 digits
if ($facility_id_ip == 1) {
	$sql = "update $hath_temp_table set hospitalzipcode = substring(hospitalzipcode, 1, 5)";
}
else {
	$sql = "update $hath_temp_table set zip = substring(zip, 1, 5)";
}
#print "$sql\n";
$rs = &run_mysql_fast($sql, undef, $dbh, undef);
print "$sql|$rs\n";



if (3 == 4) {
	my $facility_id_ip = 3;

	my %temp_tables;
	$sql = "SELECT facility_id, hath_table FROM tbl_facility_type";
	$rs = &run_mysql_fast($sql, 4, $dbh, undef);
	foreach my $k (keys %{$rs}) {
		my $facility_id = $$rs{$k}{'facility_id'};
		my $temp_table = $$rs{$k}{'hath_table'};
		$temp_tables{$facility_id} = $temp_table;
	}

	my $hath_temp_table = $temp_tables{$facility_id_ip};

	$sql = "SELECT rpt_rec_num, system_city FROM $hath_temp_table ORDER BY rpt_rec_num";
	$rs = &run_mysql_fast($sql, 4, $dbh, undef);

	foreach my $k (sort {$a <=> $b} keys %{$rs}) {
		my $rpt_rec_num = $$rs{$k}{'rpt_rec_num'};
		my $city = &trim($$rs{$k}{'system_city'});
		my $state = "";

		if ($city =~ / /) {
			$city =~ s/ +/\|/g;
			my @parts = split(/\|/, $city);
			my $zip = $parts[$#parts];
			$state = $parts[$#parts-1];
			if ($state) {
				$state =~ s/(\W)*//sig;
				$state = substr ($state, 0, 2);
			}
			$city = "";

			for my $a (0 .. $#parts-2){
				$city .= $parts[$a] . " ";
			}
			$city =~ s/ +$//;
		}

		if (length($city) > 0) {
			$city = "'$city'";
		}
		else {
			$city = "NULL";
		}


		if (length($state) > 0) {
			$state = "'$state'";
		}
		else {
			$state = "NULL";
		}

		my $sql2 = "UPDATE $hath_temp_table SET system_city = $city, system_state = $state WHERE rpt_rec_num = $rpt_rec_num";
		#print"$sql2\n";
		my $rs2 = &run_mysql_fast($sql2, undef, $dbh, undef);
		print"$sql2|$rs2\n";
	}

} #if (3 == 4)




&db_close($dbh);





################## FUNCTIONS ################

sub add_error_code
{
	my($rpt_rec_num, $err_code, $dbh, $test) = @_;

	my $sql2 = "INSERT INTO hath_temp_error_codes (rpt_rec_num, err_code) VALUES ($rpt_rec_num, '$err_code')";
	if (!$test) {
		my $rs2 = &run_mysql_fast($sql2, undef, $dbh, undef);
		print "$sql2|$rs2\n";
	}
	else {
		print "$sql2\n";
	}
}