#! /usr/bin/perl -w

use strict;
use LWP::Simple;
use URI::Encode;

require "/var/www/cgi-bin/lib/req-milo.pl";

my $uri = URI::Encode->new();

my $flag = "ALL";

my $op_file = "";
if ($flag eq "ALL") {
	#$op_file = "data/zipcodes_all.dat";
	$op_file = "/var/www/cgi-bin/logs/google.dat";
}
else {
	$op_file = "/var/www/cgi-bin/logs/google.dat";
}

my $url = "http://www.google.com/search?q=";
my $QS = "###STREET_ADDRESS###%2C+###STATE###";

my $sql;
if ($flag eq "ALL") {
	$sql = "select distinct upper(HospitalStreet) as hospitalstreet, upper(HospitalCity) as hospitalcity, upper(HospitalState) as hospitalstate, shortzip from hath_temp  order by hospitalstate, shortzip";
}
else {
	$sql = "select distinct upper(HospitalStreet) as hospitalstreet, upper(HospitalCity) as hospitalcity, HospitalCounty, upper(HospitalState) as hospitalstate, HospitalZipCode, shortzip from hath where shortzip NOT IN (select zipcode from zipcodes) order by hospitalstate, shortzip";
}
my $rs = &run_mysql($sql,4);

open (OP, ">$op_file");
foreach my $k (sort {$a <=> $b} keys%{$rs}) {
	my $address = "";
	my $state = "";

	$address = $$rs{$k}{'hospitalstreet'};
	$state = $$rs{$k}{'hospitalstate'};

	my $qs = $QS;
	$qs =~ s/###STREET_ADDRESS###/$address/sig;
	$qs =~ s/###STATE###/$state/sig;

	$qs = $uri->encode($qs);

	my $html = get ($url.$qs);
	my @matches = $html =~ m/$zipcode_pattern/sig;
	my @zipcodes;
	foreach my $zip (@matches) {
		$zip =~ s/$remove_pattern//sig;
		my ($first_5, $last_4) = split('-', $zip);
		push @zipcodes, $first_5;
	}

	#print OP "$address|$city|$state|$county|$shortzip|" . join(',', @zipcodes). "\n";
	print OP "$address|$city|$state|$shortzip|" . join(',', @zipcodes). "\n";

}
close(OP);