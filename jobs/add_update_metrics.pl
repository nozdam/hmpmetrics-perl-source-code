#! /usr/bin/perl

use strict;

require "/var/www/cgi-bin/lib/req-formulas.pl";

print "$0 START: " . localtime(time) . "\n";

#my $facility_type = 1;
#my $metric_column_name = "ratio_of_cost_to_charges_rcc";
# EBITDAR Margin
#my $formula_orig = "C000001-10100-0500 / (C000001-10100-0600 + C000001-10100-0700)";

my $facility_types = &get_all_facility_types();

#my $sql = "select metrics_id, element, formulae, quartile_db_column, facility_type from standard_metrics where quartile_db_column is not null order by element";
#my $rs = &run_mysql($sql,4);

my $sql = "select history_id, metric_id, action, processing_status, old_value, new_value from metric_history where processing_status = 0";
my $rs_history = &run_mysql($sql,4);
my %metric_ids;

my $records = keys %{$rs_history};

if ($records > 0) {
	foreach my $k (sort {$a <=> $b} keys %{$rs_history}) {
		my $history_id = $$rs_history{$k}{'history_id'};
		my $metric_id = $$rs_history{$k}{'metric_id'};
		my $action = $$rs_history{$k}{'action'};
		my $processing_status = $$rs_history{$k}{'processing_status'};
		my $old_value = $$rs_history{$k}{'old_value'};
		my $new_value = $$rs_history{$k}{'new_value'};
		if (!exists $metric_ids{$metric_id}) {

			$metric_ids{$metric_id} = 1;

			my $should_process = &should_process_history_record($action, $old_value, $new_value);

			if ($should_process) {
				$sql = "select metrics_id, element, formulae, quartile_db_column, facility_type, isformulae from standard_metrics where metrics_id = $metric_id";
				my $rs = &run_mysql($sql,4);

				my $facility_type = $$rs{0}{'facility_type'};
				my $metrics_id = $$rs{0}{'metrics_id'};
				my $element = $$rs{0}{'element'};
				my $metric_column_name = $$rs{0}{'quartile_db_column'};
				my $formula_orig = $$rs{0}{'formulae'};
				my $is_formulae = $$rs{0}{'isformulae'};

				#print "Element: $element, Column Name: $metric_column_name, Formula: $formula_orig, $is_formulae\n";
				my $report_ids = &get_all_rpt_id_values_for_metric($formula_orig, $facility_type, $facility_types, undef);

				if ($metric_column_name) {
					&update_metric_values($metric_column_name, $facility_type, $formula_orig, $is_formulae, $facility_types, $report_ids);
					#print "\n";
				}
			}
		} #if (! exists $metric_ids{$metric_id}) {

		&mark_history_record_processed($history_id);
	} #foreach my $k (sort {$a <=> $b} keys %{$rs_history}) {
} # if($records >0)
else {
	print "No records found to process\n";
}

print "$0 END: " . localtime(time) . "\n\n";
