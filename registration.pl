#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: Registration.pl
#  author: Jayanth Raj
#----------------------------------------------------------------------------

use strict;
use CGI;
use DBI;

# use Session;
my $cgi = new CGI;

use CGI::Session;

my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");

my $dbh;
my ($sql, $sth, $key);

# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my $defaultYear= $ini_value{'DEFAULT_YEAR'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

my %statesDetails = &getStatesDetails();
my %hospitalDetails = &getHospitalDetails();
my %passwordSecurityQry = &getPasswordSecurityQry();
my %roleDetails = &getRoleDetails();

print "Content-Type: text/html\n\n";

print qq{
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<title>PROJECT MILO Registration Form...</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="/css/register/main.css" rel="stylesheet" type="text/css">
<link href="/css/register/qm.css" rel="stylesheet" type="text/css">

<script language="JavaScript" type="text/JavaScript">

//This function validates the form entries...
function funcValidateForm() {
	var frmFormName=document.forms["frmSingerSLRegistry"];

	spattern = /^[\\s]+\$\/
	pattern= /^.+\@.+\\..+\$\/	
	
    	str=frmFormName.txtUserName.value;
	if (str.match(spattern)) {
		alert("Please enter a valid entry for the Email Address...")
		frmFormName.txtUserName.focus();
		return false;
	}
	if (!str.match(pattern)) {
		alert("Please enter a valid entry for the Email Address...")
		frmFormName.txtUserName.focus();
		return false;
	}

	pattern=/^[a-zA-Z0-9\'\-\_\+\#\|\.\,\&\@\(\)\s]+\$\/

	str=frmFormName.txtPassword.value;
	if (frmFormName.txtPassword.value=="" || frmFormName.txtPassword.value.length<5) {
		alert("Please enter the Password. It should be more than 4 characters.");
		frmFormName.txtPassword.focus();
		return false;
	}
	if (str.match(spattern)) {
		alert("Please enter a valid entry for the Password...")
		frmFormName.txtPassword.focus();
		return false;
	}
	if (!str.match(pattern)) {
		alert("Invalid characters. Please check and re-enter the Password...")
		frmFormName.txtPassword.focus();
		return false;
	}

	str=frmFormName.txtPasswordConfirm.value;
	if (frmFormName.txtPasswordConfirm.value=="" || frmFormName.txtPasswordConfirm.value.length<5) {
		alert("Please enter the Confirmation Password. It should be more than 4 characters.");
		frmFormName.txtPasswordConfirm.focus();
		return false;
	}
	if (str.match(spattern)) {
		alert("Please enter a valid entry for the Confirmation Password...")
		frmFormName.txtPasswordConfirm.focus();
		return false;
	}
	if (!str.match(pattern)) {
		alert("Invalid characters. Please check and re-enter the Confirmation Password...")
		frmFormName.txtPasswordConfirm.focus();
		return false;
	}

	if (frmFormName.txtPasswordConfirm.value!=frmFormName.txtPassword.value) {
		alert("Conrirmation Password does not match with the Password...");
		frmFormName.txtPasswordConfirm.select();
		return false;
	}
	
	if (frmFormName.txtSecurityQry.options[frmFormName.txtSecurityQry.selectedIndex].value=="") {
		alert("Please select a security question...");
		frmFormName.txtSecurityQry.focus();
		return false;
	}
	else if(frmFormName.txtSecurityQry.options[frmFormName.txtSecurityQry.selectedIndex].value=="0"){		
		str=frmFormName.txtSecurityOtherQry.value;
		if (str.replace(/^\\s+|\\s+\$/g,"") =="") {
			alert("Please enter your security question...")
			frmFormName.txtSecurityOtherQry.focus();
			return false;				
		}
		frmFormName.txtSecurityOtherQry.value =  frmFormName.txtSecurityOtherQry.value.replace(/\'/g,"''");
	}	
	str=frmFormName.txtSecurityAns.value;		
	if (str.replace(/^\\s+|\\s+\$/g,"") =="") {
		alert("Please enter your security question answer...")
		frmFormName.txtSecurityAns.focus();
		return false;				
	}
	frmFormName.txtSecurityAns.value = frmFormName.txtSecurityAns.value.replace(/\'/g,"''");
	
	if(frmFormName.txtUserType.options[frmFormName.txtUserType.selectedIndex].value == "0"){
		alert("Please set user type");
		frmFormName.txtUserType.focus();
		return false;
	}
	if (frmFormName.txtNameTitle.options[frmFormName.txtNameTitle.selectedIndex].value=="n") {
		alert("Please select your title...");
		frmFormName.txtNameTitle.focus();
		return false;
	}

	pattern=/^[a-zA-Z\'\-\.\,\&\@\(\)\s]+\$\/
	str=frmFormName.txtFirstName.value;
	if (frmFormName.txtFirstName.value=="" || frmFormName.txtFirstName.value.length<3) {
		alert("Please enter your First Name. It should be more than 2 characters.");
		frmFormName.txtFirstName.focus();
		return false;
	}
	if (str.match(spattern)) {
		alert("Please enter a valid entry for your First Name...")
		frmFormName.txtFirstName.focus();
		return false;
	}
	if (!str.match(pattern)) {
		alert("Invalid characters. Please enter only letters in your First Name...")
		frmFormName.txtFirstName.focus();
		return false;
	}

	pattern=/^[a-zA-Z\'\-\.\,\&\@\(\)\s]+\$\/
	str=frmFormName.txtLastName.value;
	if (frmFormName.txtLastName.value=="" || frmFormName.txtLastName.value.length<3) {
		alert("Please enter your Last Name. It should be more than 2 characters.");
		frmFormName.txtLastName.focus();
		return false;
	}
	if (str.match(spattern)) {
		alert("Please enter a valid entry for your Last Name...")
		frmFormName.txtLastName.focus();
		return false;
	}
	if (!str.match(pattern)) {
		alert("Invalid characters. Please enter only letters in your Last Name...")
		frmFormName.txtLastName.focus();
		return false;
	}

	pattern = /^[0-9\-\+\,\(\)\s]+\$\/
	str=trim(frmFormName.txtTelephone1.value);
	if (trim(frmFormName.txtTelephone1.value)!="") {
		if (str.match(spattern)) {
			alert("Please enter a valid entry for the Telephone Number...")
			frmFormName.txtTelephone1.select();
			return false;
		}
		if (!str.match(pattern)) {
			alert("Invalid characters. Please check your entry and type in only numbers...")
			frmFormName.txtTelephone1.select();
			return false;
		}
	}

	str=trim(frmFormName.txtTelephone2.value);
	if (trim(frmFormName.txtTelephone2.value)!="") {
		if (str.match(spattern)) {
			alert("Please enter a valid entry for the Telephone Number...")
			frmFormName.txtTelephone2.select();
			return false;
		}
		if (!str.match(pattern)) {
			alert("Invalid characters. Please check your entry and type in only numbers...")
			frmFormName.txtTelephone2.select();
			return false;
		}
	}
	
	
	frmFormName.txtOtherFacility.value = frmFormName.txtOtherFacility.value.replace(/\'/g,"''");
	frmFormName.txtAddress1.value = frmFormName.txtAddress1.value.replace(/\'/g,"''");
	frmFormName.txtAddress2.value = frmFormName.txtAddress2.value.replace(/\'/g,"''");
	frmFormName.txtAddress3.value = frmFormName.txtAddress3.value.replace(/\'/g,"''");
	
	
	spattern = /^[\s]+\$\/
	pattern= /^.+\@.+\..+\$\/

	/*str=frmFormName.txtEmail.value;
	if (frmFormName.txtEmail.value!="") {
		if (str.match(spattern)) {
			alert("Please enter a valid entry for the Email Address...")
			frmFormName.txtEmail.focus();
			return false;
		}
		if (!str.match(pattern)) {
			alert("Please enter a valid entry for the Email Address...")
			frmFormName.txtEmail.focus();
			return false;
		}
	}*/

	// Removes leading whitespaces
	function LTrim( value ) {

		var re = /\s*((\S+\s*)*)/;
		return value.replace(re, "$1");

	}

	// Removes ending whitespaces
	function RTrim( value ) {

		var re = /((\s*\S+)*)\s*/;
		return value.replace(re, "$1");

	}

	// Removes leading and ending whitespaces
	function trim( value ) {

		return LTrim(RTrim(value));

	}


}


var minpwlength = 4;
var fairpwlength = 7;

var STRENGTH_SHORT = 0;  // less than minpwlength
var STRENGTH_WEAK = 1;  // less than fairpwlength
var STRENGTH_FAIR = 2;  // fairpwlength or over, no numbers
var STRENGTH_STRONG = 3; // fairpwlength or over with at least one number

img0 = new Image();
img1 = new Image();
img2 = new Image();
img3 = new Image();

img0.src = '/images/password/tooshort.jpg';
img1.src = '/images/password/fair.jpg';
img2.src = '/images/password/medium.jpg';
img3.src = '/images/password/strong.jpg';

var strengthlevel = 0;

var strengthimages = Array( img0.src,img1.src,img2.src,img3.src );

function updatestrength( pw ) {
	document.frmSingerSLRegistry.img.style.visibility = "visible";

	if( istoosmall( pw ) ) {

		strengthlevel = STRENGTH_SHORT;

	}
	else if( !isfair( pw ) ) {

		strengthlevel = STRENGTH_WEAK;

	}
	else if( hasnum( pw ) ) {

		strengthlevel = STRENGTH_STRONG;

	}
	else {

		strengthlevel = STRENGTH_FAIR;

	}

	document.getElementById( 'strength' ).src = strengthimages[ strengthlevel ];

}

function isfair( pw ) {

	if( pw.length < fairpwlength ) {

		return false;

	}
	else {

		return true;

	}

}

function istoosmall( pw ) {

	if( pw.length < minpwlength ) {

		return true;

	}
	else {

		return false;

	}

}

function hasnum( pw ) {

	var hasnum = false;

	for( var counter = 0; counter < pw.length; counter ++ ) {

		if( !isNaN( pw.charAt( counter ) ) ) {

			hasnum = true;

		}

	}


	return hasnum;

}

function checkOtherFacility(Obj)
{	
	if(Obj.options[Obj.selectedIndex].value == 'Other')
	{
		document.getElementById('otherFacility').innerHTML = '<table><tr><td><input name="txtOtherFacility" class="TextBox01" id="txtOtherFacility" size="60" maxlength="200" type="text"> <font color="#ff0000"></font></td></tr></table>';
		document.getElementById('otherFacilityLabel').innerHTML = '<table><tr><td><p><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Specify Your Facility</b></font></p></td></tr></table>';
		document.getElementById('otherFacilityColon').innerHTML = '<table><tr><td><font color="#333399"><b>:</b></font></td></tr></table>';
	}
	else
	{
		document.getElementById('otherFacility').innerHTML = '';
		document.getElementById('otherFacilityLabel').innerHTML = '';
		document.getElementById('otherFacilityColon').innerHTML = '';
	}
}

function checkOtherSecurityAns(Obj)
{	
	if(Obj.options[Obj.selectedIndex].value == '0')
	{
		document.getElementById('otherSecurityAns').innerHTML = '<table><tr><td><input name="txtSecurityOtherQry" class="TextBox01" id="txtSecurityOtherQry" size="60" maxlength="500" type="text"> <font color="#ff0000">*</font></td></tr></table>';
		document.getElementById('otherSecurityAnsLabel').innerHTML = '<table><tr><td><p><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Specify Your Question</b></font></p></td></tr></table>';
		document.getElementById('otherSecurityAnsColon').innerHTML = '<table><tr><td><font color="#333399"><b>:</b></font></td></tr></table>';		
	}
	else
	{
		document.getElementById('otherSecurityAns').innerHTML = '';
		document.getElementById('otherSecurityAnsLabel').innerHTML = '';
		document.getElementById('otherSecurityAnsColon').innerHTML = '';
	}
}


</script>


<script type="text/javascript">
function toggle_username(userid) {
	document.getElementById('username_exists').innerHTML = '';

	

	var frmFormName=document.forms["frmSingerSLRegistry"];
	
	spattern = /^[\\s]+\$\/
	pattern= /^.+\@.+\\..+\$\/	
			
    	str=frmFormName.txtUserName.value;
	if (str.match(spattern)) {
		alert("Please enter a valid entry for the Email Address...")
		frmFormName.txtUserName.focus();
		return false;
	}
	if (!str.match(pattern)) {
		alert("Please enter a valid entry for the Email Address...")
		frmFormName.txtUserName.focus();
		return false;
	}
	
	// Ajax 
	if (window.XMLHttpRequest) {
		http = new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		http = new ActiveXObject("Microsoft.XMLHTTP");
	}
	handle = document.getElementById(userid);
	var url = '/cgi-bin/RegistrationUserValidation.pl?';
	if(handle.value.length > 0) {
		var fullurl = url + 'search=' + handle.value +'&saction=checkUser';        
		http.open("GET", fullurl, true);
		http.send(null);
		http.onreadystatechange = statechange_username;
	}else{
		document.getElementById('username_exists').innerHTML = '';
	}

}

function statechange_username() {
    if (http.readyState == 4) {
        var xmlObj = http.responseXML;                
        var html = xmlObj.getElementsByTagName('result').item(0).firstChild.data;        
        var show = " ";        
        if(html.length == 5)
        {        	
        	document.getElementById('txtUserName').select();
        	show= "<table><tr><td><img src='/images/chk_off.png'></td><td>User Name <b><i>"+ document.getElementById('txtUserName').value +"</i></b> is not available</td></tr></table>";
        	document.getElementById('txtUserName').value = '';
        }
        else if(html.length == 6)
        {
        	show= "<table><tr><td><img src='/images/chk_on.png'></td><td>User Name is available</td></tr></table>";
        }
        document.getElementById('username_exists').innerHTML = show;        
    }      
}
</script>


<link rel="stylesheet" type="text/css" href="/milo.css">

</head>
<body leftmargin="0" topmargin="0" marginheight="20" marginwidth="0">
<!--header start-->
<CENTER>
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/cgi-bin/index.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/cr_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="/cgi-bin/cr_batchx.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="/cgi-bin/snf_batch1.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="/cgi-bin/standard_reports.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Standard Reports</div>">
			        <img src="/images/stardardreport_inner_icon.gif" onmouseover="src='/images/stardardreport_inner_icon_hover.gif';"
                                    onmouseout="src='/images/stardardreport_inner_icon.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 102px" align="Center">
                            <a href="/cgi-bin/PeerMain.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
                                <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->    
    <!--content start-->
    <table cellpadding="0" cellspacing="0" width="885px">
        <tr><td align="left">Welcome <b>
            	$firstName $lastName
            	</b></td>  
            	<td align="right">
            	<a href="/cgi-bin/login.pl?saction=signOut">Sign Out</a>
            	</td>
        	</tr>
    </table>
    <table cellpadding="0" cellspacing="0" width="885px">
        <tr>            
            <td valign="top" style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; ">
  
	  	<table align="center" class="ColrBk01" width="885px" border="0" cellpadding="4" cellspacing="0">
		  <tbody><tr>
			<td class="Fnt11Pix" background="/images/register/grad_01_hover.gif"><b class="Colr03">Create new user</b></td>
			<td class="Fnt11Pix" background="/images/register/grad_01_hover.gif" align="right"><b class="Colr03"></b></td>
		  </tr>
		</tbody></table>
		<table class="ColrBk02" width="885px" border="0" cellpadding="2" cellspacing="0">
		  <tbody><tr>
			<td><form action="/cgi-bin/Registration_DAO.pl" method="post" name="frmSingerSLRegistry" id="frmSingerSLRegistry" onsubmit="return funcValidateForm();">
				<table width="100%"  bgcolor="#ffffff" border="0" cellpadding="4" cellspacing="0">
				  <tbody><tr>
					<td colspan="2" class="ColrBk03"><b>&nbsp;<font color="#0066cc">Login Details...</font></b></td>
					<td class="ColrBk03">&nbsp;</td>
					<td class="ColrBk03">&nbsp;</td>
				  </tr>
				  <tr>
					<td width="10%">&nbsp;</td>
					<td width="22%"><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Email ID</b></font></td>
					<td width="4%"><font color="#333399"><b>:</b></font></td>
					<td><input name="txtUserName" class="TextBox01" id="txtUserName" size="50" maxlength="200" type="text" onchange="toggle_username('txtUserName')"> <font color="#ff0000"> * (Will be used as Login Name)</font>
					
					<div id="username_exists" style="font-size: 11px;font-weight: bold;color:#FF3300"> </div>					
					</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><p><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Password</b></font></p></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtPassword" class="TextBox01" id="txtPassword" size="25" maxlength="20" type="password" onKeyUp="updatestrength(this.value)"> <font color="#ff0000">*</font> &nbsp;<font color="#333399">Password Strength </font>&nbsp; <img  id="strength" src="/images/password/tooshort.jpg" name="img" alt="" style="visibility:hidden;" />
					</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><p><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Confirm Password</b></font></p></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtPasswordConfirm" class="TextBox01" id="txtPasswordConfirm" size="25" maxlength="20" type="password"> <font color="#ff0000">*</font> </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7">If you forget your account details, Milo will ask you Security Question </b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><select name="txtSecurityQry" class="TextBox01" id="txtSecurityQry" onChange="checkOtherSecurityAns(this)">
						<option value="" SELECTED>--Select One--</option>
						};	               	
						foreach $key(sort {$passwordSecurityQry{$a} cmp $passwordSecurityQry{$b} } keys %passwordSecurityQry){            	
							    print "<OPTION VALUE=\"$key\">$passwordSecurityQry{$key}</OPTION> \n";            	
						}
						print qq{

					  <option value="0">- Type your question here -</option>
					  </select> <font color="#ff0000">*</font>					  
					  </td>
				  </tr>
				  <tr>
					  <td></td>
					  <td><div id="otherSecurityAnsLabel" style="font-size: 11px;font-weight: bold;color:#FF3300"> </td>
					  <td><div id="otherSecurityAnsColon" style="font-size: 11px;font-weight: bold;color:#FF3300"> </td>
					  <td>
					  <div id="otherSecurityAns" style="font-size: 11px;font-weight: bold;color:#FF3300"> </div>
					  </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Your Answer</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtSecurityAns" class="TextBox01" id="txtSecurityAns" size="80" maxlength="500" type="text"> <font color="#ff0000">*</font> </td>
				  </tr>
				  <tr>
				  	<td>&nbsp;</td>
				  	<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> User Type</b></font></td>
				  	<td><font color="#333399"><b>:</b></font></td>
				  	<td><select name="txtUserType" class="TextBox01" id="txtUserType">
						<option value="0" SELECTED>--Select One--</option>
						};	               	
						foreach $key(sort {$roleDetails{$a} cmp $roleDetails{$b} } keys %roleDetails){            	
							    print "<OPTION VALUE=\"$key\">$roleDetails{$key}</OPTION> \n";            	
						}
						print qq{					  
					  </select> <font color="#ff0000">*</font> </td>
				  </tr>
				  <tr>
					<td colspan="2">&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td colspan="4" class="ColrBk03"><b><font color="#0066cc">&nbsp;<strong>Contact Details</strong>...</font></b></td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> First Name</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><select name="txtNameTitle" class="TextBox01" id="txtNameTitle">
						<option value="n" selected="selected">Select Title...</option>
						<option value="Mr.">Mr.</option>
						<option value="Mrs.">Mrs.</option>
						<option value="Ms.">Ms.</option>
						<option value="Dr.">Dr.</option>
						<option value="Prof.">Prof.</option>
						<option value="Rev.">Rev.</option>
						<option value="">Other</option>
					  </select> <input name="txtFirstName" class="TextBox01" id="txtFirstName" size="30" maxlength="100" type="text">
					  <font color="#ff0000">*</font>
					  <select name="txtNameSuffix" class="TextBox01" id="txtNameSuffix">
						<option value="n" selected="selected">Select Suffix...</option>
						<option value="CEO">CEO</option>
						<option value="Director">Director</option>
					  </select>
					  
					  </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Middle Name</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtMiddleName" class="TextBox01" id="txtMiddleName" size="30" maxlength="100" type="text"> <font color="#ff0000"></font> </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Last Name</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtLastName" class="TextBox01" id="txtLastName" size="30" maxlength="100" type="text"> <font color="#ff0000">*</font> </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Facility</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><select name="txtFacility" class="TextBox01" id="txtFacility" onChange="checkOtherFacility(this)">
						<option value="" SELECTED>--Select--</option>
						};	               	
						foreach $key(sort {$hospitalDetails{$a} cmp $hospitalDetails{$b} } keys %hospitalDetails){            	
							    print "<OPTION VALUE=\"$key\">$hospitalDetails{$key}</OPTION> \n";            	
						}
						print qq{

					  <option value="Other">Other</option>
					  </select> <font color="#ff0000"></font>					  
					  </td>
				  </tr>
				  
				  <tr>
				  <td></td>
				  <td><div id="otherFacilityLabel" style="font-size: 11px;font-weight: bold;color:#FF3300"> </div></td>
				  <td><div id="otherFacilityColon" style="font-size: 11px;font-weight: bold;color:#FF3300"> </div></td>
				  <td>
				  <div id="otherFacility" style="font-size: 11px;font-weight: bold;color:#FF3300"> </div>
				  </td>
				  </tr>
				  
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Address</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtAddress1" class="TextBox01" id="txtAddress1" size="60" maxlength="100" type="text"> <font color="#ff0000"></font> </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtAddress2" class="TextBox01" id="txtAddress2" size="60" maxlength="100" type="text"> <font color="#ff0000"></font> </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtAddress3" class="TextBox01" id="txtAddress3" size="60" maxlength="100" type="text"> <font color="#ff0000"></font> </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> City</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtCity" class="TextBox01" id="txtCity" size="30" maxlength="100" type="text"> <font color="#ff0000"></font> </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> State</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><select name="txtState" class="TextBox01" id="txtState">
						<option value="" SELECTED>--Select--</option>
						};	               	
						foreach $key(sort {$statesDetails{$a} cmp $statesDetails{$b} } keys %statesDetails){            	
							    print "<OPTION VALUE=\"$key\">$statesDetails{$key}</OPTION> \n";            	
						}
          					print qq{

					  </select> <font color="#ff0000"></font>					  
					  </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Zip Code</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtZip" class="TextBox01" id="txtZip" size="15" maxlength="10" type="text"> <font color="#ff0000"></font> </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Telephone1</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtTelephone1" class="TextBox01" id="txtTelephone1" size="50" maxlength="10" type="text"> <font color="#ff0000"> (Land Phone, Mobile, Office)</font></td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Telephone2</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtTelephone2" class="TextBox01" id="txtTelephone2" size="50" maxlength="10" type="text"> <font color="#ff0000"> (Land Phone, Mobile, Office)</font></td>
				  </tr>
				  <!-- <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Email ID</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtEmail" class="TextBox01" id="txtEmail" size="50" maxlength="100" type="text"> <font color="#ff0000"></font></td>
				  </tr> -->
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td colspan="4"><table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tbody>
						<tr>
						  <td width="25"><img src="/images/register/pix_trans.gif" width="120" height="5"></td>
						  <td width="25"></td>
						  <td></td>
						</tr>
					  </tbody></table></td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td><input type="hidden" name="saction" value="Register"><input name="cmdNext" class="TextBox01" id="cmdNext" value="Submit" type="submit"> <input name="cmdReset" onclick="/cgi-bin/index.pl" class="TextBox01" id="cmdReset" value="Back" type="button"></td>
				  </tr>
				</tbody></table>
			  </form>
	             </td>
		  </tr>
		</tbody>
	      </table>
          </td>
       </tr>
  </tbody>
 </table>
 </td>
  </tr>
  </table>
  <!--content end-->
  </CENTER>
  </body>  
  </html>
};

#--------------------------------------------------------------------------------------
sub sessionExpire
{

print "Content-Type: text/html\n\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#--------------------------------------------------------------------------------
# - Get details from States table. 

sub getStatesDetails
{
	my ($qry, $stmt, $record);
	
	$qry = "select * from states order by state_name";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();   	
   	
   	my %state;   	
   	
	while ($record = $stmt->fetchrow_hashref) {	
		$state{$$record{state}} = $$record{state_name};		
  	}  	
  	return %state;
}

#--------------------------------------------------------------------------------
# - Get details from Hospital table. 

sub getHospitalDetails
{
	my ($qry, $stmt, $record);
	
	$qry = "select * from Hospitals order by HOSP_NAME";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();   	
   	
   	my %state;   	
   	
	while ($record = $stmt->fetchrow_hashref) {	
		$state{$$record{PROVIDER_NUMBER}} = $$record{HOSP_NAME};		
  	}  	
  	return %state;
}

#--------------------------------------------------------------------------------
# - Get details from password_security_qry table. 

sub getPasswordSecurityQry
{
	my ($qry, $stmt, $record);
	
	$qry = "select * from password_security_qry ";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();   	
   	
   	my %state;   	
   	
	while ($record = $stmt->fetchrow_hashref) {	
		$state{$$record{Qry_ID}} = $$record{Query};		
  	}  	
  	return %state;
}

#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------
# - Get details from Role table. 

sub getRoleDetails
{
	my ($qry, $stmt, $record);
	
	$qry = "select * from role ";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();   	
   	
   	my %role;   	
   	
	while ($record = $stmt->fetchrow_hashref) {	
		$role{$$record{RoleID}} = $$record{RoleName};		
  	}  	
  	return %role;
}