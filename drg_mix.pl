#!/usr/bin/perl

#--------------------------------------------------------------------
# Program: drg_mix.pl
#  Author: Steve Spicer
# Written: Sun Sep 28 17:15:42 CDT 2008
# Revised: 
#   Notes:
# Depends: DBI.pm, CGI.pm
#--------------------------------------------------------------------


use strict;
use DBI;
use CGI;
do "common_header.pl";
  print "Content-Type: text/html\n\n";
&headerScript();
do "pagefooter.pl";

use CGI::Session;

my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");


if(!$userID)
{
	&sessionExpire;
}

my $provider   = $cgi->param('provider');
my $county_cd  = $cgi->param('county_cd');
my $state_cd   = $cgi->param('state_cd');

my $remote_user  = $ENV{"REMOTE_USER"};
my $remote_addr  = $ENV{"REMOTE_ADDR"};

my $healthsouth = "208.68.216.10";

my $start_time = time;

my %ini_value;

unless (&read_inifile("milo.ini")) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{"DB_TYPE"};
my $dblogin    = $ini_value{"DB_LOGIN"};
my $dbpasswd   = $ini_value{"DB_PASSWORD"};
my $dbname     = $ini_value{"DB_NAME"};
my $dbserver   = $ini_value{"DB_SERVER"};

my ($sql, $dbh, $sth,$sql_f,$facility_id,$master_table,$report_table,$data_table,$row);


unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}
my $facility = $cgi->param("facility");
$sql_f = "select facility_id ,master_table,report_table,data_table from tbl_facility_type where facility_name = '$facility'";
$sth = $dbh->prepare($sql_f);
$sth->execute();

while ($row = $sth->fetchrow_hashref) {
	$facility_id = $$row{facility_id};
	$master_table = $$row{master_table};
	$report_table = $$row{report_table};
	$data_table = $$row{data_table};
}
open(J, ">data.txt");
print J $sql_f;
close J;
#----------------------------------------------------------------------------

$sql = qq{
  SELECT th.Name as HOSP_NAME,th.address as STREET_ADDR,
th.City as CITY,th.State as STATE,
th.Zip as ZIP_CODE,th.County as COUNTY 
     FROM $master_table as th
    WHERE th.ProviderNo = ?
};

unless ($sth = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute($provider)) {
   &display_error("ERROR executing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

my ($name, $address, $city, $state, $zip, $county) = $sth->fetchrow_array;

$zip =~ s/-$//;  # crappy data

$sth->finish();

#----------------------------------------------------------------------------

my $and_where = '';
my $subtitle;
my @sql_args;

push @sql_args, $provider;

if ($county_cd and $state_cd) {

   $sql = qq{
   
			SELECT SSA_COUNTY, SSA_STATE
			FROM SSA_COUNTYCODES
			WHERE SSA_STATE_CODE = ? 
			AND SSA_CNTY_CD = ?
		};

   unless ($sth = $dbh->prepare($sql)) {
      &display_error("ERROR preparing SQL query:", $sth->errstr);
      $dbh->disconnect;
      exit(1);
   }

   unless ($sth->execute($state_cd, $county_cd)) {
      &display_error("ERROR executing SQL query:", $sth->errstr);
      $dbh->disconnect;
      exit(1);
   }

   my ($ssa_county, $ssa_state) = $sth->fetchrow_array;

   $sth->finish();

   $and_where = "AND STATE_CD = ? AND CNTY_CD = ?";
   push @sql_args, $state_cd, $county_cd;
   $subtitle = "<FONT COLOR=\"RED\">FOR PATIENTS RESIDING IN $ssa_county COUNTY $ssa_state</FONT><BR>";

}

# ------------Selection of year in combo-box --------------------------

my $dataYear=2009;

my $year=$cgi->param('year');
if($year!=''){
 $dataYear=$year;
}

$sql = qq{
   SELECT DRG_CD, DRG_DESC, MDC, SUM(PMT_AMT) AS AMT, COUNT(*) AS CNT
     FROM LDSM_$dataYear
LEFT JOIN DRG_TABLE ON DRG_CD = DRG_CODE
    WHERE PROVIDER  = ? 
	  AND DRG_YEAR  = $dataYear
      AND SGMT_NUM  = 1 $and_where
 GROUP BY DRG_CD
 ORDER BY AMT DESC
};

unless ($sth = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL query:",$sth->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute(@sql_args)) {
   &display_error("ERROR executing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

my $now = localtime();

my $rows = $sth->rows;

print qq{
<HTML>
<HEAD><TITLE>DRG ANALYSIS BY PROVIDER</TITLE>
<SCRIPT SRC="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<SCRIPT SRC="/JS/dropmenu.js"></SCRIPT>
<link rel="stylesheet" href="/css/dropmenu.css" type="text/css" />
</HEAD>
<BODY>
<CENTER>
};

&innerHeader();
print qq{
<form name='selection'>
 <INPUT TYPE="hidden" NAME="facility" VALUE="$facility"> 
    <!--content start-->
   
        <table cellpadding="0" cellspacing="0" width="885px">
            <tr>
                <td valign="top">
                    <img src="/images/content_left.gif" /></td>
            <td valign="top" style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px">

<Table cellpadding="0" cellspacing="0" width="100%">
	<Tr>
		<Td style="padding-bottom:10px">
				<tr><td><span class="pageheader">DRG ANALYSIS FOR PROVIDER $provider</span>&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp <b>YEAR : </b><font color='#EE0000'>$dataYear</font></td></tr>
				<tr><td><Strong> $subtitle $name</strong></td></tr>
	<tr><td><span  class="tahoma12">$address<BR>$city, $state, $zip</span></td></tr>
		</td>
		
	</tr>
	<tr><td>
	<br></td></tr>
	<tr align="left">
	<td>
	<script>
	function submitYear(value){
		document.selection.submit();
	}
	</script>
	
	<input type="hidden" value=$provider name='provider' />
	DRG MIX : <select  name="year" onChange="submitYear(this.value)">
	<option value=''>Select Year</option>
	<option value=2009> 2009</option>
	<option value=2008> 2008</option>
	<option value=2007> 2007</option>
	<option value=2006> 2006</option>
</select>


</td>
<td align="right" valign="bottom" style="padding-bottom:10px" class="tahoma12">
$now
</td>
</tr>
</form>
</table>


<TABLE CELLPADDING="4" WIDTH="100%" class=\"gridstyle\">
  
  <TR class="gridheader">
    <TD>DRG CODE</TD>
    <TD>MDC CODE</TD>
    <TD>DRG DESCRIPTION</TD>
    <TD>CLAIMS</TD>
    <TD>PAYMENT AMOUNT</TD>
    <TD>PERCENT OF TOTAL</TD>
    <TD>ACCUM PERCENT</TD>
  </TR>
};

my $total_claims = 0;
my $total_amount = 0;

my $count = 0;

my @buffer;

my ($class, $i, $edit_amount, $edit_pct);
my ($drg, $desc, $pmt_amt, $mdc, $claim_count);

while (($drg, $desc, $mdc, $pmt_amt, $claim_count) = $sth->fetchrow_array) {

   $total_claims += $claim_count;
   $total_amount += $pmt_amt;

   push @buffer, [$drg, $desc, $mdc, $pmt_amt, $claim_count];
}


my $pct;
my $cumulative_pct = 0;
my $edit_cumulative;
my $avg_claim;

my $limit = @buffer;

my $link;

for ($i=0;$i<$limit;++$i) {

   ($drg, $desc, $mdc, $pmt_amt, $claim_count) = @{$buffer[$i]};

   $link = "<A HREF=\"/cgi-bin/dischg.pl?provider=$provider&drg=$drg&year=$dataYear&facility=$facility\" TITLE=\"DISCHARGE SUMMARY\">$claim_count</A>";
  
#   unless ($remote_addr eq $healthsouth or $remote_user eq "spicer") {
#      $link = $claim_count;
#   }

   $edit_amount = &comify($pmt_amt);

   $pct = $pmt_amt / $total_amount * 100;

   $edit_pct = sprintf ("%.02f", $pct);

   $cumulative_pct += $pct;

   $edit_cumulative = sprintf ("%.02f", $cumulative_pct);

   $class = (++$count % 2) ? 'gridrow' : 'gridalternate';

   $avg_claim = &comify(sprintf("%.0f", $pmt_amt / $claim_count));

   print "   <TR CLASS=\"$class\">\n";
   print "      <TD ALIGN=\"center\">$drg</TD>\n";
   print "      <TD ALIGN=\"center\">$mdc</TD>\n";
   print "      <TD ALIGN=\"left\">$desc</TD>\n";
   print "      <TD ALIGN=\"right\">$link</TD>\n";
   print "      <TD ALIGN=\"right\"><div title=\"AVG: $avg_claim\">$edit_amount</div></TD>\n";
   print "      <TD ALIGN=\"right\">$edit_pct &#37;</TD>\n";
   print "      <TD ALIGN=\"right\">$edit_cumulative &#37;</TD>\n";
   print "   </TR>\n";
}


my $edit_count = &comify($total_claims);
   $edit_amount = &comify(sprintf("%.02f",$total_amount));

$class = (++$count % 2) ? "gridalternate" : "gridrow";

print "   <TR CLASS=\"$class\">\n";
print "      <TD ALIGN=\"right\" COLSPAN=\"3\"><B>TOTAL:</B></TD>\n";
print "      <TD ALIGN=\"right\"><B>$edit_count</B></TD>\n";
print "      <TD ALIGN=\"right\"><B>$edit_amount</B></TD>\n";
print "      <TD ALIGN=\"left\">&nbsp;</TD>\n";
print "      <TD ALIGN=\"left\">&nbsp;</TD>\n";
print "   </TR>\n";

my $elapsed_time = time - $start_time;

print qq{
</TABLE>
<span class="tahoma12" >$count ROWS IN $elapsed_time SECONDS</span>

</td>
            <td valign="top">
                <img src="/images/content_right.gif" /></td>
        </tr>
    </table>
    <!--content end-->
	};
&TDdoc;
print qq{   
    </CENTER>
</BODY>
</HTML>
};

$sth->finish;
$dbh->disconnect;
   
exit(0);


#------------------------------------------------------------------------------

sub display_error
{
   my $s;
   print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE></HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>

<FONT SIZE="5" COLOR="RED">
<B>ERRORS</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" BORDER="1" CELLPADDING="20" WIDTH="600">
<TR><TD>
<UL>
};
   foreach $s (@_) {
      print "$s<BR>";
   }
   print qq{
</TD></TR>
</TABLE>

<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</FONT>
</CENTER>
</BODY>
</HTML>
};
}

#----------------------------------------------------------------------------
sub comify
{
   local($_) = shift;
   1 while s/^(-?\d+)(\d{3})/$1,$2/;
   return($_);
}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#--------------------------------------------------------------------------------------
sub sessionExpire
{

print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

#------------------------------------------------------------------------------




