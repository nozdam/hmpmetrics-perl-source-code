#!/usr/bin/perl

use CGI;
use DBI;
do "common_header.pl";
print "Content-Type: text/html\n\n";
&headerScript();
my $cgi = new CGI;
do "pagefooter.pl";
use CGI::Session;
do "audit.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $orderBy = $cgi->param("orderBy");
my $facility = $cgi->param("facility");
if(!$orderBy){
	$orderBy = "Name";
}
if(!$userID)
{
	&sessionExpire;
}

#&view("view","","Exceptions");

my $text = $cgi->param('providers');
my $checkedYear = $cgi->param('yearCheck');
my $datasetIds = $cgi->param('datasetIds');
my $dataElementIds = $cgi->param('dataElementIds');

$datasetIds = substr($datasetIds, 1);
$dataElementIds = substr($dataElementIds, 1);

# Database 

my $dbh;
my ($sql, $sth);


# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my $defaultYear= $ini_value{'DEFAULT_YEAR'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

$sql = "select ID,Name,CreatedOn,ModifiedOn,(select concat(First_Name, ' ',Last_Name) as CreatedBy from userdetails where UsersID = exceptions.CreatedBy) as CreatedBy ,(select concat(First_Name, ' ',Last_Name) as ModifiedBy from userdetails where UsersID = exceptions.ModifiedBy) as ModifiedBy, CancelYN from exceptions order by $orderBy";
$sth = $dbh->prepare($sql);
$sth->execute();

my $divHeight = 0;
my $rows_returned = $sth->rows();

if($rows_returned<200){
  $divHeight = ($rows_returned * 50) + "px";
}else{
  $divHeight = "200px";
}



print <<EOF
<HTML>
<HEAD><TITLE>Exceptions</TITLE>
<link href="/css/admin_panel/Exceptions/ExceptionMain.css" rel="stylesheet" type="text/css" />	
 <script language="javascript" src="/JS/admin_panel/exceptions/ExceptionMain.js" ></script>
</HEAD>
<BODY onload="ShowDetails()">
<FORM NAME = "frmExceptionsMain" Action="/cgi-bin/PeerActions.pl?facility=$facility" method="post">
<CENTER>
EOF
;
&innerHeader();
print <<EOF
<div id='bodyDiv' style="position:absolute;z-index:100;display:none;background-image:url(/images/background-trans.png);height:100%;width:100%;"><div align='center'  id="displaybox" style="display: none;"></div></div>

    <!--content start-->
    
      <table cellpadding="0" cellspacing="0" width="885px">
        <tr>
            <td valign="top">
                <img src="/images/content_left.gif" /></td>
            <td valign="top" style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; ">            
		
		<table width="100%">
		  <tr>
			<td><table bgColor=\"#B4CDCD\" width=\"100%\" align=\"right\"><tr><td width=\"55%\"><b>Exceptions :</b></td><td><select onchange=\"SortBy(1)\" name=\"SortMain\" id=\"SortMain\"><option value=\"Name\">Sort By</option><option value=\"Name\">Name</option><option value=\"CreatedOn\">Created Date</option><option value=\"ModifiedOn\">Modified Date</option><option value=\"CreatedBy\">Created By</option><option value=\"ModifiedBy\">Modified By</option></select></td><td><span  class=\"button\"><INPUT TYPE=\"button\" VALUE=\" Create \" onClick=\"CallCreateException()\"></span></td><td><span  class=\"button\"><INPUT TYPE=\"button\" VALUE=\" Edit \" onClick=\"CallEditException()\"></span></td><td><span  class=\"button\"><INPUT TYPE=\"button\" VALUE=\" Delete \" onclick=\"delRep()\"></span></td></tr></table></td>
		  </tr>
		  <tr>
   			<td>
   				<div style="position:static;overflow:scroll;height:150px;">
				<TABLE class="gridstyle" align='center' CELLPADDING="0" CELLSPACING="0" width="100%">
					
					<tr class='gridHeader'>
						<td>SlNo</td><td>Name</td><td>Created On</td><td>Created By</td><td>Modified On</td><td>Modified By</td><td>select</td>
					</tr>
EOF
;
				my $i = 0;		
				while ($row1 = $sth->fetchrow_hashref) {
					$Excp_id = $$row1{ID};
					$Excp_name = $$row1{Name};
					$rpt_status = $$row1{CancelYN};
					$rpt_createdate = $$row1{CreatedOn};
					$rpt_createdby = $$row1{CreatedBy};					
					$rpt_alterdate = $$row1{ModifiedOn};
					$rpt_modifiedby = $$row1{ModifiedBy};
					$class1 = (++$i % 2) ? 'gridrow' : 'gridalternate';					
					if($i == 1){
						print "<tr class=\"$class1\"><td>$i</td><td><a href=\"/cgi-bin/ExceptionDetails.pl?facility=$facility&reload=false&mainID=$Excp_id&dtName=$Excp_name\" target=\"Exceptionsframe\" onclick=\"CheckIt($i)\">$Excp_name</a></td><td>$rpt_createdate</td><td>$rpt_createdby</td><td>$rpt_alterdate</td><td>$rpt_modifiedby</td><td><input id=\"chk$i\" onclick=\"SetReady(this,$i,$Excp_id,'$Excp_name')\" type=\"checkbox\" name=\"delExceptions\" value=\"$Excp_id\" checked></td></tr>\n"; 	
					}else{
						print "<tr class=\"$class1\"><td>$i</td><td><a href=\"/cgi-bin/ExceptionDetails.pl?facility=$facility&reload=false&mainID=$Excp_id&dtName=$Excp_name\" target=\"Exceptionsframe\" onclick=\"CheckIt($i)\">$Excp_name</a></td><td>$rpt_createdate</td><td>$rpt_createdby</td><td>$rpt_alterdate</td><td>$rpt_modifiedby</td><td><input id=\"chk$i\" onclick=\"SetReady(this,$i,$Excp_id,'$Excp_name')\" type=\"checkbox\" name=\"delExceptions\" value=\"$Excp_id\"></td></tr>\n"; 	
					}	
					if($i == 1){
					   	print "<input type=\"hidden\" name=\"firstid\" value=\"$Excp_id\">";
					   	print "<input type=\"hidden\" name=\"firstName\" value=\"$Excp_name\">";
  					}
				}

					
print "</TABLE>\n";
print "</div>\n";
print "</td>\n";
print " </tr>\n";
print "<tr>\n";
print "<td><table bgColor=\"#B4CDCD\" width=\"100%\" align=\"right\"><tr><td width=\"55%\"><span id=\"dtLabel\" style=\"font-weight:bold;\">Details of : </span><span id=\"dtName\"></span></td><td align=\"left\"><select style=\"visibility:hidden;\" onchange=\"SortBy(2)\" name=\"SortDetail\"><option value=\"\">Sort By</option><option value=\"Formula\">Formula</option><option value=\"Operator\">Operator</option><option value=\"Sign\">Sign</option></select></td><td><span  class=\"button\"><INPUT TYPE=\"button\" id=\"btnCancel\" VALUE=\"Cancel\" onclick=\"CallCancel()\" ></span></td><td><span  class=\"button\"><INPUT TYPE=\"button\" id=\"btnSave\" VALUE=\"Save\" onClick=\"CallSave(1)\" disabled></span></td><td><span  class=\"button\"><INPUT TYPE=\"button\" id=\"btnAddMore\" VALUE=\"Add Columns\" onClick=\"CallSave(2)\" disabled></span></td><td></td></tr></table></td>\n";
print "</tr>\n";
print "<tr>\n";
print "<td valign=\"top\">\n";
print "<iframe name=\"Exceptionsframe\"  width=\"100%\" height=\"230px\" frameborder=0 cellpadding=0 cellspacing=0 ></iframe>\n";
print "</td>\n";
print "</tr>\n";
print "</table>\n";				
print "</td>\n";
print "<td valign=\"top\">\n";
print "<img src=\"/images/content_right.gif\" />\n";
print "</td>\n";
print "</tr>\n";
print "</table>\n";
print "</CENTER><!--content end-->\n";
&TDdoc;
print "<!-- ExceptionDetails on body load will set 'Excp_mainid' value used for Edit functionality-->\n";
print "<input type=\"hidden\" name=\"Excp_mainid\">\n";
print "<input type=\"hidden\" name=\"Excp_name\">\n";
print "<input type=\"hidden\" name=\"saction\">\n";
	print "<input type=\"hidden\" name=\"facility\" value=\"$facility\">";
print "<input type=\"hidden\" name=\"orderBy\" value=\"$orderBy\">\n";
print "</FORM>\n";
print "</BODY>\n";
print "</HTML>\n";    
   
#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#------------------------------------------------------------------------------
sub get_value
{
   my ($Exception_id, $worksheet, $line, $column, $fYear) = @_;

   my $yyyy = $fYear;

   my ($sthVal, $value);

   my $sql = qq(
    SELECT CR_VALUE         
      FROM CR_ALL_DATA
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
   );

   unless ($sthVal = $dbh->prepare($sql)) {
       &display_error('Error preparing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sthVal->execute($Exception_id, $worksheet, $line, $column)) {
       &display_error('Error executing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }
   
   if ($sthVal->rows) {
      $value = $sthVal->fetchrow_array;
   }

   $sthVal->finish();

   return $value;
}
#--------------------------------------------------------------------------------------

sub sessionExpire
{
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}


print qq{
<HTML>
<HEAD><TITLE>Exceptions-ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
<BR>
<BR>
};
&innerHeader();
print qq{
    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}
    