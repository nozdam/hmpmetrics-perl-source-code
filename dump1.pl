#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: dump.pl
#----------------------------------------------------------------------------

use strict;
use CGI;

my $cgi = new CGI;

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>DUMP</TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css" />
</HEAD>
<BODY>
<BR>
<BR>
<FONT SIZE="5" COLOR="RED">
<B>CGI PARAM DUMPER</B><BR>
</FONT>
<BR>
<PRE>
};


my @names = $cgi->param;

my %hash;

my ($i, $name, $value, $wrk, $row, $col, $fmt, $des);

my $max = 0;

foreach $name (@names) {
    $value = $cgi->param("$name");

    if ($name =~ m/^([A-Z]+)-(\d+)$/) {
       ${$hash{$1}}[$2] = $value;
       $max = $2 if ($2 > $max);
    }
    else {
       print "$name = $value\n";      
    }
}

for ($i = 1; $i < $max; ++$i)  {
    $wrk = ${$hash{'WRK'}}[$i]; 
    $row = ${$hash{'ROW'}}[$i]; 
    $col = ${$hash{'COL'}}[$i]; 
    $fmt = ${$hash{'FMT'}}[$i]; 
    $des = ${$hash{'DES'}}[$i]; 
    print "$wrk|$row|$col|$fmt|$des\n";
}


print qq{
</PRE>
<FORM ACTION="#">
<INPUT TYPE="button" VALUE=" CLOSE " onclick="window.close()">
</FORM>
</BODY>
</HTML>
};

exit 0;

