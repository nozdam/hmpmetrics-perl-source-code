#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: cr_explore.pl
#  author: Steve Spicer
#  Written: Wed Oct 24 20:14:32 CDT 2007
#
#  Research Data Assistance Center
#  http://www.resdac.umn.edu/Tools/TBs/TN-005-revised.asp
#----------------------------------------------------------------------------

use strict;
use CGI;
use DBI;
do "pagefooter.pl";
use CGI::Session;
do "audit.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $prov = $session->param("str");
my $rpt_no = $session->param("sstr");

# if(!$userID)
# {
	# &sessionExpire;
# }
&view("View","Cost report details","Cost Report");
my $action    = $cgi->param('action');  
#my $dataset   = $cgi->param('dataset'); 
my $worksheet = $cgi->param('worksheet');
my $reportid  = $cgi->param('reportid');
my $facility   = $cgi->param('facility');

my $provider  = $cgi->param('provider');
my %ini_value;

my %status_lookup = (
 1 => 'As Submitted',
 2 => 'Settled w/o Audit',
 3 => 'Settled with Audit',
 4 => 'Reopened',
 5 => 'Amended',
);

unless ($reportid) {
   &display_error('REPORT ID NOT SPECIFIED');
   exit(0);
}

my $start_time = time;

unless (&read_inifile('milo.ini')) {
   &display_error('CAN\'T READ milo.ini');
   exit(1);
}

#----------------------------------------------------------------------------

open INPUT, "wk_xwalk.txt";

my ($line, $key, $string, %hash);
   
while ($line = <INPUT>) {
   chomp($line);
    ($key, $string) = split / /,$line,2;
    $hash{$key} = $string;
}
   
close INPUT;

my %table_def;

opendir(DIR,'./definitions');
my @dir = readdir(DIR);
closedir(DIR);

foreach $string (@dir) {
   if ($string =~ m/^(\S{7})\.def$/) {   # look for def files
      $table_def{$1} = 'Y';
   }
}



#----------------------------------------------------------------------------

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my ($dbh, $sql, $sth);

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

my ($rpt_status, $rpt_year, $rpt_provider, $rpt_stus_cd, $fi_num, $fy_bgn_dt, $fy_end_dt);
if($facility eq 'HOSPITAL'){
$sql = qq{
   SELECT RPT_YEAR, PRVDR_NUM, RPT_STUS_CD, FI_NUM,
          DATE_FORMAT(FY_BGN_DT, '%m-%d-%Y'),
          DATE_FORMAT(FY_END_DT, '%m-%d-%Y')
     FROM CR_ALL_RPT
    WHERE RPT_REC_NUM = ?
};
}

if($facility eq 'RENAL'){
$sql = qq{
   SELECT RPT_YEAR, PRVDR_NUM, RPT_STUS_CD, FI_NUM,
          DATE_FORMAT(FY_BGN_DT, '%m-%d-%Y'),
          DATE_FORMAT(FY_END_DT, '%m-%d-%Y')
     FROM rnl_ALL_RPT
    WHERE RPT_REC_NUM = ?
};
}
if($facility eq 'HHA'){
$sql = qq{
   SELECT RPT_YEAR, PRVDR_NUM, RPT_STUS_CD, FI_NUM,
          DATE_FORMAT(FY_BGN_DT, '%m-%d-%Y'),
          DATE_FORMAT(FY_END_DT, '%m-%d-%Y')
     FROM hha_ALL_RPT
    WHERE RPT_REC_NUM = ?
};
}
if($facility eq 'SNF'){
$sql = qq{
   SELECT RPT_YEAR, PRVDR_NUM, RPT_STUS_CD, FI_NUM,
          DATE_FORMAT(FY_BGN_DT, '%m-%d-%Y'),
          DATE_FORMAT(FY_END_DT, '%m-%d-%Y')
     FROM snf_ALL_RPT
    WHERE RPT_REC_NUM = ?
};
}
if($facility eq 'HOSPICE'){
$sql = qq{
   SELECT RPT_YEAR, PRVDR_NUM, RPT_STUS_CD, FI_NUM,
          DATE_FORMAT(FY_BGN_DT, '%m-%d-%Y'),
          DATE_FORMAT(FY_END_DT, '%m-%d-%Y')
     FROM hospc_ALL_RPT
    WHERE RPT_REC_NUM = ?
};
}
unless ($sth = $dbh->prepare($sql)) {
   &display_error("Error preparing SQL query", $sth->errstr);
   $dbh->disconnect();
   exit(0);
}

unless ($sth->execute($reportid)) {
   &display_error("Error executing SQL query", $sth->errstr);
   $dbh->disconnect();
   exit(0);
}

($rpt_year, $rpt_provider, $rpt_stus_cd, $fi_num, $fy_bgn_dt, $fy_end_dt) = $sth->fetchrow_array;

$rpt_status = $status_lookup{$rpt_stus_cd};

$sth->finish();

my ($hospital_name, $hospital_address, $hospital_city, $hospital_state, $hospital_zip);

if($facility eq 'HOSPITAL'){
$hospital_name    = &get_value_by_addr($rpt_year, $reportid, 'S200000:00200:0100');
$hospital_address = &get_value_by_addr($rpt_year, $reportid, 'S200000:00100:0100');
$hospital_city    = &get_value_by_addr($rpt_year, $reportid, 'S200000:00101:0100');
$hospital_state   = &get_value_by_addr($rpt_year, $reportid, 'S200000:00101:0200');
$hospital_zip     = &get_value_by_addr($rpt_year, $reportid, 'S200000:00101:0300');
}
if($facility eq 'HOSPICE'){
$sql = qq{
    SELECT hp.Name as HOSP_NAME,hp.Street as STREET_ADDR,
hp.City as CITY,hp.State as STATE,hp.Zip as ZIP_CODE
,hp.County as COUNTY 
 FROM tblhospiceproviders  as hp
 WHERE ProviderNo = ?
};
unless ($sth = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute($provider)) {
   &display_error("ERROR executing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}
($hospital_name, $hospital_address, $hospital_city, $hospital_state, $hospital_zip) = $sth->fetchrow_array;
}
if($facility eq 'SNF'){
$sql = qq{
    SELECT sn.Name as SNF_NAME,sn.Street as STREET_ADDR,
sn.City as CITY,sn.State as STATE,sn.Zip as ZIP_CODE
,sn.County as COUNTY 
 FROM tblsnfs  as sn
 WHERE ProviderNo = ?
};
unless ($sth = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute($provider)) {
   &display_error("ERROR executing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}
($hospital_name, $hospital_address, $hospital_city, $hospital_state, $hospital_zip) = $sth->fetchrow_array;
}
if($facility eq 'HHA'){
$sql = qq{
    SELECT ha.Name as HHA_NAME,ha.Street as STREET_ADDR,
ha.City as CITY,ha.State as STATE,ha.Zip as ZIP_CODE
 FROM tblhomehealthagencies  as ha
 WHERE ProviderNo = ?
};
unless ($sth = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute($provider)) {
   &display_error("ERROR executing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}
($hospital_name, $hospital_address, $hospital_city, $hospital_state, $hospital_zip) = $sth->fetchrow_array;
}
if($facility eq 'RENAL'){
$sql = qq{
 SELECT rn.Name as RNL_NAME,rn.Street as STREET_ADDR,
rn.City as CITY,rn.State as STATE,rn.Zip as ZIP_CODE
,rn.County as COUNTY 
 FROM tblrenalproviders  as rn
 WHERE ProviderNo = ?
};
unless ($sth = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute($provider)) {
   &display_error("ERROR executing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}
($hospital_name, $hospital_address, $hospital_city, $hospital_state, $hospital_zip) = $sth->fetchrow_array;
}
if ($worksheet) {
   &explore_worksheet($reportid, $worksheet);
} else {
   &explore_report($reportid);
}

$dbh->disconnect();

exit(0);

#----------------------------------------------------------------------------

sub explore_report
{
   my ($reportid) = @_;
#AND RPT_YEAR		= $rpt_year
   my ($worksheet, $value_count, $hover, $desc, $other, $link, $class);

   my $count = 0;

   my ($sth, $sql);
  if($facility eq 'HOSPITAL'){ 
    $sql = qq/
    SELECT CR_WORKSHEET, COUNT(*)
      FROM CR_ALL_DATA
     WHERE CR_REC_NUM  = ?	 
  GROUP BY CR_WORKSHEET
   /;
   }
if($facility eq 'RENAL'){
   $sql = qq/
    SELECT CR_WORKSHEET, COUNT(*)
      FROM rnl_ALL_DATA
     WHERE CR_REC_NUM  = ?	 
  GROUP BY CR_WORKSHEET
   /;
   }
   if($facility eq 'HHA'){
   $sql = qq/
    SELECT CR_WORKSHEET, COUNT(*)
      FROM hha_ALL_DATA
     WHERE CR_REC_NUM  = ?	 
  GROUP BY CR_WORKSHEET
   /;
   }
     if($facility eq 'SNF'){
   $sql = qq/
    SELECT CR_WORKSHEET, COUNT(*)
      FROM snf_ALL_DATA
     WHERE CR_REC_NUM  = ?	 
  GROUP BY CR_WORKSHEET
   /;
   }
	 if($facility eq 'HOSPICE'){
   $sql = qq/
    SELECT CR_WORKSHEET, COUNT(*)
      FROM hospc_ALL_DATA
     WHERE CR_REC_NUM  = ?	 
  GROUP BY CR_WORKSHEET
   /;
   }
   unless ($sth = $dbh->prepare($sql)) {
       &display_error('Error preparing SQL query: ', $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sth->execute($reportid)) {
       &display_error('Error executing SQL query: ', $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sth->rows()) {
     &display_error("NOTHING AVAILABLE FOR REPORTID: $reportid YEAR: $rpt_year");
     return(0);
   }
   &print_page_top();
   print qq{
<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0" WIDTH="885px" class="gridstyle">
   <TR CLASS="gridheader">
     <TD ALIGN="CENTER"><B>LINK</B></TD>
     <TD ALIGN="CENTER"><B>WORKSHEET</B></TD>
     <TD ALIGN="LEFT"><B>DESCRIPTION</B></TD>
     <TD ALIGN="RIGHT"><B>VALUES</B></TD>
     <TD ALIGN="CENTER"><B>ALTERNATE<BR>VIEW</B></TD>
   </TR>
   };

   my ($xlink, $xlsfile);

   while (($worksheet, $value_count) = $sth->fetchrow_array) {

      $desc = $hash{$worksheet};

      $xlsfile = sprintf ("255296_%s.XLS",substr($worksheet,0,1));

      $xlink = "<A HREF=\"/$xlsfile\" TARGET=\"_blank\" TITLE=\"CLICK FOR A SAMPLE BLANK REPORT FROM THIS SECTION IN EXCEL FORMAT\"><IMG BORDER=\"0\" SRC=\"/icons/excel_icon.gif\"></A>";

      $link = "<A HREF=\"/cgi-bin/cr_explore2.pl?facility=$facility&reportid=$reportid&provider=$provider&worksheet=$worksheet\">$value_count</A>";

      $desc = '&nbsp;' unless ($desc);

      $class = (++$count % 2) ? 'gridrow' : 'gridalternate';

      if ($table_def{$worksheet} eq 'Y') {
         $other = "<A HREF=\"/cgi-bin/cr_table2.pl?facility=$facility&reportid=$reportid&provider=$provider&worksheet=$worksheet\"><IMG BORDER=\"0\" SRC=\"/icons/table_icon.jpg\"></A>";
      } else {
         $other = '';
      }

      print "   <TR CLASS=\"$class\">\n";
      print "      <TD ALIGN=\"CENTER\">$xlink</TD>\n";
      print "      <TD ALIGN=\"CENTER\">$worksheet</TD>\n";
      print "      <TD ALIGN=\"LEFT\">$desc</TD>\n";
      print "      <TD ALIGN=\"RIGHT\">$link</TD>\n";
      print "      <TD ALIGN=\"CENTER\">$other</TD>\n";
      print "   </TR>\n";
   }

   print qq{
</TABLE>
<FONT SIZE="1">
$count ROWS<BR>
</FONT>
};
&TDdoc;
print qq{
</CENTER>
</BODY>
</HTML>
   };

   return(0);
}

#----------------------------------------------------------------------------

sub explore_worksheet
{
   my ($reportid, $worksheet) = @_;
#AND RPT_YEAR		= $rpt_year
   my ($address, $row, $col, $value, $class, $align, $editlink);

   my $desc = $hash{$worksheet};

   my $count = 0;

   my ($sth, $sql);
	  if($facility eq 'HOSPITAL'){ 
	   $sql = qq/
       SELECT CR_LINE, CR_COLUMN, CR_VALUE, KB_DESC
         FROM CR_ALL_DATA
    LEFT JOIN KB_TABLE ON KB_ADDRESS = CONCAT(CR_WORKSHEET, ':', CR_LINE, ':', CR_COLUMN)
        WHERE CR_REC_NUM    = ?
          AND CR_WORKSHEET  = ?
		  
    /;
	  
	  }
	  
   if($facility eq 'RENAL'){
   $sql = qq/
       SELECT CR_LINE, CR_COLUMN, CR_VALUE, KB_DESC
         FROM rnl_ALL_DATA
    LEFT JOIN KB_TABLE ON KB_ADDRESS = CONCAT(CR_WORKSHEET, ':', CR_LINE, ':', CR_COLUMN)
        WHERE CR_REC_NUM    = ?
          AND CR_WORKSHEET  = ?
		  
    /;
}

 if($facility eq 'HHA'){
   $sql = qq/
       SELECT CR_LINE, CR_COLUMN, CR_VALUE, KB_DESC
         FROM hha_ALL_DATA
    LEFT JOIN KB_TABLE ON KB_ADDRESS = CONCAT(CR_WORKSHEET, ':', CR_LINE, ':', CR_COLUMN)
        WHERE CR_REC_NUM    = ?
          AND CR_WORKSHEET  = ?
		  
    /;
}

 if($facility eq 'SNF'){
   $sql = qq/
       SELECT CR_LINE, CR_COLUMN, CR_VALUE, KB_DESC
         FROM snf_ALL_DATA
    LEFT JOIN KB_TABLE ON KB_ADDRESS = CONCAT(CR_WORKSHEET, ':', CR_LINE, ':', CR_COLUMN)
        WHERE CR_REC_NUM    = ?
          AND CR_WORKSHEET  = ?
		  
    /;
}
if($facility eq 'HOSPICE'){
   $sql = qq/
       SELECT CR_LINE, CR_COLUMN, CR_VALUE, KB_DESC
         FROM hospc_ALL_DATA
    LEFT JOIN KB_TABLE ON KB_ADDRESS = CONCAT(CR_WORKSHEET, ':', CR_LINE, ':', CR_COLUMN)
        WHERE CR_REC_NUM    = ?
          AND CR_WORKSHEET  = ?
		  
    /;
}
   unless ($sth = $dbh->prepare($sql)) {
      &display_error("Error preparing sql query", $sth->errstr);
      $dbh->disconnect();
      exit(0);
   }

   unless ($sth->execute($reportid, $worksheet)) {
       &display_error("Error preparing sql query", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sth->rows()) {
     &display_error("NOTHING AVAILABLE FOR REPORTID: $reportid WORKSHEET: $worksheet");
     return(0);
   }

   &print_page_top();

   print qq{
<BR>
<TABLE BORDER="0" CELLPADDING="6" CELLSPACING="0" WIDTH="885px" class="gridstyle">
   <TR CLASS="gridheader">
     <TD ALIGN="CENTER"><B>ADDRESS</B></TD>
     <TD ALIGN="CENTER"><B>DESCRIPTION IF POPULATED</B></TD>
     <TD ALIGN="CENTER"><B>LINE</B></TD>
     <TD ALIGN="CENTER"><B>COLUMN</B></TD>
     <TD ALIGN="CENTER"><B>VALUE</B></TD>
   </TR>
   };

   while (($row, $col, $value, $desc) = $sth->fetchrow_array) {

      $address = "$worksheet:$row:$col";

      if ($row =~ m/^(\d\d\d)(\d\d)/) {
         $row = "$1.$2";
         $row =~ s/^0+//;
         $row =~ s/\.0+$//;
         $row = '0' if ($row eq '');
      }

      if ($col =~ m/^(\d\d)(\d\d)/) {
         $col = "$1.$2";
         $col =~ s/\.0+$//;
         $col =~ s/^0+//;
         $col = '0' if ($col eq '');
      }

      $align = 'LEFT';

      if ($value =~ m/^-?\d+$|-?\d*\.\d+$/) {
         $align = 'RIGHT';
#        $value = &comify($value);
      }

      $editlink = "<A HREF=\"#\" onClick=\"MyWindow=window.open('/cgi-bin/kb_update.pl?address=$address','MyWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=600,height=450,left=25,top=25'); return false;\">$address</A>";

      $class = (++$count % 2) ? 'gridrow' : 'gridalternate';

      print "   <TR CLASS=\"$class\">\n";
      print "      <TD ALIGN=\"CENTER\">$editlink</TD>\n";
      print "      <TD ALIGN=\"LEFT\">$desc</TD>\n";
      print "      <TD ALIGN=\"CENTER\">$row</TD>\n";
      print "      <TD ALIGN=\"CENTER\">$col</TD>\n";
      print "      <TD ALIGN=\"$align\">$value</TD>\n";
      print "   </TR>\n";
   }

   print qq{
</TABLE>
<FONT SIZE="1">
$count ROWS<BR>
</FONT>
};
&TDdoc;
print qq{
</CENTER>
</BODY>
</HTML>
   };

   return(0);
}
#window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';
#----------------------------------------------------------------------------
sub CR_dropdown
{
#print "Content-Type: text/html\n\n";
my $rpt= substr $prov,1;
my @rptprov = split(/,/, $rpt);
my $rpt_list =	substr $rpt_no,1;
my @rpt_id = split(/,/, $rpt_list);
print qq{

<script>
function opt_select(){
var opt=document.frmcrexplorer.CR.options[document.frmcrexplorer.CR.selectedIndex].value;
window.parent.location.href="/cgi-bin/cr_explore2.pl?facility=$facility&reportid="+opt+"&provider=$provider&action=go";
}
</script>


<form name="frmcrexplorer">
<table align="left">
<tr>
<td>
<strong>Cost Reports </strong>
	<select name="CR" title="CHOOSE A COST REPORT" onchange="opt_select()">	
	<option >Select CR end date</option>
	};
	
		for(my $i=0;$i< scalar(@rptprov);$i++){
			print "<option name=\"Report\" value=".@rpt_id[$i].">".@rptprov[$i]."</option>";
		}
	

	
	print qq{
	</select>
	</td>
	</tr>
	</table>
	</form>
	};		
}
#-------------------------------------------------------------------------------
sub print_page_top
{
  
   $worksheet = 'ALL' unless ($worksheet);

   print "Content-Type: text/html\n\n";

   print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
<HEAD><TITLE>COST REPORT EXPLORER</TITLE>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon">
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
<link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />
</HEAD>
<BODY>
<CENTER>
<BR>
<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/cgi-bin/demo_index.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/demo_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>Standard Reports</div>">
			        <img src="/images/stardardreport_inner_icon.gif" onmouseover="src='/images/stardardreport_inner_icon_hover.gif';"
                                    onmouseout="src='/images/stardardreport_inner_icon.gif';" style="border: 0px; cursor: pointer" /></a></td>
                <td style="width: 102px" align="Center">
		                            <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
		                                <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->
    <!--content start-->
    
    <table cellpadding="0" cellspacing="0" width="885px">
        <tr><td align="left">Welcome <b>
            	};
            	
            	print $firstName . " " . $lastName;
            	print qq{
            	</b></td>  
            	<td align="right">
            	<a href="#">Sign Out</a>
            	</td>
        	</tr>
    </table>
    
        <table cellpadding="0" cellspacing="0" width="885px">
            <tr>
                <!--<td valign="top">
                    <img src="/images/content_left.gif" /></td>-->
            <td valign="top" style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px">
<TABLE BORDER="0" CELLPADDING="2" WIDTH="100%">
   <TR>
     <TD ALIGN="LEFT" VALIGN="TOP">
       
        <Div class="pageheader"> $hospital_name</div>
      <span class="tahoma12">
        $hospital_address<BR>
        $hospital_city, $hospital_state  $hospital_zip<BR><BR>
        </B></span>
       
     </TD>
     <TD ALIGN="left" VALIGN="TOP" class="tahoma12" style="border-left:dotted 1px #ffffff; padding-left:10px;">
       WORKSHEET:<BR>
        PROVIDER:<BR>
        FY BEGIN:<BR>
          FY END:<BR>
          STATUS:<BR>
              FI:<BR>
     </TD>
     <TD ALIGN="LEFT" VALIGN="TOP" class="tahoma12" style="border-right:dotted 1px #ffffff; padding-right:10px">
        <B>
        <FONT COLOR="#EE0000">
        $worksheet<BR>
        $rpt_provider<BR>
        $fy_bgn_dt<BR>
        $fy_end_dt<BR>
        $rpt_status<BR>
        $fi_num<BR>
        </FONT>
        </B>
     </TD>
     <TD ALIGN="RIGHT" VALIGN="TOP">
        <IMG SRC="/icons/CMSLogo.gif">
     </TD>
   </TR>
   
   <tr>
   <td>};
   &CR_dropdown;
   print qq{
   </td>
   </tr>
</TABLE>
</td>
            <!--<td valign="top">
                <img src="/images/content_right.gif" /></td>-->
        </tr>
    </table>
    <!--content end-->
};
}

#----------------------------------------------------------------------------	

sub get_value_by_addr
{
   my ($year, $reportid, $address) = @_;
# AND RPT_YEAR		= $year
   my ($worksheet, $row, $col) = split /[-:\|]/,$address;

   my ($sth, $sql);
     if($facility eq 'HOSPITAL'){ 
    $sql = qq/
    SELECT CR_VALUE         
      FROM CR_ALL_DATA 
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
	  
   /;
   
   }
   if($facility eq 'RENAL'){
   $sql = qq/
    SELECT CR_VALUE         
      FROM rnl_ALL_DATA 
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
	  
   /;
}

   if($facility eq 'HHA'){
   $sql = qq/
    SELECT CR_VALUE         
      FROM hha_ALL_DATA
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
	  
   /;
}

if($facility eq 'SNF'){
   $sql = qq/
    SELECT CR_VALUE         
      FROM snf_ALL_DATA
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
	  
   /;
}

if($facility eq 'HOSPICE'){
   $sql = qq/
    SELECT CR_VALUE         
      FROM hospc_ALL_DATA
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
	  
   /;
}
   unless ($sth = $dbh->prepare($sql)) {
       &display_error("Error preparing sql query", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   $sth->execute($reportid, $worksheet, $row, $col);

   my ($value) = $sth->fetchrow_array;

   return($value);
}


#----------------------------------------------------------------------------

sub display_error
{
   my @list = @_;

   my ($string, $s);

   foreach $s (@list) {
      $string .= "$s<BR>\n";
   }

   print "Content-Type: text/html\n\n";

   print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
<HEAD><TITLE>ERROR</TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
<link href="/JS/ergosign.styleform.css" media="all" type="text/css" rel="stylesheet">

    <script type="text/javascript" src="/JS/mootools-1.2-core.js"></script>

    <script type="text/javascript" src="/JS/ergosign.styleform.js"></script>

    <script type="text/javascript" src="/JS/script.js"></script>
    <link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />
</HEAD>
<BODY><a style="display:scroll;position:fixed;left:5px;bottom:5px;" href="#" title="Feedback Please"><img border=0 src="/images/feedback.png"/></a>
<CENTER>
<BR>
<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/cgi-bin/demo_index.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/demo_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>Standard Reports</div>">
			        <img src="/images/stardardreport_inner_icon.gif" onmouseover="src='/images/stardardreport_inner_icon_hover.gif';"
                                    onmouseout="src='/images/stardardreport_inner_icon.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    <td style="width: 102px" align="Center">
		                                <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
		                                    <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->
    <!--content start-->
    <table cellpadding="0" cellspacing="0" width="885px">
        <tr><td align="left">Welcome
            	};
            	
            	print $firstName . " " . $lastName;
            	print qq{
            	</td>  
            	<td align="right">
            	<a href="#">Sign Out</a>
            	</td>
        	</tr>
    </table>
        <table cellpadding="0" cellspacing="0" width="885px">
            <tr>
                <td valign="top">
                    <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px">
<FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<TABLE CLASS="error" WIDTH="600">
   <TR>
      <TD ALIGN="CENTER">
          <FONT SIZE="4">
          <BR>
          $string
          <BR>
          </FONT>
      </TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</td>
            <td valign="top">
                <img src="/images/content_right.gif" /></td>
        </tr>
    </table>
    <!--content end-->
	};

&TDdoc;
print qq{
</CENTER>
</BODY>
</HTML>
   };

   return(0);
}


#----------------------------------------------------------------------------

sub comify
{
  local($_) = shift;
  1 while s/^(-?\d+)(\d{3})/$1,$2/;
  return($_);
}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#--------------------------------------------------------------------------------------
sub sessionExpire
{

print "Content-Type: text/html\n\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}


#------------------------------------------------------------------------------

