#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: setedit.pl
#   author: Steve Spicer
#  Written: Sun Nov 23 13:23:42 CST 2008
#  Revised: Tue Jun  9 18:52:01 CDT 2009
#----------------------------------------------------------------------------

use strict;
use CGI;

my $cgi = new CGI;
do "common_header.pl";
print "Content-Type: text/html\n\n";
&headerScript();
my %ini_value;
my $set       = $cgi->param('set');  
my $action    = $cgi->param('action');  

if ($action eq "save") {
   &save_results($set)
} else {
   &display_form();
}

exit(0);

#----------------------------------------------------------------------------

sub display_error
{
   my @list = @_;

   my ($string, $s);

   foreach $s (@list) {
      $string .= "$s<BR>\n";
   }

  

   print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>

</HEAD>
<BODY>
<CENTER>
    <!--content start--><br>
        <table cellpadding="0" cellspacing="0" width="885px">
            <tr>
                <td valign="top">
                    <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px">
<FONT SIZE="5" COLOR="RED">
<B>ERROR:</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" VALUE=" CLOSE " onclick="window.close()"></span>
</FORM>
</td>
            <td valign="top">
                <img src="/images/content_right.gif" /></td>
        </tr>
    </table>
    <!--content end-->
</CENTER>
</BODY>
</HTML>
   };

   return(0);
}

#----------------------------------------------------------------------------

sub display_form
{
   unless (open INPUT, "packages/$set") {
      &display_error("CAN'T OPEN SET \"$set\"");
      return(0);
   }

  
   
   print <<EOF
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
<HEAD>
<TITLE>SELECTED ELEMENT EDITOR</TITLE>
<script type="text/javascript" src="/JS/admin_panel/spreadsheet_generator/setedit.js"></script>

</HEAD>
<BODY>
<FORM NAME="form1" ID="form1" METHOD="POST" ACTION="/cgi-bin/setedit.pl">
<CENTER>
    <!--content start--><br>
        <table cellpadding="0" cellspacing="0" width="885px">
            <tr>
                <td valign="top">
                    <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px" valign="top">
<Div class="pageheader">EDIT SELECTION SET: $set</div>

<TABLE NAME="TBL1" ID="TBL1" BORDER="0" CELLPADDING="1" WIDTH="750" class=\"gridstyle\">   
EOF
;

   my %lookup = (
     'O' => 'OTHER',
     'S' => 'STRING',
     'C' => 'MONEY',
     'D' => 'DECIMAL',
     'N' => 'NUMBER',
     'U' => 'URL',
   );

   my ($line, $option_list, $selected, $key);
   my ($worksheet, $row, $col, $fmt, $desc);

   while ($line = <INPUT>) {
      chomp($line);

      next if ($line =~ m/^#|^$/);

      ($worksheet, $row, $col, $fmt, $desc) = split /\|/,$line;

      $option_list = '';
       
      foreach $key (keys %lookup) {
         $selected = ($key eq $fmt) ? 'SELECTED' : '';
         $option_list .= "         <OPTION VALUE=\"$key\" $selected>$lookup{$key}</OPTION>\n";
      }

      print "   <TR>\n";
      print "      <TD ALIGN=\"left\"><INPUT class=\"textboxstyle\" TYPE=\"TEXT\" NAME=\"WRK\" SIZE=\"7\" VALUE=\"$worksheet\"></TD>\n";
      print "      <TD ALIGN=\"left\"><INPUT class=\"textboxstyle\" TYPE=\"TEXT\" NAME=\"ROW\" SIZE=\"5\" VALUE=\"$row\"></TD>\n";
      print "      <TD ALIGN=\"left\"><INPUT class=\"textboxstyle\" TYPE=\"TEXT\" NAME=\"COL\" SIZE=\"5\" VALUE=\"$col\"></TD>\n";
      print "      <TD ALIGN=\"left\">\n";
      print "         <SELECT NAME=\"FMT\">\n";
      print              "$option_list";
      print "         </SELECT>\n";
      print "      </TD>\n";
      print "      <TD ALIGN=\"left\">\n";
      print "        <INPUT class=\"textboxstyle\" TYPE=\"TEXT\" NAME=\"DES\" SIZE=\"35\" VALUE=\"$desc\">\n";
      print "        <IMG SRC=\"/icons/delete_icon.gif\" onclick=\"delete_row(this)\" ALT=\"DELETE THIS ROW\">\n";
      print "        <IMG SRC=\"/icons/plus-green.gif\" onclick=\"insert_row(this)\" ALT=\"INSERT ROW BEFORE THIS ONE\">\n";
      print "      </TD>\n";
      print "   </TR>\n";
   }

   close INPUT;

    print qq{
</TABLE>
<BR>
<span class="button"><INPUT TYPE="button" VALUE=" CLOSE " onclick="window.close()"></span>
<span class="button"><INPUT TYPE="button" VALUE="  ADD ROW TO BOTTOM  " onclick="add_row()"></span>
<INPUT TYPE="hidden" NAME="action" VALUE="save">
<span class="button"><INPUT TYPE="button" VALUE="  SAVE AS  " onclick="save_results()"></span>
<INPUT class="textboxstyle" TYPE="TEXT" NAME="set" VALUE="$set" SIZE="15" MAXLENGTH="15">

</td>
            <td valign="top">
                <img src="/images/content_right.gif" /></td>
        </tr>
    </table>
    <!--content end-->
</CENTER>
</FORM>
</BODY>
</HTML>
   };

}

#----------------------------------------------------------------------------

sub save_results
{
   my ($fname) = @_;

   $fname =~ s/\///g;  # remove any slashes
   $fname =~ s/ /_/g;  # i dont like spaces
   $fname = lc($fname);

   unless ($fname =~ m/\.txt$/) {
     $fname .= '.txt';
   }

   unless (open OUTPUT, ">./packages/$fname") {
     display_error("ERROR OPENING \"$fname\" FOR WRITING", $!);
     return(0);
   }
        
   my %hash;

   my @names = $cgi->param;

   my ($i, $name, $value, $wrk, $row, $col, $fmt, $des);

   my $max = 0;

   foreach $name (@names) {
       $value = $cgi->param("$name");
   
       if ($name =~ m/^([A-Z]+)-(\d+)$/) {
          ${$hash{$1}}[$2] = $value;
          $max = $2 if ($2 > $max);
       }
   }

   for ($i = 1; $i <= $max; ++$i)  {
       $wrk = ${$hash{'WRK'}}[$i]; 
       $row = ${$hash{'ROW'}}[$i]; 
       $col = ${$hash{'COL'}}[$i]; 
       $fmt = ${$hash{'FMT'}}[$i]; 
       $des = ${$hash{'DES'}}[$i]; 
       print OUTPUT "$wrk|$row|$col|$fmt|$des\n";
   }

   close OUTPUT;

   &display_success;

   return 0;
}

#----------------------------------------------------------------------------

sub display_success
{
   my $now = localtime();

 
   
   print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
<HEAD>
<TITLE>EDIT SELECTION GRID</TITLE>

<link href="/css/cost_report/ergosign.styleform.css" media="all" type="text/css" rel="stylesheet">

    <script type="text/javascript" src="/JS/mootools-1.2-core.js"></script>

    <script type="text/javascript" src="/JS/cost_report/ergosign.styleform.js"></script>

    <script type="text/javascript" src="/JS/script.js"></script>
    
</HEAD>
<BODY onload="styleForm()">
<CENTER>
<BR>
<BR>
<TABLE BORDER="1" CELLSPACING="0" CELLPADDING="8" WIDTH="400">
   <TR>
      <TD ALIGN="CENTER">
         <BR>
         <FONT SIZE="5" COLOR="GREEN">
         OPERATION SUCCESSFUL<BR>
         </FONT>
         <FONT SIZE="1">$now</FONT><BR>
         <BR>
      </TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" VALUE=" CLOSE " onclick="window.close()"></span>
</FORM>
</CENTER>
</BODY>
</HTML>
   }
}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#------------------------------------------------------------------------------

