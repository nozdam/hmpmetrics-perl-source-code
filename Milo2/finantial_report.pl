#!/usr/bin/perl

use CGI;
use DBI;

do "../common_header.pl";
do "../pagefooter.pl";
my $cgi = new CGI;
use Date::Calc qw(Delta_Days);
use CGI::Session;
do "../audit.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");

my $providerid = $session->param("providerid");

if(!$userID)
{
	&sessionExpire;
}


# Database

my $dbh;
my ($sql, $sth,$stmt,$prevprovider,$stht,$prev_prov);

&view("View","Generate finantial report","Milo2");
# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};

my ($years,$yearLoopCount,$htmlData,$hospitalName);

$yearLoopCount = 0;
@fYears = reverse(@fYears);
foreach my $fYears (@fYears){
	if($yearLoopCount <5){
		if($years){
			$years = $years . "," . $fYears;
		}
		else{
			$years = $fYears;
		}
	}
	$yearLoopCount = $yearLoopCount + 1;
}

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

#Get the provider name
if($providerid){
	$sql = "Select HospitalName as HOSP_NAME from tblhospitalmaster where ProviderNo = $providerid";
	$stmt = $dbh->prepare($sql);
	$stmt->execute();
	while ($row = $stmt->fetchrow_hashref) {
		$hospitalName = $$row{HOSP_NAME};
	}
}

#Generate Report

my (@provider_number,@rpt_rec_num,@rpt_year,@fy_bgn_dt,@fy_end_dt,$recCount,@prev_prov,@control_type,@facility_type);
my (@inpatient_revenue, @outpatient_revenue, @contractual_allowance, @net_patient_revenue, @total_operating_expenses, @operating_income);
my (@income_from_investments, @governmental_approp, @non_operating_revenue, @net_income, @current_assets, @fixed_assets, @current_liabilities);
my (@contributions_etc, @long_term_liabilities, @total_liabilities, @total_fund_balances);
$recCount = 1;
if($providerid){

	$prevprovider="Select PreviousProviderNo from tblpreviousprovider where CurrentProviderNo = '$providerid' ";#($providerid,$prevprovider)

	$sth = $dbh->prepare($prevprovider);
	$sth->execute();

	 while ($row = $sth->fetchrow_hashref){
	 push (@prev_prov, $$row{PreviousProviderNo});
	 }

	 foreach my $prev_prov(@prev_prov){
	$providerid=$providerid.",".$prev_prov;
	}


	$sql = "SELECT c.prvdr_num,c.rpt_rec_num,year(c.fy_end_dt) AS rpt_year, c.rpt_stus_cd, c.fi_num, c.fy_bgn_dt, c.fy_end_dt,t.control_type_name AS control_type_name,
			s.Hospital_Type_Name as facility_type, b.inpatient_revenue, b.outpatient_revenue, b.contractual_allowance, b.net_patient_revenue, b.total_operating_expenses, b.operating_income,
			b.income_from_investments, b.governmental_approp, b.non_operating_revenue, b.net_income, b.assets_in_general_fund AS current_assets, b.fixed_assets, b.current_liabilities,
			b.contributions_etc, b.long_term_liabilities, b.total_liabilities, b.total_fund_balances
			FROM CR_ALL_RPT as c INNER JOIN tblhospitalmaster as k on k.ProviderNo=c.PRVDR_NUM
			INNER JOIN _tbl_provider_metrics AS b ON c.rpt_rec_num = b.rpt_rec_num AND c.formatid = b.formatid
			INNER JOIN derived_metrics as d ON c.rpt_rec_num = d.rpt_rec_num AND c.formatid = d.formatid
			INNER JOIN control_type as t on t.Control_Type_id=k.HMPControl
			INNER JOIN hospital_type as s on s.Hospital_Type_id=k.HMPFacilityType
			WHERE c.PRVDR_NUM in ($providerid) AND year(c.FY_END_DT)  in ($years) ORDER BY c.FY_END_DT desc";
	$sth = $dbh->prepare($sql);
	$sth->execute();
	while ($row = $sth->fetchrow_hashref) {
		push (@rpt_rec_num, $$row{rpt_rec_num});
		push (@rpt_year, $$row{rpt_year});
		push (@fy_bgn_dt, $$row{fy_bgn_dt});
		push (@fy_end_dt, $$row{fy_end_dt});
		push(@control_type,$$row{control_type_name});
		push(@facility_type,$$row{facility_type});
		push(@provider_number,$$row{prvdr_num});

		push(@inpatient_revenue, $$row{inpatient_revenue});
		push(@outpatient_revenue, $$row{outpatient_revenue});
		push(@contractual_allowance, $$row{contractual_allowance});
		push(@net_patient_revenue, $$row{net_patient_revenue});
		push(@total_operating_expenses, $$row{total_operating_expenses});
		push(@operating_income, $$row{operating_income});
		push(@income_from_investments, $$row{income_from_investments});
		push(@governmental_approp, $$row{governmental_approp});
		push(@non_operating_revenue, $$row{non_operating_revenue});
		push(@net_income, $$row{net_income});
		push(@current_assets, $$row{current_assets});
		push(@fixed_assets, $$row{fixed_assets});
		push(@current_liabilities, $$row{current_liabilities});
		push(@contributions_etc, $$row{contributions_etc});
		push(@long_term_liabilities, $$row{long_term_liabilities});
		push(@total_liabilities, $$row{total_liabilities});
		push(@total_fund_balances, $$row{total_fund_balances});

		$recCount++;
	}

		open(J, ">data.txt");
	    print J $sql;
	  close J;
	#Starting a new Row
	$htmlData = $htmlData . "<tr>";

	#FY Begin-Date
	$htmlData = $htmlData . "<td align=\"left\">" . "FY Begin-Date" . "</td>";
	my $countX = 0;
	foreach my $fy_bgn_dt (@fy_bgn_dt){
		if($countX%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $fy_bgn_dt . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $fy_bgn_dt . "</td>";
		}
		$countX++;
	}

	$htmlData = $htmlData . "</tr>";
	$htmlData = $htmlData . "<tr>";

	#FY End-Date
	$htmlData = $htmlData . "<td align=\"left\">" . "FY End-Date" . "</td>";
	my $countY = 0;
	foreach my $fy_end_dt (@fy_end_dt){
		if($countY%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $fy_end_dt . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $fy_end_dt . "</td>";
		}
		$countY++;
	}

	$htmlData = $htmlData . "</tr>";

	#Number of Days in Period
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"left\">" . "Number of Days in Period" . "</td>";
	foreach my $fy_bgn_dt (@fy_bgn_dt){
		@fy_bgn = split("-",$fy_bgn_dt);
		@fy_end  = split("-",$fy_end_dt[$arrayCount]);
		$value = Delta_Days(@fy_bgn, @fy_end);
		$value = $value+1;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $value . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $value . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Provider Number
	$htmlData = $htmlData . "<td align=\"left\">" . "Provider Number" . "</td>";
	my $countI = 0;
	foreach my $provider_number (@provider_number){
		if($countI%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $provider_number . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $provider_number . "</td>";
		}
		$countI++;
	}

	$htmlData = $htmlData . "</tr>";
	$htmlData = $htmlData . "<tr>";

	#Type of Control#
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"left\">" . "Type of Control" . "</td>";
	my $countM = 0;
	foreach my $control_type (@control_type){
		if($countM%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $control_type . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $control_type. "</td>";
		}
		$countM++;
	}

	$htmlData = $htmlData . "</tr>";


	#Facility Type#
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"left\">" . "Facility Type" . "</td>";
	my $countF = 0;
	foreach my $facility_type (@facility_type){
		if($countF%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $facility_type . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $facility_type . "</td>";
		}
		$countF++;
	}
	$htmlData = $htmlData . "</tr>";
	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	#Income Statement
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td colspan=$recCount>" . "<b>Income Statement</b>" . "</td>";

	$htmlData = $htmlData . "</tr>";

	#Inpatient Revenue
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Inpatient Revenue" . "</td>";
	my $countALL = 0;
	foreach my $metric (@inpatient_revenue) {
		if($countALL%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($metric) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($metric) . "</td>";
		}
		$countALL++;
	}
	$htmlData = $htmlData . "</tr>";

	#Outpatient Revenue
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Outpatient Revenue" . "</td>";
	my $countALL = 0;
	foreach my $metric (@outpatient_revenue) {
		if($countALL%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($metric) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($metric) . "</td>";
		}
		$countALL++;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Patient Revenue
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td>" . "Total Patient Revenue" . "</td>";
	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-00100-0100");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";

	#Contractual Allowance (Discounts)
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Contractual Allowance (Discounts)" . "</td>";
	my $countALL = 0;
	foreach my $metric (@contractual_allowance) {
		if($countALL%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($metric) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($metric) . "</td>";
		}
		$countALL++;
	}
	$htmlData = $htmlData . "</tr>";

	#Net Patient Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Net Patient Revenues" . "</td>";
	my $countALL = 0;
	foreach my $metric (@net_patient_revenue) {
		if($countALL%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($metric) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($metric) . "</td>";
		}
		$countALL++;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Operating Expense
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Total Operating Expense" . "</td>";
	my $countALL = 0;
	foreach my $metric (@total_operating_expenses) {
		if($countALL%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($metric) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($metric) . "</td>";
		}
		$countALL++;
	}
	$htmlData = $htmlData . "</tr>";

	#Operating Income
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "<b>Operating Income</b>" . "</td>";
	my $countALL = 0;
	foreach my $metric (@operating_income) {
		if($countALL%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($metric) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($metric) . "</b></td>";
		}
		$countALL++;
	}
	$htmlData = $htmlData . "</tr>";

	#Other Income (Contributions, Bequests, etc.)
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Other Income (Contributions, Bequests, etc.)" . "</td>";
	my $countALL = 0;
	foreach my $metric (@contributions_etc) {
		if($countALL%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($metric) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($metric) . "</td>";
		}
		$countALL++;
	}
	$htmlData = $htmlData . "</tr>";

	#Income from Investments
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Income from Investments" . "</td>";
	my $countALL = 0;
	foreach my $metric (@income_from_investments) {
		if($countALL%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($metric) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($metric) . "</td>";
		}
		$countALL++;
	}
	$htmlData = $htmlData . "</tr>";

	#Governmental Appropriations
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Governmental Appropriations" . "</td>";
	my $countALL = 0;
	foreach my $metric (@governmental_approp) {
		if($countALL%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($metric) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($metric) . "</td>";
		}
		$countALL++;
	}
	$htmlData = $htmlData . "</tr>";

	#Miscellaneous Non-Patient Revenue
	#my $formula = "G300000-00900-0100 + G300000-01000-0100 + G300000-01100-0100 + G300000-01200-0100 + G300000-01300-0100 + G300000-01400-0100 + G300000-01500-0100 + G300000-01700-0100 + G300000-01800-0100 + G300000-02000-0100 + G300000-02100-0100 + G300000-02200-0100 + G300000-02400-0100 + G300000-02401-0100 + G300000-02402-0100 + G300000-02403-0100 + G300000-02404-0100 + G300000-02405-0100 + G300000-02406-0100 + G300000-02407-0100 + G300000-02404-0100 + G300000-02405-0100 + G300000-02406-0100 + G300000-02407-0100 + G300000-02408-0100 + G300000-02409-0100 + G300000-02410-0100 + G300000-02411-0100 + G300000-02412-0100 + G300000-02419-0100 + G300000-02420-0100";
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td>" . "Miscellaneous Non-Patient Revenue" . "</td>";
	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, $formula);
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";

	#Total Non-Patient Revenue
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Total Non-Patient Revenue" . "</td>";
	my $countALL = 0;
	foreach my $metric (@non_operating_revenue) {
		if($countALL%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($metric) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($metric) . "</td>";
		}
		$countALL++;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Other Expenses
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td>" . "Total Other Expenses" . "</td>";
	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-03000-0100");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";

	#Net Income or (Loss)
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "<b>Net Income or (Loss)</b>" . "</td>";
	my $countALL = 0;
	foreach my $metric (@net_income) {
		if($countALL%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($metric) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($metric) . "</b></td>";
		}
		$countALL++;
	}
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";

	#Balance Sheet
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td colspan=$recCount>" . "<b>Balance Sheet</b>" . "</td>";
	$htmlData = $htmlData . "</tr>";

	#Assets
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td colspan=$recCount>" . "<b>Assets</b>" . "</td>";
	$htmlData = $htmlData . "</tr>";

	#Current Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Current Assets" . "</td>";
	my $countALL = 0;
	foreach my $metric (@current_assets) {
		if($countALL%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($metric) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($metric) . "</td>";
		}
		$countALL++;
	}
	$htmlData = $htmlData . "</tr>";

	#Fixed Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Fixed Assets" . "</td>";
	my $countALL = 0;
	foreach my $metric (@fixed_assets) {
		if($countALL%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($metric) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($metric) . "</td>";
		}
		$countALL++;
	}
	$htmlData = $htmlData . "</tr>";

	#Other Assets
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td>" . "Other Assets" . "</td>";
	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-02600-0100");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";

	#Total Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "<b>Total Assets</b>" . "</td>";
	my $countALL = 0;
	foreach my $metric (@fixed_assets) {
		if($countALL%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($metric+$current_assets[$countALL]) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($metric+$current_assets[$countALL]) . "</b></td>";
		}
		$countALL++;
	}
	$htmlData = $htmlData . "</tr>";

	#Liabilities and Fund Balances
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td colspan=$recCount><b>" . "Liabilities and Fund Balances" . "</b></td>";
	$htmlData = $htmlData . "</tr>";

	#Current Liabilities
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Current Liabilities" . "</td>";
	my $countALL = 0;
	foreach my $metric (@current_liabilities) {
		if($countALL%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($metric) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($metric) . "</td>";
		}
		$countALL++;
	}
	$htmlData = $htmlData . "</tr>";

	#Long-Term Liabilities
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Long-Term Liabilities" . "</td>";
	my $countALL = 0;
	foreach my $metric (@long_term_liabilities) {
		if($countALL%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($metric) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($metric) . "</td>";
		}
		$countALL++;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Liabilities
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "<b>Total Liabilities</b>" . "</td>";
	my $countALL = 0;
	foreach my $metric (@total_liabilities) {
		if($countALL%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($metric) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($metric) . "</b></td>";
		}
		$countALL++;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Fund Balances
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Total Fund Balances" . "</td>";
	my $countALL = 0;
	foreach my $metric (@total_fund_balances) {
		if($countALL%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($metric) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($metric) . "</td>";
		}
		$countALL++;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Liabilities & Fund Balances
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "<b>Total Liabilities & Fund Balances</b>" . "</td>";
	my $countALL = 0;
	foreach my $metric (@total_liabilities) {
		if($countALL%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($metric + $total_fund_balances[$countALL]) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($metric + $total_fund_balances[$countALL]) . "</b></td>";
		}
		$countALL++;
	}
	$htmlData = $htmlData . "</tr>";
}else{
	$htmlData="<tr><td>Please select provider number and run report again.<a href=\"/cgi-bin/Milo2/SelectReport.pl\">Back</a></td></tr>";
}
print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>Financial Reports</TITLE>
<SCRIPT SRC="/JS/sorttable.js"></SCRIPT>
<SCRIPT SRC="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<SCRIPT SRC="/JS/dropmenu.js"></SCRIPT>
<link rel="stylesheet" href="/css/dropmenu.css" type="text/css" />
</HEAD>
<body>
<BR>
<FORM NAME = "frmFinacial" Action="/cgi-bin/finantial_report.pl" method="post">
<CENTER>
};
&innerHeader();
print qq{

    <!--content start-->

        <table cellpadding="0" cellspacing="0" width="885px">
	  <tr>
	        <td width="16"><img src="/images/tableRonuded/top_lef.gif" width="16" height="16"></td>
	        <td height="16" background="/images/tableRonuded/top_mid.gif"><img src="/images/tableRonuded/top_mid.gif" width="16" height="16"></td>
	        <td width="24"><img src="/images/tableRonuded/top_rig.gif" width="24" height="16"></td>
	      </tr>
	      <tr>
	        <td width="16" background="/images/tableRonuded/cen_lef.gif"></td>
	        <td align="center" valign="middle" bgcolor="#FFFFFF">

<table border="1em" bgColor="#FFFFFF" width="100%" cellpadding="0" cellspacing="0">
<tr class='gridHeader'><td nowrap colspan=$recCount><b>Financial Report for Provider : </b><font style="color:#C11B17;">$hospitalName - $providerid</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"/cgi-bin/Milo2/SelectReport.pl\">Back</a></td></tr>
$htmlData
</table>
</td>
		<td width="24" background="/images/tableRonuded/cen_rig.gif"><img src="/images/tableRonuded/cen_rig.gif" width="24" height="11"></td>
	      </tr>
	      <tr>
		    <td width="16" height="16"><img src="/images/tableRonuded/bot_lef.gif" width="16" height="16"></td>
		    <td height="16" background="/images/tableRonuded/bot_mid.gif"><img src="/images/tableRonuded/bot_mid.gif" width="16" height="16"></td>
		    <td width="24" height="16"><img src="/images/tableRonuded/bot_rig.gif" width="24" height="16"></td>
	      </tr>
   </table>
};
&TDdoc;
print qq{
</body>
</HTML>
};
#--------------------------------------------------------------------------------------

sub sessionExpire
{

print "Content-Type: text/html\n\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};
&headerScript();
print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}
#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#--------------------------------------------------------------------------------

sub getFormulae
{
	my ($report_id, $yyyy, $formulae) = @_;
	my ($value);
	#$value = &getFormulaeValue($report_id,"(G200000-002500-0200)/(G200000-002500-0300)",$yyyy);

	$value = &getFormulaeValue($report_id,$formulae,$yyyy);

	#EBITDAR as a Percent of Interest Expense
	#Net Revenue per FTE

	return $value;
}


#----------------------------------------------------------------------------

sub getFormulaeValue
{
	my ($report_id,$formulae,$yyyy) = @_;
	my ($value, $i, $elementVal, $operation);

	for ($i=0;$i<length($formulae);$i++)
	{
		if(substr($formulae, $i,1) =~ /[A-Z]/ )
		{
			#Spliting elements from Address
			my ($worksheet, $line, $column) = split /[-:]/,substr($formulae, $i,18);
			if(&get_value($report_id, $worksheet, $line, $column, $yyyy))
			{
				$elementVal = &get_value($report_id, $worksheet, $line, $column, $yyyy);
			}
			else
			{
				$elementVal = 0;
			}


			$value = $value.$elementVal;
			$i = $i+17;
		}
		elsif(substr($formulae, $i,1) =~ /\s/ )
		{

		}
		else
		{

			$value = $value . substr($formulae, $i,1);
		}
	}
	$value = eval($value);
	if(!$value)
	{
		$value = "0";
	}
	else
	{
		#$value = sprintf "%.2f", $value;
		$value = $value;
	}
	return $value;

}

#------------------------------------------------------------------------------

sub get_value
{
   my ($report_id, $worksheet, $line, $column, $fYear) = @_;

   my $yyyy = $fYear;

   my ($sthVal, $value);

   my $sql = qq(
    SELECT CR_VALUE
      FROM CR_ALL_DATA
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
   );

   unless ($sthVal = $dbh->prepare($sql)) {
       &display_error('Error preparing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sthVal->execute($report_id, $worksheet, $line, $column)) {
       &display_error('Error executing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

   if ($sthVal->rows) {
      $value = $sthVal->fetchrow_array;
   }

   $sthVal->finish();

   return $value;
}

#------------------------------------------------------------------------------

sub numNegFormat
{
   my ($val,$dollar) = @_;
   if($val<0){
   	$val = $val*-1;
   	$val= Currency_Format($val);
   	if($dollar){
   		$val = "(" . "\$" . $val . ")";
   	}
   	else{
   		$val = "(" . $val . ")";
   	}
   }
   else{
   	if($dollar){
   		$val= "\$" . Currency_Format($val);
   	}
   	else{
   		$val= Currency_Format($val);
   	}
   }
   return $val;
}

#------------------------------------------------------------------------------

sub USA_Format {
(my $n = shift) =~ s/\G(\d{1,3})(?=(?:\d\d\d)+(?:\.|$))/$1,/g;
return "\$$n";
}

#------------------------------------------------------------------------------

sub Currency_Format {
(my $n = shift) =~ s/\G(\d{1,3})(?=(?:\d\d\d)+(?:\.|$))/$1,/g;
return "$n";
}
#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}
