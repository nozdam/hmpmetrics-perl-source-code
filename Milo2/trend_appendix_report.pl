#!/usr/bin/perl

use CGI;
use DBI;
use strict;

do "../common_header.pl";
do "../pagefooter.pl";
my $cgi = new CGI;
use Date::Calc qw(Delta_Days);
use CGI::Session;
do "../audit.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my ($arrayCount, $value, $row, $s, @fy_bgn, @fy_end);

my $providerid =$session->param("providerid");
#my $providerid ="171354";

if(!$userID)
{
	&sessionExpire;
}


# Database

my $dbh;
my ($sql, $sth,$stmt);

&view("View","Generate trend appendix report","Milo2");
# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};

my ($years,$yearLoopCount,$htmlData,$hospitalName);

$yearLoopCount = 0;
@fYears = reverse(@fYears);
foreach my $fYears (@fYears){
	if($yearLoopCount <5){
		if($years){
			$years = $years . "," . $fYears;
		}
		else{
			$years = $fYears;
		}
	}
	$yearLoopCount = $yearLoopCount + 1;
}

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

#Get the provider name
if($providerid){
	$sql = "Select HospitalName as HOSP_NAME from tblhospitalmaster where ProviderNo = $providerid";
	$stmt = $dbh->prepare($sql);
	$stmt->execute();
	while ($row = $stmt->fetchrow_hashref) {
		$hospitalName = $$row{HOSP_NAME};
	}
}

#Declare cmi variable
my %cmi;

#Generate Report

my(@provider_number,@rpt_rec_num,@rpt_year,@fy_bgn_dt,@fy_end_dt,$recCount,$prevprovider,$prev_prov,@prev_prov,@control_type,@facility_type);
my (@average_age_of_plant, @accumulated_depreciation_total, @average_payment_period, @ftes_per_aob, @current_asset_turnover, @days_cash_on_hand, @days_cash_on_hand_all_sources, @days_in_net_patient_ar, @days_in_net_total_ar, @debt_financing_percentage, @ebidar_margin, @equity_financing_percentage, @excess_margin, @fixed_asset_turnover, @long_term_debt_to_total_assets, @medicare_margin_general_acute_service_only, @medicare_operations_rcc, @occupancy_rate, @outlier_percentage, @labor_cost_as_a_percent_of_revenue, @ratio_of_cost_to_charges, @return_on_assets, @return_on_equity, @total_asset_turnover, @total_debt_to_net_assets, @overall_margin, @operating_margin, @depreciation_expense, @current_liabilities, @total_operating_expenses, @total_beds, @inpatient_days, @inpatient_revenue, @other_ltc_revenue, @net_patient_revenue, @assets_in_general_fund, @cash_on_hand, @temporary_investments_specificpurposefund, @accounts_receivable, @allow_for_uncollectible, @notes_receivable, @other_receivables, @total_fund_balances, @net_income, @interest_expense, @lease_cost, @total_patient_revenues_amount, @non_operating_revenue, @long_term_liabilities, @total_liabilities, @beds_available, @fringe_benefits, @operating_income, @medicare_capital_rcc, @total_discharges, @current_ratio, @investments, @fixed_assets, @salary_expense, @num_days);
$recCount = 1;
if($providerid){
$prevprovider="Select PreviousProviderNo from tblpreviousprovider where CurrentProviderNo = '$providerid' ";#($providerid,$prevprovider)

	$sth = $dbh->prepare($prevprovider);
	$sth->execute();

	 while ($row = $sth->fetchrow_hashref){
	 push (@prev_prov, $$row{PreviousProviderNo});
	 }

	 foreach my $prev_prov(@prev_prov){
	$providerid=$providerid.",".$prev_prov;
	}

	$sql = "SELECT c.PRVDR_NUM,c.RPT_REC_NUM, year(c.FY_END_DT) as RPT_YEAR, c.RPT_STUS_CD, c.FI_NUM, c.FY_BGN_DT, c.FY_END_DT,t.Control_Type_Name as CONTROL_TYPE_NAME,
			s.Hospital_Type_Name as FACILITY_TYPE, DATEDIFF(c.fy_end_dt, c.fy_bgn_dt) AS num_days,
			d.average_age_of_plant, d.accumulated_depreciation_total, d.average_payment_period, d.ftes_per_aob, d.current_asset_turnover, d.days_cash_on_hand, d.days_cash_on_hand_all_sources,
			d.days_in_net_patient_ar, d.days_in_net_total_ar, d.debt_financing_percentage, d.ebidar_margin, d.equity_financing_percentage, d.excess_margin, d.fixed_asset_turnover, d.long_term_debt_to_total_assets,
			d.medicare_margin_general_acute_service_only, d.medicare_operations_rcc, d.occupancy_rate, d.outlier_percentage, d.labor_cost_as_a_percent_of_revenue, d.ratio_of_cost_to_charges,
			d.return_on_assets, d.return_on_equity, d.total_asset_turnover, d.total_debt_to_net_assets, d.overall_margin, d.operating_margin, d.medicare_capital_rcc, d.current_ratio,
			b.depreciation_expense, b.current_liabilities, b.total_operating_expenses, b.total_beds, b.inpatient_days, b.inpatient_revenue, b.other_ltc_revenue,
			b.net_patient_revenue, b.assets_in_general_fund, b.cash_on_hand, b.temporary_investments_specificpurposefund, b.accounts_receivable, b.allow_for_uncollectible,
			b.notes_receivable, b.other_receivables, b.total_fund_balances, b.net_income, b.interest_expense, b.lease_cost, b.total_patient_revenues_amount, b.non_operating_revenue,
			b.long_term_liabilities, b.total_liabilities, b.beds_available, b.fringe_benefits, b.operating_income, b.total_discharges, b.investments, b.fixed_assets, b.salary_expense
			FROM CR_ALL_RPT as c INNER JOIN tblhospitalmaster as k on k.ProviderNo=c.PRVDR_NUM
			INNER JOIN _tbl_provider_metrics b ON c.rpt_rec_num = b.rpt_rec_num AND c.formatid = b.formatid
			INNER JOIN derived_metrics d ON c.rpt_rec_num = d.rpt_rec_num AND c.formatid = d.formatid
			INNER JOIN control_type as t on t.Control_Type_id=k.HMPControl
			INNER JOIN hospital_type as s on s.Hospital_Type_id=k.HMPFacilityType
			WHERE c.PRVDR_NUM in ($providerid) AND year(c.FY_END_DT)  in ($years) ORDER BY c.FY_END_DT desc";
	$sth = $dbh->prepare($sql);
	$sth->execute();
	while ($row = $sth->fetchrow_hashref) {
		push (@rpt_rec_num, $$row{RPT_REC_NUM});
		push (@rpt_year, $$row{RPT_YEAR});
		push (@fy_bgn_dt, $$row{FY_BGN_DT});
		push (@fy_end_dt, $$row{FY_END_DT});
		push(@control_type,$$row{CONTROL_TYPE_NAME});
		push(@facility_type,$$row{FACILITY_TYPE});
		push(@provider_number,$$row{PRVDR_NUM});

		push(@average_age_of_plant, $$row{average_age_of_plant});
		push(@accumulated_depreciation_total, $$row{accumulated_depreciation_total});
		push(@average_payment_period, $$row{average_payment_period});
		push(@ftes_per_aob, $$row{ftes_per_aob});
		push(@current_asset_turnover, $$row{current_asset_turnover});
		push(@days_cash_on_hand, $$row{days_cash_on_hand});
		push(@days_cash_on_hand_all_sources, $$row{days_cash_on_hand_all_sources});
		push(@days_in_net_patient_ar, $$row{days_in_net_patient_ar});
		push(@days_in_net_total_ar, $$row{days_in_net_total_ar});
		push(@debt_financing_percentage, $$row{debt_financing_percentage});
		push(@ebidar_margin, $$row{ebidar_margin});
		push(@equity_financing_percentage, $$row{equity_financing_percentage});
		push(@excess_margin, $$row{excess_margin});
		push(@fixed_asset_turnover, $$row{fixed_asset_turnover});
		push(@long_term_debt_to_total_assets, $$row{long_term_debt_to_total_assets});
		push(@medicare_margin_general_acute_service_only, $$row{medicare_margin_general_acute_service_only});
		push(@medicare_operations_rcc, $$row{medicare_operations_rcc});
		push(@occupancy_rate, $$row{occupancy_rate});
		push(@outlier_percentage, $$row{outlier_percentage});
		push(@labor_cost_as_a_percent_of_revenue, $$row{labor_cost_as_a_percent_of_revenue});
		push(@ratio_of_cost_to_charges, $$row{ratio_of_cost_to_charges});
		push(@return_on_assets, $$row{return_on_assets});
		push(@return_on_equity, $$row{return_on_equity});
		push(@total_asset_turnover, $$row{total_asset_turnover});
		push(@total_debt_to_net_assets, $$row{total_debt_to_net_assets});
		push(@overall_margin, $$row{overall_margin});
		push(@operating_margin, $$row{operating_margin});
		push(@depreciation_expense, $$row{depreciation_expense});
		push(@current_liabilities, $$row{current_liabilities});
		push(@total_operating_expenses, $$row{total_operating_expenses});
		push(@total_beds, $$row{total_beds});
		push(@inpatient_days, $$row{inpatient_days});
		push(@inpatient_revenue, $$row{inpatient_revenue});
		push(@other_ltc_revenue, $$row{other_ltc_revenue});
		push(@net_patient_revenue, $$row{net_patient_revenue});
		push(@assets_in_general_fund, $$row{assets_in_general_fund});
		push(@cash_on_hand, $$row{cash_on_hand});
		push(@temporary_investments_specificpurposefund, $$row{temporary_investments_specificpurposefund});
		push(@accounts_receivable, $$row{accounts_receivable});
		push(@allow_for_uncollectible, $$row{allow_for_uncollectible});
		push(@notes_receivable, $$row{notes_receivable});
		push(@other_receivables, $$row{other_receivables});
		push(@total_fund_balances, $$row{total_fund_balances});
		push(@net_income, $$row{net_income});
		push(@interest_expense, $$row{interest_expense});
		push(@lease_cost, $$row{lease_cost});
		push(@total_patient_revenues_amount, $$row{total_patient_revenues_amount});
		push(@non_operating_revenue, $$row{non_operating_revenue});
		push(@long_term_liabilities, $$row{long_term_liabilities});
		push(@total_liabilities, $$row{total_liabilities});
		push(@beds_available, $$row{beds_available});
		push(@fringe_benefits, $$row{fringe_benefits});
		push(@operating_income, $$row{operating_income});
		push(@investments, $$row{investments});
		push(@fixed_assets, $$row{fixed_assets});
		push(@medicare_capital_rcc, $$row{medicare_capital_rcc});
		push(@salary_expense, $$row{salary_expense});
		push(@current_ratio, $$row{current_ratio});
		push(@num_days, $$row{num_days});

		$recCount++;
	}

	open(J, ">data.txt");
	print J $sql;
	close J;


	$sql = "SELECT unadjusted_cmi,year FROM cmi WHERE provider_number in ($providerid)";
	$sth = $dbh->prepare($sql);
	$sth->execute();

	if($sth->rows > 0){
	while($row = $sth->fetchrow_hashref){
		$cmi{$$row{year}}=$$row{unadjusted_cmi};
	}

}


	#Starting a new Row
	$htmlData = $htmlData . "<tr>";

	#FY Begin-Date
	$htmlData = $htmlData . "<td align=\"right\">" . "FY Begin-Date" . "</td>";
	my $countX = 0;
	foreach my $fy_bgn_dt (@fy_bgn_dt){
		if($countX%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $fy_bgn_dt . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $fy_bgn_dt . "</td>";
		}
		$countX++;
	}

	$htmlData = $htmlData . "</tr>";
	$htmlData = $htmlData . "<tr>";

	#FY End-Date
	$htmlData = $htmlData . "<td align=\"right\">" . "FY End-Date" . "</td>";
	my $countY = 0;
	foreach my $fy_end_dt (@fy_end_dt){
		if($countY%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $fy_end_dt . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $fy_end_dt . "</td>";
		}
		$countY++;
	}

	$htmlData = $htmlData . "</tr>";

	#Number of Days in Period
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Number of Days in Period" . "</td>";
	foreach my $fy_bgn_dt (@fy_bgn_dt){
		@fy_bgn = split("-",$fy_bgn_dt);
		@fy_end  = split("-",$fy_end_dt[$arrayCount]);
		$value = Delta_Days(@fy_bgn, @fy_end);
		$value = $value+1;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $value . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $value . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#Provider Number
	$htmlData = $htmlData . "<td align=\"right\">" . "Provider Number" . "</td>";
	my $countI = 0;
	foreach my $provider_number (@provider_number){
		if($countI%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $provider_number . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $provider_number . "</td>";
		}
		$countI++;
	}

	$htmlData = $htmlData . "</tr>";
	$htmlData = $htmlData . "<tr>";

	#Type of Control#
	$htmlData = $htmlData . "<td align=\"right\">" . "Type of Control" . "</td>";
	my $countM = 0;
	foreach my $control_type (@control_type){
		if($countM%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $control_type . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $control_type. "</td>";
		}
		$countM++;
	}

	$htmlData = $htmlData . "</tr>";


	#Facility Type#
	$htmlData = $htmlData . "<td align=\"right\">" . "Facility Type" . "</td>";
	my $countF = 0;
	foreach my $facility_type (@facility_type){
		if($countF%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $facility_type . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $facility_type . "</td>";
		}
		$countF++;
	}
	$htmlData = $htmlData . "</tr>";
	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Average Age of Plant
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Average Age of Plant</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@average_age_of_plant, 1);
	$htmlData = $htmlData . "</tr>";


	#Accumulated Depreciation
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Accumulated Depreciation" . "</td>";
	$htmlData .= &print_metric($htmlData, \@accumulated_depreciation_total, 0);
	$htmlData = $htmlData . "</tr>";

	#Depreciation Expense
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Depreciation Expense" . "</td>";
	$htmlData .= &print_metric($htmlData, \@depreciation_expense, 0);
	$htmlData = $htmlData . "</tr>";


	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Average Payment Period (Days)
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Average Payment Period (Days)</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@average_payment_period, 1);
	$htmlData = $htmlData . "</tr>";




	#Total Current Liabilities
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Current Liabilities" . "</td>";
	$htmlData .= &print_metric($htmlData, \@current_liabilities, 0);
	$htmlData = $htmlData . "</tr>";

	#Total Operating Expenses
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Operating Expenses" . "</td>";
	$htmlData .= &print_metric($htmlData, \@total_operating_expenses, 0);
	$htmlData = $htmlData . "</tr>";


	#Total Other Expenses
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\">" . "Total Other Expenses" . "</td>";
	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-03000-0100");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";


	#Depreciation Expense
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Depreciation Expense" . "</td>";
	$htmlData .= &print_metric($htmlData, \@depreciation_expense, 0);
	$htmlData = $htmlData . "</tr>";


	#No. of Days in the Period
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "No. of Days in the Period" . "</td>";
	$htmlData .= &print_metric($htmlData, \@num_days, 0);
	$htmlData = $htmlData . "</tr>";


	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	if (1 == 2) {
	##Case-Mix Adjusted Average Length of Stay (ALOS)
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Case-Mix Adjusted Average Length of Stay (ALOS)</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "( S300001-01200-0600 / S300001-01200-1500) / $cmi{$rpt_year}");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";




	#Total Patient Days
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Patient Days" . "</td>";
	$htmlData .= &print_metric($htmlData, \@inpatient_days, 0);
	$htmlData = $htmlData . "</tr>";

	#Total Discharges
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Discharges" . "</td>";
	$htmlData .= &print_metric($htmlData, \@total_discharges, 0);
	$htmlData = $htmlData . "</tr>";


	#CMI
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "CMI" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = $cmi{$rpt_year};
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";
	}

	##Case-Mix Adjusted FTEs per AOB
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Case-Mix Adjusted FTEs per AOB</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@ftes_per_aob, 1);
	$htmlData = $htmlData . "</tr>";




	#Total No. of Beds
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total No. of Beds" . "</td>";
	$htmlData .= &print_metric($htmlData, \@total_beds, 0);
	$htmlData = $htmlData . "</tr>";

	#Total Patient Days
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Patient Days" . "</td>";
	$htmlData .= &print_metric($htmlData, \@inpatient_days, 0);
	$htmlData = $htmlData . "</tr>";


	#FTEs - Swing Beds
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\">" . "FTEs - Swing Beds" . "</td>";
	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "S300001-00400-1100");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";


	#Total Patient Revenues
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\">" . "Total Patient Revenues" . "</td>";

	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G200000-02500-0300");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";

	#Total Inpatient Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Inpatient Revenues" . "</td>";
	$htmlData .= &print_metric($htmlData, \@inpatient_revenue, 0);
	$htmlData = $htmlData . "</tr>";


	#NF Revenues
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\">" . "NF Revenues" . "</td>";
	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G200000-00700-0100");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";

	#Other LTC Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Other LTC Revenues" . "</td>";
	$htmlData .= &print_metric($htmlData, \@other_ltc_revenue, 0);
	$htmlData = $htmlData . "</tr>";


	#No. of Days in the Period
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "No. of Days in the Period" . "</td>";
	$htmlData .= &print_metric($htmlData, \@num_days, 0);
	$htmlData = $htmlData . "</tr>";

	#CMI
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\">" . "CMI" . "</td>";
	#foreach my $rpt_year (@rpt_year){
	#	$value = $cmi{$rpt_year};
	#	$value = sprintf "%.2f", $value;
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";


	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Current Asset Turnover
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Current Asset Turnover</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@current_asset_turnover, 1);
	$htmlData = $htmlData . "</tr>";




	#Net Patient Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Patient Revenues" . "</td>";
	$htmlData .= &print_metric($htmlData, \@net_patient_revenue, 0);
	$htmlData = $htmlData . "</tr>";

	#Total Current Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Current Assets" . "</td>";
	$htmlData .= &print_metric($htmlData, \@assets_in_general_fund, 0);
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";

	##Current Ratio
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Current Ratio</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@current_ratio, 1);
	$htmlData = $htmlData . "</tr>";




	#Total Current Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Current Assets" . "</td>";
	$htmlData .= &print_metric($htmlData, \@assets_in_general_fund, 0);
	$htmlData = $htmlData . "</tr>";

	#Total Current Liabilities
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Current Liabilities" . "</td>";
	$htmlData .= &print_metric($htmlData, \@current_liabilities, 0);
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";

	##Days Cash on Hand
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Days Cash on Hand</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@days_cash_on_hand, 1);
	$htmlData = $htmlData . "</tr>";




	#Cash on Hand and in Banks
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Cash on Hand and in Banks" . "</td>";
	$htmlData .= &print_metric($htmlData, \@cash_on_hand, 0);
	$htmlData = $htmlData . "</tr>";

	#No. of Days in the Period
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "No. of Days in the Period" . "</td>";
	$htmlData .= &print_metric($htmlData, \@num_days, 0);
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";

	##Days Cash on Hand - All Sources
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Days Cash on Hand - All Sources</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@days_cash_on_hand_all_sources, 1);
	$htmlData = $htmlData . "</tr>";


	#Cash on Hand and in Banks
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Cash on Hand and in Banks" . "</td>";
	$htmlData .= &print_metric($htmlData, \@cash_on_hand, 0);
	$htmlData = $htmlData . "</tr>";

	#Marketable Securities
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Marketable Securities" . "</td>";
	$htmlData .= &print_metric($htmlData, \@temporary_investments_specificpurposefund, 0);
	$htmlData = $htmlData . "</tr>";

	#Investments
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Investments" . "</td>";
	$htmlData .= &print_metric($htmlData, \@investments, 0);
	$htmlData = $htmlData . "</tr>";

	#Total Operating Expenses
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Operating Expenses" . "</td>";
	$htmlData .= &print_metric($htmlData, \@total_operating_expenses, 0);
	$htmlData = $htmlData . "</tr>";

	#No. of Days in the Period
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "No. of Days in the Period" . "</td>";
	$htmlData .= &print_metric($htmlData, \@num_days, 0);
	$htmlData = $htmlData . "</tr>";


	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";

	##Days in Net Patient AR
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Days in Net Patient AR</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@days_in_net_patient_ar, 1);
	$htmlData = $htmlData . "</tr>";


	#Accounts Receivables
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Accounts Receivables" . "</td>";
	$htmlData .= &print_metric($htmlData, \@accounts_receivable, 0);
	$htmlData = $htmlData . "</tr>";

	#Allowance for Uncollectables
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Allowance for Uncollectables" . "</td>";
	$htmlData .= &print_metric($htmlData, \@allow_for_uncollectible, 0);
	$htmlData = $htmlData . "</tr>";

	#Net Patient Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Patient Revenues" . "</td>";
	$htmlData .= &print_metric($htmlData, \@net_patient_revenue, 0);
	$htmlData = $htmlData . "</tr>";



	#No. of Days in the Period
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "No. of Days in the Period" . "</td>";
	$htmlData .= &print_metric($htmlData, \@num_days, 0);
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Days in Net Total AR
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Days in Net Total AR</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@days_in_net_total_ar, 1);
	$htmlData = $htmlData . "</tr>";


	#Accounts Receivables
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Accounts Receivables" . "</td>";
	$htmlData .= &print_metric($htmlData, \@accounts_receivable, 0);
	$htmlData = $htmlData . "</tr>";

	#Notes Receivable
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Notes Receivable" . "</td>";
	$htmlData .= &print_metric($htmlData, \@notes_receivable, 0);
	$htmlData = $htmlData . "</tr>";

	#Other Receivables
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Other Receivables" . "</td>";
	$htmlData .= &print_metric($htmlData, \@other_receivables, 0);
	$htmlData = $htmlData . "</tr>";

	#Allowance for Uncollectables
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Allowance for Uncollectables" . "</td>";
	$htmlData .= &print_metric($htmlData, \@allow_for_uncollectible, 0);
	$htmlData = $htmlData . "</tr>";

	#Net Patient Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Patient Revenues" . "</td>";
	$htmlData .= &print_metric($htmlData, \@net_patient_revenue, 0);
	$htmlData = $htmlData . "</tr>";



	#No. of Days in the Period
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "No. of Days in the Period" . "</td>";
	$htmlData .= &print_metric($htmlData, \@num_days, 0);
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Debt Financing %
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Debt Financing %</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@debt_financing_percentage, 1);
	$htmlData = $htmlData . "</tr>";


	#Total Assets
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\">" . "Total Assets" . "</td>";
	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-02700-0100");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";

	#Net Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Assets" . "</td>";
	$htmlData .= &print_metric($htmlData, \@total_fund_balances, 0);
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##EBIDAR Margin
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>EBIDAR Margin</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@ebidar_margin, 1);
	$htmlData = $htmlData . "</tr>";


	#Net Income
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Income" . "</td>";
	$htmlData .= &print_metric($htmlData, \@net_income, 0);
	$htmlData = $htmlData . "</tr>";

	#Interest Expense
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Interest Expense" . "</td>";
	$htmlData .= &print_metric($htmlData, \@interest_expense, 0);
	$htmlData = $htmlData . "</tr>";

	#Depreciation Expense
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Depreciation Expense" . "</td>";
	$htmlData .= &print_metric($htmlData, \@depreciation_expense, 0);
	$htmlData = $htmlData . "</tr>";

	#Lease Cost
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Lease Cost" . "</td>";
	$htmlData .= &print_metric($htmlData, \@lease_cost, 0);
	$htmlData = $htmlData . "</tr>";

	#Total Patient Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Patient Revenues" . "</td>";
	$htmlData .= &print_metric($htmlData, \@total_patient_revenues_amount, 0);
	$htmlData = $htmlData . "</tr>";



	#Total Other Income
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Other Income" . "</td>";
	$htmlData .= &print_metric($htmlData, \@non_operating_revenue, 0);
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Equity Financing %
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Equity Financing %</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@equity_financing_percentage, 1);
	$htmlData = $htmlData . "</tr>";


	#Fund Balance
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Fund Balance" . "</td>";
	$htmlData .= &print_metric($htmlData, \@total_fund_balances, 0);
	$htmlData = $htmlData . "</tr>";

	#Total Assets
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\">" . "Total Assets" . "</td>";
	#$htmlData .= &print_metric($htmlData, \@total_fund_balances, 0);
	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-02700-0100 + G000000-02700-0200 + G000000-02700-0300 + G000000-02700-0400");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";


	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Excess Margin
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Excess Margin</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@excess_margin, 1);
	$htmlData = $htmlData . "</tr>";


	#Net Patient Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Patient Revenues" . "</td>";
	$htmlData .= &print_metric($htmlData, \@net_patient_revenue, 0);
	$htmlData = $htmlData . "</tr>";

	#Total Operating Expenses
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Operating Expenses" . "</td>";
	$htmlData .= &print_metric($htmlData, \@total_operating_expenses, 0);
	$htmlData = $htmlData . "</tr>";

	#Total Other Income
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Other Income" . "</td>";
	$htmlData .= &print_metric($htmlData, \@non_operating_revenue, 0);
	$htmlData = $htmlData . "</tr>";


	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Fixed Asset Turnover
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Fixed Asset Turnover</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@fixed_asset_turnover, 1);
	$htmlData = $htmlData . "</tr>";


	#Net Patient Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Patient Revenues" . "</td>";
	$htmlData .= &print_metric($htmlData, \@net_patient_revenue, 0);
	$htmlData = $htmlData . "</tr>";

	#Total Fixed Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Fixed Assets" . "</td>";
	$htmlData .= &print_metric($htmlData, \@fixed_assets, 0);
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";

	##Long-Term Debt to Total Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Long-Term Debt to Total Assets</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@long_term_debt_to_total_assets, 1);
	$htmlData = $htmlData . "</tr>";


	#Total Long-Term Liabilities
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Long-Term Liabilities" . "</td>";
	$htmlData .= &print_metric($htmlData, \@long_term_liabilities, 0);
	$htmlData = $htmlData . "</tr>";

	#Total Assets
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\">" . "Total Assets" . "</td>";

	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-02700-0100");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";

	#Total Liabilities
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Liabilities" . "</td>";
	$htmlData .= &print_metric($htmlData, \@total_liabilities, 0);
	$htmlData = $htmlData . "</tr>";


	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Medicare Capital RCC
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Medicare Capital RCC</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@medicare_capital_rcc, 1);
	$htmlData = $htmlData . "</tr>";


	#Total Medicare Capital Costs
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\">" . "Total Medicare Capital Costs" . "</td>";

	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "D10A181-05200-0100");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";



	#Total Inpatient Medicare Charges
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\">" . "Total Inpatient Medicare Charges" . "</td>";

	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "D40A180-10300-0200");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";


	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Medicare Margin
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Medicare Margin</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@medicare_margin_general_acute_service_only, 1);
	$htmlData = $htmlData . "</tr>";


	#TBD1 - Subtotal (Reduced for D&C)
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\">" . "Subtotal (Reduced for D&C)" . "</td>";

	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "E00A18A-02200-0100");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";



	#TBD2 - Medicare Operations Cost
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\">" . "Medicare Operations Cost" . "</td>";

	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "D10A181-05300-0100");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";



	#TBD3 - Medicare Capital Cost
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\">" . "Medicare Capital Cost" . "</td>";

	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "D10A181-05200-0100");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Medicare Operations RCC
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Medicare Operations RCC</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@medicare_operations_rcc, 1);
	$htmlData = $htmlData . "</tr>";


	#Total Medicare Inpatient Costs (Excluding Capital Related Costs)
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\" nowrap>" . "Total Medicare Inpatient Costs (Excluding Capital Related Costs)" . "</td>";

	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "D10A181-05300-0100");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";


	#Total Inpatient Medicare Charges
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\">" . "Total Inpatient Medicare Charges" . "</td>";

	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "D40A180-10300-0200");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";


	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Occupancy Rate
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Occupancy Rate</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@occupancy_rate, 1);
	$htmlData = $htmlData . "</tr>";


	#Total Patient Days
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Patient Days" . "</td>";
	$htmlData .= &print_metric($htmlData, \@inpatient_days, 0);
	$htmlData = $htmlData . "</tr>";

	#Total Bed Days Available
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Bed Days Available" . "</td>";
	$htmlData .= &print_metric($htmlData, \@beds_available, 0);
	$htmlData = $htmlData . "</tr>";



	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Outlier Percentage
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Outlier Percentage</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@outlier_percentage, 1);
	$htmlData = $htmlData . "</tr>";



	#TBD2 - Outlier Payments After 10/1/97
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\">" . "Outlier Payments After 10/1/97" . "</td>";

	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "E00A18A-00201-0100");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";



	#TBD3 - Capital DRG outlier payments prior to 10/1/97
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\">" . "Capital DRG outlier payments prior to 10/1/97" . "</td>";

	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "L00A181-00300-0100");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";



	#TBD4 - Capital DRG Outlier Payments for Services
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\">" . "Capital DRG Outlier Payments for Services" . "</td>";

	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "L00A181-00301-0100");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";




	#TBD5 - Subprovider Total Amount Payable (reduced for D&C)
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\">" . "Subprovider Total Amount Payable (reduced for D&C)" . "</td>";

	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "E31B181-01700-0100");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";




	#TBD7 - Subtotal (Reduced for D&C)
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\">" . "Subtotal (Reduced for D&C)" . "</td>";

	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "E00A18A-02200-0100");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";



	#TBD8 - Outlier Payments after D&C
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\">" . "Outlier Payments after D&C" . "</td>";
	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "E00A18B-03200-0100");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";



	#TBD9 - SNF Total Amount due After D&C
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\">" . "SNF Total Amount due After D&C" . "</td>";
	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "E30C183-05500-0200");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Labor cost as a percent of Revenue
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Labor cost as a percent of Revenue</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@labor_cost_as_a_percent_of_revenue, 1);
	$htmlData = $htmlData . "</tr>";


	#Salary Expense
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Salary Expense" . "</td>";
	$htmlData .= &print_metric($htmlData, \@salary_expense, 0);
	$htmlData = $htmlData . "</tr>";


	#Contract Labor Expense
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\">" . "Contract Labor Expense" . "</td>";
	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "S300002-00900-0300 + S300002-00901-0300 + S300002-00902-0300 + S300002-01000-0300 + S300002-01001-0300 + S300002-01100-0300 + S300002-01200-0300 + S300002-01201-0300");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";



	#Employee Benefits
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Employee Benefits" . "</td>";
	$htmlData .= &print_metric($htmlData, \@fringe_benefits, 0);
	$htmlData = $htmlData . "</tr>";

	#Net Patient Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Patient Revenues" . "</td>";
	$htmlData .= &print_metric($htmlData, \@net_patient_revenue, 0);
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	if (1 == 2)  {
	##Quick Ratio
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Quick Ratio</b>" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(G000000-01100-0100 - G000000-00700-0100) / G000000-03600-0100");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#Total Current Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Current Assets" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-01100-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Inventory
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Inventory" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-00700-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Current Liabilities
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Current Liabilities" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-03600-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";



	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";
	}



	##RCC
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>RCC</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@ratio_of_cost_to_charges, 1);
	$htmlData = $htmlData . "</tr>";


	if (1== 2) {
	#Total Hospital Costs
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Hospital Costs" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "C000001-10100-0500");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Inpatient Charges
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Inpatient Charges" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "C000001-10100-0600");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Outpatient Charges
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Outpatient Charges" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "C000001-10100-0700");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";
	}

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";

	##Return on Assets (ROA)
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Return on Assets (ROA)</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@return_on_assets, 1);
	$htmlData = $htmlData . "</tr>";

	#Net Income
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Income" . "</td>";
	$htmlData .= &print_metric($htmlData, \@net_income, 0);
	$htmlData = $htmlData . "</tr>";


	#Total Assets
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\">" . "Total Assets" . "</td>";
	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-02700-0100");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";

	##Return on Equity (ROE)
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Return on Equity (ROE)</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@return_on_equity, 1);
	$htmlData = $htmlData . "</tr>";

	#Net Income
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Income" . "</td>";
	$htmlData .= &print_metric($htmlData, \@net_income, 0);
	$htmlData = $htmlData . "</tr>";

	#Total Assets
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\">" . "Total Assets" . "</td>";

	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-02700-0100");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";

	#Total Liabilities
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Liabilities" . "</td>";
	$htmlData .= &print_metric($htmlData, \@total_liabilities, 0);
	$htmlData = $htmlData . "</tr>";


	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";

	##Total Asset Turnover
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Total Asset Turnover</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@total_asset_turnover, 1);
	$htmlData = $htmlData . "</tr>";

	#Net Patient Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Patient Revenues" . "</td>";
	$htmlData .= &print_metric($htmlData, \@net_patient_revenue, 0);
	$htmlData = $htmlData . "</tr>";

	#Total Other Income
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Other Income" . "</td>";
	$htmlData .= &print_metric($htmlData, \@non_operating_revenue, 0);
	$htmlData = $htmlData . "</tr>";


	#Total Assets
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\">" . "Total Assets" . "</td>";
	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-02700-0100");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";

	##Total Debt to Net Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Total Debt to Net Assets</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@total_debt_to_net_assets, 1);
	$htmlData = $htmlData . "</tr>";

	#Total Liabilities
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Liabilities" . "</td>";
	$htmlData .= &print_metric($htmlData, \@total_liabilities, 0);
	$htmlData = $htmlData . "</tr>";


	#Total Assets
	#$arrayCount = 0;
	#$htmlData = $htmlData . "<tr>";
	#$htmlData = $htmlData . "<td align=\"right\">" . "Total Assets" . "</td>";
	#foreach my $rpt_year (@rpt_year){
	#	$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-02700-0100");
	#	if($arrayCount%2==0){
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
	#	}else{
	#		$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
	#	}
	#	$arrayCount = $arrayCount + 1;
	#}
	#$htmlData = $htmlData . "</tr>";


	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";

	##Total Overall Margin
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Overall Margin</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@overall_margin, 1);
	$htmlData = $htmlData . "</tr>";


	#Net Income
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Income" . "</td>";
	$htmlData .= &print_metric($htmlData, \@net_income, 0);
	$htmlData = $htmlData . "</tr>";

	#Net Patient Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Patient Revenues" . "</td>";
	$htmlData .= &print_metric($htmlData, \@net_patient_revenue, 0);
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";

	##Operating Margin
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Operating Margin</b>" . "</td>";
	$htmlData .= &print_metric($htmlData, \@operating_margin, 1);
	$htmlData = $htmlData . "</tr>";


	#Net Income from Service to Patients
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Income from Service to Patients" . "</td>";
	$htmlData .= &print_metric($htmlData, \@operating_income, 0);
	$htmlData = $htmlData . "</tr>";

	#Net Patient Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Patient Revenues" . "</td>";
	$htmlData .= &print_metric($htmlData, \@net_patient_revenue, 0);
	$htmlData = $htmlData . "</tr>";


}else{
	$htmlData="<tr><td>Please select provider number and run report again.<a href=\"/cgi-bin/Milo2/SelectReport.pl\">Back</a></td></tr>";
}
print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>Trend Appendix Reports</TITLE>
<script type="text/javascript" src="/JS/tablesort.js"></script>
<SCRIPT SRC="/JS/sorttable.js"></SCRIPT>
<SCRIPT SRC="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<SCRIPT SRC="/JS/dropmenu.js"></SCRIPT>
<link rel="stylesheet" href="/css/dropmenu.css" type="text/css" />
</HEAD>
<body>
<BR>
<FORM NAME = "frmFinacial" Action="/cgi-bin/finantial_report.pl" method="post">
<CENTER>
};
&innerHeader();
print qq{

    <!--content start-->

        <table cellpadding="0" cellspacing="0" width="885px">
	  <tr>
	        <td width="16"><img src="/images/tableRonuded/top_lef.gif" width="16" height="16"></td>
	        <td height="16" background="/images/tableRonuded/top_mid.gif"><img src="/images/tableRonuded/top_mid.gif" width="16" height="16"></td>
	        <td width="24"><img src="/images/tableRonuded/top_rig.gif" width="24" height="16"></td>
	      </tr>
	      <tr>
	        <td width="16" background="/images/tableRonuded/cen_lef.gif"></td>
	        <td align="center" valign="middle" bgcolor="#FFFFFF">

<table border="1em" bgColor="#FFFFFF" width="100%" cellpadding="0" cellspacing="0">
<tr class='gridHeader'><td nowrap colspan=$recCount><b>Trend Appendix Report for Provider : </b><font style="color:#C11B17;">$hospitalName - $providerid</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"/cgi-bin/Milo2/SelectReport.pl\">Back</a></td></tr>
$htmlData
</table>
</td>
		<td width="24" background="/images/tableRonuded/cen_rig.gif"><img src="/images/tableRonuded/cen_rig.gif" width="24" height="11"></td>
	      </tr>
	      <tr>
		    <td width="16" height="16"><img src="/images/tableRonuded/bot_lef.gif" width="16" height="16"></td>
		    <td height="16" background="/images/tableRonuded/bot_mid.gif"><img src="/images/tableRonuded/bot_mid.gif" width="16" height="16"></td>
		    <td width="24" height="16"><img src="/images/tableRonuded/bot_rig.gif" width="24" height="16"></td>
	      </tr>
   </table>
};
&TDdoc;
print qq{
</body>
</HTML>
};
#--------------------------------------------------------------------------------------

sub sessionExpire
{

print "Content-Type: text/html\n\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};
&headerScript();
print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}
#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#--------------------------------------------------------------------------------

sub getFormulae
{
	my ($report_id, $yyyy, $formulae) = @_;
	my ($value);
	#$value = &getFormulaeValue($report_id,"(G200000-002500-0200)/(G200000-002500-0300)",$yyyy);

	$value = &getFormulaeValue($report_id,$formulae,$yyyy);

	#EBITDAR as a Percent of Interest Expense
	#Net Revenue per FTE

	return $value;
}


#----------------------------------------------------------------------------

sub getFormulaeValue
{
	my ($report_id,$formulae,$yyyy) = @_;
	my ($value, $i, $elementVal, $operation);

	for ($i=0;$i<length($formulae);$i++)
	{
		if(substr($formulae, $i,1) =~ /[A-Z]/ )
		{
			#Spliting elements from Address
			my ($worksheet, $line, $column) = split /[-:]/,substr($formulae, $i,18);
			if(&get_value($report_id, $worksheet, $line, $column, $yyyy))
			{
				$elementVal = &get_value($report_id, $worksheet, $line, $column, $yyyy);
			}
			else
			{
				$elementVal = 0;
			}


			$value = $value.$elementVal;
			$i = $i+17;
		}
		elsif(substr($formulae, $i,1) =~ /\s/ )
		{

		}
		else
		{

			$value = $value . substr($formulae, $i,1);
		}
	}
	$value = eval($value);
	if(!$value)
	{
		$value = "0";
	}
	else
	{
		#$value = sprintf "%.2f", $value;
		$value = $value;
	}
	return $value;

}

#------------------------------------------------------------------------------

sub get_value
{
   my ($report_id, $worksheet, $line, $column, $fYear) = @_;

   my $yyyy = $fYear;

   my ($sthVal, $value);

   my $sql = qq(
    SELECT CR_VALUE
      FROM CR_ALL_DATA
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
   );

   unless ($sthVal = $dbh->prepare($sql)) {
       &display_error('Error preparing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sthVal->execute($report_id, $worksheet, $line, $column)) {
       &display_error('Error executing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

   if ($sthVal->rows) {
      $value = $sthVal->fetchrow_array;
   }

   $sthVal->finish();

   return $value;
}

#------------------------------------------------------------------------------

sub numNegFormat
{
   my ($val,$dollar) = @_;
   if($val<0){
   	$val = $val*-1;
   	$val= Currency_Format($val);
   	if($dollar){
   		$val = "(" . "\$" . $val . ")";
   	}
   	else{
   		$val = "(" . $val . ")";
   	}
   }
   else{
   	if($dollar){
   		$val= "\$" . Currency_Format($val);
   	}
   	else{
   		$val= Currency_Format($val);
   	}
   }
   return $val;
}

#------------------------------------------------------------------------------

sub USA_Format {
(my $n = shift) =~ s/\G(\d{1,3})(?=(?:\d\d\d)+(?:\.|$))/$1,/g;
return "\$$n";
}

#------------------------------------------------------------------------------

sub Currency_Format {
(my $n = shift) =~ s/\G(\d{1,3})(?=(?:\d\d\d)+(?:\.|$))/$1,/g;
return "$n";
}

sub Percentage_Format {
	my ($n) = @_;
	return sprintf("%.2f%", ($n*100));
}


sub print_metric
{
	my($htmlData, $metric_array, $is_bold) = @_;

	my $htmlDataInner;

	my $countALL = 0;
	foreach my $metric (@$metric_array)
	{
		if($countALL%2==0)
		{
			if ($is_bold)
			{
				$htmlDataInner .= "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($metric) . "</b></td>";
			}
			else
			{
				$htmlDataInner .= "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($metric) . "</td>";
			}
		}
		else
		{
			if ($is_bold)
			{
				$htmlDataInner .= "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($metric) . "</b></td>";
			}
			else
			{
				$htmlDataInner .= "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($metric) . "</td>";
			}

		}
		$countALL++;
	}
	return $htmlDataInner;
}


sub print_num_days
{
	my($htmlData, $fy_end_date, $fy_bgn_date, $is_bold) = @_;

	my $htmlDataInner;

	my $countALL = 0;
	foreach my $metric (@$fy_end_date)
	{
		if($countALL%2==0)
		{
			if ($is_bold)
			{
				$htmlDataInner .= "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($metric - $$fy_bgn_date[$countALL]) . "</b></td>";
			}
			else
			{
				$htmlDataInner .= "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($metric - $$fy_bgn_date[$countALL]) . "</td>";
			}
		}
		else
		{
			if ($is_bold)
			{
				$htmlDataInner .= "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($metric - $$fy_bgn_date[$countALL]) . "</b></td>";
			}
			else
			{
				$htmlDataInner .= "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($metric - $$fy_bgn_date[$countALL]) . "</td>";
			}

		}
		$countALL++;
	}
	return $htmlDataInner;
}

#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach my $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}
