#!/usr/bin/perl

use CGI;
use DBI;

do "../common_header.pl";
do "../pagefooter.pl";
my $cgi = new CGI;
use Date::Calc qw(Delta_Days);
use CGI::Session;
do "../audit.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");

my $providerid = $session->param("providerid");
#my $providerid ="171354";

if(!$userID)
{
	&sessionExpire;
}

&view("View","Generate quartile report","Milo2");
# Database

my $dbh;
my ($sql, $sth,$stmt);


# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};

my ($years,$yearLoopCount,$htmlData,$hospitalName);

$yearLoopCount = 0;
@fYears = reverse(@fYears);
foreach my $fYears (@fYears){
	if($yearLoopCount <5){
		if($years){
			$years = $years . "," . $fYears;
		}
		else{
			$years = $fYears;
		}
	}
	$yearLoopCount = $yearLoopCount + 1;
}

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

#Get the provider name
if($providerid){
	$sql = "Select HospitalName as HOSP_NAME from tblhospitalmaster where ProviderNo = $providerid";
	$stmt = $dbh->prepare($sql);
	$stmt->execute();
	while ($row = $stmt->fetchrow_hashref) {
		$hospitalName = $$row{HOSP_NAME};
	}
}

#Declare cmi variable
my %cmi;

	#selected matrics
	my $matrics=$session->param("matrics");

	#retrieve hash for matrics battleship coordinates
	my $matrics_battles=$session->param("matrics_battles");

	#retrieve hash for matrics order
	my $matrics_order=$session->param("matrics_order");

#Generate Report

my(@rpt_rec_num,@rpt_year,@fy_bgn_dt,@fy_end_dt,$recCount);
$recCount = 1;
if($providerid){
$prevprovider="Select PreviousProviderNo from tblpreviousprovider where CurrentProviderNo = '$providerid' ";#($providerid,$prevprovider)

	$sth = $dbh->prepare($prevprovider);
	$sth->execute();

	 while ($row = $sth->fetchrow_hashref){
	 push (@prev_prov, $$row{PreviousProviderNo});
	 }

	 foreach my $prev_prov(@prev_prov){
	$providerid=$providerid.",".$prev_prov;
	}
	$sql = "SELECT RPT_REC_NUM, year(FY_END_DT) as RPT_YEAR, RPT_STUS_CD, FI_NUM, FY_BGN_DT, FY_END_DT FROM CR_ALL_RPT WHERE PRVDR_NUM in ($providerid) AND year(FY_END_DT)  in ($years) ORDER BY RPT_YEAR desc ";
	$sth = $dbh->prepare($sql);
	$sth->execute();

	while ($row = $sth->fetchrow_hashref) {
		push (@rpt_rec_num, $$row{RPT_REC_NUM});
		push (@rpt_year, $$row{RPT_YEAR});
		push (@fy_bgn_dt, $$row{FY_BGN_DT});
		push (@fy_end_dt, $$row{FY_END_DT});
		$recCount++;
	}


	$sql = "SELECT unadjusted_cmi,year FROM cmi WHERE provider_number in ($providerid)";
		$sth = $dbh->prepare($sql);
		$sth->execute();
		while($row = $sth->fetchrow_hashref){
			$cmi{$$row{year}}=$$row{unadjusted_cmi};
	}

	#Starting a new Row

	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td colspan=3 width=100px align=\"center\"><table cellspacing=2 cellpadding=2 border=1>";


	#FY
	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=6><b>".$hospitalName." - Facility of Interest</b></td></tr><tr>";
	$htmlData = $htmlData . "<td nowrap><b>Metric</b></td>";
	my $countY = 0;
	foreach my $rpt_year (@rpt_year){
		if($countY%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . $rpt_year . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . $rpt_year . "</b></td>";
		}
		$countY++;
	}

	$htmlData = $htmlData . "</tr>";



	if(@{$matrics}){
		#Display Report
		foreach my $mat (@{$matrics}){
			$arrayCount = 0;
			$htmlData = $htmlData . "<tr>";
			$htmlData = $htmlData . "<td>" . $mat . "</td>";

			foreach my $rpt_year (@rpt_year){

				if(($mat eq "Case-Mix Adjusted Average Length of Stay (ALOS)") || ($mat eq "Case-Mix Adjusted FTEs per AOB"))
				{
				#$htmlData = $htmlData."Hello" .$mat ."</br>";
				$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "$$matrics_battles{$mat}/$cmi{$rpt_year}");
				$value = sprintf "%.2f", $value;
				if($arrayCount%2==0){
					$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
				}else{
					$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
				}
				$arrayCount = $arrayCount + 1;
				}elsif ($mat eq "Operating Margin"){
				$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "$$matrics_battles{$mat}");
				$value=&valpercentage($value);
				if($arrayCount%2==0){
			if(($value >= "-26.00f") && ($value <= "26.00f" )) {
				$value=&numNegFormat($value);
				$value = $value."%";
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $value . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . "N/A". "</td>";
		}
		}elsif(($value >= "-26.00f") && ($value <= "26.00f") ){
			$value=&numNegFormat($value);
			$value = $value."%";
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $value . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" ."N/A". "</td>";
		}

		$arrayCount = $arrayCount + 1;
				}elsif($mat eq "Overall Margin"){
					$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "$$matrics_battles{$mat}");
					$value=&valpercentage($value);
				if($arrayCount%2==0){
if(($value >= "-19.00f") && ($value <= "26.00f" )) {
			$value=&numNegFormat($value);
			$value = $value."%";
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" .$value. "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" ."N/A". "</td>";
		}
		}elsif(($value >= "-19.00f") && ($value <= "26.00f") ){
			$value=&numNegFormat($value);
			$value = $value."%";
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" .$value. "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" ."N/A". "</td>";

		}
		$arrayCount = $arrayCount + 1;
				}elsif($mat eq "Average Age of Plant"){
				$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "$$matrics_battles{$mat}");
				$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
		if(($value >= "0.00f") && ($value <= "21.24f") ){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
		    $htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" ."N/A". "</td>";
		}
		}elsif(($value >= "0.00f") && ($value <= "21.24f") ){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}else{
		    $htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . "N/A" . "</td>";

		}
		$arrayCount = $arrayCount + 1;
				}elsif ($mat eq "Average Payment Period (Days)"){
				#$htmlData = $htmlData."Hello" .$mat ."</br>";
				$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "$$matrics_battles{$mat}");
				#$htmlData = $htmlData."Hello" .$value ."</br>";
				$value = sprintf "%.2f", $value;
				if($arrayCount%2==0){
					if(($value >= "7.13f") && ($value <= "105.18f") ){
						$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
					}else{
						$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" ."N/A" . "</td>";
					}
		}elsif(($value >= "7.13f") && ($value <= "105.18f") ){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}else{
		     $htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" ."N/A" . "</td>";

		}
		$arrayCount = $arrayCount + 1;
				}elsif($mat eq "Labor cost as a percent of Revenue"){
				$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "$$matrics_battles{$mat}");
				$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
if(($value >= "0.29f") && ($value <= "0.69f") ){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
		     $htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" ."N/A" . "</td>";
		}
		}elsif(($value >= "0.29f") && ($value <= "0.69f") ){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}else{
		     $htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" ."N/A" . "</td>";
		}

		$arrayCount = $arrayCount + 1;
				}elsif($mat eq "FTEs per AOB"){
				$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "$$matrics_battles{$mat}");
				$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
if(($value >= "1.78f") && ($value <= "8.48f") ){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
		     $htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" ."N/A" . "</td>";
		}
		}elsif(($value >= "1.78f") && ($value <= "8.48f") ){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}else{
		     $htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" ."N/A" . "</td>";
		}

		$arrayCount = $arrayCount + 1;
				}
				else {
				$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "$$matrics_battles{$mat}");

				$value = sprintf "%.2f", $value;
				if($arrayCount%2==0){
					$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
				}else{
					$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
				}
				$arrayCount = $arrayCount + 1;
					}
			}
			$htmlData = $htmlData . "</tr>";
		}
	$htmlData = $htmlData . "</table></td>";
	$htmlData = $htmlData . "</tr>";

	}else
		{
		print qq{ <tr><td>No matrics selected<a href= "/cgi-bin/Milo2/select_element.pl ">Back</a></td></tr> };
		}

}else{
	$htmlData="<tr><td>Please select provider number and run report again.<a href=\"/cgi-bin/Milo2/SelectReport.pl\">Back</a></td></tr>";
}
print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>Quartile Reports</TITLE>
<SCRIPT SRC="/JS/sorttable.js"></SCRIPT>
<SCRIPT SRC="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<SCRIPT SRC="/JS/dropmenu.js"></SCRIPT>
<link rel="stylesheet" href="/css/dropmenu.css" type="text/css" />
</HEAD>
<body>
<BR>
<FORM NAME = "frmFinacial" Action="/cgi-bin/finantial_report.pl" method="post">
<CENTER>
};
&innerHeader();
print qq{

    <!--content start-->

        <table cellpadding="0" cellspacing="0" width="885px">
	  <tr>
	        <td width="16"><img src="/images/tableRonuded/top_lef.gif" width="16" height="16"></td>
	        <td height="16" background="/images/tableRonuded/top_mid.gif"><img src="/images/tableRonuded/top_mid.gif" width="16" height="16"></td>
	        <td width="24"><img src="/images/tableRonuded/top_rig.gif" width="24" height="16"></td>
	      </tr>
	      <tr>
	        <td width="16" background="/images/tableRonuded/cen_lef.gif"></td>
	        <td align="center" valign="middle" bgcolor="#FFFFFF">

<table border="1em" bgColor="#FFFFFF" width="100%" cellpadding="0" cellspacing="0">
<tr class='gridHeader'><td nowrap colspan=$recCount><b>Quartile Report for Provider : </b><font style="color:#C11B17;">$hospitalName ($providerid)</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"/cgi-bin/Milo2/SelectReport.pl\">Back</a></td></tr>
$htmlData

<tr bgColor= "#EBDDE2 "><td colspan=3>&nbsp;</td></tr>
<!--Quartile Ranking-->
};
print qq{<tr><td><div style= "position:static;width:850px;overflow:scroll; " ><table cellspacing="0" cellpadding="0" ><tr>};

for(my $i=1; $i<=3; $i++){
			if($i==1){
				$area="National";
				@Provider=@{$session->param("Provider_List_National")};
				$Total_Cost_Report=scalar(@Provider);
				#print "<script type='text/javascript'>window.alert($Total_Cost_Report);</script>";
			}
			elsif($i==2){
				$area="State";
				@Provider=@{$session->param("Provider_List_State")};
				$Total_Cost_Report=scalar(@Provider);
			}
			else {
				$area="Locale";
				@Provider=@{$session->param("Provider_List_Locale")};
				$Total_Cost_Report=scalar(@Provider);
			}
			print qq{	<td><table border= "1">
		     <tr><td colspan=6 align= "center "><b>Quartile Ranking - $area</b></td></tr>
		     <tr><td>Metrics</td>
			};

	    $countY = 0;
		foreach my $rpt_year (@rpt_year){
		#print "<script type='text/javascript'>window.alert('$rpt_year');</script>";
			if($countY%2==0){
				print qq{
				<!--#Year -->
				  <td style=\"background-color:#F5F5DC\"><b> $rpt_year </b></td>
			};
			}else {
				print qq{
				<!--#Year-->
				  <td style=\"background-color:#F0FFFF\"><b>$rpt_year </b></td>
				};
			}
			$countY++;
		}
		print "</tr>";

		#print scalar(@{$matrics});
		foreach my $mat (@{$matrics}){
			#print scalar(@{$matrics});
		#Display National, State and Locale
			print qq{<tr><td>$mat</td>};
			$countY = 0;
			foreach my $rpt_year (@rpt_year){
			#print "<script type='text/javascript'>window.alert('$rpt_year');</script>";
				if($countY%2==0){
					print qq{
					<!--#Year -->
					  <td style=\"background-color:#F5F5DC\"><b> </b></td>
					};
				}else {
					print qq{
					<!--#Year-->
					  <td style=\"background-color:#F0FFFF\"><b> </b></td>
					};
				}
				$countY++;
			}
			print qq{</tr>};
		}
	print qq{</table></td>};

}
print qq{</table></div></td></tr>};

print qq{
</table>
</td>
		<td width="24" background="/images/tableRonuded/cen_rig.gif"><img src="/images/tableRonuded/cen_rig.gif" width="24" height="11"></td>
	      </tr>
	      <tr>
		    <td width="16" height="16"><img src="/images/tableRonuded/bot_lef.gif" width="16" height="16"></td>
		    <td height="16" background="/images/tableRonuded/bot_mid.gif"><img src="/images/tableRonuded/bot_mid.gif" width="16" height="16"></td>
		    <td width="24" height="16"><img src="/images/tableRonuded/bot_rig.gif" width="24" height="16"></td>
	      </tr>
   </table>
};
&TDdoc;
print qq{
</body>
</HTML>
};
#--------------------------------------------------------------------------------------

sub sessionExpire
{

print "Content-Type: text/html\n\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};
&headerScript();
print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";
print qq{
</body>
</html>
};

}
#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#--------------------------------------------------------------------------------

sub getFormulae
{
	my ($report_id, $yyyy, $formulae) = @_;
	my ($value);
	#$value = &getFormulaeValue($report_id,"(G200000-002500-0200)/(G200000-002500-0300)",$yyyy);

	$value = &getFormulaeValue($report_id,$formulae,$yyyy);

	#EBITDAR as a Percent of Interest Expense
	#Net Revenue per FTE

	return $value;
}


#----------------------------------------------------------------------------

sub getFormulaeValue
{
	my ($report_id,$formulae,$yyyy) = @_;
	my ($value, $i, $elementVal, $operation);

	for ($i=0;$i<length($formulae);$i++)
	{
		if(substr($formulae, $i,1) =~ /[A-Z]/ )
		{
			#Spliting elements from Address
			my ($worksheet, $line, $column) = split /[-:]/,substr($formulae, $i,18);
			if(&get_value($report_id, $worksheet, $line, $column, $yyyy))
			{
				$elementVal = &get_value($report_id, $worksheet, $line, $column, $yyyy);
			}
			else
			{
				$elementVal = 0;
			}


			$value = $value.$elementVal;
			$i = $i+17;
		}
		elsif(substr($formulae, $i,1) =~ /\s/ )
		{

		}
		else
		{

			$value = $value . substr($formulae, $i,1);
		}
	}
	$value = eval($value);
	if(!$value)
	{
		$value = "0";
	}
	else
	{
		#$value = sprintf "%.2f", $value;
		$value = $value;
	}
	return $value;

}

#------------------------------------------------------------------------------

sub get_value
{
   my ($report_id, $worksheet, $line, $column, $fYear) = @_;

   my $yyyy = $fYear;

   my ($sthVal, $value);

   my $sql = qq(
    SELECT CR_VALUE
        FROM CR_ALL_DATA
		WHERE CR_REC_NUM    = ?
	   AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?

   );

   unless ($sthVal = $dbh->prepare($sql)) {
       &display_error('Error preparing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sthVal->execute($report_id, $worksheet, $line, $column)) {
       &display_error('Error executing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

   if ($sthVal->rows) {
      $value = $sthVal->fetchrow_array;
   }

   $sthVal->finish();

   return $value;
}

#------------------------------------------------------------------------------

sub numNegFormat
{
   my ($val,$dollar) = @_;
   if($val<0){
   	$val = $val*-1;
   	$val= Currency_Format($val);
   	if($dollar){
   		$val = "(" . "\$" . $val . ")";
   	}
   	else{
   		$val = "(" . $val . ")";
   	}
   }
   else{
   	if($dollar){
   		$val= "\$" . Currency_Format($val);
   	}
   	else{
   		$val= Currency_Format($val);
   	}
   }
   return $val;
}
#--------------------------------------
sub valpercentage
{
	my ($val)=@_;
	$val=$val*100;
	$val = sprintf "%.2f", $val;
	#$val=$val."%";
	return $val;

}
#------------------------------------------------------------------------------

sub USA_Format {
(my $n = shift) =~ s/\G(\d{1,3})(?=(?:\d\d\d)+(?:\.|$))/$1,/g;
return "\$$n";
}

#------------------------------------------------------------------------------

sub Currency_Format {
(my $n = shift) =~ s/\G(\d{1,3})(?=(?:\d\d\d)+(?:\.|$))/$1,/g;
return "$n";
}

#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>};
	&TDdoc;
print qq{
</BODY>
</HTML>
};

}
