#!/usr/bin/perl

use CGI;
use DBI;
use strict;
require "/var/www/cgi-bin/lib/req-milo.pl";

my $cgi = new CGI;
use Date::Calc qw(Delta_Days);
use CGI::Session;
do "../audit.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $lowest_year = $session->param("lowest_year");

my $providerid = $session->param("providerid");
#my $providerid ="171354";

if(!$userID)
{
	&sessionExpire;
}


my $hospital_name = "";

#Get the provider name
if($providerid){
	my $sql = "select hospitalname as hosp_name from tblhospitalmaster where providerno = $providerid";
	my $rs = &run_mysql($sql,4);
	$hospital_name = $$rs {0}{'hosp_name'};
}
#------------------------------------------------------------------

my $Total_Cost_Report=0;

#selected matrics
my $matrics=$session->param("matrics");

#retrieve hash for matrics battleship coordinates
my $matrics_battles=$session->param("matrics_battles");
my $metrics_fieldnames=$session->param("metrics_fieldnames");

#retrieve hash for matrics order
my $matrics_order=$session->param("matrics_order");


my $quartile = $cgi->param("quartile");
my $metric = $cgi->param("metric");
my $quartile_year = $cgi->param("quartile_year");
my $area = $cgi->param("area");
my $metric_column_name = $$metrics_fieldnames{$metric};
my $key_name = "PROVIDER-HASH|$area|$metric_column_name";
my $provider_hash = $session->param($key_name);
my $metric2 = $metric;
$metric2 =~ s/ /_/sig;
my $file_name = "$providerid-$area-$quartile_year-Q$quartile-$metric2.csv";

print "Content-type: test/csv\n";
print "Content-Disposition: attachment; filename=$file_name\n";
print "Cache-Control: no-cache\n";
print "Pragma: no-cache\n\n";
print "Metric: $metric\n";
print "Provider: $providerid, Area: $area, Year: $quartile_year, Quartile: Q$quartile\n\n";
print "Provider No,Name,Address,City,State,Zip,Metric Value\n";


my $count = 1;
foreach my $provider_num (sort {$$provider_hash{$quartile_year}{$quartile}{"PROVIDERS"}{$b} <=> $$provider_hash{$quartile_year}{$quartile}{"PROVIDERS"}{$a}}keys %{$$provider_hash{$quartile_year}{$quartile}{"PROVIDERS"}}) {
	my $sql = "select hospitalname, hospitalstreet, hospitalcity, hospitalstate, shortzip from tblhospitalmaster where providerno = '$provider_num'";
	my $rs = &run_mysql($sql,4);
	my $hospital_name = $$rs{0}{'hospitalname'};
	my $hospital_address = $$rs{0}{'hospitalstreet'};
	my $hospital_city = $$rs{0}{'hospitalcity'};
	my $hospital_state = $$rs{0}{'hospitalstate'};
	my $hospital_zipcode = $$rs{0}{'shortzip'};
	my $metric_value = $$provider_hash{$quartile_year}{$quartile}{"PROVIDERS"}{$provider_num};
	print "$provider_num,$hospital_name,$hospital_address,$hospital_city,$hospital_state,$hospital_zipcode,$metric_value\n";
	$count++;
}
&view("Download","Quartile appendix report detailed","Milo2");