#!/usr/bin/perl

use CGI;
use DBI;

do "../pagefooter.pl";
do "../common_header.pl";
my $cgi = new CGI;

use CGI::Session;

my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");

if(!$userID)
{
	&sessionExpire;
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>Milo 2.0</TITLE>
 <script type="text/javascript" src="/JS/AjaxScripts/prototype.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/effects.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/controls.js"></script>
</HEAD>
<BODY>
<BR>
<FORM NAME = "frmFinacial" Action="/cgi-bin/Milo2/search.pl" method="post">
<CENTER>
};
&innerHeader();
print qq{

    <!--content start-->

        <table cellpadding="0" cellspacing="0" width="885px">
	  <tr>
	        <td width="16"><img src="/images/tableRonuded/top_lef.gif" width="16" height="16"></td>
	        <td height="16" background="/images/tableRonuded/top_mid.gif"><img src="/images/tableRonuded/top_mid.gif" width="16" height="16"></td>
	        <td width="24"><img src="/images/tableRonuded/top_rig.gif" width="24" height="16"></td>
	      </tr>
	      <tr>
	        <td width="16" background="/images/tableRonuded/cen_lef.gif"></td>
	        <td align="center" valign="middle" bgcolor="#FFFFFF">
			<table style="border:1px solid gray;"  cellpadding="3" cellspacing="3" align="center" width="100%" >
				<tr class="gridheader">
					<td>
						<b>Enter Provider Number</b>
					</td>

				</tr>
				<tr>
				    <td valign="middle">
					<table align="center" width="50%" cellpadding="2" cellspacing="2">
					    <tr>
						<td>
							Provider Number :
						</td>
						<td>
							<input type="text" name="txtProviderNo" style="width:250px;" />
						</td>
					    </tr>
					    <tr align="center">
					    	<td colspan=2>
					    		<input type="submit" name="submit" value="submit" />
					    	</td>
					    </tr>
					</table>
				   </td>
				</tr>
			</table>
		</td>
		<td width="24" background="/images/tableRonuded/cen_rig.gif"><img src="/images/tableRonuded/cen_rig.gif" width="24" height="11"></td>
	      </tr>
	      <tr>
		    <td width="16" height="16"><img src="/images/tableRonuded/bot_lef.gif" width="16" height="16"></td>
		    <td height="16" background="/images/tableRonuded/bot_mid.gif"><img src="/images/tableRonuded/bot_mid.gif" width="16" height="16"></td>
		    <td width="24" height="16"><img src="/images/tableRonuded/bot_rig.gif" width="24" height="16"></td>
	      </tr>
   </table>
 <table cellpadding="0" cellspacing="0" width="885px">
   	  <tr>
   	        <td width="16"><img src="/images/tableRonuded/top_lef.gif" width="16" height="16"></td>
   	        <td height="16" background="/images/tableRonuded/top_mid.gif"><img src="/images/tableRonuded/top_mid.gif" width="16" height="16"></td>
   	        <td width="24"><img src="/images/tableRonuded/top_rig.gif" width="24" height="16"></td>
   	      </tr>
   	      <tr>
   	        <td width="16" background="/images/tableRonuded/cen_lef.gif"></td>
   	        <td align="center" valign="middle" bgcolor="#FFFFFF">
   			<iframe style="z-index:0;" src="/cgi-bin/Milo2/autocompleter.pl" width="100%" height="300px"></iframe>
   		</td>
   		<td width="24" background="/images/tableRonuded/cen_rig.gif"><img src="/images/tableRonuded/cen_rig.gif" width="24" height="11"></td>
   	      </tr>
   	      <tr>
   		    <td width="16" height="16"><img src="/images/tableRonuded/bot_lef.gif" width="16" height="16"></td>
   		    <td height="16" background="/images/tableRonuded/bot_mid.gif"><img src="/images/tableRonuded/bot_mid.gif" width="16" height="16"></td>
   		    <td width="24" height="16"><img src="/images/tableRonuded/bot_rig.gif" width="24" height="16"></td>
   	      </tr>
   </table>};
	&TDdoc;
	print qq{
	<!--content end-->

</CENTER>
</FORM>
</BODY>
</HTML>
};
#--------------------------------------------------------------------------------------

sub sessionExpire
{

print "Content-Type: text/html\n\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};
&headerScript();
print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}
#-----------------------------------------------------------------------------

#Perl trim function to remove whitespace from the start and end of the string

sub trim
{
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}

