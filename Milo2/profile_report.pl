#!/usr/bin/perl

use CGI;
use DBI;

do "../common_header.pl";
do "../pagefooter.pl";
my $cgi = new CGI;
use Date::Calc qw(Delta_Days);
use CGI::Session;
do "../audit.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $status = 0;
$status = $cgi->param("status");
my $providerid ;
if($status == 1){
my $provider  = $cgi->param('provider');
$session->param("providerid",$provider);
}else{
$providerid = $session->param("providerid");
}
#my $providerid = "171354";
$providerid = $session->param("providerid");
if(!$userID)
{
	&sessionExpire;
}

&view("View","Generate profile report","Milo2");
# Database

my $dbh;
my ($sql, $sth,$stmt);


# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};

my $dispYear ="";

my ($years,$yearLoopCount,$htmlData,$hospitalName);

$yearLoopCount = 0;
@fYears = reverse(@fYears);
foreach my $fYears (@fYears){
	if($yearLoopCount <3){
		if($years){
			$years = $years . "," . $fYears;
		}
		else{
			$years = $fYears;
		}
	}
	$yearLoopCount = $yearLoopCount + 1;
}

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

#Get the provider name
if($providerid){
	$sql = "Select HospitalName as HOSP_NAME from tblhospitalmaster where ProviderNo = $providerid";
	$stmt = $dbh->prepare($sql);
	$stmt->execute();
	while ($row = $stmt->fetchrow_hashref) {
		$hospitalName = $$row{HOSP_NAME};
	}
}

#Generate Report

my($rpt_rec_num,$rpt_year,$fy_bgn_dt,$fy_end_dt,$recCount);
my ($Hospital_Name, $Street, $City, $State, $Zip_Code, $Type_of_Facility, $Type_of_Control, $HealthCareSystem);
$recCount = 1;
if($providerid){
	$sql = "SELECT c.RPT_REC_NUM, year(c.FY_END_DT) as RPT_YEAR, c.RPT_STUS_CD, c.FI_NUM, c.FY_BGN_DT, c.FY_END_DT, b.hospital_name, b.address, b.city, b.state, b.zip_code, b.system_name, ct.control_type_name, ht.hospital_type_name
			FROM CR_ALL_RPT c INNER JOIN _tbl_provider_metrics b ON c.rpt_rec_num = b.rpt_rec_num AND c.formatid = b.formatid
			INNER JOIN control_type ct ON b.type_of_control = ct.control_type_id
			INNER JOIN hospital_type ht ON b.hospital_type = ht.hospital_type_id
			WHERE c.PRVDR_NUM = '$providerid' ORDER BY c.FY_END_DT DESC LIMIT 1";
	$sth = $dbh->prepare($sql);
	$sth->execute();

	while ($row = $sth->fetchrow_hashref) {
		$rpt_rec_num =  $$row{RPT_REC_NUM};
		$rpt_year = $$row{RPT_YEAR};
		$fy_bgn_dt = $$row{FY_BGN_DT};
		$fy_end_dt = $$row{FY_END_DT};

		$Hospital_Name = $$row{hospital_name};
		$Street = $$row{address};
		$City = $$row{city};
		$State = $$row{state};
		$Zip_Code = $$row{zip_code};
		$Type_of_Facility = $$row{hospital_type_name};
		$Type_of_Control = $$row{control_type_name};
		$HealthCareSystem = $$row{system_name};
		$recCount++;
	}
	open(J, ">data.txt");
	print J $sql;
	close J;
	# Background Information data.

	#my $Hospital_Name = $hospitalName;
	my $Provider_Number = $providerid;
	#my $Street = &get_value($rpt_rec_num,"S200000", "00100", "0100",$rpt_year);
	#my $State = &get_value($rpt_rec_num,"S200000","00101","0200",$rpt_year);
	#my  $City = "";
	#my $Zip_Code ="";

	#if($providerid){
	#	$sql = "Select HospitalCity as CITY,HospitalZipCode as ZIP_CODE from tblhospitalmaster where ProviderNo = $providerid";
	#	$stmt = $dbh->prepare($sql);
	#	$stmt->execute();
	#	while ($row = $stmt->fetchrow_hashref) {
	#		$City = $$row{CITY};
	#		$Zip_Code = $$row{ZIP_CODE};
	#	}
	#}

	my $Telephone_number = "";
	#my $Type_of_Facility_id = &getFormulae($rpt_rec_num, $rpt_year, "S200000-01900-0100");
	#my $Type_of_Facility = "";
	#if($Type_of_Facility_id){
	#	$sql = "Select Hospital_Type_Name from Hospital_Type where Hospital_Type_id = $Type_of_Facility_id";
	#	$stmt = $dbh->prepare($sql);
	#	$stmt->execute();
	#	while ($row = $stmt->fetchrow_hashref) {
	#		$Type_of_Facility = $$row{Hospital_Type_Name};
	#	}
	#}

	#my $Type_of_Control_id = &getFormulae($rpt_rec_num, $rpt_year, "S200000-01800-0100");
	#my $Type_of_Control = "";
	#if($Type_of_Control_id){
	#	$sql = "Select Control_Type_Name from Control_Type where Control_Type_id = $Type_of_Control_id";
	#	$stmt = $dbh->prepare($sql);
	#	$stmt->execute();
	#	while ($row = $stmt->fetchrow_hashref) {
	#		$Type_of_Control = $$row{Control_Type_Name};
	#	}
	#}

	#my $HealthCareSystem = &get_value($rpt_rec_num,"S200000","04001","0100",$rpt_year);
	my $Total_Employees = &getFormulae($rpt_rec_num, $rpt_year, "S300001-01200-1000");
	my $Total_Discharges  = &getFormulae($rpt_rec_num, $rpt_year, "S300001-01200-1300");
	my $Total_Patient_Days = &getFormulae($rpt_rec_num, $rpt_year, "S300001-01200-0600");
	my $General_MedSurg_Beds = &getFormulae($rpt_rec_num, $rpt_year, "S300001-01200-0100");
	my $Special_Care_Beds = &getFormulae($rpt_rec_num, $rpt_year, "S300001-01200-0100 - S300001-02500-0100");


	## Acute Utilization Statistics by Payor for Beds

	my $Routine_Services_beds = &getFormulae($rpt_rec_num, $rpt_year, "S300001-00500-0100");
	my $Intensive_Care_Unit_beds	= &getFormulae($rpt_rec_num, $rpt_year, "S300001-00600-0100");
	my $Coronary_Intensive_Care_beds = &getFormulae($rpt_rec_num, $rpt_year, "S300001-00700-0100");
	my $Total_Acute_beds = &getFormulae($rpt_rec_num, $rpt_year, "S300001-01200-0100");

	## Acute Utilization Statistics by Payor for Revenue.

	my $Routine_Services_Revenue = &getFormulae($rpt_rec_num, $rpt_year, "G200000-00900-0100");
	my $Intensive_Care_Unit_Revenue	= &getFormulae($rpt_rec_num, $rpt_year, "G200000-01000-0100");
	my $Coronary_Intensive_Care_Revenue = &getFormulae($rpt_rec_num, $rpt_year, "G200000-01100-0100");
	my $Total_Acute_Revenue = &getFormulae($rpt_rec_num, $rpt_year, "G200000-01600-0100");

	# Acute Utilization Statistics by Payor for Medicare.

	my $Routine_Services_Medicare = &getFormulae($rpt_rec_num, $rpt_year, "S300001-00500-0400");
	my $Intensive_Care_Unit_Medicare	= &getFormulae($rpt_rec_num, $rpt_year, "S300001-00600-0400");
	my $Coronary_Intensive_Care_Medicare = &getFormulae($rpt_rec_num, $rpt_year, "S300001-00700-0400");
	my $Total_Acute_Medicare = &getFormulae($rpt_rec_num, $rpt_year, "S300001-01200-0400");

	## Acute Utilization Statistics by Payor for Medicaid.

	my $Routine_Services_Medicaid = &getFormulae($rpt_rec_num, $rpt_year, "S300001-00500-0500");
	my $Intensive_Care_Unit_Medicaid	= &getFormulae($rpt_rec_num, $rpt_year, "S300001-00600-0500");
	my $Coronary_Intensive_Care_Medicaid = &getFormulae($rpt_rec_num, $rpt_year, "S300001-00700-0500");
	my $Total_Acute_Medicaid = &getFormulae($rpt_rec_num, $rpt_year, "S300001-01200-0500");

	## Acute Utilization Statistics by Payor for Others.

	my $Routine_Services_Others = &getFormulae($rpt_rec_num, $rpt_year, "S300001-00500-0300");
	my $Intensive_Care_Unit_Others	= &getFormulae($rpt_rec_num, $rpt_year, "S300001-00600-0300");
	my $Coronary_Intensive_Care_Others = &getFormulae($rpt_rec_num, $rpt_year, "S300001-00700-0300");
	my $Total_Acute_Others = &getFormulae($rpt_rec_num, $rpt_year, "S300001-01200-0300");

	# Acute Utilization Statistics by Payor for Total.

	my $Routine_Services_Total = &getFormulae($rpt_rec_num, $rpt_year, "S300001-00500-0600");
	my $Intensive_Care_Unit_Total	= &getFormulae($rpt_rec_num, $rpt_year, "S300001-00600-0600");
	my $Coronary_Intensive_Care_Total = &getFormulae($rpt_rec_num, $rpt_year, "S300001-00700-0600");
	my $Total_Acute_Total = &getFormulae($rpt_rec_num, $rpt_year, "S300001-01200-0600");

	# For discharges ALOS / ADC

	# Acute Utilization Statistics by Payor for Medicare.

	my $Discharges_Medicare = &getFormulae($rpt_rec_num, $rpt_year, "S300001-00100-1300");
	my $Average_Length_of_Stay_Medicare = &getFormulae($rpt_rec_num, $rpt_year, "S300001-01200-0400 / S300001-00100-1300");
	$Average_Length_of_Stay_Medicare = sprintf "%.2f", $Average_Length_of_Stay_Medicare;

	my $Average_Daily_Census_Medicare = &getFormulae($rpt_rec_num, $rpt_year, "S300001-01200-0400 / 365");
	$Average_Daily_Census_Medicare = sprintf "%.2f", $Average_Daily_Census_Medicare;

	# Acute Utilization Statistics by Payor for Medicaid.

	my $Discharges_Medicaid = &getFormulae($rpt_rec_num, $rpt_year, "S300001-00100-1400");
	my $Average_Length_of_Stay_Medicaid = &getFormulae($rpt_rec_num, $rpt_year, "S300001-01200-0500 / S300001-00100-1400");
	$Average_Length_of_Stay_Medicaid = sprintf "%.2f", $Average_Length_of_Stay_Medicaid;

	my $Average_Daily_Census_Medicaid = &getFormulae($rpt_rec_num, $rpt_year, "S300001-01200-0500 / 365");
	$Average_Daily_Census_Medicaid = sprintf "%.2f", $Average_Daily_Census_Medicaid;

	# Acute Utilization Statistics by Payor for Other.

	my $Discharges_Other = &getFormulae($rpt_rec_num, $rpt_year, "S300001-0100-1200");
	my $Average_Length_of_Stay_Other = &getFormulae($rpt_rec_num, $rpt_year, "S300001-01200-0300 / S300001-00100-1200");
	$Average_Length_of_Stay_Other = sprintf "%.2f", $Average_Length_of_Stay_Other;

	my $Average_Daily_Census_Other = &getFormulae($rpt_rec_num, $rpt_year, "S300001-01200-0300 / 365");
	$Average_Daily_Census_Other = sprintf "%.2f", $Average_Daily_Census_Other;

	# Acute Utilization Statistics by Payor for Total.

	my $Discharges_Total = &getFormulae($rpt_rec_num, $rpt_year, "S300001-00100-1500");
	my $Average_Length_of_Stay_Total = &getFormulae($rpt_rec_num, $rpt_year, "S300001-01200-0600 / S300001-00100-1500");
	$Average_Length_of_Stay_Total = sprintf "%.2f", $Average_Length_of_Stay_Total;

	my $Average_Daily_Census_Total = &getFormulae($rpt_rec_num, $rpt_year, "S300001-01200-0600 / 365");
	$Average_Daily_Census_Total = sprintf "%.2f", $Average_Daily_Census_Total;



	#Starting a new Row
	$htmlData = $htmlData . "<tr>";

	$htmlData = $htmlData . "<td><table cellpadding=\"2\" cellspacing=\"2\" width=\"100%\" border=1><tr><td>";
	$htmlData = $htmlData . "<tr><td style=\"background-color:#CCCCFF;\" colspan=2><b>Bacground Information</b></td></tr>";
	$htmlData = $htmlData . "<tr><td>Hospital Name</td><td nowrap>$Hospital_Name</td></tr>";
	$htmlData = $htmlData . "<tr><td>Provider Number</td><td>$Provider_Number</td></tr>";
	$htmlData = $htmlData . "<tr><td>Street</td><td>$Street</td></tr>";
	$htmlData = $htmlData . "<tr><td>City, State, Zip Code</td><td>$City, $State, $Zip_Code</td></tr>";
	$htmlData = $htmlData . "<tr><td>Telephone number</td><td>$Telephone_number</td></tr>";
	$htmlData = $htmlData . "<tr><td>Type of Facility</td><td>$Type_of_Facility</td></tr>";
	$htmlData = $htmlData . "<tr><td>Type of Control</td><td>$Type_of_Control</td></tr>";
	$htmlData = $htmlData . "<tr><td>Health Care System</td><td>$HealthCareSystem</td></tr>";
	$htmlData = $htmlData . "<tr><td>Total Employees </td><td>$Total_Employees</td></tr>";
	$htmlData = $htmlData . "<tr><td>Total Discharges </td><td>$Total_Discharges</td></tr>";
	$htmlData = $htmlData . "<tr><td>Total Patient Days </td><td>$Total_Patient_Days</td></tr>";
	$htmlData = $htmlData . "<tr><td nowrap>General Med/Surg Beds</td><td>$General_MedSurg_Beds</td></tr>";
	$htmlData = $htmlData . "<tr><td>Special Care Beds</td><td>$Special_Care_Beds</td></tr>";

	$htmlData = $htmlData . "</td></tr></table></td>";

	$htmlData = $htmlData . "<td valign=\"top\"><table cellpadding=\"2\" cellspacing=\"2\" width=\"100%\" border=\"1\"><tr><td>";
	$htmlData = $htmlData . "<tr><td style=\"background-color:#CCCCFF;\" colspan=\"7\"><b>Acute Utilization Statistics by Payor</b></td></tr>";
	$htmlData = $htmlData . "<tr><td></td><td><b>Beds</b></td><td align=\"center\"><b>Revenue</b></td><td colspan=4 align=\"center\"><b>Inpatient Days</b></td></tr>";
	$htmlData = $htmlData . "<tr align=\"center\" bgcolor=\"#99CCFF;\"><td colspan=3></td><td><b>Medicare</b></td><td><b>Medicaid</b></td><td><b>Others</b></td><td><b>Total</b></td></tr>";
	$htmlData = $htmlData . "<tr><td nowrap>Routine Services</td><td align=\"center\">".&numNegFormat($Routine_Services_beds)."</td><td align=\"center\">".&numNegFormat($Routine_Services_Revenue,true)."</td><td align=\"center\">".&numNegFormat($Routine_Services_Medicare)."</td><td align=\"center\">".&numNegFormat($Routine_Services_Medicaid)."</td><td align=\"center\">".&numNegFormat($Routine_Services_Others)."</td><td align=\"center\">".&numNegFormat($Routine_Services_Total)."</td></tr>";
	$htmlData = $htmlData . "<tr><td>Intensive Care Unit</td><td align=\"center\">".&numNegFormat($Intensive_Care_Unit_beds)."</td><td align=\"center\">".&numNegFormat($Intensive_Care_Unit_Revenue,true)."</td><td align=\"center\">".&numNegFormat($Intensive_Care_Unit_Medicare)."</td><td align=\"center\">".&numNegFormat($Intensive_Care_Unit_Medicaid)."</td><td align=\"center\">".&numNegFormat($Intensive_Care_Unit_Others)."</td><td align=\"center\">".&numNegFormat($Intensive_Care_Unit_Total)."</td></tr>";
	$htmlData = $htmlData . "<tr><td nowrap>Coronary Intensive Care</td><td align=\"center\">".&numNegFormat($Coronary_Intensive_Care_beds)."</td><td align=\"center\">".&numNegFormat($Coronary_Intensive_Care_Revenue,true)."</td><td align=\"center\">".&numNegFormat($Coronary_Intensive_Care_Medicare)."</td><td align=\"center\">".&numNegFormat($Coronary_Intensive_Care_Medicaid)."</td><td align=\"center\">".&numNegFormat($Coronary_Intensive_Care_Others)."</td><td align=\"center\">".&numNegFormat($Coronary_Intensive_Care_Total)."</td></tr>";
	$htmlData = $htmlData . "<tr><td>Total Acute</td><td align=\"center\">".&numNegFormat($Total_Acute_beds)."</td><td align=\"center\">".&numNegFormat($Total_Acute_Revenue,true)."</td><td align=\"center\">".&numNegFormat($Total_Acute_Medicare)."</td><td align=\"center\">".&numNegFormat($Total_Acute_Medicaid)."</td><td align=\"center\">".&numNegFormat($Total_Acute_Others)."</td><td align=\"center\">".&numNegFormat($Total_Acute_Total)."</td></tr>";
	$htmlData = $htmlData . "<tr><td></td><td></td><td></td><td colspan=4 align=\"center\"><b>Discharges / ALOS / ADC</b></td></tr>";
	$htmlData = $htmlData . "<tr align=\"center\" bgcolor=\"#99CCFF;\"><td colspan=3></td><td><b>Medicare</b></td><td><b>Medicaid</b></td><td><b>Others</b></td><td><b>Total</b></td></tr>";
	$htmlData = $htmlData . "<tr><td>Discharges</td><td></td><td></td><td align=\"center\">".&numNegFormat($Discharges_Medicare)."</td><td align=\"center\">".&numNegFormat($Discharges_Medicaid)."</td><td align=\"center\">".&numNegFormat($Discharges_Other)."</td><td align=\"center\">".&numNegFormat($Discharges_Total)."</td></tr>";
	$htmlData = $htmlData . "<tr><td nowrap>Average Length of Stay</td><td></td><td></td><td align=\"center\">".&numNegFormat($Average_Length_of_Stay_Medicare)."</td><td align=\"center\">".&numNegFormat($Average_Length_of_Stay_Medicaid)."</td><td align=\"center\">".&numNegFormat($Average_Length_of_Stay_Other)."</td><td align=\"center\">".&numNegFormat($Average_Length_of_Stay_Total)."</td></tr>";
	$htmlData = $htmlData . "<tr><td>Average Daily Census</td><td></td><td></td><td align=\"center\">".&numNegFormat($Average_Daily_Census_Medicare)."</td><td align=\"center\">".&numNegFormat($Average_Daily_Census_Medicaid)."</td><td align=\"center\">".&numNegFormat($Average_Daily_Census_Other)."</td><td align=\"center\">".&numNegFormat($Average_Daily_Census_Total)."</td></tr>";


	$htmlData = $htmlData . "</td></tr></table></td>";

	$htmlData = $htmlData . "</tr>";
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td valign=\"top\"><table cellpadding=\"2\" cellspacing=\"2\" width=\"100%\" border=1><tr><td>";
	$htmlData = $htmlData . "<tr><td colspan=2 style=\"background-color:#CCCCFF;\"><b>Credit Information</b></td></tr>";
	$htmlData = $htmlData . "<tr><td>Bond Rating (if Applicable)</td><td>N/A</td></tr>";
	$htmlData = $htmlData . "<tr><td>Bond Authority</td><td>N/A</td></tr>";
	$htmlData = $htmlData . "<tr><td>Credit Enhancer(if Applicable)</td><td>N/A</td></tr>";
	$htmlData = $htmlData . "<tr><td>Debt Service Schedule</td><td>N/A</td></tr>";

	$htmlData = $htmlData . "</td></tr></table></td>";

	$htmlData = $htmlData . "<td><table cellpadding=\"2\" cellspacing=\"2\" width=\"100%\" border=\"1\"><tr><td>";
	$htmlData = $htmlData . "<tr><td style=\"background-color:#CCCCFF;\" colspan=7><b>Other Utilization Statistics by Payor</b></td></tr>";
	$htmlData = $htmlData . "<tr><td></td><td><b>Beds</b></td><td align=\"center\"><b>Revenue</b></td><td colspan=4 align=\"center\"><b>Inpatient Days</b></td></tr>";
	$htmlData = $htmlData . "<tr align=\"center\" bgcolor=\"#99CCFF;\"><td colspan=3></td><td><b>Medicare</b></td><td><b>Medicaid</b></td><td><b>Others</b></td><td><b>Total</b></td></tr>";
	$htmlData = $htmlData . "<tr><td>Psychiatric Unit</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
	$htmlData = $htmlData . "<tr><td>Rehabilitation Unit</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
	$htmlData = $htmlData . "<tr><td>Total Other</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
	$htmlData = $htmlData . "<tr><td>Total Complex</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
	$htmlData = $htmlData . "<tr><td></td><td></td><td></td><td colspan=4 align=\"center\"><b>Gross Patient Revenue</b></td></tr>";
	$htmlData = $htmlData . "<tr align=\"center\" bgcolor=\"#99CCFF;\"><td colspan=3></td><td><b>Medicare</b></td><td><b>Medicaid</b></td><td><b>Others</b></td><td><b>Total</b></td></tr>";
	$htmlData = $htmlData . "<tr><td>Total Hospital Patient Revenue</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";

	$htmlData = $htmlData . "</td></tr></table></td>";

	$htmlData = $htmlData . "</tr>";
}else{
	$htmlData="<tr><td>&nbsp;</td></tr><tr><td>Please select provider number and run report again.<a href=\"/cgi-bin/Milo2/SelectReport.pl\">Back</a></td></tr><tr><td>&nbsp;</td></tr>";
}
print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>Profile Reports</TITLE>
<SCRIPT SRC="/JS/sorttable.js"></SCRIPT>
<SCRIPT SRC="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<SCRIPT SRC="/JS/dropmenu.js"></SCRIPT>
<link rel="stylesheet" href="/css/dropmenu.css" type="text/css" />
</HEAD>
<body>
<BR>
<FORM NAME = "frmFinacial" Action="/cgi-bin/finantial_report.pl" method="post">
<CENTER>
};
&innerHeader();
print qq{
    <!--content start-->

        <table align="center" cellpadding="0" cellspacing="0" width="90%">
	  <tr>
	        <td width="16"><img src="/images/tableRonuded/top_lef.gif" width="16" height="16"></td>
	        <td height="16" background="/images/tableRonuded/top_mid.gif"><img src="/images/tableRonuded/top_mid.gif" width="16" height="16"></td>
	        <td width="24"><img src="/images/tableRonuded/top_rig.gif" width="24" height="16"></td>
	      </tr>
	      <tr>
	        <td width="16" background="/images/tableRonuded/cen_lef.gif"></td>
	        <td align="center" valign="middle" bgcolor="#FFFFFF">

<table border="1em" bgColor="#FFFFFF" width="100%" cellpadding="0" cellspacing="0">
<tr class='gridHeader'><td nowrap colspan=4><b>Profile Report for the year : $rpt_year</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"/cgi-bin/Milo2/SelectReport.pl\">Back</a></td></tr>
$htmlData
</table>
</td>
		<td width="24" background="/images/tableRonuded/cen_rig.gif"><img src="/images/tableRonuded/cen_rig.gif" width="24" height="11"></td>
	      </tr>
	      <tr>
		    <td width="16" height="16"><img src="/images/tableRonuded/bot_lef.gif" width="16" height="16"></td>
		    <td height="16" background="/images/tableRonuded/bot_mid.gif"><img src="/images/tableRonuded/bot_mid.gif" width="16" height="16"></td>
		    <td width="24" height="16"><img src="/images/tableRonuded/bot_rig.gif" width="24" height="16"></td>
	      </tr>
   </table>
};
&TDdoc;
print qq{
</body>
</HTML>
};
#--------------------------------------------------------------------------------------

sub sessionExpire
{

print "Content-Type: text/html\n\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};
&headerScript();
print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#--------------------------------------------------------------------------------

sub getFormulae
{
	my ($report_id, $yyyy, $formulae) = @_;
	my ($value);
	#$value = &getFormulaeValue($report_id,"(G200000-002500-0200)/(G200000-002500-0300)",$yyyy);

	$value = &getFormulaeValue($report_id,$formulae,$yyyy);

	#EBITDAR as a Percent of Interest Expense
	#Net Revenue per FTE

	return $value;
}


#----------------------------------------------------------------------------

sub getFormulaeValue
{
	my ($report_id,$formulae,$yyyy) = @_;
	my ($value, $i, $elementVal, $operation);

	for ($i=0;$i<length($formulae);$i++)
	{
		if(substr($formulae, $i,1) =~ /[A-Z]/ )
		{
			#Spliting elements from Address
			my ($worksheet, $line, $column) = split /[-:]/,substr($formulae, $i,18);
			if(&get_value($report_id, $worksheet, $line, $column, $yyyy))
			{
				$elementVal = &get_value($report_id, $worksheet, $line, $column, $yyyy);
			}
			else
			{
				$elementVal = 0;
			}


			$value = $value.$elementVal;
			$i = $i+17;
		}
		elsif(substr($formulae, $i,1) =~ /\s/ )
		{

		}
		else
		{

			$value = $value . substr($formulae, $i,1);
		}
	}
	$value = eval($value);
	if(!$value)
	{
		$value = "0";
	}
	else
	{
		#$value = sprintf "%.2f", $value;
		$value = $value;
	}
	return $value;

}

#------------------------------------------------------------------------------

sub get_value
{
   my ($report_id, $worksheet, $line, $column, $fYear) = @_;

   my $yyyy = $fYear;


   my ($sthVal, $value);

   my $sql = qq(
    SELECT CR_VALUE
      FROM CR_ALL_DATA
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?

   );


   unless ($sthVal = $dbh->prepare($sql)) {
       &display_error('Error preparing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sthVal->execute($report_id, $worksheet, $line, $column)) {
       &display_error('Error executing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }



   if ($sthVal->rows) {
      $value = $sthVal->fetchrow_array;
   }

   $sthVal->finish();

   return $value;
}

#------------------------------------------------------------------------------

sub numNegFormat
{
   my ($val,$dollar) = @_;
   if($val<0){
   	$val = $val*-1;
   	$val= Currency_Format($val);
   	if($dollar){
   		$val = "(" . "\$" . $val . ")";
   	}
   	else{
   		$val = "(" . $val . ")";
   	}
   }
   else{
   	if($dollar){
   		$val= "\$" . Currency_Format($val);
   	}
   	else{
   		$val= Currency_Format($val);
   	}
   }
   return $val;
}

#------------------------------------------------------------------------------

sub USA_Format {
(my $n = shift) =~ s/\G(\d{1,3})(?=(?:\d\d\d)+(?:\.|$))/$1,/g;
return "\$$n";
}

#------------------------------------------------------------------------------

sub Currency_Format {
(my $n = shift) =~ s/\G(\d{1,3})(?=(?:\d\d\d)+(?:\.|$))/$1,/g;
return "$n";
}

#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="window.location.href='/cgi-bin/Milo2/index.pl'" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}
