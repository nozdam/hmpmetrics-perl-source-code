#!/usr/bin/perl

use CGI;
use DBI;
use CGI::Session;
do "common_header.pl";
print "Content-Type: text/html\n\n";
&headerScript();
my $cgi = new CGI;

my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");

my $saction = $cgi->param("saction");
my $FormulaName = $cgi->param("txtName");
my $worksheet = $cgi->param("txtWorksheet");
my $lineno = $cgi->param("txtLineNo");
my $columnno = $cgi->param("txtColNo");
my $Formula = $cgi->param("txtformula");
my $updatedID = $cgi->param('id');
my $bcoordinates = $worksheet."-".$lineno."-".$columnno;
my $facility = $cgi->param("facility");


if(!$userID)
{
	&sessionExpire;
}

# Database

my $dbh;
my ($sql, $sth);
my ($sql1, $sth1);
my ($sql2, $sth2,$s,$sql_f,$facility_id);


# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};

my $msg = "";


unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error('ERROR connecting to database', $DBI::errstr);
   exit(1);
}
$sql_f = "select facility_id from tbl_facility_type where facility_name = '$facility'";
$sth1 = $dbh->prepare($sql_f);
$sth1->execute();

while ($row = $sth1->fetchrow_hashref) {
	$facility_id = $$row{facility_id};
}
if(trim($saction) eq 'create'){
	$sql = "insert into standard_metrics (element,isFormulae,Formulae,facility_type) values ('$FormulaName',0,'$bcoordinates',$facility_id)";
	$sth = $dbh->prepare($sql);
	$sth->execute();
	$msg = $msg."<script>alert('Formula created successfully.');parent.formulabuilder.location.href='/cgi-bin/FormulaTabPageB.pl?facility=$facility';</script>";

	# $sql1 = "insert into standard_reports_metrics (Summary_Info) values ('NA')";
	# $sth1 = $dbh1->prepare($sql1);
	# $sth1->execute();
	# $msg = $msg."<script>alert('Formula created successfully.');parent.formulabuilder.location.href='/cgi-bin/FormulaTabPageB.pl?facility_id=$facility_id';</script>";
}

if(trim($saction) eq 'edit'){
	$sql = "update standard_metrics set element = '$FormulaName',Formulae ='$bcoordinates',isFormulae = 0 where metrics_id = '$updatedID' and facility_type = $facility_id";
	open(J, ">data.txt");
	    print J $sql;
	  close J;
	$sth = $dbh->prepare($sql);
	$sth->execute();
	$msg = $msg."<script>alert('Formula updated successfully.');parent.formulabuilder.location.href='/cgi-bin/FormulaTabPageC.pl?facility=$facility';</script>";
}

if(trim($saction) eq 'delete'){
	$sql = "delete from formulas where ID = '$updatedID'";
	$sth = $dbh->prepare($sql);
	$sth->execute();
	$msg = $msg."<script>alert('Formula deleted successfully.');parent.formulabuilder.location.href='/cgi-bin/FormulaTabPageC.pl?facility=$facility';</script>";
}

#$sth->finish();
$dbh->disconnect();


print qq{
<HTML>
<HEAD><TITLE>Standard Reports</TITLE>
 <script type="text/javascript" src="/JS/AjaxScripts/prototype.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/effects.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/controls.js"></script>
 <link href="/css/admin_panel/Exceptions/ExceptionActions.css" rel="stylesheet" type="text/css" />	
</HEAD>
<BODY>
<FORM NAME = "frmFormulaActions" Action="/cgi-bin/ReportActions.pl?facility=$facility" method="post">
<TABLE align="top" border=1 class="gridstyle" CELLPADDING="0" CELLSPACING="0" width="100%">
	<tr>
		<td>$msg
};

		print "$msg\n";
print qq{
</td>

	</tr>
</TABLE>
</FORM>
</BODY>
</HTML>
};

#-----------------------------------------------------------------------------


sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}



print qq{
<HTML>
<HEAD><TITLE>Standard Reports-ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
<BR>
<BR>
<FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}
#----------------------------------------------------------------------------

#Perl trim function to remove whitespace from the start and end of the string

sub trim($)
{
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}
