#!/usr/bin/perl -w

#----------------------------------------------------------------------------
# Program: log_check.pl
#  Author: Steve Spicer
# Revised: Thu Jul 17 11:32:55 CDT 2008
#----------------------------------------------------------------------------
use strict;

my $depth = 100;

my %ini_value;

my ($key, $value);

&read_inifile("milo.ini");

my $title_line = $ini_value{'TITLE_LINE'};

my $now = localtime();


my ($fname, $line);

$fname = $ini_value{'APACHE_ERROR_LOG'};

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>$title_line - ENVIRONMENT</TITLE>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon">
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
<link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />

</HEAD>
<BODY>
<CENTER>

<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/index.html" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/cr_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="/cgi-bin/cr_batchx.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="/cgi-bin/snf_batch1.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="/cgi-bin/graph.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Graph Utility</div>">
                                <img src="/images/graph_icon_inner.gif" onmouseover="src='/images/graph_icon_inner_hover.gif';"
                                    onmouseout="src='/images/graph_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
               <td style="width: 102px" align="Center">
	                                   <a href="/cgi-bin/PeerMain.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
	                                       <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->
    




<!--content start--><br>
    <table cellpadding="0" cellspacing="0" align="center" width="885px">
        <tr>
            <td valign="top">
                <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px">


<div class="pageheader">

$title_line<BR>

</div>
<div>
AS OF: $now</div>

<br><br>
<div class="tahoma14"><B>ERROR LOG</B> $fname</div>

<BR>
<TEXTAREA style=\"width:100%\" ROWS=\"20\" STYLE="font-size:10pt">
};

open INPUT, "tail -$depth $fname |" || print "ERROR: $fname $!";

while ($line = <INPUT>) {
   unless ($line =~ m/mowen|house/i) {
     print $line;
   }
}

close INPUT;

print "</TEXTAREA>";
print "<BR>\n";
print "<BR>\n";

$fname = $ini_value{'APACHE_ACCESS_LOG'};
print"<div class=\"tahoma14\"><B>ACCESS LOG</B> $fname</div>\n";

print "<BR>\n";
print "<TEXTAREA style=\"width:100%\" ROWS=\"20\" STYLE=\"font-size:10pt\">";


open INPUT, "tail -$depth $fname |" || print "ERROR: $fname $!";

while ($line = <INPUT>) {
   unless ($line =~ m/mowen|house/i) {
     print $line;
   }
}

close INPUT;

print "</TEXTAREA>\n";
print "<BR>\n";
print"<div class=\"tahoma14\"><B>DISK USAGE</B></div>\n";

print "<BR>\n";
print "<TEXTAREA style=\"width:100%\" ROWS=\"10\">\n";

open INPUT, "df -h |" || print "ERROR $!";

while ($line = <INPUT>) {
   print $line;
}
  
print qq{
</TEXTAREA>
<FORM><span class="button">
<INPUT TYPE="button" VALUE=" BACK " onClick="history.go(-1);return true;"></span>
</FORM>

 			</td>
	            <td valign="top">
	                <img src="/images/content_right.gif" /></td>
	        </tr>
	    </table>
	    <!--content end-->


</CENTER>
</BODY>
</HTML>
};


#----------------------------------------------------------------------------
#  loads ini file
#----------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

