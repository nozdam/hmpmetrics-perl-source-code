#!/usr/bin/perl
use strict;
use CGI;
use DBI;
use CGI::Session;
use URI::Encode;
do "common_header.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $userID = $session->param("userID");
my $facility_id = $cgi->param("facility_id");
if(!$userID)
{
	&sessionExpire;
}

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};

my ($dbh, $sql, $sth);

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error('ERROR connecting to database', $DBI::errstr);
   exit(1);
}

my $element = $cgi->param("txtElement");
my $metric = $cgi->param("txtMetric");
my $worksheet = $cgi->param("txtWorksheet");
my $lineno = $cgi->param("txtLineNo");
my $columnno = $cgi->param("txtColumnNo");
my $isformula=$cgi->param("txtIsfor");
my $metr_name = $cgi->param("txtMetrx_name");
my $matricsFormula = $cgi->param("formula");
my $btnType = $cgi->param("btnType");
my $txtExpression = $cgi->param("txtExpression");
my $rpt_details_isFormulae = $cgi->param("isFormulae");
my $sel_for = $cgi->param("paramA");
my $element_name = $cgi->param("paramB");

my ($sql1,$sth1,$row1);
my ($sql,$sth,$sql2,$sth2);

$sql1 = "select metrics_id,element,isFormulae,formulae from standard_metrics where Formulae !='' and facility_type = $facility_id order by element ";
$sth1 = $dbh->prepare($sql1);
$sth1->execute();

my ($metricsid,$elements, $formulae,$isFor);

my $formula_optionsA ="<OPTION VALUE='' SELECTED>-- Select --</OPTION>";

while (($metricsid,$elements,$isFor,$formulae) = $sth1->fetchrow_array) {
	if($isFor == 0){
      $formula_optionsA = $formula_optionsA."<OPTION VALUE='$formulae~$metricsid'>$elements (MI)</OPTION>";
	 	}else{
	  $formula_optionsA = $formula_optionsA."<OPTION VALUE='$formulae~$metricsid'>$elements (F)</OPTION>";
}
}
$sth1->finish();

print "Content-Type: text/html\n\n";
&headerScript();
print qq{
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Formulas - Select Mode</title>
    <link href="/css/tabs.css" rel="stylesheet" type="text/css" />

	
   <link href="/css/admin_panel/user_management/userManagement.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="/JS/admin_panel/standard_metrics/metrics_create.js" ></script>

</head>
<body>
<form name="frmfromulaTab">
<input type="hidden" name="element" value="$element">
<input type="hidden" name="sel_for" value="$sel_for">
<input type="hidden" name="txtIsfor" value="">
<input type="hidden" name="txtMetric" value="">
<input type="hidden" name="txtElement" value="">
<input type="hidden" name="saction" value="">
<input type="hidden" name="btnType" value="">
<input type="hidden" name="hdnColumnNo" value="">
<input type="hidden" name="hdnLineNo" value="">
<input type="hidden" name="hdnWorksheet" value="">
<input type="hidden" name="optionsA" value ="$formula_optionsA">
<input type="hidden" name="facility_id" value="$facility_id">
<div id="content">
 <table  width="20%" height="20%" cellpadding="0" cellspacing="0">

			<tr>
				<td>
					Metric Name : <input type="text" id="txtMetrx_name" name="txtMetrx_name" style = "width:450px;height:20px;" value="" >
				</td>
			</tr>
			<tr><td >&nbsp;</td></tr>
			<tr>
			<td style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">  Element : <input id="poBoxRadioNo" name="poBoxRadio" type="radio" class="radio-btn" value="No" onClick="setWindow(this,0);" checked /> Single Cost Report Address <style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\"> <input id="poBoxRadioYes" name="poBoxRadio" type="radio" class="radio-btn" value="Yes"  onClick="setWindow(this,1);" /> Formula <input id="poBoxRadioBulk" name="poBoxRadio" type="radio" class="radio-btn" value="Bulk"  onClick="setWindow(this,2);" /> Bulk</td>
			</tr>

			<tr><td >&nbsp;</td></tr>
			<tr>
			<td style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\"><input type="checkbox" name="create_provider_metric" value="1"/> Create Provider Metric?</td>
			</tr>

			<tr><td >&nbsp;</td></tr>
			<tr>
    			<td >
    				<div id="selfrm"></div>
    			</td>
    		</tr>
    		<tr>
		    	<td style="width:20px;height:14px;">
		    		<textarea name="txtExpression" style="height:162px;width:450px;display:none;"></textarea>

					<div name = "micontent" id = "micontent" style="height:162px;width:450px;">
<table align="center" class="gridstyle" width="100%" height="80%" cellpadding="0" cellspacing="0">
    		<tr>
			<td> <tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Worksheet : </td><td style=\"background-color:#edf2fb\"><input type=\"text\" name=\"txtWorksheet\" id=\"txtWorksheet\"  maxLength=\"7\"></td></tr>\n
			<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Line No : </td><td style=\"background-color:#edf2fb\"><input type=\"text\" name=\"txtLineNo\"  maxLength=\"5\"></td></tr>\n
			<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Column No : </td><td style=\"background-color:#edf2fb\"><input type=\"text\" name=\"txtColumnNo\" maxLength=\"4\"></td></tr>\n</td>
			</tr>
			</table>
			<table name = "mibuttons" id = "mibuttons" align="left" width="20%" cellpadding="0" cellspacing="0">
			<tr>
			<td align="center"><span  class=\"button\"><input style="width:100px;" type="button" name="btnUpdate" id="save" value ="Save" onclick = "callSubmit()" ></span></td>

			</tr>
			</table>
</div>
		    	</td>
    		</tr>
    		<tr>
    			<td align="center">
		    	<table name = "formulabuttons" id = "formulabuttons" align="left" width="20%" cellpadding="0" cellspacing="0" style="margin-left:5px;display:none;">
		    	<tr>
		    	<td align="center"><span  class=\"button\"><input type="button" style="width:25px;" name="btnOpenBrace" value =" ( " onclick="AddOperator(this)"></span></td>
		    	<td align="center"><span  class=\"button\"><input type="button" style="width:25px;" name="btnCloseBrace" value =" ) " onclick="AddOperator(this)"></span></td>
		    	<td align="center"><span  class=\"button\"><input style="width:25px;" type="button" name="btnModule" value =" % " onclick="AddOperator(this)"></span></td>
		    	<td align="center"><span  class=\"button\"><input style="width:25px;" type="button" name="btnAdd" value =" + " onclick="AddOperator(this)"></span></td>
		    	<td align="center"><span  class=\"button\"><input style="width:25px;" type="button" name="btnMulti" value =" * " onclick="AddOperator(this)"></span></td>
		    	<td align="center"><span  class=\"button\"><input style="width:25px;" type="button" name="btnDivid" value =" / " onclick="AddOperator(this)"></span></td>
		    	<td align="center"><span  class=\"button\"><input style="width:25px;" type="button" name="btnMinus" value =" - " onclick="AddOperator(this)"></span></td>
		    	<td align="center"><span  class=\"button\"><input style="width:50px;" type="button" name="btnClear" value =" Clear " onclick="ClearIt()"></span></td>
		      	<td align="center"><span  class=\"button\"><input type="button" style="width:50px;" name="btnLoad" id="Load" value ="Save" onclick="callSubmit1()"></span></td>
			  	</tr>
		    	</table>
		    	</td>
    		</tr>
    </table>
</div>
</table>
</form>
</body>
</html>
};

#------------------------------------------------------------------------------------
sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach my $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}

#------------------------------------------------------------------------------
