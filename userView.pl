#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: userView.pl
#  author: Jagadish.M
#----------------------------------------------------------------------------

use strict;
use CGI;
use DBI;

# use Session;
my $cgi = new CGI;
do "common_header.pl";
print "Content-Type: text/html\n\n";
do "pagefooter.pl";
use CGI::Session;
do "audit.pl";
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");

if(!$userID)
{
	&sessionExpire;
}

my $dbh;
my ($sql, $sth, $key);

# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my $defaultYear= $ini_value{'DEFAULT_YEAR'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

$sql = "select role.RoleName,users.UserID,users.UserName,users.IsActive,userdetails.First_Name,userdetails.Middle_Name,userdetails.Last_Name,users.CreatedDate,users.ModifiedDate from users,userdetails,role where users.UserID = userdetails.UsersID and users.RoleID = role.RoleID order by userdetails.First_Name";
$sth = $dbh->prepare($sql);
$sth->execute();



print qq{
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<title>PROJECT MILO User Management</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="/css/register/main.css" rel="stylesheet" type="text/css">
<link href="/css/register/qm.css" rel="stylesheet" type="text/css">
<link href="/css/tabs.css" rel="stylesheet" type="text/css" />   
<link href="/css/admin_panel/user_management/userManagement.css" rel="stylesheet" type="text/css" />
</head>
<body leftmargin="0" topmargin="0" marginheight="20" marginwidth="0" align="centre">
<CENTER>
};
&innerHeader();

print qq{
    <!--content start-->
   
    <table cellpadding="0" cellspacing="0" width="885px">
        <tr>            
            <td valign="top" >
  
	  	
		<ol id="toc">
		    <li class="current"><a href="/cgi-bin/userView.pl"><span>View</span></a></li>
		    <li><a href="/cgi-bin/userCreate.pl"><span>Create User</span></a></li>
		    <li ><a href="/cgi-bin/userModify.pl"><span>Modify User</span></a></li>	
		    <li><a href="/cgi-bin/userAudit.pl"><span>Audit Log</span></a></li>
		</ol>
		<div align "center" class="content" style="border:0px;padding:0em;">
		<table align="center" class="ColrBk01" width="100%" border="0" cellpadding="4" cellspacing="0">
		  <tbody><tr>
			<td align ="right" class="Fnt11Pix" background="/images/register/grad_01_hover.gif"><b class="Colr03">User details</b></td>
			<td class="Fnt11Pix" background="/images/register/grad_01_hover.gif" align="right"><b class="Colr03"></b></td>
		  </tr>
		</tbody></table>
		<table class="ColrBk02" width="100%" border="0" cellpadding="2" cellspacing="0">
		  <tbody><tr>
			<td><form action="/cgi-bin/userActions.pl" method="post" name="frmSingerSLRegistry" id="frmSingerSLRegistry" onsubmit="return funcValidateForm();">
				<table width="100%"  bgcolor="#ffffff" border="0" cellpadding="4" cellspacing="0">
				  <tbody>
			<tr class="ColrBk03"><td><b><font color="#0066cc">User Name</font></td><td><b><font color="#0066cc">Login Name</font></td><td><b><font color="#0066cc">Role</font></b></td><td><b><font color="#0066cc">Created On</font></td><td><b><font color="#0066cc">Modified On</font></td><td><b><font color="#0066cc">Status</font></td><td><b><font color="#0066cc">Action</font></td></tr>          
};
			while (my $row1 = $sth->fetchrow_hashref) {
				if($$row1{IsActive} == 1){  
					print "<tr><td>$$row1{First_Name} $$row1{Middle_Name} $$row1{Last_Name}</td><td>$$row1{UserName}</td><td>$$row1{RoleName}</td><td>$$row1{CreatedDate}</td><td>$$row1{ModifiedDate}</td><td>Active</td><td><a href=\"/cgi-bin/userActions.pl?saction=delete&status=0&uid=$$row1{UserID}\">In Activate</a></td></tr>\n";
				}else{
					print "<tr><td>$$row1{First_Name} $$row1{Middle_Name} $$row1{Last_Name}</td><td>$$row1{UserName}</td><td>$$row1{RoleName}</td><td>$$row1{CreatedDate}</td><td>$$row1{ModifiedDate}</td><td>In Active</td><td><a href=\"/cgi-bin/userActions.pl?saction=delete&status=1&uid=$$row1{UserID}\">Activate</a></td></tr>\n";
				}	
			}
print qq {
			<tr>
								<td colspan="4"><table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tbody>
									<tr>
									  <td width="25"><img src="/images/register/pix_trans.gif" width="120" height="5"></td>
									  <td width="25"></td>
									  <td></td>
									</tr>
								  </tbody></table></td>
							  </tr>							  
							</tbody></table>
						  </form>
				             </td>
					  </tr>
					</tbody>
				      </table>
				      </div>
			          </td>
			       </tr>
			  </tbody>
			 </table>
			 
			 </td>
			  </tr>
			  </table>
			  <!--content end-->
			  };
			  
			  &TDdoc;
			  print qq{
			  </CENTER>
			  </body>  
			  </html>
};
	
#--------------------------------------------------------------------------------------
sub sessionExpire
{

print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};
&headerScript();
print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach my $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}

#------------------------------------------------------------------------------