#!/usr/bin/perl

use strict;
use CGI;
use DBI;
my ($firstName, $lastName, $userID, $role);
my $cgi=new CGI;
my $sid = $cgi->cookie('CGISESSID') || $cgi->param('CGISESSID') || undef;
if ($sid) {
	my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
	$firstName = $session->param("firstName");
	$lastName = $session->param("lastName");
	$userID = $session->param("userID");
	$role = $session->param("role");
}

do "/var/www/cgi-bin/module_facility.pl";

sub headerScript{
	print qq{
	<LINK REL="shortcut icon" HREF="/icons/favicon.ico" TYPE="image/x-icon">
	<LINK REL="stylesheet" TYPE="text/css" HREF="/css/common_css/rounded_button.css">
	<LINK REL="stylesheet" TYPE="text/css" HREF="/css/common_css/milo.css">
	<script language="javascript" src="/JS/common_header/common_header.js" ></script>
	<LINK REL="stylesheet" TYPE="text/css" HREF="/css/common_header/common_header.css">
	};
}

sub mainHeader{
&headerScript();
print qq{
<style type="text/css">
.submenu
{

}
.submenu li
{

}
</style>
<ul id="sddm">
    <table cellpadding="0" cellspacing="0" width="750px" align="center">
        <tr>
            <td>
                <img src="/images/home_menu_left.gif" /></td>
            <td style="background-image: url(/images/home_menu_middle.gif); width: 100%; background-repeat: repeat-x;
                text-align: center">
                <table cellpadding="0" cellspacing="0">

			<td style="width: 180px" align="Center">

				<img src="/images/search_icon-new.gif" onmouseover="src='/images/search_icon_hover-new.gif';mopen('search')"
				    onmouseout="src='/images/search_icon-new.gif';"  style="border: 0px; cursor: pointer" /></td>

			<td style="width: 180px" align="Center">

				<img src="/images/cost_report_icon.gif" onmouseover="src='/images/cost_report_icon_hover.gif';mopen('costreport')"
				    onmouseout="src='/images/cost_report_icon.gif';" style="border: 0px; cursor: pointer" /></td>
			<td style="width: 180px" align="Center">

				<img src="/images/stardardreport_icon.gif" onmouseover="src='/images/stardardreport_icon_hover.gif';mopen('stdreport')"
				    onmouseout="src='/images/stardardreport_icon.gif';" style="border: 0px; cursor: pointer" /></td>
			<td style="width: 180px" align="Center">

				<img src="/images/peer_group_icon.gif" onmouseover="src='/images/peer_group_icon_hover.gif';mopen('peergroup')"
				    onmouseout="src='/images/peer_group_icon.gif';" style="border: 0px; cursor: pointer" /></td>

		    </tr>
					 <tr>
		    <td style="width: 116px" align="Center">
		    	<li> <div id="search"
				onmouseover="mcancelclosetime()"
				onmouseout="mclosetime()">
			  	    <a href="/cgi-bin/cr_search.pl?facility=hospital">Hospital</a>
			    <a href="/cgi-bin/cr_search.pl?facility=snf">Skilled Nursing Facility</a>
			    <a href="/cgi-bin/cr_search.pl?facility=renal">Renal Facility</a>
			    <a href="/cgi-bin/cr_search.pl?facility=hospice">Hospice</a>
			    <a href="/cgi-bin/cr_search.pl?facility=hha">Home Health Agency</a>
        		</div></li>
		    </td>

		    <td style="width: 155px" align="Center" class="submenu">
		    	<li> <div id="costreport"
				onmouseover="mcancelclosetime()"
				onmouseout="mclosetime()">
			    <a href="/cgi-bin/cost_reports.pl?facility=hospital">Hospital</a>
			    <a href="/cgi-bin/cost_reports.pl?facility=snf">Skilled Nursing Facility</a>
			    <a href="/cgi-bin/cost_reports.pl?facility=renal">Renal Facility</a>
			    <a href="/cgi-bin/cost_reports.pl?facility=hospice">Hospice</a>
			    <a href="/cgi-bin/cost_reports.pl?facility=hha">Home Health Agency</a>
			</div></li>
		    </td>
		    <td style="width: 132px" align="Center">
			<li> <div id="stdreport"
				onmouseover="mcancelclosetime()"
				onmouseout="mclosetime()">
			    <a href="/cgi-bin/standard_reports.pl?facility=hospital">Hospital</a>
			    <a href="/cgi-bin/standard_reports.pl?facility=snf">Skilled Nursing Facility</a>
			    <a href="/cgi-bin/standard_reports.pl?facility=renal">Renal Facility</a>
			    <a href="/cgi-bin/standard_reports.pl?facility=hospice">Hospice</a>
			    <a href="/cgi-bin/standard_reports.pl?facility=hha">Home Health Agency</a>
			</div></li>
		    </td>

		    <td style="width: 168px" align="Center">
			<li> <div id="peergroup"
				onmouseover="mcancelclosetime()"
				onmouseout="mclosetime()">
			    <a href="/cgi-bin/PeerMain.pl?facility=hospital">Hospital</a>
			    <a href="/cgi-bin/PeerMain.pl?facility=snf">Skilled Nursing Facility</a>
			    <a href="/cgi-bin/PeerMain.pl?facility=renal">Renal Facility</a>
			    <a href="/cgi-bin/PeerMain.pl?facility=hospice">Hospice</a>
			    <a href="/cgi-bin/PeerMain.pl?facility=hha">Home Health Agency</a>
			</div></li>
		    </td>
		    </tr>
                </table>
            </td>
            <td>
                <img src="/images/home_menu_right.gif" /></td>
        </tr>
    </table>
	</ul>
	};
	}

sub innerHeader{

&headerScript();
print qq{
  <ul id="sddm">
<table cellpadding="0" cellspacing="0" width="885" align="center">

        <tr><td align="left">Welcome <b>
	        	};

	        	print $firstName . " " . $lastName;
    	print qq{

    	</b>
    	<a href="/cgi-bin/login.pl?saction=signOut">(Sign Out)</a>
    	</td>
		<td colspan=3 align="right" style="padding-right:5px;" class="gridHlink">
    		<a href="/cgi-bin/changePassword.pl">Change Password</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/cgi-bin/Profile.pl">Profile</a>&nbsp;&nbsp;&nbsp;
			};
			if($role eq '2')
    		{
    			print qq{
				<font size="2" color="#2e7738"> <a href="/cgi-bin/AdminPanel.pl">Admin Panel</a>
    			};
    		}

			print qq{
			&nbsp;&nbsp;&nbsp;<a href="http://www.hcmpllc.com" target="_new">About Us</a></td>
    	</tr>

		</table>

		<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                   <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/cgi-bin/index.pl" class=\"addToolTip\" title=\"Home Page\">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 120px" align="Center" class=\"addToolTip\" title=\"Search\">

                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';mopen('search')"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>

                        <td style="width: 120px" align="Center">
                        	<a href="#" class=\"addToolTip\" title=\"Cost Report\">
				   <img src="/images/cost_report_icon_inner.gif" onmouseover="src='/images/cost_report_icon_inner_hover.gif';mopen('costreport')"
                                    onmouseout="src='/images/cost_report_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a>
                        </td>
                        <td style="width: 120px" align="Center">
                            <a href="#" class=\"addToolTip\" title=\"Standard Reports\">
			        <img src="/images/stardardreport_inner_icon.gif" onmouseover="src='/images/stardardreport_inner_icon_hover.gif';mopen('stdreport')"
                                    onmouseout="src='/images/stardardreport_inner_icon.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 120px" align="Center">
                            <a href="#" class=\"addToolTip\" title=\"Peer Group\">
			             <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';mopen('peergroup')"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
					 <tr>
			    <td style="width: 78px" align="Center">
			    </td>
			    <td style="width: 81px" align="Center">
				<li> <div id="search"
					onmouseover="mcancelclosetime()"
					onmouseout="mclosetime()">
				    <a href="/cgi-bin/cr_search.pl?facility=hospital">Hospital</a>
			    <a href="/cgi-bin/cr_search.pl?facility=snf">Skilled Nursing Facility</a>
			    <a href="/cgi-bin/cr_search.pl?facility=renal">Renal Facility</a>
			    <a href="/cgi-bin/cr_search.pl?facility=hospice">Hospice</a>
			    <a href="/cgi-bin/cr_search.pl?facility=hha">Home Health Agency</a>
				</div></li>
			    </td>

			    <td style="width: 96px" align="Center">
			    	<li> <div id="costreport"
					onmouseover="mcancelclosetime()"
					onmouseout="mclosetime()">
				     <a href="/cgi-bin/cost_reports.pl?facility=hospital">Hospital</a>
			    <a href="/cgi-bin/cost_reports.pl?facility=snf">Skilled Nursing Facility</a>
			    <a href="/cgi-bin/cost_reports.pl?facility=renal">Renal Facility</a>
			    <a href="/cgi-bin/cost_reports.pl?facility=hospice">Hospice</a>
			    <a href="/cgi-bin/cost_reports.pl?facility=hha">Home Health Agency</a>
				</div></li>
                        	</td>
			    <td style="width: 79px" align="Center">
				<li> <div id="stdreport"
					onmouseover="mcancelclosetime()"
					onmouseout="mclosetime()">
				    <a href="/cgi-bin/standard_reports.pl?facility=hospital">Hospital</a>
					<a href="/cgi-bin/standard_reports.pl?facility=snf">Skilled Nursing Facility</a>
					<a href="/cgi-bin/standard_reports.pl?facility=renal">Renal Facility</a>
					<a href="/cgi-bin/standard_reports.pl?facility=hospice">Hospice</a>
					<a href="/cgi-bin/standard_reports.pl?facility=hha">Home Health Agency</a>
				</div></li>
			    </td>

			    <td style="width: 102px" align="Center">
				<li> <div id="peergroup"
					onmouseover="mcancelclosetime()"
					onmouseout="mclosetime()">
				    <a href="/cgi-bin/PeerMain.pl?facility=hospital">Hospital</a>
					<a href="/cgi-bin/PeerMain.pl?facility=snf">Skilled Nursing Facility</a>
					<a href="/cgi-bin/PeerMain.pl?facility=renal">Renal Facility</a>
					<a href="/cgi-bin/PeerMain.pl?facility=hospice">Hospice</a>
					<a href="/cgi-bin/PeerMain.pl?facility=hha">Home Health Agency</a>
				</div></li>
			    </td>
		    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>

    </table><script language="javascript" src="/JS/tooltip.js" ></script>
	    </ul>
		<br>
};
&header();
}

