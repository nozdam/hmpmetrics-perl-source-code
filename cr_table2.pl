#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: cr_table.pl
#   author: Steve Spicer
#  Written: Wed Oct 24 20:14:32 CDT 2007
#
#  Research Data Assistance Center
#  http://www.resdac.umn.edu/Tools/TBs/TN-005-revised.asp
#----------------------------------------------------------------------------

use strict;
use CGI;
use DBI;
do "pagefooter.pl";
use CGI::Session;
do "audit.pl";
my %status_lookup = (
 1 => 'As Submitted',
 2 => 'Settled w/o Audit',
 3 => 'Settled with Audit',
 4 => 'Reopened',
 5 => 'Amended',
);
&view("View","Cost report alternative view","Cost Report");
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $prov = $session->param("str");
my $rpt_no = $session->param("sstr");
my $provider  = $cgi->param('provider');
# if(!$userID)
# {
	# &sessionExpire;
# }

my $facility   = $cgi->param('facility');
my $reportid  = $cgi->param('reportid');
my $worksheet = $cgi->param('worksheet');

my $start_time = time;

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error('CAN\'T READ milo.ini');
   exit(1);
}


my %a_labels;
my %row_labels;
my %column_labels;

&load_table_definitions($worksheet);

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my ($dbh, $sql, $sth);

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}
if($facility eq 'HOSPITAL'){
$sql = qq{
   SELECT RPT_YEAR, PRVDR_NUM, RPT_STUS_CD,
          DATE_FORMAT(FY_BGN_DT, '%m-%d-%Y'),
          DATE_FORMAT(FY_END_DT, '%m-%d-%Y')
     FROM CR_ALL_RPT
    WHERE RPT_REC_NUM = ?
};

}
if($facility eq 'RENAL'){
$sql = qq{
   SELECT RPT_YEAR, PRVDR_NUM, RPT_STUS_CD,
          DATE_FORMAT(FY_BGN_DT, '%m-%d-%Y'),
          DATE_FORMAT(FY_END_DT, '%m-%d-%Y')
     FROM rnl_ALL_RPT
    WHERE RPT_REC_NUM = ?
};
}
if($facility eq 'HHA'){
$sql = qq{
   SELECT RPT_YEAR, PRVDR_NUM, RPT_STUS_CD,
          DATE_FORMAT(FY_BGN_DT, '%m-%d-%Y'),
          DATE_FORMAT(FY_END_DT, '%m-%d-%Y')
     FROM hha_ALL_RPT
    WHERE RPT_REC_NUM = ?
};
}

if($facility eq 'SNF'){
$sql = qq{
   SELECT RPT_YEAR, PRVDR_NUM, RPT_STUS_CD,
          DATE_FORMAT(FY_BGN_DT, '%m-%d-%Y'),
          DATE_FORMAT(FY_END_DT, '%m-%d-%Y')
     FROM snf_ALL_RPT
    WHERE RPT_REC_NUM = ?
};
}
if($facility eq 'HOSPICE'){
$sql = qq{
   SELECT RPT_YEAR, PRVDR_NUM, RPT_STUS_CD,
          DATE_FORMAT(FY_BGN_DT, '%m-%d-%Y'),
          DATE_FORMAT(FY_END_DT, '%m-%d-%Y')
     FROM hospc_ALL_RPT
    WHERE RPT_REC_NUM = ?
};
}
unless ($sth = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL:", $sth->errstr);
   $dbh->disconnect();
   exit(0);
}

unless ($sth->execute($reportid)) {
   &display_error("ERROR executing SQL:", $sth->errstr);
   $dbh->disconnect();
   exit(0);
}

my ($rpt_year, $rpt_provider, $rpt_stus_cd, $fy_bgn_dt, $fy_end_dt) = $sth->fetchrow_array;

my $rpt_status = $status_lookup{$rpt_stus_cd};

$sth->finish();

my ($hospital_name, $hospital_address, $hospital_city, $hospital_state, $hospital_zip);
if($facility eq 'HOSPITAL'){
$hospital_name    = &get_value_by_addr($rpt_year, $reportid, 'S200000:00200:0100');
$hospital_address = &get_value_by_addr($rpt_year, $reportid, 'S200000:00100:0100');
$hospital_city    = &get_value_by_addr($rpt_year, $reportid, 'S200000:00101:0100');
$hospital_state   = &get_value_by_addr($rpt_year, $reportid, 'S200000:00101:0200');
$hospital_zip     = &get_value_by_addr($rpt_year, $reportid, 'S200000:00101:0300');
}
if($facility eq 'HOSPICE'){
$sql = qq{
    SELECT hp.Name as HOSP_NAME,hp.Street as STREET_ADDR,
hp.City as CITY,hp.State as STATE,hp.Zip as ZIP_CODE
,hp.County as COUNTY 
 FROM tblhospiceproviders  as hp
 WHERE ProviderNo = ?
};
unless ($sth = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute($provider)) {
   &display_error("ERROR executing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}
($hospital_name, $hospital_address, $hospital_city, $hospital_state, $hospital_zip) = $sth->fetchrow_array;
}
if($facility eq 'SNF'){
$sql = qq{
    SELECT sn.Name as SNF_NAME,sn.Street as STREET_ADDR,
sn.City as CITY,sn.State as STATE,sn.Zip as ZIP_CODE
,sn.County as COUNTY 
 FROM tblsnfs  as sn
 WHERE ProviderNo = ?
};
unless ($sth = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute($provider)) {
   &display_error("ERROR executing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}
($hospital_name, $hospital_address, $hospital_city, $hospital_state, $hospital_zip) = $sth->fetchrow_array;
}
if($facility eq 'HHA'){
$sql = qq{
    SELECT ha.Name as HHA_NAME,ha.Street as STREET_ADDR,
ha.City as CITY,ha.State as STATE,ha.Zip as ZIP_CODE
 FROM tblhomehealthagencies  as ha
 WHERE ProviderNo = ?
};
unless ($sth = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute($provider)) {
   &display_error("ERROR executing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}
($hospital_name, $hospital_address, $hospital_city, $hospital_state, $hospital_zip) = $sth->fetchrow_array;
}
if($facility eq 'RENAL'){
$sql = qq{
 SELECT rn.Name as RNL_NAME,rn.Street as STREET_ADDR,
rn.City as CITY,rn.State as STATE,rn.Zip as ZIP_CODE
,rn.County as COUNTY 
 FROM tblrenalproviders  as rn
 WHERE ProviderNo = ?
};
unless ($sth = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute($provider)) {
   &display_error("ERROR executing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}
($hospital_name, $hospital_address, $hospital_city, $hospital_state, $hospital_zip) = $sth->fetchrow_array;
}
&explore_worksheet($reportid, $rpt_year, $worksheet);

$dbh->disconnect();

exit(0);

#----------------------------------------------------------------------------

sub explore_worksheet
{

   my ($reportid, $year, $worksheet) = @_;

   my ($address, $row, $col, $value, $class, $align, $editlink);


   if ($ini_value{'USE_A_TABLE'} eq 'Y') {
      &load_a_table($reportid, $year);
   }

   my ($sth, $sql);
#AND RPT_YEAR		= $year
if($facility eq 'HOSPITAL'){
$sql = qq/
       SELECT CR_LINE, CR_COLUMN, CR_VALUE
         FROM CR_ALL_DATA
        WHERE CR_REC_NUM    = ?
          AND CR_WORKSHEET  = ?
		
     ORDER BY CR_LINE, CR_COLUMN
      /;


}
if($facility eq 'RENAL'){
   $sql = qq/
       SELECT CR_LINE, CR_COLUMN, CR_VALUE
         FROM rnl_ALL_DATA
        WHERE CR_REC_NUM    = ?
          AND CR_WORKSHEET  = ?
		
     ORDER BY CR_LINE, CR_COLUMN
      /;
}
if($facility eq 'HHA'){
   $sql = qq/
       SELECT CR_LINE, CR_COLUMN, CR_VALUE
         FROM hha_ALL_DATA
        WHERE CR_REC_NUM    = ?
          AND CR_WORKSHEET  = ?
		
     ORDER BY CR_LINE, CR_COLUMN
      /;
}
if($facility eq 'SNF'){
   $sql = qq/
       SELECT CR_LINE, CR_COLUMN, CR_VALUE
         FROM snf_ALL_DATA
        WHERE CR_REC_NUM    = ?
          AND CR_WORKSHEET  = ?
		
     ORDER BY CR_LINE, CR_COLUMN
      /;
}
if($facility eq 'HOSPICE'){
   $sql = qq/
       SELECT CR_LINE, CR_COLUMN, CR_VALUE
         FROM hospc_ALL_DATA
        WHERE CR_REC_NUM    = ?
          AND CR_WORKSHEET  = ?
		
     ORDER BY CR_LINE, CR_COLUMN
      /;
}
   unless ($sth = $dbh->prepare($sql)) {
      &display_error("ERROR preparing SQL:", $sth->errstr);
      exit(0);
   }

   unless ($sth->execute($reportid, $worksheet)) {
      &display_error("ERROR executing SQL:", $sth->errstr);
      exit(0);
   }

   unless ($sth->rows()) {
      &display_error("NOTHING AVAILABLE FOR REPORT ID: $sql <BR>$reportid");
      return(0);
   }

   my $key;
   my %col_hash;
   my %value_hash;

   while (($row, $col, $value) = $sth->fetchrow_array) {
       $value_hash{"$row:$col"} = $value;
       $col_hash{$col} = 'Y';
   }

   print "Content-Type: text/html\n\n";

   print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
<HEAD><TITLE>WORKSHEET B-1 EXPLORER</TITLE>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon">
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
<link href="/JS/ergosign.styleform.css" media="all" type="text/css" rel="stylesheet">

    <script type="text/javascript" src="/JS/mootools-1.2-core.js"></script>

    <script type="text/javascript" src="/JS/ergosign.styleform.js"></script>

    <script type="text/javascript" src="/JS/script.js"></script>
    <link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />
</HEAD>
<BODY>
<CENTER>
<BR>
<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/cgi-bin/demo_index.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/demo_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>Standard Reports</div>">
			        <img src="/images/stardardreport_inner_icon.gif" onmouseover="src='/images/stardardreport_inner_icon_hover.gif';"
                                    onmouseout="src='/images/stardardreport_inner_icon.gif';" style="border: 0px; cursor: pointer" /></a></td>
                       <td style="width: 102px" align="Center">
		                                   <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
		                                       <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->
    <!--content start-->
    <table cellpadding="0" cellspacing="0" width="885px">
            <tr><td align="left">Welcome <b>
                	};
                	
                	print $firstName . " " . $lastName;
                	print qq{
                	</b></td>  
                	<td align="right">
                	<a href="#">Sign Out</a>
                	</td>
            	</tr>
    </table>
        <table cellpadding="0" cellspacing="0" width="885px">
            <tr>
                <td valign="top">
                    <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px">
<TABLE BORDER="0" CELLPADDING="2" width="100%">
   <TR>
     <TD ALIGN="LEFT" VALIGN="TOP">
       <Div class="pageheader"> 
        
		$hospital_name</div>
        <span class="tahoma12"><b>$hospital_address<BR>
        $hospital_city, $hospital_state  $hospital_zip
        </B></span>
       
     </TD>
     <TD ALIGN="left" VALIGN="TOP" class="tahoma12" style="border-left:dotted 1px #ffffff; padding-left:10px;">
       WORKSHEET:<BR>
        PROVIDER:<BR>
        FY BEGIN:<BR>
          FY END:<BR>
          STATUS:<BR>
     </TD>
     <TD ALIGN="LEFT" VALIGN="TOP" class="tahoma12" style="border-right:dotted 1px #ffffff; padding-left:10px;">
        <B>
        <FONT COLOR="#EE0000">
        $worksheet<BR>
        $rpt_provider<BR>
        $fy_bgn_dt<BR>
        $fy_end_dt<BR>
        $rpt_status<BR>
        </FONT>
        </B>
     </TD>
     <TD ALIGN="RIGHT" VALIGN="TOP">
        <IMG SRC="/icons/CMSLogo.gif">
     </TD>
   </TR>
   </TABLE>   
};
   &CR_dropdown;
print qq{
   <TABLE  BORDER="0" CELLPADDING="6" CELLSPACING="0" width="100%" class="gridstyle">
   <TR CLASS="gridheader">
       <TD ALIGN=\"CENTER\"><B>LINE</B></TD>
       <TD ALIGN=\"CENTER\"><B>TITLE</B></TD>
};

   my ($s, $alt, $key, $title);
 
   my %col_index;

   my $column = 0;

   foreach $key (sort keys %col_hash) {
        $s = $column_labels{$key};

        if ($ini_value{'PLUG_COLUMN_HEADINGS'} eq 'Y') {
           $alt = $a_labels{"0$key"};
           if (defined($alt)) {
              $s = "<FONT COLOR=\"RED\">$alt</FONT>";
           }
        }

       print "      <TD ALIGN=\"CENTER\"><B>$s</B><BR><FONT COLOR=\"GREEN\">($key)</FONT></TD>\n";
       
       $col_index{$key} = $column;
       ++$column;
   }

   my $table_columns = $column;

   print "</TR>\n";

   my $save_row = 'void';
   my $count    = 0;
   my $n;
   my @element;

   foreach $key (sort keys %value_hash) {
      ($row,$col) = split /:/,$key;
      $value = $value_hash{$key};

      unless ($row eq $save_row) {
         unless ($save_row eq 'void') {
             &render_row($save_row, ++$count, $table_columns, @element);
             @element = ();
         }
         $save_row = $row;
      }

      $n = $col_index{$col};
      $element[$n] = $value;
   }

   &render_row($save_row, ++$count, $table_columns, @element);
   @element = ();

   print qq{
</TABLE>
<FONT SIZE="1">
$count ROWS<BR>
</FONT>
</td>
            <td valign="top">
                <img src="/images/content_right.gif" /></td>
        </tr>
    </table>
    <!--content end-->
	};
&TDdoc;
print qq{
</CENTER>
</BODY>
</HTML>
   };

   return(0);
}



#----------------------------------------------------------------------------

sub render_row
{

   my ($row, $count, $table_columns, @elements) = @_;

   my ($i, $align, $title, $value);

   my $alt = $a_labels{$row};

#  if (defined($alt)) {
#     $title = "(A) $alt";
#  } else {
#     $title = $row_labels{$row};
#  }

   $title = $row_labels{$row};

   unless ($title) {
      if (defined($alt)) {
         $title = "<FONT COLOR=\"RED\">$alt</FONT>";
      }
   }

   my $class = ($count % 2) ? 'gridrow' : 'gridalternate';

   print "   <TR CLASS=\"$class\">\n";
   print "       <TD ALIGN=\"CENTER\"><FONT COLOR=\"GREEN\">($row)</FONT></TD>\n";
   print "       <TD ALIGN=\"CENTER\" NOWRAP=\"1\"><B>$title</B></TD>\n";

   for ($i = 0; $i < $table_columns; ++$i) {
       if (defined($elements[$i])) {
          $value = $elements[$i];
       } else {
          $value = '&nbsp';
       }

       $align = 'LEFT';

       if ($value =~ m/^-?\d+$|-?\d*\.\d+$/) {
          $align = 'RIGHT';
          $value = &comify($value);
       }

       print "       <TD ALIGN=\"$align\"><DIV TITLE=\"$worksheet:$row\">$value</DIV></TD>\n";
   }
   print "   </TR>\n";
}


#----------------------------------------------------------------------------


sub load_a_table
{
   my ($reportid, $year) = @_;

   my ($row, $col, $value);
# AND RPT_YEAR		= $year
   my ($sth, $sql);
 if($facility eq 'HOSPITAL'){
$sql = qq/
       SELECT CR_LINE, CR_COLUMN, CR_VALUE
         FROM CR_ALL_DATA
        WHERE CR_REC_NUM    = ?
          AND CR_COLUMN     = '0000'
          AND CR_WORKSHEET  = 'A000000'
		 
     ORDER BY CR_LINE, CR_COLUMN
      /;

} 
if($facility eq 'RENAL'){	
   $sql = qq/
       SELECT CR_LINE, CR_COLUMN, CR_VALUE
         FROM rnl_ALL_DATA
        WHERE CR_REC_NUM    = ?
          AND CR_COLUMN     = '0000'
          AND CR_WORKSHEET  = 'A000000'
		 
     ORDER BY CR_LINE, CR_COLUMN
      /;
}
if($facility eq 'HHA'){	
   $sql = qq/
       SELECT CR_LINE, CR_COLUMN, CR_VALUE
         FROM hha_ALL_DATA
        WHERE CR_REC_NUM    = ?
          AND CR_COLUMN     = '0000'
          AND CR_WORKSHEET  = 'A000000'
		 
     ORDER BY CR_LINE, CR_COLUMN
      /;
}
if($facility eq 'SNF'){	
   $sql = qq/
       SELECT CR_LINE, CR_COLUMN, CR_VALUE
         FROM snf_ALL_DATA
        WHERE CR_REC_NUM    = ?
          AND CR_COLUMN     = '0000'
          AND CR_WORKSHEET  = 'A000000'
		 
     ORDER BY CR_LINE, CR_COLUMN
      /;
}
if($facility eq 'HOSPICE'){	
   $sql = qq/
       SELECT CR_LINE, CR_COLUMN, CR_VALUE
         FROM hospc_ALL_DATA
        WHERE CR_REC_NUM    = ?
          AND CR_COLUMN     = '0000'
          AND CR_WORKSHEET  = 'A000000'
		 
     ORDER BY CR_LINE, CR_COLUMN
      /;
}
   unless ($sth = $dbh->prepare($sql)) {
      &display_error("ERROR preparing SQL:", $sth->errstr);
      exit(0);
   }

   unless ($sth->execute($reportid)) {
      &display_error("ERROR executing SQL:", $sth->errstr);
      exit(0);
   }

   my $key;

   while (($row, $col, $value) = $sth->fetchrow_array) {
      if ($value =~ m/^(\d\d\d\d\d)(.+$)/) {
         $value = "$1 $2";
      }
      $a_labels{$row} = $value;
   }

   $sth->finish();
}


#----------------------------------------------------------------------------

sub get_value_by_addr
{
   my ($year, $reportid, $address) = @_;

   my ($worksheet, $row, $col) = split /[-:\|]/,$address;

   my ($sth, $sql);
# AND RPT_YEAR		= $year
 if($facility eq 'HOSPITAL'){
  $sql = qq/
    SELECT CR_VALUE         
      FROM CR_ALL_DATA 
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
   /;
 }
if($facility eq 'RENAL'){
   $sql = qq/
    SELECT CR_VALUE         
      FROM rnl_ALL_DATA
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
	 
   /;
}
if($facility eq 'HHA'){
   $sql = qq/
    SELECT CR_VALUE         
      FROM hha_ALL_DATA 
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
	 
   /;
}

if($facility eq 'SNF'){
   $sql = qq/
    SELECT CR_VALUE         
      FROM snf_ALL_DATA
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
	 
   /;
}
if($facility eq 'HOSPICE'){
   $sql = qq/
    SELECT CR_VALUE         
      FROM hospc_ALL_DATA 
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
	 
   /;
}
   unless ($sth = $dbh->prepare($sql)) {
       &display_error("Error preparing sql query", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   $sth->execute($reportid, $worksheet, $row, $col);

   my ($value) = $sth->fetchrow_array;

   return($value);
}


#----------------------------------------------------------------------------

sub display_error
{
   my @list = @_;

   my ($string, $s);

   foreach $s (@list) {
      $string .= "$s<BR>\n";
   }

   print "Content-Type: text/html\n\n";

   print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
<HEAD><TITLE>ERROR</TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
<link href="/JS/ergosign.styleform.css" media="all" type="text/css" rel="stylesheet">

    <script type="text/javascript" src="/JS/mootools-1.2-core.js"></script>

    <script type="text/javascript" src="/JS/ergosign.styleform.js"></script>

    <script type="text/javascript" src="/JS/script.js"></script>
    <link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />
</HEAD>
<BODY>
<CENTER>
<BR>
<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/cgi-bin/demo_index.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/demo_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>Standard Reports</div>">
			        <img src="/images/stardardreport_inner_icon.gif" onmouseover="src='/images/stardardreport_inner_icon_hover.gif';"
                                    onmouseout="src='/images/stardardreport_inner_icon.gif';" style="border: 0px; cursor: pointer" /></a></td>
                      <td style="width: 102px" align="Center">
		                                  <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
		                                      <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->
    <!--content start-->
    <table cellpadding="0" cellspacing="0" width="885px">
            <tr><td align="left">Welcome <b>
                	};
                	
                	print $firstName . " " . $lastName;
                	print qq{
                	</b></td>  
                	<td align="right">
                	<a href="#">Sign Out</a>
                	</td>
            	</tr>
    </table>
        <table cellpadding="0" cellspacing="0" width="885px">
            <tr>
                <td valign="top">
                    <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px">
<FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<TABLE CLASS="error" WIDTH="600">
   <TR>
      <TD ALIGN="CENTER">
          <FONT SIZE="4">
          <BR>
          $string
          <BR>
          </FONT>
      </TD>};
	  
	     &CR_dropdown;
	  print qq{
	  
   </TR>
</TABLE>

<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM></td>
            <td valign="top">
                <img src="/images/content_right.gif" /></td>
        </tr>
    </table>
    <!--content end-->
</CENTER>
</BODY>
</HTML>
   };

   return(0);
}

#----------------------------------------------------------------------------
sub CR_dropdown
{
#print "Content-Type: text/html\n\n";
my $rpt= substr $prov,1;
my @rptprov = split(/,/, $rpt);
my $rpt_list =	substr $rpt_no,1;
my @rpt_id = split(/,/, $rpt_list);
print qq{

<script>
function opt_select(){
var opt=document.frmcrexplorer.CR.options[document.frmcrexplorer.CR.selectedIndex].value;
window.parent.location.href="/cgi-bin/cr_explore2.pl?facility=$facility&provider=$provider&reportid="+opt+"&action=go";
}
</script>


<form name="frmcrexplorer">
<table align="left">
<tr>
<td>
<strong>Cost Reports </strong>
	<select name="CR" title="CHOOSE A COST REPORT" onchange="opt_select()">	
	<option >Select CR end date</option>
	};
	
		for(my $i=0;$i< scalar(@rptprov);$i++){
			print "<option name=\"Report\" value=".@rpt_id[$i].">".@rptprov[$i]."</option>";
		}
	

	
	print qq{
	</select>
	</td>
	</tr>
	</table>
	</form>
	};		
}

#----------------------------------------------------------------------------
sub comify
{
  local($_) = shift;
  1 while s/^(-?\d+)(\d{3})/$1,$2/;
  return($_);
}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#------------------------------------------------------------------------------


sub load_table_definitions
{
   my ($worksheet) = @_;

   my ($line, $aaa, $bbb);

   unless (-f "definitions/$worksheet.def") {
      &display_error("NO TABLE DEFINITION: $worksheet.def");
   }

   open INPUT, "definitions/$worksheet.def";


   while ($line = <INPUT>) {

      next if ($line =~ m/^\#/);   # comment

      if ($line =~ m/(.*)=(.*)/) {
         $ini_value{$1} = $2;
      }

      if ($line =~ m/^ROW-LABELS-BEGIN/) {
               print STDERR "CHECKPOINT BBB\n";
         do {
            $line = <INPUT>;
            chomp($line);
            ($aaa, $bbb) = split /\t/, $line;
            if ($bbb) {
               $row_labels{$aaa} = $bbb;
               print STDERR "$aaa $bbb\n";
            }
         } until ($line =~ m/^ROW-LABELS-END/ or eof(INPUT));
      }

      if ($line =~ m/^COLUMN-LABELS-BEGIN/) {
         do {
            $line = <INPUT>;
            chomp($line);
            ($aaa, $bbb) = split /\t/, $line;
            if ($bbb) {
               $column_labels{$aaa} = $bbb;
               print STDERR "$aaa $bbb\n";
            }
         } until ($line =~ m/^COLUMN-LABELS-END/ or eof(INPUT));
      }
   }

   close INPUT;
}

#--------------------------------------------------------------------------------------
sub sessionExpire
{

print "Content-Type: text/html\n\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

