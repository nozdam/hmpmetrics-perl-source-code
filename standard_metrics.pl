#!/usr/bin/perl

use strict;
use Spreadsheet::WriteExcel;
use Date::Calc qw(Delta_Days);
use CGI;
use DBI;
do "common_header.pl";
do "pagefooter.pl";
use CGI::Session;
do "audit.pl";

my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $saction = $cgi->param("saction");
my $element = $cgi->param("txtElement");
my $metric = $cgi->param("txtMetric");
my $worksheet = $cgi->param("txtWorksheet");
my $lineno = $cgi->param("txtLineNo");
my $columnno = $cgi->param("txtColumnNo");
my $isformula=$cgi->param("txtIsfor");
my $oldFormula=$cgi->param("hdnOldformula");
my $oldElement=$cgi->param("hdnOldelement");
my $reasonChange=$cgi->param("txtReason");
my $metr_name = $cgi->param("txtMetrx_name");
my $matricsFormula = $cgi->param("formula");
my $btnType = $cgi->param("btnType");
my $txtExpression = $cgi->param("txtExpression");
my $rpt_details_isFormulae = $cgi->param("isFormulae");
my $facility = $cgi->param("facility");
my $msg ;

if(!$userID)
{
	&sessionExpire;
}
my $heading = uc($facility) . " STANDARD METRICS";
my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};

my ($dbh, $sql,$row,$facility_id, $sth);

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error('ERROR connecting to database', $DBI::errstr);
   exit(1);
}
my ($sql1,$sth1,$row1);
my ($sql,$sth,$sql2,$sth2);

my $sel_for = $cgi->param("paramA");
my $element_name = $cgi->param("paramB");

$sql = "select facility_id from tbl_facility_type where facility_name = '$facility'";
$sth = $dbh->prepare($sql);
$sth->execute();
while ($row = $sth->fetchrow_hashref) {
	$facility_id = $$row{facility_id};
}

$sql1 = "select metrics_id,element,isFormulae,formulae,quartile_db_column from standard_metrics where Formulae !='' and facility_type = $facility_id order by element ";
$sth1 = $dbh->prepare($sql1);
$sth1->execute();

my ($metricsid,$elements, $formulae,$isFor,$metric_column_name);

my $formula_optionsA ="<OPTION VALUE='' SELECTED>-- Select --</OPTION>";

while (($metricsid,$elements,$isFor,$formulae,$metric_column_name) = $sth1->fetchrow_array) {
	if($isFor == 0){
      $formula_optionsA = $formula_optionsA."<OPTION VALUE='$formulae~$metricsid~$metric_column_name'>$elements (MI)</OPTION>";
	 	}else{
	  $formula_optionsA = $formula_optionsA."<OPTION VALUE='$formulae~$metricsid~$metric_column_name'>$elements (F)</OPTION>";
}
}
$sth1->finish();

print "Content-Type: text/html\n\n";


print qq{
<HTML>
<HEAD><TITLE>Standard Metrics</TITLE>
 <script type="text/javascript" src="/JS/AjaxScripts/prototype.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/effects.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/controls.js"></script>
 <style>
 form
 {
 margin:0;
 }
 </style>
</HEAD>
<BODY>
<BR>
<CENTER>
};
&innerHeader();
print qq{

<div id='bodyDiv' style="position:absolute;z-index:100;display:none;background-image:url(/images/background-trans.png);height:96%;width:96%;"><div align='center'  id="displaybox" style="display: none;"></div></div>

<!--content start-->

			<table bgColor=\"#B4CDCD\" width="885px" align=\"centre\">
			<tr>
					<td width=\"55%\"><b>$heading</b></td>
					<td align="right" ><span class=\"button\"><input type=\"button\" name=\"btnCreate\" value=\"Create New Metric\" onClick=\"CallCreate()\"></span></td>
			</tr>
			</table>
<table cellpadding="0" cellspacing="0" width="885px">
        <tr>
			    <td valign="top">
                <img src="/images/content_left.gif" /></td>
            <td valign="top" style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; ">

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Formulas - Select Mode</title>
    <link href="/css/tabs.css" rel="stylesheet" type="text/css" />
	<link href="/css/admin_panel/user_management/userManagement.css" rel="stylesheet" type="text/css" />

<script language="javascript" src="/JS/admin_panel/standard_metrics/standard_metrics.js" ></script>
<link href="/css/admin_panel/standard_metrics/standard_metrics.css" rel="stylesheet" type="text/css" />

<form name="frmfromulaTab" Action="/cgi-bin/standard_metrics.pl?facility=$facility">
<body onload="selectBox()">
<input type="hidden" name="hdnNewelement" value="">
<input type="hidden" name="hdnOldelement" value="">
<input type="hidden" name="hdnNewformula" value="">
<input type="hidden" name="hdnOldformula" value="">
<input type="hidden" name="element" value="$element">
<input type="hidden" name="facility_id" value="$facility_id">
<input type="hidden" name="sel_for" value="$sel_for">
<input type="hidden" name="ajaxCall" value="">
<input type="hidden" name="txtIsfor" value="">
<input type="hidden" name="txtMetric" value="">
<input type="hidden" name="txtElement" value="">
<input type="hidden" name="optionsA" value ="$formula_optionsA">
<input type="hidden" name="saction" value="">
<input type="hidden" name="facility" value="$facility">
<input type="hidden" name="btnType" value="">
<div id="content">
    <table align="center" width="80%" height="20%" cellpadding="0" cellspacing="0">
	<tr><td >&nbsp;</td></tr>
	    	<tr>
    			<td align="left" class="gridHlink">Select Metrics :</td>
				<td align="right">
				(MI)- Master Indicator
				<br>
				(F)-Formula
				</td>
    		</tr>
    		<tr>
    			<td>
    				<div id="selfrm"></div>
    			</td>
    		</tr>
			<tr><td >&nbsp;</td></tr>
			<tr>
				<td>
					Metric Name : <input type="text" id="txtMetrx_name" name="txtMetrx_name" style = "width:410px;height:20px;" value="" >
				</td>
			</tr>
			<tr><td >&nbsp;</td></tr>
			<tr>
			<td style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">  Element : <input id="poBoxRadioNo" name="poBoxRadio" type="radio" class="radio-btn" value="No" onClick="setWindow(this,0);"/> Single Cost Report Address</td><td style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\"> <input id="poBoxRadioYes" name="poBoxRadio" type="radio" class="radio-btn" value="Yes" onClick="setWindow(this,1);" checked/> Formula</td>
			</tr>

			<tr><td >&nbsp;</td></tr>
			<tr>
			<td style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\"><input type="checkbox" name="create_provider_metric" value="1"/> Create Provider Metric?</td>
			</tr>

			<tr><td >&nbsp;</td></tr>

    		<tr>
		    	<td style="width:20px;height:14px;">
		    		<textarea name="txtExpression" style="height:162px;width:410px;"></textarea>

					<div name = "micontent" id = "micontent" style="display:none;width:410px;">
<table align="center" width="100%" height="80%" cellpadding="0"  border="0" cellspacing="0">
    		 <tr height="50px"><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Worksheet : </td><td style=\"background-color:#edf2fb\"><input type=\"text\" name=\"txtWorksheet\" id=\"txtWorksheet\"  maxLength=\"7\" value = "$worksheet"></td></tr>\n
			<tr height="50px"><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Line No : </td><td style=\"background-color:#edf2fb\"><input type=\"text\" name=\"txtLineNo\"  maxLength=\"5\"></td></tr>\n
			<tr height="50px"><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Column No : </td><td style=\"background-color:#edf2fb\"><input type=\"text\" name=\"txtColumnNo\" maxLength=\"4\"></td></tr>

</table>
			</div>
		    	</td>
    		</tr>
    		<tr>
    			<td align="left" valign="top">

		    	<table name = "formulabuttons" id = "formulabuttons" align="left"  width="20%" cellpadding="0" cellspacing="0">

		    	<tr>

		    	<td  align="left" nowrap><span  class=\"button\"><input type="button" name="btnOpenBrace" value =" ( " onclick="AddOperator(this)"></span>
		    	&nbsp;<span  class=\"button\"><input type="button" style="width:25px;" name="btnCloseBrace" value =" ) " onclick="AddOperator(this)"></span>
		    	&nbsp;<span  class=\"button\"><input type="button" style="width:25px;" name="btnModule" value =" % " onclick="AddOperator(this)"></span>
		    	&nbsp;<span  class=\"button\"><input style="width:25px;" type="button" name="btnAdd" value =" + " onclick="AddOperator(this)"></span>
		    	&nbsp;<span  class=\"button\"><input style="width:25px;" type="button" name="btnMulti" value =" * " onclick="AddOperator(this)"></span>
		    	&nbsp;<span  class=\"button\"><input style="width:25px;" type="button" name="btnDivid" value =" / " onclick="AddOperator(this)"></span>
		    	&nbsp;<span  class=\"button\"><input style="width:25px;" type="button" name="btnMinus" value =" - " onclick="AddOperator(this)"></span>
		    	&nbsp;<span  class=\"button\"><input style="width:50px;" type="button" name="btnClear" value =" Clear " onclick="ClearIt()"></span></td>

		</tr>
</table>
</div>
<div id="reason" style="margin-top:10px; clear:both;">
<b> Reason for change </b> (Max:600 characters) :<font color ="red" > *  </font>
	<br>
	<form name=my_form method=post>
<textarea name="txtReason" rows="3" cols="45" onKeyPress=check_length(this.form);>
</textarea>
</form>
</div>
<div id="mibuttons" style="display:none;">
			<span  class=\"button\"><input style="width:100px;" type="button" name="btnUpdate" id="Update" value ="Update" onclick = "callSubmit()" ></span>&nbsp;<span  class=\"button\"><input style="width:100px;" type="button" name="btnDelete" id="Delete" value ="Delete" onclick = "deleteMatrics()"></span>
</div>
<div id="mibuttons1">
	<span  class=\"button\"><input type="button" style="width:50px;" name="btnLoad" id="Load" value ="Update" onclick="callSubmit1()"></span>&nbsp;<span  class=\"button\"><input type="button" style="width:50px;" name="btnLoad" id="Delete" value ="Delete" onclick="deleteMatrics()"></span>
</div>
   	</td>
    		</tr>
</table>

};
print qq{
<br>
<div id="crdata">

</div>
<BR>
<div id="history">
</div>
</form>
</body>
</html>
};
print $msg;
print qq{

<input type="hidden" name="hdnColumnNo" value="">
<input type="hidden" name="hdnLineNo" value="">
<input type="hidden" name="hdnWorksheet" value="">
};

#-----------------------------
sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#--------------------------------------------------------------------------------------

sub sessionExpire
{
print "Content-Type: text/html\n\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};
print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";
print qq{
</body>
</html>
};
}

#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;
my $string;
print "Content-Type: text/html\n\n";
print qq{
<HTML>
<HEAD><TITLE>Standard Reports-ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};
}





