#!/usr/bin/perl

use DBI;
use DateTime;
use CGI::Session;

do "common_header.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $userID = $session->param("userID");

my $dbh;
my %ini_value;

unless (&read_inifileNew('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};
my $upload_dir = $ini_value{'PEER_IMPORT_FILE_PATH'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error('ERROR connecting to database', $DBI::errstr);
   exit(1);
}

#-----------------------------------------------------------------------------------

sub insert{

my ($action,$metric_id,$new_value) = @_;
	my $dt = DateTime->now(time_zone=>'local');
	my ($sql,$sth);
	my $user_ip = $cgi->remote_host();

$sql = "insert into metric_history(user_id,log_date,action,metric_id,new_value,user_ip) values(?,?,?,?,?,?)";

					
	unless ($sth = $dbh->prepare($sql)) {
		&display_error('ERROR preparing SQL', $sth->errstr);
		$dbh->disconnect();
		exit(1);
	}	
	
	unless ($sth->execute($userID,$dt,$action,$metric_id,$new_value,$user_ip)) {
		&display_error('ERROR executing SQL', $sth->errstr, $sql);
		$dbh->disconnect();
		exit(1);
   	}
}

#-----------------------------------------------------------------------------
sub update{

my ($action,$metric_id,$description,$new_value,$old_value) = @_;
	my $dt = DateTime->now(time_zone=>'local');
	my ($sql,$sth);
	my $user_ip = $cgi->remote_host();

$sql = "insert into metric_history(user_id,log_date,action,metric_id,old_value,new_value,description,user_ip) values(?,?,?,?,?,?,?,?)";

					
	unless ($sth = $dbh->prepare($sql)) {
		&display_error('ERROR preparing SQL', $sth->errstr);
		$dbh->disconnect();
		exit(1);
	}	
	
	unless ($sth->execute($userID,$dt,$action,$metric_id,$old_value,$new_value,$description,$user_ip)) {
		&display_error('ERROR executing SQL', $sth->errstr, $sql);
		$dbh->disconnect();
		exit(1);
   	}
}
#------------------------------------------------------------------------

sub delete{

my ($action,$metric_id,$description,$old_value) = @_;
	my $dt = DateTime->now(time_zone=>'local');
	my ($sql,$sth);
	my $user_ip = $cgi->remote_host();

$sql = "insert into metric_history(user_id,log_date,action,metric_id,old_value,description,user_ip) values(?,?,?,?,?,?,?)";

					
	unless ($sth = $dbh->prepare($sql)) {
		&display_error('ERROR preparing SQL', $sth->errstr);
		$dbh->disconnect();
		exit(1);
	}	
	
	unless ($sth->execute($userID,$dt,$action,$metric_id,$old_value,$description,$user_ip)) {
		&display_error('ERROR executing SQL', $sth->errstr, $sql);
		$dbh->disconnect();
		exit(1);
   	}
}
#----------------------------------------------------------------------

sub read_inifileNew
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}


#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;
my $string;
print "Content-Type: text/html\n\n";
print qq{
<HTML>
<HEAD><TITLE>Standard Reports-ERROR</TITLE>
<link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};
}








