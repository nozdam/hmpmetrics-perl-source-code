#!/usr/bin/perl -w

#----------------------------------------------------------------------------
#  program: snf_setedit.pl
#   author: Steve Spicer
#  Written: Sun Nov 23 13:23:42 CST 2008
#  Revised:
#----------------------------------------------------------------------------

use strict;
use CGI;

my $cgi = new CGI;

my %ini_value;

my $set       = $cgi->param('set');  
my $action    = $cgi->param('action');  

if ($action eq "save") {
   &save_results($set)
} else {
   &display_form();
}

exit(0);

#----------------------------------------------------------------------------

sub display_error
{
   my @list = @_;

   my ($string, $s);

   foreach $s (@list) {
      $string .= "$s<BR>\n";
   }

   print "Content-Type: text/html\n\n";

   print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css" />

</HEAD>
<BODY onload="styleForm()">
<CENTER>
<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/index.html" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/cr_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="/cgi-bin/cr_batchx.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="/cgi-bin/snf_batch1.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="/cgi-bin/graph.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Graph Utility</div>">
                                <img src="/images/graph_icon_inner.gif" onmouseover="src='/images/graph_icon_inner_hover.gif';"
                                    onmouseout="src='/images/graph_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                      <td style="width: 102px" align="Center">
		                                  <a href="/cgi-bin/PeerMain.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
		                                      <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->
    
<FONT SIZE="5" COLOR="RED">
<B>ERROR:</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" VALUE=" CLOSE " onclick="window.close()"></span>
</FORM>
</CENTER>
</BODY>
</HTML>
   };

   return(0);
}

#----------------------------------------------------------------------------

sub display_form
{
   unless (open INPUT, "snf_packages/$set") {
      &display_error("CAN'T OPEN SET \"$set\"");
      return(0);
   }

   print "Content-Type: text/html\n\n";
   
   print <<EOF
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
<HEAD>
<TITLE>SELECTED ELEMENT EDITOR</TITLE>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon">
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css" />
<link href="/JS/ergosign.styleform.css" media="all" type="text/css" rel="stylesheet">

    <script type="text/javascript" src="/JS/mootools-1.2-core.js"></script>

    <script type="text/javascript" src="/JS/ergosign.styleform.js"></script>

    <script type="text/javascript" src="/JS/script.js"></script>
    <link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function setfocus() {
   document.forms[0].desc.focus();
}

function save_results() {
   rename_variables();
   document.form1.submit();
}

function rename_variables() {
   var table = document.getElementById('TBL1');
   var row = table.getElementsByTagName('tr'); 

   for (var i = 0; i < row.length; ++i) {
        var col = row[i].getElementsByTagName('input');
        for (var x = 0; x < col.length; ++x) {
           oldname = col[x].getAttribute('name').substr(0,3);
           col[x].setAttribute('name', oldname + '-' + i);
        }

        var col = row[i].getElementsByTagName('select');
        for (var x = 0; x < col.length; ++x) {
           oldname = col[x].getAttribute('name').substr(0,3);
           col[x].setAttribute('name', oldname + '-' + i);
        }
    }
}

function delete_row(obj) {  
   var row   = obj.parentNode.parentNode;
   var index = row.sectionRowIndex;
   document.getElementById('TBL1').deleteRow(index);
}

function add_row() {
   var tbl = document.getElementById('TBL1');
   var lastRow = tbl.rows.length;
   var newrow  = tbl.insertRow(lastRow);
   populate_row(newrow);
}

function insert_row(obj) {  
   var row   = obj.parentNode.parentNode;
   var index = row.sectionRowIndex;
   var newrow = document.getElementById('TBL1').insertRow(index);
   populate_row(newrow);
}

function populate_row(newrow) {
   var newcell0 = newrow.insertCell(0);
   var newcell1 = newrow.insertCell(1);
   var newcell2 = newrow.insertCell(2);
   var newcell3 = newrow.insertCell(3);
   var newcell4 = newrow.insertCell(4);

   newcell0.innerHTML = '<INPUT TYPE="TEXT" NAME="WRK" SIZE="7">';
   newcell1.innerHTML = '<INPUT TYPE="TEXT" NAME="ROW" SIZE="5">';
   newcell2.innerHTML = '<INPUT TYPE="TEXT" NAME="COL" SIZE="5">';
   newcell3.innerHTML = '<SELECT NAME="FMT">'
                      + '    <OPTION VALUE="O">OTHER</OPTION>'
                      + '    <OPTION VALUE="S">STRING</OPTION>'
                      + '    <OPTION VALUE="C">MONEY</OPTION>'
                      + '    <OPTION VALUE="D">DECIMAL</OPTION>'
                      + '    <OPTION VALUE="N">NUMBER</OPTION>'
                      + '</SELECT>';

   newcell4.innerHTML = '<INPUT TYPE="TEXT\" NAME="DES" SIZE="35">' +
                        '&nbsp;<IMG SRC="/icons/delete_icon.gif" onclick="delete_row(this)" style=/"cursor:pointer/">' +
                        '&nbsp;<IMG SRC="/icons/plus-green.gif" onclick="insert_row(this)" style=/"cursor:pointer/">';
}

</script>

</HEAD>
<BODY onload="styleForm()">
<CENTER>
<FORM NAME="form1" ID="form1" METHOD="POST" ACTION="/cgi-bin/snf_setedit.pl">
<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/index.html" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/cr_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="/cgi-bin/cr_batchx.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="/cgi-bin/snf_batch1.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="/cgi-bin/graph.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Graph Utility</div>">
                                <img src="/images/graph_icon_inner.gif" onmouseover="src='/images/graph_icon_inner_hover.gif';"
                                    onmouseout="src='/images/graph_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                       <td style="width: 102px" align="Center">
		                                   <a href="/cgi-bin/PeerMain.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
		                                       <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->
    <!--content start--><br>
        <table cellpadding="0" cellspacing="0" width="885px">
            <tr>
                <td valign="top">
                    <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px" valign="top">

<Div class="pageheader">EDIT SELECTION SET: $set</div>
<div  class="gridstyle" style="background-color:white; padding:10px">
<TABLE NAME="TBL1" ID="TBL1" BORDER="0"  CELLPADDING="1" WIDTH="100%">
  
EOF
;

   my %lookup = (
     'O' => 'OTHER',
     'S' => 'STRING',
     'C' => 'MONEY',
     'D' => 'DECIMAL',
     'N' => 'NUMBER',
   );

   my ($line, $option_list, $selected, $key);
   my ($worksheet, $row, $col, $fmt, $desc);

   while ($line = <INPUT>) {
      chomp($line);

      next if ($line =~ m/^#|^$/);

      ($worksheet, $row, $col, $fmt, $desc) = split /\|/,$line;

      $option_list = '';
       
      foreach $key (keys %lookup) {
         $selected = ($key eq $fmt) ? 'SELECTED' : '';
         $option_list .= "         <OPTION VALUE=\"$key\" $selected>$lookup{$key}</OPTION>\n";
      }

      print "   <TR>\n";
      print "      <TD ALIGN=\"left\"><INPUT TYPE=\"TEXT\" class=\"textboxstyle\" NAME=\"WRK\" SIZE=\"7\" VALUE=\"$worksheet\"></TD>\n";
      print "      <TD ALIGN=\"left\"><INPUT TYPE=\"TEXT\" class=\"textboxstyle\" NAME=\"ROW\" SIZE=\"5\" VALUE=\"$row\"></TD>\n";
      print "      <TD ALIGN=\"left\"><INPUT TYPE=\"TEXT\" class=\"textboxstyle\" NAME=\"COL\" SIZE=\"5\" VALUE=\"$col\"></TD>\n";
      print "      <TD ALIGN=\"left\">\n";
      print "         <SELECT NAME=\"FMT\">\n";
      print              "$option_list";
      print "         </SELECT>\n";
      print "      </TD>\n";
      print "      <TD ALIGN=\"left\">\n";
      print "        <INPUT TYPE=\"TEXT\" class=\"textboxstyle\" NAME=\"DES\" SIZE=\"35\" VALUE=\"$desc\">\n";
      print "        <IMG SRC=\"/icons/delete_icon.gif\" onclick=\"delete_row(this)\" ALT=\"DELETE THIS ROW\" style=\"cursor:pointer; padding-left:25px\">\n";
      print "        <IMG SRC=\"/icons/plus-green.gif\" onclick=\"insert_row(this)\" ALT=\"INSERT ROW BEFORE THIS ONE\" style=\"cursor:pointer; padding-left:25px\">\n";
      print "      </TD>\n";
      print "   </TR>\n";
   }

   close INPUT;

    print qq{
</TABLE></div>
<Div style="padding-top:5px">
<Table cellpadding="0" cellspacing="0">
	<tr>
		<Td>
<span class="button">
<INPUT TYPE="button" VALUE="  ADD ROW TO BOTTOM  " onclick="add_row()"></span>
<INPUT TYPE="hidden" NAME="action" VALUE="save">
<span class="button"><INPUT TYPE="button" VALUE="  SAVE AS  " onclick="save_results()"></span>
</td>
		<td style="padding-top:2px; padding-left:5px"><INPUT TYPE="TEXT" class="textboxstyle" NAME="set" VALUE="$set" SIZE="15" MAXLENGTH="15"></</td>
</tr>
</table>
</div>

</td>
            <td valign="top">
                <img src="/images/content_right.gif" /></td>
        </tr>
    </table>
    <!--content end-->
</FORM>
</CENTER>
</BODY>
</HTML>
   };

}

#----------------------------------------------------------------------------

sub save_results
{
   my ($fname) = @_;

   $fname =~ s/\///g;  # remove any slashes
   $fname =~ s/ /_/g;  # i dont like spaces
   $fname = lc($fname);

   unless ($fname =~ m/\.txt$/) {
     $fname .= '.txt';
   }

   unless (open OUTPUT, ">./snf_packages/$fname") {
     display_error("ERROR OPENING \"$fname\" FOR WRITING", $!);
     return(0);
   }
        
   my %hash;

   my @names = $cgi->param;

   my ($i, $name, $value, $wrk, $row, $col, $fmt, $des);

   my $max = 0;

   foreach $name (@names) {
       $value = $cgi->param("$name");
   
       if ($name =~ m/^([A-Z]+)-(\d+)$/) {
          ${$hash{$1}}[$2] = $value;
          $max = $2 if ($2 > $max);
       }
   }

   for ($i = 1; $i < $max; ++$i)  {
       $wrk = ${$hash{'WRK'}}[$i]; 
       $row = ${$hash{'ROW'}}[$i]; 
       $col = ${$hash{'COL'}}[$i]; 
       $fmt = ${$hash{'FMT'}}[$i]; 
       $des = ${$hash{'DES'}}[$i]; 
       print OUTPUT "$wrk|$row|$col|$fmt|$des\n";
   }

   close OUTPUT;

   &display_success;

   return 0;
}

#----------------------------------------------------------------------------

sub display_success
{
   my $now = localtime();

   print "Content-Type: text/html\n\n";
   
   print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
<HEAD>
<TITLE>EDIT SELECTION GRID</TITLE>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon" />
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css" />
<link href="/JS/ergosign.styleform.css" media="all" type="text/css" rel="stylesheet">

    <script type="text/javascript" src="/JS/mootools-1.2-core.js"></script>

    <script type="text/javascript" src="/JS/ergosign.styleform.js"></script>

    <script type="text/javascript" src="/JS/script.js"></script>
    <link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />
</HEAD>
<BODY onload="styleForm()"> 
<CENTER>
<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/index.html" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/cr_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="/cgi-bin/cr_batchx.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="/cgi-bin/snf_batch1.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="/cgi-bin/graph.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Graph Utility</div>">
                                <img src="/images/graph_icon_inner.gif" onmouseover="src='/images/graph_icon_inner_hover.gif';"
                                    onmouseout="src='/images/graph_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                       <td style="width: 102px" align="Center">
		                                   <a href="/cgi-bin/PeerMain.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
		                                       <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end--><br><br>
<TABLE BORDER="1" CELLSPACING="0" CELLPADDING="8" WIDTH="400">
   <TR>
      <TD ALIGN="CENTER">
         <BR>
         <FONT SIZE="5" COLOR="GREEN">
         OPERATION SUCCESSFUL<BR>
         </FONT>
         <FONT SIZE="1">$now</FONT><BR>
         <BR>
      </TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" VALUE=" RETURN " onclick="history.go(-2)"></span>
</FORM>
</CENTER>
</BODY>
</HTML>
   }
} 

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#------------------------------------------------------------------------------

