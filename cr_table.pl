#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: cr_table.pl
#   author: Steve Spicer
#  Written: Wed Oct 24 20:14:32 CDT 2007
#
#  Research Data Assistance Center
#  http://www.resdac.umn.edu/Tools/TBs/TN-005-revised.asp
#----------------------------------------------------------------------------

use strict;
use CGI;
use DBI;
require "/var/www/cgi-bin/lib/req-costreports.pl";
do "common_header.pl";
print "Content-Type: text/html\n\n";
&headerScript();
do "pagefooter.pl";
use CGI::Session;
do "audit.pl";
my %status_lookup = (
 1 => 'As Submitted',
 2 => 'Settled w/o Audit',
 3 => 'Settled with Audit',
 4 => 'Reopened',
 5 => 'Amended',
);


&view("View","Cost report alternative view","Cost Report");
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $prov = $session->param("str");
my $rpt_no = $session->param("sstr");
my $facility = $cgi->param("facility");
my $provider = $cgi->param("provider");
my ($sql_f,$facility_id,$master_table,$report_table,$data_table,$row,$sth1);
if(!$userID)
{
	&sessionExpire;
}

my $heading = uc($facility) . " COST REPORT";
my $reportid  = $cgi->param('reportid');
my $worksheet = $cgi->param('worksheet');

my $start_time = time;

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error('CAN\'T READ milo.ini');
   exit(1);
}


my %a_labels;
my %row_labels;
my %column_labels;



my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my ($dbh, $sql, $sth);

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}
$sql_f = "select facility_id ,master_table,report_table,data_table from tbl_facility_type where facility_name = '$facility'";
$sth = $dbh->prepare($sql_f);
$sth->execute();

while ($row = $sth->fetchrow_hashref) {
	$facility_id = $$row{facility_id};
	$master_table = $$row{master_table};
	$report_table = $$row{report_table};
	$data_table = $$row{data_table};
}
&load_table_definitions($worksheet);
$sql = qq{
   SELECT year(FY_END_DT) as RPT_YEAR, PRVDR_NUM, RPT_STUS_CD,
          DATE_FORMAT(FY_BGN_DT, '%m-%d-%Y'),
          DATE_FORMAT(FY_END_DT, '%m-%d-%Y')
     FROM $report_table
    WHERE RPT_REC_NUM = ?
};

unless ($sth = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL:", $sth->errstr);
   $dbh->disconnect();
   exit(0);
}

unless ($sth->execute($reportid)) {
   &display_error("ERROR executing SQL:", $sth->errstr);
   $dbh->disconnect();
   exit(0);
}

my ($rpt_year, $rpt_provider, $rpt_stus_cd, $fy_bgn_dt, $fy_end_dt) = $sth->fetchrow_array;

my $rpt_status = $status_lookup{$rpt_stus_cd};

$sth->finish();

my ($name, $address, $city, $state, $zip);


$sql = qq{
  SELECT Name,address,
City ,State,Zip
 FROM $master_table
 WHERE ProviderNo in ($provider)
};

unless ($sth1 = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL query:", $sth1->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth1->execute()) {
   &display_error("ERROR executing SQL query:", $sth1->errstr);
   $dbh->disconnect;
   exit(1);
}

($name,$address,$city,$state,$zip) = $sth1->fetchrow_array;
&explore_worksheet($reportid, $rpt_year, $worksheet);

$sth1->finish;
$dbh->disconnect();

exit(0);

#----------------------------------------------------------------------------

sub explore_worksheet
{
   my ($reportid, $year, $worksheet) = @_;

   my ($address, $row, $col, $value, $class, $align, $editlink);

   if ($ini_value{'USE_A_TABLE'} eq 'Y') {
      &load_a_table($reportid, $year);
   }

   my ($sth, $sql);

   $sql = qq{
       SELECT CR_LINE, CR_COLUMN, CR_VALUE
       FROM $data_table
       WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       ORDER BY CR_LINE, CR_COLUMN
      };

   my (%line_mappings, %line_mappings_r);
   &map_lines($data_table, $reportid, $worksheet, $dbh, \%line_mappings, \%line_mappings_r);

   unless ($sth = $dbh->prepare($sql)) {
      &display_error("ERROR preparing SQL:", $sth->errstr);
      exit(0);
   }

   unless ($sth->execute($reportid, $worksheet)) {
      &display_error("ERROR executing SQL:", $sth->errstr);
      exit(0);
   }

   unless ($sth->rows()) {
      &display_error("NOTHING AVAILABLE FOR REPORT ID: $sql <BR>$reportid");
      return(0);
   }

   my $key;
   my %col_hash;
   my %value_hash;

   &modify_line_numbers($worksheet, $sth, \%line_mappings, &get_worksheet_inserts(), \%value_hash, \%col_hash);
   #foreach my $k (sort {$a <=> $b} keys %value_hash) {
   #	   print "$k|$value_hash{$k}<br>\n";
   #}



   print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
<HEAD><TITLE>WORKSHEET B-1 EXPLORER</TITLE>
<link href="/css/cost_report/ergosign.styleform.css" media="all" type="text/css" rel="stylesheet">
  <!--  <script type="text/javascript" src="/JS/mootools-1.2-core.js"></script> -->
   <!-- <script type="text/javascript" src="/JS/cost_report/ergosign.styleform.js"></script> -->
   <!-- <script type="text/javascript" src="/JS/script.js"></script> -->
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
    <!--content start-->

        <table cellpadding="0" cellspacing="0" width="885px">
            <tr>
                <td valign="top">
                    <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px">
			<table bgColor=\"#B4CDCD\" width="885px" align=\"centre\">
<tr>
	<td>
		<font size="2" color="#2e7738"> <b>$heading</b><br>

	</td>
	</tr>
	</table>

<TABLE BORDER="0" CELLPADDING="2" width="100%">
   <TR>
     <TD ALIGN="LEFT" VALIGN="TOP">
       <Div class="pageheader">

		$name</div>
        <span class="tahoma12"><b>$address<BR>
        $city, $state  $zip
        </B></span>

     </TD>
     <TD ALIGN="left" VALIGN="TOP" class="tahoma12" style="border-left:dotted 1px #ffffff; padding-left:10px;">
       WORKSHEET:<BR>
        PROVIDER:<BR>
        FY BEGIN:<BR>
          FY END:<BR>
          STATUS:<BR>
     </TD>
     <TD ALIGN="LEFT" VALIGN="TOP" class="tahoma12" style="border-right:dotted 1px #ffffff; padding-left:10px;">
        <B>
        <FONT COLOR="#EE0000">
        $worksheet (<a href="cr_explore_xls.pl?facility=$facility&reportid=$reportid&provider=$provider&cr_worksheet=$worksheet">Download <IMG BORDER="0" SRC="/icons/excel_icon.gif"></a>)<BR>
        $rpt_provider<BR>
        $fy_bgn_dt<BR>
        $fy_end_dt<BR>
        $rpt_status<BR>
        </FONT>
        </B>
     </TD>
     <TD ALIGN="RIGHT" VALIGN="TOP">
        <IMG SRC="/icons/CMSLogo.gif">
     </TD>
   </TR>
   </TABLE>
};
   &CR_dropdown;
print qq{
   <TABLE  BORDER="0" CELLPADDING="6" CELLSPACING="0" width="100%" class="gridstyle" style="clear:both;">
   <TR CLASS="gridheader">
       <TD ALIGN=\"CENTER\"><B>LINE</B></TD>
       <TD ALIGN=\"CENTER\"><B>TITLE</B></TD>
};

   my ($s, $alt, $key, $title);

   my %col_index;

   my $column = 0;

   foreach $key (sort keys %col_hash) {
        $s = $column_labels{$key};

        if ($ini_value{'PLUG_COLUMN_HEADINGS'} eq 'Y') {
           $alt = $a_labels{"0$key"};
           if (defined($alt)) {
              $s = "<FONT COLOR=\"RED\">$alt</FONT>";
           }
        }

       print "      <TD ALIGN=\"CENTER\"><B>$s</B><BR><FONT COLOR=\"GREEN\">($key)</FONT></TD>\n";

       $col_index{$key} = $column;
       ++$column;
   }

   my $table_columns = $column;

   print "</TR>\n";

   my $save_row = 'void';
   my $count    = 0;
   my $n;
   my @element;

   foreach $key (sort keys %value_hash) {
      ($row,$col) = split /:/,$key;
      $value = $value_hash{$key};

      unless ($row eq $save_row) {
         unless ($save_row eq 'void') {
             &render_row($save_row, ++$count, $table_columns, \%line_mappings_r, @element);
             @element = ();
         }
         $save_row = $row;
      }

      $n = $col_index{$col};
      $element[$n] = $value;
   }

   &render_row($save_row, ++$count, $table_columns, \%line_mappings_r, @element);
   @element = ();

   print qq{
</TABLE>
<FONT SIZE="1">
$count ROWS<BR>
</FONT>
</td>
            <td valign="top">
                <img src="/images/content_right.gif" /></td>
        </tr>
    </table>
    <!--content end-->
	};
&TDdoc;
print qq{
</CENTER>
</BODY>
</HTML>
   };

   return(0);
}



#----------------------------------------------------------------------------

sub render_row
{

   my ($row, $count, $table_columns, $line_mappings_r, @elements) = @_;

   my ($i, $align, $title, $value);

   my $alt = $a_labels{$row};

   #if ( (keys %{$line_mappings_r}) && exists $$line_mappings_r{$row}) {
	#	$row = $$line_mappings_r{$row};
   #}

#  if (defined($alt)) {
#     $title = "(A) $alt";
#  } else {
#     $title = $row_labels{$row};
#  }

   $title = $row_labels{$row};

   unless ($title) {
      if (defined($alt)) {
         $title = "<FONT COLOR=\"RED\">$alt</FONT>";
      }
   }

   my $class = ($count % 2) ? 'gridrow' : 'gridalternate';

   print "   <TR CLASS=\"$class\">\n";
   print "       <TD ALIGN=\"CENTER\"><FONT COLOR=\"GREEN\">($row)</FONT></TD>\n";
   print "       <TD ALIGN=\"CENTER\" NOWRAP=\"1\"><B>$title</B></TD>\n";

   for ($i = 0; $i < $table_columns; ++$i) {
       if (defined($elements[$i])) {
          $value = $elements[$i];
       } else {
          $value = '&nbsp';
       }

       $align = 'LEFT';

       if ($value =~ m/^-?\d+$|-?\d*\.\d+$/) {
          $align = 'RIGHT';
          $value = &comify($value);
       }

       print "       <TD ALIGN=\"$align\"><DIV TITLE=\"$worksheet:$row\">$value</DIV></TD>\n";
   }
   print "   </TR>\n";
}


#----------------------------------------------------------------------------


sub load_a_table
{
   my ($reportid, $year) = @_;

   my ($row, $col, $value);
# AND RPT_YEAR		= $year
   my ($sth, $sql);

   $sql = qq{
       SELECT CR_LINE, CR_COLUMN, CR_VALUE
         FROM $data_table
        WHERE CR_REC_NUM    = ?
          AND CR_COLUMN     = '0000'
          AND CR_WORKSHEET  = 'A000000'

     ORDER BY CR_LINE, CR_COLUMN
      };


   unless ($sth = $dbh->prepare($sql)) {
      &display_error("ERROR preparing SQL:", $sth->errstr);
      exit(0);
   }

   unless ($sth->execute($reportid)) {
      &display_error("ERROR executing SQL:", $sth->errstr);
      exit(0);
   }

   my $key;

   while (($row, $col, $value) = $sth->fetchrow_array) {
      if ($value =~ m/^(\d\d\d\d\d)(.+$)/) {
         $value = "$1 $2";
      }
      $a_labels{$row} = $value;
   }

   $sth->finish();
}


#----------------------------------------------------------------------------

sub get_value_by_addr
{
   my ($year, $reportid, $address) = @_;

   my ($worksheet, $row, $col) = split /[-:\|]/,$address;

   my ($sth, $sql);
# AND RPT_YEAR		= $year
   $sql = qq{
    SELECT CR_VALUE
      FROM $data_table BB
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?

   };

   unless ($sth = $dbh->prepare($sql)) {
       &display_error("Error preparing sql query", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   $sth->execute($reportid, $worksheet, $row, $col);

   my ($value) = $sth->fetchrow_array;

   return($value);
}


#----------------------------------------------------------------------------

sub display_error
{
   my @list = @_;

   my ($string, $s);

   foreach $s (@list) {
      $string .= "$s<BR>\n";
   }

   print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
<HEAD><TITLE>ERROR</TITLE>
<link href="/JS/ergosign.styleform.css" media="all" type="text/css" rel="stylesheet">

    <script type="text/javascript" src="/JS/mootools-1.2-core.js"></script>

    <script type="text/javascript" src="/JS/ergosign.styleform.js"></script>

    <script type="text/javascript" src="/JS/script.js"></script>
</HEAD>
<BODY>
<CENTER>
<BR>
};

&innerHeader();
print qq{
    <!--content start-->
    <table cellpadding="0" cellspacing="0" width="885px">
            <tr><td align="left">Welcome <b>
                	};

                	print $firstName . " " . $lastName;
                	print qq{
                	</b></td>
                	<td align="right">
                	<a href="/cgi-bin/login.pl?saction=signOut">Sign Out</a>
                	</td>
            	</tr>
    </table>
        <table cellpadding="0" cellspacing="0" width="885px">
            <tr>
                <td valign="top">
                    <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px">
<FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<TABLE CLASS="error" WIDTH="600">
   <TR>
      <TD ALIGN="CENTER">
          <FONT SIZE="4">
          <BR>
          $string
          <BR>
          </FONT>
      </TD>};

	     &CR_dropdown;
	  print qq{

   </TR>
</TABLE>

<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM></td>
            <td valign="top">
                <img src="/images/content_right.gif" /></td>
        </tr>
    </table>
    <!--content end-->
</CENTER>
</BODY>
</HTML>
   };

   return(0);
}

#----------------------------------------------------------------------------
sub CR_dropdown
{
#print "Content-Type: text/html\n\n";
my $rpt= substr $prov,1;
my @rptprov = split(/,/, $rpt);
my $rpt_list =	substr $rpt_no,1;
my @rpt_id = split(/,/, $rpt_list);
print qq{

<script>
function opt_select(){
var opt=document.frmcrexplorer.CR.options[document.frmcrexplorer.CR.selectedIndex].value;
window.parent.location.href="/cgi-bin/cr_explore.pl?facility=$facility&reportid="+opt+"&action=go&provider=$provider";
}
</script>


<form name="frmcrexplorer">
<table align="left">
<tr>
<td>
<strong>Cost Reports </strong>
	<select name="CR" title="CHOOSE A COST REPORT" onchange="opt_select()">
	<option >Select CR end date</option>
	};

		for(my $i=0;$i< scalar(@rptprov);$i++){
			print "<option name=\"Report\" value=".@rpt_id[$i].">".@rptprov[$i]."</option>";
		}



	print qq{
	</select>
	</td>
	</tr>
	</table>
	</form>
	};
}

#----------------------------------------------------------------------------
sub comify
{
  local($_) = shift;
  1 while s/^(-?\d+)(\d{3})/$1,$2/;
  return($_);
}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#------------------------------------------------------------------------------


sub load_table_definitions
{
   my ($worksheet) = @_;

 my ($aaa, $bbb,$sth1,$c_sql,$r_sql,$rc_sql,$def_id);

   $rc_sql= qq{
	select definition_id from tbl_cost_report_definition
where cr_worksheet = '$worksheet' and facility_type = $facility_id

   };

   unless ($sth = $dbh->prepare($rc_sql)) {
       &display_error("Error preparing sql query", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   $sth->execute();

  ($def_id) = $sth->fetchrow_array;


   $r_sql = qq{
    SELECT label_value,title
      FROM tbl_cost_report_definition_detail
	 where label_type = 'row'
	 and definition_id = $def_id
   };


   unless ($sth = $dbh->prepare($r_sql)) {
       &display_error("Error preparing sql query", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   $sth->execute();

  while(($aaa,$bbb) = $sth->fetchrow_array){

			if ($bbb) {
               $row_labels{$aaa} = $bbb;
            }
   }

	$c_sql = qq{
    SELECT label_value,title
      FROM tbl_cost_report_definition_detail
	 where label_type = 'column'
	and definition_id = $def_id
   };

    unless ($sth = $dbh->prepare($c_sql)) {
       &display_error("Error preparing sql query", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   $sth->execute();

 while(($aaa,$bbb) = $sth->fetchrow_array){
			if ($bbb) {
               $column_labels{$aaa} = $bbb;
            }
   }


}

#--------------------------------------------------------------------------------------
sub sessionExpire
{

print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

