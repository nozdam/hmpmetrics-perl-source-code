#!/usr/bin/perl

#--------------------------------------------------------------------
# Program: dischg.pl
#  Author: Steve Spicer
# Written: Sun Sep 28 17:15:42 CDT 2008
# Revised: 
#   Notes:
# Depends: DBI.pm, CGI.pm
#--------------------------------------------------------------------

use strict;
use DBI;
use CGI;
use CGI::Session;
do "common_header.pl";
  print "Content-Type: text/html\n\n";
&headerScript();
do "pagefooter.pl";
my %short_lookup = (
  '01' => 'SELF CARE',
  '02' => 'ACUTE CARE',
  '03' => 'SNF',
  '04' => 'ICF',
  '05' => 'Discharged/transferred to another type of institution for inpatient care (including distinct parts)',
  '06' => 'HOME HEALTH',
  '07' => 'LEFT',
  '08' => 'HOME HEALTH IV',
  '09' => 'Admitted as an inpatient to this hospital.  In situations where a patient is admitted before midnight of the third day following the day of an outpatient service, the outpatient services are considered inpatient',
  '20' => 'Expired (did not recover - Christian Science patient)',
  '30' => 'Still patient',
  '40' => 'Expired at home (hospice claims only)',
  '41' => 'Expired in a medical facility such as hospital, SNF, ICF, or freestanding hospice. (Hospice claims only)',
  '42' => 'Expired - place unknown (Hospice claims only)',
  '43' => 'Discharged/transferred to a federal hospital',
  '50' => 'HOSPICE - HOME',
  '51' => 'HOSPICE - MEDICAL FACILITY',
  '61' => 'Discharged/transferred within this institution to a hospital-based Medicare approved swing bed',
  '62' => 'Discharged/transferred to an inpatient rehabilitation facility including distinct parts units of a hospital',
  '63' => 'Discharged/transferred to a long term care hospitals',
  '64' => 'Discharged/transferred to a nursing facility certified under Medicaid but not under Medicare',
  '65' => 'PSYCH',
  '66' => 'CAH',
  '71' => 'Discharged/transferred/referred to another institution for outpatient services as specified by the discharge plan of care',
  '72' => 'Discharged/transferred/referred to this institution for outpatient services as specified by the discharge plan of care',
);
                                                          

my %lookup = (
  '01' => 'Discharged to home/self care',
  '02' => 'Discharged/transferred to other short term general hospital for inpatient care',
  '03' => 'Discharged/transferred to skilled nursing facility (SNF) with Medicare certification in anticipation of covered skilled care',
  '04' => 'Discharged/transferred to intermediate care facility (ICF)',
  '05' => 'Discharged/transferred to another type of institution for inpatient care (including distinct parts)',
  '06' => 'Discharged/transferred to home care of organized home health service organization',
  '07' => 'Left against medical advice or discontinued care',
  '08' => 'Discharged/transferred to home under care of a home IV drug therapy provider',
  '09' => 'Admitted as an inpatient to this hospital.  In situations where a patient is admitted before midnight of the third day following the day of an outpatient service, the outpatient services are considered inpatient',
  '20' => 'Expired (did not recover - Christian Science patient)',
  '30' => 'Still patient',
  '40' => 'Expired at home (hospice claims only)',
  '41' => 'Expired in a medical facility such as hospital, SNF, ICF, or freestanding hospice. (Hospice claims only)',
  '42' => 'Expired - place unknown (Hospice claims only)',
  '43' => 'Discharged/transferred to a federal hospital',
  '50' => 'Hospice - home',
  '51' => 'Hospice - medical facility',
  '61' => 'Discharged/transferred within this institution to a hospital-based Medicare approved swing bed',
  '62' => 'Discharged/transferred to an inpatient rehabilitation facility including distinct parts units of a hospital',
  '63' => 'Discharged/transferred to a long term care hospitals',
  '64' => 'Discharged/transferred to a nursing facility certified under Medicaid but not under Medicare',
  '65' => 'Discharged/Transferred to a psychiatric hospital or psychiatric distinct unit of a hospital',
  '66' => 'Discharged/transferred to a Critical Access Hospital (CAH)',
  '71' => 'Discharged/transferred/referred to another institution for outpatient services as specified by the discharge plan of care',
  '72' => 'Discharged/transferred/referred to this institution for outpatient services as specified by the discharge plan of care',
);
                                                          

my $cgi = new CGI;

my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $number=$cgi->param('provider');
my $dataYear=$cgi->param('year');


if(!$userID)
{
	&sessionExpire;
}


my $provider   = $cgi->param('provider');
my $drg        = $cgi->param('drg');

my $start_time = time;

my %ini_value;

unless (&read_inifile("milo.ini")) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{"DB_TYPE"};
my $dblogin    = $ini_value{"DB_LOGIN"};
my $dbpasswd   = $ini_value{"DB_PASSWORD"};
my $dbname     = $ini_value{"DB_NAME"};
my $dbserver   = $ini_value{"DB_SERVER"};

my ($sql, $dbh, $sth,$sql_f,$facility_id,$master_table,$report_table,$data_table,$row);
my $facility = $cgi->param("facility");


unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}
$sql_f = "select facility_id ,master_table,report_table,data_table from tbl_facility_type where facility_name = '$facility'";
$sth = $dbh->prepare($sql_f);
$sth->execute();

while ($row = $sth->fetchrow_hashref) {
	$facility_id = $$row{facility_id};
	$master_table = $$row{master_table};
	$report_table = $$row{report_table};
	$data_table = $$row{data_table};
}
#----------------------------------------------------------------------------

$sql = qq{
	SELECT th.Name as HOSP_NAME,th.address as STREET_ADDR,
th.City as CITY,th.State as STATE,
th.Zip as ZIP_CODE,th.County as COUNTY 
     FROM $master_table as th
    WHERE th.ProviderNo = ?
};

unless ($sth = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute($provider)) {
   &display_error("ERROR executing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

my ($name, $address, $city, $state, $zip, $county) = $sth->fetchrow_array;

$zip =~ s/-$//;  # crappy data

$sth->finish();

#----------------------------------------------------------------------------

$sql = qq{
   SELECT DRG_DESC
     FROM DRG_TABLE
   WHERE DRG_CODE = ?
};

unless ($sth = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL query:",$sth->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute($drg)) {
   &display_error("ERROR executing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

my ($drg_desc) = $sth->fetchrow_array;

$sth->finish();

#----------------------------------------------------------------------------

$sql = qq{
   SELECT DRG_CD, DRG_DESC, STUS_CD, COUNT(*) as cnt 
     FROM LDSM_$dataYear
LEFT JOIN DRG_TABLE ON DRG_CD = DRG_CODE
    WHERE PROVIDER  = ? 
      AND DRG_CD    = ?
      AND SGMT_NUM  = 1 
 GROUP BY STUS_CD
 ORDER BY CNT DESC
};

unless ($sth = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL query:",$sth->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute($provider, $drg)) {
   &display_error("ERROR executing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

my $now = localtime();

my $rows = $sth->rows;



print qq{
<HTML>
<HEAD><TITLE>DRG ANALYSIS BY PROVIDER</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();

print qq{
<!--content start-->

             <table cellpadding="0" cellspacing="0" width="885px">
            <tr>
                <td valign="top">
                    <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px"  valign="top">
<Table cellpadding="0" cellspacing="0" width="100%">
	<Tr>
		<Td style="padding-bottom:10px">
				<tr><td><span class="pageheader">DRG ANALYSIS FOR PROVIDER $provider</span> &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp&nbsp &nbsp<b> Year: </b><font color='#EE0000'>$dataYear</font></td></tr>
				<tr><td><B>$name</b></td></tr><tr><td>$address</td></tr><tr><td><b>$city, $state, $zip</B></td></tr>
				<tr><td><span class="tahoma12">
<B>DRG $drg $drg_desc</B></span></td></tr>
				

		</td>	
	</tr>
	<tr align="left">
	<td>
	</br>
	<script>
	function submitYear(value)
	{
		var opt=document.yearSel.year.options[document.yearSel.year.selectedIndex].value;
		window.parent.location.href="/cgi-bin/drg_mix.pl?provider=$number&facility=$facility&year="+opt+"";
		//document.selection.submit();
	}
	</script>
	<form name="yearSel" >
	DRG MIX : <select name="year" onChange="submitYear(this.value)">
	<option value=''> Select Year </option>
	<option value=2006> 2006</option>
	<option value=2007> 2007</option>
	<option value=2008> 2008</option>
	<option value=2009> 2009</option>
</select>
</form>
</td>
<td align="right" valign="bottom" padding-bottom:10px" class="tahoma12">
$now
</td>

</tr>
</table>
<TABLE CELLPADDING="4" WIDTH="100%" class="gridstyle">
  <TR class="gridHeader">
    <TH>CODE</TH>
    <TH>DESCRIPTION</TH>
    <TH>DISCHARGES</TH>
    <TH>PERCENT</TH>
  </TR>
};
my @buffer;

my $count = 0;

my ($pct, $class, $i, $desc, $status, $edit_count, $edit_pct, $cnt);
my (@label_list, @value_list);

my $total_count = 0;

while (($drg, $desc, $status, $cnt) = $sth->fetchrow_array) {
   push @buffer, [$drg, $desc, $status, $cnt];
   $total_count += $cnt;
}

my $limit = @buffer;

for ($i=0;$i<$limit;++$i) {

   ($drg, $desc, $status, $cnt) = @{$buffer[$i]};

   $edit_count = &comify($cnt);

   $edit_pct = sprintf ("%.02f", $cnt / $total_count * 100);

   $class = (++$count % 2) ? "gridrow" : "gridalternate";

   $desc = $lookup{$status};

   push @value_list, $edit_pct;
   push @label_list, $status;

   print "   <TR CLASS=\"$class\">\n";
   print "      <TD ALIGN=\"center\">$status</TD>\n";
   print "      <TD ALIGN=\"left\">$desc</TD>\n";
   print "      <TD ALIGN=\"right\">$edit_count</TD>\n";
   print "      <TD ALIGN=\"right\">$edit_pct &#37;</TD>\n";
   print "   </TR>\n";
}

$edit_count = &comify($total_count);

$class = (++$count % 2) ? "gridrow" : "gridalternate";

print "   <TR CLASS=\"$class\">\n";
print "      <TD ALIGN=\"right\" COLSPAN=\"2\"><B>TOTAL:</B></TD>\n";
print "      <TD ALIGN=\"right\"><B>$edit_count</B></TD>\n";
print "      <TD>&nbsp;</TD>\n";
print "   </TR>\n";
my $elapsed_time = time - $start_time;


my $url  = "/cgi-bin/piechart.pl?facility=$facility&title=DISCHARGE+SUMMARY+DRG:+$drg";
   $url .= "&labels=" . join(',', @label_list);
   $url .= "&values=" . join(',', @value_list);


print qq{
</TABLE>
$count ROWS IN $elapsed_time SECONDS
<BR>
<table align="center"><tr><td>
<IMG SRC=\"$url\">
</td></tr></table>
</td>
            <td valign="top">
                <img src="/images/content_right.gif" /></td>
        </tr>
    </table>
    <!--content end-->
};
&TDdoc;
print qq{
</CENTER>
</BODY>
</HTML>
};

$sth->finish;
$dbh->disconnect;
   
exit(0);


#------------------------------------------------------------------------------

sub display_error
{
   my $s;



   print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE></HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<FONT SIZE="5" COLOR="RED">
<B>ERRORS</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" BORDER="1" CELLPADDING="20" WIDTH="600">
<TR><TD>
<UL>
};
   foreach $s (@_) {
      print "$s<BR>";
   }
   print qq{
</TD></TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</FONT>
</CENTER>
</BODY>
</HTML>
};
}

#----------------------------------------------------------------------------
sub comify
{
   local($_) = shift;
   1 while s/^(-?\d+)(\d{3})/$1,$2/;
   return($_);
}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#--------------------------------------------------------------------------------------
sub sessionExpire
{
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}


#------------------------------------------------------------------------------




