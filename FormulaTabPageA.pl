#!/usr/bin/perl

use CGI;
use DBI;
do "common_header.pl";
print "Content-Type: text/html\n\n";
&headerScript();
my $cgi = new CGI;

use CGI::Session;

my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");

# Database

my $dbh;
my ($sql, $sth,$formula_optionsB,$s);

# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my $defaultYear= $ini_value{'DEFAULT_YEAR'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

my $sel_for = $cgi->param("paramA");
my $element_name = $cgi->param("paramB");
#$sql = "select ID as formula_id,indicator,BattleshipCoordinates from Formulas order by indicator ";
#$sth = $dbh->prepare($sql);
#$sth->execute();

#my ($formula_id, $indicator,$BattleshipCoordinates);

#my $formula_optionsA;#  = "<OPTION VALUE='' SELECTED>-- Select --</OPTION>";

#while (($formula_id, $indicator,$BattleshipCoordinates) = $sth->fetchrow_array) {
 #     $formula_optionsA = $formula_optionsA."<OPTION VALUE='$BattleshipCoordinates'>$indicator</OPTION>";
#}
#$sth->finish();

my ($sql1,$sth1,$sql_f,$facility_id);
my $facility = $cgi->param("facility");
$sql_f = "select facility_id from tbl_facility_type where facility_name = '$facility'";
$sth1 = $dbh->prepare($sql_f);
$sth1->execute();

while ($row = $sth1->fetchrow_hashref) {
	$facility_id = $$row{facility_id};
}
$sql1 = "select element,isFormulae,formulae from standard_metrics where Formulae !='' and facility_type = $facility_id order by element ";
$sth1 = $dbh->prepare($sql1);
$sth1->execute();


my ($element, $formulae,$isFor);

my $formula_optionsA ="<OPTION VALUE='' SELECTED>-- Select --</OPTION>";

while (($element,$isFor,$formulae) = $sth1->fetchrow_array) {
	if($isFor == 0){
      $formula_optionsA = $formula_optionsA."<OPTION VALUE='$formulae'>$element (MI)</OPTION>";
	}else{
	  $formula_optionsA = $formula_optionsA."<OPTION VALUE='$formulae'>$element (F) </OPTION>";
}
}
$sth1->finish();




print qq{
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Formulas - Select Mode</title>
    <link href="/css/tabs.css" rel="stylesheet" type="text/css" />
<link href="/css/admin_panel/user_management/userManagement.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="/JS/admin_panel/standard_report/FormulaTabPageA.js" ></script>
</head>
<body onload="selectBox()">
<form name="frmfromulaTabA" >
<input type="hidden" name="sel_for" value ="$sel_for">
<input type="hidden" name="optionsA" value ="$formula_optionsA">
<input type="hidden" name="optionsB" value ="$formula_optionsB">
<input type="hidden" name="element_name" value ="$element_name">
<input type="hidden" name="optionsS" value ="">
<ol id="toc">
    <li class="current"><a href="/cgi-bin/FormulaTabPageA.pl?facility=$facility"><span>Formula</span></a></li>
    <li><a href="/cgi-bin/FormulaTabPageB.pl?facility=$facility"><span>Create Indicator</span></a></li>
    <li><a href="/cgi-bin/FormulaTabPageC.pl?facility=$facility"><span>Edit Indicator</span></a></li>
</ol>
<div class="content">
    <table align="center" class="gridstyle" width="20%" height="20%" cellpadding="0" cellspacing="0">
    		<tr>
    			<td align="center" class="gridHlink">Select Master Indicator Variables / Existing Formula </td>

    		</tr>
    		<tr><td >&nbsp;</td></tr>
    		<tr>
    			<td >
    				<div id="selfrm"></div>
    			</td>
    		</tr>
    		<tr>
		    	<td style="width:20px;height:14px;">
		    		<textarea name="txtExpression" style="height:162px;width:390px;"></textarea>
		    	</td>
    		</tr>
    		<tr>
    			<td align="center">
		    	<table align="center" width="50%" cellpadding="0" cellspacing="0">
		    	<tr>
		    	<td align="center"><span  class=\"button\"><input type="button" style="width:25px;" name="btnOpenBrace" value =" ( " onclick="AddOperator(this)"></span></td>
		    	<td align="center"><span  class=\"button\"><input type="button" style="width:25px;" name="btnCloseBrace" value =" ) " onclick="AddOperator(this)"></span></td>
		    	<td align="center"><span  class=\"button\"><input style="width:25px;" type="button" name="btnModule" value =" % " onclick="AddOperator(this)"></span></td>
		    	<td align="center"><span  class=\"button\"><input style="width:25px;" type="button" name="btnAdd" value =" + " onclick="AddOperator(this)"></span></td>
		    	<td align="center"><span  class=\"button\"><input style="width:25px;" type="button" name="btnMulti" value =" * " onclick="AddOperator(this)"></span></td>
		    	<td align="center"><span  class=\"button\"><input style="width:25px;" type="button" name="btnDivid" value =" / " onclick="AddOperator(this)"></span></td>
		    	<td align="center"><span  class=\"button\"><input style="width:25px;" type="button" name="btnMinus" value =" - " onclick="AddOperator(this)"></span></td>
		    	<td align="center"><span  class=\"button\"><input style="width:50px;" type="button" name="btnClear" value =" Clear " onclick="ClearIt()"></span></td>
		      	<td align="center"><span  class=\"button\"><input type="button" style="width:50px;" name="btnLoad" value =" Load " onclick="LoadFormula()"></span></td>
		  	</tr>
		    	</table>
		    	</td>
    		</tr>
    </table>
</div>
</form>
</body>
</html>
};


#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#--------------------------------------------------------------------------------------

sub sessionExpire
{


print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}

print qq{
<HTML>
<HEAD><TITLE>Standard Reports-ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}