#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: userModify.pl
#  author: Jagadish.M
#----------------------------------------------------------------------------

use strict;
use CGI;
use DBI;

# use Session;
my $cgi = new CGI;
do "pagefooter.pl";
do "common_header.pl";
print "Content-Type: text/html\n\n";
use CGI::Session;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");

my $dbh;
my ($sql, $sth, $key);

# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my $defaultYear= $ini_value{'DEFAULT_YEAR'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

my %roleDetails = &getRoleDetails();
my %usersDetails = &getUsersDetails();


# User ID selected from combo box to load details
my $uid = $cgi->param('uid'); 
my $userName ="";
my $userType = 0;

if($uid){
	$sql = "select users.UserName,users.RoleID from users where users.UserID = $uid";
	$sth = $dbh->prepare($sql);			
	$sth->execute();
	
	while(my $row = $sth->fetchrow_hashref){
		$userName = $$row{UserName};
		$userType = $$row{RoleID};
	}
}

print qq{
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<title>PROJECT MILO User Management</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="/css/register/main.css" rel="stylesheet" type="text/css">
<link href="/css/register/qm.css" rel="stylesheet" type="text/css">
<link href="/css/tabs.css" rel="stylesheet" type="text/css" />   
<link href="/css/admin_panel/user_management/userManagement.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript" src="/JS/admin_panel/user_management/userModify.js" ></script>
</head>
<body leftmargin="0" topmargin="0" marginheight="20" marginwidth="0">
<CENTER>
};
&innerHeader();
print qq{	
    <!--content start-->  
    <table cellpadding="0" cellspacing="0" width="885px">
        <tr>            
            <td valign="top" > 	
		<ol id="toc">
		    <li><a href="/cgi-bin/userView.pl"><span>View</span></a></li>
		    <li><a href="/cgi-bin/userCreate.pl"><span>Create User</span></a></li>
		    <li class="current"><a href="/cgi-bin/userModify.pl"><span>Modify User</span></a></li>	
		    <li><a href="/cgi-bin/userAudit.pl"><span>Audit Log</span></a></li>
		</ol>
		<div class="content" style="border:0px;padding:0em;">
		<table align="center" class="ColrBk01" width="100%" border="0" cellpadding="4" cellspacing="0">
		  <tbody><tr>
			<td class="Fnt11Pix" background="/images/register/grad_01_hover.gif"><b class="Colr03">Modify existing user</b></td>
			<td class="Fnt11Pix" background="/images/register/grad_01_hover.gif" align="right"><b class="Colr03"></b></td>
		  </tr>
		</tbody></table>
		<table class="ColrBk02" width="100%" border="0" cellpadding="2" cellspacing="0">
		  <tbody><tr>
			<td><form action="/cgi-bin/userActions.pl" method="post" name="frmSingerSLRegistry" id="frmSingerSLRegistry" onsubmit="return funcValidateForm();">
				<table width="100%"  bgcolor="#ffffff" border="0" cellpadding="4" cellspacing="0">
				  <tbody><tr>
					<td colspan="2" class="ColrBk03"><b>&nbsp;<font color="#0066cc">Login Details...</font></b></td>
					<td class="ColrBk03">&nbsp;</td>
					<td class="ColrBk03">&nbsp;</td>
				  </tr>
				  <tr>
				  	<td>&nbsp;</td>
				  	<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Select User</b></font></td>
				  	<td><font color="#333399"><b>:</b></font></td>
				  	<td><select onchange="getDetails()" name="txtUser" class="TextBox01" >
				  		<option value="" SELECTED>--Select--</option>
				  		};	               	
				  			foreach $key(sort {$usersDetails{$a} cmp $usersDetails{$b} } keys %usersDetails){
				  			    if($uid == $key){
				  			    	print "<OPTION VALUE=\"$key\" selected>$usersDetails{$key}</OPTION> \n";            	
				  			    }else{
				  			    	print "<OPTION VALUE=\"$key\">$usersDetails{$key}</OPTION> \n"; 
				  			    }	
				  			}
				          print qq{
				  	  </select> <font color="#ff0000"></font>					  
				  	</td>
				  </tr>
				  <tr>
					<td width="10%">&nbsp;</td>
					<td width="22%"><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Email ID</b></font></td>
					<td width="4%"><font color="#333399"><b>:</b></font></td>
					<td><input name="txtUserName" class="TextBox01" id="txtUserName" size="50" maxlength="200" type="text" onchange="toggle_username('txtUserName')" value="$userName"> <font color="#ff0000"> * (Will be used as Login Name)</font>
					
					<div id="username_exists" style="font-size: 11px;font-weight: bold;color:#FF3300"> </div>					
					</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><p><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Password</b></font></p></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtPassword" class="TextBox01" id="txtPassword" size="25" maxlength="20" type="password" onKeyUp="updatestrength(this.value)"> <font color="#ff0000">*</font> &nbsp;<font color="#333399">Password Strength </font>&nbsp; <img  id="strength" src="/images/password/tooshort.jpg" name="img" alt="" style="visibility:hidden;" />
					</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><p><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Confirm Password</b></font></p></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtPasswordConfirm" class="TextBox01" id="txtPasswordConfirm" size="25" maxlength="20" type="password"> <font color="#ff0000">*</font> </td>
				  </tr>	
				  <tr>
				    	<td>&nbsp;</td>
				    	<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> User Type</b></font></td>
				    	<td><font color="#333399"><b>:</b></font></td>
				    	<td><select name="txtUserType" class="TextBox01" id="txtUserType">
				  		<option value="0" SELECTED>--Select One--</option>
				  };	               	
				  		foreach $key(sort {$roleDetails{$a} cmp $roleDetails{$b} } keys %roleDetails){   
				  			if($userType ==$key){
				  			    print "<OPTION VALUE=\"$key\" selected>$roleDetails{$key}</OPTION> \n";            	
				  			}else{
				  			    print "<OPTION VALUE=\"$key\">$roleDetails{$key}</OPTION> \n";
				  			}
				  		}
				  print qq{					  
				  	  </select> <font color="#ff0000">*</font> </td>
				  </tr>
				  <tr>
					<td colspan="4"><table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tbody>
						<tr>
						  <td width="25"><img src="/images/register/pix_trans.gif" width="120" height="5"></td>
						  <td width="25"></td>
						  <td></td>
						</tr>
					  </tbody></table></td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td><input type="hidden" name="saction" value="update"><input name="cmdNext" class="TextBox01" id="cmdNext" value="Update" type="submit"> <input name="cmdReset" class="TextBox01" id="cmdReset" value="Clear" type="reset"></td>
				  </tr>
				</tbody></table>
			  </form>
	             </td>
		  </tr>
		</tbody>
	      </table>
	      </div>
          </td>
       </tr>
  </tbody>
 </table>
 
 </td>
  </tr>
  </table>
  <!--content end-->
  };
  
  &TDdoc;
  print qq{
  </CENTER>
  </body>  
  </html>
};

#--------------------------------------------------------------------------------------
sub sessionExpire
{

print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};
&headerScript();
print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------
# - Get details from Role table. 

sub getRoleDetails
{
	my ($qry, $stmt, $record);
	
	$qry = "select * from role ";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();   	
   	
   	my %role;   	
   	
	while ($record = $stmt->fetchrow_hashref) {	
		$role{$$record{RoleID}} = $$record{RoleName};		
  	}  	
  	return %role;
}

sub getUsersDetails
{

	my ($qry,$stmt,$record);
	
	$qry = "select * from userdetails order by First_Name";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();
	
	my %userdetail;
	
	while($record = $stmt->fetchrow_hashref){
		$userdetail{$$record{UsersID}} = $$record{First_Name};
	}
	
	return %userdetail;
}
#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach my $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}

#------------------------------------------------------------------------------