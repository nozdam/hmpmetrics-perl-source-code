#!/usr/bin/perl

use CGI;
use DBI;

my $cgi = new CGI;
use Date::Calc qw(Delta_Days);
use CGI::Session;
do "common_header.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");

my $providerid =$cgi->param("txtProviderNo");

#if(!$userID)
#{
#	&sessionExpire;
#}


# Database 

my $dbh;
my ($sql, $sth,$stmt);


# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};

my ($years,$yearLoopCount,$htmlData,$hospitalName);

$yearLoopCount = 0;
@fYears = reverse(@fYears);
foreach my $fYears (@fYears){
	if($yearLoopCount <5){
		if($years){
			$years = $years . "," . $fYears;
		}
		else{
			$years = $fYears;
		}
	}
	$yearLoopCount = $yearLoopCount + 1;	
}

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

#Get the provider name
if($providerid){
	$sql = "Select HOSP_NAME from HOSPITALS where PROVIDER_NUMBER = $providerid";
	$stmt = $dbh->prepare($sql);
	$stmt->execute();
	while ($row = $stmt->fetchrow_hashref) {	
		$hospitalName = $$row{HOSP_NAME};
	}
}

#Generate Report

my(@rpt_rec_num,@rpt_year,@fy_bgn_dt,@fy_end_dt,$recCount);
$recCount = 1;
if($providerid){
	$sql = "SELECT RPT_REC_NUM, RPT_YEAR, RPT_STUS_CD, FI_NUM, FY_BGN_DT, FY_END_DT FROM CR_ALL_RPT WHERE PRVDR_NUM = $providerid AND RPT_YEAR  in ($years) ORDER BY RPT_YEAR desc ";
	$sth = $dbh->prepare($sql);
	$sth->execute();
	while ($row = $sth->fetchrow_hashref) {	
		push (@rpt_rec_num, $$row{RPT_REC_NUM});
		push (@rpt_year, $$row{RPT_YEAR});
		push (@fy_bgn_dt, $$row{FY_BGN_DT});
		push (@fy_end_dt, $$row{FY_END_DT});
		$recCount++;
	}


	#Starting a new Row
	$htmlData = $htmlData . "<tr>";
	
	#FY Begin-Date
	$htmlData = $htmlData . "<td>" . "FY Begin-Date" . "</td>";
	my $countX = 0;
	foreach my $fy_bgn_dt (@fy_bgn_dt){
		if($countX%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $fy_bgn_dt . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $fy_bgn_dt . "</td>";
		}	
		$countX++;
	}

	$htmlData = $htmlData . "</tr>";
	$htmlData = $htmlData . "<tr>";
	
	#FY End-Date
	$htmlData = $htmlData . "<td>" . "FY End-Date" . "</td>";
	my $countY = 0;
	foreach my $fy_end_dt (@fy_end_dt){
		if($countY%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $fy_end_dt . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $fy_end_dt . "</td>";
		}
		$countY++;
	}
	
	$htmlData = $htmlData . "</tr>";
	
	#Number of Days in Period
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Number of Days in Period" . "</td>";
	foreach my $fy_bgn_dt (@fy_bgn_dt){	
		@fy_bgn = split("-",$fy_bgn_dt);
		@fy_end  = split("-",$fy_end_dt[$arrayCount]);	
		$value = Delta_Days(@fy_bgn, @fy_end);	
		$value = $value+1;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $value . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $value . "</td>";
		}	
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";
	
	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";
	
	
	#Income Statement 
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td colspan=$recCount>" . "<b>Income Statement</b>" . "</td>";
	
	$htmlData = $htmlData . "</tr>";
	
	#Inpatient Revenue
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Inpatient Revenue" . "</td>";
	
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G200000-02500-0100");
		if($arrayCount%2==0){				
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";
	
	#Outpatient Revenue
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Outpatient Revenue" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G200000-02500-0200");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}	
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";
	
	#Total Patient Revenue
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Total Patient Revenue" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-00100-0100");	
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";
	
	#Contractual Allowance (Discounts)
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Contractual Allowance (Discounts)" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-00200-0100");	
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";
	
	#Net Patient Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Net Patient Revenues" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-00300-0100");	
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";
	
	#Total Operating Expense
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Total Operating Expense" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-00400-0100");	
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";
	
	#Operating Income
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "<b>Operating Income</b>" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-00500-0100");	
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value, true) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value, true) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";
	
	#Other Income (Contributions, Bequests, etc.)
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Other Income (Contributions, Bequests, etc.)" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-00600-0100");	
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";
	
	#Income from Investments
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Income from Investments" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-00700-0100");	
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";
	
	#Governmental Appropriations
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Governmental Appropriations" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-02300-0100");	
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";
	
	#Miscellaneous Non-Patient Revenue
	my $formula = "G300000-00900-0100 + G300000-01000-0100 + G300000-01100-0100 + G300000-01200-0100 + G300000-01300-0100 + G300000-01400-0100 + G300000-01500-0100 + G300000-01700-0100 + G300000-01800-0100 + G300000-02000-0100 + G300000-02100-0100 + G300000-02200-0100 + G300000-02400-0100 + G300000-02401-0100 + G300000-02402-0100 + G300000-02403-0100 + G300000-02404-0100 + G300000-02405-0100 + G300000-02406-0100 + G300000-02407-0100 + G300000-02404-0100 + G300000-02405-0100 + G300000-02406-0100 + G300000-02407-0100 + G300000-02408-0100 + G300000-02409-0100 + G300000-02410-0100 + G300000-02411-0100 + G300000-02412-0100 + G300000-02419-0100 + G300000-02420-0100";
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Miscellaneous Non-Patient Revenue" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, $formula);	
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";
	
	#Total Non-Patient Revenue
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Total Non-Patient Revenue" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-02500-0100");	
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";
	
	#Total Other Expenses
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Total Other Expenses" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-03000-0100");	
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";
	
	#Net Income or (Loss)
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "<b>Net Income or (Loss)</b>" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-03100-0100");	
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value,true) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value,true) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";
	
	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";
	
	#Balance Sheet 
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td colspan=$recCount>" . "<b>Balance Sheet</b>" . "</td>";
	$htmlData = $htmlData . "</tr>";
	
	#Assets 
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td colspan=$recCount>" . "<b>Assets</b>" . "</td>";
	$htmlData = $htmlData . "</tr>";
	
	#Current Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Current Assets" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-01100-0100");	
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";
	
	#Fixed Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Fixed Assets" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-02100-0100");	
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";
	
	#Other Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Other Assets" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-02600-0100");	
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";
	
	#Total Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "<b>Total Assets</b>" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-02700-0100");	
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value, true) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value, true) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";
	
	#Liabilities and Fund Balances 
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td colspan=$recCount><b>" . "Liabilities and Fund Balances" . "</b></td>";
	$htmlData = $htmlData . "</tr>";
	
	#Current Liabilities
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Current Liabilities" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-03600-0100");	
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";
	
	#Long-Term Liabilities
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Long-Term Liabilities" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-04200-0100");	
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";
	
	#Total Liabilities
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "<b>Total Liabilities</b>" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-04300-0100");	
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value, true) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value, true) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";
	
	#Total Fund Balances
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Total Fund Balances" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-0510-0100");	
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";
	
	#Total Liabilities & Fund Balances
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "<b>Total Liabilities & Fund Balances</b>" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-05200-0100");	
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value, true) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value, true) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";
}else{
	$htmlData="<tr><td>Please select provider number and run report again.<a href=\"/cgi-bin/FinancialReport.pl\">Back</a></td></tr>";
}
print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>Finantial Reports</TITLE>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon">
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
<link href="tablesort.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="tablesort.js"></script>
<SCRIPT SRC="/JS/sorttable.js"></SCRIPT>
</HEAD>
<body>
<BR>
<FORM NAME = "frmFinacial" Action="/cgi-bin/finantial_report.pl" method="post">
<CENTER>
};
&innerHeader();
print qq{
    <!--content start-->
        <table cellpadding="0" cellspacing="0" width="885px">
            <tr><td align="left">Welcome <b>
                	$firstName $lastName
                	</b></td>  
                	<td align="right">
                	<a href="/cgi-bin/login.pl?saction=signOut">Sign Out</a>
                	</td>
            	</tr>
        </table>
        <table cellpadding="0" cellspacing="0" width="885px">	  
	  <tr>
	        <td width="16"><img src="/images/tableRonuded/top_lef.gif" width="16" height="16"></td>
	        <td height="16" background="/images/tableRonuded/top_mid.gif"><img src="/images/tableRonuded/top_mid.gif" width="16" height="16"></td>
	        <td width="24"><img src="/images/tableRonuded/top_rig.gif" width="24" height="16"></td>
	      </tr>
	      <tr>
	        <td width="16" background="/images/tableRonuded/cen_lef.gif"></td> 
	        <td align="center" valign="middle" bgcolor="#FFFFFF">

<table border="1em" bgColor="#FFFFFF" width="100%" cellpadding="3" cellspacing="3">
<tr class='gridHeader'><td nowrap colspan=$recCount><b>Financial Report for Provider : </b><font style="color:#C11B17;">$hospitalName - $providerid</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"/cgi-bin/FinancialReport.pl\">Back</a></td></tr>
$htmlData
</table>
</td>	
		<td width="24" background="/images/tableRonuded/cen_rig.gif"><img src="/images/tableRonuded/cen_rig.gif" width="24" height="11"></td>
	      </tr>
	      <tr>
		    <td width="16" height="16"><img src="/images/tableRonuded/bot_lef.gif" width="16" height="16"></td>
		    <td height="16" background="/images/tableRonuded/bot_mid.gif"><img src="/images/tableRonuded/bot_mid.gif" width="16" height="16"></td>
		    <td width="24" height="16"><img src="/images/tableRonuded/bot_rig.gif" width="24" height="16"></td>
	      </tr>
   </table>
};

print qq{
</body>
</HTML>
};

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#--------------------------------------------------------------------------------

sub getFormulae
{
	my ($report_id, $yyyy, $formulae) = @_;
	my ($value);
	#$value = &getFormulaeValue($report_id,"(G200000-002500-0200)/(G200000-002500-0300)",$yyyy);
	
	$value = &getFormulaeValue($report_id,$formulae,$yyyy);
	
	#EBITDAR as a Percent of Interest Expense
	#Net Revenue per FTE
	
	return $value;
}


#----------------------------------------------------------------------------

sub getFormulaeValue
{
	my ($report_id,$formulae,$yyyy) = @_;
	my ($value, $i, $elementVal, $operation);	
	
	for ($i=0;$i<length($formulae);$i++)
	{
		if(substr($formulae, $i,1) =~ /[A-Z]/ )
		{
			#Spliting elements from Address
			my ($worksheet, $line, $column) = split /[-:]/,substr($formulae, $i,18);
			if(&get_value($report_id, $worksheet, $line, $column, $yyyy))
			{	
				$elementVal = &get_value($report_id, $worksheet, $line, $column, $yyyy);
			}
			else
			{
				$elementVal = 0;
			}
				
			
			$value = $value.$elementVal;
			$i = $i+17;				
		}
		elsif(substr($formulae, $i,1) =~ /\s/ )
		{
		
		}
		else 
		{			
			
			$value = $value . substr($formulae, $i,1);			
		}		
	}
	$value = eval($value);
	if(!$value)
	{
		$value = "0";
	}
	else
	{
		#$value = sprintf "%.2f", $value;		
		$value = $value;
	}
	return $value;

}

#------------------------------------------------------------------------------

sub get_value
{
   my ($report_id, $worksheet, $line, $column, $fYear) = @_;
   
   my $yyyy = $fYear;

   my ($sthVal, $value);

   my $sql = qq(
    SELECT CR_VALUE         
      FROM CR_ALL_DATA
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
   );

   unless ($sthVal = $dbh->prepare($sql)) {
       &display_error('Error preparing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sthVal->execute($report_id, $worksheet, $line, $column)) {
       &display_error('Error executing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }
   
   if ($sthVal->rows) {
      $value = $sthVal->fetchrow_array;
   }

   $sthVal->finish();

   return $value;
}

#------------------------------------------------------------------------------

sub numNegFormat
{
   my ($val,$dollar) = @_;
   if($val<0){
   	$val = $val*-1;
   	$val= Currency_Format($val);
   	if($dollar){
   		$val = "(" . "\$" . $val . ")";
   	}
   	else{
   		$val = "(" . $val . ")";
   	}	
   }
   else{
   	if($dollar){
   		$val= "\$" . Currency_Format($val);
   	}
   	else{
   		$val= Currency_Format($val);
   	}
   }
   return $val; 
}

#------------------------------------------------------------------------------

sub USA_Format { 
(my $n = shift) =~ s/\G(\d{1,3})(?=(?:\d\d\d)+(?:\.|$))/$1,/g; 
return "\$$n"; 
}

#------------------------------------------------------------------------------

sub Currency_Format { 
(my $n = shift) =~ s/\G(\d{1,3})(?=(?:\d\d\d)+(?:\.|$))/$1,/g; 
return "$n"; 
}
