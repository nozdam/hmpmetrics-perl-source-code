#!/usr/bin/perl

use CGI;
use DBI;

my $cgi = new CGI;
do "common_header.pl";
print "Content-Type: text/html\n\n";
&headerScript();
use CGI::Session;
do "audit.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");

my $saction = $cgi->param("saction");
my $Exceptionname = $cgi->param("txtExcpName");
my $element = $cgi->param("txtFormula");
my $worksheet = $cgi->param("txtOperator");
my $lineno = $cgi->param("txtSign");
my $columnno = $cgi->param("txtValue");
my $Excp_mainid = $cgi->param("Excp_mainid");
my $Excpdetailsid = $cgi->param("Excpdetailsid");
my $facility = $cgi->param("facility");

if(!$columnno){
	$columnno = null;
}

if(!$userID)
{
	&sessionExpire;
}

# Database 

my $dbh;
my ($sql, $sth);
my ($sql1, $sth1);
my ($sql2, $sth2);


# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};

my $msg = "";
my $ExceptionId = $cgi->param("Excp_mainid");

my $strDetailsId = $cgi->param("hdnRptDetailsids");
my $strElmnt = $cgi->param("hdnElement");
my $strworksheet = $cgi->param("hdnWorksheet");
my $strLinno = $cgi->param("hdnLineNo");
my $strClmn = $cgi->param("hdnColumnNo");

my @idArray = split(/~/,$strDetailsId); 
my @elemntArray = split(/~/,$strElmnt); 
my @worksheetArray = split(/~/,$strworksheet); 
my @linnoArray = split(/~/,$strLinno); 
my @columnArray = split('~',$strClmn); 

my $len = @elemntArray;

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error('ERROR connecting to database', $DBI::errstr);
   exit(1);
}

if(trim($saction) eq 'CheckException'){	

	$sql = "select * from exceptions where upper(Exceptions_name) = upper('$Exceptionname')";
	$sth = $dbh->prepare($sql);
	$sth->execute();
	if($sth->rows()>0){
		$msg ="<script>alert('Exception name already exists.');window.parent.document.getElementById('btnSave').disabled = true;window.parent.document.getElementById('btnAddMore').disabled = true;window.location.href='/cgi-bin/ExceptionActions.pl?saction=CreateNew&facility=$facility';</script>";
	}else{
		$msg ="<script>window.parent.document.getElementById('btnSave').disabled = false;window.parent.document.getElementById('btnAddMore').disabled = false;window.location.href='/cgi-bin/ExceptionActions.pl?saction=CreateNew&txtExcpName=$Exceptionname&facility=$facility';</script>";	        
	}
}
if(trim($saction) eq 'Create'){	
	
	if(!$ExceptionId){	
		$sql = "insert into exceptions(Name,CancelYN,CreatedOn,CreatedBy) values('$Exceptionname','0',CURRENT_DATE,'$userID')";
		$sth = $dbh->prepare($sql);
		$sth->execute();
		
		$sql1 = "select ID from exceptions where Name = '$Exceptionname'";
		$sth1 = $dbh->prepare($sql1);
		$sth1->execute();
		
		@row = $sth1->fetchrow_array();
		$ExceptionId = @row[0];
		$msg = "<script>alert('Exception created successfully.')";
		
		&insert("Create","",$Exceptionname,"Exceptions");
		
	}else{
			my $sql_d="select Name from exceptions where ID = '$ExceptionId'";
	$sth = $dbh->prepare($sql_d);
	$sth->execute();
	
	while ($row = $sth->fetchrow_hashref){
						$ex_name=$$row{Name};
						}
		&update("Update","","",$ex_name,"Exceptions");
						
		$sql = "update exceptions set ModifiedOn = CURRENT_DATE ,ModifiedBy = '$userID' where ID = '$ExceptionId'";
		$sth = $dbh->prepare($sql);
		$sth->execute();
		$msg = "<script>alert('Exception updated successfully.')";
		&update("Update","","","","Exceptions");
	}
		
	$sql2 = "insert into exception_details(ExceptionID,Formula,Operator,Sign,Value) values('$ExceptionId','$element','$worksheet','$lineno','$columnno')";
	$sth2 = $dbh->prepare($sql2);
	$sth2->execute();
	$saction = "";
	$Exceptionname = "";
	$ExceptionId = "";
	$msg =$msg.";window.parent.location.href='/cgi-bin/ExceptionMain.pl?facility=$facility';</script>";	
}

#--- Displays elements in selecetd Exception for editing.

if(trim($saction) eq 'AddMore'){		

	if(!$ExceptionId){
		$sql = "insert into exceptions(Name,CancelYN,CreatedOn,CreatedBy) values('$Exceptionname','0',CURRENT_DATE,'$userID')";
		$sth = $dbh->prepare($sql);
		$sth->execute();
	
		$sql1 = "select ID from exceptions where Name = '$Exceptionname'";
		$sth1 = $dbh->prepare($sql1);
		$sth1->execute();
		
		@row = $sth1->fetchrow_array();
		$ExceptionId = @row[0];	
		
	}	
	
	$sql2 = "insert into exception_details(ExceptionID,Formula,Operator,Sign,Value) values('$ExceptionId','$element','$worksheet','$lineno','$columnno')";
	$sth2 = $dbh->prepare($sql2);
	$sth2->execute();	
	$msg ="<script>window.location.href = '/cgi-bin/AddMoreExceptions.pl?saction=AddMore&Excp_mainid=$ExceptionId&Excp_name=$Exceptionname&facility=$facility'</script>";	
}

#--- Displays elements in selecetd Exception for editing.

if(trim($saction) eq 'Edit'){
	$sql = "select * from exception_details where ExceptionID = '$Excp_mainid' order by 'Name'";
	$sth = $dbh->prepare($sql);
	$sth->execute();	
}

#--- While editing if user click 'update' this will execute.

if(trim($saction) eq 'EditSave'){	

	my $sql_u="";
	my $ex_name;
	my $sql_d="select Name from exceptions where ID = '$ExceptionId'";
	$sth = $dbh->prepare($sql_d);
	$sth->execute();
	
	while ($row = $sth->fetchrow_hashref){
						$ex_name=$$row{Name};
						}
						&update("Update","","",$ex_name,"Exceptions");
	$sql = "update exceptions set Name = '$Exceptionname', ModifiedOn = CURRENT_DATE ,ModifiedBy = '$userID' where ID = '$ExceptionId'";
	$sth = $dbh->prepare($sql);
	$sth->execute();
	
	for(my $i = 0;$i<$len; $i++) {
		$sql1 = "update exception_details set Formula = '@elemntArray[$i]',Operator = '@worksheetArray[$i]',Sign = '@linnoArray[$i]',Value = '@columnArray[$i]'  where ExceptionID = '$Excp_mainid' and ID = '@idArray[$i]'";
		$sth1 = $dbh->prepare($sql1);
		$sth1->execute();
	}
	$msg ="<script>alert('Exception updated successfully.');window.parent.location.href='/cgi-bin/ExceptionMain.pl&facility=$facility';</script>";	
}

#--- Deletes selected report

if(trim($saction) eq 'delete'){	
	
	my $ex_name;
	my $sql_d="select Name from exceptions where ID = '$ExceptionId'";
	$sth = $dbh->prepare($sql_d);
	$sth->execute();
	
	while ($row = $sth->fetchrow_hashref){
						$ex_name=$$row{Name};
						}
						
	&delete("Delete","",$ex_name,"Exceptions");
	
	$sql = "delete from exceptions where ID = '$ExceptionId'";
	$sth = $dbh->prepare($sql);
	$sth->execute();
	
	$sql1 = "delete from exception_details where ExceptionID = '$ExceptionId'";
	$sth1 = $dbh->prepare($sql1);
	$sth1->execute();
	$msg ="<script>alert('Exception deleted successfully.');window.parent.location.href='/cgi-bin/ExceptionMain.pl&facility=$facility';</script>";	
}

#--- Deletes elements from the selected report.

if(trim($saction) eq 'delDetails'){	
	$sql = "delete from exception_details where ExceptionID = '$ExceptionId' and ID = '$Excpdetailsid'";
	$sth = $dbh->prepare($sql);
	$sth->execute();	

	$msg ="<script>alert('Element deleted successfully.');window.location.href='/cgi-bin/ExceptionDetails.pl?reload=false&mainID=$ExceptionId&dtName=$reportname&reload=true&facility=$facility';</script>";
}



print qq{
<HTML>
<HEAD><TITLE>Exceptions</TITLE>
 <link href="/css/admin_panel/Exceptions/ExceptionActions.css" rel="stylesheet" type="text/css" />	
 <script type="text/javascript" src="/JS/AjaxScripts/prototype.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/effects.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/controls.js"></script> 
  <script language="javascript" src="/JS/admin_panel/exceptions/ExceptionActions.js" ></script>
</HEAD>
<BODY onload="setCreate()">
<FORM NAME = "frmExceptionActions" Action="/cgi-bin/ExceptionActions.pl?facility=$facility" method="post">
<!--<div id='bodyDiv' style="position:absolute;z-index:100;display:none;background-image:url(/images/images/background-trans.png);height:100%;width:100%;"><div align='center'  id="displaybox" style="display: none;"></div></div>-->

<TABLE align="top" border=1 class="gridstyle" CELLPADDING="0" CELLSPACING="0" width="100%">
};
	if(trim($saction) eq 'Edit'){
		my $i = 0;		
		while ($row1 = $sth->fetchrow_hashref) {
			$Excp_details_id = $$row1{ID};
			$Excp_details_Formula = $$row1{Formula};
			$Excp_details_Operator = $$row1{Operator};
			$Excp_details_Sign = $$row1{Sign};
			$Excp_details_Value = $$row1{Value};			
			
			my $strSign = "";
			if($Excp_details_Sign eq '+/-'){
			  $strSign = "<select name=\"txtSign\"><option value=\"+/-\" selected> -/+ </option><option value=\"+\"> + </option><option value=\"-\"> - </option></select>";			
			}elsif($Excp_details_Sign eq '+'){
			  $strSign = "<select name=\"txtSign\"><option value=\"+/-\"> -/+ </option><option value=\"+\" selected> + </option><option value=\"-\"> - </option></select>";
			}else{
			  $strSign = "<select name=\"txtSign\"><option value=\"+/-\"> -/+ </option><option value=\"+\"> + </option><option value=\"-\" selected> - </option></select>";
			}
			my $strOper = "";
			if($Excp_details_Operator eq '>'){
			  $strOper = "<select name=\"txtOperator\"><option value=\"0\"> Select operator </option><option value=\">\" selected> > </option><option value=\"<\"> < </option><option value=\"=\"> = </option><option value=\"!=\"> != </option><option value=\">=\"> >= </option><option value=\"<=\"> <= </option></select>";			
			}elsif($Excp_details_Operator eq '<'){
			  $strOper = "<select name=\"txtOperator\"><option value=\"0\"> Select operator </option><option value=\">\"> > </option><option value=\"<\" selected> < </option><option value=\"=\"> = </option><option value=\"!=\"> != </option><option value=\">=\"> >= </option><option value=\"<=\"> <= </option></select>";
			}elsif($Excp_details_Operator eq '>='){
			  $strOper = "<select name=\"txtOperator\"><option value=\"0\"> Select operator </option><option value=\">\"> > </option><option value=\"<\"> < </option><option value=\"=\"> = </option><option value=\"!=\"> != </option><option value=\">=\" selected> >= </option><option value=\"<=\"> <= </option></select>";
			}elsif($Excp_details_Operator eq '<='){
			  $strOper = "<select name=\"txtOperator\"><option value=\"0\"> Select operator </option><option value=\">\"> > </option><option value=\"<\"> < </option><option value=\"=\"> = </option><option value=\"!=\"> != </option><option value=\">=\"> >= </option><option value=\"<=\" selected> <= </option></select>";
			}elsif($Excp_details_Operator eq '='){
			  $strOper = "<select name=\"txtOperator\"><option value=\"0\"> Select operator </option><option value=\">\"> > </option><option value=\"<\"> < </option><option value=\"=\" selected> = </option><option value=\"!=\"> != </option><option value=\">=\"> >= </option><option value=\"<=\"> <= </option></select>";
			}else{
			  $strOper = "<select name=\"txtOperator\"><option value=\"0\"> Select operator </option><option value=\">\"> > </option><option value=\"<\"> < </option><option value=\"=\"> = </option><option value=\"!=\" selected> != </option><option value=\">=\"> >= </option><option value=\"<=\"> <= </option></select>";
			}
			
			
			print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Formula</td><td style=\"background-color:#edf2fb\"><table><tr><td><input type=\"text\" name=\"txtFormula\" id=\"Formula$i\" value=\"$Excp_details_Formula\" size=\"60\" maxLength='1000'><input type=\"hidden\" name=\"txtExceptiondetailsId\" value=\"$Excp_details_id\"></td><td><span class=\"button\"><INPUT TYPE=\"button\" value=\"Formula Builder\" name=\"btnLoad\" id=\"btnLoad0\" onClick=\"LoadFormulas(1,'Formula$i')\" /></span></td></tr></table></td></tr>\n";		
			print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Operator</td><td style=\"background-color:#edf2fb\">$strOper</td></tr>\n";
			print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Sign</td><td style=\"background-color:#edf2fb\">$strSign</td></tr>\n";
			print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Value</td><td style=\"background-color:#edf2fb\"><input type=\"text\" name=\"txtValue\" value=\"$Excp_details_Value\" maxLength=\"1000\"></td></tr>\n";						
			
			print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color::#FFFFFF\" colspan=\"2\">&nbsp;<br></td></tr>\n";	
		   $i++;	
		}	
		$len = $i;
	}else{
		if(trim($saction) eq 'AddMore' || trim($saction) eq 'EditSave' || trim($saction) eq 'delete' || trim($saction) eq 'delDetails' || trim($saction) eq 'CheckException'){
			print "$msg";
		}else{
			print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Formula</td><td style=\"background-color:#edf2fb\"><table><tr><td><input type=\"text\" id=\"Formula0\" name=\"txtFormula\" size=\"80\" style=\"height:22px;\" maxLength=\"1000\"></td><td valign=\"top\"><span class=\"button\"><INPUT TYPE=\"button\" value=\"Formula Builder\" name=\"btnLoad\" id=\"btnLoad0\" onClick=\"LoadFormulas(1,'Formula0')\" /></span><td></tr></table></td></tr>\n";			
			print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Operator</td><td style=\"background-color:#edf2fb\"><select name=\"txtOperator\"><option value=\"0\"> Select operator </option><option value=\">\"> > </option><option value=\"<\"> < </option><option value=\"=\"> = </option><option value=\"!=\"> != </option><option value=\">=\"> >= </option><option value=\"<=\"> <= </option></select></td><td></td></tr>\n";
			print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Sign</td><td style=\"background-color:#edf2fb\"><select name=\"txtSign\"><option value=\"+/-\" selected> -/+ </option><option value=\"+\"> + </option><option value=\"-\"> - </option></select></td><td></td></tr>\n";
			print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Value</td><td style=\"background-color:#edf2fb\"><input type=\"text\" name=\"txtValue\" maxLength=\"1000\"></td><td></td></tr>\n";			
			print "<tr><td></td><td>$msg</td></tr>\n";
		}	
	}

print qq{
</TABLE>
<input type="hidden" name="saction" value="$saction">
<input type="hidden" name="txtExcpName" value="$Exceptionname">
<input type="hidden" name="Excp_mainid" value="$Excp_mainid">
<input type="hidden" name="facility" value="$facility">
<!-- Below hidden text boxes are used for edit functionality-->
<input type="hidden" name="hdnRptDetailsids" value="">
<input type="hidden" name="hdnElement" value="">
<input type="hidden" name="hdnLineNo" value="">
<input type="hidden" name="hdnWorksheet" value="">
<input type="hidden" name="hdnColumnNo" value="">
<input type="hidden" name="hdnFormulaTxtId" value="">
</FORM>
</BODY>
</HTML>
};
#--------------------------------------------------------------------------------------

sub sessionExpire
{

print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}
#-----------------------------------------------------------------------------


sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}

print qq{
<HTML>
<HEAD><TITLE>Exceptions-ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
<BR>
<BR>
<FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}

#----------------------------------------------------------------------------

#Perl trim function to remove whitespace from the start and end of the string

sub trim($)
{
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}
