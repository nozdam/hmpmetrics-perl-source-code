#!/usr/bin/perl -w

#----------------------------------------------------------------------------
# Program: env_check.pl
#  Author: Steve Spicer
#----------------------------------------------------------------------------

use strict;

my %ini_value;

my ($key, $value);

&read_inifile("milo.ini");

my $title = $ini_value{'TITLE_LINE'};

my $now = localtime();

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ENVIRONMENT</TITLE></HEAD>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon">
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
<BODY>
<CENTER>
<FONT SIZE="4">
$title<BR>
ENVIRONMENT AND CONTROL FILE<BR>
</FONT>
<IMG SRC="/icons/apache_pb2_ani.gif"><BR>
AS OF: $now<BR>
<TABLE BORDER="1" CELLPADDING="2" CELLSPACING="0" WIDTH=800" BGCOLOR="#eeeeee">
   <TR BGCOLOR="#ccccff">
      <TD COLSPAN="2" ALIGN="center"><B>APACHE ENVIRONMENT</B></TD>
   </TR>
};

foreach $key (sort keys %ENV) {
    $value = $ENV{$key};
    print "   <TR>\n";
    print "      <TD ALIGN=\"RIGHT\">$key:</TD>\n";
    print "      <TD><B>$value</B></TD>\n";
    print "   </TR>\n";
}

print "   <TR BGCOLOR=\"#ccccff\">\n";
print "      <TD COLSPAN=\"2\" ALIGN=\"center\"><B>CONTROL.INI FILE</B></TD>\n";
print "   </TR>\n";

foreach $key (sort keys %ini_value) {
    $value = $ini_value{$key};

    if ($key =~ m/password/i) {
       $value = "** CENSORED **";
    }

    print "   <TR>\n";
    print "      <TD ALIGN=\"RIGHT\">$key:</TD>\n";
    print "      <TD><B>$value</B></TD>\n";
    print "   </TR>\n";
}

print qq{
</TABLE>
</CENTER>
</BODY>
</HTML>
};


#----------------------------------------------------------------------------
#  loads ini file
#----------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
