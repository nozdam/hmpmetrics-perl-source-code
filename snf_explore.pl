#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: snf_explore.pl
#  author: Steve Spicer
#  Written: Wed Oct 24 20:14:32 CDT 2007
#
#  Research Data Assistance Center
#  http://www.resdac.umn.edu/Tools/TBs/TN-005-revised.asp
#----------------------------------------------------------------------------

use strict;
use CGI;
use DBI;
do "common_header.pl";
do "pagefooter.pl";

my $cgi = new CGI;

my $action    = $cgi->param('action');  
my $year      = $cgi->param('year');
my $provider  = $cgi->param('provider'); 
my $dataset   = $cgi->param('dataset'); 
my $worksheet = $cgi->param('worksheet');


my %ini_value;

unless (&read_inifile("milo.ini")) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my ($selected, $yyyy);

my $year_options = '';

foreach $yyyy (split /,/,$ini_value{HCRIS_YEARS}) {
    $selected = ($yyyy eq $year) ? 'SELECTED' : '';
    $year_options .= "   <OPTION VALUE=\"$yyyy\" $selected>$yyyy</OPTION>\n";
}


if ($provider) {
   $provider = uc($provider);
   unless ($provider =~ m/[A-Z0-9]{6}/) {
       &display_error("PROVIDER NUMBER MUST BE 6 CHARACTERS");
       exit(0);
   }
}
else {
   &display_form();
   exit(0);
}

#----------------------------------------------------------------------------

open INPUT, "snf_xwalk.txt";

my ($line, $key, $string, %hash);
   
while ($line = <INPUT>) {
   chomp($line);
    ($key, $string) = split / /,$line,2;
    $hash{$key} = $string;
}
   
close INPUT;

#----------------------------------------------------------------------------

my $dbtype     = $ini_value{"DB_TYPE"};
my $dblogin    = $ini_value{"DB_LOGIN"};
my $dbpasswd   = $ini_value{"DB_PASSWORD"};
my $dbname     = $ini_value{"DB_NAME"};
my $dbserver   = $ini_value{"DB_SERVER"};

my $dbh;

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

# snf specific 

my $hospital_name    = &get_value_by_address($year, $provider, "S200000:00400:0100");
my $hospital_address = &get_value_by_address($year, $provider, "S200000:00100:0100");
my $hospital_city    = &get_value_by_address($year, $provider, "S200000:00200:0100");
my $hospital_state   = &get_value_by_address($year, $provider, "S200000:00200:0200");
my $hospital_zip     = &get_value_by_address($year, $provider, "S200000:00301:0100");

if ($worksheet) {
   &explore_worksheet($provider, $year, $worksheet);
} else {
   &explore_provider($provider, $year);
}

$dbh->disconnect();

exit(0);

#----------------------------------------------------------------------------

sub explore_provider
{

   my ($provider, $year) = @_;

   my ($worksheet, $value_count, $hover, $desc, $link, $class);

   my $count = 0;

   my ($sth, $sql);

   $sql = qq/
    SELECT SNF_WORKSHEET, COUNT(*)
      FROM SNF_${year}_RPT AA
 LEFT JOIN SNF_${year}_DATA BB
        ON AA.RPT_REC_NUM = BB.SNF_REC_NUM
     WHERE PRVDR_NUM     = ?
  GROUP BY SNF_WORKSHEET
   /;

   unless ($sth = $dbh->prepare($sql)) {
       &display_error("Error preparing sql query", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   $sth->execute($provider);

   unless ($sth->rows()) {
     &display_error("NOTHING AVAILABLE FOR PROVIDER $provider");
     return(0);
   }

   my $xlink = "<A HREF=\"/2540-96_Forms.xls\" TARGET=\"_blank\" TITLE=\"CLICK FOR A SAMPLE BLANK REPORT FROM THIS SECTION IN EXCEL FORMAT\"><IMG BORDER=\"0\" SRC=\"/icons/excel_icon.gif\"></A>";

   print "Content-Type: text/html\n\n";

   print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
<HEAD><TITLE>SNF COST REPORT EXPLORER</TITLE>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon">
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
<link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />
</HEAD>
<BODY>
<CENTER>
<BR>
};
&innerHeader();
print qq{

    <!--content start--><br>
        <table cellpadding="0" cellspacing="0" width="885px">
            <tr>
                <td valign="top">
                    <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px">

<TABLE BORDER="0" CELLPADDING="2" WIDTH="800">
   <TR>
     <TD ALIGN="LEFT" VALIGN="TOP">
        <FONT SIZE="4">
        <B>
        $hospital_name<BR>
        $hospital_address<BR>
        $hospital_city, $hospital_state  $hospital_zip<BR>
        </B>
        </FONT>
     </TD>
     <TD ALIGN="RIGHT" VALIGN="TOP">
        PROVIDER:<BR>
            YEAR:
     </TD>
     <TD ALIGN="LEFT" VALIGN="TOP">
        <B>
        <FONT COLOR="#EE0000">
        $provider<BR>
        </FONT>
        <FORM METHOD="GET" ACTION="/cgi-bin/snf_explore.pl">
             <SELECT NAME="year" onChange="this.form.submit()">
                $year_options
             </SELECT>
             <INPUT TYPE="hidden" name="provider" value="$provider">
             <INPUT TYPE="hidden" name="action" value="go">
        </FORM>
        </FONT>
        </B>
     </TD>
     <TD ALIGN="RIGHT" VALIGN="TOP">
        <IMG SRC="/icons/CMSLogo.gif">
     </TD>
   </TR>
</TABLE>
WORKSHEET TMPLATE: $xlink
<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0" WIDTH="100%">
   <TR CLASS="gridheader">
     <TD ALIGN="CENTER"><B>WORKSHEET</B></TD>
     <TD ALIGN="LEFT"><B>DESCRIPTION</B></TD>
     <TD ALIGN="RIGHT"><B>VALUES</B></TD>
   </TR>
   };

   while (($worksheet, $value_count) = $sth->fetchrow_array) {

      $desc = $hash{$worksheet};

      $link = "<A HREF=\"/cgi-bin/snf_explore.pl?provider=$provider&year=$year&worksheet=$worksheet\">$value_count</A>";

      $desc = '&nbsp;' unless ($desc);

      $class = (++$count % 2) ? 'gridrow' : 'gridalternate';

      print "   <TR CLASS=\"$class\">\n";
      print "      <TD ALIGN=\"CENTER\">$worksheet</TD>\n";
      print "      <TD ALIGN=\"LEFT\">$desc</TD>\n";
      print "      <TD ALIGN=\"RIGHT\">$link</TD>\n";
      print "   </TR>\n";
   }

   print qq{
</TABLE>
<FONT SIZE="1">
$count ROWS<BR>
</FONT>
</td>
            <td valign="top">
                <img src="/images/content_right.gif" /></td>
        </tr>
    </table>
    <!--content end-->
};
&TDdoc;
print qq{
</CENTER>
</BODY>
</HTML>
   };

   return(0);
}

#----------------------------------------------------------------------------

sub explore_worksheet
{

   my ($provider, $year, $worksheet) = @_;

   my ($address, $row, $col, $value, $class, $align, $editlink);

   my $desc = $hash{$worksheet};

   my $count = 0;

   my ($sth, $sql);

   $sql = qq/
    SELECT SNF_LINE, SNF_COLUMN, SNF_VALUE, KB_DESC
      FROM SNF_${year}_RPT AA
 LEFT JOIN SNF_${year}_DATA BB
        ON AA.RPT_REC_NUM = BB.SNF_REC_NUM
 LEFT JOIN SNF_KB_TABLE ON KB_ADDRESS = CONCAT(SNF_WORKSHEET, ':', SNF_LINE, ':', SNF_COLUMN)
     WHERE PRVDR_NUM     = ?
       AND SNF_WORKSHEET  = ?
   /;

   unless ($sth = $dbh->prepare($sql)) {
       &display_error("Error preparing sql query", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   $sth->execute($provider, $worksheet);

   unless ($sth->rows()) {
     &display_error("NOTHING AVAILABLE FOR PROVIDER $provider");
     return(0);
   }

   print "Content-Type: text/html\n\n";

   print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
<HEAD><TITLE>SNF COST REPORT EXPLORER</TITLE>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon">
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
</HEAD>
<BODY>
<CENTER>
<BR>
<TABLE BORDER="0" CELLPADDING="2" WIDTH="95%">
   <TR>
     <TD ALIGN="LEFT" VALIGN="TOP">
        <FONT SIZE="4">
        <B>
        $hospital_name<BR>
        $hospital_address<BR>
        $hospital_city $hospital_state  $hospital_zip<BR><BR>
        </B>
        </FONT>
     </TD>
     <TD ALIGN="RIGHT" VALIGN="TOP">
       WORKSHEET:<BR>
        PROVIDER:<BR>
            YEAR:
     </TD>
     <TD ALIGN="LEFT" VALIGN="TOP">
        <B>
        <FONT COLOR="#EE0000">
        $worksheet $desc<BR>
        $provider<BR>
        <FORM METHOD="GET" ACTION="/cgi-bin/snf_explore.pl">
             <SELECT NAME="year" onChange="this.form.submit()">
                $year_options
             </SELECT>
             <INPUT TYPE="hidden" name="provider" value="$provider">
             <INPUT TYPE="hidden" name="worksheet" value="$worksheet">
             <INPUT TYPE="hidden" name="action" value="go">
        </FORM>
        </FONT>
        </B>
     </TD>
     <TD ALIGN="RIGHT" VALIGN="TOP">
        <IMG SRC="/icons/CMSLogo.gif">
     </TD>
   </TR>
</TABLE>

</FONT>
<TABLE BORDER="1" CELLPADDING="2" CELLSPACING="0" WIDTH="95%">
   <TR CLASS="title">
     <TD ALIGN="CENTER"><B>ADDRESS</B></TD>
     <TD ALIGN="CENTER"><B>DESCRIPTION IF POPULATED</B></TD>
     <TD ALIGN="CENTER"><B>LINE</B></TD>
     <TD ALIGN="CENTER"><B>COLUMN</B></TD>
     <TD ALIGN="CENTER"><B>VALUE</B></TD>
   </TR>
   };

   while (($row, $col, $value, $desc) = $sth->fetchrow_array) {

      $address = "$worksheet:$row:$col";

      if ($row =~ m/^(\d\d\d)(\d\d)/) {
         $row = "$1.$2";
         $row =~ s/^0+//;
         $row =~ s/\.0+$//;
         $row = '0' if ($row eq '');
      }

      if ($col =~ m/^(\d\d)(\d\d)/) {
         $col = "$1.$2";
         $col =~ s/\.0+$//;
         $col =~ s/^0+//;
         $col = '0' if ($col eq '');
      }

      $align = 'LEFT';

      if ($value =~ m/^-?\d+$|-?\d*\.\d+$/) {
         $align = 'RIGHT';
#        $value = &comify($value);
      }

      $editlink = "<A HREF=\"#\" onClick=\"MyWindow=window.open('/cgi-bin/snf_kb_update.pl?address=$address','MyWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=750,height=600,left=25,top=25'); return false;\">$address</A>";

      $class = (++$count % 2) ? 'odd' : 'even';

      print "   <TR CLASS=\"$class\">\n";
      print "      <TD ALIGN=\"CENTER\">$editlink</TD>\n";
      print "      <TD ALIGN=\"LEFT\">$desc</TD>\n";
      print "      <TD ALIGN=\"CENTER\">$row</TD>\n";
      print "      <TD ALIGN=\"CENTER\">$col</TD>\n";
      print "      <TD ALIGN=\"$align\">$value</TD>\n";
      print "   </TR>\n";
   }

   print qq{
</TABLE>
<FONT SIZE="1">
$count ROWS<BR>
</FONT>
</CENTER>
};
&TDdoc;
print qq{
</BODY>
</HTML>
   };

   return(0);
}

#----------------------------------------------------------------------------


sub get_value_by_address
{
   my ($year, $provider, $address) = @_;

   my ($worksheet, $row, $col) = split /[-:\|]/,$address;

   my ($sth, $sql);

   $sql = qq/
    SELECT SNF_VALUE         
      FROM SNF_${year}_RPT AA
 LEFT JOIN SNF_${year}_DATA BB
        ON AA.RPT_REC_NUM = BB.SNF_REC_NUM
       AND SNF_WORKSHEET  = ?
       AND SNF_LINE       = ?
       AND SNF_COLUMN     = ?
     WHERE PRVDR_NUM     = ?
   /;

   unless ($sth = $dbh->prepare($sql)) {
       &display_error("Error preparing sql query", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   $sth->execute($worksheet, $row, $col, $provider);

   my ($value) = $sth->fetchrow_array;

   return($value);
}


#----------------------------------------------------------------------------

sub display_error
{
   my @list = @_;

   my ($string, $s);

   foreach $s (@list) {
      $string .= "$s<BR>\n";
   }

   print "Content-Type: text/html\n\n";

   print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
<HEAD><TITLE>ERROR</TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
<link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />

</HEAD>
<BODY>
<CENTER>
<BR>
<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/index.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/cr_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="/cgi-bin/cr_batchx.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="/cgi-bin/snf_batch1.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="/cgi-bin/graph.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Graph Utility</div>">
                                <img src="/images/graph_icon_inner.gif" onmouseover="src='/images/graph_icon_inner_hover.gif';"
                                    onmouseout="src='/images/graph_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 102px" align="Center">
                            <a href="/team.html" class="addToolTip" title="<div id='ToolTipTextWrap'>Milo Team</div>">
                                <img src="/images/milo_team_icon_inner.gif" onmouseover="src='/images/milo_team_icon_inner_hover.gif';"
                                    onmouseout="src='/images/milo_team_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->

<FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<TABLE CLASS="error" WIDTH="600">
   <TR>
      <TD ALIGN="CENTER">
          <FONT SIZE="4">
          <BR>
          $string
          <BR>
          </FONT>
      </TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
   };

   return(0);
}


#----------------------------------------------------------------------------


sub display_form
{
   print "Content-Type: text/html\n\n";
   
   print qq{
   <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
   <HTML>
   <HEAD>
   <TITLE>SNF COST REPORT EXPLORER</TITLE>
   <LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon">
   <LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
   <link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />

   </HEAD>
   <BODY>
   <CENTER>
   <BR>
   <FORM METHOD="GET" ACTION="/cgi-bin/snf_explore.pl">
};

&innerHeader();

print qq{
<!--content start--><br>
        <table cellpadding="0" cellspacing="0" width="885px">
            <tr>
                <!--<td valign="top">
                    <img src="/images/content_left.gif" /></td>-->
            <td valign="top" style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px">
<div class="pageheader">SNF COST REPORT EXPLORER</div>
   <TABLE align="center" width="100%" CELLPADDING="5" cellspacing="5" class="gridstyle" style="background-color:white;">      
      <TR>
         <TD style="padding-left:250px;">PROVIDER </TD>
         <TD style="padding-right:350px;">:&nbsp;
             <INPUT TYPE="text" class="textboxstyle" NAME="provider" SIZE="6" VALUE="$provider" TITLE="ENTER 6 DIGIT PROVIDER NUMBER">
         </TD>
       </TR>
       <TR>
         <TD style="padding-left:250px;">COST REPORT YEAR</TD>
         <TD style="padding-right:350px;">:&nbsp;
             <SELECT NAME="year" TITLE="CHOOSE A COST REPORT YEAR">
                $year_options
             </SELECT>
         </TD>
      </TR>
      <TR>
         <TD COLSPAN="2" ALIGN="center" style="padding-right:215px;">
             <INPUT TYPE="hidden" NAME="action" VALUE="go">
             <span class="button"><INPUT TYPE="submit" VALUE="  QUERY COST REPORT "></span>
         </TD>
      </TR>
   </TABLE>
</td>
            <!--<td valign="top">
                <img src="/images/content_right.gif" /></td>-->
        </tr>
    </table>
    <!--content end-->
};
&TDdoc;
print qq{
   </FORM>
   </CENTER>
   </BODY>
   </HTML>
   };
}


#----------------------------------------------------------------------------

sub comify
{
  local($_) = shift;
  1 while s/^(-?\d+)(\d{3})/$1,$2/;
  return($_);
}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#------------------------------------------------------------------------------

