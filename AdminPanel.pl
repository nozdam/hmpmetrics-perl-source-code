#!/usr/bin/perl

use CGI;
use DBI;
use strict;
my $cgi = new CGI;
do "common_header.pl";
print "Content-Type: text/html\n\n";
&headerScript();
do "pagefooter.pl";
use CGI::Session;
do "audit.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");

if(!$userID)
{
	&sessionExpire;
}

&view("View","","Admin Panel");

print qq{
<HTML>
<HEAD><TITLE>Standard Reports</TITLE>
 <script type="text/javascript" src="/JS/AjaxScripts/prototype.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/effects.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/controls.js"></script>  
</HEAD>
<BODY>
<BR>
<FORM NAME = "frmReportActions" Action="/cgi-bin/ReportActions.pl" method="post">
<CENTER>
};
&innerHeader();
print qq {
<ul id="sddm">
    <!--content start-->
               <table cellpadding="0" cellspacing="0" width="885px">
            <tr>
                <td valign="top">
                    <img src="/images/content_left.gif" /></td>
                <td valign="top" style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; ">
			<br>
			<table style="border:1px solid gray;"  cellpadding="5" cellspacing="5" align="center" width="100%" >
				<tr bgcolor="#81BEF7">
					<td colspan=5>
						<b>Admin Control Panel</b>
					</td>	
					
				</tr>
				<tr>					
					<td align="center">
						<img src="/images/AdminStdReports.gif">
					</td>	
					<td align="center">
						<img src="/images/h_spreadsheet_icon_inner.gif">
						</td>
					<td align="center">
						<img src="/images/AdminException.gif" height="60px">
					</td>
					<td align="center">
						<img src="/images/AdminUser.png">
					</td>
					<td align="center">
						<img src="/images/standard_metrics.gif">
					</td>
				</tr>
				<tr bgcolor="#D8D8D8">						
					
					<td style="width: 132px" align="Center">
						<a href="#" onmouseover="src='/images/AdminStdReports.gif'; mopen('standardrep')"
				    onmouseout="src='AdminStdReports.gif';"  style="border: 0px; cursor: pointer"><b>Standard Reports<b></a></td>
					
<td style="width: 85px" align="Center">
                            <a href="#"  title=\"Spreadsheet Generator\" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';mopen('spreadsheet');"
onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /><b>Spreadsheet Generator<b></a></td>					
					
					<td style="width: 132px" align="center">
						<a href="#" onmouseover="src='/images/AdminException.gif'; mopen('exception')"
				    onmouseout="src='AdminException.gif';"  style="border: 0px; cursor: pointer"><b>Exceptions<b></a>
					</td>
					
					<td style="width: 132px" align="center">
						<a href="/cgi-bin/userView.pl" onmouseover="src='/images/AdminUser.png'; mopen('')"
				    onmouseout="src='AdminUser.png';"  style="border: 0px; cursor: pointer"><b>User Management<b></a>
					</td>
					
					<td style="width: 132px" align="center">
						<a href="#" onmouseover="src='/images/standard_metrics.gif'; mopen('standardmet')"
				    onmouseout="src='AdminStdReports.gif';"  style="border: 0px; cursor: hand"><b>Standard Metrics<b></a></td>					
				</tr>				
								<!--<tr>
								<td>
									<img src="/images/AdminUser.png">
								</td>
								<td>
									<img src="/images/AdminUser.png">
								</td>
								<td>
									<img src="/images/AdminUser.png">
								</td>
							</tr>
							<tr>
								<td>
									<a href="/cgi-bin/DataSetConfigure.pl">User Management</a>
								</td>	
								<td>
									<a href="/cgi-bin/DataSetConfigure.pl">Exceptions</a>
								</td>
								<td>
									<a href="/cgi-bin/DataSetConfigure.pl">Exceptions</a>
								</td>			
							</tr>-->
							<tr>
			    
			    <td style="width: 116px" align="Center">
				<li> <div id="standardrep"
					onmouseover="mcancelclosetime()"
					onmouseout="mclosetime()">
				    <a href="/cgi-bin/DataSetConfigure.pl?facility=hospital">Hospital</a>
				    <a href="/cgi-bin/DataSetConfigure.pl?facility=snf">Skilled Nursing Facility</a>
				    <a href="/cgi-bin/DataSetConfigure.pl?facility=renal">Renal Facility</a>
				    <a href="/cgi-bin/DataSetConfigure.pl?facility=hospice">Hospice</a>
				    <a href="/cgi-bin/DataSetConfigure.pl?facility=hha">Home Health Agency</a>
				</div></li>
			    </td>
						
						<td style="width: 116px" align="Center">
				<li> <div id="spreadsheet"
					onmouseover="mcancelclosetime()"
					onmouseout="mclosetime()">
				    <a href="/cgi-bin/cr_batchx.pl?facility=hospital">Hospital</a>
				    <a href="#">Skilled Nursing Facility</a>
				    <a href="#">Renal Facility</a>
				    <a href="#">Hospice</a>
				    <a href="#">Home Health Agency</a>
				</div></li>
			    </td>
				
			    <td style="width: 116px" align="Center">
				<li> <div id="exception"
					onmouseover="mcancelclosetime()"
					onmouseout="mclosetime()">
				    <a href="/cgi-bin/ExceptionMain.pl?facility=hospital">Hospital</a>
				    <a href="#">Skilled Nursing Facility</a>
				    <a href="#">Renal Facility</a>
				    <a href="#">Hospice</a>
				    <a href="#">Home Health Agency</a>
				</div></li>
			    </td>
				<td>
				<div>
				</div>
				</td>
			    <td style="width: 116px" align="Center">
			    	<li> <div id="standardmet"
					onmouseover="mcancelclosetime()"
					onmouseout="mclosetime()">
				    <a href="/cgi-bin/standard_metrics.pl?facility=hospital">Hospital</a>
					<a href="/cgi-bin/standard_metrics.pl?facility=snf">Skilled Nursing Facility</a>
					<a href="/cgi-bin/standard_metrics.pl?facility=renal">Renal Facility</a>
					<a href="/cgi-bin/standard_metrics.pl?facility=hospice">Hospice</a>
					<a href="/cgi-bin/standard_metrics.pl?facility=hha">Home Health Agency</a>
				</div></li>
                        	</td>
			   
		    </tr>
			</table>
			</ul>
							<tr>
								<td colspan=3>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								</td>	
							</tr>
							
			</table>	
		
	    </tr>
	</table>
	</ul>
	<!--content end-->
	};
	
		&TDdoc;
		print qq{
</CENTER>
</FORM>
</BODY>
</HTML>
};
#--------------------------------------------------------------------------------------

sub sessionExpire
{

print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}
#-----------------------------------------------------------------------------

#Perl trim function to remove whitespace from the start and end of the string

sub trim($)
{
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}
