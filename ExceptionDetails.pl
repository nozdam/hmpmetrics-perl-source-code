#!/usr/bin/perl

use CGI;
use DBI;

my $cgi = new CGI;
do "common_header.pl";
print "Content-Type: text/html\n\n";
&headerScript();
use CGI::Session;

my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $Excp_mainid = $cgi->param("mainID");
my $reload =  $cgi->param("reload");
my $dtName = $cgi->param("dtName");
my $facility = $cgi->param("facility");
if(!$userID)
{
	&sessionExpire;
}

# Database 

my $dbh;
my ($sql, $sth);

# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};
my $orderBy = $cgi->param("orderBy");
if(!$orderBy){
	$orderBy = "Formula";
}
unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error('ERROR connecting to database', $DBI::errstr);
   exit(1);
}

$sql = "select * from exception_details where ExceptionID = '$Excp_mainid' order by $orderBy";
$sth = $dbh->prepare($sql);
$sth->execute();


print qq{
<HTML>
<HEAD><TITLE>Exceptions</TITLE>
 <script type="text/javascript" src="/JS/AjaxScripts/prototype.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/effects.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/controls.js"></script> 
 <script language="javascript" src="/JS/admin_panel/exceptions/ExceptionDetails.js" ></script>
 </HEAD>
 <BODY onload="setRepName()">
<FORM NAME = "frmExceptionDetails" Action="" method="post">
<TABLE align="top" border=1 class="gridstyle" CELLPADDING="0" CELLSPACING="0" width="100%" height="100%">			
};
			my $i = 0;
			my $isFormula = NULL;
			while ($row1 = $sth->fetchrow_hashref) {
				$Excp_details_id = $$row1{ID};
				$Excp_details_formula = $$row1{Formula};
				$Excp_details_Operator = $$row1{Operator};
				$Excp_details_Sign = $$row1{Sign};
				$Excp_details_value = $$row1{Value};								
				$dispname = $Excp_details_formula.' '.$Excp_details_Operator.' '.$Excp_details_Sign.' '.$Excp_details_value;
				$class1 = (++$i % 2) ? 'gridrow' : 'gridalternate';
				
				print "<tr ><td nowrap style=\"width:10%;color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">SlNo</td><td style=\"background-color:#edf2fb\">$i</td></tr>\n";
				print "<tr ><td nowrap style=\"width:10%;color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Formula</td><td style=\"background-color:#edf2fb\">$Excp_details_formula</td></tr>\n";				
				print "<tr ><td nowrap style=\"width:10%;color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Operator</td><td style=\"background-color:#edf2fb\">$Excp_details_Operator</td></tr>\n";
				print "<tr ><td nowrap style=\"width:10%;color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Sign</td><td style=\"background-color:#edf2fb\">$Excp_details_Sign</td></tr>\n";
				print "<tr ><td nowrap style=\"width:10%;color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Value</td><td style=\"background-color:#edf2fb\">$Excp_details_value</td></tr>\n";								
				print "<tr bgcoloe=\"#307D7E\"><td></td><td colspan=\"6\"><table width=\"450px\"><tr><td></td><td></td><td width=\"80%\" align=\"left\"><a href=\"#\" onclick=\"delDetails('/cgi-bin/ExceptionActions.pl?facility=$facility&saction=delDetails&txtExcpName=$dtName&Excp_mainid=$Excp_mainid&Excpdetailsid=$Excp_details_id\','$dispname')\" title=\"Delete '$rpt_details_element' element\"><img src=\"/images/btn_delete.gif\" border=\"0\"></a></td></tr></table></td></tr>\n";
			}
print qq{
	</TABLE>
<input type="hidden" name="dtName" value="$dtName">
<input type="hidden" name="mainID" value="$Excp_mainid">
<input type="hidden" name="orderBy" value="$orderBy">
<input type="hidden" name="recCount" value="$i">
<input type="hidden" name="facility" value="$facility">
</FORM>
</BODY>
</HTML>
};

#--------------------------------------------------------------------------------------
sub sessionExpire
{

print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}
#-----------------------------------------------------------------------------


sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}



print qq{
<HTML>
<HEAD><TITLE>Exceptions-ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
<BR>
<BR>
<FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}