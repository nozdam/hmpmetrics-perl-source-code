#!/usr/bin/perl -w

#--------------------------------------------------------------------
# Program: dbstatus.pl
#  Author: Steve Spicer
# Written: Thu Oct 18 09:07:40 CDT 2007
# Revised: Mon Jul 14 15:14:38 CDT 2008
#   Notes:
# Depends: DBI.pm, CGI.pm
#--------------------------------------------------------------------

use strict;
use DBI;

my ($line, $key, $variable, $value);

my %ini_value;

unless (&read_inifile("milo.ini")) {
   &display_error("can't read \"milo.ini\"");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my $dbuser     = $ini_value{'DB_LOGIN'};
my $dbpassword = $ini_value{'DB_PASSWORD'};

my ($dbh, $sth);

unless ($dbh = DBI->connect("dbi:$dbtype:$dbname:$dbserver",$dbuser,$dbpassword)) {
   &display_error("ERROR connecting to database:",DBI->errstr);
   exit(1);
}

# $sql = "SHOW STATUS";
my $sql = "SHOW FULL PROCESSLIST";

unless ($sth = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL query:",DBI->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute()) {
   &display_error("ERROR executing SQL query:",DBI->errstr);
   $dbh->disconnect;
   exit(1);
}

my $now = localtime();

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>MYSQL DATABASE STATUS</TITLE></HEAD>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon">
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
 <link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />

<BODY>
<CENTER>

<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/index.html" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/cr_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="/cgi-bin/cr_batchx.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="/cgi-bin/snf_batch1.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="/cgi-bin/graph.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Graph Utility</div>">
                                <img src="/images/graph_icon_inner.gif" onmouseover="src='/images/graph_icon_inner_hover.gif';"
                                    onmouseout="src='/images/graph_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                      <td style="width: 102px" align="Center">
		                                  <a href="/cgi-bin/PeerMain.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
		                                      <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->


<!--content start--><br>
    <table cellpadding="0" cellspacing="0" width="885px">
        <tr>
            <td valign="top">
                <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px">

<TABLE CELLPADDING="0" CELLSPACING="0" width="100%">
<Tr><Td>
<div class="pageheader">MYSQL DATABASE SERVER STATUS</div>
 <b>$sql</b><br>
$now

</td><td align="right">
<IMG SRC="/icons/powered-by-mysql-167x86.gif"><BR>
</td></tr>
</table>


<TABLE CELLPADDING="5" CELLSPACING="0" width="100%" class="gridstyle">
};

my (@row, $class, $element);

print "<TR BGCOLOR=\"#DDDDDD\">\n";

foreach $element (@{$sth->{NAME}}) {
   print "   <TD ALIGN=\"LEFT\" class=\"gridheader\"><B>$element</B></TD>\n";
}
print "</TR>\n";

my $count = 0;

while (@row = $sth->fetchrow_array) {

   $class = (++$count % 2) ? "gridrow" : "gridalternate";

   print "   <TR CLASS=\"$class\">\n";
   foreach $element (@row) {
      print "   <TD ALIGN=\"LEFT\">$element</TD>\n";
   }
   print "</TR>\n";
}

print qq{
</TABLE>
<FORM><span class="button">
<INPUT TYPE="button" VALUE=" BACK " onClick="history.go(-1);return true;" class="button"></span>
</FORM>
</CENTER>

	           </td>
	            <td valign="top">
	                <img src="/images/content_right.gif" /></td>
	        </tr>
	    </table>
	    <!--content end-->

</BODY>
</HTML>
};

$sth->finish;
$dbh->disconnect;

exit 0;

#------------------------------------------------------------------------------

sub display_error
{
   my $s;

   print "Content-Type: text/html\n\n";

   print qq{
<HTML>
<HEAD><TITLE>ERROR NOTIFICATION</TITLE></HEAD>
<LINK REL="stylesheet" TYPE="text/css" HREF="/scanstyle.css">
<link href="/JS/ergosign.styleform.css" media="all" type="text/css" rel="stylesheet">

    <script type="text/javascript" src="/JS/mootools-1.2-core.js"></script>

    <script type="text/javascript" src="/JS/ergosign.styleform.js"></script>

    <script type="text/javascript" src="/JS/script.js"></script>
    <link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />
<BODY>
<CENTER>
<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/index.html" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/cr_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="/cgi-bin/cr_batchx.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="/cgi-bin/snf_batch1.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="/cgi-bin/graph.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Graph Utility</div>">
                                <img src="/images/graph_icon_inner.gif" onmouseover="src='/images/graph_icon_inner_hover.gif';"
                                    onmouseout="src='/images/graph_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                       <td style="width: 102px" align="Center">
		                                   <a href="/cgi-bin/PeerMain.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
		                                       <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->

<BR>
<FONT SIZE="5" COLOR="RED">
<B>ERROR NOTIFICATION</B><BR>
</FONT>
<BR>
<TABLE BORDER="1" CELLPADDING="20" CELLSPACING="0" WIDTH="600" BGCOLOR="#FFFFCC">
<TR><TD>
<UL>
};
   foreach $s (@_) {
      print "$s<BR>";
   }
   print qq{
</TD></TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</FONT>
</CENTER>
</BODY>
</HTML>
};
}

#----------------------------------------------------------------------------
#  loads ini file
#----------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
