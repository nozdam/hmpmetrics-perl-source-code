#!/usr/bin/perl

use CGI;
use DBI;
use strict;
my $cgi = new CGI;
do "common_header.pl";
print "Content-Type: text/html\n\n";
&headerScript();
use CGI::Session;

my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");

my $saction = $cgi->param("saction");
my $reportname = $cgi->param("rept_name");
my $rpt_mainid = $cgi->param("rpt_mainid");

my $facility = $cgi->param("facility");
if(!$userID)
{
	&sessionExpire;
}

print qq{
<HTML>
<HEAD><TITLE>Standard Reports</TITLE>
 <script type="text/javascript" src="/JS/AjaxScripts/prototype.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/effects.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/controls.js"></script>
 <script type="text/javascript" src="/JS/admin_panel/standard_report/AddMoreReports.js"></script>
</HEAD>
<BODY onload="setCreate()">
<FORM NAME = "frmReportActions" Action="/cgi-bin/ReportActions.pl?facility=$facility" method="post">
<TABLE align="top" border=1 class="gridstyle" CELLPADDING="0" CELLSPACING="0" width="100%">
};
print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Element</td><td style=\"background-color:#edf2fb\"><input type=\"text\" name=\"txtElement\" size=\"60\" maxLength=\"50\"></td></tr>\n";
print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Worksheet</td><td style=\"background-color:#edf2fb\"><input type=\"text\" name=\"txtWorksheet\" maxLength=\"7\"></td></tr>\n";
print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Line No.</td><td style=\"background-color:#edf2fb\"><input type=\"text\" name=\"txtLineNo\" maxLength=\"5\"></td></tr>\n";
print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Column No.</td><td style=\"background-color:#edf2fb\"><input type=\"text\" name=\"txtColumnNo\" maxLength=\"4\"></td></tr>\n";
print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Add Formulae</td><td style=\"background-color:#edf2fb\"><input type=\"radio\" name=\"txtIsFormula\" onclick=\"set(0,0)\" value=\"0\" checked> No <input type=\"radio\" name=\"txtIsFormula\" onclick=\"set(1,0)\" value=\"1\"> Yes </td></tr>\n";
print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Formulae</td><td nowrap style=\"background-color:#edf2fb\"><input type=\"text\" id=\"Formula0\" name=\"txtFormula\"  disabled size=\"80\" maxLength=\"1000\"><span class=\"button\"><INPUT TYPE=\"button\" value=\"Formula Builder\" name=\"btnLoad\" id=\"btnLoad0\" onClick=\"LoadFormulas(1,'Formula0')\" /></span></td></tr>\n";
print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Summary Type</td><td style=\"background-color:#edf2fb\"><select name=\"txtSummary\" ><option value=\"Avg\">Average</option><option value=\"Sum\">Sum</option><option value=\"NA\" selected>No Summary</option></select></td></tr>\n";
print qq{
</TABLE>
<input type="hidden" name="saction" value="$saction">
<input type="hidden" name="txtRptName" value="$reportname">
<input type="hidden" name="facility" value="$facility">
<input type="hidden" name="rpt_mainid" value="$rpt_mainid">
<input type="hidden" name="hdnFormulaTxtId" value="">
</FORM>
</BODY>
</HTML>
};
#--------------------------------------------------------------------------------------

sub sessionExpire
{

print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};
}
#-----------------------------------------------------------------------------

#Perl trim function to remove whitespace from the start and end of the string

sub trim($)
{
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}
