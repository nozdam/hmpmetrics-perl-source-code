#!/usr/bin/perl

use CGI;
use DBI;
use strict;
do "pagefooter.pl";
my $cgi = new CGI;
do "common_header.pl";
use CGI::Session;
do "audit.pl";
use CGI::Carp qw ( fatalsToBrowser );
use File::Basename;
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $userType = $session->param("role");
my ($sth1,$row,$lHeight,$lstrDisp,$i,$row1,$peername1,$status1,$providerid1,$createon1,$alterdate1,$Description);
my ($sampleSize,$class1,$s);
if(!$userID)
{
	&sessionExpire;
}
$session->clear(["narrow_search", "narrow_search_module"]);
$session->param("narrow_search", "0");
$session->param("narrow_search_module", "peer");
my $narrow_search = $session->param("narrow_search");
my $narrow_search_module = $session->param("narrow_search_module");
# Database

my $dbh;
my ($sql, $sth,$facility_id,$sql_f);

&view("View","","Peer Group");
# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};

my $orderBy = $cgi->param('orderBy');
my $acsdesc = $cgi->param('acsdesc');
my $tmpOrder = $orderBy;

if(!$orderBy){
	$orderBy = "CreateDate asc";
}

if(!$acsdesc){
	$acsdesc = "true";
}

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error('ERROR connecting to database', $DBI::errstr);
   exit(1);
}
my $facility = $cgi->param("facility");

$sql_f = "select facility_id from tbl_facility_type where facility_name = '$facility'";
$sth1= $dbh->prepare($sql_f);
$sth1->execute();
while ($row = $sth1->fetchrow_hashref) {
	$facility_id = $$row{facility_id};
}

#Internal User
if($userType eq '1'){
	$sql = "select pm.CreatedBy as ownerid, pm.PeerGroup_ID as PeerGroup_ID, pm.Name as Name, pm.isActive as isActive,pm.CreateDate as CreateDate ,pm.AlterDate , pm.Description as Description, (select count(*) from peergroup_details as pd where pd.PeerGroup_Main_ID = pm.PeerGroup_ID) as sampleSize, concat(usrCr.First_Name, ' ', usrCr.Last_Name) as CreatedBy, concat(usrUp.First_Name, ' ', usrUp.Last_Name) as ModifiedBy from peergroup_main as pm left outer join userdetails as usrCr on usrCr.UsersID = pm.CreatedBy left outer join userdetails as usrUp on usrUp.UsersID = pm.ModifiedBy Where pm.CreatedBy in (Select distinct userid from users where roleid = 1 and IsActive = 1) and facility_type = $facility_id order by $orderBy";
}

#External User
if($userType eq '3'){
	$sql = "select pm.CreatedBy as ownerid, pm.PeerGroup_ID as PeerGroup_ID, pm.Name as Name, pm.isActive as isActive,pm.CreateDate as CreateDate ,pm.AlterDate , pm.Description as Description, (select count(*) from peergroup_details as pd where pd.PeerGroup_Main_ID = pm.PeerGroup_ID) as sampleSize, concat(usrCr.First_Name, ' ', usrCr.Last_Name) as CreatedBy, concat(usrUp.First_Name, ' ', usrUp.Last_Name) as ModifiedBy from peergroup_main as pm left outer join userdetails as usrCr on usrCr.UsersID = pm.CreatedBy left outer join userdetails as usrUp on usrUp.UsersID = pm.ModifiedBy Where pm.CreatedBy in (Select distinct userid from users where roleid = 3 and IsActive = 1) and facility_type = $facility_id order by $orderBy";
}

#Administrator
if($userType eq '2'){
	$sql = "select pm.CreatedBy as ownerid, pm.PeerGroup_ID as PeerGroup_ID, pm.Name as Name, pm.isActive as isActive,pm.CreateDate as CreateDate ,pm.AlterDate , pm.Description as Description, (select count(*) from peergroup_details as pd where pd.PeerGroup_Main_ID = pm.PeerGroup_ID) as sampleSize, concat(usrCr.First_Name, ' ', usrCr.Last_Name) as CreatedBy, concat(usrUp.First_Name, ' ', usrUp.Last_Name) as ModifiedBy from peergroup_main as pm left outer join userdetails as usrCr on usrCr.UsersID = pm.CreatedBy left outer join userdetails as usrUp on usrUp.UsersID = pm.ModifiedBy where facility_type = $facility_id order by $orderBy";
}

$sth = $dbh->prepare($sql);
$sth->execute();

print "Content-Type: text/html\n\n";

print <<EOF
<HTML>
<HEAD><TITLE>PEER GROUP</TITLE>
  <link href="/css/peer_group/peer_main.css" rel="stylesheet" type="text/css" />
 <script type="text/javascript" src="/JS/AjaxScripts/prototype.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/effects.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/controls.js"></script>
 <script type="text/javascript" src="/JS/peer_group/peer_main.js"></script>
 <SCRIPT SRC="/JS/sorttable.js"></SCRIPT>
 <SCRIPT SRC="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
 <SCRIPT SRC="/JS/dropmenu.js"></SCRIPT>
 <link rel="stylesheet" href="/css/dropmenu.css" type="text/css" />
</HEAD>
<BODY onload="ShowDetails();setArrow('$tmpOrder')"><!--<a style="display:scroll;position:fixed;left:5px;bottom:5px;" href="#" title="Feedback Please"><img border=0 src="/images/feedback.png"/></a>-->
<FORM NAME = "frmPeerMain" Action="/cgi-bin/PeerActions.pl?facility=$facility" method="post" enctype="multipart/form-data">
<CENTER>
EOF
;
&innerHeader();
print <<EOF

<div id='bodyDiv' style="position:absolute;z-index:100;display:none;background-image:url(/images/background-trans.png);height:90%;width:92%;"><div align='center'  id="displaybox" style="display: none;"></div></div>

    <!--content start-->
<div style="width:885px; background-image: url(/images/content_middle.gif); background-repeat: repeat-x; background-position:top; padding:10px">
	<div style="overflow:scroll; height:200px; vertical-align:top">
	<TABLE align='center' border=1 style="border:solid 1px #b1bed0;color:#0c4f7a;" width="890">
	<tr class='gridHeader'>
		<td>SlNo</td><td><a href='#' onclick="callSort('$acsdesc','Name',1)">Peer group name&nbsp;&nbsp;<span id='pname' class="arrow"></span></a></td><td><a href='#' onclick="callSort('$acsdesc','sampleSize',1)" >Size&nbsp;&nbsp;<span id='size'></span></a></td><td><a href='#' onclick="callSort('$acsdesc','CreateDate',1)">Created On&nbsp;&nbsp;<span id='createdon'></span></a></td><td><a href='#' onclick="callSort('$acsdesc','CreatedBy',1)">Created By&nbsp;&nbsp;<span id='createdby'></span></a></td><td><a href='#' onclick="callSort('$acsdesc','AlterDate',1)">Modified On&nbsp;&nbsp;<span id='modifiedon'></span></a></td><td><a href='#' onclick="callSort('$acsdesc','ModifiedBy',1)">Modified By&nbsp;&nbsp;<span id='modifiedby'></span></a></td>
	</tr>
EOF
;
	$i = 0;
	my ($providerid2);
	my($firstsel);
	$firstsel=0;
	if($sth->rows()>0){
	while ($row1 = $sth->fetchrow_hashref) {
     		$providerid1 = $$row1{PeerGroup_ID};
     		$peername1 = $$row1{Name};
     		$status1 = $$row1{isActive};
     		$createon1 = $$row1{CreateDate};
     		$alterdate1 = $$row1{AlterDate};
     		$Description = $$row1{Description};
     		$sampleSize = $$row1{sampleSize};
     		$class1 = (++$i % 2) ? 'gridrow' : 'gridrow';

  print "<tr class=\"$class1\" ><td>$i</td><td><a href=\"/cgi-bin/PeerDetails.pl?createdBy=$$row1{ownerid}&reload=false&mainID=$providerid1&facility=$facility\" target=\"peerframe\">$peername1</a></td><td>$sampleSize</td><td>$createon1</td><td>$$row1{CreatedBy}</td><td>$alterdate1</td><td>$$row1{ModifiedBy}</td></tr>\n";
   if($i == 1){
   	print "<tr><td><input type=\"hidden\" name=\"firstid\" value=\"$providerid1\">";
   	print "<input type=\"hidden\" name=\"firstCreatedBy\" value=\"$$row1{ownerid}\"></td></tr>";
   }
  }
  }else{
	$sql = "select max(PeerGroup_ID) as PeerGroup_ID,CreatedBy as ownerid  from peergroup_main";
	$sth = $dbh->prepare($sql);
	$sth->execute();
  while ($row1 = $sth->fetchrow_hashref) {
	$providerid2 = $$row1{PeerGroup_ID};
  }
  $firstsel=1;
   print "<tr class=\"$class1\" ><td>$i</td><td><a href=\"/cgi-bin/PeerDetails.pl?createdBy=$$row1{ownerid}&reload=false&mainID=$providerid1&facility=$facility\" target=\"peerframe\"></a></td><td></td><td></td><td></td><td></td><td></td></tr>\n";
  
  print "<tr><td>input type=\"hidden\" name=\"firstid\" value=\"$providerid2\">";
	print "<input type=\"hidden\" name=\"firstCreatedBy\" value=\"$$row1{ownerid}\"></td></tr>";

  }
  print "<tr><td><input type=\"hidden\" name=\"firstsel\" value=\"$firstsel\"></td></tr>";

  print "</table></div><br>\n";
 
  print "<table class=\"gridstyle\" cellpadding=\"5\" cellspacing=\"0\" width=\"875px\">\n";
  print "<tr><td align=\"right\" nowrap>Peer Group &nbsp;</td><td colspan=\"2\"><input type=\"text\" name=\"txtpeername\" value=\"\" size=54 disabled maxLength=\"50\"></td></tr>\n";
  print "<tr><td align=\"right\" nowrap>Criteria &nbsp;</td><td colspan=\"2\"><div id=\"sc\" style='position:static;overflow:auto;height:100px;width:100%;'></div></td></tr>\n";
  print "<tr><td align=\"right\" nowrap>Description &nbsp;</td><td colspan=\"2\"><input type=\"text\" name=\"txtdescription\" value=\"\" size=110  disabled maxLength=\"200\"></td></tr>\n";
  print "<tr><td></td><td colspan=2><table><tr><td><span class=\"button\"><input type=\"button\" name=\"btnCreate\" value=\"Create\" onClick=\"CallCreate()\"></span></td><td><span class=\"button\"><input type=\"button\" name=\"btnEdit\" value=\"Edit\" onClick=\"CallEdit()\" disabled></span></td><td><span class=\"button\"><input type=\"button\" name=\"btnDelete\" value=\"Delete\" onClick=\"CallDelete()\" disabled></span></td><td><span class=\"button\"><input type=\"button\" name=\"btnCancel\" value=\"Cancel\" onClick=\"CallCancel()\" disabled></span></td><td><span class=\"button\"><input type=\"button\" name=\"btnSearch\" value=\"Reports\" onClick=\"CallSearch()\"></span></td><td><span class=\"button\"><input type=\"button\" name=\"btnAdd\" value=\"Add Provider\" onClick=\"CallAdd()\" ></span></td><td><span class=\"button\"><input type=\"button\" name=\"btnSearch\" value=\"Narrow Search\" onClick=\"Callpage()\" ></span></td><td><span class=\"button\"><input type=\"button\" name=\"btnImport\" value=\"Peer Import\" onClick=\"CallImport()\" ></span></td></tr></table></td></tr>\n";
  print "</table><br>\n";
  print "<iframe name=\"peerframe\" width=\"100%\" frameborder=0 cellpadding=0 cellspacing=0 ></iframe>\n";
print "</div>";



print "    <!--content end-->\n";

&TDdoc;

print "</CENTER>\n";
print "<input type=\"hidden\"  name=\"saction\" value=\"\">\n";
print "<input type=\"hidden\" name=\"updId\" value=\"\">\n";
print "<input type=\"hidden\" name=\"narrow_search\" value =\"$narrow_search\" >";
print "<input type=\"hidden\" name=\"narrow_search_module\"  value = \"$narrow_search_module\">";
print "<input type=\"hidden\" name=\"hdnUpd\" value=\"\">\n";
print "<input type=\"hidden\" name=\"hdn_import\" value=\"\">\n";
print "<input type='hidden' name='foiId' value=''>\n";
print "<input type='hidden' name='peername' value='$peername1'>\n";
print "<input type='hidden' name='desc' value=''>\n";
print "<input type='hidden' name='strpids' value=''>\n";
print "<input type='hidden' name='strpnames' value=''>\n";
print "<input type='hidden' name='js_lproviderNum' value=''>\n";
print "<input type='hidden' name='js_lproviderName' value=''>\n";
print "<input type='hidden' name='js_lAddress' value=''>\n";
print "<input type='hidden' name='js_lCity' value=''>\n";
print "<input type='hidden' name='js_lStates' value=''>\n";
print "<input type='hidden' name='js_lZip' value=''>\n";
print "<input type='hidden' name='js_lCountry' value=''>\n";
print "<input type='hidden' name='js_lLocation' value=''>\n";
print "<input type='hidden' name='js_lRadius' value=''>\n";
print "<input type='hidden' name='js_lMin_ftes' value=''>\n";
print "<input type='hidden' name='js_lMax_ftes' value=''>\n";
print "<input type='hidden' name='js_lMin_beds' value=''>\n";
print "<input type='hidden' name='js_lMax_beds' value=''>\n";
print "<input type='hidden' name='js_lToc' value=''>\n";
print "<input type='hidden' name='js_lHosp_Type' value=''>\n";
print "<input type='hidden' name='js_lSystem' value=''>\n";
print "<input type='hidden' name='js_lMsa' value=''>\n";
print "<input type='hidden' name='js_lnfp' value=''>\n";
print "<input type='hidden' name='js_lgovt' value=''>\n";
print "<input type='hidden' name='js_lTeachHosp' value=''>\n";
print "<input type='hidden' name='js_lGeoArea' value=''>\n";
print "<input type='hidden' name='js_lMin_net_income' value=''>\n";
print "<input type='hidden' name='js_lMax_net_income' value=''>\n";
print "<input type='hidden' name='js_lMin_zscore' value=''>\n";
print "<input type='hidden' name='js_lMax_zscore' value=''>\n";
print "<input type='hidden' name='js_lMin_Opr_Margin' value=''>\n";
print "<input type='hidden' name='js_lMax_Opr_Margin' value=''>\n";
print "<input type='hidden' name='facility' value = '$facility'>\n";
print "<input type='hidden' name='userID' value = '$userID'>\n";
print "<input type='hidden' name='userType' value = '$userType'>\n";

print "</FORM>\n";
print "</BODY>\n";
qq{</HTML>
};

#--------------------------------------------------------------------------------------
sub sessionExpire
{

print "Content-Type: text/html\n\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};
&headerScript();
print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}
#-----------------------------------------------------------------------------


sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
<BR>
<BR>
};
&innerHeader();
print qq{
    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}
