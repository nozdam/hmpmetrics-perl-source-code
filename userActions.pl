#!/usr/bin/perl

use CGI;
use DBI;

use CGI::Session;
do "common_header.pl";
do "audit.pl";
my $cgi = new CGI;
my $userName = $cgi->param('txtUserName'); 
my $password = $cgi->param('txtPassword');
my $securityQry = $cgi->param('txtSecurityQry');
my $securityOtherQry = $cgi->param('txtSecurityOtherQry');  
my $securityAns = $cgi->param('txtSecurityAns');
my $txtUserType = $cgi->param('txtUserType'); 
my $saction = $cgi->param('saction');

my $firstName = $cgi->param('txtFirstName');
my $middleName = $cgi->param('txtMiddleName');
my $lastName = $cgi->param('txtLastName');
my $emailID = $cgi->param('txtEmail');
my $Address1 = $cgi->param('txtAddress1');
my $Address2 = $cgi->param('txtAddress2');
my $Address3 = $cgi->param('txtAddress3');
my $city = $cgi->param('txtCity');
my $state = $cgi->param('txtState');
my $zip = $cgi->param('txtZip');
my $title = $cgi->param('txtNameTitle');
my $phone1 = $cgi->param('txtTelephone1');
my $phone2 = $cgi->param('txtTelephone2');

my $nameSuffix = $cgi->param('txtNameSuffix');
my $company_name = $cgi->param('txtCompany');

my $str = NULL;

if(!$zip)
{
	$zip = NULL;
}

# Database 

my $dbh;
my ($sql, $sth);

# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my $defaultYear= $ini_value{'DEFAULT_YEAR'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

my ($userID);

if ($saction eq 'Register')
{
	# my ($roleID); -- not required

	$sql = "select UserID from Users where UserName = '$userName' ";
	$sth = $dbh->prepare($sql);			
	$sth->execute();
	if($sth->rows()>0)
	{	
		$str ="<script>alert('User already exists');window.parent.location.href='/cgi-bin/userView.pl';</script>";
	}
	else
	{
		# Role ID will be assigned from combobox in creat page
		
		#$sql = "select RoleID from role where RoleName = 'User'";
		#$sth = $dbh->prepare($sql);			
		#$sth->execute();
		#@row = $sth->fetchrow_array();
		#$roleID = @row[0];


		$sql = "Insert into Users (UserName, Password, IsActive, CreatedDate, RoleID, Security_Qry_ID, Security_Other_Qry, Security_Ans) values ('$userName',AES_ENCRYPT('$password','bs'), 1, CURRENT_DATE, $txtUserType, $securityQry, '$securityOtherQry', '$securityAns')";
		$sth = $dbh->prepare($sql);			
		$sth->execute();


		$sql = "select UserID from Users where UserName = '$userName' and Password = AES_ENCRYPT('$password','bs') and CreatedDate = CURRENT_DATE ";
		$sth = $dbh->prepare($sql);			
		$sth->execute();
		@row = $sth->fetchrow_array();
		$userID = @row[0];


		$sql = "Insert into userdetails (UsersID, First_Name, Middle_Name, Last_Name, emailID, Address1, Address2, Address3, city, State, zip, Title, Phone1, Phone2, CreatedDate, Suffix_Title,company_name) values ($userID, '$firstName', '$middleName', '$lastName', '$emailID', '$Address1', '$Address2', '$Address3', '$city', '$state', $zip, '$title', '$phone1', '$phone2', CURRENT_DATE, '$nameSuffix','$company_name')";
		$sth = $dbh->prepare($sql);			
		$sth->execute();
		
		&insert("Create","New user registered","","User Management");
		$str ="<script>alert('User created successfully');window.parent.location.href='/cgi-bin/userView.pl';</script>";
	}	
	
	
}

if($saction eq 'update'){

	my $uid = $cgi->param('txtUser');
	my $user_name;
	my $usql = "select UserName from users where UserID = $uid";
	$sth = $dbh->prepare($usql);			
	$sth->execute();

	
	while ($row = $sth->fetchrow_hashref) {
	
		$user_name=$$row{UserName};
		
	}
	&update("Update",$user_name."user data modified","","","User Management");	
	
	$sql = "update users set UserName = '$userName' , Password = AES_ENCRYPT('$password','bs'),RoleID = $txtUserType , ModifiedDate = CURRENT_DATE where UserID = $uid";
	$sth = $dbh->prepare($sql);			
	$sth->execute();
	

	
	$str ="<script>alert('User updated successfully');window.parent.location.href='/cgi-bin/userView.pl';</script>";
	
}

if ($saction eq 'delete'){
	my $uid = $cgi->param('uid');
	my $status = $cgi->param('status');
	open(J, ">data.txt");
	print J $status;
	close J;
	if($status == 0)
	{
	
		&update("Update","user view page","Activate","Inactivate","User Management");	
	}else
	{
		&update("Update","user view page","Inactivate","Activate","User Management");
	}
	$sql = "update users set IsActive = $status ,ModifiedDate = CURRENT_DATE where users.UserID = $uid";
	$sth = $dbh->prepare($sql);			
	$sth->execute();
	
	$str ="<script>alert('User status updated successfully');window.parent.location.href='/cgi-bin/userView.pl';</script>";	
}


my $session = new CGI::Session("driver:File", undef, {Directory=>"/tmp"});

my $sid = $session->id();

my $cookie = $cgi->cookie(CGISESSID => $session->id);

#$session->param("firstName", $firstName);
#$session->param("lastName", $lastName);
#$session->param("userID", $userID);
#$session->expire('+1h');

print $cgi->header(-cookie=>$cookie);

print <<EOF
<HTML>
<HEAD><TITLE>User Management</TITLE>
</HEAD>
<BODY>
<FORM name="frmUserActions">
<input type="hidden" name="duplicate" value="">
EOF
;    
print "$str";
print qq{
</FORM>
</BODY>
</HTML>
};


#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach my $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}

#------------------------------------------------------------------------------

