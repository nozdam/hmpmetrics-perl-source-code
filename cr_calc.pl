#!/usr/bin/perl
#----------------------------------------------------------------------------
#  program: cr_calc.pl
#  author: Steve Spicer
#  Written: Sun Sep 28 23:24:54 CDT 2008
#
#  Research Data Assistance Center
#  http://www.resdac.umn.edu/Tools/TBs/TN-005-revised.asp
#----------------------------------------------------------------------------

use strict;
use CGI;
use DBI;
use CGI::Session;


do "common_header.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");

if(!$userID)
{
	&sessionExpire;
}


my $year      = $cgi->param('year');
my $provider  = $cgi->param('provider'); 


my %definition;

$definition{'Quick Ratio'} = qq(
<B>An indicator of a company's short-term liquidity.</B><BR>
The quick ratio measures a company's ability to meet<BR>
its short-term obligations with its most liquid assets.<BR>
The higher the quick ratio, the better the position of the company.  <BR>
<BR>
The quick ratio is calculated as:   Current Assets - Inventories / Current Liabilities<BR>
<BR>
<BR>
Also known as the "acid-test ratio" or the "quick assets ratio".<BR>
);


$definition{'Operating Margin'} = qq(
<B>Used to measure a company's pricing strategy and operating efficiency</B>.<BR>
<BR>
Operating Margin = Operating Income / Net Sales<BR>
<BR>
Operating margin is a measurement of what proportion of a company's revenue<BR>
is left over after paying for variable costs of production such as wages,<BR>
raw materials, etc. A healthy operating margin is required for a company <BR>
to be able to pay for its fixed costs, such as interest on debt.<BR>
<BR>
Also known as "operating profit margin" or "net profit margin". <BR>
);


my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

unless ($year) {
   $year = $ini_value{DEFAULT_YEAR};
}

my ($selected, $yyyy);

my $year_options = '';

foreach $yyyy (split /,/,$ini_value{HCRIS_YEARS}) {
    $selected = ($yyyy eq $year) ? 'SELECTED' : '';
    $year_options .= "   <OPTION VALUE=\"$yyyy\" $selected>$yyyy</OPTION>\n";
}

if ($provider) {
   $provider = uc($provider);
   unless ($provider =~ m/[A-Z0-9]{6}/) {
       &display_error('PROVIDER NUMBER MUST BE 6 CHARACTERS');
       exit(0);
   }
}
else {
   &display_form();
   exit(0);
}

#----------------------------------------------------------------------------

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my $dbh;

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error('ERROR connecting to database', $DBI::errstr);
   exit(1);
}

my %hash_value;

my $hospital_name                = &get_value_by_address($year, $provider, 'S200000:00200:0100');
my $hospital_address             = &get_value_by_address($year, $provider, 'S200000:00100:0100');
my $hospital_city                = &get_value_by_address($year, $provider, 'S200000:00101:0100');
my $hospital_state               = &get_value_by_address($year, $provider, 'S200000:00101:0200');
my $hospital_zip                 = &get_value_by_address($year, $provider, 'S200000:00101:0300');

my $net_income                   = &get_value_by_address($year, $provider, 'G300000:03100:0100');  # or loss
my $interest_expense             = &get_value_by_address($year, $provider, 'A700003:00500:1100');  
my $depreciation_expense         = &get_value_by_address($year, $provider, 'A700003:00500:0900');  
my $lease_cost                   = &get_value_by_address($year, $provider, 'A700003:00500:1000');  
my $operating_revenue            = &get_value_by_address($year, $provider, 'G300000:00300:0100');
my $operating_expense            = &get_value_by_address($year, $provider, 'G300000:00400:0100');
my $non_operating_revenue        = &get_value_by_address($year, $provider, 'G300000:02500:0100');
my $salary_expense               = &get_value_by_address($year, $provider, 'A000000:10100:0100');

my $contract_labor               = &get_value_by_address($year, $provider, 'S300002:00900:0300');
   $contract_labor              += &get_value_by_address($year, $provider, 'S300002:00901:0300');
   $contract_labor              += &get_value_by_address($year, $provider, 'S300002:00902:0300');
   $contract_labor              += &get_value_by_address($year, $provider, 'S300002:01000:0300');
   $contract_labor              += &get_value_by_address($year, $provider, 'S300002:01001:0300');
   $contract_labor              += &get_value_by_address($year, $provider, 'S300002:01100:0300');
   $contract_labor              += &get_value_by_address($year, $provider, 'S300002:01200:0300');
   $contract_labor              += &get_value_by_address($year, $provider, 'S300002:01201:0300');

my $fringe_benefits              = &get_value_by_address($year, $provider, 'A000000:00500:0200');  # i question

my $total_assets                 = &get_value_by_address($year, $provider, 'G000000:02700:0100');
my $total_liabilities            = &get_value_by_address($year, $provider, 'G000000:04300:0100');

my $total_current_assets         = &get_value_by_address($year, $provider, 'G000000:01100:0100');
my $total_current_liabilities    = &get_value_by_address($year, $provider, 'G000000:03600:0100');
my $inventory                    = &get_value_by_address($year, $provider, 'G000000:00700:0100');
my $cash_on_hand                 = &get_value_by_address($year, $provider, 'G000000:00100:0100');
my $market_securities            = &get_value_by_address($year, $provider, 'G000000:00200:0100');
my $investments                  = &get_value_by_address($year, $provider, 'G000000:02200:0100');
my $accounts_receivable          = &get_value_by_address($year, $provider, 'G000000:00400:0100');
my $allowance_for_uncollectable  = &get_value_by_address($year, $provider, 'G000000:00600:0100');
my $notes_receivable             = &get_value_by_address($year, $provider, 'G000000:00300:0100');
my $other_receivables            = &get_value_by_address($year, $provider, 'G000000:00500:0100');
my $other_expenses               = &get_value_by_address($year, $provider, 'G300000:03000:0100');

my $long_term_liabilities        = &get_value_by_address($year, $provider, 'G000000:04200:0100');

my $accumulated_depreciation    = &get_value_by_address($year, $provider, 'G000000:01201:0100');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01201:0100');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01301:0100');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01401:0100');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01501:0100');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01601:0100');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01701:0100');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01801:0100');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01901:0100');

   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01201:0200');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01301:0200');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01401:0200');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01501:0200');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01601:0200');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01701:0200');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01801:0200');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01901:0200');

   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01201:0300');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01301:0300');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01401:0300');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01501:0300');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01601:0300');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01701:0300');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01801:0300');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01901:0300');

   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01201:0400');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01301:0400');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01401:0400');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01501:0400');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01601:0400');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01701:0400');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01801:0400');
   $accumulated_depreciation   += &get_value_by_address($year, $provider, 'G000000:01901:0400');

$dbh->disconnect();


my @error_list;


unless ($operating_expense) {
   push @error_list, 'OPERATING REVENUE IS ZERO OR UNDEFINED';
}

unless ($total_assets) {
   push @error_list, 'TOTAL ASSETS IS ZERO OR UNDEFINED';
}

unless ($total_current_liabilities) {
   push @error_list, 'TOTAL CURRENT LIABILITIES IS ZERO OR UNDEFINED';
}

my $n = @error_list;

if ($n > 0) {
   &display_error(@error_list);
   exit(0);
}


my $ebitdar               = $net_income + $interest_expense + $depreciation_expense + $lease_cost;
my $operating_margin      = ($operating_revenue - $operating_expense) / $operating_revenue * 100;
my $excess_margin         = ($operating_revenue - $operating_expense + $non_operating_revenue) / ($operating_revenue + $non_operating_revenue) * 100;
my $personnel_exp_pct_tor = ($salary_expense + $contract_labor + $fringe_benefits) / $operating_revenue * 100;
my $return_on_equity      = $net_income / ($total_assets - $total_liabilities) * 100;
my $return_on_assets      = ($net_income / $total_assets) * 100;
my $current_ratio         = $total_current_assets / $total_current_liabilities;
my $quick_ratio           = ($total_current_assets - $inventory) / $total_current_liabilities;
my $days_cash_onhand      = ($cash_on_hand + $market_securities) / (($operating_expense - $depreciation_expense) / 365);
my $days_cash_onhand_all  = ($cash_on_hand + $market_securities + $investments) / (($operating_expense - $depreciation_expense) / 365);
my $days_in_patient_ar    = ($accounts_receivable - $allowance_for_uncollectable) / ($operating_revenue / 365);
my $days_net_total_receivables   = ($accounts_receivable + $notes_receivable + $other_receivables - $allowance_for_uncollectable) / ($operating_revenue / 365);
my $average_payment_period   = $total_current_liabilities / (($operating_expense + $other_expenses - $depreciation_expense) / 365);

my $inventory_turnover;

if ($inventory) {
   $inventory_turnover    = ($operating_revenue + $non_operating_revenue) / $inventory;
}

my $total_asset_turnover;

if ($total_assets) {
   $total_asset_turnover  = ($operating_revenue + $non_operating_revenue) / $total_assets;
}

my $lt_debt_to_net_assets = $long_term_liabilities / ($total_assets - $total_liabilities);
my $debt_to_net_assets    = $total_liabilities / ($total_assets - $total_liabilities);

if ($depreciation_expense) {
   my $average_age_of_plant  = $accumulated_depreciation / $depreciation_expense;
   $hash_value{'Average Age of Plant'}      = sprintf("%.2f", $average_age_of_plant);
}


$hash_value{'EBIDAR'}                       = &comify($ebitdar);
$hash_value{'Operating Margin'}             = sprintf("%.2f", $operating_margin);
$hash_value{'Excess Margin'}                = sprintf("%.2f", $excess_margin);
$hash_value{'Personnel Expense as Percent of Total Operating Revenue'} = sprintf("%.2f", $personnel_exp_pct_tor);
$hash_value{'Percent Return On Equity'}     = sprintf("%.2f", $return_on_equity);
$hash_value{'Percent Return On Assets'}     = sprintf("%.2f", $return_on_assets);
$hash_value{'Current Ratio'}                = sprintf("%.2f", $current_ratio);
$hash_value{'Quick Ratio'}                  = sprintf("%.2f", $quick_ratio);
$hash_value{'Days Cash Onhand'}             = sprintf("%.2f", $days_cash_onhand);
$hash_value{'Days Cash Onhand All'}         = sprintf("%.2f", $days_cash_onhand_all);
$hash_value{'Days In Patient Receivables'}  = sprintf("%.2f", $days_in_patient_ar);
$hash_value{'Days Net Total Receivables'}   = sprintf("%.2f", $days_net_total_receivables);
$hash_value{'Average Payment Period'}       = sprintf("%.2f", $average_payment_period);
$hash_value{'Inventory Turnover'}           = sprintf("%.2f", $inventory_turnover);
$hash_value{'Total Asset Turnover'}         = sprintf("%.2f", $total_asset_turnover);
$hash_value{'Long Term Debt to Net Assets'} = sprintf("%.2f", $lt_debt_to_net_assets);
$hash_value{'Total Debt to Net Assets'}     = sprintf("%.2f", $debt_to_net_assets);

&display_results();

exit(0);

#----------------------------------------------------------------------------

sub display_results
{
   print "Content-Type: text/html\n\n";

   print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
<HEAD><TITLE>COST REPORT EXPLORER</TITLE>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon">
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
<link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
    <!--content start-->
    
    
        <table cellpadding="0" cellspacing="0" width="885px">
            <tr>
                <td valign="top">
                    <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px">
<div class="pageheader">FINANCIAL CALCULATIONS</div>

<table cellpadding="0" cellspacing="0" width="100%">
 <TR>
   	<TD style="padding-bottom:10px">   
        <B>
        $provider<BR>
        $hospital_name</B><BR>
        $hospital_address<BR>
        $hospital_city, $hospital_state  $hospital_zip
        </TD>
   	<TD ALIGN="right" valign="bottom" style="padding-bottom:10px">
   		<FORM METHOD="GET" ACTION="/cgi-bin/cr_calc.pl">
		             COST REPORT
		             <SELECT NAME="year" onChange="this.form.submit()">
		                $year_options
		             </SELECT>
		             <INPUT TYPE="hidden" name="provider" value="$provider">
        </FORM>
   	</TD>
   </TR>   
</table>

<TABLE  ALIGN="CENTER" BORDER="0" CELLPADDING="2" WIDTH="100%" class="gridstyle">
   
};

my ($key, $value);

foreach $key (sort keys %hash_value) {
    $value = $hash_value{$key};
    print "   <TR>\n";
    print "       <TD ALIGN=\"LEFT\" class=\"gridrow\" style=\"border:solid 1px #b1bed0;\">\n";
    print "          <A CLASS=\"poppholder\" HEREF=\"#\">\n";
    print "          $key\n";
    print "          <SPAN CLASS=\"popupinfo\">$definition{$key}</SPAN>\n";
    print "          </A>\n";
    print "       </TD>\n";
    print "       <TD ALIGN=\"RIGHT\" class=\"gridrow\" style=\"border:solid 1px #b1bed0;\">$value</TD>\n";
    print "   </TR>\n";
}

print qq{
</TABLE>
</td>
            <td valign="top">
                <img src="/images/content_right.gif" /></td>
        </tr>
    </table>
    <!--content end-->
</CENTER>
</BODY>
</HTML>
   };

   return(0);
}


#----------------------------------------------------------------------------


sub get_value_by_address
{
   my ($year, $provider, $address) = @_;

   my ($worksheet, $row, $col) = split /[-:\|]/,$address;

   my ($sth, $sql);

   $sql = qq/
    SELECT CR_VALUE         
      FROM CR_ALL_RPT AA
 LEFT JOIN CR_ALL_DATA BB
        ON AA.RPT_REC_NUM = BB.CR_REC_NUM
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
     WHERE PRVDR_NUM     = ?
	 AND RPT_YEAR	= $year
      LIMIT 1
   /;

   unless ($sth = $dbh->prepare($sql)) {
       &display_error("ERROR preparing SQL query", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   $sth->execute($worksheet, $row, $col, $provider);

   my ($value) = $sth->fetchrow_array;

   $value = 0 unless(defined $value);

   return($value);
}


#----------------------------------------------------------------------------

sub display_error
{
   my @list = @_;

   my ($string, $s);

   foreach $s (@list) {
      $string .= "$s<BR>\n";
   }

   print "Content-Type: text/html\n\n";

   print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
<HEAD><TITLE>ERROR</TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
<link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>

    <!--content start-->
    
    
    <table cellpadding="0" cellspacing="0" width="885px">
    	<tr><td align="left">Welcome <b>
    		};
    
    		print $firstName . " " . $lastName;
    		print qq{
    		</b></td>  
    		<td align="right">
    		<a href="/cgi-bin/login.pl?saction=signOut">Sign Out</a>
    		</td>
    		</tr>
    </table>
    
        <table cellpadding="0" cellspacing="0" width="885px">
            <tr>
                <td valign="top">
                    <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px">
<FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<TABLE CLASS="error" WIDTH="600">
   <TR>
      <TD ALIGN="CENTER">
          <FONT SIZE="4">
          <BR>
          $string
          <BR>
          </FONT>
      </TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</td>
            <td valign="top">
                <img src="/images/content_right.gif" /></td>
        </tr>
    </table>
    <!--content end-->
</BODY>
</HTML>
   };

   return(0);
}


#----------------------------------------------------------------------------

sub comify
{
  local($_) = shift;
  1 while s/^(-?\d+)(\d{3})/$1,$2/;
  return($_);
}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}


#--------------------------------------------------------------------------------------
sub sessionExpire
{

print "Content-Type: text/html\n\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}


#------------------------------------------------------------------------------

