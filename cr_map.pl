#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: cr_map.pl
#   author: Steve Spicer
#  Written: Sun Sep 21 12:43:50 CDT 2008
#     uses: google map api
#----------------------------------------------------------------------------

use strict;
use CGI;
use CGI::Session;

do "pagefooter.pl";
do "common_header.pl";
print "Content-Type: text/html\n\n";
&headerScript();
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");

if(!$userID)
{
	&sessionExpire;
}


my $lat    = $cgi->param('lat');    
my $lon    = $cgi->param('lon');    
my $desc   = $cgi->param('desc');

my @error_list;

unless ($lat) {
   push @error_list, 'NO LATITUDE PROVIDED';
}

unless ($lon) {
   push @error_list, 'NO LONGITUDE PROVIDED';
}

unless ($lon >= -180 and $lon <= 180) {
   push @error_list, 'LATITUDE [$lon] OUT OF RANGE';
}

unless ($lat >= -90 and $lat <= 90) {
   push @error_list, 'LONGITUDE [$lat] OUT OF RANGE';
}

my $n = @error_list;

if ($n) {
   &display_error(@error_list);
   exit(0);
}

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error('CAN\'T READ milo.ini');
   exit(1);
}


&display_map($lat,$lon,$desc);

exit(0);


#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my ($s, $string);

foreach $s (@list) {
   $string .= "$s<BR>\n";
}

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};

&innerHeader();
print qq{

<BR>
<BR>
<FONT SIZE="5" COLOR="RED">
<B>ERROR:</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}

#----------------------------------------------------------------------------

sub display_map
{
   my ($lat, $lon, $desc) = @_;

   my $key = $ini_value{GMAP_KEY};

   # my $key = "ABQIAAAAj8H1vjJYe2dD9su_U4y6_BQTK2UG7OGS2KVJMk5-OBCR7u_zShSQoOrOcYcJqPaQ9QGMTZ2Q32CfNQ";


   print <<EOF
<HTML>
<HEAD><TITLE>GOOGLE MAPS</TITLE>

<SCRIPT SRC="http://maps.google.com/maps?file=api&v=2&key=$key" type="text/javascript"></script>
<script language="javascript" src="/JS/search/cr_map.js" ></script>
</HEAD>
<BODY onload="loadthemap($lat,$lon,'$desc')"  onunload="GUnload()">
EOF
;
&innerHeader();
print <<EOF
<!--content start-->
	
        <table cellpadding="0" cellspacing="0" width="885px" align="center">
            <tr>
                <td valign="top">
                    <img src="/images/content_left.gif" /></td>
            <td valign="top" style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px">
<Div class="pageheader">$desc</div>
LATTITUDE=$lat / LONGITUDE=$lon<br><br>
<div id="map" style="width: 100%; height: 600px; border:1px solid #006600;"></div>
</td>
           <td valign="top">
                <img src="/images/content_right.gif" /></td>
        </tr>
    </table>
    <!--content end-->

<CENTER>
<TABLE CLASS="dentry" BORDER="1" CELLSPACING="0" WIDTH="800" style="display:none">
  <TR>
    <TD COLSPAN="2" ALIGN="CENTER">
      PUT CONTENT HERE <BR>
      <FONT SIZE="5"><B</B></FONT><BR>
      <BR>
    </TD>
  </TR>
  <TR>
    <TD ALIGN="CENTER">
        LEFT CONTENT 
    </TD>
    <TD ALIGN="CENTER">
        RIGHT<BR>
        
        <BR>
        <BR>
    </TD>
  </TR>
</TABLE>
<BR>
</BODY>
</HTML>
EOF
;
&TDdoc;
}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}


#--------------------------------------------------------------------------------------
sub sessionExpire
{


print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}


#------------------------------------------------------------------------------

