#!/usr/bin/perl

use CGI;
use DBI;
use Date::Calc qw(Delta_Days);
do "common_header.pl";

my $cgi = new CGI;

use CGI::Session;
use PDF::Create;
do "audit.pl";

my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");

if(!$userID)
{
	&sessionExpire;
}


my $spreadHead = $cgi->param('spreadHead');
my $spreadData = $cgi->param('spreadData');
my $chkBoxVal = $cgi->param('chkBoxChecked');

# Database 

my $dbh;
my ($sql, $sth);


# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my $defaultYear= $ini_value{'DEFAULT_YEAR'};




#------------------------------------------------------------------------------------------



#print "Content-type: application/pdf\n";
#print "Content-Disposition: attachment; filename=cost_report_batch.pdf\n";
#print "Cache-Control: no-cache\n";
#print "Pragma: no-cache\n\n";

print CGI::header( -type => 'application/x-pdf', -attachment => 'cost_report_batch.pdf' );

my $pdf = new PDF::Create('filename'     => '-', # Stdout
                            'Author'       => 'Milo',
                            'Title'        => 'Standard Reports',
                            'CreationDate' => [ localtime ],
                           );

# add a A4 sized page
my $root = $pdf->new_page('MediaBox' => $pdf->get_page_size('A4'));

# Add a page which inherits its attributes from $root
my $pageFirst = $root->new_page;
my $page = $root->new_page;

# Prepare 2 fonts
my $f1 = $pdf->font('Subtype'  => 'Type1',
	      'Encoding' => 'WinAnsiEncoding',
	      'BaseFont' => 'Helvetica');
my $f2 = $pdf->font('Subtype'  => 'Type1',
	      'Encoding' => 'WinAnsiEncoding',
	      'BaseFont' => 'Helvetica-Bold');

my $now = localtime();
my $img = $pdf->image("C:/Program Files/Apache Software Foundation/Apache2.2/htdocs/images/HMP_logo.gif");

$pageFirst->image('image' => $img, 'xpos' => 10, 'ypos'=> 800, 'xscale' => 0.5, yscale => 0.5);
&insert("Create","PDF generated","","Standard Report");
$pageFirst->stringc($f2, 20, 300, 650, "Healthcare Management Partners");
$pageFirst->stringc($f2, 20, 300, 600, "Medicare Cost Report Data extract");
$pageFirst->stringc($f1, 15, 300, 550, "GENERATED: $now");
my $col = 0;
my $row = 0;

$col = 10;
my $i = 0;
my @colLen = {0,0,0,0,0};
my $colLenCount = 1;

#finding the colum max length. 
foreach $s (split /\~/,$spreadHead)
{
	if(!($s eq "NoVal"))

	{
		foreach $c (split /\~/,$chkBoxVal)
		{
			if($i eq $c)
			{
				if(length($s) > $colLen[$colLenCount])
				{
					$colLen[$colLenCount] = length($s)
				}				
				++$colLenCount;
			}
		}
		++$i;
	}
}	

foreach $data (split /\:/,$spreadData)
{		
	$i = 0;	
	$colLenCount = 1;
	foreach $s (split /\~/,$data)
	{
		if(!($s eq "NoVal"))
		{
			foreach $c (split /\~/,$chkBoxVal)
			{
				if($i eq $c)
				{
					if(length($s) > $colLen[$colLenCount])
					{
						$colLen[$colLenCount] = length($s)
					}				
					++$colLenCount;
				}
			}	
			++$i;
		}
	}		
}

#printing the data into pdf file. 
$i = 0;	
$colLenCount = 1;
my $headerValues;
foreach $s (split /\~/,$spreadHead)
{
	if(!($s eq "NoVal"))

	{
		foreach $c (split /\~/,$chkBoxVal)
		{
			if($i eq $c)
			{				
				$page->string($f1, 10, $col, 790, $s);
				$col = $col + ($colLen[$colLenCount] * 6);
				++$colLenCount;
				$headerValues = $headerValues . $s . "~";
			}
		}			
		++$i;
	}
	
}

$page->line(0, 785, 612, 785);
$row = 770;
my $pageNumber = 1;

$page->image('image' => $img, 'xpos' => 10, 'ypos'=> 800, 'xscale' => 0.5, yscale => 0.5);

foreach $data (split /\:/,$spreadData)
{	
	$col = 10;
	$i = 0;	
	$colLenCount = 1;
	foreach $s (split /\~/,$data)
	{
		if(!($s eq "NoVal"))
		{
			foreach $c (split /\~/,$chkBoxVal)
			{
				if($i eq $c)
				{
					$page->string($f1, 8, $col, $row, $s);
					$col = $col + ($colLen[$colLenCount] * 6);						
					++$colLenCount;
				}
			}	
			++$i;
		}
	}	
	$row = $row - 20;
	if ($row <= 60)
	{
		$page->line(0, 55, 612, 55);
		$page->stringc($f1, 8, 100, 45, $now);
		$page->stringc($f1, 8, 550, 45, "Page $pageNumber");
		$page = $root->new_page;
		$page->image('image' => $img, 'xpos' => 10, 'ypos'=> 800, 'xscale' => 0.5, yscale => 0.5);
		$colLenCount = 1;
		$col = 10;
		foreach $hed (split /\~/,$headerValues)
		{
			$page->string($f1, 10, $col, 790, $hed);
			$col = $col + ($colLen[$colLenCount] * 6);
			++$colLenCount;
		}
		$page->line(0, 785, 612, 785);
		$row = 770;		
		++$pageNumber;
	}	
}
$page->line(0, 55, 612, 55);
$page->stringc($f1, 8, 100, 45, $now);
$page->stringc($f1, 8, 550, 45, "Page $pageNumber");
		
#$page->string($f1, 10, 0, 792, 'by John Doe <john.doe@example.com>');

# Add the missing PDF objects and a the footer then close the file
$pdf->close;





#--------------------------------------------------------------------------------------

sub sessionExpire
{

print "Content-Type: text/html\n\n";

print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#------------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}
print "Content-Type: text/html\n\n";
&headerScript();
print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
<BR>
<BR>
<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/cgi-bin/index.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/cr_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="/cgi-bin/cr_batchx.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="/cgi-bin/snf_batch1.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="/cgi-bin/standard_reports.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Standard Reports</div>">
			        <img src="/images/stardardreport_inner_icon.gif" onmouseover="src='/images/stardardreport_inner_icon_hover.gif';"
                                    onmouseout="src='/images/stardardreport_inner_icon.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 102px" align="Center">
                            <a href="/cgi-bin/PeerMain.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
			                                    <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->
    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}
