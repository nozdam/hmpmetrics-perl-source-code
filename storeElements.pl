#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: storeElements.pl
#  author: Jagadish.M
#----------------------------------------------------------------------------

use strict;
use Spreadsheet::WriteExcel;
use Date::Calc qw(Delta_Days);
use CGI;
use DBI;

do "common_header.pl";
print "Content-Type: text/html\n\n";
&headerScript();
use CGI::Session;

my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $userType = $session->param("role");
my $facility = $cgi->param("facility");
my $str = "";


my $elementname = $cgi->param("txtelementname");
my $dataElementIds = $cgi->param("dataElementIds");

my $len = length ($dataElementIds);

$dataElementIds= substr($dataElementIds,1,$len);

my @arrElements = split(/,/,$dataElementIds);


if(!$userID)
{
	&sessionExpire;
}

my $start_time = time;

my $remote_user  = $ENV{'REMOTE_USER'};

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};

my ($dbh, $sql, $sth,$qry,$facility_id,$row);
my ($sql1, $sth1);
my ($sql2, $sth2);
my ($sql3, $sth3);

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error('ERROR connecting to database', $DBI::errstr);
   exit(1);
}

$sql = "select facility_id from tbl_facility_type where facility_name = '$facility'";
$sth = $dbh->prepare($sql);
$sth->execute();
	  while ($row = $sth->fetchrow_hashref) {
	$facility_id = $$row{facility_id};
}
my $elem;
if($elementname){
	# Check for duplicate element name.
	$sql3 = "select * from elements where Name = '$elementname' and facility_type =$facility_id";
	$sth3 = $dbh->prepare($sql3);
	$sth3->execute();

	if($sth3->rows()>0){
		$str ="";
	}else{

		#First insert the element table data

		$sql = "insert into elements(Name,CreatedOn,Createdby,CancelYN,facility_type) values('$elementname',CURRENT_DATE,$userID,'1',$facility_id)";
		$sth = $dbh->prepare($sql);
		$sth->execute();

		#Select the elements ID insert to insert in elementdetails

		$sql1 = "select element_id from elements where Name = '$elementname' and facility_type =$facility_id";
		$sth1 = $dbh->prepare($sql1);
		$sth1->execute();

		while(my $record = $sth1->fetchrow_hashref){
			my $elementID = $$record{element_id};
				foreach $elem (@arrElements) {
					$sql2 = "insert into elementdetails(element_id,standard_reportdetailst_id) values($elementID,$elem)";
					$sth2 = $dbh->prepare($sql2);
					$sth2->execute();
				}
										}
	   $str ="<script>alert('Element saved successfully.')</script>";
	}
}



print qq{
<html>
<head>
</head>
	<body>
		$str;
	</body>
</html>
};

#-----------------------------------------------------------------------------


sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#--------------------------------------------------------------------------------------
sub sessionExpire
{

print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>

};

print qq{
</body>
</html>
};
}
#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach my $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}

#------------------------------------------------------------------------------
}