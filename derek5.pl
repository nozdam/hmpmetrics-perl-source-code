#!/usr/bin/perl -w

#--------------------------------------------------------------------
# Program: derek5.pl
#  Author: Steve Spicer
# Written: Mon Dec  1 20:56:42 CST 2008
# Revised: 
#   Notes:
# Depends: DBI.pm, CGI.pm
#--------------------------------------------------------------------

use strict;
use DBI;
use CGI;

my $cgi = new CGI;

my $county_cd  = $cgi->param('county_cd');
my $state_cd   = $cgi->param('state_cd');

my $start_time = time;

my %ini_value;

unless (&read_inifile("milo.ini")) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{"DB_TYPE"};
my $dblogin    = $ini_value{"DB_LOGIN"};
my $dbpasswd   = $ini_value{"DB_PASSWORD"};
my $dbname     = $ini_value{"DB_NAME"};
my $dbserver   = $ini_value{"DB_SERVER"};

my ($sql, $dbh, $sth);

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

$sql = qq{
   SELECT SSA_COUNTY, SSA_STATE
     FROM SSA_COUNTYCODES
    WHERE SSA_STATE_CODE = ?
      AND SSA_CNTY_CD = ?
};

unless ($sth = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL query:",  $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute($state_cd, $county_cd)) {
   &display_error("ERROR executing SQL query:",  $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

my ($title_county, $title_state) = $sth->fetchrow_array;

$sth->finish();

#----------------------------------------------------------------------------

#      AND DRG_CD   = 462

$sql = qq{
   SELECT DRG_CD, DRG_DESC, COUNT(*), SUM(PMT_AMT) AS AMT
     FROM LDSM_2007
LEFT JOIN DRG_TABLE ON DRG_CD = DRG_CODE
    WHERE STATE_CD = ?
      AND CNTY_CD  = ?
      AND SGMT_NUM = 1
 GROUP BY DRG_CD
 ORDER BY AMT DESC 
};

unless ($sth = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute($state_cd, $county_cd)) {
   &display_error("ERROR executing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

my $now = localtime();

my $rows = $sth->rows;

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>DRG ANALYSIS BY PROVIDER</TITLE>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon" />
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css" />
</HEAD>
<BODY>
<CENTER>
<FONT SIZE="5">
<B>MEDICARE CLAIMS SUMMARY<BR>
FOR PATIENTS RESIDING IN $title_county COUNTY, $title_state</B><BR>
</FONT>
<FONT SIZE="2">
$now<BR>
</FONT>
<BR>
<TABLE BORDER="1" CELLPADDING="4" WIDTH="90%">
  <TR BGCOLOR="#dddddd">
    <TH>DRG<BR>CODE</TH>
    <TH>DESCRIPTION</TH>
    <TH ALIGN="right">CLAIM<BR>COUNT</TH>
    <TH ALIGN="right">TOTAL<BR>AMOUNT PAID</TH>
    <TH ALIGN="right">PERCENT<BR>OF TOTAL</TH>
  </TR>
};

my $total_claims = 0;
my $total_amount = 0;

my $count = 0;

my @buffer;

my ($class, $i, $edit_amount, $edit_pct);
my ($provider, $name, $city, $state);
my ($drg, $desc, $pmt_amt, $claim_count, $amtlink);

while (($drg, $desc, $claim_count, $pmt_amt) = $sth->fetchrow_array) {

   $total_claims += $claim_count;
   $total_amount += $pmt_amt;

   push @buffer, [$drg, $desc, $claim_count, $pmt_amt];
}

my $link;

my $limit = @buffer;

for ($i=0;$i<$limit;++$i) {

   ($drg, $desc, $claim_count, $pmt_amt) = @{$buffer[$i]};

   $edit_amount = &comify($pmt_amt);

   $edit_pct = sprintf ("%.02f", $pmt_amt / $total_amount * 100);

   $class = (++$count % 2) ? "odd" : "even";


   print "   <TR CLASS=\"$class\">\n";
   print "      <TD ALIGN=\"center\">$drg</TD>\n";
   print "      <TD ALIGN=\"left\">$desc</TD>\n";
   print "      <TD ALIGN=\"right\">$claim_count</TD>\n";
   print "      <TD ALIGN=\"right\">$edit_amount</TD>\n";
   print "      <TD ALIGN=\"right\">$edit_pct &#37;</TD>\n";
   print "   </TR>\n";
}

my $edit_count  = &comify($total_claims);
   $edit_amount = &comify(sprintf("%.02f",$total_amount));

$class = (++$count % 2) ? "odd" : "even";

print "   <TR CLASS=\"$class\">\n";
print "      <TD ALIGN=\"right\" COLSPAN=\"2\"><B>TOTAL:</B></TD>\n";
print "      <TD ALIGN=\"right\"><B>$edit_count</B></TD>\n";
print "      <TD ALIGN=\"right\"><B>$edit_amount</B></TD>\n";
print "      <TD ALIGN=\"left\">&nbsp;</TD>\n";
print "   </TR>\n";

my $elapsed_time = time - $start_time;

print qq{
</TABLE>
$count ROWS IN $elapsed_time SECONDS
</CENTER>
</BODY>
</HTML>
};

$sth->finish;
$dbh->disconnect;
   
exit(0);


#------------------------------------------------------------------------------

sub display_error
{
   my $s;

   print "Content-Type: text/html\n\n";

   print qq{
<HTML>
<HEAD><TITLE>ERROR NOTIFICATION</TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css" />
<link href="/JS/ergosign.styleform.css" media="all" type="text/css" rel="stylesheet">

    <script type="text/javascript" src="/JS/mootools-1.2-core.js"></script>

    <script type="text/javascript" src="/JS/ergosign.styleform.js"></script>

    <script type="text/javascript" src="/JS/script.js"></script>
    <link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />

</HEAD>
<BODY>
<CENTER>
<BR>
<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/index.html" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/cr_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="/cgi-bin/cr_batchx.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="/cgi-bin/snf_batch1.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="/cgi-bin/graph.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Graph Utility</div>">
                                <img src="/images/graph_icon_inner.gif" onmouseover="src='/images/graph_icon_inner_hover.gif';"
                                    onmouseout="src='/images/graph_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                      <td style="width: 102px" align="Center">
		                                  <a href="/cgi-bin/PeerMain.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
		                                      <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->

<FONT SIZE="5" COLOR="RED">
<B>ERRORS NOTIFICATION</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" BORDER="1" CELLPADDING="20" WIDTH="600">
<TR><TD>
<UL>
};
   foreach $s (@_) {
      print "$s<BR>";
   }
   print qq{
</TD></TR>
</TABLE>
</FONT>
</CENTER>
</BODY>
</HTML>
};
}

#----------------------------------------------------------------------------

sub comify
{
   local($_) = shift;
   1 while s/^(-?\d+)(\d{3})/$1,$2/;
   return($_);
}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#------------------------------------------------------------------------------

