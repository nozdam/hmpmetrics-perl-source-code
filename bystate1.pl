#!/usr/bin/perl -w

#--------------------------------------------------------------------
# Program: derek1.pl
#  Author: Steve Spicer
# Written: Thu Oct 18 09:07:40 CDT 2007
# Revised: 
#   Notes:
# Depends: DBI.pm, CGI.pm
#--------------------------------------------------------------------

use strict;
use DBI;
use CGI;

my $cgi = new CGI;

my $given_state = $cgi->param('state');

my %state_lookup = (
  'AL' => 'Alabama',
  'AK' => 'Alaska',
  'AZ' => 'Arizona',
  'AR' => 'Arkansas',
  'CA' => 'California',
  'CO' => 'Colorado',
  'CT' => 'Connecticut',
  'DE' => 'Delaware',
  'DC' => 'District Of Columbia',
  'FL' => 'Florida',
  'GA' => 'Georgia',
  'HI' => 'Hawaii',
  'ID' => 'Idaho',
  'IL' => 'Illinois',
  'IN' => 'Indiana',
  'IA' => 'Iowa',
  'KS' => 'Kansas',
  'KY' => 'Kentucky',
  'LA' => 'Louisiana',
  'ME' => 'Maine',
  'MD' => 'Maryland',
  'MA' => 'Massachusetts',
  'MI' => 'Michigan',
  'MN' => 'Minnesota',
  'MS' => 'Mississippi',
  'MO' => 'Missouri',
  'MT' => 'Montana',
  'NE' => 'Nebraska',
  'NV' => 'Nevada',
  'NH' => 'New Hampshire',
  'NJ' => 'New Jersey',
  'NM' => 'New Mexico',
  'NY' => 'New York',
  'NC' => 'North Carolina',
  'ND' => 'North Dakota',
  'OH' => 'Ohio',
  'OK' => 'Oklahoma',
  'OR' => 'Oregon',
  'PA' => 'Pennsylvania',
  'RI' => 'Rhode Island',
  'SC' => 'South Carolina',
  'SD' => 'South Dakota',
  'TN' => 'Tennessee',
  'TX' => 'Texas',
  'UT' => 'Utah',
  'VT' => 'Vermont',
  'VA' => 'Virginia',
  'WA' => 'Washington',
  'WV' => 'West Virginia',
  'WI' => 'Wisconsin',
  'WY' => 'Wyoming',
);

my $state_name = uc($state_lookup{$given_state});

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error('CAN\'T READ milo.ini');
   exit(1);
}


my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my ($sql, $dbh, $sth);

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

#----------------------------------------------------------------------------

$sql = qq{
   SELECT PROVIDER_NUMBER, HOSP_NAME, STREET_ADDR, CITY, STATE, ZIP_CODE, COUNTY 
     FROM HOSPITALS
    WHERE STATE = ?
    ORDER BY CITY, ZIP_CODE
};

unless ($sth = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL query:",DBI->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute($given_state)) {
   &display_error("ERROR executing SQL query:",DBI->errstr);
   $dbh->disconnect;
   exit(1);
}

my $now = localtime();

my $rows = $sth->rows;

 my $rows_returned = $sth->rows();
 
print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>HOSPITALS IN $state_name</TITLE>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon">
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
<link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />
<SCRIPT>
/*-- Show hide reports menu --*/
function showMenu(id,rows){	
	

	var menuID = document.getElementById(id);
	
	// for more than one row.
	for (var i=1;i<=rows;i++){  
		if(i == id){ 
			// show current row menu
			document.getElementById(i).style.visibility ='visible';		
		}else{  
			//Hide all other rows menus
			document.getElementById(i).style.visibility ='hidden';
		}
	}
	
	// show menu when only one row in search result.
	if(id == 1){ 
		menuID.style.visibility = 'visible';//(menuID.style.visibility == 'visible') ? 'hidden' : 'visible';
	}
	

	// Hide menu when only one row in search result
	if(id == 0){ 
		document.getElementById('1').style.visibility = 'hidden';
	}   
		
}
</SCRIPT>
</HEAD>

<BODY>
<CENTER>

<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/index.html" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/cr_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="/cgi-bin/cr_batchx.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="/cgi-bin/snf_batch1.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="/cgi-bin/graph.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Graph Utility</div>">
                                <img src="/images/graph_icon_inner.gif" onmouseover="src='/images/graph_icon_inner_hover.gif';"
                                    onmouseout="src='/images/graph_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 102px" align="Center">
			                            <a href="/cgi-bin/PeerMain.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
			                                <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->


<!--content start--><br>
    <table cellpadding="0" cellspacing="0" width="885px">
        <tr>
            <td valign="top">
                <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px">

<div style="padding-top:10px; padding-bottom:10px; float:left" class="pageheader">
<B>$state_name HOSPITALS</B>
</div><div style="padding-top:10px; padding-bottom:10px; float:right">

<FONT SIZE="2">
$now
</FONT></div>

<TABLE BORDER="0" CELLPADDING="0" cellspacing="0" WIDTH="100%" class="gridstyle">
  <TR class="gridheader">
    <TD style="padding-left:5px">Provider #</TD>
    <TD>Hospital Name</TD>
    <TD>Address</TD>
    <TD>County</TD>    
  </TR>
};


my $count = 0;

my ($bgcolor, $class, $element, $link, $src_link);
my ($provider, $name, $address, $city, $state, $zip, $county);
my ($drg_link, $cr_link);

while (($provider, $name, $address, $city, $state, $zip, $county) = $sth->fetchrow_array) {

   $drg_link = "<A HREF=\"/cgi-bin/drg_mix.pl?provider=$provider\" TITLE=\"DRG MIX\">DRG MIX</A>";

   $src_link = "<A HREF=\"/cgi-bin/derek3.pl?provider=$provider\" TITLE=\"WHERE PATIENTS COME FORM\">PATIENT SOURCE</A>";

   $cr_link = "<A HREF=\"/cgi-bin/choose1.pl?provider=$provider\">COST REPORT</A>";

   $class = (++$count % 2) ? "gridrow" : "gridalternate";

   print "  <TR CLASS=\"$class\" onclick=\"showMenu(0,$rows_returned)\">\n";
   print "   <TD valign=\"top\" style=\"padding-top:10px; padding-left:5px\"><a href=\"#\" onmouseover=\"showMenu($count ,$rows_returned)\" ><B>$provider</B></a>\n";
   print "    <div id=\"$count\" class=\"$class\" align=\"right\" style=\"border:2px solid #36648B;top;100px;left:75px;position:absolute;visibility:hidden;\"><table CELLPADDING=\"2\"><tr bgcolor=\"#B9D3EE\"><td colspan=2 align=\"center\" style=\"height:30px;border-bottom:0px solid #FFFFFF;color:#000000;font-family:Helvetica;font-size:12px;font-weight:bold;\" nowrap >Reports</td></tr><tr bgcolor=\"#B9D3EE\"><td><img src=\"/images/costreport_icon.gif\" /></td><td class=\"gridHlink\" nowrap>$cr_link </td></tr><tr bgcolor=\"#B9D3EE\"><td><img src=\"/images/drgmix_icon.gif\" /></td><td class=\"gridHlink\" nowrap>$drg_link</td></tr><tr bgcolor=\"#B9D3EE\"><td><img src=\"/images/patientsource_icon.gif\" /></td><td nowrap class=\"gridHlink\">$src_link</td></tr></table></div></TD>\n";
   print "   <TD ALIGN=\"left\" ><B>$name</B></TD>\n";
   print "   <TD ALIGN=\"left\" >$address<BR>$city, $state  $zip</TD>\n";
   print "   <TD valign=\"top\" style=\"padding-top:10px\">$county</TD>\n";   
   print "  </TR>\n";
}

print qq{
</TABLE>
$count ROWS
	  </td>
		<td valign="top">
		<img src="/images/content_right.gif" /></td>
	</tr>
	</table>
<!--content end-->

</CENTER>
</BODY>
</HTML>
};

$sth->finish;
$dbh->disconnect;
   
exit(0);


#------------------------------------------------------------------------------

sub display_error
{
   my $s;

   print "Content-Type: text/html\n\n";

   print qq{
<HTML>
<HEAD><TITLE>ERROR NOTIFICATION</TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css" />
<link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />
</HEAD>
<BODY>
<CENTER>
<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/index.html" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/cr_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="/cgi-bin/cr_batchx.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="/cgi-bin/snf_batch1.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="/cgi-bin/graph.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Graph Utility</div>">
                                <img src="/images/graph_icon_inner.gif" onmouseover="src='/images/graph_icon_inner_hover.gif';"
                                    onmouseout="src='/images/graph_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 102px" align="Center">
			                            <a href="/cgi-bin/PeerMain.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
			                                <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->

<BR>
<FONT SIZE="5" COLOR="RED">
<B>ERRORS NOTIFICATION</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" BORDER="1" CELLPADDING="20"  WIDTH="600">
<TR><TD>
<UL>
};
   foreach $s (@_) {
      print "$s<BR>";
   }
   print qq{
</TD></TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</FONT>
</CENTER>
</BODY>
</HTML>
};
}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#------------------------------------------------------------------------------

