#!/usr/bin/perl


use CGI;
my $cgi = new CGI;
my %ini_value;
my $s;
my $row;
my $facility = $cgi->param("facility");
do "common_header.pl";
print "Content-Type: text/html\n\n";
&headerScript();
print qq{
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Formulas - Create Mode</title>
    <link href="/css/tabs.css" rel="stylesheet" type="text/css" />
	<script language="javascript" src="/JS/admin_panel/standard_report/FormulaTabPageB.js" ></script>
</head>
<body>
<form name="frmFormulaTabPageB" action="/cgi-bin/FormulaActions.pl?facility=$facility" method="post">
<input type="hidden" name="saction" value="create">
<input type="hidden" name="facility" value="$facility">
<ol id="toc">
    <li><a href="/cgi-bin/FormulaTabPageA.pl?facility=$facility"><span>Formula</span></a></li>
    <li class="current"><a href="/cgi-bin/FormulaTabPageB.pl?facility=$facility"><span>Create Indicator</span></a></li>
    <li><a href="/cgi-bin/FormulaTabPageC.pl?facility=$facility"><span>Edit Indicator</span></a></li>
</ol>
<div class="content">
    <table class="gridstyle" cellpadding="3" cellspacing="3">
    	<tr>
    		<td align="right" style="font-weight:bold;">Name :</td><td><input type="text" name="txtName" value=""></td><td style="font-weight:bold;color:red;"> * </td>
    	</tr>
    	<tr>
	    	<td align="right" style="font-weight:bold;">Work Sheet No. :</td><td><input type="text" name="txtWorksheet" maxLength="7" value=""></td><td></td>
	</tr>
    	<tr>
	    	<td align="right" style="font-weight:bold;">Line No. :</td><td><input type="text" name="txtLineNo" maxLength="5" value=""></td><td></td>
    	</tr>
    	<tr>
	    	<td align="right" style="font-weight:bold;">Column No. :</td><td><input type="text" name="txtColNo" maxLength="4" value=""></td><td></td>
    	</tr>
    	<tr>
	    	<td></td><td><span class="button"><input type="button" name="btnClear" value="Clear" onclick="clearAll()"></span><span class="button"><input type="button" name="btnSave" value="Save" onclick="callSubmit()"></span></td><td></td>
    	</tr>
    </table>
</div>
</form>
</body>
</html>
}
;

#--------------------------------------------------------------------------------------

sub sessionExpire
{

print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}



