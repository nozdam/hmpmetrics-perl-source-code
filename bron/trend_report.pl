#!/usr/bin/perl

use CGI;
use DBI;

do "./common_header.pl";
do "../pagefooter.pl";
my $cgi = new CGI;
use Date::Calc qw(Delta_Days);
use CGI::Session;
do "../audit.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");

my $providerid = $session->param("providerid");
#my $providerid ="171354";

if(!$userID)
{
	&sessionExpire;
}


# Database 

my $dbh;
my ($sql, $sth,$stmt,$prevprovider);


# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}
&view("View","Generate trend report","bron");
my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};

my ($years,$yearLoopCount,$htmlData,$hospitalName);

$yearLoopCount = 0;
@fYears = reverse(@fYears);
foreach my $fYears (@fYears){
	if($yearLoopCount <5){
		if($years){
			$years = $years . "," . $fYears;
		}
		else{
			$years = $fYears;
		}
	}
	$yearLoopCount = $yearLoopCount + 1;	
}

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

#Get the provider name
if($providerid){
	$sql = "Select HospitalName as HOSP_NAME from tblhospitalmaster where ProviderNo = $providerid";
	$stmt = $dbh->prepare($sql);
	$stmt->execute();
	while ($row = $stmt->fetchrow_hashref) {	
		$hospitalName = $$row{HOSP_NAME};
	}
}

#Declare cmi variable
my %cmi;

#Generate Report

my(@provider_number,@rpt_rec_num,@rpt_year,@fy_bgn_dt,@fy_end_dt,$recCount,@prev_prov,$prev_prov,@control_type,@facility_type);
$recCount = 1;
if($providerid){

$prevprovider="Select PreviousProviderNo from tblpreviousprovider where CurrentProviderNo = '$providerid' ";#($providerid,$prevprovider)
	
	$sth = $dbh->prepare($prevprovider);
	$sth->execute();
	 
	 while ($row = $sth->fetchrow_hashref){
	 push (@prev_prov, $$row{PreviousProviderNo});
	 }
	 
	 foreach my $prev_prov(@prev_prov){
	$providerid=$providerid.",".$prev_prov;
	}
	
	  
	$sql = "SELECT c.PRVDR_NUM, c.RPT_REC_NUM, year(c.FY_END_DT) as RPT_YEAR, c.RPT_STUS_CD, c.FI_NUM, c.FY_BGN_DT, c.FY_END_DT,t.Control_Type_Name as CONTROL_TYPE_NAME,
s.Hospital_Type_Name as FACILITY_TYPE
FROM CR_ALL_RPT as c INNER JOIN tblhospitalmaster as k on k.ProviderNo=c.PRVDR_NUM
INNER JOIN control_type as t on t.Control_Type_id=k.HMPControl
INNER JOIN hospital_type as s on s.Hospital_Type_id=k.HMPFacilityType
 WHERE PRVDR_NUM in ($providerid) AND year(FY_END_DT)  in ($years) ORDER BY FY_END_DT desc";
	$sth = $dbh->prepare($sql);
	$sth->execute();
	
	while ($row = $sth->fetchrow_hashref) {	
		push (@rpt_rec_num, $$row{RPT_REC_NUM});
		push (@rpt_year, $$row{RPT_YEAR});
		push (@fy_bgn_dt, $$row{FY_BGN_DT});
		push (@fy_end_dt, $$row{FY_END_DT});
		push(@control_type,$$row{CONTROL_TYPE_NAME});
		push(@facility_type,$$row{FACILITY_TYPE});
		push(@provider_number,$$row{PRVDR_NUM});
		$recCount++;
	}

open(J, ">Data.txt");
	    print J $sql;
	  close J;
	$sql = "SELECT unadjusted_cmi,year FROM cmi WHERE provider_number in ($providerid)";
		$sth = $dbh->prepare($sql);
		$sth->execute();
		while($row = $sth->fetchrow_hashref){
			$cmi{$$row{year}}=$$row{unadjusted_cmi};
	}

	#Starting a new Row
	$htmlData = $htmlData . "<tr>";
	#FY Begin-Date
	$htmlData = $htmlData . "<td align=\"left\">" . "FY Begin-Date" . "</td>";
	my $countX = 0;
	foreach my $fy_bgn_dt (@fy_bgn_dt){
		if($countX%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $fy_bgn_dt . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $fy_bgn_dt . "</td>";
		}	
		$countX++;
	}

	$htmlData = $htmlData . "</tr>";
	$htmlData = $htmlData . "<tr>";
	
	
	#FY End-Date
	$htmlData = $htmlData . "<td align=\"left\">" . "FY End-Date" . "</td>";
	my $countY = 0;
	foreach my $fy_end_dt (@fy_end_dt){
		if($countY%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $fy_end_dt . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $fy_end_dt . "</td>";
		}
		$countY++;
	}
	
	$htmlData = $htmlData . "</tr>";
	
	#Number of Days in Period
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"left\">" . "Number of Days in Period" . "</td>";
	foreach my $fy_bgn_dt (@fy_bgn_dt){	
		@fy_bgn = split("-",$fy_bgn_dt);
		@fy_end  = split("-",$fy_end_dt[$arrayCount]);	
		my $value = Delta_Days(@fy_bgn, @fy_end);	
		$value = $value+1;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $value . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $value . "</td>";
		}	
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";
	
	
	#Provider Number
	$htmlData = $htmlData . "<td align=\"left\">" . "Provider Number" . "</td>";
	my $countI = 0;
	foreach my $provider_number (@provider_number){
		if($countI%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $provider_number . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $provider_number . "</td>";
		}	
		$countI++;
	}

	$htmlData = $htmlData . "</tr>";
	$htmlData = $htmlData . "<tr>";
	
	#Type of Control#
	$htmlData = $htmlData . "<td align=\"left\">" . "Type of Control" . "</td>";
	my $countM = 0;
	foreach my $control_type (@control_type){
		if($countM%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $control_type . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $control_type. "</td>";
		}
		$countM++;
	}
	
	$htmlData = $htmlData . "</tr>";
	
	
	#Facility Type#
	$htmlData = $htmlData . "<td align=\"left\">" . "Facility Type" . "</td>";
	my $countF = 0;
	foreach my $facility_type (@facility_type){
		if($countF%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $facility_type . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $facility_type . "</td>";
		}
		$countF++;
	}
	$htmlData = $htmlData . "</tr>";
	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";	
	
	
	
	#FY 
	$htmlData = $htmlData . "<td nowrap><b>" .$hospitalName. "</b></td>";
	my $countY = 0;
	foreach my $rpt_year (@rpt_year){
		if($countY%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . $rpt_year . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . $rpt_year . "</b></td>";
		}
		$countY++;
	}
	
	$htmlData = $htmlData . "</tr>";
	
	
	##Average Age of Plant 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Average Age of Plant" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(G000000-01201-0100 + G000000-01301-0100 + G000000-01401-0100 + G000000-01501-0100 + G000000-01601-0100 + G000000-01701-0100 +G000000-01801-0100 + G000000-01901-0100) / A700003-00500-0900");
		$value = sprintf "%.2f", $value; 
		
		
		if($arrayCount%2==0){	
if(($value >= "0.00f") && ($value <= "21.24f") ){		
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
		    $htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" ."N/A". "</td>";
		}
		}else{
		if(($value >= "0.00f") && ($value <= "21.24f") ){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}else{
		    $htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . "N/A" . "</td>";
		}
		
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";
	
		
			
	##Average Payment Period (Days) 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Average Payment Period (Days)" . "</td>";
	
	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(G000000-03600-0100 / (G300000-00400-0100 + G300000-03000-0100 - A700003-00500-0900) / 365)");
		$value = sprintf "%.2f", $value;	

		if($arrayCount%2==0){	
if(($value >= "7.13f") && ($value <= "105.18f") ){		
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
		     $htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" ."N/A" . "</td>";			
		}
		}else{
		if(($value >= "7.13f") && ($value <= "105.18f") ){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}else{
		     $htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" ."N/A" . "</td>";
		}
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";

	
	
	##Case-Mix Adjusted Average Length of Stay (ALOS) 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Case-Mix Adjusted Average Length of Stay (ALOS)" . "</td>";
	
	
	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "( S300001-01200-0600 / S300001-01200-1500) / $cmi{$rpt_year}");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){				
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";

	
	##Case-Mix Adjusted FTEs/AOB 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Case-Mix Adjusted FTEs per AOB" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(S300001-02500-1000 / (((S300001-01200-0600 - S300001-00400-1100) * G200000-02500-0300) / (G200000-02500-0100 - G200000-00700-0100 - G200000-00800-0100) / 365)) / $cmi{$rpt_year}");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){				
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";



	##Current Asset Turnover 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Current Asset Turnover" . "</td>";
	
	
	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-00300-0100 / G000000-01100-0100");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){				
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";

	
		
	##Current Ratio 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Current Ratio" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-01100-0100 / G000000-03600-0100");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){				
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";


	##Days Cash on Hand 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Days Cash on Hand" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "((G000000-00100-0100 + G000000-00200-0200) / (G300000-00400-0100 / 365))");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){				
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";


	
	##Days Cash on Hand - All Sources 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Days Cash on Hand - All Sources" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "((G000000-00100-0100 + G000000-00200-0200 + G000000-02200-0100) / (G300000-00400-0100 / 365))");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){				
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";

	
	##Days in Net Patient AR 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Days in Net Patient AR" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(G000000-00400-0100 - G000000-00600-0100) / (G300000-00300-0100 / 365)");
		$value = sprintf "%.2f", $value;
		
		if($arrayCount%2==0){				
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";

	
	##Days in Net Total AR 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Days in Net Total AR" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(G000000-00400-0100 + G000000-00300-0100 + G000000-00500-0100 - G000000-00600-0100) / (G300000-00300-0100 / 365)");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){				
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";

	
	##Debt Financing % 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td>" . "Debt Financing %" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(G000000-02700-0100 - G000000-05100-0100) / G000000-02700-0100");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){				
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";


		
	##EBIDAR Margin 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td >" . "EBIDAR Margin" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(G300000-03100-0100 + A700003-00500-1100 + A700003 -00500-0900 + A700003-00500-1000) / (G300000-00100-0100 + G300000-02500-0100)");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){				
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";


	
	##Equity Financing % 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td >" . "Equity Financing %" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(G000000-05100-0100 + G000000-05100-0200 + G000000-05100-0300 + G000000-05100-0400) / (G000000-02700-0100 + G000000-02700-0200 + G000000-02700-0300 + G000000-02700-0400)");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){				
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";


	
	
	##Excess Margin 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td >" . "Excess Margin" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(((G300000-00300-0100 - G300000-00400-0100 + G300000-02500-0100) / (G300000-00300-0100 + G300000-02500-0100)) *100)");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){				
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";

	
	##Fixed Asset Turnover 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td >" . "Fixed Asset Turnover" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-00300-0100 / G000000-02100-0100");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){				
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";

	
	##Long-Term Debt to Total Assets 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td >" . "Long-Term Debt to Total Assets" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-04200-0100 / (G000000-02700-0100 - G000000-04300-0100)");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){				
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";


	
	##Medicare Capital RCC 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td >" . "Medicare Capital RCC" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "D10A181-05200-0100 / D40A180-10300-0200");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){				
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";


	
	##Medicare Margin 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td >" . "Medicare Margin" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(E00A18A-02200-0100 - D10A181-05300-0100) / (D10A181-05200-0100 - E00A18A-02200-0100)");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){				
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";

		
	##Medicare Operations RCC 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td >" . "Medicare Operations RCC" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "D10A181-05300-0100 / D40A180-10300-0200");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){				
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";

	
		
	##Occupancy Rate 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td >" . "Occupancy Rate" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "S300001-01200-0600 / S300001-01200-0200");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){				
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";


		
	##Outlier Percentage 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td >" . "Outlier Percentage" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(E00A18A-00200-0100 + E00A18A-00201-0100 + L00A181-00300-0100 + L00A181-00301-0100) / (E31B181-01700-0100 + E32B181-01700-0100 + E00A18A-02200-0100 + E00A18B-03200-0100 + E30C183-05500-0200)");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){				
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";

		
	##Labor cost as a percent of Revenue
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "Labor cost as a percent of Revenue" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "((S300002-00601-0300+S300002-01500-0300+S300003-00200-0300+S300003-00600-0300)/G300000-00300-1000)*100");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			if(($value >= "0.29f") && ($value <= "0.69f" )){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" ."N/A". "</td>";			
		}
		}else{
		if(($value >= "0.29f") && ($value <= "0.69f" )){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" ."N/A". "</td>";			
		}
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";

	
		
	##Quick Ratio 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td >" . "Quick Ratio" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(G000000-01100-0100 - G000000-00700-0100) / G000000-03600-0100");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){				
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";
	
		
	##RCC 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td >" . "RCC" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "C000001-10100-0500 / (C000001-10100-0600 + C000001-10100-0700)");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){				
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";


	##Return on Assets (ROA) 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td >" . "Return on Assets (ROA)" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(((G300000-03100-0100 / G000000-02700-0100) * 100))");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){				
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";

	
	##Return on Equity (ROE) 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td >" . "Return on Equity (ROE)" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "((G300000-03100-0100 / (G000000-02700-0100 - G000000-04300-0100)) * 100)");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){				
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";
	
	
	##Total Asset Turnover 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td >" . "Total Asset Turnover" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "((G300000-00300-0100 + G300000-02500-0100) / G000000-02700-0100)");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){				
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";
	
	
	##Total Debt to Net Assets 
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td >" . "Total Debt to Net Assets" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-04300-0100 / (G000000-02700-0100 - G000000-04300-0100)");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){				
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";

		
	##Operating Margin
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td >" . "Operating Margin " . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(G300000-00500-0100-A800000-01400-0200)/G300000-00300-0100");
		$value = sprintf "%.2f", $value;
		#$htmlData = $htmlData . "<td>" ."Hello"	.	$value;
		if($arrayCount%2==0){
			if(($value >= "-0.26f") && ($value <= "0.26f" )) {
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";			
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . "N/A". "</td>";
		}
		}else{
			if(($value >= "-0.26f") && ($value <= "0.26f") ){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" ."N/A". "</td>";
		}
		}
		
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";


	##Overall Margin
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td >" . "Overall Margin" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(G300000-03100-0100-A800000-01400-0200)/G300000-00300-0100");
		$value=&valpercentage($value);
		#$value = sprintf "%.2f", $value;
		#$htmlData=$htmlData. "Hello".$value."<br>";
		if($arrayCount%2==0){
if(($value >= "-19.00f") && ($value <= "26.00f" )) {		
			$value=&numNegFormat($value);
			$value = $value."%";
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" .$value. "</td>";				
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" ."N/A". "</td>";
		}
		}else{
		if(($value >= "-19.00f") && ($value <= "26.00f") ){
		
			$value=&numNegFormat($value);
			$value = $value."%";
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" .$value. "</td>";
				
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" ."N/A". "</td>";
		}
		}
		$arrayCount = $arrayCount + 1;		
	}
	$htmlData = $htmlData . "</tr>";


	
	
}else{
	$htmlData="<tr><td>Please select provider number and run report again.<a href=\"/cgi-bin/bron/SelectReport.pl\">Back</a></td></tr>";
}
print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>Trend Reports</TITLE>
<SCRIPT SRC="/JS/sorttable.js"></SCRIPT>
<SCRIPT SRC="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<SCRIPT SRC="/JS/dropmenu.js"></SCRIPT>
<link rel="stylesheet" href="/css/dropmenu.css" type="text/css" />
</HEAD>
<body>
<BR>
<FORM NAME = "frmFinacial" Action="/cgi-bin/finantial_report.pl" method="post">
<CENTER>
};
&innerHeader();
print qq{

    <!--content start-->
        
        <table cellpadding="0" cellspacing="0" width="885px">	  
	  <tr>
	        <td width="16"><img src="/images/tableRonuded/top_lef.gif" width="16" height="16"></td>
	        <td height="16" background="/images/tableRonuded/top_mid.gif"><img src="/images/tableRonuded/top_mid.gif" width="16" height="16"></td>
	        <td width="24"><img src="/images/tableRonuded/top_rig.gif" width="24" height="16"></td>
	      </tr>
	      <tr>
	        <td width="16" background="/images/tableRonuded/cen_lef.gif"></td> 
	        <td align="center" valign="middle" bgcolor="#FFFFFF">

<table border="1em" bgColor="#FFFFFF" width="100%" cellpadding="0" cellspacing="0">
<tr class='gridHeader'><td nowrap colspan=$recCount><b>Trend Report for Provider : </b><font style="color:#C11B17;">$providerid</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"/cgi-bin/bron/SelectReport.pl\">Back</a></td></tr>
$htmlData
</table>
</td>	
		<td width="24" background="/images/tableRonuded/cen_rig.gif"><img src="/images/tableRonuded/cen_rig.gif" width="24" height="11"></td>
	      </tr>
	      <tr>
		    <td width="16" height="16"><img src="/images/tableRonuded/bot_lef.gif" width="16" height="16"></td>
		    <td height="16" background="/images/tableRonuded/bot_mid.gif"><img src="/images/tableRonuded/bot_mid.gif" width="16" height="16"></td>
		    <td width="24" height="16"><img src="/images/tableRonuded/bot_rig.gif" width="24" height="16"></td>
	      </tr>
   </table>
};
&TDdoc;
print qq{
</body>
</HTML>
};
#--------------------------------------------------------------------------------------

sub sessionExpire
{

print "Content-Type: text/html\n\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};
&headerScript();
print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}
#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#--------------------------------------------------------------------------------

sub getFormulae
{
	my ($report_id, $yyyy, $formulae) = @_;
	
	my ($value);
	#$value = &getFormulaeValue($report_id,"(G200000-002500-0200)/(G200000-002500-0300)",$yyyy);
	
	$value = &getFormulaeValue($report_id,$formulae,$yyyy);
	
	#EBITDAR as a Percent of Interest Expense
	#Net Revenue per FTE
	
	return $value;
}


#----------------------------------------------------------------------------

sub getFormulaeValue
{
	my ($report_id,$formulae,$yyyy) = @_;
	my ($value, $i, $elementVal, $operation);	
	
	for ($i=0;$i<length($formulae);$i++)
	{
		if(substr($formulae, $i,1) =~ /[A-Z]/ )
		{
			#Spliting elements from Address
			my ($worksheet, $line, $column) = split /[-:]/,substr($formulae, $i,18);
			if(&get_value($report_id, $worksheet, $line, $column, $yyyy))
			{	
				$elementVal = &get_value($report_id, $worksheet, $line, $column, $yyyy);
			}
			else
			{
				$elementVal = 0;
			}
				
			
			$value = $value.$elementVal;
			$i = $i+17;				
		}
		elsif(substr($formulae, $i,1) =~ /\s/ )
		{
		
		}
		else 
		{			
			
			$value = $value . substr($formulae, $i,1);			
		}		
	}
	$value = eval($value);
	if(!$value)
	{
		$value = "0";
	}
	else
	{
		#$value = sprintf "%.2f", $value;		
		$value = $value;
	}
	return $value;

}

#------------------------------------------------------------------------------

sub get_value
{
   my ($report_id, $worksheet, $line, $column, $fYear) = @_;
   
   my $yyyy = $fYear;

   my ($sthVal, $value);

   my $sql = qq(
    SELECT CR_VALUE         
      FROM CR_ALL_DATA
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
   );

   unless ($sthVal = $dbh->prepare($sql)) {
       &display_error('Error preparing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sthVal->execute($report_id, $worksheet, $line, $column)) {
       &display_error('Error executing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }
   
	  
   if ($sthVal->rows) {
      $value = $sthVal->fetchrow_array;
   }
open(J, ">data.txt");
	    print J $sql;
	  close J;
   $sthVal->finish();

   return $value;
}

#------------------------------------------------------------------------------

sub numNegFormat
{
   my ($val,$dollar) = @_;
   if( $val<0){
  
   	$val = $val*-1;
   	$val= Currency_Format($val);
   	if($dollar){
   		$val = "(" . "\$" . $val . ")";
		
   	}
   	else{
   		$val = "(" . $val . ")";
		
   	}	
   }
   else{
   	if($dollar){
   		$val= "\$" . Currency_Format($val);
		
   	}
   	else{
   		$val= Currency_Format($val);
		
   	}
   }
   	
   return $val; 
}

#------------------------------------------------------------------------------
sub valpercentage
{
	my ($val)=@_;
	$val=$val*100;
	$val = sprintf "%.2f", $val;
	#$val=$val."%";
	return $val;

}
#------------------------------------------------------------------------------
sub USA_Format { 
(my $n = shift) =~ s/\G(\d{1,3})(?=(?:\d\d\d)+(?:\.|$))/$1,/g; 
return "\$$n"; 
}

#------------------------------------------------------------------------------

sub Currency_Format { 
(my $n = shift) =~ s/\G(\d{1,3})(?=(?:\d\d\d)+(?:\.|$))/$1,/g; 
return "$n"; 
}


#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>
    
    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>};
	&TDdoc;
print qq{
</BODY>
</HTML>
};

}
