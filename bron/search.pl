#!/usr/bin/perl

require "/var/www/cgi-bin/lib/req-milo.pl";

use CGI;
use DBI;
use strict;
do "../pagefooter.pl";
do "../common_header.pl";
my $cgi = new CGI;
use Date::Calc qw(Delta_Days);
use CGI::Session;

my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");

my $providerid = &trim($cgi->param("txtProviderNo"));
#my $providerid ="171354";
if(!$providerid){
	$providerid = &trim($session->param("providerid"));
}

$session->param("providerid", $providerid);

if(!$userID)
{
	&sessionExpire;
}


# Database

my $dbh;
my ($sql, $sth,$stmt);


# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};

my ($years,$yearLoopCount,$htmlData,$hospitalName);

$yearLoopCount = 0;
@fYears = reverse(@fYears);
foreach my $fYears (@fYears){
	if($yearLoopCount <5){
		if($years){
			$years = $years . "," . $fYears;
		}
		else{
			$years = $fYears;
		}
	}
	$yearLoopCount = $yearLoopCount + 1;
}

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

#Declare cmi variable
my %cmi;

#Generate Report

my(@rpt_rec_num,@rpt_year,@fy_bgn_dt,@fy_end_dt,$recCount);
$recCount = 1;

if($providerid){
	#Get the provider name
	$sql = " Select HospitalName as HOSP_NAME from tblhospitalmaster where ProviderNo = $providerid";
	$stmt = $dbh->prepare($sql);
	$stmt->execute();
	while (my $row = $stmt->fetchrow_hashref) {
		$hospitalName = $$row{HOSP_NAME};
	}

	if ($hospitalName) {
		$sql = "SELECT RPT_REC_NUM, year(FY_END_DT) as RPT_YEAR, RPT_STUS_CD, FI_NUM, FY_BGN_DT, FY_END_DT FROM CR_ALL_RPT WHERE PRVDR_NUM = $providerid AND year(FY_END_DT)  in ($years) ORDER BY RPT_YEAR desc ";
		$sth = $dbh->prepare($sql);
		$sth->execute();
		while (my $row = $sth->fetchrow_hashref) {
			push (@rpt_rec_num, $$row{RPT_REC_NUM});
			push (@rpt_year, $$row{RPT_YEAR});
			push (@fy_bgn_dt, $$row{FY_BGN_DT});
			push (@fy_end_dt, $$row{FY_END_DT});
			$recCount++;
		}

		$sql = "SELECT unadjusted_cmi,year FROM cmi WHERE provider_number=$providerid";
			$sth = $dbh->prepare($sql);
			$sth->execute();
			while(my $row = $sth->fetchrow_hashref){
				$cmi{$$row{year}}=$$row{unadjusted_cmi};
		}

		#Starting a new Row
		$htmlData = $htmlData . "<tr><td align=\"center\"><table width=\"100%\" cellpadding=1 cellspacing=1>";

		#System Affiliation
		$htmlData = $htmlData . "<tr style=\"border:1px solid black\" ><td style=\"background-color:#F5F5DC;\"><b>System Affiliation</b></td>";
		$htmlData = $htmlData . "<td colspan=\"4\"><table width=\"100%\" cellpadding=1 cellspacing=1>";
		$htmlData = $htmlData . "<tr bgcolor=\"#F5F5DC\"><td><input type=\"checkbox\" name=\"sa_Select_All\" id=\"sa_Select_All\" checked=\"yes\"/>&nbsp;&nbsp;&nbsp;&nbsp;Select All</td></tr>";
		$htmlData = $htmlData . "<tr bgcolor=\"#F0FFFF\"><td><input type=\"checkbox\" name=\"sa_NA\" id=\"sa_NA\" />&nbsp;&nbsp;&nbsp;&nbsp;N/A</td></tr>";
		$htmlData = $htmlData . "<tr bgcolor=\"#F5F5DC\"><td><input type=\"checkbox\" name=\"sa_small\" id=\"sa_small\" />&nbsp;&nbsp;&nbsp;&nbsp;Small(No. of Hospitals < 11)</td></tr>";
		$htmlData = $htmlData . "<tr bgcolor=\"#F0FFFF\"><td><input type=\"checkbox\" name=\"sa_medium\" id=\"sa_medium\" />&nbsp;&nbsp;&nbsp;&nbsp;Medium(11 <= No. of Hospitals < 49)</td></tr>";
		$htmlData = $htmlData . "<tr bgcolor=\"#F5F5DC\"><td><input type=\"checkbox\" name=\"sa_large\" id=\"sa_large\" />&nbsp;&nbsp;&nbsp;&nbsp;Large(No. of Hospitals >= 49)</td></tr>";
		$htmlData = $htmlData . "</table></td></tr>";


		#Teaching Status
		$htmlData = $htmlData . "<tr style=\"border:1px solid black\" ><td style=\"background-color:#F0FFFF;\"><b>Teaching Status</b></td>";
		$htmlData = $htmlData . "<td colspan=\"4\"><table width=\"100%\" cellpadding=1 cellspacing=1>";
		$htmlData = $htmlData . "<tr bgcolor=\"#F5F5DC\"><td><input type=\"checkbox\" name=\"ts_Select_All\" id=\"ts_Select_All\" checked=\"yes\"/>&nbsp;&nbsp;&nbsp;&nbsp;Select All</td></tr>";
		$htmlData = $htmlData . "<tr bgcolor=\"#F0FFFF\"><td><input type=\"checkbox\" name=\"ts_NA\" id=\"ts_NA\" />&nbsp;&nbsp;&nbsp;&nbsp;N/A</td></tr>";
		$htmlData = $htmlData . "<tr bgcolor=\"#F5F5DC\"><td><input type=\"checkbox\" name=\"ts_Minor\" id=\"ts_Minor\" />&nbsp;&nbsp;&nbsp;&nbsp;Minor (No. of Interns and Residents < 25)</td></tr>";
		$htmlData = $htmlData . "<tr bgcolor=\"#F0FFFF\"><td><input type=\"checkbox\" name=\"ts_Major\" id=\"ts_Major\" />&nbsp;&nbsp;&nbsp;&nbsp;Major (25 < No. of Interns and Residents < 100)</td></tr>";
		$htmlData = $htmlData . "<tr bgcolor=\"#F5F5DC\"><td><input type=\"checkbox\" name=\"ts_Complax\" id=\"ts_Complax\" />&nbsp;&nbsp;&nbsp;&nbsp;Complex (No. of Interns and Residents > 100)</td></tr>";
		$htmlData = $htmlData . "</table></td></tr>";


		#Hospital Type
		$htmlData = $htmlData . "<tr style=\"border:1px solid black\" ><td style=\"background-color:#F5F5DC;\"><b>Hospital Type</b></td>";
		$htmlData = $htmlData . "<td colspan=\"4\"><table width=\"100%\" cellpadding=1 cellspacing=1>";
		$htmlData = $htmlData . "<tr bgcolor=\"#F0FFFF\"><td><input type=\"checkbox\" name=\"ht_SelectAll\" id=\"ht_SelectAll\" checked=\"yes\"/>&nbsp;&nbsp;&nbsp;&nbsp;Select All</td></tr>";
		$htmlData = $htmlData . "<tr bgcolor=\"#F5F5DC\"><td><input type=\"checkbox\" name=\"ht_GeneralAcute\" id=\"ht_GeneralAcute\" />&nbsp;&nbsp;&nbsp;&nbsp;General Acute</td></tr>";
		$htmlData = $htmlData . "<tr bgcolor=\"#F0FFFF\"><td><input type=\"checkbox\" name=\"ht_Cancer\" id=\"ht_Cancer\" />&nbsp;&nbsp;&nbsp;&nbsp;Cancer</td></tr>";
		$htmlData = $htmlData . "<tr bgcolor=\"#F5F5DC\"><td><input type=\"checkbox\" name=\"ht_Children\" id=\"ht_Children\" />&nbsp;&nbsp;&nbsp;&nbsp;Children's</td></tr>";
		$htmlData = $htmlData . "<tr bgcolor=\"#F0FFFF\"><td><input type=\"checkbox\" name=\"ht_LongTermCare\" id=\"ht_LongTermCare\" />&nbsp;&nbsp;&nbsp;&nbsp;Long-Term Care (LTC)</td></tr>";
		$htmlData = $htmlData . "<tr bgcolor=\"#F5F5DC\"><td><input type=\"checkbox\" name=\"ht_Psychiatric\" id=\"ht_Psychiatric\" />&nbsp;&nbsp;&nbsp;&nbsp;Psychiatric</td></tr>";
		$htmlData = $htmlData . "<tr bgcolor=\"#F0FFFF\"><td><input type=\"checkbox\" name=\"ht_Rehabilitation\" id=\"ht_Rehabilitation\" />&nbsp;&nbsp;&nbsp;&nbsp;Rehabilitation</td></tr>";
		$htmlData = $htmlData . "</table></td></tr>";

		#Critical Access
		$htmlData = $htmlData . "<tr style=\"border:1px solid black\" ><td style=\"background-color:#F0FFFF;\"><b>Critical Access</b></td>";
		$htmlData = $htmlData . "<td colspan=\"4\"><table width=\"100%\" cellpadding=1 cellspacing=1>";
		$htmlData = $htmlData . "<tr bgcolor=\"#F5F5DC\"><td><input type=\"checkbox\" name=\"critical_access_y\" value=\"1\"/>&nbsp;&nbsp;&nbsp;&nbsp;Yes</td></tr>";
		$htmlData = $htmlData . "<tr bgcolor=\"#F0FFFF\"><td><input type=\"checkbox\" name=\"critical_access_n\" value=\"1\"/>&nbsp;&nbsp;&nbsp;&nbsp;No</td></tr>";
		$htmlData = $htmlData . "</table></td></tr>";


		#Type of Control
		$htmlData = $htmlData . "<tr style=\"border:1px solid black\"><td style=\"background-color:#F5F5DC;\"><b>Type of Control</b></td>";
		$htmlData = $htmlData . "<td colspan=\"4\"><table width=\"100%\" cellpadding=1 cellspacing=1>";
		$htmlData = $htmlData . "<tr bgcolor=\"#F5F5DC\"><td><input type=\"checkbox\" name=\"toc_SelectAll\" id=\"toc_SelectAll\" checked=\"yes\"/>&nbsp;&nbsp;&nbsp;&nbsp;Select All</td></tr>";
		$htmlData = $htmlData . "<tr bgcolor=\"#F0FFFF\"><td><input type=\"checkbox\" name=\"toc_Governmental\" id=\"toc_Governmental\" />&nbsp;&nbsp;&nbsp;&nbsp;Governmental</td></tr>";
		$htmlData = $htmlData . "<tr bgcolor=\"#F5F5DC\"><td><input type=\"checkbox\" name=\"toc_ForProfit\" id=\"toc_ForProfit\" />&nbsp;&nbsp;&nbsp;&nbsp;For-Profit</td></tr>";
		$htmlData = $htmlData . "<tr bgcolor=\"#F0FFFF\"><td><input type=\"checkbox\" name=\"toc_NonProfit\" id=\"toc_NonProfit\" />&nbsp;&nbsp;&nbsp;&nbsp;Non-Profit</td></tr>";
		$htmlData = $htmlData . "<tr bgcolor=\"#F5F5DC\"><td><input type=\"checkbox\" name=\"toc_NonProfitChurch\" id=\"toc_NonProfitChurch\" />&nbsp;&nbsp;&nbsp;&nbsp;Non-Profit, Church</td></tr>";
		$htmlData = $htmlData . "</table></td></tr>";

		#Management Authority
		$htmlData = $htmlData . "<tr style=\"border:1px solid black\" ><td style=\"background-color:#F0FFFF;\"><b>Has Management?</b></td>";
		$htmlData = $htmlData . "<td colspan=\"4\"><table width=\"100%\" cellpadding=1 cellspacing=1>";
		$htmlData = $htmlData . "<tr bgcolor=\"#F0FFFF\"><td><input type=\"checkbox\" name=\"has_management_y\" value=\"1\"/>&nbsp;&nbsp;&nbsp;&nbsp;Yes</td></tr>";
		$htmlData = $htmlData . "<tr bgcolor=\"#F5F5DC\"><td><input type=\"checkbox\" name=\"has_management_n\" value=\"1\"/>&nbsp;&nbsp;&nbsp;&nbsp;No</td></tr>";
		$htmlData = $htmlData . "</table></td></tr>";


		#Beds
		$htmlData = $htmlData . "<tr style=\"border:1px solid black\" bgcolor=\"#F5F5DC\">";
		$htmlData = $htmlData . "<td><b>Beds</b></td>";
		$htmlData = $htmlData . "<td>Between</td>";
		$htmlData = $htmlData . "<td><input type=\"text\" name=\"min_bed\" style=\"width:40\" /></td>";
		$htmlData = $htmlData . "<td>AND</td>";
		$htmlData = $htmlData . "<td><input type=\"text\" name=\"max_bed\" style=\"width:40\" /></td></tr>";

		#FTE's
		$htmlData = $htmlData . "<tr style=\"border:1px solid black\" bgcolor=\"#F0FFFF\">";
		$htmlData = $htmlData . "<td><b>FTE's</b></td>";
		$htmlData = $htmlData . "<td>Between</td>";
		$htmlData = $htmlData . "<td><input type=\"text\" name=\"min_fte\" style=\"width:40\" /></td>";
		$htmlData = $htmlData . "<td>AND</td>";
		$htmlData = $htmlData . "<td><input type=\"text\" name=\"max_fte\" style=\"width:40\" /></td></tr>";

		#CMI
		$htmlData = $htmlData . "<tr style=\"border:1px solid black\" bgcolor=\"#F5F5DC\">";
		$htmlData = $htmlData . "<td><b>CMI</b></td>";
		$htmlData = $htmlData . "<td>Between</td>";
		$htmlData = $htmlData . "<td><input type=\"text\" name=\"min_cmi\" style=\"width:40\" /></td>";
		$htmlData = $htmlData . "<td>AND</td>";
		$htmlData = $htmlData . "<td><input type=\"text\" name=\"max_cmi\" style=\"width:40\" /></td></tr>";
		$htmlData = $htmlData . "</table>";
		$htmlData = $htmlData . "<tr><td align=\"center\" style=\"padding-top:5px\"><span class=\"button\"><input id =\"frmSubmit\" type=\"Submit\" value=\"Next\" /></span></td></tr></td></tr>";
	}
	else {
		$htmlData="<tr><td>The provider number you have entered is incorrect. Please try again.<a href=\"/cgi-bin/bron/index.pl\">Back</a></td></tr>";
	}

}else{
	$htmlData="<tr><td>Please select provider number and run report again.<a href=\"/cgi-bin/bron/index.pl\">Back</a></td></tr>";
}
print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>Peer Group Creation</TITLE>
<SCRIPT SRC="/JS/jquery-1.5.2.min.js"></SCRIPT>
<SCRIPT SRC="/JS/sorttable.js"></SCRIPT>
<SCRIPT SRC="/JS/milo_2_0/search.js"></SCRIPT>
<SCRIPT SRC="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<SCRIPT SRC="/JS/dropmenu.js"></SCRIPT>
<link rel="stylesheet" href="/css/dropmenu.css" type="text/css" />
</HEAD>
<body>
<BR>

<FORM ID="frmFinacial" NAME = "frmFinacial" Action="/cgi-bin/bron/select_element.pl" method="post">

<CENTER>
};
&innerHeader();
print qq{
    <!--content start-->

        <table cellpadding="0" cellspacing="0" width="885px">
	  <tr>
	        <td width="16"><img src="/images/tableRonuded/top_lef.gif" width="16" height="16"></td>
	        <td height="16" background="/images/tableRonuded/top_mid.gif"><img src="/images/tableRonuded/top_mid.gif" width="16" height="16"></td>
	        <td width="24"><img src="/images/tableRonuded/top_rig.gif" width="24" height="16"></td>
	      </tr>
	      <tr>
	        <td width="16" background="/images/tableRonuded/cen_lef.gif"></td>
	        <td align="center" valign="middle" bgcolor="#FFFFFF">

<table border="1em" bgColor="#FFFFFF" width="100%" cellpadding="0" cellspacing="0">
<tr class='gridHeader'><td nowrap colspan=$recCount><b>Peer Group Creation Search : </b><font style="color:#C11B17;">$providerid</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"/cgi-bin/bron/index.pl\">Back</a></td></tr>
$htmlData
</table>
</td>
		<td width="24" background="/images/tableRonuded/cen_rig.gif"><img src="/images/tableRonuded/cen_rig.gif" width="24" height="11"></td>
	      </tr>
	      <tr>
		    <td width="16" height="16"><img src="/images/tableRonuded/bot_lef.gif" width="16" height="16"></td>
		    <td height="16" background="/images/tableRonuded/bot_mid.gif"><img src="/images/tableRonuded/bot_mid.gif" width="16" height="16"></td>
		    <td width="24" height="16"><img src="/images/tableRonuded/bot_rig.gif" width="24" height="16"></td>
	      </tr>
   </table>
};

	&TDdoc;

print qq{
</body>
</HTML>
<script language\"\">
	\$(document).ready(function() {
		\$(\"#sa_Select_All\").click(function(){check_uncheck_system_affiliation_all();});
		\$(\"#sa_NA\").click(function(){check_uncheck_system_affiliation();});
		\$(\"#sa_small\").click(function(){check_uncheck_system_affiliation();});
		\$(\"#sa_medium\").click(function(){check_uncheck_system_affiliation();});
		\$(\"#sa_large\").click(function(){check_uncheck_system_affiliation();});

		\$(\"#ts_Select_All\").click(function(){check_uncheck_teaching_status_all();});
		\$(\"#ts_NA\").click(function(){check_uncheck_teaching_status();});
		\$(\"#ts_Minor\").click(function(){check_uncheck_teaching_status();});
		\$(\"#ts_Major\").click(function(){check_uncheck_teaching_status();});
		\$(\"#ts_Complax\").click(function(){check_uncheck_teaching_status();});

		\$(\"#ht_SelectAll\").click(function(){check_uncheck_hospital_type_all();});
		\$(\"#ht_GeneralAcute\").click(function(){check_uncheck_hospital_type();});
		\$(\"#ht_Cancer\").click(function(){check_uncheck_hospital_type();});
		\$(\"#ht_Children\").click(function(){check_uncheck_hospital_type();});
		\$(\"#ht_LongTermCare\").click(function(){check_uncheck_hospital_type();});
		\$(\"#ht_Psychiatric\").click(function(){check_uncheck_hospital_type();});
		\$(\"#ht_CritcalAccess\").click(function(){check_uncheck_hospital_type();});
		\$(\"#ht_Rehabilitation\").click(function(){check_uncheck_hospital_type();});

		\$(\"#toc_SelectAll\").click(function(){check_uncheck_control_type_all();});
		\$(\"#toc_Governmental\").click(function(){check_uncheck_control_type();});
		\$(\"#toc_ForProfit\").click(function(){check_uncheck_control_type();});
		\$(\"#toc_NonProfit\").click(function(){check_uncheck_control_type();});
		\$(\"#toc_NonProfitChurch\").click(function(){check_uncheck_control_type();});

		\$(\"#frmFinacial\").submit(function(){check_default_if_all_blank();});

	});
</script>
};
#--------------------------------------------------------------------------------------



sub display_error
{
   my @list = @_;

   my ($s, $string);

   foreach $s (@list) {
      $s =~ s/\n/<BR \/>/g;
      $string .= "$s<BR>\n";
   }

   print "Content-Type: text/html\n\n";

   print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>

<CENTER>
};
&innerHeader();
print qq{
<BR>
<FONT SIZE="5" COLOR="RED">
<B></B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR ALIGN="LEFT">
      <TD>$string</TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="javascript:window.location.href='/cgi-bin/cr_search.pl'" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
   };
}

#----------------------------------------------------------------------------
sub sessionExpire
{

print "Content-Type: text/html\n\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};
&headerScript();
print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}
#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#--------------------------------------------------------------------------------

sub getFormulae
{
	my ($report_id, $yyyy, $formulae) = @_;
	my ($value);
	#$value = &getFormulaeValue($report_id,"(G200000-002500-0200)/(G200000-002500-0300)",$yyyy);

	$value = &getFormulaeValue($report_id,$formulae,$yyyy);

	#EBITDAR as a Percent of Interest Expense
	#Net Revenue per FTE

	return $value;
}


#----------------------------------------------------------------------------

sub getFormulaeValue
{
	my ($report_id,$formulae,$yyyy) = @_;
	my ($value, $i, $elementVal, $operation);

	for ($i=0;$i<length($formulae);$i++)
	{
		if(substr($formulae, $i,1) =~ /[A-Z]/ )
		{
			#Spliting elements from Address
			my ($worksheet, $line, $column) = split /[-:]/,substr($formulae, $i,18);
			if(&get_value($report_id, $worksheet, $line, $column, $yyyy))
			{
				$elementVal = &get_value($report_id, $worksheet, $line, $column, $yyyy);
			}
			else
			{
				$elementVal = 0;
			}


			$value = $value.$elementVal;
			$i = $i+17;
		}
		elsif(substr($formulae, $i,1) =~ /\s/ )
		{

		}
		else
		{

			$value = $value . substr($formulae, $i,1);
		}
	}
	$value = eval($value);
	if(!$value)
	{
		$value = "0";
	}
	else
	{
		#$value = sprintf "%.2f", $value;
		$value = $value;
	}
	return $value;

}

#------------------------------------------------------------------------------

sub get_value
{
   my ($report_id, $worksheet, $line, $column, $fYear) = @_;

   my $yyyy = $fYear;

   my ($sthVal, $value);

   my $sql = qq(
    SELECT CR_VALUE
      FROM CR_${yyyy}_DATA
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
   );

   unless ($sthVal = $dbh->prepare($sql)) {
       &display_error('Error preparing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sthVal->execute($report_id, $worksheet, $line, $column)) {
       &display_error('Error executing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

   if ($sthVal->rows) {
      $value = $sthVal->fetchrow_array;
   }

   $sthVal->finish();

   return $value;
}

#------------------------------------------------------------------------------

sub numNegFormat
{
   my ($val,$dollar) = @_;
   if($val<0){
   	$val = $val*-1;
   	$val= Currency_Format($val);
   	if($dollar){
   		$val = "(" . "\$" . $val . ")";
   	}
   	else{
   		$val = "(" . $val . ")";
   	}
   }
   else{
   	if($dollar){
   		$val= "\$" . Currency_Format($val);
   	}
   	else{
   		$val= Currency_Format($val);
   	}
   }
   return $val;
}

#------------------------------------------------------------------------------

sub USA_Format {
(my $n = shift) =~ s/\G(\d{1,3})(?=(?:\d\d\d)+(?:\.|$))/$1,/g;
return "\$$n";
}

#------------------------------------------------------------------------------

sub Currency_Format {
(my $n = shift) =~ s/\G(\d{1,3})(?=(?:\d\d\d)+(?:\.|$))/$1,/g;
return "$n";
}
#-------------------------------------------------------------------------------
#Perl trim function to remove whitespace from the start and end of the string

sub trim($)
{
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}
