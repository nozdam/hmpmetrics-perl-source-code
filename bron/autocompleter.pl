#!/usr/bin/perl -w

use CGI;
use CGI::Ajax;
my $cgi = new CGI;
do "../common_header.pl";
my $action    = $cgi->param('paramA'); 

my $show = "visible";
my $hide = "hidden";

print "Content-Type: text/html\n\n";
print qq{

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	
};
&headerScript();
	print qq{
 	
	<script type="text/javascript" src="/JS/AjaxScripts/prototype.js"></script>
	<script type="text/javascript" src="/JS/AjaxScripts/effects.js"></script>
	<script type="text/javascript" src="/JS/AjaxScripts/controls.js"></script>
	<link href="/css/common_css/autocomplete.css" rel="stylesheet" type="text/css" />
	<script language="javascript" src="/JS/milo_2_0/autocompleter.js" ></script>
</head>
<body bgColor='#87AEC5' >
<FORM Name="frmAuto" method="post">
	<table width="100%">
	<tr><td class='gridHlink>
	<div >
		<label '>Enter Provider Name/Provider Number</label><br><input type="text" style="width:100%;" id="search" name="search" />	
	</div>
	</td></tr>
	<tr><td>
		<div id="jmm" style="z-index:40;background:#FFFFFF;border:1px solid gray;width:510px;height:250px;overflow:auto;overflow-x:hide;"></div>
	</td></tr>
	<tr>
	<td style=\"width:1000px;\"><table align=\"center\"><tr><td><span class='button'><input type='button' onclick=\"CallAdd()\" value=\"Add Selected\"></span></td><td></td><td></td></tr></table></td>
	</tr>
	<tr style="display:none;"><td style="width:1000px;"><table class="gridstyle" align='center' CELLPADDING="0" CELLSPACING="0" width="100%"><tr class='gridHeader'><td nowrap>Provider #</td><td nowrap>Provider Name</td><td><div id="ssize"></div></td><td align="right" nowrap><input type='checkbox' name='All' onclick='selectAll(this)'></td></tr></table></td></tr>
	<tr>
	   <td>
	      <div style="z-index:10;position:fixed;width:100%;height:200px;overflow:auto;visibility:hidden;" id='lstOfProviders'>
		<table align="left" id="mySampleTable" width="100%" style="border:solid 1px #c0c0c0">
		
		</table>
	      </div>
	   </td>
	 </tr>
	</table>
	<script type="text/javascript">
		new Ajax.Autocompleter("search","jmm","/cgi-bin/bron/server.pl");
	</script>
<input type="hidden" name='txtAutoIds' >
<input type="hidden" name='txtAutoNames'>
<input type="hidden" name='txtAutofoid'>
</FORM>
</body>
</html>

}
;
