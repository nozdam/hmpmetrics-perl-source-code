#!/usr/bin/perl

use CGI;
use DBI;

do "../common_header.pl";
do "../pagefooter.pl";
my $cgi = new CGI;
use Date::Calc qw(Delta_Days);
use CGI::Session;
do "../audit.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");

my $providerid =$session->param("providerid");
#my $providerid ="171354";

if(!$userID)
{
	&sessionExpire;
}


# Database

my $dbh;
my ($sql, $sth,$stmt);

&view("View","Generate trend appendix report","bron");
# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};

my ($years,$yearLoopCount,$htmlData,$hospitalName);

$yearLoopCount = 0;
@fYears = reverse(@fYears);
foreach my $fYears (@fYears){
	if($yearLoopCount <5){
		if($years){
			$years = $years . "," . $fYears;
		}
		else{
			$years = $fYears;
		}
	}
	$yearLoopCount = $yearLoopCount + 1;
}

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

#Get the provider name
if($providerid){
	$sql = "Select HospitalName as HOSP_NAME from tblhospitalmaster where ProviderNo = $providerid";
	$stmt = $dbh->prepare($sql);
	$stmt->execute();
	while ($row = $stmt->fetchrow_hashref) {
		$hospitalName = $$row{HOSP_NAME};
	}
}

#Declare cmi variable
my %cmi;

#Generate Report

my(@provider_number,@rpt_rec_num,@rpt_year,@fy_bgn_dt,@fy_end_dt,$recCount,$prevprovider,$prev_prov,@prev_prov,@control_type,@facility_type);
$recCount = 1;
if($providerid){
$prevprovider="Select PreviousProviderNo from tblpreviousprovider where CurrentProviderNo = '$providerid' ";#($providerid,$prevprovider)

	$sth = $dbh->prepare($prevprovider);
	$sth->execute();

	 while ($row = $sth->fetchrow_hashref){
	 push (@prev_prov, $$row{PreviousProviderNo});
	 }

	 foreach my $prev_prov(@prev_prov){
	$providerid=$providerid.",".$prev_prov;
	}

	$sql = "SELECT c.PRVDR_NUM,c.RPT_REC_NUM, year(c.FY_END_DT) as RPT_YEAR, c.RPT_STUS_CD, c.FI_NUM, c.FY_BGN_DT, c.FY_END_DT,t.Control_Type_Name as CONTROL_TYPE_NAME,
s.Hospital_Type_Name as FACILITY_TYPE
FROM CR_ALL_RPT as c INNER JOIN tblhospitalmaster as k on k.ProviderNo=c.PRVDR_NUM
INNER JOIN control_type as t on t.Control_Type_id=k.HMPControl
INNER JOIN hospital_type as s on s.Hospital_Type_id=k.HMPFacilityType
 WHERE PRVDR_NUM in ($providerid) AND year(FY_END_DT)  in ($years) ORDER BY FY_END_DT desc";
	$sth = $dbh->prepare($sql);
	$sth->execute();
	while ($row = $sth->fetchrow_hashref) {
		push (@rpt_rec_num, $$row{RPT_REC_NUM});
		push (@rpt_year, $$row{RPT_YEAR});
		push (@fy_bgn_dt, $$row{FY_BGN_DT});
		push (@fy_end_dt, $$row{FY_END_DT});
		push(@control_type,$$row{CONTROL_TYPE_NAME});
		push(@facility_type,$$row{FACILITY_TYPE});
		push(@provider_number,$$row{PRVDR_NUM});
		$recCount++;
	}


	$sql = "SELECT unadjusted_cmi,year FROM cmi WHERE provider_number in ($providerid)";
	$sth = $dbh->prepare($sql);
	$sth->execute();

	if($sth->rows > 0){
	while($row = $sth->fetchrow_hashref){
		$cmi{$$row{year}}=$$row{unadjusted_cmi};
	}

}


	#Starting a new Row
	$htmlData = $htmlData . "<tr>";

	#FY Begin-Date
	$htmlData = $htmlData . "<td align=\"right\">" . "FY Begin-Date" . "</td>";
	my $countX = 0;
	foreach my $fy_bgn_dt (@fy_bgn_dt){
		if($countX%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $fy_bgn_dt . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $fy_bgn_dt . "</td>";
		}
		$countX++;
	}

	$htmlData = $htmlData . "</tr>";
	$htmlData = $htmlData . "<tr>";

	#FY End-Date
	$htmlData = $htmlData . "<td align=\"right\">" . "FY End-Date" . "</td>";
	my $countY = 0;
	foreach my $fy_end_dt (@fy_end_dt){
		if($countY%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $fy_end_dt . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $fy_end_dt . "</td>";
		}
		$countY++;
	}

	$htmlData = $htmlData . "</tr>";

	#Number of Days in Period
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Number of Days in Period" . "</td>";
	foreach my $fy_bgn_dt (@fy_bgn_dt){
		@fy_bgn = split("-",$fy_bgn_dt);
		@fy_end  = split("-",$fy_end_dt[$arrayCount]);
		$value = Delta_Days(@fy_bgn, @fy_end);
		$value = $value+1;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $value . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $value . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#Provider Number
	$htmlData = $htmlData . "<td align=\"right\">" . "Provider Number" . "</td>";
	my $countI = 0;
	foreach my $provider_number (@provider_number){
		if($countI%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $provider_number . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $provider_number . "</td>";
		}
		$countI++;
	}

	$htmlData = $htmlData . "</tr>";
	$htmlData = $htmlData . "<tr>";

	#Type of Control#
	$htmlData = $htmlData . "<td align=\"right\">" . "Type of Control" . "</td>";
	my $countM = 0;
	foreach my $control_type (@control_type){
		if($countM%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $control_type . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $control_type. "</td>";
		}
		$countM++;
	}

	$htmlData = $htmlData . "</tr>";


	#Facility Type#
	$htmlData = $htmlData . "<td align=\"right\">" . "Facility Type" . "</td>";
	my $countF = 0;
	foreach my $facility_type (@facility_type){
		if($countF%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . $facility_type . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . $facility_type . "</td>";
		}
		$countF++;
	}
	$htmlData = $htmlData . "</tr>";
	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Average Age of Plant
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Average Age of Plant</b>" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(G000000-01201-0100 + G000000-01301-0100 + G000000-01401-0100 + G000000-01501-0100 + G000000-01601-0100 + G000000-01701-0100 +G000000-01801-0100 + G000000-01901-0100) / A700003-00500-0900");
		$value = sprintf "%.2f", $value;


		if($arrayCount%2==0){
if(($value > "0.00f") && ($value <= "21.24f") ){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
		    $htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" ."N/A". "</td>";
		}
		}else{
		if(($value > "0.00f") && ($value <= "21.24f") ){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}else{
		    $htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . "N/A" . "</td>";
		}

		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#Accumulated Depreciation
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Accumulated Depreciation" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-01201-0100 + G000000-01301-0100 + G000000-01401-0100 + G000000-01501-0100 + G000000-01601-0100 + G000000-01701-0100 + G000000-01801-0100 + G000000-01901-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Depreciation Expense
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Depreciation Expense" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "A700003-00500-0900");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Average Payment Period (Days)
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Average Payment Period (Days)</b>" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(G000000-03600-0100 / ((G300000-00400-0100 + G300000-03000-0100 - A700003-00500-0900) / 365))");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
if(($value => "7.13f") && ($value <= "105.18f") ){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
		     $htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" ."N/A" . "</td>";
		}
		}else{
		if(($value => "7.13f") && ($value <= "105.18f") ){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}else{
		     $htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" ."N/A" . "</td>";
		}
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";




	#Total Current Liabilities
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Current Liabilities" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-03600-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Operating Expenses
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Operating Expenses" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-00400-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#Total Other Expenses
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Other Expenses" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-03000-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#Depreciation Expense
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Depreciation Expense" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "A700003-00500-0900");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#No. of Days in the Period
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "No. of Days in the Period" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "S200000-01700-0400 - S200000-01700-0201");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Case-Mix Adjusted Average Length of Stay (ALOS)
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Case-Mix Adjusted Average Length of Stay (ALOS)</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "( S300001-01200-0600 / S300001-01200-1500) / $cmi{$rpt_year}");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";




	#Total Patient Days
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Patient Days" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "S300001-01200-0600");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Discharges
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Discharges" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "S300001-01200-1500");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#CMI
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "CMI" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = $cmi{$rpt_year};
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";

	##Case-Mix Adjusted FTEs per AOB
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Case-Mix Adjusted FTEs per AOB</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(S300001-02500-1000 / (((S300001-01200-0600 - S300001-00400-1100) * G200000-02500-0300) / (G200000-02500-0100 - G200000-00700-0100 - G200000-00800-0100) / 365)) / $cmi{$rpt_year}");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";




	#Total No. of Beds
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total No. of Beds" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "S300001-02500-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Patient Days
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Patient Days" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "S300001-01200-0600");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#FTEs - Swing Beds
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "FTEs - Swing Beds" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "S300001-00400-1100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#Total Patient Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Patient Revenues" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G200000-02500-0300");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Inpatient Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Inpatient Revenues" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G200000-02500-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#NF Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "NF Revenues" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G200000-00700-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Other LTC Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Other LTC Revenues" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G200000-00800-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#No. of Days in the Period
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "No. of Days in the Period" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(S200000-01700-0400 - S200000-01700-0201)");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#CMI
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "CMI" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = $cmi{$rpt_year};
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Current Asset Turnover
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Current Asset Turnover</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-00300-0100 / G000000-01100-0100");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";




	#Net Patient Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Patient Revenues" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-00300-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Current Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Current Assets" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-01100-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";

	##Current Ratio
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Current Ratio</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-01100-0100 / G000000-03600-0100");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";




	#Total Current Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Current Assets" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-01100-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Current Liabilities
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Current Liabilities" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-03600-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";

	##Days Cash on Hand
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Days Cash on Hand</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "((G000000-00100-0100 + G000000-00200-0200) / (G300000-00400-0100 / 365))");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";




	#Cash on Hand and in Banks
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Cash on Hand and in Banks" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-00100-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#No. of Days in the Period
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "No. of Days in the Period" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "S200000-01700-0400 - S200000-01700-0201");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";

	##Days Cash on Hand - All Sources
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Days Cash on Hand - All Sources</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "((G000000-00100-0100 + G000000-00200-0200 + G000000-02200-0100) / (G300000-00400-0100 / 365))");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#Cash on Hand and in Banks
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Cash on Hand and in Banks" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-00100-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Marketable Securities
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Marketable Securities" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-00200-0200");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Investments
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Investments" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-02200-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Operating Expenses
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Operating Expenses" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-00400-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#No. of Days in the Period
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "No. of Days in the Period" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "S200000-01700-0400 - S200000-01700-0201");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";

	##Days in Net Patient AR
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Days in Net Patient AR</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(G000000-00400-0100 - G000000-00600-0100) / (G300000-00300-0100 / 365)");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#Accounts Receivables
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Accounts Receivables" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-00400-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Allowance for Uncollectables
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Allowance for Uncollectables" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-00600-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Net Patient Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Patient Revenues" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-00300-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";



	#No. of Days in the Period
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "No. of Days in the Period" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "S200000-01700-0400 - S200000-01700-0201");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Days in Net Total AR
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Days in Net Total AR</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(G000000-00400-0100 + G000000-00300-0100 + G000000-00500-0100 - G000000-00600-0100) / (G300000-00300-0100 / 365)");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#Accounts Receivables
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Accounts Receivables" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-00400-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Notes Receivable
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Notes Receivable" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-00300-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Other Receivables
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Other Receivables" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-00500-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Allowance for Uncollectables
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Allowance for Uncollectables" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-00600-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Net Patient Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Patient Revenues" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-00300-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";



	#No. of Days in the Period
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "No. of Days in the Period" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "S200000-01700-0400 - S200000-01700-0201");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Debt Financing %
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Debt Financing %</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(G000000-02700-0100 - G000000-05100-0100) / G000000-02700-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &Percentage_Format($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &Percentage_Format($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#Total Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Assets" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-02700-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Net Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Assets" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-05100-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##EBIDAR Margin
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>EBIDAR Margin</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(G300000-03100-0100 + A700003-00500-1100 + A700003 -00500-0900 + A700003-00500-1000) / (G300000-00100-0100 + G300000-02500-0100)");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#Net Income
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Income" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-03100-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Interest Expense
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Interest Expense" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "A700003-00500-1100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Depreciation Expense
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Depreciation Expense" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "A700003 -00500-0900");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Lease Cost
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Lease Cost" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "A700003-00500-1000");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Patient Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Patient Revenues" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-00100-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";



	#Total Other Income
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Other Income" . "</td>";
	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-02500-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Equity Financing %
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Equity Financing %</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(G000000-05100-0100 + G000000-05100-0200 + G000000-05100-0300 + G000000-05100-0400) / (G000000-02700-0100 + G000000-02700-0200 + G000000-02700-0300 + G000000-02700-0400)");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &Percentage_Format($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &Percentage_Format($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#Fund Balance
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Fund Balance" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-05100-0100 + G000000-05100-0200 + G000000-05100-0300 + G000000-05100-0400");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Assets" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-02700-0100 + G000000-02700-0200 + G000000-02700-0300 + G000000-02700-0400");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Excess Margin
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Excess Margin</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(((G300000-00300-0100 - G300000-00400-0100 + G300000-02500-0100) / (G300000-00300-0100 + G300000-02500-0100)) *100)");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#Net Patient Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Patient Revenues" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-00300-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Operating Expenses
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Operating Expenses" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-00400-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Other Income
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Other Income" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-02500-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Fixed Asset Turnover
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Fixed Asset Turnover</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-00300-0100 / G000000-02100-0100");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#Net Patient Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Patient Revenues" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-00300-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Fixed Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Fixed Assets" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-02100-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";

	##Long-Term Debt to Total Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Long-Term Debt to Total Assets</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-04200-0100 / (G000000-02700-0100 - G000000-04300-0100)");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#Total Long-Term Liabilities
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Long-Term Liabilities" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-04200-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Assets" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-02700-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Liabilities
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Liabilities" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-04300-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Medicare Capital RCC
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Medicare Capital RCC</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "D10A181-05200-0100 / D40A180-10300-0200");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#Total Medicare Capital Costs
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Medicare Capital Costs" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "D10A181-05200-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Inpatient Medicare Charges
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Inpatient Medicare Charges" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "D40A180-10300-0200");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Medicare Margin
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Medicare Margin</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(E00A18A-02200-0100 - D10A181-05300-0100) / (D10A181-05200-0100 - E00A18A-02200-0100)");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#TBD1 - Subtotal (Reduced for D&C)
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Subtotal (Reduced for D&C)" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "E00A18A-02200-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#TBD2 - Medicare Operations Cost
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Medicare Operations Cost" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "D10A181-05300-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#TBD3 - Medicare Capital Cost
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Medicare Capital Cost" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "D10A181-05200-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Medicare Operations RCC
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Medicare Operations RCC</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "D10A181-05300-0100 / D40A180-10300-0200");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#Total Medicare Inpatient Costs (Excluding Capital Related Costs)
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\" nowrap>" . "Total Medicare Inpatient Costs (Excluding Capital Related Costs)" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "D10A181-05300-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Inpatient Medicare Charges
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Inpatient Medicare Charges" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "D40A180-10300-0200");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Occupancy Rate
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Occupancy Rate</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "S300001-01200-0600 / S300001-01200-0200");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#Total Patient Days
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Patient Days" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "S300001-01200-0600");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Bed Days Available
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Bed Days Available" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "S300001-01200-0200");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";



	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Outlier Percentage
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Outlier Percentage</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(E00A18A-00200-0100 + E00A18A-00201-0100 + L00A181-00300-0100 + L00A181-00301-0100) / (E31B181-01700-0100 + E32B181-01700-0100 + E00A18A-02200-0100 + E00A18B-03200-0100 + E30C183-05500-0200)");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#TBD2 - Outlier Payments After 10/1/97
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Outlier Payments After 10/1/97" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "E00A18A-00201-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#TBD3 - Capital DRG outlier payments prior to 10/1/97
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Capital DRG outlier payments prior to 10/1/97" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "L00A181-00300-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#TBD4 - Capital DRG Outlier Payments for Services
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Capital DRG Outlier Payments for Services" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "L00A181-00301-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#TBD5 - Subprovider Total Amount Payable (reduced for D&C)
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Subprovider Total Amount Payable (reduced for D&C)" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "E31B181-01700-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#TBD7 - Subtotal (Reduced for D&C)
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Subtotal (Reduced for D&C)" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "E00A18A-02200-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#TBD8 - Outlier Payments after D&C
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Outlier Payments after D&C" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "E00A18B-03200-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#TBD9 - SNF Total Amount due After D&C
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "SNF Total Amount due After D&C" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "E30C183-05500-0200");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Labor cost as a percent of Revenue
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Labor cost as a percent of Revenue</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "((S300002-00601-0300+S300002-01500-0300+S300003-00200-0300+S300003-00600-0300)/G300000-00300-1000)*100");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			if(($value >= "0.29f") && ($value <= "0.69f" )){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" ."N/A". "</td>";
		}
		}else{
		if(($value >= "0.29f") && ($value <= "0.69f" )){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" ."N/A". "</td>";
		}
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#Salary Expense
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Salary Expense" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "A000000-10100-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Contract Labor Expense
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Contract Labor Expense" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "S300002-00900-0300 + S300002-00901-0300 + S300002-00902-0300 + S300002-01000-0300 + S300002-01001-0300 + S300002-01100-0300 + S300002-01200-0300 + S300002-01201-0300");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Employee Benefits
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Employee Benefits" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "A000000-00500-0200");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Net Patient Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Patient Revenues" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-00300-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##Quick Ratio
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Quick Ratio</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(G000000-01100-0100 - G000000-00700-0100) / G000000-03600-0100");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#Total Current Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Current Assets" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-01100-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Inventory
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Inventory" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-00700-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Current Liabilities
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Current Liabilities" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-03600-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";



	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";


	##RCC
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>RCC</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "C000001-10100-0500 / (C000001-10100-0600 + C000001-10100-0700)");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &Percentage_Format($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &Percentage_Format($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#Total Hospital Costs
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Hospital Costs" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "C000001-10100-0500");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Inpatient Charges
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Inpatient Charges" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "C000001-10100-0600");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Outpatient Charges
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Outpatient Charges" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "C000001-10100-0700");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";



	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";

	##Return on Assets (ROA)
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Return on Assets (ROA)</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(((G300000-03100-0100 / G000000-02700-0100) * 100))");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Net Income
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Income" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-03100-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Assets" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-02700-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";

	##Return on Equity (ROE)
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Return on Equity (ROE)</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "((G300000-03100-0100 / (G000000-02700-0100 - G000000-04300-0100)) * 100)");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Net Income
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Income" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-03100-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Assets" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-02700-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Liabilities
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Liabilities" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-04300-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";



	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";

	##Total Asset Turnover
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Total Asset Turnover</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "((G300000-00300-0100 + G300000-02500-0100) / G000000-02700-0100)");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Net Patient Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Patient Revenues" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-00300-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Other Income
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Other Income" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-02500-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Assets" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-02700-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";

	##Total Debt to Net Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Total Debt to Net Assets</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-04300-0100 / (G000000-02700-0100 - G000000-04300-0100)");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\"><b>" . &numNegFormat($value) . "</b></td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\"><b>" . &numNegFormat($value) . "</b></td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Liabilities
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Liabilities" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-04300-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Total Assets
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Total Assets" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G000000-02700-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";

	##Total Overall Margin
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Overall Margin</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(G300000-03100-0100-A800000-01400-0200)/G300000-00300-0100");
		$value = sprintf "%.2f", $value;
		if($arrayCount%2==0){
			if(($value >= "-0.19f") && ($value <= "0.26f" )) {
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . "N/A". "</td>";
		}
		}else{
			if(($value >= "-0.19f") && ($value <= "0.26f") ){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" ."N/A". "</td>";
		}
		}

		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#Net Income
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Income" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-03100-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Net Patient Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Patient Revenues" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-00300-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	$htmlData = $htmlData . "<tr bgColor=\"#EBDDE2\"><td colspan=$recCount>&nbsp;</td></tr>";

	##Operating Margin
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td nowrap>" . "<b>Operating Margin</b>" . "</td>";


	foreach my $rpt_year (@rpt_year){
		$value =&getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "(G300000-00500-0100-A800000-01400-0200)/G300000-00300-0100");
		#$htmlData = $htmlData . "<td>" ."Hello"	.	$value;
		if($arrayCount%2==0){
			if(($value >= "-0.26f") && ($value <= "0.26f" )) {
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &Percentage_Format($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . "N/A". "</td>";
		}
		}else{
			if(($value >= "-0.26f") && ($value <= "0.26f") ){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &Percentage_Format($value) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" ."N/A". "</td>";
		}
		}

		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


	#Net Income from Service to Patients
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Income from Service to Patients" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-00500-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";

	#Net Patient Revenues
	$arrayCount = 0;
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td align=\"right\">" . "Net Patient Revenues" . "</td>";

	foreach my $rpt_year (@rpt_year){
		$value = &getFormulae($rpt_rec_num[$arrayCount], $rpt_year, "G300000-00300-0100");
		if($arrayCount%2==0){
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F5F5DC\">" . &numNegFormat($value, true) . "</td>";
		}else{
			$htmlData = $htmlData . "<td align=\"right\" style=\"background-color:#F0FFFF\">" . &numNegFormat($value, true) . "</td>";
		}
		$arrayCount = $arrayCount + 1;
	}
	$htmlData = $htmlData . "</tr>";


}else{
	$htmlData="<tr><td>Please select provider number and run report again.<a href=\"/cgi-bin/bron/SelectReport.pl\">Back</a></td></tr>";
}
print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>Trend Appendix Reports</TITLE>
<script type="text/javascript" src="/JS/tablesort.js"></script>
<SCRIPT SRC="/JS/sorttable.js"></SCRIPT>
<SCRIPT SRC="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<SCRIPT SRC="/JS/dropmenu.js"></SCRIPT>
<link rel="stylesheet" href="/css/dropmenu.css" type="text/css" />
</HEAD>
<body>
<BR>
<FORM NAME = "frmFinacial" Action="/cgi-bin/finantial_report.pl" method="post">
<CENTER>
};
&innerHeader();
print qq{

    <!--content start-->

        <table cellpadding="0" cellspacing="0" width="885px">
	  <tr>
	        <td width="16"><img src="/images/tableRonuded/top_lef.gif" width="16" height="16"></td>
	        <td height="16" background="/images/tableRonuded/top_mid.gif"><img src="/images/tableRonuded/top_mid.gif" width="16" height="16"></td>
	        <td width="24"><img src="/images/tableRonuded/top_rig.gif" width="24" height="16"></td>
	      </tr>
	      <tr>
	        <td width="16" background="/images/tableRonuded/cen_lef.gif"></td>
	        <td align="center" valign="middle" bgcolor="#FFFFFF">

<table border="1em" bgColor="#FFFFFF" width="100%" cellpadding="0" cellspacing="0">
<tr class='gridHeader'><td nowrap colspan=$recCount><b>Trend Appendix Report for Provider : </b><font style="color:#C11B17;">$hospitalName - $providerid</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"/cgi-bin/bron/SelectReport.pl\">Back</a></td></tr>
$htmlData
</table>
</td>
		<td width="24" background="/images/tableRonuded/cen_rig.gif"><img src="/images/tableRonuded/cen_rig.gif" width="24" height="11"></td>
	      </tr>
	      <tr>
		    <td width="16" height="16"><img src="/images/tableRonuded/bot_lef.gif" width="16" height="16"></td>
		    <td height="16" background="/images/tableRonuded/bot_mid.gif"><img src="/images/tableRonuded/bot_mid.gif" width="16" height="16"></td>
		    <td width="24" height="16"><img src="/images/tableRonuded/bot_rig.gif" width="24" height="16"></td>
	      </tr>
   </table>
};
&TDdoc;
print qq{
</body>
</HTML>
};
#--------------------------------------------------------------------------------------

sub sessionExpire
{

print "Content-Type: text/html\n\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};
&headerScript();
print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}
#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#--------------------------------------------------------------------------------

sub getFormulae
{
	my ($report_id, $yyyy, $formulae) = @_;
	my ($value);
	#$value = &getFormulaeValue($report_id,"(G200000-002500-0200)/(G200000-002500-0300)",$yyyy);

	$value = &getFormulaeValue($report_id,$formulae,$yyyy);

	#EBITDAR as a Percent of Interest Expense
	#Net Revenue per FTE

	return $value;
}


#----------------------------------------------------------------------------

sub getFormulaeValue
{
	my ($report_id,$formulae,$yyyy) = @_;
	my ($value, $i, $elementVal, $operation);

	for ($i=0;$i<length($formulae);$i++)
	{
		if(substr($formulae, $i,1) =~ /[A-Z]/ )
		{
			#Spliting elements from Address
			my ($worksheet, $line, $column) = split /[-:]/,substr($formulae, $i,18);
			if(&get_value($report_id, $worksheet, $line, $column, $yyyy))
			{
				$elementVal = &get_value($report_id, $worksheet, $line, $column, $yyyy);
			}
			else
			{
				$elementVal = 0;
			}


			$value = $value.$elementVal;
			$i = $i+17;
		}
		elsif(substr($formulae, $i,1) =~ /\s/ )
		{

		}
		else
		{

			$value = $value . substr($formulae, $i,1);
		}
	}
	$value = eval($value);
	if(!$value)
	{
		$value = "0";
	}
	else
	{
		#$value = sprintf "%.2f", $value;
		$value = $value;
	}
	return $value;

}

#------------------------------------------------------------------------------

sub get_value
{
   my ($report_id, $worksheet, $line, $column, $fYear) = @_;

   my $yyyy = $fYear;

   my ($sthVal, $value);

   my $sql = qq(
    SELECT CR_VALUE
      FROM CR_ALL_DATA
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
   );

   unless ($sthVal = $dbh->prepare($sql)) {
       &display_error('Error preparing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sthVal->execute($report_id, $worksheet, $line, $column)) {
       &display_error('Error executing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

   if ($sthVal->rows) {
      $value = $sthVal->fetchrow_array;
   }

   $sthVal->finish();

   return $value;
}

#------------------------------------------------------------------------------

sub numNegFormat
{
   my ($val,$dollar) = @_;
   if($val<0){
   	$val = $val*-1;
   	$val= Currency_Format($val);
   	if($dollar){
   		$val = "(" . "\$" . $val . ")";
   	}
   	else{
   		$val = "(" . $val . ")";
   	}
   }
   else{
   	if($dollar){
   		$val= "\$" . Currency_Format($val);
   	}
   	else{
   		$val= Currency_Format($val);
   	}
   }
   return $val;
}

#------------------------------------------------------------------------------

sub USA_Format {
(my $n = shift) =~ s/\G(\d{1,3})(?=(?:\d\d\d)+(?:\.|$))/$1,/g;
return "\$$n";
}

#------------------------------------------------------------------------------

sub Currency_Format {
(my $n = shift) =~ s/\G(\d{1,3})(?=(?:\d\d\d)+(?:\.|$))/$1,/g;
return "$n";
}

sub Percentage_Format {
	my ($n) = @_;
	return sprintf("%.2f%", ($n*100));
}

#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}
