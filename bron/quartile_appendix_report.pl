#!/usr/bin/perl

use CGI;
use DBI;
use strict;
use URI::Encode;
require "/var/www/cgi-bin/lib/req-milo.pl";

do "../common_header.pl";
do "../pagefooter.pl";
my $cgi = new CGI;
use Date::Calc qw(Delta_Days);
use CGI::Session;
do "../audit.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $lowest_year = $session->param("lowest_year");

my $providerid = $session->param("providerid");
#my $providerid ="171354";

if(!$userID)
{
	&sessionExpire;
}

# Database
my $dbh;
my ($sql, $sth,$stmt);

# Get Database connection
my %ini_value;
&view("View","Generate quartile appendix report","bron");

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};

my ($years,$yearLoopCount,$htmlData,$hospitalName);

$yearLoopCount = 0;
@fYears = reverse(@fYears);
foreach my $fYears (@fYears){
	if($yearLoopCount <6){
		if($years){
			$years = $years . "," . $fYears;
		}
		else{
			$years = $fYears;
		}
	}
	$yearLoopCount = $yearLoopCount + 1;
}

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

#Get the provider name
if($providerid){
	$sql = "Select HospitalName as HOSP_NAME from tblhospitalmaster where ProviderNo = $providerid";
	$stmt = $dbh->prepare($sql);
	$stmt->execute();
	while (my $row = $stmt->fetchrow_hashref) {
		$hospitalName = $$row{HOSP_NAME};
	}
}
#------------------------------------------------------------------

my $Total_Cost_Report=0;

#selected matrics
my $matrics=$session->param("matrics");

#retrieve hash for matrics battleship coordinates
my $matrics_battles=$session->param("matrics_battles");
my $metrics_fieldnames=$session->param("metrics_fieldnames");

#retrieve hash for matrics order
my $matrics_order=$session->param("matrics_order");



#Generate Report
print "Content-Type: text/html\n\n";
print qq{
<HTML>
<HEAD><TITLE>Quartile Appendix Report</TITLE>
<SCRIPT SRC="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<SCRIPT SRC="/JS/dropmenu.js"></SCRIPT>
<link rel="stylesheet" href="/css/dropmenu.css" type="text/css" />
</HEAD>
<body>
<BR>
<FORM NAME = "frmFinacial" Action="/cgi-bin/finantial_report.pl" method="post">
<CENTER>
};
&innerHeader();
print qq{
    <!--content start-->

        <table cellpadding="0" cellspacing="0" width="885px">
	  <tr>
	        <td width="16"><img src="/images/tableRonuded/top_lef.gif" width="16" height="16"></td>
	        <td height="16" background="/images/tableRonuded/top_mid.gif"><img src="/images/tableRonuded/top_mid.gif" width="16" height="16"></td>
	        <td width="24"><img src="/images/tableRonuded/top_rig.gif" width="24" height="16"></td>
	      </tr>
	      <tr>
	        <td width="16" background="/images/tableRonuded/cen_lef.gif"></td>
	        <td align="center" valign="middle" bgcolor="#FFFFFF">

<table border="1em" bgColor="#FFFFFF" width="850px" cellpadding="0" cellspacing="0">
<tr class='gridHeader'><td nowrap colspan=3><b>Quartile Appendix for Provider : </b><font style="color:#C11B17;">$hospitalName ($providerid)</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"/cgi-bin/bron/SelectReport.pl\">Back</a></td></tr>
};

my(@rpt_rec_num,@rpt_year,@fy_bgn_dt,@fy_end_dt,$recCount, $area, @prev_prov);
$recCount = 1;
if($providerid){
	if(@{$matrics}){
		open (J,"data.txt");
		#Display National, State and Locale for each Matric
		foreach my $mat (@{$matrics}){
			my @Provider = ();
			print qq{<tr><td><div style= "position:static;width:850px;overflow:scroll; " ><table cellspacing="0" cellpadding="0" ><tr>};
			#Display National, State and Locale
			for(my $i=1; $i<=3; $i++){
				if($i==1){
					$area="National";
					if ($session->param("Provider_List_National")) {
						@Provider=@{$session->param("Provider_List_National")};
						$Total_Cost_Report=scalar(@Provider);
					}
				}
				elsif($i==2){
					$area="State";
					if ($session->param("Provider_List_State")) {
						@Provider=@{$session->param("Provider_List_State")};
						$Total_Cost_Report=scalar(@Provider);
					}
				}
				else {
					$area="Locale";
					if ($session->param("Provider_List_Locale")) {
						@Provider=@{$session->param("Provider_List_Locale")};
						$Total_Cost_Report=scalar(@Provider);
					}
				}
				print qq{	<td valign="top"><table><tr><td><table border= "1 ">
				 <tr><td colspan=10 align= "center "><b>$mat - $area</b></td></tr>
				 <tr><td><table><tr><td></td></tr><tr><td></td></tr><tr><td nowrap>1st Quartile</td></tr><tr><td nowrap>2nd Quartile</td></tr><tr><td>3rd Quartile</td></tr><tr><td>4th Quartile</td></tr><tr><td>Total *</td></tr><tr><td></td></tr><tr><td></td></tr></td></table></td>
				};
				&cost_report_derived($mat, $area, @Provider);
				print "</td></tr></table></td></tr></table></td>";
			} # end of  for(my $i=1; $i<=3; $i++)
			print qq{  </tr></table></div></td></tr><tr bgColor= "#EBDDE2 "><td colspan=3>&nbsp;</td></tr>};
		} #end of foreach my $mat (@{$matrics}){
	} #if(@{$matrics}){
	else {
		print qq{ <tr><td>No matrics selected<a href= "/cgi-bin/bron/select_element.pl ">Back</a></td></tr> };
	}
} # end of if($providerid){
else{
		print qq{<tr><td>Please select provider number and run report again.<a href= "/cgi-bin/bron/SelectReport.pl ">Back</a></td></tr>};
}



print qq{
</table>
</td>
		<td width="24" background="/images/tableRonuded/cen_rig.gif"><img src="/images/tableRonuded/cen_rig.gif" width="24" height="11"></td>
	      </tr>
	      <tr>
		    <td width="16" height="16"><img src="/images/tableRonuded/bot_lef.gif" width="16" height="16"></td>
		    <td height="16" background="/images/tableRonuded/bot_mid.gif"><img src="/images/tableRonuded/bot_mid.gif" width="16" height="16"></td>
		    <td width="24" height="16"><img src="/images/tableRonuded/bot_rig.gif" width="24" height="16"></td>
	      </tr>
   </table>
};
&TDdoc;
print qq{
</body>
</HTML>
};
#--------------------------------------------------------------------------------------

sub sessionExpire
{

print "Content-Type: text/html\n\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};
&headerScript();
print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}
#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#--------------------------------------------------------------------------------

sub getFormulae
{
	my ($report_id, $yyyy, $formulae) = @_;
	my ($value);
	#$value = &getFormulaeValue($report_id,"(G200000-002500-0200)/(G200000-002500-0300)",$yyyy);

	$value = &getFormulaeValue($report_id,$formulae,$yyyy);

	#EBITDAR as a Percent of Interest Expense
	#Net Revenue per FTE

	return $value;
}
#----------------------------------------------------------------------------

sub getFormulaeValue
{
	my ($report_id,$formulae,$yyyy) = @_;
	my ($value, $i, $elementVal, $operation);

	for ($i=0;$i<length($formulae);$i++)
	{
		if(substr($formulae, $i,1) =~ /[A-Z]/ )
		{
			#Spliting elements from Address
			my ($worksheet, $line, $column) = split /[-:]/,substr($formulae, $i,18);
			my $battleship_value = &get_value($report_id, $worksheet, $line, $column, $yyyy);
			if($battleship_value)
			{
				$elementVal = &get_value($report_id, $worksheet, $line, $column, $yyyy);
			}
			else
			{
				$elementVal = 0;
			}


			$value = $value.$elementVal;
			$i = $i+17;
		}
		elsif(substr($formulae, $i,1) =~ /\s/ )
		{

		}
		else
		{

			$value = $value . substr($formulae, $i,1);
		}
	}
	$value = eval($value);
	if(!$value)
	{
		$value = "0";
	}
	else
	{
		#$value = sprintf "%.2f", $value;
		$value = $value;
	}
	return $value;

}
#------------------------------------------------------------------------------

sub get_value
{
   my ($report_id, $worksheet, $line, $column, $fYear) = @_;

   my $yyyy = $fYear;

   my ($sthVal, $value);

   my $sql = qq(
    SELECT CR_VALUE
       FROM CR_ALL_DATA
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?

   );

   unless ($sthVal = $dbh->prepare($sql)) {
       &display_error('Error preparing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sthVal->execute($report_id, $worksheet, $line, $column)) {
       &display_error('Error executing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

   if ($sthVal->rows) {
      $value = $sthVal->fetchrow_array;
   }

   $sthVal->finish();

   return $value;
}
#------------------------------------------------------------------------------

sub numNegFormat
{
   my ($val,$dollar) = @_;
   if($val<0){
   	$val = $val*-1;
   	$val= Currency_Format($val);
   	if($dollar){
   		$val = "(" . "\$" . $val . ")";
   	}
   	else{
   		$val = "(" . $val . ")";
   	}
   }
   else{
   	if($dollar){
   		$val= "\$" . Currency_Format($val);
   	}
   	else{
   		$val= Currency_Format($val);
   	}
   }
   return $val;
}
#------------------------------------------------------------------------------

sub USA_Format {
(my $n = shift) =~ s/\G(\d{1,3})(?=(?:\d\d\d)+(?:\.|$))/$1,/g;
return "\$$n";
}
#------------------------------------------------------------------------------

sub Currency_Format {
(my $n = shift) =~ s/\G(\d{1,3})(?=(?:\d\d\d)+(?:\.|$))/$1,/g;
return "$n";
}
#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach my $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}
#----------------------------------------------------------------------------



sub cost_report_derived {
	my ($metric, $area, @provider_list)=@_;

	my $uri = URI::Encode->new();

	my $column_name = $$metrics_fieldnames{$metric};
	my $provider_list = join (',', @provider_list);
	my %return;

	#print "Metric=$metric, Column name=$column_name<br/>\n";

	$sql = "SELECT STD($column_name) as std_deviation, AVG($column_name) as average FROM base_derived WHERE partial_year <> '1' AND $column_name IS NOT NULL and fy_year >= $lowest_year";
	#print"$sql<br/>\n";
	my $rs = &run_mysql($sql,4);
	my $std_deviation = $$rs{0}{'std_deviation'};
	my $mean = $$rs{0}{'average'};

	my $correction_factor_upper = $mean + (3 * $std_deviation);
	my $correction_factor_lower = $mean - (3 * $std_deviation);

	$sql = "SELECT rpt_rec_num, prvdr_num, fy_year AS rpt_year, $column_name FROM base_derived WHERE partial_year <> '1' AND $column_name IS NOT NULL AND $column_name < $correction_factor_upper AND $column_name > $correction_factor_lower and fy_year >= $lowest_year and prvdr_num IN ($provider_list)";
	#print"$sql<br/>\n";
	$rs = &run_mysql($sql,4);
	foreach my $k (keys %{$rs}) {
		$return{$$rs{$k}{'rpt_year'}}{"INCLUDED"}{$$rs{$k}{'prvdr_num'}} = $$rs{$k}{$column_name};
	}

	$sql = "SELECT rpt_rec_num, prvdr_num, fy_year AS rpt_year, $column_name FROM base_derived WHERE partial_year <> '1' AND $column_name IS NOT NULL AND ($column_name >= $correction_factor_upper OR $column_name <= $correction_factor_lower) and fy_year >= $lowest_year and prvdr_num IN ($provider_list)";
	#print"$sql<br/>\n";
	$rs = &run_mysql($sql,4);
	foreach my $k (keys %{$rs}) {
		$return{$$rs{$k}{'rpt_year'}}{"EXCLUDED"}{$$rs{$k}{'prvdr_num'}} = $$rs{$k}{$column_name};
	}


	my %return2;
	foreach my $year (sort {$a <=> $b}keys %return) {
		my $count = keys %{$return{$year}{"INCLUDED"}};
		my $i = 1;

		##Sort the providers ascending or descending based on '$$matrics_order{$metric}' according to the column values to put them in their percentiles.
		if ($$matrics_order{$metric} eq 'ASC') {
			foreach my $provider_num (sort {$return{$year}{"INCLUDED"}{$a} <=> $return{$year}{"INCLUDED"}{$b}} keys %{$return{$year}{"INCLUDED"}}) {

				my $quartile = &calculate_quartile($i, $count);

				$return2{$year}{$quartile}{"PROVIDERS"}{$provider_num} = $return{$year}{"INCLUDED"}{$provider_num};
				$return2{$year}{$quartile}{"TOTAL"} += $return{$year}{"INCLUDED"}{$provider_num};
				$return2{$year}{$quartile}{"COUNT"}++ ;

				$i++;
			}# end of provider_num
		}
		else {
			foreach my $provider_num (sort {$return{$year}{"INCLUDED"}{$b} <=> $return{$year}{"INCLUDED"}{$a}} keys %{$return{$year}{"INCLUDED"}}) {

				my $quartile = &calculate_quartile($i, $count);

				$return2{$year}{$quartile}{"PROVIDERS"}{$provider_num} = $return{$year}{"INCLUDED"}{$provider_num};
				$return2{$year}{$quartile}{"TOTAL"} += $return{$year}{"INCLUDED"}{$provider_num};
				$return2{$year}{$quartile}{"COUNT"}++ ;

				$i++;
			}# end of provider_num
		}

		## EXCLUSIONS AS QUARTILES
		foreach my $provider_num (sort {$return{$year}{"EXCLUDED"}{$a} <=> $return{$year}{"EXCLUDED"}{$b}} keys %{$return{$year}{"EXCLUDED"}}) {
			my $count = keys %{$return{$year}{"EXCLUDED"}};
			my $quartile = "EXCLUDED";

			$return2{$year}{$quartile}{"PROVIDERS"}{$provider_num} = $return{$year}{"EXCLUDED"}{$provider_num};
			$return2{$year}{$quartile}{"TOTAL"} += $return{$year}{"EXCLUDED"}{$provider_num};
			$return2{$year}{$quartile}{"COUNT"}++ ;
		}
	} # end of year

	my $providerid_current = $session->param("providerid");
	$sql = "SELECT rpt_rec_num, prvdr_num, fy_year AS rpt_year, $column_name FROM base_derived WHERE fy_year >= $lowest_year and prvdr_num = '$providerid_current'";
	$rs = &run_mysql($sql,4);
	foreach my $k (keys %{$rs}) {
		my $quartile = "CURRENT_PROVIDER";
		$return2{$$rs{$k}{'rpt_year'}}{$quartile}{"PROVIDERS"}{$$rs{$k}{'prvdr_num'}} = $$rs{$k}{$column_name};
	}

	my $key_name = "PROVIDER-HASH|$area|$column_name";
	$session->clear($key_name);
	$session->param($key_name, \%return2);

	#Print years in descending order.
	foreach my $year (sort {$b <=> $a} keys %return2) {

		my $bgcolor = &bgcolor($year);

		print qq{<td valign="top"><table border="1"><tr><td colspan=4 align= "left" style= "background-color:$bgcolor"><b>$year</b></td></tr>
			  <tr><td  align= "center " style= "background-color:$bgcolor">QT</td>
		      <td align= "left" style= "background-color:$bgcolor">Mean</td>
			  <td align= "left" style= "background-color:$bgcolor">Median</td>
			  <td align= "left" style= "background-color:$bgcolor">Count</td></tr>
		};

		my $year_total = 0;
		my $year_count = 0;
		my @year_data_values;
		my $year_excluded = 0;
		my $current_provider_value = $return2{$year}{"CURRENT_PROVIDER"}{"PROVIDERS"}{$providerid_current};
		my $current_provider_quartile = "";
		my %quartile_means;

		foreach my $quartile (sort {$a cmp $b} keys %{$return2{$year}}) {
			if ($quartile eq "EXCLUDED") {
				$year_excluded = keys %{$return2{$year}{$quartile}{"PROVIDERS"}};
			}
			elsif ($quartile eq "CURRENT_PROVIDER") {
			}
			else {
				my @providers = sort {$a <=> $b} keys %{$return2{$year}{$quartile}{"PROVIDERS"}};
				my $provider_set = join("|", @providers);

				my @data_values = values %{$return2{$year}{$quartile}{"PROVIDERS"}};

				#@data_values = sort {$b <=> $a} @data_values;
				#my $upper = $data_values[0];
				#my $lower = $data_values[$#data_values];
				#if ($current_provider_value <= $upper && $current_provider_value >= $lower) {
				#	$current_provider_quartile = $quartile;
				#}

				my $median = sprintf "%.2f", &calculate_median(@data_values);
				my $mean = sprintf "%.2f", ($return2{$year}{$quartile}{'TOTAL'}/$return2{$year}{$quartile}{'COUNT'});
				$quartile_means{$quartile} = $mean;

				push (@year_data_values, @data_values);
				$year_total += $return2{$year}{$quartile}{'TOTAL'};
				$year_count += $return2{$year}{$quartile}{'COUNT'};

				my $qs = $uri->encode("quartile_year=$year&quartile=$quartile&area=$area&metric=$metric");

				print qq{<tr><td  align= "left" style= "background-color:$bgcolor">$quartile</td>
				  <td  align= "right" style= "background-color:$bgcolor">$mean</td>
				  <td align= "right" style= "background-color:$bgcolor">$median</td>
				  <td align= "right" style= "background-color:$bgcolor"><a href='/cgi-bin/bron/quartile_appendix_providers.pl?$qs'>$return2{$year}{$quartile}{'COUNT'}</a></td></tr>
				};
			}
		}

		$current_provider_quartile = &calculate_current_provider_quartile($current_provider_value, $$matrics_order{$metric}, \%quartile_means);
		my $year_mean = "-";
		if($year_count != 0){
			$year_mean = sprintf "%.2f", ($year_total/$year_count);
		}
		my $year_median = sprintf "%.2f", &calculate_median(@year_data_values);
		my $total_count = $year_count + $year_excluded;

		my $qs = $uri->encode("quartile_year=$year&quartile=EXCLUDED&area=$area&metric=$metric");

		if ($current_provider_value) {
			$current_provider_value = sprintf "%.2f", (0 + $current_provider_value);
		}
		else {
			$current_provider_value = "";
		}

		print qq{<!--#Total-->
			  <tr><td style= "border-top-style: solid;border-top-color:black;background-color:$bgcolor" align="left" >TOT</td>
			  <td style= "border-top-style: solid;border-top-color:black;background-color:$bgcolor" align="right" >$year_mean</td>
			  <td style= "border-top-style: solid;border-top-color:black;background-color:$bgcolor" align="right" >$year_median</td>
			  <td style= "border-top-style: solid;border-top-color:black;background-color:$bgcolor" align="right" >$year_count</td></tr>
			<!--#Excluded Cost Report-->
			  <tr><td colspan=3 align= "left" style= "border-top-style:solid;border-top-color:black; border-top-width:2px;background-color:$bgcolor" nowrap>Excluded Cost Reports</td>
			  <td align= "right" style= "border-top-style:solid;border-top-color:black; border-top-width:2px;background-color:$bgcolor"><a href='/cgi-bin/bron/quartile_appendix_providers.pl?$qs'>$year_excluded</a></td></tr>
			<!--#Total Cost Reports-->
			  <tr><td colspan=3 align= "left" style= "border-bottom-style:solid;border-bottom-color:black; border-bottom-width:2px;background-color:$bgcolor">Total Cost Reports</td>
			  <td align= "right" style= "border-bottom-style:solid;border-bottom-color:black; border-bottom-width:2px;background-color:$bgcolor">$total_count</td></tr>


			  <tr><td  align= "left" style= "background-color:$bgcolor">$hospitalName</td>
				  <td  align= "right" style= "background-color:$bgcolor">$current_provider_value</td>
				  <td align= "right" style= "background-color:$bgcolor">$current_provider_value</td>
				  <td align= "right" style= "background-color:$bgcolor">1</td>
			  </tr>

			  <tr><td colspan="3" align= "left" style= "border-bottom-style:solid;border-bottom-color:black; border-bottom-width:2px;background-color:$bgcolor">$hospitalName QUARTILE</td>
			  <td colspan="1" align= "center" style= "border-bottom-style:solid;border-bottom-color:black; border-bottom-width:2px;background-color:$bgcolor">$current_provider_quartile</td></tr></table></td>
		};
	}
}


sub calculate_median {
	my(@data_set) = @_;
	#print "ORIG: @data_set\n";
	@data_set =	sort {$a <=> $b} (@data_set);
	#print "SORTED: @data_set\n";

	my $count = @data_set;
	my $median = 0;
	#print "COUNT: $count\n";
	#print "#DATA_SET: $#data_set\n";

	if ($count%2 == 0) {
		#$median = ($data_set[$#data_set] + $data_set[$#data_set+1])/2;
		$median = ($data_set[($count/2 - 1)] + $data_set[($count/2)]) / 2; #If count = 6 then we want count/2 = 3 and 4. (The -1 is coz of the zero-based index).
	}
	else {
		$median = $data_set[($count+1)/2 - 1]; #or just $#data_set/2 coz $#data_set = $count-1.
	}
	#print "MEDIAN: $median\n";

	return $median;
}

sub calculate_quartile {
	my ($index, $count) = @_;
	#Q1
	if ($index <= ($count/4) || ($count < 4 && $index == 1) ) {
		return '1';
	}
	#Q2
	elsif ( ($index > ($count/4) && $index <= ($count/2)) || ($count < 4 && $index == 2) ) {
		return '2';
	}
	#Q3
	elsif ( ($index > ($count/2) && $index <= (3 * $count/4)) || ($count < 4 && $index == 3) ) {
		return '3';
	}
	#Q4
	else {
		return '4';
	}
}

sub calculate_current_provider_quartile {
	my($current_provider_value, $sort_order, $quartile_means) = @_;

	my $current_quartile = "N/A";

	return $current_quartile if(!$current_provider_value);

	my ($mean_1, $mean_2, $mean_3, $mean_4);
	foreach my $quartile (keys %{$quartile_means}) {
		if ($quartile == 1) {
			$mean_1 = $$quartile_means{$quartile};
		}
		elsif ($quartile == 2) {
			$mean_2 = $$quartile_means{$quartile};
		}
		elsif ($quartile == 3) {
			$mean_3 = $$quartile_means{$quartile};
		}
		else {
			$mean_4 = $$quartile_means{$quartile};
		}
	}

	if ($sort_order eq "DESC") {
		if ($current_provider_value > (($mean_1 + $mean_2)/2)) {
			$current_quartile = 1;
		}
		if ($current_provider_value <= (($mean_1 + $mean_2)/2)) {
			$current_quartile = 2;
		}
		if ($current_provider_value <= (($mean_2 + $mean_3)/2)) {
			$current_quartile = 3;
		}
		if ($current_provider_value <= (($mean_3 + $mean_4)/2)) {
			$current_quartile = 4;
		}
	}
	else {
		if ($current_provider_value < (($mean_1 + $mean_2)/2)) {
			$current_quartile = 1;
		}
		if ($current_provider_value >= (($mean_1 + $mean_2)/2)) {
			$current_quartile = 2;
		}
		if ($current_provider_value >= (($mean_2 + $mean_3)/2)) {
			$current_quartile = 3;
		}
		if ($current_provider_value >= (($mean_3 + $mean_4)/2)) {
			$current_quartile = 4;
		}
	}

	return $current_quartile;
}

sub bgcolor {
	my ($year) = @_;
	if ($year%2 == 0) {
		return "#F5F5DC";
	}
	return "#F0FFFF";
}


