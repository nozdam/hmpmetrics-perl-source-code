#!/usr/bin/perl

use CGI;
use DBI;
use strict;
require "/var/www/cgi-bin/lib/req-milo.pl";

do "../common_header.pl";
do "../pagefooter.pl";
my $cgi = new CGI;
use Date::Calc qw(Delta_Days);
use CGI::Session;
do "../audit.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $lowest_year = $session->param("lowest_year");

my $providerid = $session->param("providerid");
#my $providerid ="171354";

if(!$userID)
{
	&sessionExpire;
}

# Get Database connection
my %ini_value;
&view("View","Generate quartile appendix report","bron");

my $hospital_name = "";

#Get the provider name
if($providerid){
	my $sql = "select hospitalname as hosp_name from tblhospitalmaster where providerno = $providerid";
	my $rs = &run_mysql($sql,4);
	$hospital_name = $$rs {0}{'hosp_name'};
}
#------------------------------------------------------------------

my $Total_Cost_Report=0;

#selected matrics
my $matrics=$session->param("matrics");

#retrieve hash for matrics battleship coordinates
my $matrics_battles=$session->param("matrics_battles");
my $metrics_fieldnames=$session->param("metrics_fieldnames");

#retrieve hash for matrics order
my $matrics_order=$session->param("matrics_order");


my $quartile = $cgi->param("quartile");
my $metric = $cgi->param("metric");
my $quartile_year = $cgi->param("quartile_year");
my $area = $cgi->param("area");
my $metric_column_name = $$metrics_fieldnames{$metric};
my $key_name = "PROVIDER-HASH|$area|$metric_column_name";
my $provider_hash = $session->param($key_name);


#Generate Report
print "Content-Type: text/html\n\n";
#foreach my $k (keys %{$metrics_fieldnames}) {
#	print "$k -- $$metrics_fieldnames{$k}<br/>\n";
#}
#print "$key_name<br\>\n";
print qq{
<HTML>
<HEAD><TITLE>Quartile Appendix Report</TITLE>
</HEAD>
<body>
<BR>
<CENTER>
};
&innerHeader();
print qq{
    <!--content start-->

        <table cellpadding="0" cellspacing="0" width="885px">
	  <tr>
	        <td width="16"><img src="/images/tableRonuded/top_lef.gif" width="16" height="16"></td>
	        <td height="16" background="/images/tableRonuded/top_mid.gif"><img src="/images/tableRonuded/top_mid.gif" width="16" height="16"></td>
	        <td width="24"><img src="/images/tableRonuded/top_rig.gif" width="24" height="16"></td>
	      </tr>
	      <tr>
	        <td width="16" background="/images/tableRonuded/cen_lef.gif"></td>
	        <td align="center" valign="middle" bgcolor="#FFFFFF">

<table bgColor="#FFFFFF" width="850px" cellpadding="0" cellspacing="0">
<tr class='gridHeader'><td nowrap colspan=3><b>Providers for Quartile $quartile of year $quartile_year for metric <i>$metric</i> excluding </b><font style="color:#C11B17;">$hospital_name ($providerid)</font> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"/cgi-bin/bron/quartile_appendix_report.pl\">Back</a><br/>
<a href="quartile_appendix_providers_csv.pl?$ENV{'QUERY_STRING'}">Download to CSV</a>
</td></tr>
<tr><td><div style= "position:static;width:885px;overflow:scroll;">
	<table border="1em" cellspacing="0" cellpadding="0"><tr><td>Provider No</td><td>Name</td><td>Address</td><td>City</td><td>State</td><td>Zip</td><td>Metric Value</td></tr>
};

#$return2{$year}{$quartile}{"PROVIDERS"}{$provider_num} = $return{$year}{"EXCLUDED"}{$provider_num};

my $count = 1;
foreach my $provider_num (sort {$$provider_hash{$quartile_year}{$quartile}{"PROVIDERS"}{$b} <=> $$provider_hash{$quartile_year}{$quartile}{"PROVIDERS"}{$a}}keys %{$$provider_hash{$quartile_year}{$quartile}{"PROVIDERS"}}) {
	my $sql = "select hospitalname, hospitalstreet, hospitalcity, hospitalstate, shortzip from tblhospitalmaster where providerno = '$provider_num'";
	my $rs = &run_mysql($sql,4);
	my $hospital_name = $$rs{0}{'hospitalname'};
	my $hospital_address = $$rs{0}{'hospitalstreet'};
	my $hospital_city = $$rs{0}{'hospitalcity'};
	my $hospital_state = $$rs{0}{'hospitalstate'};
	my $hospital_zipcode = $$rs{0}{'shortzip'};
	my $metric_value = $$provider_hash{$quartile_year}{$quartile}{"PROVIDERS"}{$provider_num};
	my $bgcolor = &bgcolor($count);
	print qq{<tr bgcolor='$bgcolor' valign="top"><td>$provider_num</td><td>$hospital_name</td><td>$hospital_address</td><td>$hospital_city</td><td>$hospital_state</td><td>$hospital_zipcode</td><td align="right">$metric_value</td></tr>};
	$count++;
}

print qq{</table></div></td></tr><tr bgColor="#EBDDE2"><td></td></tr>};
print qq{
</table>
</td>
		<td width="24" background="/images/tableRonuded/cen_rig.gif"><img src="/images/tableRonuded/cen_rig.gif" width="24" height="11"></td>
	      </tr>
	      <tr>
		    <td width="16" height="16"><img src="/images/tableRonuded/bot_lef.gif" width="16" height="16"></td>
		    <td height="16" background="/images/tableRonuded/bot_mid.gif"><img src="/images/tableRonuded/bot_mid.gif" width="16" height="16"></td>
		    <td width="24" height="16"><img src="/images/tableRonuded/bot_rig.gif" width="24" height="16"></td>
	      </tr>
   </table>
};
&TDdoc;
print qq{
</body>
</HTML>
};


sub bgcolor {
	my ($year) = @_;
	if ($year%2 == 0) {
		return "#F5F5DC";
	}
	return "#F0FFFF";
}


