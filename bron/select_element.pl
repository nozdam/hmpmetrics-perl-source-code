#!/usr/bin/perl

use CGI;
use DBI;
use strict;
do "../pagefooter.pl";
do "../common_header.pl";
my $cgi = new CGI;
require "/var/www/cgi-bin/lib/req-milo.pl";
use Date::Calc qw(Delta_Days);
use CGI::Session;

my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");



my $providerid = $session->param("providerid");
#my $providerid ="171354";

if(!$userID)
{
	&sessionExpire;
}

#------------------------------------------------------------------------------

#Array for provider List
my @Provider_List_National=();
my @Provider_List_State=();
my @Provider_List_Locale=();

#Hash for matrics battels ship coordinates
my %matrics_battles=(
	'Average Age of Plant'=>'(G000000-01201-0100 + G000000-01301-0100 + G000000-01401-0100 + G000000-01501-0100 + G000000-01601-0100 + G000000-01701-0100 + G000000-01801-0100 + G000000-01901-0100) / A700003-00500-0900',
	'Average Payment Period (Days)'=>'(G000000-03600-0100 / (G300000-00400-0100 + G300000-03000-0100 - A700003-00500-0900) / 365)',
	'Current Asset Turnover'=>'(G300000-00300-0100 / G000000-01100-0100)',
	'Current Ratio'=>'(G000000-01100-0100 / G000000-03600-0100)',
	'Days Cash On Hand'=>'((G000000-00100-0100 + G000000-00200-0200) / (G300000-00400-0100 / 365))',
	'Days Cash On Hand - All Sources'=>'((G000000-00100-0100 + G000000-00200-0200 + G000000-02200-0100) / (G300000-00400-0100 / 365))',
	'Days in Net Patient AR'=>'(G000000-00400-0100 - G000000-00600-0100) / (G300000-00300-0100 / 365)',
	'Days in Net Total AR'=>'(G000000-00400-0100 + G000000-00300-0100 + G000000-00500-0100 - G000000-00600-0100) / (G300000-00300-0100 / 365)',
	'Debt Financing Percentage'=>'(G000000-04200-0100 / G000000-02700-0100)',
	'EBIDAR Margin'=>'(G300000-03100-0100 + A700003-00500-1100 + A70003 -00500-0900 + A700003-00500-1000) / (G300000-00100-0100 + G300000-02500-0100)',
	'Equity Financing Percentage'=>'(G000000-05100-0100 + G000000-05100-0200 + G000000-05100-0300 + G000000-05100-0400) / (G000000-02700-0100 + G000000-02700-0200 + G000000-02700-0300 + G000000-02700-0400)',
	'Excess Margin'=>'(((G300000-00300-0100 - G300000-00400-0100 + G300000-02500-0100) / (G300000-00300-0100 + G300000-02500-0100)) *100)',
	'Fixed Asset Turnover'=>'G300000-00300-0100 / G000000-02100-0100',
	'Long-Term Debt to Total Assets'=>'G000000-04200-0100 / (G000000-02700-0100 - G000000-04300-0100)',
	'Medicare Capital RCC'=>'D10A181-05200-0100 / D40A180-10300-0200',
	'Medicare Margin (for general acute service only)'=>'(E00A18A-02200-0100 - D10A181-05300-0100) / (D10A181-05200-0100 - E00A18A-02200-0100)',
	'Medicare Operations RCC'=>'D10A181-05300-0100 / D40A180-10300-0200',
	'Occupancy Rate'=>'S300001-01200-0600 / S300001-01200-0200',
	'Outlier Percentage'=>'(E00A18A-00200-0100 + E00A18A-00201-0100 + L00A181-00300-0100 + L00A181-00301-0100) / (E31B181-01700-0100 + E32B181-01700-0100 + E00A18A-02200-0100 + E00A18B-03200-0100 + E30C183-05500-0200)',
	'Labor cost as a percent of Revenue'=>'((S300002-00601-0300+S300002-01500-0300+S300003-00200-0300+S300003-00600-0300)/G300000-00300-1000)*100',
	'Quick Ratio'=>'(G000000-01100-0100 - G000000-00700-0100) / G000000-03600-0100',
	'Ratio of Cost to Charges (RCC)'=>'C000001-10100-0500 / (C000001-10100-0600 + C000001-10100-0700)',
	'Return on Assets (ROA)'=>'((G300000-03100-0100 / G000000-02700-0100) * 100)',
	'Return on Equity (ROE)'=>'((G300000-03100-0100 / (G000000-02700-0100 - G000000-04300-0100)) * 100)',
	'Total Asset Turnover'=>'((G300000-00300-0100 + G300000-02500-0100) / G000000-02700-0100)',
	'Total Debt to Net Assets'=>'G000000-04300-0100 / (G000000-02700-0100 - G000000-04300-0100)',
	'Operating Margin'=>'(G300000-00500-0100-A800000-01400-0200)/G300000-00300-0100',
	'Overall Margin'=>'(G300000-03100-0100-A800000-01400-0200)/G300000-00300-0100',
	'FTEs per AOB'=>'([S300002-00601-0400+S300003-00200-0400+S300003-00600-0400]/2080)/(S300001-01200-0600+S300001-1400-0600+S300001-1401-0600+S300001-1402-0600+S300001-1403-0600+S300001-1404-0600+S300001-1405-0600+S300001-1406-0600+S300001-1407-0600+S300001-1408-0600+S300001-1409-0600+S300001-1410-0600+S300001-1411-0600+S300001-1412-0600+S300001-1413-0600+S300001-1414-0600+S300001-1415-0600+S300001-1416-0600+S300001-1417-0600+S300001-1418-0600+S300001-1419-0600+S300001-1420-0600+S300001-1421-0600+S300001-1422-0600+S300001-1423-0600+S300001-1424-0600+S300001-1425-0600+S300001-1426-0600+S300001-1427-0600+S300001-1428-0600+S300001-1429-0600+S300001-1430-0600+S300001-1431-0600+S300001-1432-0600+S300001-1433-0600+S300001-1434-0600+S300001-1435-0600+S300001-1436-0600+S300001-1437-0600+S300001-1438-0600+S300001-1439-0600+S300001-1440-0600+S300001-1441-0600+S300001-1442-0600+S300001-1443-0600+S300001-1444-0600+S300001-1445-0600+S300001-1446-0600+S300001-1447-0600+S300001-1448-0600+S300001-1449-0600+S300001-1450-0600+S300001-1451-0600+S300001-1452-0600+S300001-1453-0600+S300001-1454-0600+S300001-1455-0600+S300001-1456-0600+S300001-1457-0600+S300001-1458-0600+S300001-1459-0600+S300001-1460-0600+S300001-1461-0600+S300001-1462-0600+S300001-1463-0600+S300001-1464-0600+S300001-1465-0600+S300001-1466-0600+S300001-1467-0600+S300001-1468-0600+S300001-1469-0600+S300001-1470-0600+S300001-1471-0600+S300001-1472-0600+S300001-1473-0600+S300001-1474-0600+S300001-1475-0600+S300001-1476-0600+S300001-1477-0600+S300001-1478-0600+S300001-1479-0600+S300001-1480-0600+S300001-1481-0600+S300001-1482-0600+S300001-1483-0600+S300001-1484-0600+S300001-1485-0600+S300001-1486-0600+S300001-1487-0600+S300001-1488-0600+S300001-1489-0600+S300001-1490-0600+S300001-1491-0600+S300001-1492-0600+S300001-1493-0600+S300001-1494-0600+S300001-1495-0600+S300001-1496-0600+S300001-1497-0600+S300001-1498-0600+S300001-1499-0600+S300001-1500-0600+S300001-1501-0600+S300001-1502-0600+S300001-1503-0600+S300001-1504-0600+S300001-1505-0600+S300001-1506-0600+S300001-1507-0600+S300001-1508-0600+S300001-1509-0600+S300001-1510-0600+S300001-1511-0600+S300001-1512-0600+S300001-1513-0600+S300001-1514-0600+S300001-1515-0600+S300001-1516-0600+S300001-1517-0600+S300001-1518-0600+S300001-1519-0600+S300001-1520-0600+S300001-1521-0600+S300001-1522-0600+S300001-1523-0600+S300001-1524-0600+S300001-1525-0600+S300001-1526-0600+S300001-1527-0600+S300001-1528-0600+S300001-1529-0600+S300001-1530-0600+S300001-1531-0600+S300001-1532-0600+S300001-1533-0600+S300001-1534-0600+S300001-1535-0600+S300001-1536-0600+S300001-1537-0600+S300001-1538-0600+S300001-1539-0600+S300001-1540-0600+S300001-1541-0600+S300001-1542-0600+S300001-1543-0600+S300001-1544-0600+S300001-1545-0600+S300001-1546-0600+S300001-1547-0600+S300001-1548-0600+S300001-1549-0600+S300001-1550-0600+S300001-1551-0600+S300001-1552-0600+S300001-1553-0600+S300001-1554-0600+S300001-1555-0600+S300001-1556-0600+S300001-1557-0600+S300001-1558-0600+S300001-1559-0600+S300001-1560-0600+S300001-1561-0600+S300001-1562-0600+S300001-1563-0600+S300001-1564-0600+S300001-1565-0600+S300001-1566-0600+S300001-1567-0600+S300001-1568-0600+S300001-1569-0600+S300001-1570-0600+S300001-1571-0600+S300001-1572-0600+S300001-1573-0600+S300001-1574-0600+S300001-1575-0600+S300001-1576-0600+S300001-1577-0600+S300001-1578-0600+S300001-1579-0600+S300001-1580-0600+S300001-1581-0600+S300001-1582-0600+S300001-1583-0600+S300001-1584-0600+S300001-1585-0600+S300001-1586-0600+S300001-1587-0600+S300001-1588-0600+S300001-1589-0600+S300001-1590-0600+S300001-1591-0600+S300001-1592-0600+S300001-1593-0600+S300001-1594-0600+S300001-1595-0600+S300001-1596-0600+S300001-1597-0600+S300001-1598-0600+S300001-1599-0600+S300001-1600-0600+S300001-1601-0600+S300001-1602-0600+S300001-1603-0600+S300001-1604-0600+S300001-1605-0600+S300001-1606-0600+S300001-1607-0600+S300001-1608-0600+S300001-1609-0600+S300001-1610-0600+S300001-1611-0600+S300001-1612-0600+S300001-1613-0600+S300001-1614-0600+S300001-1615-0600+S300001-1616-0600+S300001-1617-0600+S300001-1618-0600+S300001-1619-0600+S300001-1620-0600+S300001-1621-0600+S300001-1622-0600+S300001-1623-0600+S300001-1624-0600+S300001-1625-0600+S300001-1626-0600+S300001-1627-0600+S300001-1628-0600+S300001-1629-0600+S300001-1630-0600+S300001-1631-0600+S300001-1632-0600+S300001-1633-0600+S300001-1634-0600+S300001-1635-0600+S300001-1636-0600+S300001-1637-0600+S300001-1638-0600+S300001-1639-0600+S300001-1640-0600+S300001-1641-0600+S300001-1642-0600+S300001-1643-0600+S300001-1644-0600+S300001-1645-0600+S300001-1646-0600+S300001-1647-0600+S300001-1648-0600+S300001-1649-0600+S300001-1650-0600+S300001-1651-0600+S300001-1652-0600+S300001-1653-0600+S300001-1654-0600+S300001-1655-0600+S300001-1656-0600+S300001-1657-0600+S300001-1658-0600+S300001-1659-0600+S300001-1660-0600+S300001-1661-0600+S300001-1662-0600+S300001-1663-0600+S300001-1664-0600+S300001-1665-0600+S300001-1666-0600+S300001-1667-0600+S300001-1668-0600+S300001-1669-0600+S300001-1670-0600+S300001-1671-0600+S300001-1672-0600+S300001-1673-0600+S300001-1674-0600+S300001-1675-0600+S300001-1676-0600+S300001-1677-0600+S300001-1678-0600+S300001-1679-0600+S300001-1680-0600+S300001-1681-0600+S300001-1682-0600+S300001-1683-0600+S300001-1684-0600+S300001-1685-0600+S300001-1686-0600+S300001-1687-0600+S300001-1688-0600+S300001-1689-0600+S300001-1690-0600+S300001-1691-0600+S300001-1692-0600+S300001-1693-0600+S300001-1694-0600+S300001-1695-0600+S300001-1696-0600+S300001-1697-0600+S300001-1698-0600+S300001-1699-0600+S300001-1700-0600+S300001-1701-0600+S300001-1702-0600+S300001-1703-0600+S300001-1704-0600+S300001-1705-0600+S300001-1706-0600+S300001-1707-0600+S300001-1708-0600+S300001-1709-0600+S300001-1710-0600+S300001-1711-0600+S300001-1712-0600+S300001-1713-0600+S300001-1714-0600+S300001-1715-0600+S300001-1716-0600+S300001-1717-0600+S300001-1718-0600+S300001-1719-0600+S300001-1720-0600+S300001-1721-0600+S300001-1722-0600+S300001-1723-0600+S300001-1724-0600+S300001-1725-0600+S300001-1726-0600+S300001-1727-0600+S300001-1728-0600+S300001-1729-0600+S300001-1730-0600+S300001-1731-0600+S300001-1732-0600+S300001-1733-0600+S300001-1734-0600+S300001-1735-0600+S300001-1736-0600+S300001-1737-0600+S300001-1738-0600+S300001-1739-0600+S300001-1740-0600+S300001-1741-0600+S300001-1742-0600+S300001-1743-0600+S300001-1744-0600+S300001-1745-0600+S300001-1746-0600+S300001-1747-0600+S300001-1748-0600+S300001-1749-0600+S300001-1750-0600+S300001-1751-0600+S300001-1752-0600+S300001-1753-0600+S300001-1754-0600+S300001-1755-0600+S300001-1756-0600+S300001-1757-0600+S300001-1758-0600+S300001-1759-0600+S300001-1760-0600+S300001-1761-0600+S300001-1762-0600+S300001-1763-0600+S300001-1764-0600+S300001-1765-0600+S300001-1766-0600+S300001-1767-0600+S300001-1768-0600+S300001-1769-0600+S300001-1770-0600+S300001-1771-0600+S300001-1772-0600+S300001-1773-0600+S300001-1774-0600+S300001-1775-0600+S300001-1776-0600+S300001-1777-0600+S300001-1778-0600+S300001-1779-0600+S300001-1780-0600+S300001-1781-0600+S300001-1782-0600+S300001-1783-0600+S300001-1784-0600+S300001-1785-0600+S300001-1786-0600+S300001-1787-0600+S300001-1788-0600+S300001-1789-0600+S300001-1790-0600+S300001-1791-0600+S300001-1792-0600+S300001-1793-0600+S300001-1794-0600+S300001-1795-0600+S300001-1796-0600+S300001-1797-0600+S300001-1798-0600+S300001-1799-0600)/365/(C000001-10100-0600/(C000001-10100-0600+C000001-10100-0700))'

	);

$session->param("matrics_battles", \%matrics_battles);


#Hash for matrics order
my %matrics_order=(
	'Average Age of Plant'=>'ASC',
	'Average Payment Period (Days)'=>'ASC',
	'Current Asset Turnover'=>'DESC',
	'Current Ratio'=>'DESC',
	'Days Cash On Hand'=>'DESC',
	'Days Cash On Hand - All Sources'=>'DESC',
	'Days in Net Patient AR'=>'ASC',
	'Days in Net Total AR'=>'ASC',
	'Debt Financing Percentage'=>'ASC',
	'EBIDAR Margin'=>'DESC',
	'Equity Financing Percentage'=>'ASC',
	'Excess Margin'=>'DESC',
	'Fixed Asset Turnover'=>'ASC',
	'Long-Term Debt to Total Assets'=>'ASC',
	'Medicare Capital RCC'=>'ASC',
	'Medicare Margin (for general acute service only)'=>'DESC',
	'Medicare Operations RCC'=>'ASC',
	'Occupancy Rate'=>'DESC',
	'Outlier Percentage'=>'DESC',
	'Labor cost as a percent of Revenue'=>'ASC',
	'Quick Ratio'=>'DESC',
	'Ratio of Cost to Charges (RCC)'=>'ASC',
	'Return on Assets (ROA)'=>'DESC',
	'Return on Equity (ROE)'=>'DESC',
	'Total Asset Turnover'=>'ASC',
	'Total Debt to Net Assets'=>'ASC',
	'Operating Margin'=>'DESC',
	'Overall Margin'=>'DESC',
	'FTEs per AOB'=>'ASC'
	);
$session->param("matrics_order", \%matrics_order);

my %metrics_fieldnames =(
	'Average Age of Plant'=>'avg_age_of_plant',
	'Average Payment Period (Days)'=>'average_payment_period',
	'Current Asset Turnover'=>'current_asset_turnover',
	'Current Ratio'=>'current_ratio',
	'Days Cash On Hand'=>'days_cash_on_hand',
	'Days Cash On Hand - All Sources'=>'days_cash_on_hand_all_sources',
	'Days in Net Patient AR'=>'days_in_net_patient_ar',
	'Days in Net Total AR'=>'days_in_net_total_ar',
	'Debt Financing Percentage'=>'debt_financing_percentage',
	'EBIDAR Margin'=>'ebidar_margin',
	'Equity Financing Percentage'=>'equity_financing_percentage',
	'Excess Margin'=>'excess_margin',
	'Fixed Asset Turnover'=>'fixed_asset_turnover',
	'Long-Term Debt to Total Assets'=>'long_term_debt_to_total_assets',
	'Medicare Capital RCC'=>'medicare_capital_rcc',
	'Medicare Margin (for general acute service only)'=>'medicare_margin_general_accute',
	'Medicare Operations RCC'=>'medicare_operations_rcc',
	'Occupancy Rate'=>'occupancy_rate',
	'Outlier Percentage'=>'outlier_percentage',
	'Labor cost as a percent of Revenue'=>'labor_cost_as_pct_of_revenue',
	'Quick Ratio'=>'quick_ratio',
	'Ratio of Cost to Charges (RCC)'=>'ratio_of_cost_to_charges_rcc',
	'Return on Assets (ROA)'=>'return_on_assets',
	'Return on Equity (ROE)'=>'return_on_equity',
	'Total Asset Turnover'=>'total_asset_turnover',
	'Total Debt to Net Assets'=>'total_debt_to_net_assets',
	'Operating Margin'=>'operating_margin',
	'Overall Margin'=>'overall_margin',
	'FTEs per AOB'=>'ftes_per_aob'
);
$session->param("metrics_fieldnames", \%metrics_fieldnames);

$session->param("lowest_year", 2005);

	#Retrieve data from the search page
	my $min_beds;
	my $max_beds;
	my $min_ftes;
	my $max_ftes;
	my $min_cmi;
	my $max_cmi;
	#System Affilation
	my $sa_Select_All;
	my $sa_NA;
	my $sa_small;
	my $sa_medium;
	my $sa_large;
	#Type of Control
	my $toc_SelectAll;
	my $toc_Governmental;
	my $toc_ForProfit;
	my $toc_NonProfit;
	my $toc_NonProfitChurch;
	#Teaching Status
	my $ts_SelectAll;
	my $ts_NA;
	my $ts_Minor;
	my $ts_Major;
	my $ts_Complex;
	#Hospital Type
	my $ht_SelectAll;
	my $ht_GeneralAcute;
	my $ht_Cancer;
	my $ht_Children;
	my $ht_LongTermCare;
	my $ht_Psychiatric;
	my $ht_Rehabilitation;

	my $critical_access_n;
	my $critical_access_y;
	my $has_management_n;
	my $has_management_y;



my $retain_value="FALSE";
if($cgi->param("retain_value")){
	$retain_value=$cgi->param("retain_value");
}


$critical_access_n = $cgi->param("critical_access_n");
$critical_access_y = $cgi->param("critical_access_y");
$has_management_n = $cgi->param("has_management_n");
$has_management_y = $cgi->param("has_management_y");
$session->param("critical_access_n",$critical_access_n);
$session->param("critical_access_y",$critical_access_y);
$session->param("has_management_n",$has_management_n);
$session->param("has_management_y",$has_management_y);

#Storing search data in session
if($retain_value eq "FALSE"){
	$min_beds=$cgi->param("min_bed");
	$max_beds=$cgi->param("max_bed");
	$min_ftes=$cgi->param("min_fte");
	$max_ftes=$cgi->param("max_fte");
	$min_cmi=$cgi->param("min_cmi");
	$max_cmi=$cgi->param("max_cmi");
	$toc_SelectAll=$cgi->param("toc_SelectAll");
	$toc_Governmental=$cgi->param("toc_Governmental");
	$toc_ForProfit=$cgi->param("toc_ForProfit");
	$toc_NonProfit=$cgi->param("toc_NonProfit");
	$toc_NonProfitChurch=$cgi->param("toc_NonProfitChurch");

	#System Affilation
	$sa_Select_All = $cgi->param("sa_Select_All");
	$sa_NA = $cgi->param("sa_NA");
	$sa_small = $cgi->param("sa_small");
	$sa_medium = $cgi->param("sa_medium");
	$sa_large = $cgi->param("sa_large");

	#Teaching Status
	$ts_SelectAll=$cgi->param("ts_Select_All");
	$ts_NA=$cgi->param("ts_NA");
	$ts_Minor=$cgi->param("ts_Minor");
	$ts_Major=$cgi->param("ts_Major");
	$ts_Complex=$cgi->param("ts_Complax");

	#Hospital Type
	$ht_SelectAll=$cgi->param("ht_SelectAll");
	$ht_GeneralAcute=$cgi->param("ht_GeneralAcute");
	$ht_Cancer=$cgi->param("ht_Cancer");
	$ht_Children=$cgi->param("ht_Children");
	$ht_LongTermCare=$cgi->param("ht_LongTermCare");
	$ht_Psychiatric=$cgi->param("ht_Psychiatric");
	$ht_Rehabilitation=$cgi->param("ht_Rehabilitation");
	$session->param("min_bed",$min_beds);
	$session->param("max_bed",$max_beds);
	$session->param("min_fte",$min_ftes);
	$session->param("max_fte",$max_ftes);
	$session->param("min_cmi",$min_cmi);
	$session->param("max_cmi",$max_cmi);

	#System Affilation
	$session->param("sa_Select_All",$sa_Select_All);
	$session->param("sa_NA",$sa_NA);
	$session->param("sa_small",$sa_small);
	$session->param("sa_medium",$sa_medium);
	$session->param("sa_large",$sa_large);

	#Type of Control
	$session->param("toc_SelectAll",$toc_SelectAll);
	$session->param("toc_Governmental",$toc_Governmental);
	$session->param("toc_ForProfit",$toc_ForProfit);
	$session->param("toc_NonProfit",$toc_NonProfit);
	$session->param("toc_NonProfitChurch",$toc_NonProfitChurch);
	#Teaching Status
	$session->param("ts_SelectAll",$ts_SelectAll);
	$session->param("ts_NA",$ts_NA);
	$session->param("ts_Minor",$ts_Minor);
	$session->param("ts_Major",$ts_Major);
	$session->param("ts_Complax",$ts_Complex);

	$session->param("ht_SelectAll",$ht_SelectAll);
	$session->param("ht_GeneralAcute",$ht_GeneralAcute);
	$session->param("ht_Cancer",$ht_Cancer);
	$session->param("ht_Children",$ht_Children);
	$session->param("ht_LongTermCare",$ht_LongTermCare);
	$session->param("ht_Psychiatric",$ht_Psychiatric);
	$session->param("ht_Rehabilitation",$ht_Rehabilitation);

}
else{
	$min_beds=$session->param("min_bed");
	$max_beds=$session->param("max_bed");
	$min_ftes=$session->param("min_fte");
	$max_ftes=$session->param("max_fte");
	$min_cmi=$session->param("min_cmi");
	$max_cmi=$session->param("max_cmi");
	$toc_SelectAll=$session->param("toc_SelectAll");
	$toc_Governmental=$session->param("toc_Governmental");
	$toc_ForProfit=$session->param("toc_ForProfit");
	$toc_NonProfit=$session->param("toc_NonProfit");
	$toc_NonProfitChurch=$session->param("toc_NonProfitChurch");
	$ht_SelectAll=$session->param("ht_SelectAll");
	$ht_GeneralAcute=$session->param("ht_GeneralAcute");
	$ht_Cancer=$session->param("ht_Cancer");
	$ht_Children=$session->param("ht_Children");
	$ht_LongTermCare=$session->param("ht_LongTermCare");
	$ht_Psychiatric=$session->param("ht_Psychiatric");
	$ht_Rehabilitation=$session->param("ht_Rehabilitation");

	#Teaching Status
	$ts_SelectAll = $session->param("ts_SelectAll",$ts_SelectAll);
	$ts_NA = $session->param("ts_NA",$ts_NA);
	$ts_Minor = $session->param("ts_Minor",$ts_Minor);
	$ts_Major = $session->param("ts_Major",$ts_Major);
	$ts_Complex = $session->param("ts_Complax",$ts_Complex);
}
open(D,">Data.txt");
print D $toc_SelectAll.$toc_Governmental.$toc_ForProfit.$toc_NonProfit.$toc_NonProfitChurch;
close(D);
# Database

my $dbh;
my ($sql, $sth,$stmt);


# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};

my ($years,$yearLoopCount,$htmlData,$hospitalName, $State, $MSA);

$yearLoopCount = 0;
@fYears = reverse(@fYears);
foreach my $fYears (@fYears){
	if($yearLoopCount <5){
		if($years){
			$years = $years . "," . $fYears;
		}
		else{
			$years = $fYears;
		}
	}
	$yearLoopCount = $yearLoopCount + 1;
}

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

#Get the provider name
if($providerid){
	$sql = "Select h.HospitalName as HOSP_NAME, h.HospitalState as STATE, z.msa from tblhospitalmaster as h, ZIPCODES as z where h.ShortZip=z.zipcode AND ProviderNo = $providerid";
	$stmt = $dbh->prepare($sql);
	$stmt->execute();
	while (my $row = $stmt->fetchrow_hashref) {
		$hospitalName = $$row{HOSP_NAME};
		$State=$$row{STATE};
		$MSA=$$row{msa};
	}
}


#Execute Query for provider is urban orrural
my $urban_rural=&urban_rural();
#Execute query for getting Provider List for National, State and Locale
&query_database();

#Generate Report

my(@rpt_rec_num,@rpt_year,@fy_bgn_dt,@fy_end_dt,$recCount);
$recCount = 1;
if($providerid){
	$sql = "SELECT RPT_REC_NUM, year(FY_END_DT) as RPT_YEAR, RPT_STUS_CD, FI_NUM, FY_BGN_DT, FY_END_DT FROM CR_ALL_RPT WHERE PRVDR_NUM = $providerid AND year(FY_END_DT)  in ($years) ORDER BY RPT_YEAR desc ";
	$sth = $dbh->prepare($sql);
	$sth->execute();
	while (my $row = $sth->fetchrow_hashref) {
		push (@rpt_rec_num, $$row{RPT_REC_NUM});
		push (@rpt_year, $$row{RPT_YEAR});
		push (@fy_bgn_dt, $$row{FY_BGN_DT});
		push (@fy_end_dt, $$row{FY_END_DT});
		$recCount++;
	}

	#Starting a new Row
	$htmlData = $htmlData . "<tr><td align=\"center\"><table width=\"100%\" border=0 cellpadding=1 cellspacing=0>";

	#Matrics
	$htmlData = $htmlData . "<tr bgcolor=\"#98AFC7\"><td colspan=2 align=\"center\"><b>PROFITABILITY</b></td>";
	#$htmlData = $htmlData . "<td width=5></td>";
	$htmlData = $htmlData . "<td colspan=2 align=\"center\"><b>LIQUIDITY</b></td>";

	$htmlData = $htmlData . "<tr bgcolor=\"#F0FFFF\">";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk0\" value=\"EBIDAR Margin\"/></td><td align=\"left\">EBIDAR Margin</td>";
	#$htmlData = $htmlData . "<td width=5></td>";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk1\" value=\"Average Age of Plant\"/></td><td>Average Age of Plant</td>";
	$htmlData = $htmlData . "</tr>";
	$htmlData = $htmlData . "<tr bgcolor=\"#F5F5DC\"><td><input type=\"checkbox\" name=\"chk2\" value=\"Excess Margin\"/></td><td align=\"left\" nowrap>Excess Margin</td>";
	#$htmlData = $htmlData . "";
	#$htmlData = $htmlData . "<td width=5></td>";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk3\" value=\"Average Payment Period (Days)\"/></td><td nowrap>Average Payment Period (Days)</td>";
	$htmlData = $htmlData . "</tr>";
	$htmlData = $htmlData . "<tr bgcolor=\"#F0FFFF\">";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk4\" value=\"Medicare Capital RCC\"/></td><td align=\"left\">Medicare Capital RCC</td>";
	#$htmlData = $htmlData . "<td width=5></td>";
	#$htmlData = $htmlData . "";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk5\" value=\"Current Ratio\"/></td><td>Current Ratio</td></tr>";
	$htmlData = $htmlData . "<tr bgcolor=\"#F5F5DC\">";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk6\" value=\"Medicare Margin (for general acute service only)\"/></td><td align=\"left\" nowrap>Medicare Margin (for general acute service only)</td>";
	#$htmlData = $htmlData . "<td width=5></td>";
	#$htmlData = $htmlData . "";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk7\" value=\"Days Cash On Hand\"/></td><td>Days Cash On Hand</td></tr>";
	$htmlData = $htmlData . "<tr bgcolor=\"#F0FFFF\">";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk8\" value=\"Medicare Operations RCC\"/></td><td align=\"left\">Medicare Operations RCC</td>";
	#$htmlData = $htmlData . "<td width=5></td>";
	#$htmlData = $htmlData . "";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk9\" value=\"Days Cash On Hand - All Sources\"/></td><td>Days Cash On Hand - All Sources</td></tr>";
	$htmlData = $htmlData . "<tr bgcolor=\"#F5F5DC\">";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk10\" value=\"Outlier Percentage\"/></td><td align=\"left\">Outlier Percentage</td>";
	#$htmlData = $htmlData . "<td width=5></td>";
	#$htmlData = $htmlData . "";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk11\" value=\"Days in Net Patient AR\"/></td><td>Days in Net Patient AR</td></tr>";
	$htmlData = $htmlData . "<tr bgcolor=\"#F0FFFF\">";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk12\" value=\"Labor cost as a percent of Revenue\"/></td><td align=\"left\" nowrap>Labor cost as a percent of Revenue</td>";
	#$htmlData = $htmlData . "<td width=5></td>";
	#$htmlData = $htmlData . "";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk13\" value=\"Days in Net Total AR\"/></td><td>Days in Net Total AR</td></tr>";
	$htmlData = $htmlData . "<tr bgcolor=\"#F5F5DC\">";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk14\" value=\"Ratio of Cost to Charges (RCC)\"/></td><td align=\"left\">Ratio of Cost to Charges (RCC)</td>";
	#$htmlData = $htmlData . "<td width=5></td>";
	#$htmlData = $htmlData . "";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk15\" value=\"Quick Ratio\"/></td><td>Quick Ratio</td></tr>";
	$htmlData = $htmlData . "<tr>";
	$htmlData = $htmlData . "<td style=\"background-color:#F0FFFF\"><input type=\"checkbox\" name=\"chk16\" value=\"Return on Assets (ROA)\"/></td><td align=\"left\" style=\"background-color:#F0FFFF\">Return on Assets (ROA)</td>";
	#$htmlData = $htmlData . "<td width=5></td>";

	$htmlData = $htmlData . "<td style=\"background-color:#98AFC7\" colspan=2 align=\"center\"><b>ACTIVITY</b></td></tr>";
	$htmlData = $htmlData . "<tr bgcolor=\"#F5F5DC\">";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk17\" value=\"Return on Equity (ROE)\"/></td><td>Return on Equity (ROE)</td>";
	#$htmlData = $htmlData . "<td width=5></td>";
	#$htmlData = $htmlData . "";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk18\" value=\"Current Asset Turnover\"/></td><td align=\"left\">Current Asset Turnover</td></tr>";
	$htmlData = $htmlData . "<tr bgcolor=\"#F0FFFF\">";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk19\" value=\"Operating Margin\"/></td><td align=\"left\">Operating Margin</td>";
	#$htmlData = $htmlData . "<td width=5></td>";
	#$htmlData = $htmlData . "";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk20\" value=\"Fixed Asset Turnover\"/></td><td>Fixed Asset Turnover</td></tr>";
	$htmlData = $htmlData . "<tr bgcolor=\"#F5F5DC\">";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk21\" value=\"Overall Margin\"/></td><td align=\"left\">Overall Margin</td>";
	#$htmlData = $htmlData . "<td width=5></td>";
	#$htmlData = $htmlData . "";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk22\" value=\"Total Asset Turnover\"/></td><td>Total Asset Turnover</td></tr>";

	$htmlData = $htmlData . "<tr bgcolor=\"#98AFC7\"><td colspan=2 align=\"center\"><b>CAPITAL STRUCTURE</b></td>";
	#$htmlData = $htmlData . "<td width=5></td>";
	$htmlData = $htmlData . "<td colspan=2 align=\"center\"><b>OTHER</b></td></tr>";

	$htmlData = $htmlData . "<tr bgcolor=\"#F5F5DC\">";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk23\" value=\"Debt Financing Percentage\"/></td><td align=\"left\">Debt Financing Percentage</td>";
	#$htmlData = $htmlData . "<td width=5></td>";
	#$htmlData = $htmlData . "";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk30\" value=\"Occupancy Rate\"/></td><td>Occupancy Rate</td></tr>";
	$htmlData = $htmlData . "<tr bgcolor=\"#F0FFFF\">";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk25\" value=\"Equity Financing Percentage\"/></td><td align=\"left\">Equity Financing Percentage</td>";
	#$htmlData = $htmlData . "<td width=5></td>";
	#$htmlData = $htmlData . "";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk29\" value=\"Total Debt to Net Assets\"/></td><td align=\"left\">Total Debt to Net Assets</td></tr>";
	$htmlData = $htmlData . "<tr bgcolor=\"#F5F5DC\">";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk27\" value=\"Long-Term Debt to Total Assets\"/></td><td align=\"left\">Long-Term Debt to Total Assets</td>";
	#$htmlData = $htmlData . "<td width=5></td>";
	#$htmlData = $htmlData . "";
	$htmlData = $htmlData . "<td><input type=\"checkbox\" name=\"chk28\" value=\"FTEs per AOB\"/></td><td>FTEs per AOB</td></tr>";


	$htmlData = $htmlData . "</table>";
	$htmlData = $htmlData . "<tr><td align=\"center\" style=\"padding-top:5px\"><span class=\"button\"><input type=\"button\" value=\"Back\" onClick=\"window.location.href='/cgi-bin/Milo2/search.pl'\"/></span>&nbsp;&nbsp;<span class=\"button\"><input type=\"Submit\" value=\"Next\" /></span></td></tr></td></tr>";



}else{
	$htmlData="<tr><td>Please select provider number and run report again.<a href=\"/cgi-bin/Milo2/index.pl\">Back</a></td></tr>";
}
print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>Metrics</TITLE>
<SCRIPT SRC="/JS/sorttable.js"></SCRIPT>
<SCRIPT SRC="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<SCRIPT SRC="/JS/dropmenu.js"></SCRIPT>
<link rel="stylesheet" href="/css/dropmenu.css" type="text/css" />
</HEAD>
<body>
<BR>
<FORM NAME = "frmFinacial" Action="/cgi-bin/Milo2/SelectReport.pl" method="post">
<CENTER>
};
&innerHeader();
print qq{

    <!--content start-->

        <table cellpadding="0" cellspacing="0" width="885px">
	  <tr>
	        <td width="16"><img src="/images/tableRonuded/top_lef.gif" width="16" height="16"></td>
	        <td height="16" background="/images/tableRonuded/top_mid.gif"><img src="/images/tableRonuded/top_mid.gif" width="16" height="16"></td>
	        <td width="24"><img src="/images/tableRonuded/top_rig.gif" width="24" height="16"></td>
	      </tr>
	      <tr>
	        <td width="16" background="/images/tableRonuded/cen_lef.gif"></td>
	        <td align="center" valign="middle" bgcolor="#FFFFFF">

<table border="1em" bgColor="#FFFFFF" width="100%" cellpadding="0" cellspacing="0">
<tr class='gridHeader'><td nowrap colspan=$recCount><b>Select Metrics : </b><font style="color:#C11B17;">$providerid  -  $hospitalName</font></td></tr>
$htmlData
</table>
</td>
		<td width="24" background="/images/tableRonuded/cen_rig.gif"><img src="/images/tableRonuded/cen_rig.gif" width="24" height="11"></td>
	      </tr>
	      <tr>
		    <td width="16" height="16"><img src="/images/tableRonuded/bot_lef.gif" width="16" height="16"></td>
		    <td height="16" background="/images/tableRonuded/bot_mid.gif"><img src="/images/tableRonuded/bot_mid.gif" width="16" height="16"></td>
		    <td width="24" height="16"><img src="/images/tableRonuded/bot_rig.gif" width="24" height="16"></td>
	      </tr>
   </table>
};
	&TDdoc;
print qq{
</body>
</HTML>
};
#--------------------------------------------------------------------------------------
sub sessionExpire
{

print "Content-Type: text/html\n\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};
&headerScript();
print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}
#------------------------------------------------------------------------------
sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and left spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#--------------------------------------------------------------------------------

sub getFormulae
{
	my ($report_id, $yyyy, $formulae) = @_;
	my ($value);
	#$value = &getFormulaeValue($report_id,"(G200000-002500-0200)/(G200000-002500-0300)",$yyyy);

	$value = &getFormulaeValue($report_id,$formulae,$yyyy);

	#EBITDAR as a Percent of Interest Expense
	#Net Revenue per FTE

	return $value;
}


#----------------------------------------------------------------------------

sub getFormulaeValue
{
	my ($report_id,$formulae,$yyyy) = @_;
	my ($value, $i, $elementVal, $operation);

	for ($i=0;$i<length($formulae);$i++)
	{
		if(substr($formulae, $i,1) =~ /[A-Z]/ )
		{
			#Spliting elements from Address
			my ($worksheet, $line, $column) = split /[-:]/,substr($formulae, $i,18);
			my $battleship_value = &get_value($report_id, $worksheet, $line, $column, $yyyy);
			if($battleship_value)
			{
				$elementVal = $battleship_value;
			}
			else
			{
				$elementVal = 0;
			}


			$value = $value.$elementVal;
			$i = $i+17;
		}
		elsif(substr($formulae, $i,1) =~ /\s/ )
		{

		}
		else
		{

			$value = $value . substr($formulae, $i,1);
		}
	}
	$value = eval($value);
	if(!$value)
	{
		$value = "0";
	}
	else
	{
		#$value = sprintf "%.2f", $value;
		$value = $value;
	}
	return $value;

}

#------------------------------------------------------------------------------

sub get_value
{
   my ($report_id, $worksheet, $line, $column, $fYear) = @_;

   my $yyyy = $fYear;

   my ($sthVal, $value);

   my $sql = qq(
    SELECT CR_VALUE
      FROM CR_ALL_DATA
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
   );

   unless ($sthVal = $dbh->prepare($sql)) {
       &display_error('Error preparing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sthVal->execute($report_id, $worksheet, $line, $column)) {
       &display_error('Error executing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

   if ($sthVal->rows) {
      $value = $sthVal->fetchrow_array;
   }

   $sthVal->finish();

   return $value;
}

#------------------------------------------------------------------------------

sub numNegFormat
{
   my ($val,$dollar) = @_;
   if($val < 0){
   	$val = $val*-1;
   	$val= Currency_Format($val);
   	if($dollar){
   		$val = "(" . "\$" . $val . ")";
   	}
   	else{
   		$val = "(" . $val . ")";
   	}
   }
   else{
   	if($dollar){
   		$val= "\$" . Currency_Format($val);
   	}
   	else{
   		$val= Currency_Format($val);
   	}
   }
   return $val;
}

#------------------------------------------------------------------------------

sub USA_Format {
(my $n = shift) =~ s/\G(\d{1,3})(?=(?:\d\d\d)+(?:\.|$))/$1,/g;
return "\$$n";
}

#------------------------------------------------------------------------------

sub Currency_Format {
(my $n = shift) =~ s/\G(\d{1,3})(?=(?:\d\d\d)+(?:\.|$))/$1,/g;
return "$n";
}


#--------------------------------------------------------------------------------
sub query_database
{
	#my $query = "select distinct b.prvdr_num from _tbl_provider_metrics b, hath h, cmi c, zipcodes z where  h.rpt_rec_num = b.rpt_rec_num and b.prvdr_num = c.provider_number and b.fy_year = c.year and b.shortzip = z.zipcode and b.prvdr_num NOT IN (select distinct providerno from hath where bmid = (select distinct bmid from hath where providerno = '$providerid'))";

	my $lowest_year = $session->param("lowest_year");

	my $query = "select distinct b.prvdr_num from _tbl_provider_metrics b left outer join vw_hath_systemmaster h on h.rpt_rec_num = b.rpt_rec_num left outer join cmi c on b.prvdr_num = c.provider_number and b.fy_year = c.year left outer join zipcodes z on b.shortzip = z.zipcode where b.fy_year >= $lowest_year and b.prvdr_num NOT IN (select distinct providerno from hath where bmid = (select distinct bmid from hath where providerno = '$providerid'))";

	if (!$ts_SelectAll) {
		$query .= " and ( ";
		my $sub_clause = "";
		if ($ts_NA) {
			$sub_clause .= ((length($sub_clause) == 0) ? ' ' : ' OR ');
			$sub_clause .= " teaching_status is null ";
		}
		if ($ts_Minor) {
			$sub_clause .= ((length($sub_clause) == 0) ? ' ' : ' OR ');
			$sub_clause .= " teaching_status < 25 ";
		}
		if ($ts_Major) {
			$sub_clause .= ((length($sub_clause) == 0) ? ' ' : ' OR ');
			$sub_clause .= " (teaching_status >= 25 and teaching_status < 100) ";
		}
		if ($ts_Complex) {
			$sub_clause .= ((length($sub_clause) == 0) ? ' ' : ' OR ');
			$sub_clause .= " teaching_status >= 100 ";
		}
		$query .= $sub_clause;
		$query .= " ) ";
	}

	if (!$ht_SelectAll) {
		$query .= " and ( ";
		my $sub_clause = "";
		if ($ht_GeneralAcute) {
			$sub_clause .= (length($sub_clause) == 0) ? ' ' : ' OR ';
			$sub_clause .= " hmpfacilitytype = 1 ";
		}
		if ($ht_Cancer) {
			$sub_clause .= (length($sub_clause) == 0) ? ' ' : ' OR ';
			$sub_clause .= " hmpfacilitytype = 3 ";
		}
		if ($ht_Children) {
			$sub_clause .= (length($sub_clause) == 0) ? ' ' : ' OR ';
			$sub_clause .= " hmpfacilitytype = 7 ";
		}
		if ($ht_LongTermCare) {
			$sub_clause .= (length($sub_clause) == 0) ? ' ' : ' OR ';
			$sub_clause .= " hmpfacilitytype = 2 ";
		}
		if ($ht_Psychiatric) {
			$sub_clause .= (length($sub_clause) == 0) ? ' ' : ' OR ';
			$sub_clause .= " hmpfacilitytype = 4 ";
		}
		if ($ht_Rehabilitation) {
			$sub_clause .= (length($sub_clause) == 0) ? ' ' : ' OR ';
			$sub_clause .= " hmpfacilitytype = 5 ";
		}
		$query .= $sub_clause;
		$query .= " ) ";
	}

	if (!$toc_SelectAll) {
		$query .= " and ( ";
		my $sub_clause = "";
		if ($toc_Governmental) {
			$sub_clause .= (length($sub_clause) == 0) ? ' ' : ' OR ';
			$sub_clause .= " hmpcontrol in (7,8,9,10,11,12,13) ";
		}
		if ($toc_ForProfit) {
			$sub_clause .= (length($sub_clause) == 0) ? ' ' : ' OR ';
			$sub_clause .= " hmpcontrol in (3,4,5,6) ";
		}
		if ($toc_NonProfit) {
			$sub_clause .= (length($sub_clause) == 0) ? ' ' : ' OR ';
			$sub_clause .= " hmpcontrol in (2) ";
		}
		if ($toc_NonProfitChurch) {
			$sub_clause .= (length($sub_clause) == 0) ? ' ' : ' OR ';
			$sub_clause .= " hmpcontrol in (1) ";
		}
		$query .= $sub_clause;
		$query .= " ) ";
	}

	#FTEs
	if ($min_ftes) {
		$query .= " and ftes_with_sub_providers >= $min_ftes ";
	}
	if ($max_ftes) {
		$query .= " and ftes_with_sub_providers <= $max_ftes ";
	}

	#Beds
	if ($min_beds) {
		$query .= " and total_beds >= $min_beds ";
	}
	if ($max_beds) {
		$query .= " and total_beds <= $max_beds ";
	}

	#CMI
	if ($min_cmi) {
		$query .= " and unadjusted_cmi >= $min_cmi ";
	}
	if ($max_cmi) {
		$query .= " and unadjusted_cmi <= $max_cmi ";
	}

	#Critical access
	if ( (!$critical_access_n && $critical_access_y) || ($critical_access_n && !$critical_access_y) ) {
		if ($critical_access_y) {
			$query .= " and critical_access = 'Y' ";
		}
		elsif ($critical_access_n) {
			$query .= " and critical_access <> 'Y' ";
		}
	}

	#Management Authority
	if ( (!$has_management_n && $has_management_y) || ($has_management_n && !$has_management_y) ) {
		if ($has_management_y) {
			$query .= " and (MgmtCodeID is not null and MgmtCodeID <> '') ";
		}
		elsif ($has_management_n) {
			$query .= " and (MgmtCodeID is null or MgmtCodeID = '') ";
		}
	}

	#System Affiliation
	if (!$sa_Select_All) {
		my $sub_clause = "";

		if ($sa_NA) {
			$sub_clause .= ((length($sub_clause) == 0) ? ' ' : ' OR ');
			$sub_clause .= " NoHospitals is null ";
		}
		if ($sa_small) {
			$sub_clause .= ((length($sub_clause) == 0) ? ' ' : ' OR ');
			$sub_clause .= " NoHospitals < 11 ";
		}
		if ($sa_medium) {
			$sub_clause .= ((length($sub_clause) == 0) ? ' ' : ' OR ');
			$sub_clause .= " (NoHospitals >= 11 and NoHospitals < 49) ";
		}
		if ($sa_large) {
			$sub_clause .= ((length($sub_clause) == 0) ? ' ' : ' OR ');
			$sub_clause .= " NoHospitals >= 49 ";
		}

		$query .= " and ( ";
		$query .= $sub_clause;
		$query .= " ) ";
	}


	my $query_state = $query;
	$query_state .= " and b.hospitalstate = '$State' ";

	my $query_local = $query_state;
	#Urban
	if ($urban_rural == 1) {
		$query_local .= " and z.msa = '$MSA' ";
	}
	#Rural
	else {
		$query_local .= " and urban_rural <> 1 ";
	}

	my $rs = &run_mysql($query, 4);
	foreach my $k (keys %{$rs}) {
		push (@Provider_List_National, $$rs{$k}{'prvdr_num'});
	}
	if(@Provider_List_National){
		$session->param("Provider_List_National", \@Provider_List_National);
	}
	else {
		$session->clear("Provider_List_National");
	}

	$rs = &run_mysql($query_state, 4);
	foreach my $k (keys %{$rs}) {
		push (@Provider_List_State, $$rs{$k}{'prvdr_num'});
	}
	if(@Provider_List_State){
		$session->param("Provider_List_State", \@Provider_List_State);
	}
	else {
		$session->clear("Provider_List_State");
	}

	$rs = &run_mysql($query_local, 4);
	foreach my $k (keys %{$rs}) {
		push (@Provider_List_Locale, $$rs{$k}{'prvdr_num'});
	}
	if(@Provider_List_Locale){
		$session->param("Provider_List_Locale", \@Provider_List_Locale);
	}
	else {
		$session->clear("Provider_List_Locale");
	}


	open(D1,">data.txt");
	print D1 "\n\n$query\n\n$query_state\n\n$query_local\n\n";
	close(D1);

	if (1 == 2)
	{
		   my (@list, $row, $op, $n, $class, $string);
		   my ($inlist, $i,$j);

		   my @where_list = ();
		   my @sql_args   = ();
		   my @error_list;

			  $n = @where_list;
			  $op = ($n) ? 'AND' : 'WHERE';
			  push @where_list, "$op h.ProviderNo NOT IN($providerid)";

		   my $workSheetLeftJoin = '';
		   if (($min_beds)||($max_beds)||($min_ftes)||($max_ftes)||($min_cmi)||($max_cmi)||($toc_SelectAll)||($toc_Governmental)||($toc_ForProfit)||($toc_NonProfit)||($toc_NonProfitChurch)||($ts_SelectAll)||($ts_NA)||($ts_Minor)||($ts_Major)||($ts_Complex)||($ht_GeneralAcute)||($ht_Cancer)||($ht_Children)||($ht_LongTermCare)||($ht_Psychiatric)||($ht_Rehabilitation))
		   {
			$workSheetLeftJoin = " left outer join CR_ALL_RPT as rpt on rpt.PRVDR_NUM = h.ProviderNo and rpt.year(FY_END_DT) = $defaultYear ";
		   }
		   #beds
		   my $bedsLeftJoin = '';
		   if (($min_beds)||($max_beds))
		   {
			$bedsLeftJoin = " left outer join CR_ALL_DATA as dataBed on dataBed.CR_REC_NUM = rpt.RPT_REC_NUM ";
			$n = @where_list;
			$op = ($n) ? 'AND' : 'WHERE';
				push @where_list, "$op dataBed.CR_WORKSHEET = 'S300001' and dataBed.CR_LINE= '02500' and dataBed.CR_COLUMN = '0100' \n";
		   }

		   if ($min_beds) {

			  $n = @where_list;
			  $op = ($n) ? 'AND' : 'WHERE';
			  push @where_list, "$op dataBed.CR_VALUE >= (select 0 + ? )\n";
			  push @sql_args, $min_beds;
		   }

		   if ($max_beds) {
			  $n = @where_list;
			  $op = ($n) ? 'AND' : 'WHERE';
			  push @where_list, "$op dataBed.CR_VALUE <= (select 0 + ? )\n";
			  push @sql_args, $max_beds;
		   }
		   #fte
		   my $ftesLeftJoin = '';
		   if (($min_ftes)||($max_ftes)){
			$ftesLeftJoin = " left outer join CR_ALL_DATA as dataFte on dataFte.CR_REC_NUM = rpt.RPT_REC_NUM ";
			$n = @where_list;
			$op = ($n) ? 'AND' : 'WHERE';
				push @where_list, "$op dataFte.CR_WORKSHEET = 'S300001' and dataFte.CR_LINE= '01200' and dataFte.CR_COLUMN = '1000' \n";
		   }

		   if ($min_ftes) {
			  $n = @where_list;
			  $op = ($n) ? 'AND' : 'WHERE';
			  push @where_list, "$op dataFte.CR_VALUE >= (select 0 + ? )\n";
			  push @sql_args, $min_ftes;
		   }

		   if ($max_ftes) {
			  $n = @where_list;
			  $op = ($n) ? 'AND' : 'WHERE';
			  push @where_list, "$op dataFte.CR_VALUE <= (select 0 + ? )\n";
			  push @sql_args, $max_ftes;
		   }
		   #cmi
		   my $cmiLeftJoin = '';
		   if (($min_cmi)||($max_cmi))
		   {
			$cmiLeftJoin = " left outer join cmi on h.ProviderNo = cmi.provider_number ";
			$n = @where_list;
			$op = ($n) ? 'AND' : 'WHERE';
			push @where_list, "$op cmi.year= ?";
			push @sql_args, $defaultYear;
		   }

		   if ($min_cmi) {
			  $n = @where_list;
			  $op = ($n) ? 'AND' : 'WHERE';
			  push @where_list, "$op cmi.unadjusted_cmi >= (select 0 + ? )\n";
			  push @sql_args, $min_cmi;
		   }

		   if ($max_cmi){
			  $n = @where_list;
			  $op = ($n) ? 'AND' : 'WHERE';
			  push @where_list, "$op cmi.unadjusted_cmi <= (select 0 + ? )\n";
			  push @sql_args, $max_cmi;
		   }
 #System Affilation
			#my $saLeftJoin = '';
			#if($sa_Select_All)
			#{}
			#elsif(($sa_NA)||($sa_small)||($sa_medium)||($sa_large))
			#{
			#	$saLeftJoin = " left outer join hospital_type as ht on ";
			#}

		   # Hospital Type
		   my $htLeftJoin = '';
		   if ($ht_SelectAll)
		   {}
		   elsif(($ht_GeneralAcute)||($ht_Cancer)||($ht_Children)||($ht_LongTermCare)||($ht_Psychiatric)||($ht_Rehabilitation))
		   {
				$htLeftJoin = " left outer join CR_ALL_DATA as dataHtype on dataHtype.CR_REC_NUM = rpt.RPT_REC_NUM ";

				if($ht_GeneralAcute){
					$n = @where_list;
					$op = ($n) ? 'AND' : 'WHERE';
					push @where_list, "$op dataHtype.CR_WORKSHEET = 'S200000' and dataHtype.CR_LINE= '01900' and dataHtype.CR_COLUMN = '0100' and dataHtype.CR_VALUE  = 1 \n";
				}
				if($ht_Cancer){
					$n = @where_list;
					$op = ($n) ? 'AND' : 'WHERE';
					push @where_list, "$op dataHtype.CR_WORKSHEET = 'S200000' and dataHtype.CR_LINE= '01900' and dataHtype.CR_COLUMN = '0100' and dataHtype.CR_VALUE  = 3 \n";
				}
				if($ht_Children){
					$n = @where_list;
					$op = ($n) ? 'AND' : 'WHERE';
					push @where_list, "$op dataHtype.CR_WORKSHEET = 'S200000' and dataHtype.CR_LINE= '01900' and dataHtype.CR_COLUMN = '0100' and dataHtype.CR_VALUE  = 7 \n";
				}
				if($ht_LongTermCare){
					$n = @where_list;
					$op = ($n) ? 'AND' : 'WHERE';
					push @where_list, "$op dataHtype.CR_WORKSHEET = 'S200000' and dataHtype.CR_LINE= '01900' and dataHtype.CR_COLUMN = '0100' and dataHtype.CR_VALUE  = 2 \n";
				}
				if($ht_Psychiatric){
					$n = @where_list;
					$op = ($n) ? 'AND' : 'WHERE';
					push @where_list, "$op dataHtype.CR_WORKSHEET = 'S200000' and dataHtype.CR_LINE= '01900' and dataHtype.CR_COLUMN = '0100' and dataHtype.CR_VALUE  = 4 \n";
				}
				if(($ht_Rehabilitation)){
					$n = @where_list;
					$op = ($n) ? 'AND' : 'WHERE';
					push @where_list, "$op dataHtype.CR_WORKSHEET = 'S200000' and dataHtype.CR_LINE= '01900' and dataHtype.CR_COLUMN = '0100' and dataHtype.CR_VALUE  = '5' \n";
				}
# Critical access is a sub type. All the ones above are facility_type (in hath).
				#if($ht_CritcalAccess){
				#	$n = @where_list;
				#	$op = ($n) ? 'AND' : 'WHERE';
				#	push @where_list, "$op dataHtype.CR_WORKSHEET = 'S200000' and dataHtype.CR_LINE= '03000' and dataHtype.CR_COLUMN = '0100' and dataHtype.CR_VALUE  = 'Y' \n";
				#}
		   }

		   #Type of Control($toc_SelectAll)||($toc_Governmental)||($toc_ForProfit)||($toc_NonProfit)||($toc_NonProfitChurch)

		   if ($toc_SelectAll)
		   {}
		   elsif(($toc_Governmental)||($toc_ForProfit)||($toc_NonProfit)||($toc_NonProfitChurch))
		   {

			   if ($toc_Governmental) {
				  $n = @where_list;
				  $op = ($n) ? 'AND' : 'WHERE';
				  push @where_list, "$op h.HMPControl IN (8,9,10,11,12,13) \n";

			   }

			   if ($toc_ForProfit) {
				  $n = @where_list;
				  $op = ($n) ? 'AND' : 'WHERE';
				  push @where_list, "$op h.HMPControl IN (3,4,5,6) \n";

			   }

			   if ($toc_NonProfit) {
				  $n = @where_list;
				  $op = ($n) ? 'AND' : 'WHERE';
				  push @where_list, "$op h.HMPControl IN (2)\n";

			   }

			   if ($toc_NonProfitChurch) {
				  $n = @where_list;
				  $op = ($n) ? 'AND' : 'WHERE';
				  push @where_list, "$op h.HMPControl IN (1)\n";

			   }
		  }

		  # Teaching Status
		  my $ts_leftjoin = '';
		  if($ts_SelectAll){
		  }elsif(($ts_NA)||($ts_Minor)||($ts_Major)||($ts_Complex)){
			$ftesLeftJoin = " left outer join CR_ALL_DATA as dataTS on dataTS.CR_REC_NUM = rpt.RPT_REC_NUM ";
			if ($ts_Minor){
			$n = @where_list;
			$op = ($n) ? 'AND' : 'WHERE';
				push @where_list, "$op dataTS.CR_WORKSHEET = 'S300001' and dataTS.CR_LINE= '02500' and dataTS.CR_COLUMN = '0700' and (dataTS.CR_VALUE BETWEEN 1 and 25)\n";
		   }

		   if ($ts_Major){
			$n = @where_list;
			$op = ($n) ? 'AND' : 'WHERE';
				push @where_list, "$op dataTS.CR_WORKSHEET = 'S300001' and dataTS.CR_LINE= '02500' and dataTS.CR_COLUMN = '0700' and (dataTS.CR_VALUE BETWEEN 25 and 100)\n";
		   }

		   if ($ts_Complex){
			$n = @where_list;
			$op = ($n) ? 'AND' : 'WHERE';
				push @where_list, "$op dataTS.CR_WORKSHEET = 'S300001' and dataTS.CR_LINE= '02500' and dataTS.CR_COLUMN = '0700' and (dataTS.CR_VALUE > 100)\n";
		   }
		   if ($ts_NA){
			$n = @where_list;
			$op = ($n) ? 'AND' : 'WHERE';
				push @where_list, "$op dataTS.CR_WORKSHEET = 'S300001' and dataTS.CR_LINE= '02500' and dataTS.CR_COLUMN = '0700' and dataTS.CR_VALUE = 0\n";
		   }
		  }


		#------------End of new fields---------------------------------

		#Provider List- National
		   $sql = qq{
		   SELECT distinct h.ProviderNo as PROVIDER_NUMBER
			 FROM tblhospitalmaster as h
			 LEFT JOIN tblsystemmaster as sys ON sys.SystemID = h.SystemID
			 $workSheetLeftJoin
			 $bedsLeftJoin
			 $ftesLeftJoin
			 $cmiLeftJoin
			 $htLeftJoin
			 @where_list
		   };
			 open(J, ">>data.txt");
			print J "\n\n\n$sql";
			close J;

		   unless ($sth = $dbh->prepare($sql)) {
			   &display_error('ERROR preparing SQL', $sth->errstr);
			   $dbh->disconnect();
			   exit(1);
		   }


		   unless ($sth->execute(@sql_args)) {
			   &display_error('ERROR executing SQL', $sth->errstr, $sql);
			   $dbh->disconnect();
			   exit(1);
		   }


		  while ($row = $sth->fetchrow_hashref) {
			push (@Provider_List_National, $$row{PROVIDER_NUMBER}); #all provider list
		  }


		  if(@Provider_List_National){
		  $session->param("Provider_List_National", \@Provider_List_National);
		  }

		#------------------------------------------------------
		#Provider list--State
		my @where_list_state=@where_list;
		my @sql_args_state=@sql_args;

		if($State){
			$n = @where_list_state;
			$op = ($n) ? 'AND' : 'WHERE';
			push @where_list_state, "$op h.HospitalState = ?";
			push @sql_args_state, $State;

		my $sql_state = qq{
		   SELECT distinct h.ProviderNo as PROVIDER_NUMBER
			 FROM tblhospitalmaster as h
			 LEFT JOIN tblsystemmaster as sys ON sys.SystemID = h.SystemID
			 $workSheetLeftJoin
			 $bedsLeftJoin
			 $ftesLeftJoin
			 $cmiLeftJoin
			 $htLeftJoin
			 @where_list_state
		   };

		   unless ($sth = $dbh->prepare($sql_state)) {
			   &display_error('ERROR preparing SQL', $sth->errstr);
			   $dbh->disconnect();
			   exit(1);
		   }


		   unless ($sth->execute(@sql_args_state)) {
			   &display_error('ERROR executing SQL', $sth->errstr, $sql_state);
			   $dbh->disconnect();
			   exit(1);
		   }


		  while ($row = $sth->fetchrow_hashref) {
			push (@Provider_List_State, $$row{PROVIDER_NUMBER}); #all provider list-State
		  }
		 }
		 if(@Provider_List_State){
		  $session->param("Provider_List_State", \@Provider_List_State);
		  }
		#------------------------------------------------------
		#Provider list--Locale
		my @sql_args_UR=@sql_args;

		if($urban_rural==2){
			my $leftJoinRural = '';
			my @where_list_Rural=@where_list;

			$n = @where_list_Rural;
			$op = ($n) ? 'AND' : 'WHERE';
			push @where_list_Rural, "$op h.HospitalState = ?";
			push @sql_args_UR, $State;

			$n = @where_list_Rural;
			$op = ($n) ? 'AND' : 'WHERE';
			push @where_list_Rural, "$op dataGeoAr.CR_WORKSHEET = 'S200000' and dataGeoAr.CR_LINE= '02103' and dataGeoAr.CR_COLUMN = '0100' and dataGeoAr.cr_value = ?\n";
			push @sql_args_UR,$urban_rural;
			$leftJoinRural = " left outer join CR_ALL_DATA as dataGeoAr on dataGeoAr.CR_REC_NUM = rpt.RPT_REC_NUM";

			unless ($workSheetLeftJoin)
		   {
			$workSheetLeftJoin = " left outer join CR_ALL_RPT as rpt on rpt.PRVDR_NUM = h.ProviderNo and rpt.year(FY_END_DT) = $defaultYear";
		   }
			$sql = qq{
		   SELECT distinct h.ProviderNo as PROVIDER_NUMBER
			 FROM tblhospitalmaster as h
			 LEFT JOIN tblsystemmaster as sys ON sys.SystemID = h.SystemID
			 $workSheetLeftJoin
			 $leftJoinRural
			 $bedsLeftJoin
			 $ftesLeftJoin
			 $cmiLeftJoin
			 $htLeftJoin
			 @where_list_Rural
		   };
		}
		else{
			my $leftJoinMSA = '';
			my @where_list_MSA=@where_list;

			$n = @where_list_MSA;
			$op = ($n) ? 'AND' : 'WHERE';
			push @where_list_MSA, "$op msa.MSACode = ?";
			push @sql_args_UR, $MSA;
			$leftJoinMSA = " left outer join zipcodes as z on z.zipcode = h.ShortZip left outer join msa on msa.MSACode = z.msa ";

			$sql = qq{
				SELECT distinct h.ProviderNo as PROVIDER_NUMBER
				FROM tblhospitalmaster as h
				LEFT JOIN tblsystemmaster as sys ON sys.SystemID = h.SystemID
				$leftJoinMSA
				$workSheetLeftJoin
				$bedsLeftJoin
				$ftesLeftJoin
				$cmiLeftJoin
				$htLeftJoin
				@where_list_MSA
		   };
		}

		   unless ($sth = $dbh->prepare($sql)) {
			   &display_error('ERROR preparing SQL', $sth->errstr);
			   $dbh->disconnect();
			   exit(1);
		   }


		   unless ($sth->execute(@sql_args_UR)) {
			   &display_error('ERROR executing SQL', $sth->errstr, $sql);
			   $dbh->disconnect();
			   exit(1);
		   }


		  while ($row = $sth->fetchrow_hashref) {
			push (@Provider_List_Locale, $$row{PROVIDER_NUMBER}); #all provider list-Locale
		  }
		  if(@Provider_List_Locale){
		  $session->param("Provider_List_Locale", \@Provider_List_Locale);
		  }


	}
}

#----------------------------------------------
#Urban or Rural
sub urban_rural
{
	my $sql = "SELECT dataGeoAr.cr_value FROM CR_ALL_RPT as rpt
	INNER JOIN CR_ALL_DATA as dataGeoAr
	WHERE dataGeoAr.CR_REC_NUM = rpt.RPT_REC_NUM
	AND dataGeoAr.CR_WORKSHEET = 'S200000'
	AND dataGeoAr.CR_LINE= '02103'
	AND dataGeoAr.CR_COLUMN = '0100'
	AND rpt.PRVDR_NUM = $providerid
	AND year(FY_END_DT) = $defaultYear ";
	unless ($sth = $dbh->prepare($sql)) {
       &display_error('ERROR preparing SQL', $sth->errstr);
       $dbh->disconnect();
       exit(1);
   }

   unless ($sth->execute()) {
       &display_error('ERROR executing SQL', $sth->errstr, $sql);
       $dbh->disconnect();
       exit(1);
   }
   my $rows_returned = $sth->rows();

   if(my $row = $sth->fetchrow_hashref) {
	return $$row{cr_value};
  }
}
#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach my $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
<SCRIPT SRC="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<SCRIPT SRC="/JS/dropmenu.js"></SCRIPT>
<link rel="stylesheet" href="/css/dropmenu.css" type="text/css" />
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}

#------------------------------------------------------------------------------

