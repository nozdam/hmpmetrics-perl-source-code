#!/usr/bin/perl

use CGI;
use DBI;
do "common_header.pl";
print "Content-Type: text/html\n\n";
&headerScript();

my $cgi = new CGI;

use CGI::Session;

my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $saction = $cgi->param('saction');
my $selID = $cgi->param('id');
my $facility = $cgi->param("facility");
my $id = '';
my $fname = '';
my $lno = '';
my $colno = '';
my $wrkshno = '';
my $bcoordinates = '';

# Database

my $dbh;
my ($sql, $sth,$row,$s);

# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my $defaultYear= $ini_value{'DEFAULT_YEAR'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}



my ($sql1,$sth1,$isFor,$sql_f,$facility_id);
$sql_f = "select facility_id from tbl_facility_type where facility_name = '$facility'";
$sth1 = $dbh->prepare($sql_f);
$sth1->execute();

while ($row = $sth1->fetchrow_hashref) {
	$facility_id = $$row{facility_id};
}
$sql1 = "select metrics_id,element,formulae,isFormulae from standard_metrics where Formulae !='' and facility_type = $facility_id order by element";
$sth1 = $dbh->prepare($sql1);
$sth1->execute();

my ($ID, $indicator,$formulae);

my $formula_optionsA  = "<OPTION VALUE='' SELECTED>-- Select --</OPTION>";

while (($ID, $indicator,$formulae,$isFor) = $sth1->fetchrow_array) {
	if($isFor == 0){
      $formula_optionsA = $formula_optionsA."<OPTION TITLE='$indicator' VALUE='$ID'>$indicator</OPTION>";
	  }
}



print qq{
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Formulas - Select Mode</title>
    <link href="/css/tabs.css" rel="stylesheet" type="text/css" />
   
</head>
<script language="javascript" src="/JS/admin_panel/standard_report/FormulaTabPageC.js" ></script>
<body onload="setSelected();">
<form name="frmFormulaTabPageC" action="/cgi-bin/FormulaActions.pl?facility=$facility" method="post">
<input type="hidden" name="id" value ="$id">
<input type="hidden" name="saction" value ="edit">
<input type="hidden" name="facility" value ="$facility">

<ol id="toc">
    <li><a href="/cgi-bin/FormulaTabPageA.pl?facility=$facility"><span>Formula</span></a></li>
    <li><a href="/cgi-bin/FormulaTabPageB.pl?facility=$facility"><span>Create Indicator</span></a></li>
    <li class="current"><a href="/cgi-bin/FormulaTabPageC.pl?facility=$facility"><span>Edit Indicator</span></a></li>
</ol>
<div class="content">
    <table class="gridstyle" width="80%" cellpadding="3" cellspacing="3">
		<tr>
			  <td style="font-weight:bold;" align="right" >Masters :</div></td>
			  <td colspan=2>
			  	<select name='formula' style='width:240px;' onchange='selctFormula()'>
};
				print "$formula_optionsA";

print qq{
			  	</select>
			  </td>
			  <td></td>
    		</tr>
		<tr>
			 <td align="right" style="font-weight:bold;">Name :</td><td><input type="text" name="txtName" value="$fname" readonly>
			 </td><td style="font-weight:bold;color:red;"></td>
		</tr>
		<tr>
			 <td align="right" style="font-weight:bold;">Work Sheet No. :</td><td><input type="text" name="txtWorksheet" maxLength="7" value="$wrkshno"></td><td></td>
		</tr>
		<tr>
			 <td align="right" style="font-weight:bold;">Line No. :</td><td><input type="text" name="txtLineNo" maxLength="5" value="$lno"></td><td></td>
		</tr>
		<tr>
			<td align="right" style="font-weight:bold;">Column No. :</td><td><input type="text" name="txtColNo" maxLength="4" value="$colno"></td><td></td>
		</tr>
		<tr>
		    	<td align="right"><span  class=\"button\"><input type="hidden" name="btnDelete" value ="Delete" onclick=\"CallDelete()\" ></span></td>
		    	<td>
		    	<span  class=\"button\"><input type="button" name="btnUpdate" value ="Update" onclick="callSubmit()"></span>
		    	<span  class=\"button\"><input type="button" name="btnCancel" value ="Reset" Onclick="formula_clear()"></span>
		    	</td>
		    	<td></td>
		</tr>
    </table>
</div>
</form>
</body>
</html>
};


#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#--------------------------------------------------------------------------------------

sub sessionExpire
{

print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}



print qq{
<HTML>
<HEAD><TITLE>Standard Reports-ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}