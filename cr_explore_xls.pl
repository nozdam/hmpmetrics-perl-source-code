#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: cr_explore_xls.pl
#  author: Nozer Damania
#  Written: 8/4/2011
#
#----------------------------------------------------------------------------

use strict;
use CGI;
use DBI;
use CGI::Session;
require "/var/www/cgi-bin/lib/req-costreports.pl";
do "audit.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
if(!$userID)
{
	print "Content-Type: text/html\n\n";
	&sessionExpire;
}
else {

	print "Content-type: application/vnd.ms-excel\n";

	my $facility = $cgi->param("facility");
	my $worksheet = $cgi->param('worksheet');
	my $cr_rec_num  = $cgi->param('reportid');
	my $provider = $cgi->param('provider');
	my $cr_worksheet = $cgi->param('cr_worksheet');


	my $dbh = &db_open();


	my $sql = "select facility_id, data_table from tbl_facility_type where facility_name = '$facility'";
	my $rs = &run_mysql_fast($sql,4,$dbh);
	my $facility_type = $$rs{0}{'facility_id'};
	my $data_table = $$rs{0}{'data_table'};


	my $spreadsheet_new = "";

	#Write the values to a new spreadsheet.
	my $dest_book  = Spreadsheet::WriteExcel->new('-');

	my %worksheet;
	if ($cr_worksheet) {
		#$sql = "SELECT cr_worksheet, cr_line, cr_column, cr_value FROM $data_table WHERE cr_rec_num = $cr_rec_num AND cr_worksheet = '$cr_worksheet' and cr_type = 'N'";
		$sql = "SELECT cr_worksheet, cr_line, cr_column, cr_value FROM $data_table WHERE cr_rec_num = $cr_rec_num AND cr_worksheet = '$cr_worksheet'";
		$spreadsheet_new = "HMP-" . $cr_rec_num . "-" . $cr_worksheet . ".xls";
	}
	else {
		$sql = "SELECT cr_worksheet, cr_line, cr_column, cr_value FROM $data_table WHERE cr_rec_num = $cr_rec_num";
		$spreadsheet_new = "HMP-" . $cr_rec_num . ".xls";
	}

	$rs = &run_mysql_fast($sql,4,$dbh);
	foreach my $k (keys %{$rs}) {
		my $line = $$rs{$k}{"cr_line"};
		my $column = $$rs{$k}{"cr_column"};
		my $value = $$rs{$k}{"cr_value"};
		my $cr_worksheet2 = $$rs{$k}{"cr_worksheet"};
		$worksheet{$cr_worksheet2}{$line}{$column} = $value;
	}


	foreach my $cr_worksheet (sort {$a cmp $b} keys %worksheet) {
		#Get the spreadsheet path.
		$sql = "SELECT cost_report_form_link, definition FROM tbl_cost_report_definition WHERE facility_type = $facility_type AND cr_worksheet = '$cr_worksheet'";
		$rs = &run_mysql_fast($sql,4,$dbh);
		my $spreadsheet_path = &get_cr_path() . $$rs{0}{'cost_report_form_link'};
		my $spreadsheet_name = $cr_worksheet;

		my (%line_mappings, %line_mappings_r, %value_hash, %col_hash);
		&map_lines($data_table, $cr_rec_num, $cr_worksheet, $dbh, \%line_mappings, \%line_mappings_r);

		foreach my $line (sort {$a <=> $b} keys%{$worksheet{$cr_worksheet}}) {
			foreach my $column (sort {$a <=> $b} keys%{$worksheet{$cr_worksheet}{$line}}) {
				&modify_line_number($worksheet, $line, $column, $worksheet{$cr_worksheet}{$line}{$column}, "multi", \%line_mappings, &get_worksheet_inserts(), \%value_hash, \%col_hash);
			}
		}
		undef %col_hash; #Dont need this at all.

		my $is_line_mappings = (keys %line_mappings);
		&create_work_sheet($spreadsheet_name, $spreadsheet_path, $is_line_mappings, $dest_book, \%value_hash);
		print "Worksheet: $cr_worksheet\n";
	}

	&db_close($dbh);

	&insert("Create","Spreadsheet generated","","Cost Report");


	print "Content-Disposition: attachment; filename=$spreadsheet_new\n";
	print "Cache-Control: no-cache\n";
	print "Pragma: no-cache\n\n";

	$dest_book->close();
}









sub sessionExpire
{


print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}


#------------------------------------------------------------------------------

