#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: kb_update.pl
#   author: Steve Spicer
#  Written: Wed Sep 10 19:19:49 CDT 2008
#  Revised:
#  Research Data Assistance Center
#  http://www.resdac.umn.edu/Tools/TBs/TN-005-revised.asp
#----------------------------------------------------------------------------

use strict;
use CGI;
use DBI;
do "common_header.pl";
print "Content-Type: text/html\n\n";
&headerScript();
my $cgi = new CGI;

my $action    = $cgi->param('action');
my $address   = $cgi->param('address');
my $desc      = $cgi->param('desc');
my $notes     = $cgi->param('notes');
my $fmt       = $cgi->param('fmt');
my $verified  = $cgi->param('verified');
my $facility = $cgi->param("facility");
$verified = ($verified eq 'on') ? 'Y' : 'N';
my $reportid = $cgi->param("reportid");
my $worksheet = $cgi->param("worksheet");
my $author    = $ENV{"REMOTE_USER"};
my ($sql_f,$facility_id,$master_table,$report_table,$data_table,$row,$sth);
my %ini_value;

unless (&read_inifile("milo.ini")) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}
if($action){
}else{
$action = '';
}
my $ref_old;   # hash referece for old record

my $dbtype     = $ini_value{"DB_TYPE"};
my $dblogin    = $ini_value{"DB_LOGIN"};
my $dbpasswd   = $ini_value{"DB_PASSWORD"};
my $dbname     = $ini_value{"DB_NAME"};
my $dbserver   = $ini_value{"DB_SERVER"};

my $dbh;

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}
$sql_f = "select facility_id ,master_table,report_table,data_table from tbl_facility_type where facility_name = '$facility'";
$sth = $dbh->prepare($sql_f);
$sth->execute();

while ($row = $sth->fetchrow_hashref) {
	$facility_id = $$row{facility_id};
	$master_table = $$row{master_table};
	$report_table = $$row{report_table};
	$data_table = $$row{data_table};
}

if ($action eq 'UPDATE') {
   &update_kb_record($address, $desc, $notes, $fmt, $verified, $author,$facility_id);

   &display_success();
} elsif ($action eq 'INSERT') {
   &insert_kb_record($address, $desc, $notes, $fmt, $verified, $author);
   &display_success();
} else {
   &get_kb_record($address);
   &display_form();
}

$dbh->disconnect();

exit(0);

#----------------------------------------------------------------------------

sub get_kb_record
{

   my ($address) = @_;

   my ($sth);

   my $sql = "SELECT * FROM KB_TABLE WHERE KB_ADDRESS = ? and facility_type=$facility_id";

   unless ($sth = $dbh->prepare($sql)) {
      &display_error("ERROR preparing sql query", $sth->errstr);
      $dbh->disconnect();
      exit(0);
   }

   unless ($sth->execute($address)) {
       &display_error("ERROR executing sql query", $sth->errstr);
       exit(0);
   }

   $ref_old = $sth->fetchrow_hashref;

   $sth->finish();

   return(0);
}

#----------------------------------------------------------------------------

sub update_kb_record
{
   my  ($address, $desc, $notes, $fmt, $verified, $author,$facility_id) = @_;

   my ($sth);

   my $sql = qq{
    UPDATE KB_TABLE
       SET KB_DESC     = ?,
           KB_NOTES    = ?,
           KB_FORMAT   = ?,
           KB_VERIFIED = ?,
           KB_AUTHOR   = ?
     WHERE KB_ADDRESS  = ?
	 and facility_type= ?
   };
	open(J, ">data.txt");
						print J $facility_id;
						close J;
   unless ($sth = $dbh->prepare($sql)) {
       &display_error("ERROR preparing sql update", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sth->execute($desc, $notes, $fmt, $verified, $author, $address,$facility_id)) {
       &display_error("ERROR executing sql update", $sth->errstr);
       exit(0);
   }

   $sth->finish();

   return(0);
}

#----------------------------------------------------------------------------

sub insert_kb_record
{
   my  ($address, $desc, $notes, $fmt, $verified, $author) = @_;

   my ($sth);

   my $sql = qq{
    INSERT INTO KB_TABLE
            (KB_ADDRESS, KB_DESC, KB_NOTES, KB_FORMAT, KB_VERIFIED, KB_AUTHOR,facility_type)
     VALUES (?, ?, ?, ?, ?, ?, ?)
   };

   unless ($sth = $dbh->prepare($sql)) {
       &display_error("ERROR preparing sql update", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sth->execute($address, $desc, $notes, $fmt, $verified, $author,$facility_id)) {
       &display_error("ERROR executing sql update", $sth->errstr);
       exit(0);
   }

   $sth->finish();

   return(0);
}
#----------------------------------------------------------------------------

sub display_error
{
   my @list = @_;

   my ($string, $s);

   foreach $s (@list) {
      $string .= "$s<BR>\n";
   }
   print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
<BR>
<!--header start-->
<table cellpadding="0" cellspacing="0" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/index.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/cr_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="/cgi-bin/cr_batchx.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="/cgi-bin/snf_batch1.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="/cgi-bin/graph.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Graph Utility</div>">
                                <img src="/images/graph_icon_inner.gif" onmouseover="src='/images/graph_icon_inner_hover.gif';"
                                    onmouseout="src='/images/graph_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                  <td style="width: 102px" align="Center">
		                              <a href="/cgi-bin/PeerMain.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
		                                  <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->
    <!--content start--><br>
        <table cellpadding="0" cellspacing="0" width="885px">
            <tr>
                <td valign="top">
                    <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px">
<FONT SIZE="5" COLOR="RED">
<B>ERROR:</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" VALUE=" CLOSE " onclick="window.close();"></span>
</FORM>
</CENTER>
</BODY>
</HTML>
   };

   return(0);
}


#----------------------------------------------------------------------------


sub display_form
{
   my ($selected, $key);

   my $desc     = $$ref_old{'KB_DESC'};
   my $notes    = $$ref_old{'KB_NOTES'};
   my $author   = $$ref_old{'KB_AUTHOR'};
   my $verified = $$ref_old{'KB_VERIFIED'};
   my $fmt      = $$ref_old{'KB_FORMAT'};

   my $checked = ($verified eq 'Y') ? 'CHECKED' : '';

   my $action = (defined $$ref_old{'KB_ADDRESS'}) ? 'UPDATE' : 'INSERT';

   my %lookup = (
     'S' => 'STRING',
     'M' => 'MONEY',
     'D' => 'DECIMAL',
     'N' => 'NUMBER',
   );

   my $option_list = '';

   foreach $key (keys %lookup) {
      $selected = ($key eq $fmt) ? 'SELECTED' : '';
      $option_list .= "<OPTION VALUE=\"$key\" $selected>$lookup{$key}</OPTION>\n";
   }



   print <<EOF
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
<HEAD>
<TITLE>KNOWLEDGE BASE UPDATE TOOL</TITLE>
<script type="text/javascript">
function setfocus() {
   document.forms[0].desc.focus();
}
</script>

</HEAD>
<BODY onLoad="setfocus()">
<CENTER>
<BR>
<FORM NAME="form1" ID="form1" METHOD="POST" ACTION="/cgi-bin/kb_update.pl?">
<div class="gridstyle" style="padding:10px; background-color:white">
<div class="pageheader"><B>UPDATE KNOWLEDGE BASE</B></div>
<TABLE CELLPADDING="4" WIDTH="100%">

   <TR>
      <TD ALIGN="RIGHT">ADDRESS:</TD>
      <TD ALIGN="LEFT">
          <INPUT TYPE="text" NAME="address" SIZE="20" MAXLENGTH="18" VALUE="$address" READONLY>
      </TD>
   </TR>
   <TR>
      <TD ALIGN="RIGHT">DESCRIPTION:</TD>
      <TD ALIGN="LEFT">
          <INPUT TYPE="text" NAME="desc" ID="desc" SIZE="50" MAXLENGTH="255" VALUE="$desc" TITLE="ENTER NOTES">
      </TD>
   </TR>
   <TR>
      <TD ALIGN="RIGHT">NOTES:</TD>
      <TD ALIGN="LEFT">
        <TEXTAREA NAME="notes" ROWS="10" COLS="50">$notes</TEXTAREA>
      </TD>
   </TR>
   <TR>
      <TD ALIGN="RIGHT">FORMAT:</TD>
      <TD ALIGN="LEFT">
          <SELECT NAME="fmt">
             $option_list
           </SELECT>
           &nbsp; &nbsp; &nbsp; &nbsp;
           VERFIED: <INPUT TYPE="CHECKBOX" NAME="verified" $checked>
      </TD>
   </TR>
   <TR>
      <TD ALIGN="RIGHT">UPDATED BY:</TD>
      <TD ALIGN="LEFT">
          <INPUT TYPE="text" NAME="author" SIZE="12" MAXLENGTH="12" VALUE="$author">
      </TD>
   </TR>
   <TR>
      <TD></td>
<td align="left">
      <span class="button">    <INPUT TYPE="button" VALUE=" CANCEL " onclick="window.close()"></span>

      <span class="button">  <INPUT TYPE="submit" VALUE="  $action RECORD  "></span>
          <BR>
      </TD></tr>
</TABLE></div>
<INPUT TYPE="hidden" NAME="action" VALUE="$action">
<INPUT TYPE="hidden" NAME="facility" VALUE="$facility">
<INPUT TYPE="hidden" NAME="worksheet" VALUE="$worksheet">
<INPUT TYPE="hidden" NAME="reportid" VALUE="$reportid">
</FORM>
</CENTER>
</BODY>
</HTML>
EOF
;
}

#----------------------------------------------------------------------------

sub display_success
{
   my $now = localtime();



   print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
<HEAD>
<TITLE>KNOWLEDGE BASE UPDATE TOOL</TITLE>
</HEAD>
<BODY>
<CENTER>
<BR>
<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/index.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/cr_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="/cgi-bin/cr_batchx.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="/cgi-bin/snf_batch1.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="/cgi-bin/graph.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Graph Utility</div>">
                                <img src="/images/graph_icon_inner.gif" onmouseover="src='/images/graph_icon_inner_hover.gif';"
                                    onmouseout="src='/images/graph_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                   <td style="width: 102px" align="Center">
		                               <a href="/cgi-bin/PeerMain.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
		                                   <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->
    <!--content start--><br>
        <table cellpadding="0" cellspacing="0" width="885px">
            <tr>
                <td valign="top">
                    <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px">
<TABLE BORDER="1" CELLSPACING="0" CELLPADDING="8" WIDTH="400">
   <TR>
      <TD ALIGN="CENTER">
         <BR>
         <FONT SIZE="5" COLOR="GREEN">
         OPERATION SUCCESSFUL<BR>
         </FONT>
         <FONT SIZE="1">$now</FONT><BR>
         <BR>
      </TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<INPUT TYPE="button" VALUE=" CLOSE " onclick="window.close();">
</FORM>
</td>
            <td valign="top">
                <img src="/images/content_right.gif" /></td>
        </tr>
    </table>
    <!--content end-->
</CENTER>
</BODY>
</HTML>
   }
}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#------------------------------------------------------------------------------

