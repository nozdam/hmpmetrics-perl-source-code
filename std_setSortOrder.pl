#!/usr/bin/perl

use CGI;
use CGI::Ajax;
my $cgi = new CGI;
do "common_header.pl";
print "Content-Type: text/html\n\n";
&headerScript();
my $action    = $cgi->param('paramA'); 

my $show = "visible";
my $hide = "hidden";

my $btnLabel = "Mark as FOI";
my $btnClick = "MarkAsFOI()";


if($action eq '1'){	
	$btnLabel = "Add Group";
	$btnClick = "AddList()";
}

print <<EOF

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>	
<script type="text/javascript" src="/JS/standard_report/std_setSortOrder.js"></script>
</head>
<body bgColor='#87AEC5' onload="setDisplay()">
<FORM Name="frmsetSortOrder" method="post" action="/cgi-bin/standard_reports_wizard.pl">
<DIV id="dispOrderDiv" style="background-color:#FFFFFF;position:static;width:100%;height:300px;overflow:scroll;overflow-x:hidden;">		
</DIV>	
</FORM>
</body>
</html>
EOF
;
