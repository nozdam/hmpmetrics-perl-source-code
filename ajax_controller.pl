#!/usr/bin/perl

use CGI;
use DBI;
use strict;
do "common_header.pl";
my $cgi = new CGI;
my $param = $cgi->param('search');
my $peerID = $cgi->param('peerID');
my $saction = $cgi->param('saction');
my $duFormula = $cgi->param('duFormula');
my $facility = $cgi->param("facility");
my $dbh;
my ($sql, $sth,$id,$fname,$bcoordinates,$row,$facility_id,$master_table,$sql_f);
my $selID = $cgi->param('id');

# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my $defaultYear= $ini_value{'DEFAULT_YEAR'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}
$sql_f = "select facility_id ,master_table from tbl_facility_type where facility_name = '$facility'";
$sth = $dbh->prepare($sql_f);
$sth->execute();

while ($row = $sth->fetchrow_hashref) {
	$facility_id = $$row{facility_id};
	$master_table = $$row{master_table};
}

if ($saction eq 'selectPeer')
{
$sql = "select  pd.Provider_number as Number, replace(m.Name, '&','~') as Name
from peergroup_details as pd
inner join peergroup_main as pm
on pm.PeerGroup_ID = pd.PeerGroup_Main_ID
inner join $master_table m
on m.ProviderNo = pd.Provider_number
where pd.PeerGroup_Main_ID = $peerID
Order By pd.FOI desc";
$sth = $dbh->prepare($sql);
$sth->execute();

print CGI->header('text/xml', 'Cache-Control: no-store, no-cache, must-revalidate', 'Cache-Control: post-check=0, pre-check=0', 'Pragma: no-cache');

print <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<root>
<data1>
EOF
;

while ($row = $sth->fetchrow_hashref) {
	print "<result> $$row{Number} - $$row{Name} </result>";
}
print "</data1>";
print "</root>";
}

if ($saction eq 'CheckReport')
{
$sql = "select * from standard_reports where upper(reports_name) = upper('$param') and facility_type = $facility_id";
$sth = $dbh->prepare($sql);
$sth->execute();

print CGI->header('text/xml', 'Cache-Control: no-store, no-cache, must-revalidate', 'Cache-Control: post-check=0, pre-check=0', 'Pragma: no-cache');

print <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<result>
EOF
;
if($sth->rows()>0){
	print "true</result>";
}else
{
	print "false</result>";
}
}

if ($saction eq 'CheckException')
{
$sql = "select * from exceptions where upper(Name) = upper('$param')";
$sth = $dbh->prepare($sql);
$sth->execute();

print CGI->header('text/xml', 'Cache-Control: no-store, no-cache, must-revalidate', 'Cache-Control: post-check=0, pre-check=0', 'Pragma: no-cache');

print <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<result>
EOF
;
if($sth->rows()>0){
	print "true</result>";
}else
{
	print "false</result>";
}
}

if ($saction eq 'CheckEditReport')
{
$sql = "select * from standard_reports where upper(reports_name) = upper('$param') and upper(reports_name) != upper('$peerID') and facility_type = $facility_id";
$sth = $dbh->prepare($sql);
$sth->execute();

print CGI->header('text/xml', 'Cache-Control: no-store, no-cache, must-revalidate', 'Cache-Control: post-check=0, pre-check=0', 'Pragma: no-cache');

print <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<result>
EOF
;
if($sth->rows()>0){
	print "true</result>";
}else
{
	print "false</result>";
}
}

if ($saction eq 'CheckElement')
{
$sql = "select * from elements where upper(name) = upper('$param')";
$sth = $dbh->prepare($sql);
$sth->execute();

print CGI->header('text/xml', 'Cache-Control: no-store, no-cache, must-revalidate', 'Cache-Control: post-check=0, pre-check=0', 'Pragma: no-cache');

print <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<result>
EOF
;
if($sth->rows()>0){
	print "true</result>";
}else
{
	print "false</result>";
}
}
my $dataElement;
my $retValue;
if ($saction eq 'stdReportChkDuplicateUser')
{
$sql = "select element from standard_metrics where replace(Formulae, ' ', '') = replace('$duFormula', ' ', '') and facility_type = $facility_id ";
$sth = $dbh->prepare($sql);
$sth->execute();

print CGI->header('text/xml', 'Cache-Control: no-store, no-cache, must-revalidate', 'Cache-Control: post-check=0, pre-check=0', 'Pragma: no-cache');

print <<EOF
<?xml version="1.0" encoding="UTF-8"?>

EOF
;

if($sth->rows()>0){
	while($dataElement = $sth->fetchrow_hashref)
	{
		$retValue = $$dataElement{element}
	}
	print "<result>$retValue</result>";
}else
{
	print "<result> </result>";
}
}

if($saction eq 'LoadDetails'){
	$sql = "select metrics_id,element,formulae from standard_metrics where Formulae !='' and metrics_id ='$selID' order by element";
	$sth = $dbh->prepare($sql);
	$sth->execute();


	print CGI->header('text/xml', 'Cache-Control: no-store, no-cache, must-revalidate', 'Cache-Control: post-check=0, pre-check=0', 'Pragma: no-cache');

print <<EOF
<?xml version="1.0" encoding="UTF-8"?>

EOF
;
if($sth->rows()>0){

	while ($row = $sth->fetchrow_hashref)
	{
		$id = $$row{metrics_id};
		$fname = $$row{element};
		$bcoordinates = $$row{formulae};
	}
	print "<result>$fname-$bcoordinates</result>";
}else
{
	print "<result> </result>";
}
}
#------------------------------------------------------------------------------
sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }
   close INIFILE;
   return 1;
}
#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach my $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}

#------------------------------------------------------------------------------
