#!/usr/bin/perl

require "/var/www/cgi-bin/lib/req-formulas.pl";

use CGI;
use DBI;
use URI::Encode;
use strict;

my $uri = URI::Encode->new();

do "metrics_audit.pl";
my $cgi = new CGI;
my $param = $cgi->param('search');
my $peerID = $cgi->param('peerID');
my $saction = $cgi->param('saction');
my $worksheet = $cgi->param('worksheet');
my $lineno =  $cgi->param('lineno');
my $col =  $cgi->param('col');
my $isfor =  $cgi->param('isfor');
my $metric =  $cgi->param('metric');
my $txtExpression = $uri->decode($cgi->param('formula'));
my $metric_elem = $cgi->param('elem');
my $metric_name = $uri->decode($cgi->param('metric_name'));
my $oldFormula=$cgi->param("oldFormula");
my $oldElement=$cgi->param("oldElement");
my $rfc = $cgi->param("RFC");
my $facility_id = $cgi->param("facility_id");
my $create_provider_metric = $cgi->param("create_provider_metric");
#my $elem = $cgi->param('elem');
my $dbh;
my ($sql, $sth,$sql1,$sth1,$sql2,$sth2,$sql3,$sth3,$alert,$row);


# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my $defaultYear= $ini_value{'DEFAULT_YEAR'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

if ($saction eq 'update')
{
	my $metric_column_name;
	if($oldElement ne $metric_name)
	{
		&update("Update",$metric,$rfc,$metric_name,$oldElement);
	}
	if($oldFormula ne $txtExpression)
	{
		&update("Update",$metric,$rfc,$txtExpression,$oldFormula);
	}
	if ($create_provider_metric) {
		$metric_column_name = &convert_fieldname_to_columnname($metric_name);
		if (&validate_metric_column_name($metric_column_name, $facility_id)) {
			&update("Update",$metric,$rfc,$metric_column_name,"");
		}
		else {
			$metric_column_name = "";
		}
	}

if ($metric_column_name) {
	$sql = "UPDATE standard_metrics set isFormulae =$isfor ,Formulae = upper('$txtExpression') ,element = '$metric_name', quartile_db_column = '$metric_column_name' where  metrics_id = $metric and facility_type = $facility_id";
}
else {
	$sql = "UPDATE standard_metrics set isFormulae =$isfor ,Formulae = upper('$txtExpression') ,element = '$metric_name' where  metrics_id = $metric and facility_type = $facility_id";
}

$sth = $dbh->prepare($sql);
$sth->execute();

print CGI->header('text/xml');

print <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<result> Metric updated successfully </result>
EOF
;

}

if ($saction eq 'delete')
{
$alert = "";
$sql2 = "SELECT * from standard_reports_metrics where metrics_id = $metric ";
$sth2 = $dbh->prepare($sql2);
$sth2->execute();

if($sth2->rows() == 0 ){

&delete("Delete",$metric,$rfc,$oldFormula);
$sql = "DELETE from standard_metrics where metrics_id = $metric and facility_type = $facility_id";
$sth = $dbh->prepare($sql);
$sth->execute();

$sql1="DELETE from standard_reports_metrics where metrics_id = $metric ";
$sth1 = $dbh->prepare($sql1);
$sth1->execute();
$alert = "Metric deleted successfully";
}
else
{
$alert = "Metric cannot be deleted";
}
print CGI->header('text/xml');

print <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<result> $alert </result>
EOF
;

}

if ($saction eq 'loadReport')
{
$sql = "select sr.reports_name from standard_reports as sr inner join standard_reports_metrics as sm on  sm.reports_id=sr.reports_id where sm.metrics_id = $metric and sr.facility_type = $facility_id";
$sth = $dbh->prepare($sql);
$sth->execute();

print CGI->header('text/xml', 'Cache-Control: no-store, no-cache, must-revalidate', 'Cache-Control: post-check=0, pre-check=0', 'Pragma: no-cache');

print <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<root>
<data1>
EOF
;

while ($row = $sth->fetchrow_hashref) {
	print "<result> $$row{reports_name} </result>";

}
print "</data1>";
print "</root>";
qq{

};

}

if ($saction eq 'stdReportChkDuplicateUser')
{
$sql = "select element from standard_metrics where replace(Formulae, ' ', '') = replace('$txtExpression', ' ', '') and metrics_id != $metric and facility_type = $facility_id";
$sth = $dbh->prepare($sql);
$sth->execute();

$sql1="select metrics_id from standard_metrics where element = '$metric_name' and metrics_id != $metric and facility_type = $facility_id";

$sth1= $dbh->prepare($sql1);
$sth1->execute();

print CGI->header('text/xml', 'Cache-Control: no-store, no-cache, must-revalidate', 'Cache-Control: post-check=0, pre-check=0', 'Pragma: no-cache');

print <<EOF
<?xml version="1.0" encoding="UTF-8"?>

EOF
;
my $retVal="";
my $retValue="";
if($sth->rows()>0 ){
	$retValue = "Element already exists";
}
if($sth1->rows()>0){
	$retValue = "Element name already exists";
}
	print "<result>$retValue </result>";

qq{

};
}

if ($saction eq 'metricCreateDuplicateUser')
{
$sql = "select element from standard_metrics where replace(Formulae, ' ', '') = replace('$txtExpression', ' ', '') and facility_type = $facility_id";
$sth = $dbh->prepare($sql);
$sth->execute();

$sql1="select element from standard_metrics where element = '$metric_name' and facility_type = $facility_id";
$sth1= $dbh->prepare($sql1);
$sth1->execute();

print CGI->header('text/xml', 'Cache-Control: no-store, no-cache, must-revalidate', 'Cache-Control: post-check=0, pre-check=0', 'Pragma: no-cache');

print <<EOF
<?xml version="1.0" encoding="UTF-8"?>

EOF
;
my $retVal="";
my $retValue="";
if($sth->rows()>0 ){
	$retValue = "Element already exists";
}
if($sth1->rows()>0){
	$retValue = "Element name already exists";
}

	print "<result>$retValue </result>";

qq{

};
}

if ($saction eq 'save'){

my $is_valid = 0;
my $metric_column_name;
if ($create_provider_metric) {
	$metric_column_name = &convert_fieldname_to_columnname($metric_name);
	$is_valid = &validate_metric_column_name($metric_column_name, $facility_id);
}
if (!$is_valid) {
	$sql2 = "insert into standard_metrics(element,isFormulae, Formulae,facility_type) values('$metric_name',$isfor,upper('$txtExpression'),$facility_id)";
}
else {
	$sql2 = "insert into standard_metrics(element,isFormulae, Formulae,facility_type,quartile_db_column) values('$metric_name',$isfor,upper('$txtExpression'),$facility_id,'$metric_column_name')";
}
$sth2= $dbh->prepare($sql2);
$sth2->execute();

$sql3 = "select metrics_id from standard_metrics where element='$metric_name' and Formulae=upper('$txtExpression') and facility_type = $facility_id";
$sth3= $dbh->prepare($sql3);
$sth3->execute();
my $audit_metric_id = "";
while ($row = $sth3->fetchrow_hashref) {
	$audit_metric_id = $$row{metrics_id};
}

&insert("Create",$audit_metric_id,$txtExpression);

print CGI->header('text/xml');

print <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<result> Metric saved successfully </result>
EOF
;
}



if ($saction eq 'loadHistory')
{
$sql = "select log_date as log_date,concat(us.Last_Name,\",\",First_Name) as userName,user_ip as user_ip,action as action,sm.element as element,old_value as old_value,new_value as new_value,description as description from metric_history as mh left outer join userdetails as us on us.UsersID = mh.user_id left outer join standard_metrics as sm on sm.metrics_id = mh.metric_id where sm.metrics_id = $metric and sm.facility_type = $facility_id";
$sth = $dbh->prepare($sql);
$sth->execute();


print CGI->header('text/xml', 'Cache-Control: no-store, no-cache, must-revalidate', 'Cache-Control: post-check=0, pre-check=0', 'Pragma: no-cache');

print <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<root>
<data1>
EOF
;

while ($row = $sth->fetchrow_hashref) {
	my $result = $$row{element} . "~" . $$row{old_value} . "~" . $$row{new_value} . "~" . $$row{description};
	print "<result> $$row{log_date} </result>";
	print "<result1> $$row{userName} </result1>";
	print "<result2> $$row{user_ip} </result2>";
	print "<result3> $$row{action} </result3>";
	print "<result4> $$row{element} </result4>";
	print "<result5> $$row{old_value} </result5>";
	print "<result6> $$row{new_value} </result6>";
	print "<result7> $$row{description} </result7>";
}
print "</data1>";
print "</root>";
qq{

};

}



#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach my $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}

#------------------------------------------------------------------------------
