#!/usr/bin/perl 

#----------------------------------------------------------------------------
#  program: cr_batch1.pl
#  author: Steve Spicer
#  Written: Wed Oct 24 20:14:32 CDT 2007
#----------------------------------------------------------------------------

use strict;
use Spreadsheet::WriteExcel;
use CGI;
use DBI;

my $start_time = time;

my $cgi = new CGI;

my $action    = $cgi->param('action');    
my $project   = $cgi->param('project');   # project name
my $text      = $cgi->param('providers'); # aaa.txt bbb.txt
my $show_keys = $cgi->param('showkeys');
my $dir       = $cgi->param('direction');
my $year      = $cgi->param('year');
my @datasets  = $cgi->param('datasets');

my %ini_value;

unless (&read_inifile("milo.ini")) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

unless ($action eq 'go') {
   &display_form();
   exit(0);
}

$year = $ini_value{'DEFAULT_YEAR'} unless ($year =~ m/^\d\d\d\d$/);  # avoid any sql injection possibilities

my ($line, @list, $s, $len, $string, $provider, $n, $width, $value, $row, $column, $desc);

my @provider_list;

foreach $s (split /\n/,$text) {
   $s =~ s/[ \r\n]//g;
   $len = length($s);
   if ($len == 6 || $len == 11) {
      push @provider_list, $s;
   }
}

$n = @provider_list;

unless ($n) {
   &display_error("YOU MUST PROVIDE ONE OR MORE 6 CHARACTER PROVIDER ID'S");
   exit(0);
}

my $dbtype     = $ini_value{"DB_TYPE"};
my $dblogin    = $ini_value{"DB_LOGIN"};
my $dbpasswd   = $ini_value{"DB_PASSWORD"};
my $dbname     = $ini_value{"DB_NAME"};
my $dbserver   = $ini_value{"DB_SERVER"};

my $dbh;

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

my @report_definition;

my $fname;

foreach $fname (@datasets) {
   open(INPUT, "./packages/$fname");
   while ($line = <INPUT>) {
     chomp($line);
     push @report_definition, $line;
   }
   close INPUT;
}

my $workbook = Spreadsheet::WriteExcel->new("-");
#   $workbook->set_tempdir('/tmp');

my $sheet1 = $workbook->addworksheet("main");

my $amount_format = $workbook->addformat();
   $amount_format->set_num_format(0x2b);      #  (#,##0.00_); (#,##0.00)

my $money_format = $workbook->addformat();
   $money_format->set_num_format(0x2c);      #  (#,##0.00_); (#,##0.00)

my $amount_bold_format = $workbook->addformat();
   $amount_bold_format->set_num_format(0x2c);      #  (#,##0.00_); (#,##0.00)
   $amount_bold_format->set_bold();

my $date_format = $workbook->addformat();
   $date_format->set_num_format(0x0e);        #  m/d/yy
   $date_format->set_align("center");

my $number_format = $workbook->addformat();
   $number_format->set_num_format(0x01);      #  0 integer

my $comma_format = $workbook->addformat();
   $comma_format->set_num_format(0x26);   #  (#,###) red

my $format_bold = $workbook->addformat();
   $format_bold->set_bold();

my $big_format_bold = $workbook->addformat();
   $big_format_bold -> set_bold();
   $big_format_bold -> set_size(14);

my $format_bold_rotated = $workbook->addformat();
   $format_bold_rotated->set_bold();
   $format_bold_rotated->set_rotation(80);

my $format_bold_right  = $workbook->addformat();
   $format_bold_right->set_bold();
   $format_bold_right->set_align("right");

my $format = $workbook->addformat();
   $format->set_size(10);

my $col = 0;
my $row = 0;


$n = @provider_list;

if ($dir eq 'across') {
   if ($show_keys eq 'on') {
       $sheet1->set_column($col, $col, 24);
       ++$col;
   }
   $sheet1->set_column($col, $col, 40);
   ++$col;
   $sheet1->set_column($col, $col + $n, 15);
}

my $now = localtime();

$sheet1->write_string($row,   0, 'Healthcare Management Partners', $big_format_bold);
$sheet1->write_string(++$row, 0, 'Project Milo HCRIS Data Demonstration Material',$big_format_bold);
$sheet1->write_string(++$row, 0, "$project", $big_format_bold);
$sheet1->write_string(++$row, 0, "GENERATED: $now", $format_bold);

&process_report();

my $elapsed_time = time - $start_time;

# $sheet1->write_number($row, 0, $elapsed_time, $number_format);

$dbh->disconnect();

print "Content-type: application/vnd.ms-excel\n";
print "Content-Disposition: attachment; filename=cost_report_batch.xls\n";
print "Cache-Control: no-cache\n";
print "Pragma: no-cache\n\n";

$workbook->close();

exit(0);


#=============================< SUBROUTINES >================================

# across ... the hospitals left to right columns
# down  .... the hospitals are down the page

sub process_report
{
   $row += 2;
   $col  = 0;

   my $format = ($dir eq 'across') ? $format_bold : $format_bold_rotated;
   
   if ($show_keys eq 'on') {
      @list = ('KEY', 'DESCRIPTION');
   } else {
      @list = ('DESCRIPTION');
   }

   my $top_row  = $row;

   foreach $string (@list) {
      $sheet1->write_string($row, $col, $string, $format);
      if ($dir eq 'across') { ++$col; } else { ++$row;}
   }
   
   foreach $string (@provider_list) {
      $sheet1->write_string($row, $col, $string, $format_bold_right);
      if ($dir eq 'across') { ++$col; } else { ++$row;}
   }
   
   if ($dir eq 'across') { ++$row; } else { ++$col;}

   my ($yyyy, $worksheet, $wrow, $wcol, $fmt);
   
   foreach $line (@report_definition) {

      next if ($line =~ m/^\#/);
   
      ($worksheet, $wrow, $wcol, $fmt, $desc) = split /\|/,$line,-1;
   
      if ($dir eq 'across') { $col = 0;} else {$row = $top_row;}
   
      if ($show_keys eq 'on') {
         $sheet1->write_string($row, $col, "$worksheet,$wrow,$wcol,$fmt", $format);
         if ($dir eq 'across') { ++$col; } else { ++$row;}
      }

      $sheet1->write_string($row, $col, "$desc", $format);
      if ($dir eq 'across') { ++$col; } else { ++$row;}

      foreach $line (@provider_list) {

         $provider = $line;

         if ($provider =~ m/(\S{6})[:\/\t](\d{4})/)  {
            ($provider, $yyyy) = ($1, $2);
         } else {
            $yyyy = $year;
         }

         print STDERR "$provider,$yyyy,$row,$col\n";

         if ($workheet eq 'SPECIAL') {
            $value = &get_special_value($yyyy, $worksheet, $wrow, $wcol, $provider);
         } else {
            $value = &get_value($yyyy, $worksheet, $wrow, $wcol, $provider);
         }
   
         if ($value =~ m/^\-??[\d\.]+$/) {
             if ($fmt eq '$') {
                $sheet1->write_number($row, $col, $value, $money_format);
             } elsif ($fmt eq 'M') {
                $sheet1->write_number($row, $col, $value, $amount_format);
             } elsif ($fmt eq 'C') {
                $sheet1->write_number($row, $col, $value, $comma_format);
             } elsif ($fmt eq 'S') {
                $sheet1->write_string($row, $col, $value);
             } else {
                $sheet1->write_number($row, $col, $value);
             }
         } else {
             $sheet1->write_string($row, $col, $value);
         }
   
         if ($dir eq 'across') { ++$col } else { ++$row; }
      }
    
      if ($dir eq 'across') { ++$row; } else { ++$col; }
   }
}



#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon" />
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css" />
</HEAD>
<BODY>
<CENTER>
<BR>
<BR>
<FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
</CENTER>
</BODY>
</HTML>
};

}

#------------------------------------------------------------------------------
#

sub get_value
{
   my ($yyyy, $worksheet, $line, $column, $provider) = @_;

   my ($sth, $value);

   my $sql = qq(
    SELECT CR_VALUE         
      FROM CR_${yyyy}_RPT AA
 LEFT JOIN CR_${yyyy}_DATA BB
	ON AA.RPT_REC_NUM = BB.CR_REC_NUM
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
     WHERE PRVDR_NUM     = ?
   );

   unless ($sth = $dbh->prepare($sql)) {
       &display_error("Error preparing SQL: ", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   $sth->execute($worksheet, $line, $column, $provider);
   
   if ($sth->rows) {
      $value = $sth->fetchrow_array;
   }

   $sth->finish();

   return $value;
}

#----------------------------------------------------------------------------

sub get_special_value
{
   my ($yyyy, $worksheet, $line, $column, $provider) = @_;

   return 'SPECIAL';
}


#---------------------------------------------------------------------------

sub display_form
{
   print "Content-Type: text/html\n\n";

   my ($year, $year_options, $selected);


   my $default_year = $ini_value{'DEFAULT_YEAR'};

   foreach $year (split /,/,$ini_value{'HCRIS_YEARS'}) {
      $selected = ($year == $default_year) ? 'SELECTED' : '';
      $year_options .= "               <OPTION VALUE=\"$year\" $selected>$year</OPTION>\n";
   }

   my $package_list = '';

   opendir(DIR,"./packages");
   my @list = readdir(DIR);
   closedir(DIR);

   my ($fname, $url, $string); 

   foreach $fname (sort @list) {
      if ($fname =~ m/\.txt$/) {
         $string = uc $fname;
         $string =~ s/^\d+//;  
         $string =~ s/\.txt//i;
         $url = "<A HREF=\"/cgi-bin/setedit.pl?set=$fname\">$string</A>\n";
         $package_list .= "<INPUT NAME=\"datasets\" TYPE=\"checkbox\" VALUE=\"$fname\">$url<BR>\n";
      }
   }

   print qq{
<HTML>
<HEAD><TITLE>COST REPORTS</TITLE></HEAD>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon" />
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css" />
</HEAD>
<BODY>
<BR>
<BR>
<BR>
<BR>
<CENTER>
<FORM METHOD="POST" ACTION="/cgi-bin/cr_batch1.pl">
<TABLE CLASS="dentry" CELLPADDING="5" WIDTH="800">
   <TR>
      <TD ALIGN="CENTER" COLSPAN="3">
         <IMG SRC="/icons/hmp_logo1t.gif"><BR>
         <FONT FACE="verdana" SIZE="5">
         <B>COST REPORT SPREADSHEET GENERATOR</B><BR>
            PROTOTYPE / PROOF OF CONCEPT<BR>
         </FONT>
      </TD>
   </TR>
   <TR>
      <TD ALIGN="LEFT" COLSPAN="2" VALIGN="TOP">
         <FIELDSET>
         <LEGEND><B>PROJECT IDENTIFIER</B></LEGEND>
             <INPUT NAME="project" TYPE="TEXT" SIZE="45">
         </FIELDSET>
      </TD>
      <TD ALIGN="CENTER" ROWSPAN="3">
         <FONT SIZE="1">
         <B>PEER GROUP SELECTION</B><BR>
         CUT AND PASTE FROM PROVIDER NUMBERS INTO AREA BELOW USE A SINGLE COLUMN<BR>
         </FONT>
         <FONT COLOR="RED" SIZE="1">
         NOW SUPPORTS PROVIDER/YEAR<BR>
         </FONT>
         <TEXTAREA NAME="providers" COLS="20" ROWS="20" WRAP="HARD"></TEXTAREA>
         <BR>
         <BR>
      </TD>
   </TR>
   <TR>
      <TD VALIGN="TOP">
         <FIELDSET>
         <LEGEND><B>DEFAULT YEAR</B></LEGEND>
            <SELECT  NAME="year"><BR>
               $year_options
            </SELECT>
         </FIELDSET>
       </TD>
       </TD>
       <TD VALIGN="TOP">
         <FIELDSET>
         <LEGEND><B>REPORT DIRECTION</B></LEGEND>
            <INPUT NAME="direction" TYPE="radio" VALUE="across" CHECKED> ACROSS<BR>
            <INPUT NAME="direction" TYPE="radio" VALUE="down"> DOWN<BR>
         </FIELDSET>
         <FIELDSET>
         <LEGEND><B>OTHER OPTIONS</B></LEGEND>
            <INPUT NAME="showkeys" TYPE="checkbox"> INCLUDE REFERENCE CODES
         </FIELDSET>
       </TD>
    </TR>
    <TR>
       <TD COLSPAN="2" VALIGN="TOP">
         <FIELDSET>
         <LEGEND><B>SELECT DATASETS</B></LEGEND>
             $package_list
         </FIELDSET>
         <BR>
         <CENTER>
         <INPUT TYPE="hidden" NAME="action" VALUE="go">
         <INPUT TYPE="reset"  VALUE=" CLEAR ">
         <INPUT TYPE="submit" VALUE="  GENERATE SPREADSHEET  ">
         </CENTER>
         <BR>
     </TD>
   </TR>
</TABLE>
<BR>
</FORM>
</CENTER>
</BODY>
</HTML>
   };
}


#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#------------------------------------------------------------------------------


#Database: HCRISDB  Table: HS2006_RPT  Wildcard: %
#+--------------------+---------------+-------------------+------+-----+---------+-------+---------------------------------+---------+
#| Field              | Type          | Collation         | Null | Key | Default | Extra | Privileges                      | Comment |
#+--------------------+---------------+-------------------+------+-----+---------+-------+---------------------------------+---------+
#| RPT_REC_NUM        | int(11)       |                   | NO   |     |         |       | select,insert,update,references |         |
#| PRVDR_CTRL_TYPE_CD | char(2)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| PRVDR_NUM          | char(6)       | latin1_swedish_ci | NO   | MUL |         |       | select,insert,update,references |         |
#| NPI                | decimal(10,0) |                   | YES  |     |         |       | select,insert,update,references |         |
#| RPT_STUS_CD        | char(1)       | latin1_swedish_ci | NO   |     |         |       | select,insert,update,references |         |
#| FY_BGN_DT          | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| FY_END_DT          | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| PROC_DT            | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| INITL_RPT_SW       | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| LAST_RPT_SW        | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| TRNSMTL_NUM        | char(2)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| FI_NUM             | char(5)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| ADR_VNDR_CD        | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| FI_CREAT_DT        | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| UTIL_CD            | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| NPR_DT             | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| SPEC_IND           | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| FI_RCPT_DT         | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#+--------------------+---------------+-------------------+------+-----+---------+-------+---------------------------------+---------+
