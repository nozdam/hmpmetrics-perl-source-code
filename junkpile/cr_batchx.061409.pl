#!/usr/bin/perl 

#----------------------------------------------------------------------------
#  program: cr_batch1.pl
#  author: Steve Spicer
#  Written: Wed Oct 24 20:14:32 CDT 2007
#----------------------------------------------------------------------------

use strict;
use Spreadsheet::WriteExcel;
use Date::Calc qw(Delta_Days);
use CGI;
use DBI;

my $start_time = time;

my $cgi = new CGI;

my $action    = $cgi->param('action');    
my $text      = $cgi->param('providers'); # aaa.txt bbb.txt
my $show_keys = $cgi->param('showkeys');
my $dir       = $cgi->param('direction');
my $year      = $cgi->param('year');
my $imp       = $cgi->param('imp');       # import peer list  Y/N or nothing;


my @set       = $cgi->param('set');       # CR, SNF, HH ... not implemented yet
my @datasets  = $cgi->param('datasets');

my $remote_user  = $ENV{'REMOTE_USER'};

my @errors;

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

unless ($action eq 'go') {
   &display_form();
   exit(0);
}

$year = $ini_value{'DEFAULT_YEAR'} unless ($year =~ m/^\d\d\d\d$/);  # avoid any sql injection possibilities

my ($line, @list, $s, $len, $string, $provider, $n, $width, $value, $row, $column, $desc);

my @provider_list;

my @report_id_list;

foreach $s (split /\n/,$text) {
   $s =~ s/[ \r\n]//g;
   $len = length($s);
   if ($len == 6 || $len == 11) {
      push @provider_list, $s;
   }
}

$n = @provider_list;

unless ($n) {
   &display_error('YOU MUST PROVIDE ONE OR MORE 6 CHARACTER PROVIDER IDS');
   exit(0);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my ($dbh, $sql, $sth);

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error('ERROR connecting to database', $DBI::errstr);
   exit(1);
}

# establish a hash value for every "$provider:$year" pair in play that contains report_numbers, periods / other values

$sql = qq{
   SELECT RPT_REC_NUM, RPT_STUS_CD, FY_BGN_DT, FY_END_DT
     FROM CR_ALL_RPT
    WHERE PRVDR_NUM = ?
      AND RPT_YEAR  = ?
    ORDER BY RPT_STUS_CD DESC
    LIMIT 1
};

unless ($sth = $dbh->prepare($sql)) {
    &display_error("Error preparing SQL: ", $sth->errstr);
    $dbh->disconnect();
    exit(0);
}

my ($key, $yyyy, $rpt_num, $rpt_stus_cd, $fy_bgn_dt, $fy_end_dt);


my %mem_report_num;
my %mem_fy_bgn_dt;
my %mem_fy_end_dt;
my %mem_stus_cd;

foreach $string (@provider_list) {

   if ($string =~ m/(\S{6})[:\/\t](\d{4})/)  {
     ($provider, $yyyy) = ($1, $2);
   } elsif ($string =~ m/(\S{6})/) {
     ($provider, $yyyy) = ($1, $year); # defult year
   } else  {
     push @errors, "PROVIDER UNRECOGNIZED: [$string]";
     next;
   }

   unless ($sth->execute($provider, $yyyy)) {
      &display_error("Error executing SQL: ", $sth->errstr);
      $dbh->disconnect();
      exit(0);
   }
   
   if (($rpt_num, $rpt_stus_cd, $fy_bgn_dt, $fy_end_dt) = $sth->fetchrow_array) {

      $key = "$provider,$yyyy";

      $mem_report_num{$key} = $rpt_num;
      $mem_stus_cd{$key}    = $rpt_stus_cd;
      $mem_fy_bgn_dt{$key}  = $fy_bgn_dt;
      $mem_fy_end_dt{$key}  = $fy_end_dt;
   }

   
}
 
$sth->finish;

my @report_definition;

my $fname;

foreach $fname (@datasets) {
   open(INPUT, "./packages/$fname");
   while ($line = <INPUT>) {
     chomp($line);
     push @report_definition, $line;
   }
   close INPUT;
}

#----------------------------------------------------------------------------
#----------------------------------------------------------------------------
#----------------------------------------------------------------------------
#----------------------------------------------------------------------------

my $workbook = Spreadsheet::WriteExcel->new('-');
#  $workbook->set_tempdir('/tmp');

my $sheet1 = $workbook->addworksheet('main');

my $amount_format = $workbook->addformat();
   $amount_format->set_num_format(0x2b);           #  (#,##0.00_); (#,##0.00)

my $money_format = $workbook->addformat();
   $money_format->set_num_format(0x2c);            #  (#,##0.00_); (#,##0.00)

my $amount_bold_format = $workbook->addformat();
   $amount_bold_format->set_num_format(0x2c);      #  (#,##0.00_); (#,##0.00)
   $amount_bold_format->set_bold();

my $date_format = $workbook->addformat();
   $date_format->set_num_format(0x0e);        #  m/d/yy
   $date_format->set_align("center");

my $number_format = $workbook->addformat();
   $number_format->set_num_format(0x01);      #  0 integer

my $comma_format = $workbook->addformat();
   $comma_format->set_num_format(0x26);   #  (#,###) red

my $format_bold = $workbook->addformat();
   $format_bold->set_bold();

my $big_format_bold = $workbook->addformat();
   $big_format_bold -> set_bold();
   $big_format_bold -> set_size(14);

my $format_bold_rotated = $workbook->addformat();
   $format_bold_rotated->set_bold();
   $format_bold_rotated->set_rotation(80);

my $format_bold_right  = $workbook->addformat();
   $format_bold_right->set_bold();
   $format_bold_right->set_align("right");


my $col = 0;
my $row = 0;

$n = @provider_list;

if ($dir eq 'across') {
   if ($show_keys eq 'on') {
       $sheet1->set_column($col, $col, 24);
       ++$col;
   }
   $sheet1->set_column($col, $col, 40);
   ++$col;
   $sheet1->set_column($col, $col + $n, 15);
}

my $now = localtime();

$sheet1->write_string($row,   0, 'Healthcare Management Partners', $big_format_bold);
$sheet1->write_string(++$row, 0, 'Medicare Cost Report Data extract', $big_format_bold);
$sheet1->write_string(++$row, 0, "GENERATED: $now", $format_bold);

&process_report();

my $elapsed_time = time - $start_time;

# $sheet1->write_number($row, 0, $elapsed_time, $number_format);

$dbh->disconnect();

print "Content-type: application/vnd.ms-excel\n";
print "Content-Disposition: attachment; filename=cost_report_batch.xls\n";
print "Cache-Control: no-cache\n";
print "Pragma: no-cache\n\n";

$workbook->close();

exit(0);


#=============================< SUBROUTINES >================================

# across ... the hospitals left to right columns
# down  .... the hospitals are down the page

sub process_report
{
   $row += 2;
   $col  = 0;

   my $format = ($dir eq 'across') ? $format_bold : $format_bold_rotated;
   
   if ($show_keys eq 'on') {
      @list = ('KEY', 'DESCRIPTION');
   } else {
      @list = ('DESCRIPTION');
   }

   my $top_row  = $row;

   foreach $string (@list) {
      $sheet1->write_string($row, $col, $string, $format);
      if ($dir eq 'across') { ++$col; } else { ++$row;}
   }
   
   foreach $string (@provider_list) {
      $sheet1->write_string($row, $col, $string, $format_bold_right);
      if ($dir eq 'across') { ++$col; } else { ++$row;}
   }
   
   if ($dir eq 'across') { ++$row; } else { ++$col;}

   my ($yyyy, $worksheet, $wrow, $wcol, $fmt);
   
   foreach $line (@report_definition) {

      next if ($line =~ m/^\#/);
   
      ($worksheet, $wrow, $wcol, $fmt, $desc) = split /\|/,$line,-1;
   
      if ($dir eq 'across') { $col = 0;} else {$row = $top_row;}
   
      if ($show_keys eq 'on') {
         $sheet1->write_string($row, $col, "$worksheet,$wrow,$wcol,$fmt", $format);
         if ($dir eq 'across') { ++$col; } else { ++$row;}
      }

      $sheet1->write_string($row, $col, "$desc", $format);
      if ($dir eq 'across') { ++$col; } else { ++$row;}

      foreach $line (@provider_list) {

         $provider = $line;

         if ($provider =~ m/(\S{6})[:\/\t](\d{4})/)  {
            ($provider, $yyyy) = ($1, $2);
         } else {
            $yyyy = $year;
         }

#        print STDERR "$provider,$yyyy,$row,$col\n";

         if ($worksheet eq 'SPECIAL') {
            $value = &get_special_value($provider, $yyyy, $worksheet, $wrow, $wcol);
         } else {
            $value = &get_value($provider, $yyyy, $worksheet, $wrow, $wcol);
         }
   
         if ($value =~ m/^\-??[\d\.]+$/) {
             if ($fmt eq '$') {
                $sheet1->write_number($row, $col, $value, $money_format);
             } elsif ($fmt eq 'M') {
                $sheet1->write_number($row, $col, $value, $amount_format);
             } elsif ($fmt eq 'C') {
                $sheet1->write_number($row, $col, $value, $comma_format);
             } elsif ($fmt eq 'S') {
                $sheet1->write_string($row, $col, $value);
             } else {
                $sheet1->write_number($row, $col, $value);
             }
         } else {
             if ($fmt eq 'U' && $value =~ m/http:\/\//) {
                $sheet1->write_url($row, $col, $value, 'LINK');
             } else {
                $sheet1->write_string($row, $col, $value);
             }
         }
   
         if ($dir eq 'across') { ++$col } else { ++$row; }
      }
    
      if ($dir eq 'across') { ++$row; } else { ++$col; }
   }
}



#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon" />
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css" />
</HEAD>
<BODY>
<CENTER>
<BR>
<BR>
<FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
</CENTER>
</BODY>
</HTML>
};

}

#------------------------------------------------------------------------------

sub get_value_by_address
{
    my ($year, $provider, $address) = @_;
    my ($worksheet, $line, $column) = split /:/,$address;
    return(&get_value($provider, $year, $worksheet, $line, $column));
}

#----------------------------------------------------------------------------

sub get_value
{
   my ($provider, $yyyy, $worksheet, $line, $column) = @_;

   my $rpt_rec_num = $mem_report_num{"$provider,$yyyy"};

   my ($sth, $value);

   my $sql = qq(
    SELECT CR_VALUE         
      FROM CR_${yyyy}_DATA
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
   );

   unless ($sth = $dbh->prepare($sql)) {
       &display_error("Error preparing SQL: ", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sth->execute($rpt_rec_num, $worksheet, $line, $column)) {
       &display_error("Error executing SQL: ", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }
   
   if ($sth->rows) {
      $value = $sth->fetchrow_array;
   }

   $sth->finish();

   return $value;
}

#---------------------------------------------------------------------------

sub get_special_value
{
   my ($provider, $yyyy, $worksheet, $line, $column) = @_;

   my $key = "$provider,$yyyy";

   my @status_desc = (
     'void',
     'As Submitted',
     'Settled w/o Audit',
     'Settled with Audit',
     'Reopened',
     'Amended',
   );

   if ($line eq '00100' and $column eq '0100') {
      return $mem_report_num{$key};
   } elsif ($line eq '00100' and $column eq '0200') {
      return "http://www.spicerware.com/cgi-bin/cr_explore.pl?reportid=$mem_report_num{$key}";
   } elsif ($line eq '00200' and $column eq '0100') {
      return $status_desc[$mem_stus_cd{$key}];
   } elsif ($line eq '00300' and $column eq '0100') {
      return $mem_fy_bgn_dt{$key};
   } elsif ($line eq '00300' and $column eq '0200') {
      return $mem_fy_end_dt{$key};
   } elsif ($line eq '00300' and $column eq '0300') {
      return (&compute_days_in_period($key));
   } elsif ($line eq '00400' and $column eq '0100') {   # quick ratio
      return (&compute_quick_ratio($key));
   } elsif ($line eq '00500' and $column eq '0100') {   # ebitdar
      return (&compute_ebitdar($key));
   } elsif ($line eq '00600' and $column eq '0100') {   # operating margin
      return (&compute_operating_margin($key));
   } elsif ($line eq '00700' and $column eq '0100') {   # excess margin
      return (&compute_excess_margin($key));
   } elsif ($line eq '00800' and $column eq '0100') {   # days in patient ar
      return (&compute_days_in_patient_ar($key));
   } elsif ($line eq '00900' and $column eq '0100') {   # return on assets
      return (&compute_return_on_assets($key));
   } elsif ($line eq '01000' and $column eq '0100') {   # return on equity
      return (&compute_return_on_equity($key));
   } elsif ($line eq '01100' and $column eq '0100') {   # Personnel expense percent of totoal operating revenue
      return (&compute_personnel_expense_pct_tor($key));
   }
}

#----------------------------------------------------------------------------

sub compute_days_in_period
{
   my ($key) = @_;

   my ($a_yyyy, $a_mm, $a_dd, $b_yyyy, $b_mm, $b_dd);

   if ($mem_fy_bgn_dt{$key} =~ m/(\d\d\d\d)\-(\d\d)\-(\d\d)/) {
      ($a_yyyy, $a_mm, $a_dd) = ($1,$2,$3);
   } else {
      return ('');
   }
   if ($mem_fy_end_dt{$key} =~ m/(\d\d\d\d)\-(\d\d)\-(\d\d)/) {
      ($b_yyyy, $b_mm, $b_dd) = ($1,$2,$3);
   } else {
      return ('');
   }

   my $days = &Delta_Days($a_yyyy, $a_mm, $a_dd, $b_yyyy, $b_mm, $b_dd);
   return ($days + 1);
}

#---------------------------------------------------------------------------

sub compute_quick_ratio
{
   my ($key) = @_;
   my ($provider, $year) = split /,/,$key;

   my $total_current_assets         = &get_value_by_address($year, $provider, 'G000000:01100:0100');
   my $total_current_liabilities    = &get_value_by_address($year, $provider, 'G000000:03600:0100');
   my $inventory                    = &get_value_by_address($year, $provider, 'G000000:00700:0100');

   my $quick_ratio = 'NA';

   if ($total_current_liabilities) {  # avoid divide by zero
      $quick_ratio               = ($total_current_assets - $inventory) / $total_current_liabilities;
   }
   return $quick_ratio;
}

#---------------------------------------------------------------------------

sub compute_ebitdar
{
   my ($key) = @_;
   my ($provider, $year) = split /,/,$key;

   my $net_income            = &get_value_by_address($year, $provider, 'G300000:03100:0100');
   my $interest_expense      = &get_value_by_address($year, $provider, 'A700003:00500:1100');  
   my $depreciation_expense  = &get_value_by_address($year, $provider, 'A700003:00500:0900');  
   my $lease_cost            = &get_value_by_address($year, $provider, 'A700003:00500:1000');  

   return($net_income + $interest_expense + $depreciation_expense + $lease_cost);
}

#---------------------------------------------------------------------------
sub compute_operating_margin
{
   my ($key) = @_;
   my ($provider, $year) = split /,/,$key;

   my $operating_revenue     = &get_value_by_address($year, $provider, 'G300000:00300:0100');
   my $operating_expense     = &get_value_by_address($year, $provider, 'G300000:00400:0100');

   if ($operating_revenue) {
      return (($operating_revenue - $operating_expense) / $operating_revenue * 100);
   } else {
      return('NA');
   }
}

#---------------------------------------------------------------------------

sub compute_excess_margin
{
   my ($key) = @_;
   my ($provider, $year) = split /,/,$key;

   my $operating_revenue     = &get_value_by_address($year, $provider, 'G300000:00300:0100');
   my $non_operating_revenue = &get_value_by_address($year, $provider, 'G300000:02500:0100');
   my $operating_expense     = &get_value_by_address($year, $provider, 'G300000:00400:0100');

   if ($operating_revenue + $non_operating_revenue) {
      return (($operating_revenue - $operating_expense + $non_operating_revenue) / ($operating_revenue + $non_operating_revenue) * 100);
   } else {
      return('NA');
   }
}


#----------------------------------------------------------------------------

sub compute_days_in_patient_ar
{
   my ($key) = @_;
   my ($provider, $year) = split /,/,$key;

   my $accounts_receivable          = &get_value_by_address($year, $provider, 'G000000:00400:0100');
   my $allowance_for_uncollectable  = &get_value_by_address($year, $provider, 'G000000:00600:0100');
   my $operating_revenue            = &get_value_by_address($year, $provider, 'G300000:00300:0100');

   if ($operating_revenue) {
      return(($accounts_receivable - $allowance_for_uncollectable) / ($operating_revenue / 365));
   } else {
      return 'N/A';
   }

}

#---------------------------------------------------------------------------

sub compute_return_on_equity
{
   my ($key) = @_;
   my ($provider, $year) = split /,/,$key;

   my $net_income            = &get_value_by_address($year, $provider, 'G300000:03100:0100');
   my $total_assets          = &get_value_by_address($year, $provider, 'G000000:02700:0100');
   my $total_liabilities     = &get_value_by_address($year, $provider, 'G000000:04300:0100');

   if ($total_assets - $total_liabilities) {
      return($net_income / ($total_assets - $total_liabilities) * 100);
   } else {
      return 'N/A';
   }
}
#---------------------------------------------------------------------------

sub compute_return_on_assets
{
   my ($key) = @_;
   my ($provider, $year) = split /,/,$key;

   my $net_income            = &get_value_by_address($year, $provider, 'G300000:03100:0100');
   my $total_assets          = &get_value_by_address($year, $provider, 'G000000:02700:0100');

   if ($total_assets) {
      return(($net_income / $total_assets) * 100);
   } else {
      return 'N/A';
   }
}

#----------------------------------------------------------------------------

sub compute_personnel_expense_pct_tor
{
   my ($key) = @_;
   my ($provider, $year) = split /,/,$key;

   my $salary_expense     = &get_value_by_address($year, $provider, 'A000000:10100:0100');
   my $fringe_benefits    = &get_value_by_address($year, $provider, 'A000000:00500:0200');
   my $operating_revenue  = &get_value_by_address($year, $provider, 'G300000:00300:0100');

   my $contract_labor     = &get_value_by_address($year, $provider, 'S300002:00900:0300');
      $contract_labor    += &get_value_by_address($year, $provider, 'S300002:00901:0300');
      $contract_labor    += &get_value_by_address($year, $provider, 'S300002:00902:0300');
      $contract_labor    += &get_value_by_address($year, $provider, 'S300002:01000:0300');
      $contract_labor    += &get_value_by_address($year, $provider, 'S300002:01001:0300');
      $contract_labor    += &get_value_by_address($year, $provider, 'S300002:01100:0300');
      $contract_labor    += &get_value_by_address($year, $provider, 'S300002:01200:0300');
      $contract_labor    += &get_value_by_address($year, $provider, 'S300002:01201:0300');


   if ($operating_revenue) {
      return(($salary_expense + $contract_labor + $fringe_benefits) / $operating_revenue * 100);
   } else {
      return 'N/A';
   }
}


#----------------------------------------------------------------------------

sub display_form
{

   my ($year, $year_options, $selected);

   my $default_year = $ini_value{'DEFAULT_YEAR'};

   foreach $year (reverse sort split /,/,$ini_value{'HCRIS_YEARS'}) {
      $selected = ($year == $default_year) ? 'SELECTED' : '';
      $year_options .= "               <OPTION VALUE=\"$year\" $selected>$year</OPTION>\n";
   }

   my $package_list = '';

   opendir(DIR,"./packages");
   my @list = readdir(DIR);
   closedir(DIR);

   my ($fname, $editlink, $string); 

   foreach $fname (sort @list) {
      if ($fname =~ m/\.txt$/) {
         $string = uc $fname;
         $string =~ s/^\d+//;  
         $string =~ s/\.txt//i;
         $editlink = "<A HREF=\"#\" onClick=\"MyWindow=window.open('/cgi-bin/setedit.pl?set=$fname','aaaMyWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,coyhistory=no,resizable=yes,width=850,height=700,left=25,top=25'); return false;\">$string</A>";
         $package_list .= "<INPUT NAME=\"datasets\" TYPE=\"checkbox\" VALUE=\"$fname\">$editlink<BR>\n";
      }
   }

   my $line;

   my $default_list = '';

   if ($imp =~ m/^Y/i) {
      if (open INPUT, "userdata/$remote_user.txt") {
         while ($line = <INPUT>) {
            $default_list .= $line;
         }
      }
      close INPUT;
   }

   print "Content-Type: text/html\n";
   print "Cache-Control: no-cache\n";
   print "Pragma: no-cache\n\n";

   print qq{
<HTML>
<HEAD><TITLE>COST REPORTS</TITLE></HEAD>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon" />
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css" />
</HEAD>
<BODY>
<BR>
<BR>
<BR>
<BR>
<CENTER>
<FORM NAME="form1" METHOD="POST" ACTION="/cgi-bin/cr_batchx.pl">
<TABLE CLASS="dentry" CELLPADDING="5" WIDTH="600">
   <TR>
      <TD ALIGN="CENTER" COLSPAN="3">
         <IMG SRC="/icons/hmp_logo1t.gif"><BR>
         <FONT FACE="verdana" SIZE="5">
         <B>COST REPORT SPREADSHEET GENERATOR</B><BR>
         </FONT>
      </TD>
   </TR>
   <TR>
      <TD ALIGN="LEFT" COLSPAN="2" VALIGN="TOP">
         &nbsp;
      </TD>
      <TD ALIGN="CENTER" ROWSPAN="3">
         <FONT SIZE="1">
         <B>PEER GROUP SELECTION</B><BR>
         CUT AND PASTE FROM PROVIDER NUMBERS INTO AREA BELOW USE A SINGLE COLUMN<BR>
         </FONT>
         <FONT COLOR="RED" SIZE="1">
         NOW SUPPORTS PROVIDER/YEAR<BR>
         </FONT>
         <TEXTAREA NAME="providers" COLS="20" ROWS="20" WRAP="HARD">$default_list</TEXTAREA>
         <BR>
         <INPUT TYPE="button" VALUE=" CLEAR " onclick="window.document.form1.providers.value=''">
         <BR>
         <BR>
      </TD>
   </TR>
   <TR>
       </TD>
       <TD VALIGN="TOP">
         <FIELDSET>
         <LEGEND><B>REPORT DIRECTION</B></LEGEND>
            <INPUT NAME="direction" TYPE="radio" VALUE="across"> ACROSS<BR>
            <INPUT NAME="direction" TYPE="radio" VALUE="down" CHECKED> DOWN<BR>
         </FIELDSET>
         <FIELDSET>
         <LEGEND><B>OTHER OPTIONS</B></LEGEND>
            <INPUT NAME="showkeys" TYPE="checkbox"> INCLUDE GRID-CODES<BR>
            <SELECT  NAME="year">
               $year_options
            </SELECT> DEFAULT YEAR
         </FIELDSET>
       </TD>
    </TR>
    <TR>
       <TD COLSPAN="1" VALIGN="TOP">
         <FIELDSET>
         <LEGEND><B>SELECT DATASETS</B></LEGEND>
             $package_list
         </FIELDSET>
         <BR>
         <CENTER>
         <INPUT TYPE="hidden" NAME="action" VALUE="go">
         <INPUT TYPE="reset"  VALUE=" CLEAR ">
         <INPUT TYPE="submit" VALUE="  GENERATE SPREADSHEET  ">
         </CENTER>
         <BR>
     </TD>
   </TR>
</TABLE>
<BR>
</FORM>
</CENTER>
</BODY>
</HTML>
   };
}


#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#------------------------------------------------------------------------------


#Database: HCRISDB  Table: HS2006_RPT  Wildcard: %
#+--------------------+---------------+-------------------+------+-----+---------+-------+---------------------------------+---------+
#| Field              | Type          | Collation         | Null | Key | Default | Extra | Privileges                      | Comment |
#+--------------------+---------------+-------------------+------+-----+---------+-------+---------------------------------+---------+
#| RPT_REC_NUM        | int(11)       |                   | NO   |     |         |       | select,insert,update,references |         |
#| PRVDR_CTRL_TYPE_CD | char(2)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| PRVDR_NUM          | char(6)       | latin1_swedish_ci | NO   | MUL |         |       | select,insert,update,references |         |
#| NPI                | decimal(10,0) |                   | YES  |     |         |       | select,insert,update,references |         |
#| RPT_STUS_CD        | char(1)       | latin1_swedish_ci | NO   |     |         |       | select,insert,update,references |         |
#| FY_BGN_DT          | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| FY_END_DT          | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| PROC_DT            | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| INITL_RPT_SW       | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| LAST_RPT_SW        | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| TRNSMTL_NUM        | char(2)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| FI_NUM             | char(5)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| ADR_VNDR_CD        | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| FI_CREAT_DT        | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| UTIL_CD            | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| NPR_DT             | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| SPEC_IND           | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| FI_RCPT_DT         | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#+--------------------+---------------+-------------------+------+-----+---------+-------+---------------------------------+---------+
