#!/usr/bin/perl -w

#----------------------------------------------------------------------------
#  program: exp1.pl
#  author: Steve Spicer
#  Written: Wed Oct 24 20:14:32 CDT 2007
#----------------------------------------------------------------------------

use strict;
use CGI;
use DBI;

my $cgi = new CGI;

my $action    = $cgi->param('action');  
my $year      = $cgi->param('year');
my $provider  = $cgi->param('provider'); 
my $worksheet = $cgi->param('worksheet');
my $row       = $cgi->param('line');
my $col       = $cgi->param('column');

my %ini_value;

unless (&read_inifile("milo.ini")) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $year_list = '';

my ($text, $selected, $yyyy);

foreach $yyyy (2004 .. 2007) {
    $selected = ($yyyy eq $year) ? 'SELECTED' : '';
    $year_list .= $text = "   <OPTION VALUE=\"$yyyy\" $selected>$yyyy</OPTION>\n";
}

#----------------------------------------------------------------------------

open INPUT, "wk_xwalk.txt";

my ($line, $key, $string, %hash);
   
my $worksheet_list = '';
  
while ($line = <INPUT>) {
   chomp($line);
    ($key, $string) = split / /,$line,2;
    $selected = ($key eq $worksheet) ? 'SELECTED' : '';
    $text = "   <OPTION VALUE=\"$key\" $selected>$string</OPTION>\n";
    $worksheet_list .= $text;
    $hash{$key} = $string;
}
   
close INPUT;

#----------------------------------------------------------------------------

my ($address, $value);

if ($action eq 'go') {
   ($address,$value) = &query_value($year, $provider, $worksheet, $row, $col);
}

&display_form();

exit(0);

#----------------------------------------------------------------------------


sub query_value
{

   my ($year, $provider, $worksheet, $row, $col) = @_;

   my $worksheet_name = $hash{$worksheet};

   my $wrow = sprintf("%05.0f",$row * 100);

   my $wcol = sprintf("%04.0f",$col * 100.0);

   my $dbtype     = $ini_value{"DB_TYPE"};
   my $dblogin    = $ini_value{"DB_LOGIN"};
   my $dbpasswd   = $ini_value{"DB_PASSWORD"};
   my $dbname     = $ini_value{"DB_NAME"};
   my $dbserver   = $ini_value{"DB_SERVER"};

   my ($sql, $dbh, $sth);

   unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
      &display_error("ERROR connecting to database", $DBI::errstr);
      exit(1);
   }

   $sql = qq/
    SELECT CR_VALUE         
      FROM CR_${year}_RPT AA
 LEFT JOIN CR_${year}_DATA BB
        ON AA.RPT_REC_NUM = BB.CR_REC_NUM
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
     WHERE PRVDR_NUM     = ?
   /;

   unless ($sth = $dbh->prepare($sql)) {
       &display_error("Error preparing sql query", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   $sth->execute($worksheet, $wrow, $wcol, $provider);

   my ($value) = $sth->fetchrow_array;

   $dbh->disconnect();


   my $address = "$worksheet:$wrow:$wcol";

   return($address, $value);
}


#----------------------------------------------------------------------------

sub display_error
{
   my @list = @_;

   my ($string, $s);

   foreach $s (@list) {
      $string .= "$s<BR>\n";
   }

   print "Content-Type: text/html\n\n";

   print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE></HEAD>
<BODY>
<CENTER>
<BR>
<BR>
<FONT SIZE="5" COLOR="RED">
<B>ERROR:</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
</CENTER>
</BODY>
</HTML>
   };

   return(0);
}

#----------------------------------------------------------------------------

sub display_form
{
   my $now = localtime();

   my $xlsfile = sprintf ("255296_%s.XLS",substr($address,0,1));

   my $link = "<A HREF=\"/$xlsfile\" TARGET=\"_blank\" TITLE=\"CLICK FOR A SAMPLE BLANK REPORT FROM THIS SECTION IN EXCEL FORMAT\">XLS FORM</A>";

   $link = '' unless ($action eq 'go');
   
   print "Content-Type: text/html\n\n";
   
   print qq{
   <HTML>
   <HEAD>
   <TITLE>COST REPORT RESEARCH FISHING TOOL</TITLE>
   <LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon">
   <LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
   </HEAD>
   <BODY>
   <CENTER>
   <BR>
   <BR>
   <IMG SRC="/icons/battleship_logo.gif">
   <BR>
   <FONT SIZE="3">
   <B>COST REPORT "GRID" FINDER</B><BR>
   </FONT>
   <FORM METHOD="GET" ACTION="/cgi-bin/gofish.pl">
   <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="8" WIDTH="800" BGCOLOR="#E0E0B0">
      <TR>
         <TD ALIGN="RIGHT">PROVIDER / YEAR</TD>
         <TD ALIGN="LEFT">
             <INPUT TYPE="text" NAME="provider" SIZE="6" VALUE="$provider" TITLE="ENTER 6 DIGIT PROVIDER NUMBER"> /
             <SELECT NAME="year" TITLE="CHOOSE A COST REPORT YEAR">
                $year_list
             </SELECT>
         </TD>
      </TR>
      <TR>
         <TD ALIGN="RIGHT">WORKSHEET</TD>
         <TD ALIGN="LEFT">
             <SELECT NAME="worksheet">
                $worksheet_list
             </SELECT>
         </TD>
      </TR>
      <TR>
         <TD ALIGN="RIGHT">LINE / COLUMN</TD>
         <TD ALIGN="LEFT">
             <INPUT TYPE="text" NAME="line"   SIZE="5" VALUE="$row" TITLE="ENTER WORKSHEET LINE NUMBER"> /
             <INPUT TYPE="text" NAME="column" SIZE="3" VALUE="$col" TITLE="ENTER WORKSHEET COLUMN">
         </TD>
      </TR>
      <TR>
         <TD ALIGN="RIGHT">ADDRESS</TD>
         <TD ALIGN="LEFT"><FONT SIZE="4">$address</FONT>&nbsp;&nbsp; $link</TD>
      </TR>
      <TR>
         <TD ALIGN="RIGHT">VALUE</TD>
         <TD ALIGN="LEFT"><FONT SIZE="4" COLOR="#009900"><B>$value</B></FONT></TD>
      </TR>
   </TABLE>
   <BR>
   <INPUT TYPE="hidden" NAME="action" VALUE="go">
   <INPUT TYPE="submit" VALUE="  QUERY COST REPORT ">
   </FORM>
   </CENTER>
   </BODY>
   </HTML>
   };
}


#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#------------------------------------------------------------------------------

