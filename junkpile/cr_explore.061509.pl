#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: cr_explore.pl
#  author: Steve Spicer
#  Written: Wed Oct 24 20:14:32 CDT 2007
#
#  Research Data Assistance Center
#  http://www.resdac.umn.edu/Tools/TBs/TN-005-revised.asp
#----------------------------------------------------------------------------

use strict;
use CGI;
use DBI;

my $cgi = new CGI;

my $action    = $cgi->param('action');  
my $year      = $cgi->param('year');
my $provider  = $cgi->param('provider'); 
my $dataset   = $cgi->param('dataset'); 
my $worksheet = $cgi->param('worksheet');
my $reportid  = $cgi->param('reportid');

my %ini_value;


my %status_lookup = (
 1 => 'As Submitted',
 2 => 'Settled w/o Audit',
 3 => 'Settled with Audit',
 4 => 'Reopened',
 5 => 'Amended',
);

my $start_time = time;

unless (&read_inifile('milo.ini')) {
   &display_error('CAN\'T READ milo.ini');
   exit(1);
}

my ($selected, $yyyy);

my $year_options = '';

foreach $yyyy (split /,/,$ini_value{HCRIS_YEARS}) {
    $selected = ($yyyy eq $year) ? 'SELECTED' : '';
    $year_options .= "   <OPTION VALUE=\"$yyyy\" $selected>$yyyy</OPTION>\n";
}


if ($provider) {
   $provider = uc($provider);
   unless ($provider =~ m/[A-Z0-9]{6}/) {
       &display_error("PROVIDER NUMBER MUST BE 6 CHARACTERS");
       exit(0);
   }
}

unless ($provider or $reportid) {
   &display_form();
   exit(0);
}

#----------------------------------------------------------------------------

open INPUT, "wk_xwalk.txt";

my ($line, $key, $string, %hash);
   
while ($line = <INPUT>) {
   chomp($line);
    ($key, $string) = split / /,$line,2;
    $hash{$key} = $string;
}
   
close INPUT;

my %table_def;

opendir(DIR,'./definitions');
my @dir = readdir(DIR);
closedir(DIR);

foreach $string (@dir) {
   if ($string =~ m/^(\S{7})\.def$/) {   # look for def files
      $table_def{$1} = 'Y';
   }
}



#----------------------------------------------------------------------------

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my ($dbh, $sql, $sth);

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

my ($rpt_status, $rpt_year, $rpt_provider, $rpt_stus_cd, $fy_bgn_dt, $fy_end_dt);



if ($reportid) {
   $sql = qq{
      SELECT RPT_YEAR, PRVDR_NUM, RPT_STUS_CD,
             DATE_FORMAT(FY_BGN_DT, '%m-%d-%Y'),
             DATE_FORMAT(FY_END_DT, '%m-%d-%Y')
        FROM CR_ALL_RPT
       WHERE RPT_REC_NUM = ?
   };

   unless ($sth = $dbh->prepare($sql)) {
      &display_error("Error preparing SQL query", $sth->errstr);
      $dbh->disconnect();
      exit(0);
   }

   unless ($sth->execute($reportid)) {
      &display_error("Error executing SQL query", $sth->errstr);
      $dbh->disconnect();
      exit(0);
   }

   ($rpt_year, $rpt_provider, $rpt_stus_cd, $fy_bgn_dt, $fy_end_dt) = $sth->fetchrow_array;

   $provider   = $rpt_provider;  # compatibility for now
   $year       = $rpt_year;      # compatibility for now
   $rpt_status = $status_lookup{$rpt_stus_cd};

   $sth->finish();
}

my ($hospital_name, $hospital_address, $hospital_city, $hospital_state, $hospital_zip);

if ($provider) {
   $hospital_name    = &get_value_by_address($year, $provider, "S200000:00200:0100");
   $hospital_address = &get_value_by_address($year, $provider, "S200000:00100:0100");
   $hospital_city    = &get_value_by_address($year, $provider, "S200000:00101:0100");
   $hospital_state   = &get_value_by_address($year, $provider, "S200000:00101:0200");
   $hospital_zip     = &get_value_by_address($year, $provider, "S200000:00101:0300");
}


if ($reportid) {
   $year = $rpt_year;
   $hospital_name    = &get_value_by_addr($year, $reportid, "S200000:00200:0100");
   $hospital_address = &get_value_by_addr($year, $reportid, "S200000:00100:0100");
   $hospital_city    = &get_value_by_addr($year, $reportid, "S200000:00101:0100");
   $hospital_state   = &get_value_by_addr($year, $reportid, "S200000:00101:0200");
   $hospital_zip     = &get_value_by_addr($year, $reportid, "S200000:00101:0300");
}

if ($worksheet) {
   &explore_worksheet($provider, $year, $worksheet);
} else {
   &explore_provider($provider, $year);
}

$dbh->disconnect();

exit(0);

#----------------------------------------------------------------------------

sub explore_provider
{

   my ($provider, $year) = @_;

   my ($worksheet, $value_count, $hover, $desc, $other, $link, $class);

   my $count = 0;

   my ($sth, $sql);

   $sql = qq/
    SELECT CR_WORKSHEET, COUNT(*)
      FROM CR_${year}_RPT AA
 LEFT JOIN CR_${year}_DATA BB
        ON AA.RPT_REC_NUM = BB.CR_REC_NUM
     WHERE PRVDR_NUM     = ?
  GROUP BY CR_WORKSHEET
   /;

   unless ($sth = $dbh->prepare($sql)) {
       &display_error("Error preparing SQL query", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sth->execute($provider)) {
       &display_error("Error executing SQL query", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sth->rows()) {
     &display_error("NOTHING AVAILABLE FOR PROVIDER $provider");
     return(0);
   }

   print "Content-Type: text/html\n\n";

   print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
<HEAD><TITLE>COST REPORT EXPLORER</TITLE>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon">
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
</HEAD>
<BODY>
<CENTER>
<BR>
<TABLE BORDER="0" CELLPADDING="2" WIDTH="900">
   <TR>
     <TD ALIGN="LEFT" VALIGN="TOP">
        <FONT SIZE="4">
        <B>
        $hospital_name<BR>
        $hospital_address<BR>
        $hospital_city, $hospital_state  $hospital_zip<BR>
        </B>
        </FONT>
     </TD>
     <TD ALIGN="RIGHT" VALIGN="TOP">
        PROVIDER:<BR>
        REPORTID:<BR>
        FY BEGIN:<BR>
        FY END:<BR>
        STATUS:<BR>
     </TD>
     <TD ALIGN="LEFT" VALIGN="TOP">
        <B>
        <FONT COLOR="#EE0000">
        $provider<BR>
        $reportid<BR>
        $fy_bgn_dt<BR>
        $fy_end_dt<BR>
        $rpt_status<BR>
        </FONT>
        </B>
     </TD>
     <TD ALIGN="RIGHT" VALIGN="TOP">
        <IMG SRC="/icons/CMSLogo.gif">
     </TD>
   </TR>
</TABLE>

<TABLE BORDER="1" CELLPADDING="2" CELLSPACING="0" WIDTH="900">
   <TR CLASS="title">
     <TD ALIGN="CENTER"><B>LINK</B></TD>
     <TD ALIGN="CENTER"><B>WORKSHEET</B></TD>
     <TD ALIGN="LEFT"><B>DESCRIPTION</B></TD>
     <TD ALIGN="RIGHT"><B>VALUES</B></TD>
     <TD ALIGN="CENTER"><B>ALTERNATE<BR>VIEW</B></TD>
   </TR>
   };

   my ($xlink, $xlsfile);

   while (($worksheet, $value_count) = $sth->fetchrow_array) {

      $desc = $hash{$worksheet};

      $xlsfile = sprintf ("255296_%s.XLS",substr($worksheet,0,1));

      $xlink = "<A HREF=\"/$xlsfile\" TARGET=\"_blank\" TITLE=\"CLICK FOR A SAMPLE BLANK REPORT FROM THIS SECTION IN EXCEL FORMAT\"><IMG BORDER=\"0\" SRC=\"/icons/excel_icon.gif\"></A>";

#      $link = "<A HREF=\"/cgi-bin/cr_explore.pl?provider=$provider&year=$year&worksheet=$worksheet\">$value_count</A>";

      if ($reportid) {
         $link = "<A HREF=\"/cgi-bin/cr_explore.pl?reportid=$reportid&worksheet=$worksheet\">$value_count</A>";
      } else {
         $link = "<A HREF=\"/cgi-bin/cr_explore.pl?provider&year=$year&worksheet=$worksheet\">$value_count</A>";
      }

      $desc = '&nbsp;' unless ($desc);

      $class = (++$count % 2) ? 'odd' : 'even';

      if ($table_def{$worksheet} eq 'Y') {
         $other = "<A HREF=\"/cgi-bin/cr_table.pl?reportid=$reportid&worksheet=$worksheet\"><IMG BORDER=\"0\" SRC=\"/icons/table_icon.jpg\"></A>";
      } else {
         $other = '';
      }

      print "   <TR CLASS=\"$class\">\n";
      print "      <TD ALIGN=\"CENTER\">$xlink</TD>\n";
      print "      <TD ALIGN=\"CENTER\">$worksheet</TD>\n";
      print "      <TD ALIGN=\"LEFT\">$desc</TD>\n";
      print "      <TD ALIGN=\"RIGHT\">$link</TD>\n";
      print "      <TD ALIGN=\"CENTER\">$other</TD>\n";
      print "   </TR>\n";
   }

   print qq{
</TABLE>
<FONT SIZE="1">
$count ROWS<BR>
</FONT>
</CENTER>
</BODY>
</HTML>
   };

   return(0);
}







#----------------------------------------------------------------------------

sub explore_worksheet
{

   my ($provider, $year, $worksheet) = @_;

   my ($address, $row, $col, $value, $class, $align, $editlink);

   my $desc = $hash{$worksheet};

   my $count = 0;

   my ($sth, $sql);

   if ($reportid) {

      $sql = qq/
       SELECT CR_LINE, CR_COLUMN, CR_VALUE, KB_DESC
         FROM CR_${year}_DATA
    LEFT JOIN KB_TABLE ON KB_ADDRESS = CONCAT(CR_WORKSHEET, ':', CR_LINE, ':', CR_COLUMN)
        WHERE CR_REC_NUM    = ?
          AND CR_WORKSHEET  = ?
      /;

      unless ($sth = $dbh->prepare($sql)) {
          &display_error("Error preparing sql query", $sth->errstr);
          $dbh->disconnect();
          exit(0);
      }

      unless ($sth->execute($reportid, $worksheet)) {
          &display_error("Error preparing sql query", $sth->errstr);
          $dbh->disconnect();
          exit(0);
      }

   } else {
      $sql = qq/
       SELECT CR_LINE, CR_COLUMN, CR_VALUE, KB_DESC
         FROM CR_${year}_RPT AA
    LEFT JOIN CR_${year}_DATA BB
           ON AA.RPT_REC_NUM = BB.CR_REC_NUM
    LEFT JOIN KB_TABLE ON KB_ADDRESS = CONCAT(CR_WORKSHEET, ':', CR_LINE, ':', CR_COLUMN)
        WHERE PRVDR_NUM     = ?
          AND CR_WORKSHEET  = ?
      /;

      unless ($sth = $dbh->prepare($sql)) {
          &display_error("Error preparing sql query", $sth->errstr);
          $dbh->disconnect();
          exit(0);
      }

      unless ($sth->execute($provider, $worksheet)) {
          &display_error("Error preparing sql query", $sth->errstr);
          $dbh->disconnect();
          exit(0);
      }
   }

   unless ($sth->rows()) {
     &display_error("NOTHING AVAILABLE FOR PROVIDER $provider");
     return(0);
   }

   print "Content-Type: text/html\n\n";

   print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
<HEAD><TITLE>COST REPORT EXPLORER</TITLE>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon">
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
</HEAD>
<BODY>
<CENTER>
<BR>
<TABLE BORDER="0" CELLPADDING="2" WIDTH="95%">
   <TR>
     <TD ALIGN="LEFT" VALIGN="TOP">
        <FONT SIZE="4">
        <B>
        $hospital_name<BR>
        $hospital_address<BR>
        $hospital_city, $hospital_state  $hospital_zip<BR><BR>
        </B>
        </FONT>
     </TD>
     <TD ALIGN="RIGHT" VALIGN="TOP">
       WORKSHEET:<BR>
        PROVIDER:<BR>
        FY BEGIN:<BR>
          FY END:<BR>
          STATUS:<BR>
     </TD>
     <TD ALIGN="LEFT" VALIGN="TOP">
        <B>
        <FONT COLOR="#EE0000">
        $worksheet $desc<BR>
        $provider<BR>
        $fy_bgn_dt<BR>
        $fy_end_dt<BR>
        $rpt_status<BR>
        </FONT>
        </B>
     </TD>
     <TD ALIGN="RIGHT" VALIGN="TOP">
        <IMG SRC="/icons/CMSLogo.gif">
     </TD>
   </TR>
</TABLE>

</FONT>
<TABLE BORDER="1" CELLPADDING="2" CELLSPACING="0" WIDTH="95%">
   <TR CLASS="title">
     <TD ALIGN="CENTER"><B>ADDRESS</B></TD>
     <TD ALIGN="CENTER"><B>DESCRIPTION IF POPULATED</B></TD>
     <TD ALIGN="CENTER"><B>LINE</B></TD>
     <TD ALIGN="CENTER"><B>COLUMN</B></TD>
     <TD ALIGN="CENTER"><B>VALUE</B></TD>
   </TR>
   };

   while (($row, $col, $value, $desc) = $sth->fetchrow_array) {

      $address = "$worksheet:$row:$col";

      if ($row =~ m/^(\d\d\d)(\d\d)/) {
         $row = "$1.$2";
         $row =~ s/^0+//;
         $row =~ s/\.0+$//;
         $row = '0' if ($row eq '');
      }

      if ($col =~ m/^(\d\d)(\d\d)/) {
         $col = "$1.$2";
         $col =~ s/\.0+$//;
         $col =~ s/^0+//;
         $col = '0' if ($col eq '');
      }

      $align = 'LEFT';

      if ($value =~ m/^-?\d+$|-?\d*\.\d+$/) {
         $align = 'RIGHT';
#        $value = &comify($value);
      }

      $editlink = "<A HREF=\"#\" onClick=\"MyWindow=window.open('/cgi-bin/kb_update.pl?address=$address','MyWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=750,height=600,left=25,top=25'); return false;\">$address</A>";

      $class = (++$count % 2) ? 'odd' : 'even';

      print "   <TR CLASS=\"$class\">\n";
      print "      <TD ALIGN=\"CENTER\">$editlink</TD>\n";
      print "      <TD ALIGN=\"LEFT\">$desc</TD>\n";
      print "      <TD ALIGN=\"CENTER\">$row</TD>\n";
      print "      <TD ALIGN=\"CENTER\">$col</TD>\n";
      print "      <TD ALIGN=\"$align\">$value</TD>\n";
      print "   </TR>\n";
   }

   print qq{
</TABLE>
<FONT SIZE="1">
$count ROWS<BR>
</FONT>
</CENTER>
</BODY>
</HTML>
   };

   return(0);
}

#----------------------------------------------------------------------------


sub get_value_by_address
{
   my ($year, $provider, $address) = @_;

   my ($worksheet, $row, $col) = split /[-:\|]/,$address;

   my ($sth, $sql);

   $sql = qq/
    SELECT CR_VALUE         
      FROM CR_${year}_RPT AA
 LEFT JOIN CR_${year}_DATA BB
        ON AA.RPT_REC_NUM = BB.CR_REC_NUM
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
     WHERE PRVDR_NUM     = ?
   /;

   unless ($sth = $dbh->prepare($sql)) {
       &display_error("Error preparing sql query", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   $sth->execute($worksheet, $row, $col, $provider);

   my ($value) = $sth->fetchrow_array;

   return($value);
}


#---------------------------------------------------------------------------

sub get_value_by_addr
{
   my ($year, $reportid, $address) = @_;

   my ($worksheet, $row, $col) = split /[-:\|]/,$address;

   my ($sth, $sql);

   $sql = qq/
    SELECT CR_VALUE         
      FROM CR_${year}_DATA BB
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
   /;

   unless ($sth = $dbh->prepare($sql)) {
       &display_error("Error preparing sql query", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   $sth->execute($reportid, $worksheet, $row, $col);

   my ($value) = $sth->fetchrow_array;

   return($value);
}


#----------------------------------------------------------------------------

sub display_error
{
   my @list = @_;

   my ($string, $s);

   foreach $s (@list) {
      $string .= "$s<BR>\n";
   }

   print "Content-Type: text/html\n\n";

   print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
<HEAD><TITLE>ERROR</TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
</HEAD>
<BODY>
<CENTER>
<BR>
<BR>
<BR>
<BR>
<FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<TABLE CLASS="error" WIDTH="600">
   <TR>
      <TD ALIGN="CENTER">
          <FONT SIZE="4">
          <BR>
          $string
          <BR>
          </FONT>
      </TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" />
</FORM>
</CENTER>
</BODY>
</HTML>
   };

   return(0);
}


#----------------------------------------------------------------------------


sub display_form
{
   print "Content-Type: text/html\n\n";
   
   print qq{
   <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
   <HTML>
   <HEAD>
   <TITLE>COST REPORT EXPLORER</TITLE>
   <LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon">
   <LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
   </HEAD>
   <BODY>
   <CENTER>
   <BR>
   <BR>
   <BR>
   <BR>
   <BR>
   <BR>
   <FORM METHOD="GET" ACTION="/cgi-bin/cr_explore.pl">
   <TABLE CLASS="ob" BORDER="0" CELLPADDING="5" WIDTH="500">
      <TR>
         <TD COLSPAN="2" ALIGN="CENTER">
             <IMG SRC="/icons/hmp_logo1t.gif"><BR>
             <FONT SIZE="8" COLOR="RED">
             <B>PROJECT MILO</B><BR>
             </FONT>
             <FONT SIZE="4">
             <B>COST REPORT EXPLORER</B><BR>
             </FONT>
         </TD>
      </TR>
      </TR>
         <TD ALIGN="RIGHT">PROVIDER<BR>FY BEGIN:<BR>FY END:<BR>

         </TD>
         <TD ALIGN="LEFT">
             <INPUT TYPE="text" NAME="provider" SIZE="6" VALUE="$provider" TITLE="ENTER 6 DIGIT PROVIDER NUMBER"><BR>
             $fy_bgn_dt<BR>
             $fy_end_dt<BR>
         </TD>
       </TR>
       <TR>
         <TD ALIGN="RIGHT">COST REPORT YEAR</TD>
         <TD ALIGN="LEFT">
             <SELECT NAME="year" TITLE="CHOOSE A COST REPORT YEAR">
                $year_options
             </SELECT>
         </TD>
      </TR>
      <TR>
         <TD COLSPAN="2" ALIGN="CENTER">
             <INPUT TYPE="hidden" NAME="action" VALUE="go">
             <INPUT TYPE="submit" VALUE="  QUERY COST REPORT ">
         </TD>
      </TR>
   </TABLE>
   </FORM>
   </CENTER>
   </BODY>
   </HTML>
   };
}


#----------------------------------------------------------------------------

sub comify
{
  local($_) = shift;
  1 while s/^(-?\d+)(\d{3})/$1,$2/;
  return($_);
}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#------------------------------------------------------------------------------

