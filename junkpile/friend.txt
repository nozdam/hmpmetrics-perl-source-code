S200000|00200|0200|N|Provider Number
S200000|01700|0100|S|Cost Reporting Begin Date*
S200000|01700|0200|S|Cost Reporting End Date*
S200000|00200|0100|S|Hospital
S200000|00100|0100|S|Address
S200000|00101|0100|S|City
S200000|00101|0200|S|State
S200000|00101|0300|S|Zip
S200000|00101|0400|S|County
#E300001|00140|0100|N|Inpatient IRF Average Daily Census**
A700003|00500|0900|C|Depreciation Expense
A700003|00500|1000|C|Lease Cost
A700003|00500|1100|C|Interest
C000001|10100|0500|C|Total Allowable Cost
C000001|10100|0600|C|Inpatient Charges
C000001|10100|0700|C|Outpatient Charges
D10A181|05200|0100|C|Medicare Capital Cost
D10A181|05300|0100|C|Medicare Operations Cost
D40A180|10300|0200|C|Medicare Inpatient Charges
E00A18A|00200|0100|C|Outlier Payments Prior 10/1/97
E00A18A|00201|0100|C|Outlier Payments After 10/1/97
E00A18A|02200|0100|C|Subtotal (reduced for D&C) - 
E00A18B|03200|0100|C|Subtotal
E30C183|05500|0200|C|Total Amount Payable to Provider
E31B181|01700|0100|C|Total Amount Payable to Provider
E32B181|01700|0100|C|Total Amount Payable to Provider
G000000|00100|0100|C|Cash on Hand
G000000|00200|0100|C|Market Securities
G000000|00400|0100|C|Accounts Receivable
G000000|00600|0100|C|Allowances for uncollectable
G000000|00700|0100|C|Inventory
G000000|01100|0100|C|Total Current Assets
G000000|02100|0100|C|Total fixed Assets
G000000|02200|0100|C|Investments
G000000|02600|0100|C|Total Other Assets
G000000|02700|0100|C|Total Assets
G000000|03600|0100|C|Total Current Liabilities
G000000|04200|0100|C|Total Long Term Liabilities
G000000|04300|0100|C|Total Liabilities
G000000|05100|0100|C|Total Fund Balances
G200000|00100|0100|C|Hospital IP Routine Service Revenue
G200000|01800|0100|C|Revenue from OP Services in an IP Setting*
G200000|01700|0200|C|Revenue from IP Services in an OP Setting*
G200000|01800|0200|C|Outpatient Services Revenue
G200000|02500|0100|C|Total IP Revenues
G200000|02500|0200|C|Total OP Revenues
G300000|00100|0100|C|Total Operating Revenue
G300000|00200|0100|C|Contractual Allowances and Discounts*
G300000|00300|0100|C|Net Patient Revenue
G300000|00400|0100|C|Total Operating Expenses
G300000|00500|0100|C|Net Income From Service to Patients
G300000|00600|0100|C|Other Income Contributions, Donations, etc
G300000|00700|0100|C|Other Income From Investments
G300000|02300|0100|C|Other Income - Governmant Appropriations
G300000|02500|0100|C|Total Other Income
G300000|03000|0100|C|Total Other Expenses
G300000|03100|0100|C|Total Net Income or Loss
L00A181|00300|0100|C|Cap. DRG Outlier Pmnts Prior 10/1/97
L00A181|00301|0100|C|Cap. DRG Outlier Pmnts After 10/1/97
S300001|01200|0400|N|Medicare Patient Days
S300001|01200|0500|N|Medicaid Patient Days
S300001|01200|0100|N|Beds*
S300001|01200|0600|N|Patient Days
S300001|01200|1000|N|FTEs
S300001|01200|1300|N|Medicare Discharges
S300001|01200|1400|N|Medicaid Discharges
S300001|01200|1500|N|Total Discharges
S300002|00100|0100|C|Total Salary
