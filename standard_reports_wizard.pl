#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: cr_batchx.pl
#  author: Steve Spicer
#  Written: Wed Oct 24 20:14:32 CDT 2007
#  Revised: Sun Jun 14 22:31:17 CDT 2009
#----------------------------------------------------------------------------

use strict;
use Spreadsheet::WriteExcel;
use Date::Calc qw(Delta_Days);
use CGI;
use DBI;
do "common_header.pl";

do "pagefooter.pl";
use CGI::Session;
do "audit.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $userType = $session->param("role");
my $facility = $cgi->param("facility");
my $stdRptSessionElements = $session->param('stdRptElements');
my $stdRptSessionProviders = $session->param('stdRptProviders');
my $stdRptSessionDataset = $session->param('stdRptDataset');
my $stdRptSessionFy = $session->param('stdRptFy');
my $stdRptSessionFoi = $session->param('stdRptFoi');
my $stdRptSessionDirection = $session->param('stdRptDirection');
my $stdRepDispOrder = $session->param('stdRepDispOrder');

my $callBackStdReport = $cgi->param('callBack');

if(!$userID)
{
	&sessionExpire;
}

my $start_time = time;

&view("View","Standard report customize page","Standard Report");
my $action    = $cgi->param('action');
my $text      = $cgi->param('providers'); # aaa.txt bbb.txt
my $show_keys = $cgi->param('showkeys');
my $dir       = $cgi->param('direction');
my $year      = $cgi->param('year');
my $imp       = $cgi->param('imp');       # import peer list  Y/N or nothing;
my @datasets  = $cgi->param('datasets');

my $peerID    = $cgi->param('peerID');

my $checkedYear    = $cgi->param('yearCheck');
my $callBackStdReport = $cgi->param('callBack');

if($callBackStdReport eq 'true'){}else{
	$stdRepDispOrder = '';
}

# String holding provider IDs separated by ';'
my $providerIds = $cgi->param('Ids');
my $test = '';
my @arrayOfIds = split(/:/,$providerIds);

# Load saved elements of selected element
my $loadid = $cgi->param('loadid');

my $remote_user  = $ENV{'REMOTE_USER'};

my %ini_value;


unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

# Database connection

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};

# Used in queries

my ($dbh, $sql, $sth);
my ($sql, $sth, $key);
my ($dbh, $sql,$row,$facility_id, $sth);
my ($sql1,$sth1,$sql2,$sth2,$report_table,$data_table);
unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error('ERROR connecting to database', $DBI::errstr);
   exit(1);
}

$sql = "select facility_id,report_table,data_table from tbl_facility_type where facility_name = '$facility'";
$sth = $dbh->prepare($sql);
$sth->execute();

while ($row = $sth->fetchrow_hashref) {
	$facility_id = $$row{facility_id};
	$report_table = $$row{report_table};
	$data_table = $$row{data_table};

}

# Load saved elements in combobox using subroutine

my %elementDetails = &getElementsDetails();

unless ($action eq 'go') {
   &display_form();
   exit(0);
}

$year = $ini_value{'DEFAULT_YEAR'} unless ($year =~ m/^\d\d\d\d$/);  # avoid any sql injection possibilities

my ($line, @list, $s, $len, $string, $provider, $n, $width, $value, $row, $column, $desc);

my ($key, $yyyy);

#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}
print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
<BR>
<BR>
};
&innerHeader();
print qq{

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}

#----------------------------------------------------------------------------


sub display_form
{
   my ($year, $year_options, $selected);
   opendir(DIR,"./packages");
   my @list = readdir(DIR);
   closedir(DIR);
   my ($fname, $editlink, $string);

   my $line;

   my $default_list = '';

   if ($imp =~ m/^Y/i) {
      if (open INPUT, "userdata/$remote_user.txt") {
         while ($line = <INPUT>) {
            $default_list .= $line;
         }
      }
      close INPUT;
   }

   #jayanth - to select the saved peer groups.

   # For admin show all peer groups.
   if($userType == 2){
   	$sql = "select * from peergroup_main where facility_type = $facility_id order by name";
   }# For internal user show peer groups of all internal users.
   elsif($userType == 1){
   	$sql = "select peergroup_main.PeerGroup_ID,peergroup_main.Name from peergroup_main,users,role where peergroup_main.CreatedBy = users.UserID and users.RoleID = role.RoleID and users.roleid = 1 and peergroup_main.facility_type = $facility_id order by peergroup_main.name";
   }# For external user show peer groups created by user who logs in.
   else{
   	$sql = "select peergroup_main.PeerGroup_ID,peergroup_main.Name from peergroup_main,users,role where peergroup_main.CreatedBy = users.UserID and users.RoleID = role.RoleID and peergroup_main.createdBy = $userID and peergroup_main.facility_type = $facility_id order by peergroup_main.name";
   }
   $sth = $dbh->prepare($sql);
   $sth->execute();


   print "Content-Type: text/html\n";
   print "Cache-Control: no-cache\n";
   print "Pragma: no-cache\n\n";

print qq{
<HTML>
<HEAD><TITLE>STANDARD REPORTS</TITLE>
    <link rel="stylesheet" type="text/css" media="all" href="/css/tree.css" />
	<link href="/css/standard_report/std_report_wizard.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/JS/tree.js"></script>
    <script type="text/javascript" src="/JS/AjaxScripts/prototype.js"></script>
    <script type="text/javascript" src="/JS/AjaxScripts/effects.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/controls.js"></script>
 <script type="text/javascript" src="/JS/standard_report/std_report_wizard.js"></script>
</head>
<body>
<BR>
<FORM NAME="frmStdReports" METHOD="POST" ACTION="/cgi-bin/std_reports_display.pl?facility=$facility">
<CENTER>
<div id='bodyDiv' style="position:absolute;z-index:100;display:none;background-image:url(/images/background-trans.png);height:96%;width:96%;"><div align='center'  id="displaybox" style="display: none;"></div></div>
<input type="hidden" name="facility" value="$facility">
};
&innerHeader();
print qq{

    <!--content start-->
    <table cellpadding="0" cellspacing="0" width="885px">
        <tr><td align="left">Welcome <b>
            	};

            	print $firstName . " " . $lastName;
            	print qq{
            	</b></td>
            	<td align="right">
            	<a href="/cgi-bin/login.pl?saction=signOut">Sign Out</a>
            	</td>
        	</tr>
    </table>
    <table cellpadding="0" cellspacing="0" width="885px">
        <tr>
            <td valign="top">
                <img src="/images/content_left.gif" /></td>
            <td valign="top" style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;
                text-align: left; padding: 10px">

<table width="100%" style="border:1px solid gray;" cellpadding="0" cellspacing="0"><tr><td>
    <table width="100%" bgcolor="#F4F5F7" border="1" cellpadding="5" cellspacing="0" id="HeaderTable">

        <tr class="Fnt11Pix" background="/images/register/grad_01_hover.gif">

            <td id="HeaderTableStep1" style="background-color:#81BEF7">

               <b> Step 1: </b>Select report(s)</td>

            <td id="HeaderTableStep2" style="background-color:Silver">

               <b> Step 2: </b>Select peer group</td>

            <td id="HeaderTableStep3" style="background-color:Silver">

                <b>Step 3: </b>Set Parameter(s)</td>
        </tr>

    </table>

<span id="Step1"><strong>
<table width="100%">
};
	if($loadid == 0){
		print "<tr bgcolor=\"#F4F5F7\"><td><input type=\"radio\" name=\"optConfigure\" onClick=\"Show(1)\" checked>&nbsp;<b>Select reports</b></td><td><input type=\"radio\" name=\"optConfigure\" onClick=\"Show(2)\">&nbsp;<b>Select saved reports</b></td></tr>\n";
	}else{
		print "<tr bgcolor=\"#F4F5F7\"><td><input type=\"radio\" name=\"optConfigure\" onClick=\"Show(1)\">&nbsp;<b>Select reports</b></td><td><input type=\"radio\" name=\"optConfigure\" onClick=\"Show(2)\" checked>&nbsp;<b>Select saved reports</b></td></tr>\n";
	}

print qq{

<tr><td colspan=2>

    <DIV style="background-color:#FFFFFF;position:static;width:100%;height:330px;overflow:scroll;overflow-x:hidden;">
};
if($loadid == 0){
	print "<select id=\"combo\" name=\"txtElement\" style=\"width:400px;visibility:hidden;\" onchange=\"loadElements(this.value)\">\n";
}else{
	print "<select id=\"combo\" name=\"txtElement\" style=\"width:400px;visibility:visible;\" onchange=\"loadElements(this.value)\">\n";

}
    			print "<option value=\"-1\">Select one</option>\n";

    		foreach $key(sort {$elementDetails{$a} cmp $elementDetails{$b} } keys %elementDetails){
    			if($loadid == $key){
		    		print "<OPTION VALUE=\"$key\" selected>$elementDetails{$key}</OPTION> \n";
		    	}else{
		    		print "<OPTION VALUE=\"$key\">$elementDetails{$key}</OPTION> \n";
		    	}
		}
print qq{
    </select>
    <span class="button"><input type="button" name="btnSetSortOrder" value="Set display order" onclick="setSortOrder(1)"></span>

};
    my ($sql1, $sth1);

    		    my ($sql2, $sth2);
    		 if($loadid == 0){
    		    $sql1 = "select reports_id,reports_name from standard_reports where facility_type = $facility_id order by reports_name";
    		 }else{
    		    $sql1 = "select distinct  standard_reports.reports_id as reports_id,
standard_reports.reports_name as reports_name
from standard_reports
inner join standard_reports_metrics
on  standard_reports_metrics.reports_id = standard_reports.reports_id
inner join elementdetails on
elementdetails.standard_reportdetailst_id =
standard_reports_metrics.metrics_id inner join elements
on elements.element_id = elementdetails.element_id
where elements.element_id = $loadid and
standard_reports.facility_type = $facility_id";
    		  }
    		    $sth1 = $dbh->prepare($sql1);
    		    $sth1->execute();

    		    my $str = "<ul class=\"tree\" nowrap>";
    		    my $i = 0;
    		    while (my $dataset = $sth1->fetchrow_hashref) {
    		     	#$str = $str."<li><input  type=\"checkbox\" name=\"chk_dataset$i\" id=\"chk_dataset$i\" onclick=\"dataSetSelect(this,$i)\" value=\"$$dataset{reports_id}\"><a href=\"#\" >".$$dataset{reports_name}."</a>";
    		     	$str = $str."<li><input  type=\"checkbox\" name=\"chk_dataset$i\" id=\"chk_dataset$i\" onclick=\"dataSetSelect(this,$i)\" value=\"$$dataset{reports_id}\" ";

    		     	#Getting data from std report display page and populating again
    		     	if($callBackStdReport eq 'true')
    		     	{
    		     		my $ifCheckDataset = 'false';
    		     		foreach $s (split /,/,$stdRptSessionDataset)
    		     		{
    		     			if($$dataset{reports_id} eq $s)
    		     			{
    		     				$ifCheckDataset = 'true';
    		     			}
    		     		}

    		     		if($ifCheckDataset eq 'true')
    		     		{
    		     			$str = $str." checked=\"yes\"><a href=\"#\" >".$$dataset{reports_name}."</a>";
    		     		}
    		     		else
    		     		{
    		     			$str = $str."><a href=\"#\" >".$$dataset{reports_name}."</a>";
    		     		}
    		     	}
    		     	else
    		     	{
    		     		if($loadid > 0){
					$str = $str." checked=\"yes\"><a href=\"#\" >".$$dataset{reports_name}."</a>";
    		     		}else{
    		     			$str = $str."><a href=\"#\" >".$$dataset{reports_name}."</a>";
    		     		}
    		     	}
    		     	if($loadid == 0){
    		     		$sql2 = "select * from standard_metrics
inner join standard_reports_metrics
 on standard_reports_metrics.metrics_id = standard_metrics.metrics_id
 inner join standard_reports
 on standard_reports.reports_id = standard_reports_metrics.reports_id
 where standard_reports_metrics.reports_id  = $$dataset{reports_id}
 and standard_reports.facility_type=$facility_id";
    		     	}else{
    		     		$sql2 = "select *
from standard_metrics as sm
inner join elementdetails as ed
on ed.standard_reportdetailst_id = sm.metrics_id
left outer join standard_reports_metrics as srm on
srm.metrics_id = sm.metrics_id
where ed.element_id = $loadid
and srm.reports_id = $$dataset{reports_id}";
    		     	}
    		     	$sth2 = $dbh->prepare($sql2);
    		     	$sth2->execute();
    					open(J, ">data1.txt");
	    print J $sql2;
	  close J;
    		     	$str = $str."<ul>";
    		     	my $cnt = 0;
    		     	while(my $dataElement = $sth2->fetchrow_hashref){
    		     		#$str = $str."<li><input type=\"checkbox\" name=\"chk_dataelement$i$cnt\" onclick=\"dataDeselect(this,$i)\" id=\"chk_dataelement$i$cnt\" value=\"$$dataElement{reports_details_id}\" checked=\"yes\"><a href=\"#\">".$$dataElement{element}."</a></li>";
    		     		$str = $str."<li><input type=\"checkbox\" name=\"chk_dataelement$i$cnt\" onclick=\"dataDeselect(this,$i)\" id=\"chk_dataelement$i$cnt\" value=\"$$dataElement{metrics_id}\" ";

    		     		#Getting data from std report display page and populating again
    		     		if($callBackStdReport eq 'true')
    		     		{
    		     			my $ifCheckElement = 'false';
    		     			foreach $s (split /,/,$stdRptSessionElements)
    		     			{

    		    				if($$dataElement{reports_details_id} eq $s)
    		    				{
    		    					$ifCheckElement = 'true';
    		    				}
    		     			}
    		     			if($ifCheckElement eq 'true')
    		     			{
    		     				$str = $str."checked=\"yes\"><a href=\"#\">".$$dataElement{element}."</a><input type=\"hidden\" name=\"element\" id=\"element$i$cnt\" value=\"$$dataElement{element}\"></li>";
    		     			}
    		     			else
    		     			{
    		     				$str = $str." ><a href=\"#\">".$$dataElement{element}."</a><input type=\"hidden\" name=\"element\" id=\"element$i$cnt\" value=\"$$dataElement{element}\"></li>";
    		     			}
    		     		}
    		     		else
    		     		{
    		     			if($loadid > 0){
    		     				$str = $str."checked=\"yes\"><a href=\"#\">".$$dataElement{element}."</a><input type=\"hidden\" name=\"element\" id=\"element$i$cnt\" value=\"$$dataElement{element}\"></li>";
    		     			}else{
    		     				$str = $str." ><a href=\"#\">".$$dataElement{element}."</a><input type=\"hidden\" name=\"element\" id=\"element$i$cnt\" value=\"$$dataElement{element}\"></li>";
    		     			}
    		     		}

    		     		$cnt++;
    		     	}
    		     	$str = $str."</ul>";
    		     	$str = $str."<span><input type=\"hidden\" style=\"width:0px;height:0px;\" name=\"len\" value=\"$cnt\"><input type=\"checkbox\" style=\"width:0px;height:0px;visibility:hidden;\" name=\"dsetlen\"></span></li>";
    		     	$i++;
    		    }
    		    	$str = $str."</ul>";


    		    $sth1->finish();
    		    $sth2->finish();

    		    print "$str";
    		    print "</DIV>";

print qq{
</td>
</tr>
</table>
</span>
<span id="Step2" style="display:none">

    <table bgcolor="#FFFFFF" class="gridstyle" width="100%" height="100%">
    			<tr>
    			  <td nowrap>
        			<select class="textboxstyle" style="width:80%;color:#0c4f7a;font-weight:bold;font-size:12;" name="peerGroupSelect" onChange="selectPeerGroup()">

        };

						print "<option value=0 class='gridHlink'>--Select Peer Group--</option>\n";
						while ($row = $sth->fetchrow_hashref) {
							if($peerID == $$row{PeerGroup_ID}){
								print "<option class='gridHlink' value=$$row{PeerGroup_ID} selected>$$row{Name}</option>\n";
							}else{
								print "<option class='gridHlink' value=$$row{PeerGroup_ID}>$$row{Name}</option>\n";
							}
	  					}

print qq{
	  				</select>
			 		&nbsp;<B>OR</B>&nbsp;<a href=\"#\" style="text-decoration:underline;" onclick=\"CallCreate()\">Select Provider</a><br>
		    	 	 </td>
				</tr>
				<tr align="center">
					<td><span  class="button"><INPUT TYPE="button" VALUE="CLEAR PROVIDERS"  onclick="window.document.frmStdReports.providers.value=''"></span></td>
				</tr>
				<tr>
				<td>
				<TEXTAREA NAME="providers" class="textboxstyle" COLS="43" ROWS="30" style="color:#0c4f7a;font-weight:bold;font-size:12;width:100%; height:265px; font-family:Verdana; font-size:10px;" readOnly>};
				foreach my $hospitalID (@arrayOfIds){print "$hospitalID\n";}
					#Getting data from std report display page and populating again
						if($callBackStdReport eq 'true'){print "$stdRptSessionProviders";
						}
				print qq{</TEXTAREA>
        		  </td>
        		  </tr>
	</table>
</span>
<span id="Step3" style="display:none">
    <div align="top"  style='background-color:#FFFFFF;border:1px solid gray;width:100%; height:100%;position:static;overflow:scroll;overflow-x:hidden;'>
    <table width="100%">
    	<tr>
    		<td width="20%">
            	      		  <table width="100%" height="100%" style="border:1px solid gray;" cellpadding="3" celspacing="3">
            	 		        };
            	 		        foreach my $fYears (@fYears)
    					{
    						#Getting data from std report display page and populating again
    						if($callBackStdReport eq 'true')
    						{
    							my $ifCheckElement = 'false';
    							foreach $s (split /,/,$stdRptSessionFy)
    							{
    								if($fYears eq $s)
    								{
    									$ifCheckElement = 'true';
    								}

    							}
    							if($ifCheckElement eq 'true')
    							{
    								print "<tr><td width=\"10px\"><INPUT TYPE='CHECKBOX' NAME='CHK1' value='$fYears' checked></td><td style='color:#0c4f7a;font-weight:bold;font-size:12;'>$fYears</td></tr>";
    							}
    							else
    							{
    								print "<tr><td width=\"10px\"><INPUT TYPE='CHECKBOX' NAME='CHK1' value='$fYears'></td><td style='color:#0c4f7a;font-weight:bold;font-size:12;'>$fYears</td></tr>";
    							}
    						}
    						else
    						{
    							if($fYears==$defaultYear)
    							{
    								print "<tr><td width=\"10px\"><INPUT TYPE='CHECKBOX' NAME='CHK1' value='$fYears' checked></td><td style='color:#0c4f7a;font-weight:bold;font-size:12;'>$fYears</td></tr>";
    							}
    							else
    							{
    								print "<tr><td width=\"10px\"><INPUT TYPE='CHECKBOX' NAME='CHK1' value='$fYears'></td><td style='color:#0c4f7a;font-weight:bold;font-size:12;'>$fYears</td></tr>";
    							}
    						}
             				}
             				print qq{
             			</table>
          	</td>
          	<td valign="top" width="35%">
          	<table style="border:1px solid gray;" class="gridstyle">
          	<tr>
          	<td>
          		<!--<INPUT TYPE="hidden" NAME="action" VALUE="go">-->
			<INPUT TYPE="hidden" NAME="yearCheck" >
			<INPUT TYPE="hidden" NAME="hdnRptDirection" >
		       <table width="100%" style="border:1px solid gray;" cellpadding="3" cellspacing="3">
		        <tr>
		       		  	<td colspan=2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        </tr>
			    	};

			    	#Getting data from std report display page and populating again
			    		if($callBackStdReport eq 'true')
			    		{
			    			if($stdRptSessionDirection eq 'across')
			    			{
			    				print qq{<tr><td colspan="2" style='color:#0c4f7a;font-weight:bold;font-size:12;'>Report Direction : </td></tr><tr><td colspan="2">&nbsp;</td></tr><tr><td><INPUT TYPE="radio" name="rptDirection" VALUE="across" checked></td><td> ACROSS </td></tr><tr><td><INPUT TYPE="radio" name="rptDirection" VALUE="down" ></td><td> DOWN</td></tr><tr><td colspan="2">&nbsp;</td></tr>};
			    			}
			    			else
			    			{
			    				print qq{<tr><td colspan="2" style='color:#0c4f7a;font-weight:bold;font-size:12;'>Report Direction : </td></tr><tr><td colspan="2">&nbsp;</td></tr><tr><td><INPUT TYPE="radio" name="rptDirection" VALUE="across"> </td><td>ACROSS </td><td></tr><tr><td><INPUT TYPE="radio" name="rptDirection" VALUE="down" checked></td><td> DOWN</td></tr><tr><td colspan="2">&nbsp;</td></tr>};
			    			}
			    		}
			    		else
			    		{
			    			print qq{<tr><td colspan="2" style='color:#0c4f7a;font-weight:bold;font-size:12;'>Report Direction : </tr><tr><td colspan="2">&nbsp;</td></tr><tr><td><INPUT TYPE="radio" name="rptDirection" VALUE="across"> </td><td>ACROSS</td></tr><tr><td> <INPUT TYPE="radio" name="rptDirection" VALUE="down" checked></td><td> DOWN</td></tr><tr><td colspan="2">&nbsp;</td></tr>};
			    		}

		      print qq{ </tr>
    				</table>
    				<INPUT TYPE="hidden" NAME="datasetIds" >
    				<INPUT TYPE="hidden" NAME="dataElementIds" >
    			};

    			if($callBackStdReport eq 'true'){
    				print "<INPUT TYPE=\"hidden\" NAME=\"dispOrder\" value=\"$stdRepDispOrder\">\n";
    			}else{
    				print "<INPUT TYPE=\"hidden\" NAME=\"dispOrder\" value=\"\">\n";
    			}
    				print qq{<input type="hidden" name="ElementNames">
          </td>
          <td valign="top" width="35%">
          	<table bgColor="#FFFFFF" width="100%" height="100%" align="center" cellpadding="3" cellspacing="3" style="border:1px solid gray;" >
		  <tr>
		  	<td colspan=2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<tr>
			<td colspan=2 style='color:#0c4f7a;font-weight:bold;font-size:12;'>Select any one option</td>
		</tr>
		<tr>
		  	<td colspan=2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		};
		  	#Getting data from std report display page and populating again

		  	if($callBackStdReport eq 'true')
		  	{
		  		if($stdRptSessionFoi eq 'true')
		  		{
		  			print qq{<tr><td valign="top"><input type="radio" name="foistatistics" value="true" checked></td><td valign="top" style="color:#006666;" nowrap>Include facility of Interest statistics</td></tr><tr><td valign="top"><input type="radio" name="foistatistics" value="false"></td><td valign="top" style="color:#006666;" nowrap>Exclude facility of Interest statistics</td></tr>};
		  		}
		  		else
		  		{
		  			print qq{<tr><td valign="top"><input type="radio" name="foistatistics" value="true" ></td><td valign="top" style="color:#006666;" nowrap>Include facility of Interest statistics</td></tr><tr><td valign="top"><input type="radio" name="foistatistics" value="false" checked></td><td valign="top" style="color:#006666;" nowrap>Exclude facility of Interest statistics</td></tr>};
		  		}
		  	}
		  	else
		  	{
		  		print qq{<tr><td valign="top"><input type="radio" name="foistatistics" value="true" checked></td><td valign="top" style="color:#006666;" nowrap>Include facility of Interest statistics</td></tr><tr><td valign="top"><input type="radio" name="foistatistics" value="false"></td><td valign="top" style="color:#006666;" nowrap>Exclude facility of Interest statistics</td></tr>};
		  	}

		print qq{
		<tr>
			<td colspan=2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		</table>
	  </td>
	  </tr>
	  <tr>
		<td colspan=2>
			<table width="100%" class="gridstyle" style="border-bottom:none;border-left:none;border-right:none;" cellspacing="5" cellpadding="5">
				<tr>
					 <td colspan=2 >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	  			</tr>
				<tr>
					<td><b>Save reports as </b></td><td ><INPUT style="width:250px" TYPE="text" name="txtelementname" id="txtelementname" onchange="toggle_username('txtelementname')" value=""/></td>
				</tr>

				<tr>	<td></td><td align="left"><span id="btnAction" class="button"><INPUT type="button" name="btnSaveElement" id="btnSaveElement" value="Save" onClick="javascript:elementStore()" /></span><font color=red>(It's not mandatory to save.)</font></td>

				</tr>
			</table>
		</td>
	  </tr>
	   <tr>
	  		<td colspan=2>
	  			<table width="100%" cellspacing="5" cellpadding="5">
	  				<tr>
	  					 <td colspan=2 >&nbsp;&nbsp;</td>
	  	  			</tr>
	  				<tr align="center">
	  					  <td colspan=2 align="center"><span class="button"><INPUT TYPE="button" id="SubmitFinal" VALUE="GENERATE REPORT" onClick="callsubmit()" disabled="disabled"></span></td>
	  	  			</tr>
	  			</table>
	  		</td>
	  </tr>
	  </table>
	  </td>
	</tr>
     </TABLE>
  </div>

</span>
</td>
<td valign="top">
                <img src="/images/content_right.gif" />
</td>
</tr>
<tr>
<td>
	<table width="100%" style="border:1px solid gray;"  cellpadding="3" cellspacing="3">

	    <tr>

	        <td>

	            <span class=\"button\"><input id="ButtonPrevious" type="button" value="Previous Step" disabled="disabled" name="" onclick="handleWizardPrevious()" /></span></td>

	        <td>

	            <span class=\"button\"><input id="ButtonNext" type="button" value="Next Step" name="Step2" onclick="handleWizardNext()" /></span></td>

	        <td>

	            <span class="button"><INPUT TYPE="button" VALUE="Close Wizard" onClick="window.location.href='/cgi-bin/standard_reports.pl?facility=$facility'" ></span></td>

	    </tr>

</table>
</td>
</tr>

    </table><iframe name="elementFrame" style="visibility:hidden;height:1px;width:1px;"></iframe>
    <!--content end-->
	};
&TDdoc;
	print qq{
    </CENTER>
</FORM>
</body>
</html>
  };
}

#-----------------------------------------------------------------------------


sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#-----------------------------------------------------------------------------

sub getElementsDetails
{

	my ($qry,$stmt,$record);

	$qry = "select * from elements where facility_type = $facility_id order by name";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();

	my %elementDetail;

	while($record = $stmt->fetchrow_hashref){
		$elementDetail{$$record{element_id}} = $$record{Name};
					}

	return %elementDetail;
}

#--------------------------------------------------------------------------------------
sub sessionExpire
{
print "Content-Type: text/html\n\n";
&headerScript();
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

#------------------------------------------------------------------------------


#Database: HCRISDB  Table: HS2006_RPT  Wildcard: %
#+--------------------+---------------+-------------------+------+-----+---------+-------+---------------------------------+---------+
#| Field              | Type          | Collation         | Null | Key | Default | Extra | Privileges                      | Comment |
#+--------------------+---------------+-------------------+------+-----+---------+-------+---------------------------------+---------+
#| RPT_REC_NUM        | int(11)       |                   | NO   |     |         |       | select,insert,update,references |         |
#| PRVDR_CTRL_TYPE_CD | char(2)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| PRVDR_NUM          | char(6)       | latin1_swedish_ci | NO   | MUL |         |       | select,insert,update,references |         |
#| NPI                | decimal(10,0) |                   | YES  |     |         |       | select,insert,update,references |         |
#| RPT_STUS_CD        | char(1)       | latin1_swedish_ci | NO   |     |         |       | select,insert,update,references |         |
#| FY_BGN_DT          | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| FY_END_DT          | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| PROC_DT            | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| INITL_RPT_SW       | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| LAST_RPT_SW        | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| TRNSMTL_NUM        | char(2)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| FI_NUM             | char(5)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| ADR_VNDR_CD        | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| FI_CREAT_DT        | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| UTIL_CD            | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| NPR_DT             | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| SPEC_IND           | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| FI_RCPT_DT         | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#+--------------------+---------------+-------------------+------+-----+---------+-------+---------------------------------+---------+

