#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: Registration.pl
#  author: Jayanth Raj
#----------------------------------------------------------------------------

use strict;
use CGI;
use DBI;

# use Session;
my $cgi = new CGI;
do "pagefooter.pl";
do "common_header.pl";
use CGI::Session;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");

if(!$userID)
{
	&sessionExpire;
}

my $dbh;
my ($sql, $sth, $key);

# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my $defaultYear= $ini_value{'DEFAULT_YEAR'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

my %statesDetails = &getStatesDetails();
my %passwordSecurityQry = &getPasswordSecurityQry();
my %roleDetails = &getRoleDetails();


print "Content-Type: text/html\n\n";

print qq{
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<title>PROJECT MILO User Management</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="/css/register/main.css" rel="stylesheet" type="text/css">
<link href="/css/register/qm.css" rel="stylesheet" type="text/css">
<link href="/css/tabs.css" rel="stylesheet" type="text/css" />   
<link href="/css/admin_panel/user_management/userManagement.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="/JS/admin_panel/user_management/userCreate.js" ></script>
</head>
<body leftmargin="0" topmargin="0" marginheight="20" marginwidth="0">
<CENTER>
};
&innerHeader();
print qq{
    <!--content start--> 
    <table cellpadding="0" cellspacing="0" width="885px">
        <tr>            
            <td valign="top" >	
		<ol id="toc">
		    <li><a href="/cgi-bin/userView.pl"><span>View</span></a></li> 	
		    <li class="current"><a href="/cgi-bin/userCreate.pl"><span>Create User</span></a></li>
		    <li><a href="/cgi-bin/userModify.pl"><span>Modify User</span></a></li>
		    <li><a href="/cgi-bin/userAudit.pl"><span>Audit Log</span></a></li>
		</ol>
		<div class="content" style="border:0px;padding:0em;">
		<table align="center" class="ColrBk01" width="100%" border="0" cellpadding="4" cellspacing="0">
		  <tbody><tr>
			<td class="Fnt11Pix" background="/images/register/grad_01_hover.gif"><b class="Colr03">Create new user</b></td>
			<td class="Fnt11Pix" background="/images/register/grad_01_hover.gif" align="right"><b class="Colr03"></b></td>
		  </tr>
		</tbody></table>
		<table class="ColrBk02" width="100%" border="0" cellpadding="2" cellspacing="0">
		  <tbody><tr>
			<td><form action="/cgi-bin/userActions.pl" method="post" name="frmSingerSLRegistry" id="frmSingerSLRegistry" onsubmit="return funcValidateForm();">
				<table width="100%"  bgcolor="#ffffff" border="0" cellpadding="4" cellspacing="0">
				  <tbody><tr>
					<td colspan="2" class="ColrBk03"><b>&nbsp;<font color="#0066cc">Login Details...</font></b></td>
					<td class="ColrBk03">&nbsp;</td>
					<td class="ColrBk03">&nbsp;</td>
				  </tr>
				  <tr>
					<td width="10%">&nbsp;</td>
					<td width="22%"><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Email ID</b></font></td>
					<td width="4%"><font color="#333399"><b>:</b></font></td>
					<td><input name="txtUserName" class="TextBox01" id="txtUserName" size="50" maxlength="200" type="text" onchange="toggle_username()"> <font color="#ff0000"> * (Will be used as Login Name)</font>
					
					<div id="username_exists" style="font-size: 11px;font-weight: bold;color:#FF3300"> </div>					
					</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><p><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Password</b></font></p></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtPassword" class="TextBox01" id="txtPassword" size="25" maxlength="20" type="password" onKeyUp="updatestrength(this.value)"> <font color="#ff0000">*</font> &nbsp;<font color="#333399">Password Strength </font>&nbsp; <img  id="strength" src="/images/password/tooshort.jpg" name="img" alt="" style="visibility:hidden;" />
					</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><p><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Confirm Password</b></font></p></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtPasswordConfirm" class="TextBox01" id="txtPasswordConfirm" size="25" maxlength="20" type="password"> <font color="#ff0000">*</font> </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7">If you forget your account details, Milo will ask you Security Question </b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><select name="txtSecurityQry" class="TextBox01" id="txtSecurityQry" onChange="checkOtherSecurityAns(this)">
						<option value="" SELECTED>--Select One--</option>
						};	               	
						foreach $key(sort {$passwordSecurityQry{$a} cmp $passwordSecurityQry{$b} } keys %passwordSecurityQry){            	
							    print "<OPTION VALUE=\"$key\">$passwordSecurityQry{$key}</OPTION> \n";            	
						}
						print qq{

					  <option value="0">- Type your question here -</option>
					  </select> <font color="#ff0000">*</font>					  
					  </td>
				  </tr>
				  <tr>
					  <td></td>
					  <td><div id="otherSecurityAnsLabel" style="font-size: 11px;font-weight: bold;color:#FF3300"> </td>
					  <td><div id="otherSecurityAnsColon" style="font-size: 11px;font-weight: bold;color:#FF3300"> </td>
					  <td>
					  <div id="otherSecurityAns" style="font-size: 11px;font-weight: bold;color:#FF3300"> </div>
					  </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Your Answer</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtSecurityAns" class="TextBox01" id="txtSecurityAns" size="80" maxlength="500" type="text"> <font color="#ff0000">*</font> </td>
				  </tr>
				  <tr>
				  	<td>&nbsp;</td>
				  	<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> User Type</b></font></td>
				  	<td><font color="#333399"><b>:</b></font></td>
				  	<td><select name="txtUserType" class="TextBox01" id="txtUserType">
						<option value="0" SELECTED>--Select One--</option>
						};	               	
						foreach $key(sort {$roleDetails{$a} cmp $roleDetails{$b} } keys %roleDetails){            	
							    print "<OPTION VALUE=\"$key\">$roleDetails{$key}</OPTION> \n";            	
						}
						print qq{					  
					  </select> <font color="#ff0000">*</font> </td>
				  </tr>
				  <tr>
					<td colspan="2">&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td colspan="4" class="ColrBk03"><b><font color="#0066cc">&nbsp;<strong>Contact Details</strong>...</font></b></td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> First Name</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><select name="txtNameTitle" class="TextBox01" id="txtNameTitle">
						<option value="n" selected="selected">Select Title...</option>
						<option value="Mr.">Mr.</option>
						<option value="Mrs.">Mrs.</option>
						<option value="Ms.">Ms.</option>
						<option value="Dr.">Dr.</option>
						<option value="Prof.">Prof.</option>
						<option value="Rev.">Rev.</option>
						<option value="">Other</option>
					  </select> <input name="txtFirstName" class="TextBox01" id="txtFirstName" size="30" maxlength="100" type="text">
					  <font color="#ff0000">*</font>
					  <select name="txtNameSuffix" class="TextBox01" id="txtNameSuffix">
						<option value="n" selected="selected">Select Suffix...</option>
						<option value="CEO">CEO</option>
						<option value="Director">Director</option>
					  </select>
					  
					  </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Middle Name</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtMiddleName" class="TextBox01" id="txtMiddleName" size="30" maxlength="100" type="text"> <font color="#ff0000"></font> </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Last Name</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtLastName" class="TextBox01" id="txtLastName" size="30" maxlength="100" type="text"> <font color="#ff0000">*</font> </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Company Name</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td>
						};	               	
						
						print qq{

					  <input name="txtCompany" class="TextBox01" id="txtCompany" size="50" maxlength="100" type="text">
					<font color="#ff0000"></font>					  
					  </td>
				  </tr>
				  
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Address</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtAddress1" class="TextBox01" id="txtAddress1" size="60" maxlength="100" type="text"> <font color="#ff0000"></font> </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtAddress2" class="TextBox01" id="txtAddress2" size="60" maxlength="100" type="text"> <font color="#ff0000"></font> </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtAddress3" class="TextBox01" id="txtAddress3" size="60" maxlength="100" type="text"> <font color="#ff0000"></font> </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> City</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtCity" class="TextBox01" id="txtCity" size="30" maxlength="100" type="text"> <font color="#ff0000"></font> </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> State</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><select name="txtState" class="TextBox01" id="txtState">
						<option value="" SELECTED>--Select--</option>
						};	               	
						foreach $key(sort {$statesDetails{$a} cmp $statesDetails{$b} } keys %statesDetails){            	
							    print "<OPTION VALUE=\"$key\">$statesDetails{$key}</OPTION> \n";            	
						}
          					print qq{

					  </select> <font color="#ff0000"></font>					  
					  </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Zip Code</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtZip" class="TextBox01" id="txtZip" size="15" maxlength="10" type="text"> <font color="#ff0000"></font> </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Telephone1</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtTelephone1" class="TextBox01" id="txtTelephone1" size="50" maxlength="10" type="text"> <font color="#ff0000"> (Land Phone, Mobile, Office)</font></td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Telephone2</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtTelephone2" class="TextBox01" id="txtTelephone2" size="50" maxlength="10" type="text"> <font color="#ff0000"> (Land Phone, Mobile, Office)</font></td>
				  </tr>
				  <!-- <tr>
					<td>&nbsp;</td>
					<td><font color="#333399"><b><img src="/images/register/arrow_02.gif" width="4" height="7"> Email ID</b></font></td>
					<td><font color="#333399"><b>:</b></font></td>
					<td><input name="txtEmail" class="TextBox01" id="txtEmail" size="50" maxlength="100" type="text"> <font color="#ff0000"></font></td>
				  </tr> -->
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td colspan="4"><table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tbody>
						<tr>
						  <td width="25"><img src="/images/register/pix_trans.gif" width="120" height="5"></td>
						  <td width="25"></td>
						  <td></td>
						</tr>
					  </tbody></table></td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td><input type="hidden" name="saction" value="Register"><input name="cmdNext" class="TextBox01" id="cmdNext" value="Submit" type="submit"> <input name="cmdReset" class="TextBox01" id="cmdReset" value="Clear" type="reset"></td>
				  </tr>
				</tbody></table>
			  </form>
	             </td>
		  </tr>
		</tbody>
	      </table>
	      </div>
          </td>
       </tr>
  </tbody>
 </table>
 
 </td>
  </tr>
  </table>
  <!--content end-->
  };
  &TDdoc;
  print qq{
  </CENTER>
  </body>  
  </html>
};

#--------------------------------------------------------------------------------------
sub sessionExpire
{

print "Content-Type: text/html\n\n";

print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};
&headerScript();
print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#--------------------------------------------------------------------------------
# - Get details from States table. 

sub getStatesDetails
{
	my ($qry, $stmt, $record);
	
	$qry = "select * from states order by state_name";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();   	
   	
   	my %state;   	
   	
	while ($record = $stmt->fetchrow_hashref) {	
		$state{$$record{state}} = $$record{state_name};		
  	}  	
  	return %state;
}

#--------------------------------------------------------------------------------
# - Get details from password_security_qry table. 

sub getPasswordSecurityQry
{
	my ($qry, $stmt, $record);
	
	$qry = "select * from password_security_qry ";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();   	
   	
   	my %state;   	
   	
	while ($record = $stmt->fetchrow_hashref) {	
		$state{$$record{Qry_ID}} = $$record{Query};		
  	}  	
  	return %state;
}

#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------
# - Get details from Role table. 

sub getRoleDetails
{
	my ($qry, $stmt, $record);
	
	$qry = "select * from role ";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();   	
   	
   	my %role;   	
   	
	while ($record = $stmt->fetchrow_hashref) {	
		$role{$$record{RoleID}} = $$record{RoleName};		
  	}  	
  	return %role;
}


#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach my $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}

#------------------------------------------------------------------------------