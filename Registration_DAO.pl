#!/usr/bin/perl

use CGI;
use DBI;

use CGI::Session;


my $cgi = new CGI;
my $userName = $cgi->param('txtUserName');
my $password = $cgi->param('txtPassword');
my $securityQry = $cgi->param('txtSecurityQry');
my $securityOtherQry = $cgi->param('txtSecurityOtherQry');
my $securityAns = $cgi->param('txtSecurityAns');
my $txtUserType = $cgi->param('txtUserType');
my $saction = $cgi->param('saction');

my $firstName = $cgi->param('txtFirstName');
my $middleName = $cgi->param('txtMiddleName');
my $lastName = $cgi->param('txtLastName');
my $emailID = $cgi->param('txtEmail');
my $Address1 = $cgi->param('txtAddress1');
my $Address2 = $cgi->param('txtAddress2');
my $Address3 = $cgi->param('txtAddress3');
my $city = $cgi->param('txtCity');
my $state = $cgi->param('txtState');
my $zip = $cgi->param('txtZip');
my $title = $cgi->param('txtNameTitle');
my $phone1 = $cgi->param('txtTelephone1');
my $phone2 = $cgi->param('txtTelephone2');

my $nameSuffix = $cgi->param('txtNameSuffix');
my $facility = $cgi->param('txtFacility');
my $facilityOther = $cgi->param('txtOtherFacility');

my $str = NULL;

if(!$zip)
{
	$zip = NULL;
}

# Database 

my $dbh;
my ($sql, $sth);

# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my $defaultYear= $ini_value{'DEFAULT_YEAR'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

my ($userID);

if ($saction eq 'Register')
{
	# my ($roleID); -- not required

	$sql = "select UserID from Users where UserName = '$userName' ";
	$sth = $dbh->prepare($sql);			
	$sth->execute();
	if($sth->rows()>0)
	{	
		$str ="<script>alert('User already exists');window.parent.location.href='/index.html';</script>";
	}
	else
	{
		# Role ID will be assigned from combobox in creat page
		
		#$sql = "select RoleID from role where RoleName = 'User'";
		#$sth = $dbh->prepare($sql);			
		#$sth->execute();
		#@row = $sth->fetchrow_array();
		#$roleID = @row[0];


		$sql = "Insert into Users (UserName, Password, IsActive, CreatedDate, RoleID, Security_Qry_ID, Security_Other_Qry, Security_Ans) values ('$userName',AES_ENCRYPT('$password','bs'), 1, CURRENT_DATE, $txtUserType, $securityQry, '$securityOtherQry', '$securityAns')";
		$sth = $dbh->prepare($sql);			
		$sth->execute();


		$sql = "select UserID from Users where UserName = '$userName' and Password = AES_ENCRYPT('$password','bs') and CreatedDate = CURRENT_DATE ";
		$sth = $dbh->prepare($sql);			
		$sth->execute();
		@row = $sth->fetchrow_array();
		$userID = @row[0];


		$sql = "Insert into userdetails (UsersID, First_Name, Middle_Name, Last_Name, emailID, Address1, Address2, Address3, city, State, zip, Title, Phone1, Phone2, CreatedDate, Suffix_Title, Facility_ID, Facility_Other) values ($userID, '$firstName', '$middleName', '$lastName', '$emailID', '$Address1', '$Address2', '$Address3', '$city', '$state', $zip, '$title', '$phone1', '$phone2', CURRENT_DATE, '$nameSuffix', '$facility', '$facilityOther')";
		$sth = $dbh->prepare($sql);			
		$sth->execute();

		$str ="<script>alert('User created successfully');window.parent.location.href='/cgi-bin/registration.pl';</script>";
	}	
	
	
}


my $session = new CGI::Session("driver:File", undef, {Directory=>"/tmp"});

my $sid = $session->id();

my $cookie = $cgi->cookie(CGISESSID => $session->id);

#$session->param("firstName", $firstName);
#$session->param("lastName", $lastName);
#$session->param("userID", $userID);
#$session->expire('+1h');

print $cgi->header(-cookie=>$cookie);

print <<EOF
<HTML>
<HEAD><TITLE>Create User</TITLE>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon">
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
<link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />
</HEAD>
<BODY>
<FORM name="frmPeerActions">
<input type="hidden" name="duplicate" value="">
EOF
;    
print "$str";
print qq{
</FORM>
</BODY>
</HTML>
};


#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}


