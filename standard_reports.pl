#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: cr_batchx.pl
#  author: Steve Spicer
#  Written: Wed Oct 24 20:14:32 CDT 2007
#  Revised: Sun Jun 14 22:31:17 CDT 2009
#----------------------------------------------------------------------------

use strict;
use Spreadsheet::WriteExcel;
use Date::Calc qw(Delta_Days);
use CGI;
use DBI;
do "common_header.pl";
do "pagefooter.pl";
use CGI::Session;
do "audit.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID"); 
my $userType = $session->param("role");
my $facility = $cgi->param("facility");
my $stdRptSessionElements = $session->param('stdRptElements');
my $stdRptSessionProviders = $session->param('stdRptProviders');
my $stdRptSessionDataset = $session->param('stdRptDataset');
my $stdRptSessionFy = $session->param('stdRptFy');
my $stdRptSessionFoi = $session->param('stdRptFoi');
my $stdRptSessionDirection = $session->param('stdRptDirection');
my $stdRepDispOrder = $session->param('stdRepDispOrder');

if(!$userID)
{
	&sessionExpire;
}

my $start_time = time;
my $action    = $cgi->param('action');    
my $text      = $cgi->param('providers'); # aaa.txt bbb.txt
my $show_keys = $cgi->param('showkeys');
my $dir       = $cgi->param('direction');
my $year      = $cgi->param('year');
my $imp       = $cgi->param('imp');       # import peer list  Y/N or nothing;
my @datasets  = $cgi->param('datasets');
;

my $peerID    = $cgi->param('peerID');

my $checkedYear    = $cgi->param('yearCheck');
my $callBackStdReport = $cgi->param('callBack');

# String holding provider IDs separated by ';'
my $providerIds = $cgi->param('Ids');
my $test = '';
my @arrayOfIds = split(/:/,$providerIds);

my $remote_user  = $ENV{'REMOTE_USER'};

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};

my ($dbh, $sql, $sth);
my ($dbh, $sql,$row,$facility_id, $sth,$report_table,$data_table);
my ($sql1,$sth1,$sql2,$sth2);

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error('ERROR connecting to database', $DBI::errstr);
   exit(1);
}
	
$sql = "select facility_id,report_table,data_table from tbl_facility_type where facility_name = '$facility'";
$sth = $dbh->prepare($sql);
$sth->execute();

	  while ($row = $sth->fetchrow_hashref) {
	 $report_table = $$row{report_table}; 
	$facility_id = $$row{facility_id};
	$data_table = $$row{data_table};
}

 
unless ($action eq 'go') {
   &display_form();
   exit(0);
}
my ($sth, $sql, $row);


$year = $ini_value{'DEFAULT_YEAR'} unless ($year =~ m/^\d\d\d\d$/);  # avoid any sql injection possibilities

my ($line, @list, $s, $len, $string, $provider, $n, $width, $value, $row, $column, $desc);


my ($key, $yyyy);

my @report_id_list = ();

#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
<link href="/css/standard_report/standard_report.css" rel="stylesheet" type="text/css" />
</HEAD>
<BODY>
<CENTER>
<BR> 
<BR>
};

&innerHeader();

print qq{
    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}

#--------------------------------------------------------------------------------------------------------------

sub display_form
{

   my ($year, $year_options, $selected);

   opendir(DIR,"./packages");
   my @list = readdir(DIR);
   closedir(DIR);

   my ($fname, $editlink, $string); 

   my $line;

   my $default_list = '';

   if ($imp =~ m/^Y/i) {
      if (open INPUT, "userdata/$remote_user.txt") {
         while ($line = <INPUT>) {
            $default_list .= $line;
         }
      }
      close INPUT;
   }
   
   #jayanth - to select the saved peer groups. 
   
   # For admin show all peer groups.
   if($userType == 2){
   	$sql = "select * from peergroup_main where facility_type = $facility_id order by name";
   }# For internal user show peer groups of all internal users.
   elsif($userType == 1){
   	$sql = "select peergroup_main.PeerGroup_ID,peergroup_main.Name from peergroup_main,users,role where peergroup_main.CreatedBy = users.UserID and users.RoleID = role.RoleID and users.roleid = 1  and peergroup_main.facility_type = $facility_id order by peergroup_main.name";
   }# For external user show peer groups created by user who logs in.
   else{   	 
   	$sql = "select peergroup_main.PeerGroup_ID,peergroup_main.Name from peergroup_main,users,role where peergroup_main.CreatedBy = users.UserID and users.RoleID = role.RoleID and peergroup_main.createdBy = $userID  and peergroup_main.facility_type = $facility_id order by peergroup_main.name";
	
   }	
   $sth = $dbh->prepare($sql);
   $sth->execute();   
   
 print "Content-Type: text/html\n";
   print "Cache-Control: no-cache\n";
   print "Pragma: no-cache\n\n";

   print qq{
<HTML>
<HEAD><TITLE>STANDARD REPORTS</TITLE>
    <link rel="stylesheet" type="text/css" media="all" href="/css/tree.css" />
    <script type="text/javascript" src="/JS/tree.js"></script>
    <script type="text/javascript" src="/JS/AjaxScripts/prototype.js"></script>
    <script type="text/javascript" src="/JS/AjaxScripts/effects.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/controls.js"></script>
  <script type="text/javascript" src="/JS/standard_report/standard_reports.js"></script>
    </HEAD>
<BODY>
<BR>
<FORM NAME="frmStdReports" METHOD="POST" ACTION="/cgi-bin/std_reports_display.pl?facility=$facility">
<div align="center" id='bodyDiv' style="position:absolute;z-index:100;display:none;background-image:url(/images/background-trans.png);height:96%;width:96%;"><div align='center'  id="displaybox" style="display: none;"></div></div>
<CENTER>
<input type="hidden" name="facility" value="$facility">
};
&innerHeader();
print qq{

    <!--content start-->
   
    <table cellpadding="0" cellspacing="0" width="885px">
        <tr>
            <td valign="top">
                <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;
                text-align: left; padding: 10px">
<div ></div>
<table width="100%" align="center"> };
	#Getting data from std report display page and populating again

	if($callBackStdReport eq 'true')
	{
		if($stdRptSessionFoi eq 'true')
		{
			print qq{<tr><td class="pageheader" >STANDARD REPORTS</td><td valign='top'><a href='/cgi-bin/standard_reports_wizard.pl?callBack=$callBackStdReport&peerID=$peerID&facility=$facility'><img border=0 src='/images/customize.png'  alt='Step by step wizard' title='Step by step wizard'/></a></td><td valign="top"><input type="radio" name="foistatistics" value="true" checked></td><td valign="top" style="color:#006666;">Include facility of Interest statistics</td><td valign="top"><input type="radio" name="foistatistics" value="false"></td><td valign="top" style="color:#006666;">Exclude facility of Interest statistics</td></tr>};
		}
		else
		{
			print qq{<tr><td class="pageheader" >STANDARD REPORTS</td><td valign='top'><a href='/cgi-bin/standard_reports_wizard.pl?callBack=$callBackStdReport&peerID=$peerID&facility=$facility'><img border=0 src='/images/customize.png'  alt='Step by step wizard' title='Step by step wizard'/></a></td><td valign="top"><input type="radio" name="foistatistics" value="true" ></td><td valign="top" style="color:#006666;">Include facility of Interest statistics</td><td valign="top"><input type="radio" name="foistatistics" value="false" checked></td><td valign="top" style="color:#006666;">Exclude facility of Interest statistics</td></tr>};
		}
	}
	else
	{
		print qq{<tr><td class="pageheader" >STANDARD REPORTS</td><td valign='top'><a href='/cgi-bin/standard_reports_wizard.pl?callBack=$callBackStdReport&peerID=$peerID&facility=$facility'><img border=0 src='/images/customize.png'  alt='Step by step wizard' title='Step by step wizard'/></a></td><td valign="top"><input type="radio" name="foistatistics" value="true" checked></td><td valign="top" style="color:#006666;">Include facility of Interest statistics</td><td valign="top"><input type="radio" name="foistatistics" value="false"></td><td valign="top" style="color:#006666;">Exclude facility of Interest statistics</td></tr>};
	}

	print qq{
</table>
<TABLE bordr=1 ALIGN="CENTER" CELLPADDING="0" CELLSPACING="0" WIDTH="100%" class="gridstyle" style="background-color:white">
<Tr>
	<Td valign="top" style="padding:10px" width="40%">
	<span>
	  <table class="gridstyle" style="color:#0c4f7a;font-weight:bold;font-size:12;" width="100%">	     
	     <tr>
		<td>
		    <DIV style="position:static;width:100%;height:330px;overflow:scroll;">	
};		    
		    my ($sql1, $sth1);
		    my ($sql2, $sth2);
		    		    
		    $sql1 = "select * from standard_reports where facility_type = $facility_id order by reports_name";
		    $sth1 = $dbh->prepare($sql1);
		    $sth1->execute();
		  		    
		    my $str = "<ul class=\"tree\" >";		    
		    my $i=0;
			
		    while (my $dataset = $sth1->fetchrow_hashref) {
		     	
		     	$str = $str."<li><input  type=\"checkbox\" name=\"chk_dataset$i\" id=\"chk_dataset$i\" onclick=\"dataSetSelect(this,$i)\" value=\"$$dataset{reports_id}\" "; 
		     	
		     	#Getting data from std report display page and populating again
		     	if($callBackStdReport eq 'true')
		     	{
		     		my $ifCheckDataset = 'false';
		     		foreach $s (split /,/,$stdRptSessionDataset)
		     		{
		     			if($$dataset{reports_id} eq $s)
		     			{
		     				$ifCheckDataset = 'true';
		     			}
		     		}
		     		
		     		if($ifCheckDataset eq 'true')
		     		{
		     			$str = $str." checked=\"yes\" /><a href=\"#\" >".$$dataset{reports_name}."</a>";
		     		}
		     		else
		     		{
		     			$str = $str."/><a href=\"#\" >".$$dataset{reports_name}."</a>";
		     		}
		     	}
		     	else
		     	{
		     		$str = $str."/><a href=\"#\" >".$$dataset{reports_name}."</a>";
		     	}
		     	
		     	$sql2 = "select rm.metrics_id,element,isFormulae,Formulae,rm.Summary_Info from standard_metrics sm inner join standard_reports_metrics rm on rm.metrics_id = sm.metrics_id Where reports_id = $$dataset{reports_id} ";
		     	$sth2 = $dbh->prepare($sql2);
		     	$sth2->execute();
				     	
		     	$str = $str."<ul>";
		     	my $cnt = 0;
		     	while(my $dataElement = $sth2->fetchrow_hashref){
		     		
		     		$str = $str."<li><input type=\"checkbox\" name=\"chk_dataset".$i."_chk_dataelement$i$cnt\" onclick=\"dataDeselect(this,$i)\" id=\"chk_dataset".$i."_chk_dataelement$i$cnt\" value=\"$$dataElement{metrics_id}\" ";
		     		
		     		#Getting data from std report display page and populating again
		     		if($callBackStdReport eq 'true')
		     		{
		     			my $ifCheckElement = 'false';
		     			foreach $s (split /,/,$stdRptSessionElements)
		     			{
		    			
		    				if($$dataElement{metrics_id} eq $s)
		    				{
		    					$ifCheckElement = 'true';
		    				}
		     			}
		     			if($ifCheckElement eq 'true')
		     			{
		     				$str = $str."checked=\"yes\" /><a href=\"#\">".$$dataElement{element}."</a></li>";
		     			}
		     			else
		     			{
		     				$str = $str." /><a href=\"#\">".$$dataElement{element}."</a></li>";
		     			}
		     		}
		     		else
		     		{
		     			$str = $str." /><a href=\"#\">".$$dataElement{element}."</a></li>";
		     		}
		     		
		     		$cnt++;
		     	} 	
		     	$str = $str."</ul>";
		     	$str = $str."<span><input type=\"hidden\" style=\"width:0px;height:0px;\" name=\"len\" value=\"$cnt\" /><input type=\"checkbox\" style=\"width:0px;height:0px;visibility:hidden;\" name=\"dsetlen\" /></span></li>";
		     	$i++;
		    }
		    	$str = $str."</ul>";
		    
		   # $sth1->finish();		    
		   # $sth2->finish();
		    
		    print "$str";
		    print "</DIV>";
		    
print qq{
</td>
</tr>	    
</table>
</span>
</td>
<td valign="top" style="padding:10px">
		<table class="gridstyle" width="100%">
			<tr>
			  <td colspan=2 >
				 <B>PEER GROUP SELECTION</B><BR><BR>		        	 
        			<select class="textboxstyle" style="width:100%;color:#0c4f7a;font-weight:bold;font-size:12;" name=peerGroupSelect onChange="selectPeerGroup(this)">
};				
	
					print "<option value=0 class='gridHlink'>--Select Peer Group--</option>\n";
					while ($row = $sth->fetchrow_hashref) {	
						if($peerID == $$row{PeerGroup_ID}){
							print "<option class='gridHlink' value=$$row{PeerGroup_ID} selected>$$row{Name}</option>\n"; 	
						}else{
							print "<option class='gridHlink' value=$$row{PeerGroup_ID}>$$row{Name}</option>\n"; 
						}
  					}
  
  				print qq{
  				</select>
		 		<br><a href=\"#\" onclick=\"CallCreate()\">Select Provider</a><br>
	    	 	 </td>
			</tr>
			<tr>
			  <td>        			
         			<TEXTAREA NAME="providers" class="textboxstyle" COLS="43" ROWS="30" style="color:#0c4f7a;font-weight:bold;font-size:12;width:100%; height:265px; font-family:Verdana; font-size:10px;" readOnly>};
					foreach my $hospitalID (@arrayOfIds){print "$hospitalID\n";}					
					#Getting data from std report display page and populating again
					if($callBackStdReport eq 'true')
					{
						print "$stdRptSessionProviders";
					}
					print qq{</TEXTAREA>         
        		  </td>
        		  <td width="20%">
        		  	<div style='border:1px solid gray;width:100%; height:262px;position:static;overflow:scroll;'>
        	      		  <table width="100%" cellpadding="0" celspacing="0">        	 		        
        	 		        };        	 		        
        	 		        foreach my $fYears (@fYears)
					{
						#Getting data from std report display page and populating again
						if($callBackStdReport eq 'true')
						{
							my $ifCheckElement = 'false';
							foreach $s (split /,/,$stdRptSessionFy)
							{								
								if($fYears eq $s)
								{
									$ifCheckElement = 'true';									
								}								
								
							}
							if($ifCheckElement eq 'true')
							{								
								print "<tr><td width=\"10px\"><INPUT TYPE='CHECKBOX' NAME='CHK1' value='$fYears' checked></td><td style='color:#0c4f7a;font-weight:bold;font-size:12;'>$fYears</td></tr>";
							}
							else
							{
								print "<tr><td width=\"10px\"><INPUT TYPE='CHECKBOX' NAME='CHK1' value='$fYears'></td><td style='color:#0c4f7a;font-weight:bold;font-size:12;'>$fYears</td></tr>";
							}
						}
						else
						{
							if($fYears==$defaultYear)
							{
								print "<tr><td width=\"10px\"><INPUT TYPE='CHECKBOX' NAME='CHK1' value='$fYears' checked></td><td style='color:#0c4f7a;font-weight:bold;font-size:12;'>$fYears</td></tr>";					
							}
							else
							{
								print "<tr><td width=\"10px\"><INPUT TYPE='CHECKBOX' NAME='CHK1' value='$fYears'></td><td style='color:#0c4f7a;font-weight:bold;font-size:12;'>$fYears</td></tr>";					
							}
						}	
							}           				
							print qq{
						  </table>
						   </div>	 
		         </td>
	              </tr>
	       </table>
         </td>
</tr>   
<tr>
	<td colspan=4>
		<table width="100%">
			<tr>
			<td colspan=2>
				<INPUT TYPE="hidden" NAME="action" VALUE="go">
				<INPUT TYPE="hidden" NAME="yearCheck" >	
				
				<INPUT TYPE="hidden" NAME="hdnRptDirection" >
			 	<table width="100%">
				     <tr>
					<td ><span class="button"><INPUT TYPE="button" value="BACK" onClick="history.go(-1)" /></span></td>
					<td nowrap><span class="button"><INPUT TYPE="button" VALUE="GENERATE REPORT" onClick="callsubmit()"></span></td>};
					
					#Getting data from std report display page and populating again
					if($callBackStdReport eq 'true')
					{
						if($stdRptSessionDirection eq 'across')
						{
							print qq{<td>Report Direction : <INPUT TYPE="radio" name="rptDirection" VALUE="across" checked> ACROSS <INPUT TYPE="radio" name="rptDirection" VALUE="down" > DOWN</td>};
						}
						else
						{
							print qq{<td>Report Direction : <INPUT TYPE="radio" name="rptDirection" VALUE="across"> ACROSS <INPUT TYPE="radio" name="rptDirection" VALUE="down" checked> DOWN</td>};
						}
					}
					else
					{
						print qq{<td>Report Direction : <INPUT TYPE="radio" name="rptDirection" VALUE="across"> ACROSS <INPUT TYPE="radio" name="rptDirection" VALUE="down" checked> DOWN</td>};
					}					
					
				      print qq{ </tr>
				</table>	
			</td>
			<td colspan=2>
				<INPUT TYPE="hidden" NAME="datasetIds" >  
				<INPUT TYPE="hidden" NAME="dataElementIds" >
				
				 <table width="100%">
				       <tr>
				         <td style="padding-rigth:50px;"><span  class="button"><INPUT TYPE="button" onclick="location.href='/cgi-bin/standard_reports.pl?facility=$facility'" VALUE="RESET ALL" ></span></td>								
					<td style="width:40px;"><span  class="button"><INPUT TYPE="button" VALUE="CLEAR PROVIDERS"  onclick="window.document.frmStdReports.providers.value=''"></span></td>
					</tr>
				  </table>
			</td>
			</tr>
		</table>		
	</td>	
</tr>	
</TABLE>
</td>
<td valign="top">
                <img src="/images/content_right.gif" />
</td>
</tr>

    </table>
    <!--content end-->
	};
	
&TDdoc;	
	print qq{
</CENTER>
</FORM>
<script>
 	selectPeerGroup(document.frmStdReports.peerGroupSelect);
</script>
</BODY>
</HTML>
   };
}

#-----------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#--------------------------------------------------------------------------------------
sub sessionExpire
{
print "Content-Type: text/html\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};
&headerScript();
print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

#------------------------------------------------------------------------------


#Database: HCRISDB  Table: HS2006_RPT  Wildcard: %
#+--------------------+---------------+-------------------+------+-----+---------+-------+---------------------------------+---------+
#| Field              | Type          | Collation         | Null | Key | Default | Extra | Privileges                      | Comment |
#+--------------------+---------------+-------------------+------+-----+---------+-------+---------------------------------+---------+
#| RPT_REC_NUM        | int(11)       |                   | NO   |     |         |       | select,insert,update,references |         |
#| PRVDR_CTRL_TYPE_CD | char(2)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| PRVDR_NUM          | char(6)       | latin1_swedish_ci | NO   | MUL |         |       | select,insert,update,references |         |
#| NPI                | decimal(10,0) |                   | YES  |     |         |       | select,insert,update,references |         |
#| RPT_STUS_CD        | char(1)       | latin1_swedish_ci | NO   |     |         |       | select,insert,update,references |         |
#| FY_BGN_DT          | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| FY_END_DT          | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| PROC_DT            | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| INITL_RPT_SW       | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| LAST_RPT_SW        | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| TRNSMTL_NUM        | char(2)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| FI_NUM             | char(5)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| ADR_VNDR_CD        | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| FI_CREAT_DT        | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| UTIL_CD            | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| NPR_DT             | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| SPEC_IND           | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| FI_RCPT_DT         | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#+--------------------+---------------+-------------------+------+-----+---------+-------+---------------------------------+---------+
