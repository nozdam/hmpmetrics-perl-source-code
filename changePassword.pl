#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: changePassword.pl
#  author: Jagadish.M
#----------------------------------------------------------------------------

use strict;
use CGI;
use DBI;
use CGI::Session;

do "common_header.pl";
print "Content-Type: text/html\n\n";
my $cgi = new CGI;
my $dbh;
my ($sql, $sth, $key, @row);

my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");

# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my $defaultYear= $ini_value{'DEFAULT_YEAR'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

my $userID = $session->param('userID');
my $password = $cgi->param('txtpassword');
my $cfrmpassword = $cgi->param('txtconfirmpassword');
my $str ="";

if($password){
	if($password eq $cfrmpassword)
	{
		$sql = "update users set Password = AES_ENCRYPT('$password','bs') where UserID = $userID";
		$sth = $dbh->prepare($sql);			
		$sth->execute();
		
		$str = "Password Change successfully.";
	}
	else{
		$str = "Password ,Confirm password mismatch.";
	}
}	
	


print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<link href="/css/changePassword.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Milo - Change Password</title>
<link href="/css/register/main.css" rel="stylesheet" type="text/css">
<link href="/css/register/qm.css" rel="stylesheet" type="text/css">


</head>
<body><!--<a style="display:scroll;position:fixed;left:5px;bottom:5px;" href="#" title="Feedback Please"><img border=0 src="/images/feedback.png"/></a>-->
<CENTER>
<form action="/cgi-bin/changePassword.pl" method="post" name="frmChangePassword" id="frmLogin" >
};
&innerHeader();
print qq{
    <!--content start-->    
       
        <table cellpadding="0" cellspacing="0" width="885px">
            <tr>
                <td valign="top">
                    <img src="/images/content_left.gif" /></td>
                <td valign="top" style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;
                text-align: left; padding: 10px">

<table cellpadding="2px" cellspacing="1px" bgcolor="#F4F5F7" width="50%" class="tableBorder" align="center">
    <tr>
        <td colspan="2" class="Fnt11Pix" background="/images/register/grad_01_hover.gif"><font color="#FFFFFF"><b>Change Password</b></font>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>     
    <tr>
        <td class="label" align="right" width="40%">New Password :</td>
        <td align="left" width="60%"><input type="password" name="txtpassword" maxlength="100"/></td>
    </tr>
    <tr>
        <td class="label" align="right">Confirm Password :</td>
        <td align="left"><input type="password" name="txtconfirmpassword" maxlength="100" /></td>
    </tr>     
    <tr>
        <td class="label" align="right">&nbsp;</td>
        <td align="left"><span class="button"><input type="submit" value="Update" /></span>&nbsp;<span class="button"><input type="reset" value="Clear" /></span></td>
    </tr>   
    <tr>
            <td class="label" align="right">&nbsp;</td>
            <td style="font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px;" align="left"><!--New to Milo? <a href="/cgi-bin/registration.pl">Create an account</a>--></td>
    </tr>   
    <tr>
         <td colspan="2" class="label">&nbsp;</td>
    </tr>
    <tr align="center">
                <td class="message" colspan=2> $str</td>            
    </tr>   
</table>
</td>
            <td valign="top">
                <img src="/images/content_right.gif" /></td>
        </tr>
    </table>
    <!--content end-->
</form>
</CENTER>
</body>
</html>
};


#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach my $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}

#------------------------------------------------------------------------------