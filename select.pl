#!/usr/bin/perl 

#----------------------------------------------------------------------------
# Program: select.pl
#  Author: Steve Spicer
# Request: 
# Written: 
# Revised: 
#----------------------------------------------------------------------------

use strict;
use CGI;

my ($line, $key, $worksheet,$line,$column,$fmt,$desc);

my %shortname;

open INPUT, 'all.txt';

while ($line = <INPUT>) {
   ($worksheet,$line,$column,$fmt,$desc) = split /\|/,$line;
   $key = "$worksheet:$line:$column:$fmt";
   $shortname{$key} = "$desc";
}
close INPUT;

my ($options_left, $options_right, $value);

foreach $key (sort keys %shortname) {
   $value = $shortname{$key};
   $options_right .= "     <OPTION VALUE=\"$key\">$value</OPTION>\n";
}

$options_left = "     <OPTION VALUE=\"\">&nbsp;</OPTION>\n";

my $style = qq(
<STYLE type=text/css>

  select { 
     font-family: Verdana, Arial, Helvetica, sans-serif;
     font-size: 12px;
     border: #000000;
     border-style: solid;
     border-top-width: 1px;
     border-right-width: 1px;
     border-bottom-width: 1px;
     border-left-width: 1px;
  }

  select option {
     width: 400px;
     background-color: #CCCCFF;
  }

  table {
     font-family: Verdana, Arial, Helvetica, sans-serif;
     font-size: 12px;
     font-style: normal;
     font-weight: normal;
     border-collapse: collapse;
  }
-->
</STYLE>
);


print "Content-Type: text/html\n\n";

print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
<TITLE>EXAMPLE</TITLE>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon" />
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css" />
<SCRIPT TYPE="text/JavaScript" SRC="/selectbox.js"></SCRIPT>
$style
</HEAD>
<BODY>
<BR>
<BR>
<BR>
<BR>
<CENTER>
<FORM METHOD="GET" ACTION="#">
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0">
<TR>
   <TD COLSPAN="4" ALIGN="CENTER">
     <FONT SIZE="6">DESIGN YOUR SPREADSHEET</FONT>
     <BR>
     <BR>
   </TD>
</TR
<TR>
   <TD ALIGN="CENTER" VALIGN="MIDDLE" WIDTH="75">
	<INPUT TYPE="button" VALUE="     UP      " onClick="moveOptionUp(this.form['left'])"> <BR><BR>
	<INPUT TYPE="button" VALUE=" DOWN " onClick="moveOptionDown(this.form['left'])">
   </TD>
   <TD  ALIGN="CENTER" WIDTH="450" BGCOLOR="#ffffcc">
   <B>COLUMNS TO USE IN SPREADSHEET</B><BR>
   <SELECT NAME="left" CLASS="box" MULTIPLE SIZE="30" onDblClick="moveSelectedOptions(this.form['left'],this.form['right'],false)">
      $options_left
   </SELECT>
   </TD>
   <TD VALIGN="MIDDLE" ALIGN="CENTER" WIDTH="100">
      <INPUT TYPE="button" VALUE="  &gt;&gt;  " ONCLICK="moveSelectedOptions(this.form['left'],this.form['right'],false)"><BR><BR>
      <INPUT TYPE="button" VALUE="All &gt;&gt;" ONCLICK="moveAllOptions(this.form['left'],this.form['right'],false)"><BR><BR>
      <INPUT TYPE="button" VALUE="  &lt;&lt;  " ONCLICK="moveSelectedOptions(this.form['right'],this.form['left'],false)"><BR><BR>
      <INPUT TYPE="button" VALUE="All &lt;&lt;" ONCLICK="moveAllOptions(this.form['right'],this.form['left'],false)"><BR>
   </TD>
   <TD  ALIGN="CENTER" WIDTH="450" BGCOLOR="#ffccff">
   <B>COLUMNS AVAILABLE</B><BR>
   <SELECT NAME="right" CLASS="box" MULTIPLE SIZE="30" onDblClick="moveSelectedOptions(this.form['right'],this.form['left'],false)">
      $options_right
   </SELECT>
   </TD>
</TR>
</TABLE>
</CENTER>
</FORM>
</BODY>
</HTML>
};
