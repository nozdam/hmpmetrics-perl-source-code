#!/usr/bin/perl

#--------------------------------------------------------------------
# Program: choose1.pl
#  Author: jagadish.m
# Written: Sun Apr  5 21:13:52 CDT 2009
# Revised: Sun Jun 14 18:21:56 CDT 2009
# Revised: Mon Jun 15 22:28:38 CDT 2009
#   Notes:
# Depends: DBI.pm, CGI.pm
#--------------------------------------------------------------------

use strict;
use DBI;
use CGI;
do "pagefooter.pl";
use CGI::Session;
do "audit.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");

# if(!$userID)
# {
	# &sessionExpire;
# }


my $provider  = $cgi->param('provider');

my $facility   = $cgi->param('facility');   # HOS, SNF ect  for later


&view("View","","Cost Report");
unless ($provider) {
   &display_error('NO PROVIDER NUMBER SUPPLIED');
   exit(1);
}

my %status_lookup = (
 1 => 'As Submitted',
 2 => 'Settled w/o Audit',
 3 => 'Settled with Audit',
 4 => 'Reopened',
 5 => 'Amended',
);

my $start_time = time;

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error('CAN\'T READ milo.ini');
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my ($sql, $dbh, $sth,$hsql,$sth1);

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", DBI::errstr);
   exit(1);
}


#----------------------------------------------------------------------------
my ($name, $address, $city, $state, $zip, $county);
if($facility eq 'HOSPITAL'){
$sql = qq{
   SELECT h.HospitalName as HOSP_NAME,h.HospitalStreet as STREET_ADDR,
h.HospitalCity as CITY,h.HospitalState as STATE,h.HospitalZipCode as ZIP_CODE
,h.HospitalCounty as COUNTY 
 FROM tblhospitalmaster  as h
 WHERE ProviderNo = $provider
};

}

if($facility eq 'HOSPICE'){
$hsql = qq{
    SELECT hp.Name as HOSP_NAME,hp.Street as STREET_ADDR,
hp.City as CITY,hp.State as STATE,hp.Zip as ZIP_CODE
,hp.County as COUNTY 
 FROM tblhospiceproviders  as hp
 WHERE ProviderNo = $provider
};

}
if($facility eq 'RENAL'){
$sql = qq{
    SELECT rn.Name as RNL_NAME,rn.Street as STREET_ADDR,
rn.City as CITY,rn.State as STATE,rn.Zip as ZIP_CODE
,rn.County as COUNTY 
 FROM tblrenalproviders  as rn
 WHERE ProviderNo = $provider
};

}
if($facility eq 'SNF'){
$sql = qq{
    SELECT sn.Name as SNF_NAME,sn.Street as STREET_ADDR,
sn.City as CITY,sn.State as STATE,sn.Zip as ZIP_CODE
,sn.County as COUNTY 
 FROM tblsnfs  as sn
 WHERE ProviderNo = $provider
};

}

if($facility eq 'HOSPITAL' || $facility eq 'RENAL' || $facility eq 'HOSPICE' || $facility eq 'SNF'){
unless ($sth = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute()) {
   &display_error("ERROR executing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}
($name, $address, $city, $state, $zip, $county) = $sth->fetchrow_array;
}

if($facility eq 'HHA'){
$sql = qq{
    SELECT ha.Name as HHA_NAME,ha.Street as STREET_ADDR,
ha.City as CITY,ha.State as STATE,ha.Zip as ZIP_CODE
 FROM tblhomehealthagencies  as ha
 WHERE ProviderNo = $provider
};
unless ($sth = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute()) {
   &display_error("ERROR executing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}
($name, $address, $city, $state, $zip) = $sth->fetchrow_array;
}
$zip =~ s/-$//;  # crappy data

#$sth->finish();


#----------------------------------------------------------------------------
my @rpt;
my @rpt_year;
my $prevprovider;
my @prev_prov;
my $row;
if($facility eq 'HOSPITAL'){
$prevprovider= qq{
Select PreviousProviderNo from tblpreviousprovider where CurrentProviderNo = ?
};
 	
unless ($sth = $dbh->prepare($prevprovider)) {
   &display_error('ERROR preparing SQL query:',  $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute($provider)) {
   &display_error('ERROR executing SQL query:',  $sth->errstr);
   $dbh->disconnect;
   exit(1);
}
while ($row = $sth->fetchrow_hashref){
	 push (@prev_prov, $$row{PreviousProviderNo});
	 }
	 
	 foreach my $prev_prov(@prev_prov){
	 
	$provider=$provider.",".$prev_prov;
	}
}	
if($facility eq 'RENAL'){	
$sql = qq{
   SELECT RPT_YEAR, RPT_REC_NUM, RPT_STUS_CD, 
          DATE_FORMAT(FY_BGN_DT, '%m-%d-%Y'),
          DATE_FORMAT(FY_END_DT, '%m-%d-%Y') 
     FROM rnl_ALL_RPT
    WHERE PRVDR_NUM in ($provider)
 ORDER BY FY_BGN_DT DESC
};
}
if($facility eq 'HOSPITAL'){

$sql = qq{
   SELECT RPT_YEAR, RPT_REC_NUM, RPT_STUS_CD, 
          DATE_FORMAT(FY_BGN_DT, '%m-%d-%Y'),
          DATE_FORMAT(FY_END_DT, '%m-%d-%Y') 
     FROM CR_ALL_RPT
    WHERE PRVDR_NUM in ($provider)
 ORDER BY FY_BGN_DT DESC
};
}
if($facility eq 'HHA'){

$sql = qq{
   SELECT RPT_YEAR, RPT_REC_NUM, RPT_STUS_CD, 
          DATE_FORMAT(FY_BGN_DT, '%m-%d-%Y'),
          DATE_FORMAT(FY_END_DT, '%m-%d-%Y') 
     FROM hha_ALL_RPT
    WHERE PRVDR_NUM in ($provider)
 ORDER BY FY_BGN_DT DESC
};
}
if($facility eq 'SNF'){

$sql = qq{
   SELECT RPT_YEAR, RPT_REC_NUM, RPT_STUS_CD, 
          DATE_FORMAT(FY_BGN_DT, '%m-%d-%Y'),
          DATE_FORMAT(FY_END_DT, '%m-%d-%Y') 
     FROM snf_ALL_RPT
    WHERE PRVDR_NUM in ($provider)
 ORDER BY FY_BGN_DT DESC
};
}
if($facility eq 'HOSPICE'){

$sql = qq{
   SELECT RPT_YEAR, RPT_REC_NUM, RPT_STUS_CD, 
          DATE_FORMAT(FY_BGN_DT, '%m-%d-%Y'),
          DATE_FORMAT(FY_END_DT, '%m-%d-%Y') 
     FROM hospc_ALL_RPT
    WHERE PRVDR_NUM in ($provider)
 ORDER BY FY_BGN_DT DESC
};
}
unless ($sth = $dbh->prepare($sql)) {
   &display_error('ERROR preparing SQL query:',  $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute()) {
   &display_error('ERROR executing SQL query:',  $sth->errstr);
   $dbh->disconnect;
   exit(1);
}	
my $now = localtime();
	my $rows = $sth->rows;
print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>CHOOSER</TITLE>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon" />
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css" />
</HEAD>
<BODY>
<CENTER>


<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/cgi-bin/demo_index.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/demo_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="/cgi-bin/cr_batchx.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>Standard Reports</div>">
			        <img src="/images/stardardreport_inner_icon.gif" onmouseover="src='/images/stardardreport_inner_icon_hover.gif';"
                                    onmouseout="src='/images/stardardreport_inner_icon.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 102px" align="Center">
			                            <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
			                                <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->
<!--content start-->
<table cellpadding="0" cellspacing="0" width="885px">
        <tr><td align="left">Welcome <b>
            	$firstName  $lastName
            	
            	</b></td>  
            	<td align="right">
            	<a href="#">Sign Out</a>
            	</td>
        	</tr>
    </table>
    <table cellpadding="0" cellspacing="0" width="885px">
        <tr>
            <td valign="top">
                <img src="/images/content_left.gif" /></td>
            <td valign="top" style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px">
<Table cellpadding="0" cellspacing="0" width="100%">
	<Tr>
		<Td >
			PROVIDER # <span class="pageheader">$provider</span><br>
			<B>$name</B><BR>
<span class="tahoma12">
$address, $city, $state  $zip<BR>
</FONT>
		</td>
		<td align="right" valign="bottom" class="tahoma12" >
$now</td></tr>
</table>
<TABLE BORDER="0" align="center" CELLPADDING="2" WIDTH="100%" class="gridstyle">
<tr><Td colspan="4" style="background-color:white; font-size:11px" align="right">&nbsp;Available cost reports, ordered with most recent on top</td></tr>  
<TR class="gridheader">
    <TH>Cost Report #</TH><TH>Status Code</TH><TH>Fiscal Year Begins</TH><TH>Fiscal Year Ends</TH>
</TR>
};

my ($rpt_year, $rpt_rec_num, $rpt_stus_cd, $fy_bgn_dt, $fy_end_dt);

my ($class, $link);

my $count = 0;
 
 
while (($rpt_year, $rpt_rec_num, $rpt_stus_cd, $fy_bgn_dt, $fy_end_dt) = $sth->fetchrow_array) {

   $class = (++$count % 2) ? 'gridrow' : 'gridalternate';

   $link = "<A HREF=\"/cgi-bin/cr_explore2.pl?facility=$facility&reportid=$rpt_rec_num&provider=$provider&action=go\" TITLE=\"VIEW COST REPORT\">$rpt_rec_num</A>";
  push (@rpt, $rpt_rec_num);
  push(@rpt_year,$fy_end_dt);
   print "   <TR CLASS=\"$class\">\n";
   print "      <TD ALIGN=\"center\">$link</TD>\n";
   print "      <TD ALIGN=\"center\">$status_lookup{$rpt_stus_cd}</TD>\n";
   print "      <TD ALIGN=\"center\">$fy_bgn_dt</TD>\n";
   print "      <TD ALIGN=\"center\">$fy_end_dt</TD>\n";
   print "   </TR>\n";
   
}
my $arraycount=0;
 my $str="";
 my $sstr="";
 my $rptyear=@rpt_year;
 my $i;

  for($i=0;$i<$rptyear;$i++){
    $str= $str.",".$rpt_year[$i];
	$sstr= $sstr.",".$rpt[$i];
 }

 #foreach $prov(@rpt) {

 #str=$str."<OPTION>".$prov."-".$rptyear."</OPTION>";
#$str=$str.",".$prov;
#}

$session->param("sstr", $sstr);
$session->param("str", $str);
my $elapsed_time = time - $start_time;
#my $prov=$session->load_param($cgi, ["str"]);
my $prov=$session->param("str");
print qq{
</TABLE>
<span class="tahoma12">
$count ROWS
</span>
		</td>
	
		<td valign="top">
		<img src="/images/content_right.gif" /></td>
	</tr>
	</table>
<!--content end-->
};
&TDdoc;
print qq{
</CENTER>
</BODY>
</HTML>
};

$sth->finish;
$dbh->disconnect;
   
exit(0);


#------------------------------------------------------------------------------

sub display_error
{
   my $s;

   print "Content-Type: text/html\n\n";

   print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css" />
<link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />
</HEAD>
<BODY>
<CENTER>
<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/cgi-bin/demo_index.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/demo_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>Standard Reports</div>">
			        <img src="/images/stardardreport_inner_icon.gif" onmouseover="src='/images/stardardreport_inner_icon_hover.gif';"
                                    onmouseout="src='/images/stardardreport_inner_icon.gif';" style="border: 0px; cursor: pointer" /></a></td>
                      <td style="width: 102px" align="Center">
                            <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
                                <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->
<FONT SIZE="5" COLOR="RED">
<B>ERRORS</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" BORDER="1" CELLPADDING="20" WIDTH="600">
<TR><TD>
<UL>
};
   foreach $s (@_) {
      print "$s<BR>";
   }
   print qq{
</TD></TR>
</TABLE>

<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</FONT>
</CENTER>
</BODY>
</HTML>
};
}

#----------------------------------------------------------------------------

sub comify
{
   local($_) = shift;
   1 while s/^(-?\d+)(\d{3})/$1,$2/;
   return($_);
}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#--------------------------------------------------------------------------------------
sub sessionExpire
{

print "Content-Type: text/html\n\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

#------------------------------------------------------------------------------

