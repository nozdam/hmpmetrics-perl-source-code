#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: graph1.pl
#  author: Steve Spicer
#  Written: Sun Oct 12 23:57:12 CDT 2008
#----------------------------------------------------------------------------

use strict;
use CGI;
use DBI;
use GD::Graph::linespoints;
do "pagefooter.pl";
do "common_header.pl";
print "Content-Type: text/html\n\n";
&headerScript();
use CGI::Session;

my $start_time = time;

my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");

if(!$userID)
{
	&sessionExpire;
}

my $action    = $cgi->param('action');    
my $text      = $cgi->param('providers'); # aaa.txt bbb.txt
my $mode      = $cgi->param('mode');      # svg / gif

my $om_sel    = $cgi->param('opmargin');
my $em_sel    = $cgi->param('exmargin');
my $pe_sel    = $cgi->param('peratio');

my %operating_margin;
my %excess_margin;
my %personell_expense_ratio;

my %ini_value;

unless (&read_inifile("milo.ini")) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my @year_list = split /,/,$ini_value{'HCRIS_YEARS'};  

unless ($action eq 'go') {
   &display_form();
   exit(0);
}

my ($line, @list, $s, $len, $string, $provider, $n, $width, $value, $row, $column, $desc);

my @provider_list;

foreach $s (split /\n/,$text) {
   $s =~ s/[ \r\n]//g;
   $len = length($s);
   if ($len == 6) {
      push @provider_list, uc($s);
   }
}

$n = @provider_list;

unless ($n) {
   &display_error("YOU MUST PROVIDE ONE OR MORE 6 CHARACTER PROVIDER ID'S");
   exit(0);
}

my $dbtype     = $ini_value{"DB_TYPE"};
my $dblogin    = $ini_value{"DB_LOGIN"};
my $dbpasswd   = $ini_value{"DB_PASSWORD"};
my $dbname     = $ini_value{"DB_NAME"};
my $dbserver   = $ini_value{"DB_SERVER"};

my $dbh;

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

my $graph_title = '';

if ($om_sel) {
   &compute_operating_margin_values();
   $graph_title .= "OPERATING MARGIN BY YEAR";
}

if ($em_sel) {
   &compute_excess_margin_values();
   $graph_title .= " EXCESS MARGIN BY YEAR";
}

if ($pe_sel) {
   &compute_personell_expense_ratio_values();
   $graph_title .= " PERSONELL EXPENSE AS PERCENT OF TOTAL OPERATING REVENUE";
}

$dbh->disconnect();

&generate_gif_output($graph_title);

exit(0);


#-----------------------------< SUBROUTINES >--------------------------------

sub compute_operating_margin_values
{
   my ($operating_revenue, $operating_expense);

   my ($provider, $year, $value);

   foreach $provider (@provider_list) {

      foreach $year (@year_list) {
          $operating_revenue     = &get_value_by_address($year, $provider, "G300000:00300:0100");
          $operating_expense     = &get_value_by_address($year, $provider, "G300000:00400:0100");

          if ($operating_expense && $operating_revenue) {
             $value  = ($operating_revenue - $operating_expense) / $operating_revenue * 100;
             $operating_margin{"$provider:$year"}  = sprintf("%.01f",$value);
          } else {
             $operating_margin{"$provider:$year"}  = undef;  # until i have a better idea
          }
       }
    }
}

#----------------------------------------------------------------------------

sub compute_excess_margin_values
{
   my ($operating_revenue, $operating_expense, $non_operating_revenue);

   my ($provider, $year, $value);

   foreach $provider (@provider_list) {

      foreach $year (@year_list) {
          $operating_revenue     = &get_value_by_address($year, $provider, "G300000:00300:0100");
          $operating_expense     = &get_value_by_address($year, $provider, "G300000:00400:0100");
          $non_operating_revenue = &get_value_by_address($year, $provider, "G300000:02500:0100");

          if ($operating_expense && $operating_revenue) {
             $value  = ($operating_revenue - $operating_expense + $non_operating_revenue) / ($operating_revenue + $non_operating_revenue) * 100;
             $excess_margin{"$provider:$year"}  = sprintf("%.01f",$value);
          } else {
             $excess_margin{"$provider:$year"}  = undef;  # until i have a better idea
          }
       }
    }
}

#---------------------------------------------------------------------------

sub compute_personell_expense_ratio_values
{
   my ($salary_expense, $contract_labor, $fringe_benefits, $operating_revenue);

   my ($provider, $year, $value);

   foreach $provider (@provider_list) {

      foreach $year (@year_list) {
          $salary_expense        = &get_value_by_address($year, $provider, "A000000:10100:0100");
          $operating_revenue     = &get_value_by_address($year, $provider, "G300000:00300:0100");
          $fringe_benefits       = &get_value_by_address($year, $provider, "A000000:00500:0200");  # i question
          $contract_labor        = &get_value_by_address($year, $provider, "S300002:00900:0300");
          $contract_labor       += &get_value_by_address($year, $provider, "S300002:00901:0300");
          $contract_labor       += &get_value_by_address($year, $provider, "S300002:00902:0300");
          $contract_labor       += &get_value_by_address($year, $provider, "S300002:01000:0300");
          $contract_labor       += &get_value_by_address($year, $provider, "S300002:01001:0300");
          $contract_labor       += &get_value_by_address($year, $provider, "S300002:01100:0300");
          $contract_labor       += &get_value_by_address($year, $provider, "S300002:01200:0300");
          $contract_labor       += &get_value_by_address($year, $provider, "S300002:01201:0300");

          if ($operating_revenue) {
             $value  = ($salary_expense + $contract_labor + $fringe_benefits) / $operating_revenue * 100;
             $personell_expense_ratio{"$provider:$year"}  = sprintf("%.01f",$value);
          } else {
             $personell_expense_ratio{"$provider:$year"}  = undef;  # until i have a better idea
          }
       }
    }
}



#----------------------------------------------------------------------------

sub get_value_by_address
{
   my ($year, $provider, $address) = @_;

   my ($worksheet, $row, $col) = split /[-:\|]/,$address;

   my ($sth, $sql);

   $sql = qq/
    SELECT CR_VALUE         
      FROM CR_ALL_RPT AA
 LEFT JOIN CR_ALL_DATA BB
        ON AA.RPT_REC_NUM = BB.CR_REC_NUM
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
     WHERE PRVDR_NUM     = ?
	 AND AA.year(FY_END_DT)	= $year
   /;

   unless ($sth = $dbh->prepare($sql)) {
       &display_error("Error preparing sql query", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   $sth->execute($worksheet, $row, $col, $provider);

   my ($value) = $sth->fetchrow_array;

   $value = 0 unless(defined $value);

   return($value);
}

#------------------------------------------------------------------------------


sub generate_svg_output
{

   my ($n, $i, $x, $y, $x1, $y1, $label, $sample, $text);

   my $zero;
   my $chart_width  = 800;
   my $chart_height = 600;
   my ($key, $value);
   my ($provider, $year);

   my @colors = ('red', 'green', 'blue', 'orange', 'purple', 'pink', 'lavender');
   
   my @values;
   
   my $min_value =  999999;
   my $max_value = -999999;

   my %provider_values;

   foreach $key (sort keys %operating_margin) {

       ($provider, $year) = split /\:/,$key;

       $value = $operating_margin{$key};

       if ($value > $max_value) {
          $max_value = $value;
       }
       if ($value < $min_value) {
          $min_value = $value;
       }

       push @{$provider_values{$provider}}, $value;
   }

   $n = @year_list;

   my $x_spacing = $chart_width / ($n + 1);

   unless ($min_value < 0) {
      $min_value = 0;
   }

   my $range   = ($max_value - $min_value) * 1.40;

   my $scale = ($chart_height) / $range;

   my $zero  = $chart_height;

   if ($min_value < 0) {
      $zero = $chart_height - ($scale * 1.20 * abs($min_value));
   }

   my $floor;
   my $ceiling;

   print "Content-Type: image/svg+xml\n\n";
   
   print <<EOF
<?xml version="1.0" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg viewBox="0 0 $chart_width $chart_height" xmlns="http://www.w3.org/2000/svg" version="1.1">
<!-- range=$range scale=$scale -->
 <style type="text/css"><![CDATA[
    .Border { fill:none; stroke:grey;  stroke-width:2 }
    .Line   { fill:none; stroke:blue;  stroke-width:1 }
    .Point  { stroke:black; stroke-width:1 }
    .Grid   { fill:none; stroke:lightblue; stroke-width:1 }
    .Label  { font-size:10pt; font-family:Verdana; font-weight: normal }
  ]]></style>
  <rect class="Border" x="1" y="1" width="798" height="598"/>
EOF
;
   my $title = "operating margin = (revenue - expense) /  revenue * 100";

   print "<text class=\"Label\"  x=\"150\" y=\"20\">$title</text>\n";

   my $z = $range / 10;

   for $i (1 .. 9) {
      $value  = $min_value + ($z * $i);
      $y = $zero - ($scale * $value);   # horizonal lines
      print "<path class=\"Grid\" d=\"M 0 $y L $chart_width $y\"></path>\n";
      $y = $y - 4;
      $label = sprintf("%.02f", $value);
      print "<text class=\"Label\" x=\"3\" y=\"$y\">$label</text>\n";
   }
   
   $i = 1;
   foreach $year (@year_list) {    # vertical lines
      $x = $i * $x_spacing;
      $y = $zero - 4;
      $x1 = $x - 15;
      print "<path class=\"Grid\" d=\"M $x 0 L $x, $chart_height\"></path>\n";
      print "<text class=\"Label\" x=\"$x1\" y=\"$y\">$year</text>\n";
      ++$i;
   }
   

   my $set = 0;

   foreach $key (keys %provider_values) {

      @values = @{$provider_values{$key}};

      $y = 20 * ($set + 1);

      print "<text class=\"Label\" stroke=\"$colors[$set]\" x=\"50\" y=\"$y\">$key</text>\n";

      $i = 1;
      foreach $sample (@values) {
         $x = $i * $x_spacing;   # label values
         $y = $zero - ($scale * $sample);
         print "<circle class=\"Point\" fill=\"$colors[$set]\" cx=\"$x\" cy=\"$y\" r=\"4\" />\n";

         $x = $x + 4;
         $y = $y - 4;
         $text = sprintf("%.02f%%",$sample);
         print "<text class=\"Label\" x=\"$x\" y=\"$y\">$text</text>\n";
         ++$i;
      };
   
      print "<polyline fill=\"none\" stroke=\"$colors[$set]\" stroke-width=\"1\" points=\"";
   
      $i = 1;
      foreach $sample (@values) {
         $x = $i * $x_spacing;
         $y = $zero - ($scale * $sample);
         print "$x,$y  ";
         ++$i;
      }
   
      print "\" />\n";
      ++$set;
   }

   print "</svg>\n";

}


#----------------------------------------------------------------------------

sub generate_gif_output
{

   my ($title) = @_;

   my @data;
   my @legend;
   my @values;

   my %om_values;
   my %em_values;
   my %pe_values;

   push (@data,\@year_list);

   my ($key, $value, $provider, $year);

   if ($om_sel) {
      foreach $key (sort keys %operating_margin) {
          ($provider, $year) = split /\:/,$key;
          $value = $operating_margin{$key};
          push @{$om_values{$provider}}, $value;
      }
      foreach $key (keys %om_values) {
         push @legend, "$key OPERATING MARGIN";
         push @data, \@{$om_values{$key}};
      }
   }

   if ($em_sel) {
      foreach $key (sort keys %excess_margin) {
          ($provider, $year) = split /\:/,$key;
          $value = $excess_margin{$key};
          push @{$em_values{$provider}}, $value;
      }
      foreach $key (keys %em_values) {
         push @legend, "$key EXCESS MARGIN";
         push @data, \@{$em_values{$key}};
      }
   }

   if ($pe_sel) {
      foreach $key (sort keys %personell_expense_ratio) {
          ($provider, $year) = split /\:/,$key;
          $value = $personell_expense_ratio{$key};
          push @{$pe_values{$provider}}, $value;
      }
      foreach $key (keys %pe_values) {
         push @legend, "$key PERSONELL EXPENSE RATIO";
         push @data, \@{$pe_values{$key}};
      }
   }



   my $my_graph = new GD::Graph::linespoints(1024,600);

   $my_graph->set( 
        title => $title,
        x_label => "YEAR",
        y_label => "PERCENT",
        y_max_value => 80,
        y_min_value => -50,
        y_tick_number => 10,
        show_values => 1,
        skip_undef  => 1,
    #   y_plot_values => 1,
        y_label_skip => 0,
	y_long_ticks => 1,
	x_label_position => 1/2,
        legend_placement => 'R',
        zero_axis => 1,
        line_width => 2,
        overwrite => 0,
        borderclrs => $my_graph->{dclrs},
        bar_spacing => 20,
   );

   $my_graph->set_legend(@legend);
   #my $image = ($my_graph->plot(\@data))->gif;
   
   open IMG, ">../htdocs/images/temp.gif";
   binmode IMG; #only for Windows like platforms
   print IMG ($my_graph->plot(\@data))->gif;
   close IMG;

   print qq{
   <HTML>
   <HEAD><TITLE>ERROR</TITLE>
   </HEAD>
   <BODY>
   <CENTER>
   };
   &innerHeader();
   print qq{
   <BR>
       <table cellpadding="0" cellspacing="0" width="885px">
        <tr>
            <td valign="top">
                <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;
                text-align: left; padding: 10px" valign="top"> 
			<TABLE CLASS="img" CELLPADDING="20" WIDTH="100%">
      <TR>      	 
         <TD><img border="0" src="/temp.gif"></TD>
      </TR>
   </TABLE>
</td>
            <td valign="top">
                <img src="/images/content_right.gif" /></td>
        </tr>
    </table>
 };
&TDdoc;
print qq{
    </CENTER>   
   </BODY>
   </HTML>
   };

   
}


#------------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}



print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>

<table cellpadding="0" cellspacing="0" width="885px">
        <tr>
            <td valign="top">
                <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;
                text-align: left; padding: 10px" valign="top"><FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
</td>
            <td valign="top">
                <img src="/images/content_right.gif" /></td>
        </tr>
    </table>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
};
&TDdoc;
print qq{
</CENTER>
</BODY>
</HTML>
};

}

#---------------------------------------------------------------------------

sub display_form
{


   print qq{
<HTML>
<HEAD><TITLE>COST REPORTS GRAPH</TITLE></HEAD>
</HEAD>
<BODY>
<BR>
<CENTER>
};
&innerHeader();
print qq{
<FORM METHOD="GET" ACTION="/cgi-bin/graph.pl">

    <!--content start-->
    <table cellpadding="0" cellspacing="0" width="885px">
            <tr><td align="left">Welcome <b>
                	};
                	
                	print $firstName . " " . $lastName;
                	print qq{
                	</b></td>  
                	<td align="right">
                	<a href="/cgi-bin/login.pl?saction=signOut">Sign Out</a>
                	</td>
            	</tr>
    </table>
    <table cellpadding="0" cellspacing="0" width="885px">
        <tr>
            <td valign="top">
                <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;
                text-align: left; padding: 10px" valign="top">
<div class="pageheader">FINANCIAL RATIOS FROM COST REPORTS GRAPHING</div>
<TABLE BORDER="0" CELLPADDING="5" WIDTH="100%" class="gridstyle" style="background-color:white">
	<Tr>
		<Td valign="top">
			<B> FINANCIAL RATIOS </B><br><br>

         <INPUT TYPE="checkbox" name="opmargin" CHECKED> OPERATING MARGIN<BR>
         <INPUT TYPE="checkbox" name="exmargin"> EXCESS MARGIN<BR>
         <INPUT TYPE="checkbox" name="peratio"> PERSONELL EXPENSE / TOTAL OPERATING REVENUE
		</td>
		<td valign="top" width="50%">
			<B>PEER GROUP SELECTION</B><BR>
Cut and paste from provider numbers into area below use a single column
         <br><br>

<TEXTAREA NAME="providers" COLS="10" ROWS="7" WRAP="HARD" style="width:30%">$text</TEXTAREA>

		</td>
	</tr>
</table>
<div style="padding-top:5px">
 <INPUT TYPE="hidden" NAME="action" VALUE="go">
         <INPUT TYPE="hidden" NAME="mode" VALUE="gif">
         <span class="button"><INPUT TYPE="reset"  VALUE=" CLEAR "></span>
         <span class="button"><INPUT TYPE="submit" VALUE="  GENERATE GRAPH  "></span></div>
 
</td>
            <td valign="top">
                <img src="/images/content_right.gif" /></td>
        </tr>
    </table>
    <!--content end-->
	};
&TDdoc;
print qq{
</FORM>
</CENTER>
</BODY>
</HTML>
   };
}


#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#--------------------------------------------------------------------------------------
sub sessionExpire
{


print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

#------------------------------------------------------------------------------
