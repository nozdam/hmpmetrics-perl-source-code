#!/usr/bin/perl -w

#----------------------------------------------------------------------------
#  Program: barchart.pl
#   Author: Steve Spicer
#  Written: Sun Oct  5 10:10:13 CDT 2008
#----------------------------------------------------------------------------

use strict;
use DBI;
#use GD::Graph::bars;
use GD::Graph::lines;


my @data;
my @types;
my @counts;

my $total = 0;

my %ini_value;

unless (&read_inifile("milo.ini")) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{"DB_TYPE"};
my $dblogin    = $ini_value{"DB_LOGIN"};
my $dbpasswd   = $ini_value{"DB_PASSWORD"};
my $dbname     = $ini_value{"DB_NAME"};
my $dbserver   = $ini_value{"DB_SERVER"};

my ($dbh, $sql, $sth);

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

$sql = qq{
   SELECT count(*) as count, HOSP_TYPE
     FROM HOSPITALS
 GROUP BY HOSP_TYPE
};

$sth = $dbh->prepare($sql) || die $dbh->errstr;

$sth->execute() || die $dbh->errstr;

my ($count, $hosp_type);

while (($count, $hosp_type) = $sth->fetchrow_array)
{
    push @counts, $count;
    push @types, $hosp_type;
    $total += $count;
}

$sth->finish;
$dbh->disconnect;


push (@data,\@types);
push (@data,\@counts);
push (@data,\@counts);

my $my_graph = new GD::Graph::lines(640,480);

$my_graph->set( 
        x_label => "HOSP TYPESS",
        y_label => "COUNT",
        title => "DISTRIBUTION OF $total NETWORKS BY REGION",
        y_max_value => 1500,
        y_tick_number => 10,
        y_label_skip => 1,
	y_long_ticks => 1,
	x_label_position => 1/2,
        overwrite => 0,
        borderclrs => $my_graph->{dclrs},
        bar_spacing => 20,
);

$my_graph->set_legend( qw(offset ncrement more));

my $image = ($my_graph->plot(\@data))->gif;

print "Content-Type: image/gif\n\n";
print $image;
exit 0;

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#------------------------------------------------------------------------------







# 
# Database: HCRISDB  Table: HOSPITALS  Wildcard: %
# +-----------------+---------------+-------------------+------+-----+---------+-------+---------------------------------+---------+
# | Field           | Type          | Collation         | Null | Key | Default | Extra | Privileges                      | Comment |
# +-----------------+---------------+-------------------+------+-----+---------+-------+---------------------------------+---------+
# | PROVIDER_NUMBER | char(6)       | latin1_swedish_ci | NO   | PRI |         |       | select,insert,update,references |         |
# | FYB             | date          |                   | YES  |     |         |       | select,insert,update,references |         |
# | FYE             | date          |                   | YES  |     |         |       | select,insert,update,references |         |
# | STATUS          | varchar(15)   | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
# | CTRL_TYPE       | char(2)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
# | HOSP_NAME       | varchar(45)   | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
# | STREET_ADDR     | varchar(45)   | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
# | PO_BOX          | varchar(30)   | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
# | CITY            | varchar(38)   | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
# | STATE           | varchar(11)   | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
# | ZIP_CODE        | varchar(10)   | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
# | COUNTY          | varchar(35)   | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
# | URBAN1_RURAL    | char(2)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
# | LATITUDE        | decimal(8,5)  |                   | YES  |     |         |       | select,insert,update,references |         |
# | LONGITUDE       | decimal(8,5)  |                   | YES  |     |         |       | select,insert,update,references |         |
# | GEOACC          | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
# | SYSTEM          | char(5)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
# | FTES            | decimal(5,0)  |                   | YES  |     |         |       | select,insert,update,references |         |
# | BEDS            | decimal(5,0)  |                   | YES  |     |         |       | select,insert,update,references |         |
# | HOSP_TYPE       | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
# | NET_INCOME      | decimal(11,0) |                   | YES  |     |         |       | select,insert,update,references |         |
# +-----------------+---------------+-------------------+------+-----+---------+-------+---------------------------------+---------+
# 
# mysqlshow: Cannot list columns in db: HCRISDB, table: hospitals: Table 'HCRISDB.hospitals' doesn't exist

