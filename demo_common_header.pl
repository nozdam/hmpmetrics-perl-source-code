#!/usr/bin/perl

use strict;
use CGI;
use DBI; 

my $cgi=new CGI;
#print "Content-Type: text/html\n\n";

sub headerScript{
	print qq{
	<style>
	#sddm
	{	margin: 0;
		padding: 0;
		z-index: 30}

	#sddm li
	{	margin: 0;
		padding: 0;
		list-style: none;
		float: left;
		font: bold 11px arial}

	#sddm li a
	{	display: block;
		margin: 0 1px 0 0;
		padding: 4px 10px;
		width: 60px;
		background: #5970B2;
		color: #FFF;
		text-align: center;
		text-decoration: none}

	#sddm li a:hover
	{	background-image: /images/h_search_icon_hover.gif}

	#sddm div
	{	position: absolute;
		visibility: hidden;
		margin: 0;
		padding: 0;
		background: #EAEBD8;
		border: 1px solid #5970B2}

		#sddm div a
		{	position: relative;
			display: block;
			margin: 0;
			padding: 5px 10px;
			width: auto;
			white-space: nowrap;
			text-align: left;
			text-decoration: none;
			background: #E0ECF8;
			color: #2875DE;		
			font: 11px Verdana;
			font-weight: bold}

		#sddm div a:hover
		{	background: #F5D0A9;
			color: #FFF}

	</style>


	<script>
		var timeout	= 500;
	var closetimer	= 0;
	var ddmenuitem	= 0;

	// open hidden layer
	function mopen(id)
	{	
		// cancel close timer
		mcancelclosetime();

		// close old layer
		if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';

		// get new layer and show it
		ddmenuitem = document.getElementById(id);
		ddmenuitem.style.visibility = 'visible';

	}
	// close showed layer
	function mclose()
	{
		if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';
	}

	// go close timer
	function mclosetime()
	{
		closetimer = window.setTimeout(mclose, timeout);
	}

	// cancel close timer
	function mcancelclosetime()
	{
		if(closetimer)
		{
			window.clearTimeout(closetimer);
			closetimer = null;
		}
	}

	// close layer when click-out
	document.onclick = mclose; 


	</script>

	};
}


sub indexHeader{

&headerScript();

print qq{
<ul id="sddm">
<table cellpadding="0" cellspacing="0" width="750px" align="center">
	<tr>
	    <td>
		<img src="/images/home_menu_left.gif" /></td>
	    <td style="background-image: url(/images/home_menu_middle.gif); width: 100%; background-repeat: repeat-x;
		text-align: center">
		<table cellpadding="0" cellspacing="0">
		    <tr>
			<td style="width: 116px" align="Center">
			    <a href="">
				<img src="/images/search_icon-new.gif" onmouseover="src='/images/search_icon_hover-new.gif';mopen('search')"
				    onmouseout="src='/images/search_icon-new.gif';"  style="border: 0px; cursor: pointer" /></a></td>
				          
			<td style="width: 132px" align="Center">
			    <a href="">
				<img src="/images/spreadsheet_icon-new.gif" onmouseover="src='/images/spreadsheet_icon_hover-new.gif';mopen('spreadsheet')"
				    onmouseout="src='/images/spreadsheet_icon-new.gif';" style="border: 0px; cursor: pointer" /></a></td>
			<td style="width: 155px" align="Center">
			    <a href="">
				<img src="/images/cost_report_icon.gif" onmouseover="src='/images/cost_report_icon_hover.gif';mopen('costreport')"
				    onmouseout="src='/images/cost_report_icon.gif';" style="border: 0px; cursor: pointer" /></a></td>
			<td style="width: 132px" align="Center">
			    <a href="">
				<img src="/images/stardardreport_icon.gif" onmouseover="src='/images/stardardreport_icon_hover.gif';mopen('stdreport')"
				    onmouseout="src='/images/stardardreport_icon.gif';" style="border: 0px; cursor: pointer" /></a></td>
			<td style="width: 168px" align="Center">
			    <a href="">
				<img src="/images/peer_group_icon.gif" onmouseover="src='/images/peer_group_icon_hover.gif';mopen('peergroup')"
				    onmouseout="src='/images/peer_group_icon.gif';" style="border: 0px; cursor: pointer" /></a></td>
		    </tr>
		    <tr>
		    <td style="width: 116px" align="Center">
		    	<li> <div id="search"
				onmouseover="mcancelclosetime()"
				onmouseout="mclosetime()">
			    <a href="demo_search.pl">Hospital</a>
			    <a href="#">Skilled Nursing Facility</a>
			    <a href="#">Renal Facility</a>
			    <a href="#">Hospice</a>
			    <a href="#">Home Health Agency</a>
        		</div></li>
		    </td>
		    <td style="width: 132px" align="Center">
			<li> <div id="spreadsheet"
				onmouseover="mcancelclosetime()"
				onmouseout="mclosetime()">
			    <a href="#">Hospital</a>
			    <a href="#">Skilled Nursing Facility</a>
			    <a href="#">Renal Facility</a>
			    <a href="#">Hospice</a>
			    <a href="#">Home Health Agency</a>
			</div></li>
		    </td>
		    <td style="width: 155px" align="Center">
		    	<li> <div id="costreport"
				onmouseover="mcancelclosetime()"
				onmouseout="mclosetime()">
			    <a href="#">Hospital</a>
			    <a href="#">Skilled Nursing Facility</a>
			    <a href="#">Renal Facility</a>
			    <a href="#">Hospice</a>
			    <a href="#">Home Health Agency</a>
			</div></li>
		    </td>
		    <td style="width: 132px" align="Center">
			<li> <div id="stdreport"
				onmouseover="mcancelclosetime()"
				onmouseout="mclosetime()">
			    <a href="#">Hospital</a>
			    <a href="#">Skilled Nursing Facility</a>
			    <a href="#">Renal Facility</a>
			    <a href="#">Hospice</a>
			    <a href="#">Home Health Agency</a>
			</div></li>
		    </td>
		    
		    <td style="width: 168px" align="Center">
			<li> <div id="peergroup"
				onmouseover="mcancelclosetime()"
				onmouseout="mclosetime()">
			    <a href="#">Hospital</a>
			    <a href="#">Skilled Nursing Facility</a>
			    <a href="#">Renal Facility</a>
			    <a href="#">Hospice</a>
			    <a href="#">Home Health Agency</a>
			</div></li>
		    </td>
		    </tr>
		</table>

	    </td>
	    <td>
		<img src="/images/home_menu_right.gif" /></td>
	</tr>
</table>	
		 
</ul>

};
}

sub innerHeader{
&headerScript();
print qq{
    <ul id="sddm">
    <table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/cgi-bin/demo_index.pl">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';mopen('search')"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';mopen('spreadsheet')"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                        	<a href="">
				   <img src="/images/cost_report_icon_inner.gif" onmouseover="src='/images/cost_report_icon_inner_hover.gif';mopen('costreport')"
                                    onmouseout="src='/images/cost_report_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a>
                        </td>
                        <td style="width: 79px" align="Center">
                            <a href="">
			        <img src="/images/stardardreport_inner_icon.gif" onmouseover="src='/images/stardardreport_inner_icon_hover.gif';mopen('stdreport')"
                                    onmouseout="src='/images/stardardreport_inner_icon.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 102px" align="Center">
                            <a href="">
			                                    <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';mopen('peergroup')"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                    <tr>
			    <td style="width: 78px" align="Center">
			    </td>
			    <td style="width: 81px" align="Center">
				<li> <div id="search"
					onmouseover="mcancelclosetime()"
					onmouseout="mclosetime()">
				    <a href="demo_search.pl">Hospital</a>
				    <a href="#">Skilled Nursing Facility</a>
				    <a href="#">Renal Facility</a>
				    <a href="#">Hospice</a>
				    <a href="#">Home Health Agency</a>
				</div></li>
			    </td>
			    <td style="width: 85px" align="Center">
				<li> <div id="spreadsheet"
					onmouseover="mcancelclosetime()"
					onmouseout="mclosetime()">
				    <a href="#">Hospital</a>
				    <a href="#">Skilled Nursing Facility</a>
				    <a href="#">Renal Facility</a>
				    <a href="#">Hospice</a>
				    <a href="#">Home Health Agency</a>
				</div></li>
			    </td>
			    <td style="width: 96px" align="Center">
			    	<li> <div id="costreport"
					onmouseover="mcancelclosetime()"
					onmouseout="mclosetime()">
				    <a href="#">Hospital</a>
				    <a href="#">Skilled Nursing Facility</a>
				    <a href="#">Renal Facility</a>
				    <a href="#">Hospice</a>
				    <a href="#">Home Health Agency</a>
				</div></li>
                        	</td>
			    <td style="width: 79px" align="Center">
				<li> <div id="stdreport"
					onmouseover="mcancelclosetime()"
					onmouseout="mclosetime()">
				    <a href="#">Hospital</a>
				    <a href="#">Skilled Nursing Facility</a>
				    <a href="#">Renal Facility</a>
				    <a href="#">Hospice</a>
				    <a href="#">Home Health Agency</a>
				</div></li>
			    </td>

			    <td style="width: 102px" align="Center">
				<li> <div id="peergroup"
					onmouseover="mcancelclosetime()"
					onmouseout="mclosetime()">
				    <a href="#">Hospital</a>
				    <a href="#">Skilled Nursing Facility</a>
				    <a href="#">Renal Facility</a>
				    <a href="#">Hospice</a>
				    <a href="#">Home Health Agency</a>
				</div></li>
			    </td>
		    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table>    
    </ul>
};
}