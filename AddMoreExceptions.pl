#!/usr/bin/perl

use CGI;
use DBI;

my $cgi = new CGI;
do "common_header.pl";
print "Content-Type: text/html\n\n";
&headerScript();
use CGI::Session;

my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");

my $saction = $cgi->param("saction");
my $Exceptionname = $cgi->param("Excp_name");
my $Excp_mainid = $cgi->param("Excp_mainid");
my $facility=$cgi->param("facility");

if(!$userID)
{
	&sessionExpire;
}

print qq{
<HTML>
<HEAD><TITLE>Standard Reports</TITLE>
 <script type="text/javascript" src="/JS/AjaxScripts/prototype.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/effects.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/controls.js"></script> 
  <script language="javascript" src="/JS/admin_panel/exceptions/AddMoreExceptions.js" ></script>
</HEAD>
<BODY onload="setCreate()">
<FORM NAME = "frmExceptionActions" Action="/cgi-bin/ExceptionActions.pl?facility=$facility" method="post">
<TABLE align="top" border=1 class="gridstyle" CELLPADDING="0" CELLSPACING="0" width="100%">	
};
print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Formula</td><td style=\"background-color:#edf2fb\"><table><tr><td><input type=\"text\" name=\"txtFormula\" id=\"Formula0\" size=\"60\" maxLength=\"1000\"></td><td><span class=\"button\"><INPUT TYPE=\"button\" value=\"Formula Builder\" name=\"btnLoad\" id=\"btnLoad0\" onClick=\"LoadFormulas(1,'Formula0')\" /></span></td></tr></table></td></tr>\n";
print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Operator</td><td style=\"background-color:#edf2fb\"><select name=\"txtOperator\"><option value=\"0\" selected> Select operator </option><option value=\">\"> > </option><option value=\"<\"> < </option><option value=\"=\"> = </option><option value=\"!=\"> != </option><option value=\">=\"> >= </option><option value=\"<=\"> <= </option></select></td></tr>\n";
print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Sign</td><td style=\"background-color:#edf2fb\"><select name=\"txtSign\"><option value=\"+/-\" selected> -/+ </option><option value=\"+\"> + </option><option value=\"-\"> - </option></select></td></tr>\n";
print "<tr><td nowrap style=\"color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Value</td><td style=\"background-color:#edf2fb\"><input type=\"text\" name=\"txtValue\" maxLength=\"1000\"></td></tr>\n";
print qq{
</TABLE>
<input type="hidden" name="saction" value="$saction">
<input type="hidden" name="facility" value="$facility">
<input type="hidden" name="txtExcpName" value="$Exceptionname">
<input type="hidden" name="Excp_mainid" value="$Excp_mainid">
<input type="hidden" name="hdnFormulaTxtId" value="">
</FORM>
</BODY>
</HTML>
};
#--------------------------------------------------------------------------------------

sub sessionExpire
{

print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}
#-----------------------------------------------------------------------------

#Perl trim function to remove whitespace from the start and end of the string

sub trim($)
{
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}
