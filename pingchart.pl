#!/usr/bin/perl -w

#  pingchart.pl

use strict;

use GD::Graph::lines;
#use GD::Graph::bars;

my $max_time = 0;

my @xval;
my @yval;
my @column;
my @data;

my ($line, $trash, $bytes, $addr, $seq, $ttl, $time);

my $cmd = "ping -c 10 -n www.cnn.com";

open(INPUT,"$cmd|");

while ($line = <INPUT>) 
{
   if ($line =~ m/bytes from/) {
      @column = split / /, $line;

      $bytes = $column[0];
      $addr  = $column[3];
      
      ($trash, $seq)  = split /=/, $column[4];
      ($trash, $ttl)  = split /=/, $column[5];
      ($trash, $time) = split /=/, $column[6];

      $max_time = $time if ($time > $max_time);

      push @yval, $time;
      push @xval, $seq;
   };
}

# 10 packets transmitted, 10 packets received, 0% packet loss
# round-trip min/avg/max = 124.2/138.8/166.3 ms

close(INPUT);

#-------------------------< DONE WITH DATABASE >------------------------

push (@data,\@xval);
push (@data,\@yval);

my $my_graph = new GD::Graph::lines(640,480);

$my_graph->set( 
        x_label => "PACKET SEQUENCE NUMBER",
        y_label => "ROUND TRIP TIME IN MS",
        title => "PING TIME DISTRIBUTION",
        y_max_value => ($max_time * 2),
        y_tick_number => 10,
        y_label_skip => 1,
	y_long_ticks => 1,
	x_label_position => 1/2,
        overwrite => 0,
        bar_spacing => 10,
        borderclrs => $my_graph->{dclrs}
);

my $image = ($my_graph->plot(\@data))->png;

print "Content-Type: image/png\n\n";
print $image;
