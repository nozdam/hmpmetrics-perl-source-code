#!/usr/bin/perl

use CGI;
use DBI;
do "common_header.pl";
my $cgi = new CGI;
my $param = $cgi->param('search');
my $saction = $cgi->param('saction');

# Database 

my $dbh;
my ($sql, $sth);

# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my $defaultYear= $ini_value{'DEFAULT_YEAR'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}


if ($saction eq 'checkUser')
{
$sql = "SELECT UserName FROM Users WHERE upper(UserName) = upper('$param')";
$sth = $dbh->prepare($sql);
$sth->execute();
	
print CGI->header('text/xml');

print <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<result>
EOF
;
if($sth->rows()>0) { 
	print "true</result>";
}
else
{
	print "false</result>";
}
qq{

};
}
if($saction eq 'CheckReport'){	
	$sql = "select * from standard_reports where upper(reports_name) = upper('$param')";
	$sth = $dbh->prepare($sql);
	$sth->execute();
print CGI->header('text/xml');

print <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<result>
EOF
;
if($sth->rows()>0) { 
	print "true</result>";
}
else
{
	print "false</result>";
}
qq{

};	
}	

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach my $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}

#------------------------------------------------------------------------------