#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: exp1.pl
#  author: Steve Spicer
#  Written: Wed Oct 24 20:14:32 CDT 2007
#----------------------------------------------------------------------------

use strict;
use CGI;
use DBI;
do "pagefooter.pl";
use CGI::Session;
do "demo_common_header.pl";

my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");



my $start_time = time;
#my $sess_clear = '';

#$sess_clear = $cgi->param('sess_clear');

$session->param("narrow_search", "0");
$session->param("narrow_search_module", "search");

my $providerIds ="";  # Building String of Provider IDs Peer Group
my $action    = $cgi->param('action');    
my $provider  = $cgi->param('provider'); 
my $searchProvider  = $cgi->param('provider');
my $name      = $cgi->param('name'); 
my $address   = $cgi->param('address'); 
my $city      = $cgi->param('city');
my $states    = $cgi->param('state'); 
my $zip       = $cgi->param('zip');
my $county    = $cgi->param('county');
my $location  = $cgi->param('location');
my $radius    = $cgi->param('radius');
my $min_ftes  = $cgi->param('min_ftes');  
my $max_ftes  = $cgi->param('max_ftes');
my $min_beds  = $cgi->param('min_beds');
my $max_beds  = $cgi->param('max_beds');
my $toc       = $cgi->param('toc');        # type of control 
my $hosp_type = $cgi->param('HospType');  
my $system    = $cgi->param('system');  
my $msa       = $cgi->param('msa');
my $nfp       = $cgi->param('N_F_Profit');
my $teach_hosp= $cgi->param('Teaching_Hosp');
my $geoArea   = $cgi->param('Geographical_Area');
my $govt      = $cgi->param('Governmental');
my $min_net_income = $cgi->param('min_net_income');
my $max_net_income = $cgi->param('max_net_income');
# my $max_Zscore = $cgi->param('max_Zscore');  
# my $min_Zscore = $cgi->param('min_Zscore');
my $max_Opr_Margin = $cgi->param('max_Opr_Margin');
my $min_Opr_Margin = $cgi->param('min_Opr_Margin');

####################################################################################


# $session->param("nr_state",$states);
# $session->param("nr_ctype",$toc);
# $session->param("nr_msa",$msa);
# $session->param("nr_system",$system);
# $session->param("nr_htype",$hosp_type);
# $session->param("nr_thosp",$teach_hosp);
# $session->param("nr_geoarea",$geoArea);
# $session->param("nr_bedstart",$min_beds);
# $session->param("nr_bedend",$max_beds);
# $session->param("nr_ftestart",$min_ftes);
# $session->param("nr_fteend",$max_ftes);
# $session->param("nr_incomestart",$min_net_income);
# $session->param("nr_incomeend",$max_net_income);
# $session->param("nr_Zscoremin",$max_Zscore);
# $session->param("nr_Zscoremax",$min_Zscore);
# $session->param("nr_OprMarginmin",$max_Opr_Margin);
# $session->param("nr_OprMarginmax",$min_Opr_Margin);
# $session->param("nr_Within",$radius);
# $session->param("nr_p_c_Miles_Of_zip",$location);

# Display search criteria text instead values

my $stateText = $cgi->param('srcStateText');
my $systemText = $cgi->param('srcSystemText');
my $hospText = $cgi->param('srcHospText');
my $tocText = $cgi->param('srcTocText');
my $msaText = $cgi->param('srcMSAText');
my $nfpText = $cgi->param('srcNFPText');
my $teachText = $cgi->param('srcTeachText');
my $geoArText = $cgi->param('srcGeoText');
my $govtText  = $cgi->param('srcGovtText');

# Storing search criteria in session-jagadish .m

# my $sess_systxt = $session->param("systxt");
# my $sess_statetxt = $session->param("statetxt");
# my $sess_hosptxt = $session->param("hosptxt");
# my $sess_toctxt = $session->param("toctxt");
# my $sess_msatxt = $session->param("msatxt");
# my $sess_nfptxt = $session->param("nfptxt");
# my $sess_teachtxt = $session->param("teachtxt");
# my $sess_geotxt = $session->param("geotxt");
# my $sess_goctxt = $session->param("govttxt");

# my $sess_providerno = $session->param("providerno"); 
# my $sess_name = $session->param("name"); 
# my $sess_address = $session->param("address"); 
# my $sess_city = $session->param("city");  
# my $sess_zip = $session->param("zip"); 
# my $sess_county = $session->param("county"); 
# my $sess_location = $session->param("location"); 
# my $sess_radius = $session->param("radius"); 
# my $sess_min_ftes = $session->param("min_ftes"); 
# my $sess_max_ftes = $session->param("max_ftes"); 
# my $sess_min_beds = $session->param("min_beds"); 
# my $sess_max_beds = $session->param("max_beds"); 
# my $sess_toc = $session->param("toc", $toc); 
# my $sess_hosp_type = $session->param("hosp_type"); 
# my $sess_system = $session->param("system"); 
# my $sess_state = $session->param("system");
# my $sess_msa = $session->param("msa"); 
# my $sess_nfp = $session->param("nfp"); 
# my $sess_teach_hosp = $session->param("teach_hosp"); 
# my $sess_geoArea = $session->param("geoArea"); 
# my $sess_govt = $session->param("govt"); 
# my $sess_min_net_income = $session->param("min_net_income"); 
# my $sess_max_net_income = $session->param("max_net_income"); 
# my $sess_max_Zscore = $session->param("max_Zscore"); 
# my $sess_min_Zscore = $session->param("min_Zscore"); 
# my $sess_max_Opr_Margin = $session->param("max_Opr_Margin"); 
# my $sess_min_Opr_Margin = $session->param("min_Opr_Margin"); 

#my @restored_arry_msa = split(/,/,$sess_msa);
#my @restored_arry_sys = split(/,/,$sess_systxt);
#my @restored_arry_toc = split(/,/,$sess_toctxt);
#my @restored_arry_hosp = split(/,/,$sess_hosptxt);
#my @restored_arry_state = split(/,/,$sess_statetxt);

# if($sess_clear){	
# }else{
	# $sess_clear = '';
	# @restored_arry_msa = ();
# }

my $lstrStore=''; 
my $strStates ='';
my $t = 0;

#foreach my $lstate (@states){if($t>0){$strStates = $strStates .":".$lstate;}else{$strStates = $lstate;}}

my $strToc = '';
my $y = 0 ;

#foreach my $ltoc (@toc){if($y>0){$strToc = $strToc .":".$ltoc;}else{$strToc = $ltoc;}}

# strDisp is used to display the Peer Group in Overlay and strStore is used to insert in database.
# Sequence of fields is maintained as in the interface.

my $remote_user  = $ENV{'REMOTE_USER'};

my %toc_desc = (
  '1'  => 'Voluntary Nonprofit, Church',
  '2'  => 'Voluntary Nonprofit, Other',
  '3'  => 'Proprietary, Individual',
  '4'  => 'Proprietary, Corporation',
  '5'  => 'Proprietary, Partnership',
  '6'  => 'Proprietary, Other',
  '7'  => 'Governmental, Federal',
  '8'  => 'Governmental, City-County',
  '9'  => 'Governmental, County',
  '10' => 'Governmental, State',
  '11' => 'Governmental Hospital District',
  '12' => 'Governmental, City',
  '13' => 'Governmental, Other',
);

my %type_desc = (
  '1' => 'General Short Term ',
  '2' => 'General Long Term',
  '3' => 'Cancer',
  '4' => 'Psychiatric',
  '5' => 'Rehabilitation',
  '6' => 'Religious Non-medical Health Care Institution',
  '7' => 'Children',
  '8' => 'Alcohol and Drug',
  '9' => 'Other',
);


my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my $defaultYear= $ini_value{'DEFAULT_YEAR'};

my $dbh;

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}


#jayanth 19Aug09 - Get MSA values from Database. 
my %msaDetais = &getMSADetails();
my %statesDetails = &getStatesDetails();
my %controlType = &getControlTypeDetails();
my %hospitalType = &getHospTypeDetails();
my %geographicalArea = &getGeoAreaDetails(); 
my %teachingHospital = &getTeachingHospDetails();   
 
unless ($action eq 'go') {
   &display_form();
   exit(0);
} else {
   &query_database();
}

$dbh->disconnect();
exit(0);



#----------------------------------------------------------------------------

sub query_database
{
   my (@list, $row, $op, $n, $class, $string);
   my ($inlist, $i,$j);

   my @where_list = ();
   my @sql_args   = ();
   my @error_list;


   if ($min_ftes) {
      unless ($min_ftes =~ m/^\d+/) {
         push @error_list, "MINIMUM FTES MUST BE NUMERIC";
      }
   }

   if ($provider) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op h.ProviderNo = ?\n";
      push @sql_args, $provider;
   }

   if ($name) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op h.HospitalName LIKE ?\n";
      push @sql_args, $name;
   }

   if ($address) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op h.HospitalStreet LIKE ?\n";
      push @sql_args, $address;
   }

   if ($city) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op h.HospitalCity LIKE ?\n";
      push @sql_args, $city;
   }

   $n  = @where_list;
   $op = ($n) ? 'AND' : 'WHERE';

   #$i = @states;

   #if ($i) {
   #   unless ($states[0] eq '') {
   #      $inlist = join "\", \"", @states;
   #      push @where_list, "$op h.STATE IN (\"$inlist\")\n";
   #   }
   #}

 if ($stateText) {
    $n = @where_list;
    $op = ($n) ? 'AND' : 'WHERE';
    push @where_list, "$op h.HospitalState IN($stateText)\n";
    #push @sql_args, $state;
 }

   if ($zip) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op h.HospitalZipCode = ?\n";
      push @sql_args, $zip;
   }

   if ($county) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op h.HospitalCounty LIKE ?\n";
      push @sql_args, $county;
   }

   my $workSheetLeftJoin = '';
   if (($min_beds)||($max_beds)||($min_ftes)||($max_ftes)||($min_net_income)||($max_net_income) || ($nfp) ||($geoArea)||($teach_hosp))
   {
   	$workSheetLeftJoin = " left outer join CR_" . $defaultYear . "_RPT as rpt on rpt.PRVDR_NUM = h.ProviderNo ";
   }
   
   my $bedsLeftJoin = '';
   if (($min_beds)||($max_beds))
   {
   	$bedsLeftJoin = " left outer join CR_" . $defaultYear . "_DATA as dataBed on dataBed.CR_REC_NUM = rpt.RPT_REC_NUM ";
   	$n = @where_list;
	$op = ($n) ? 'AND' : 'WHERE';
      	push @where_list, "$op dataBed.CR_WORKSHEET = 'S300001' and dataBed.CR_LINE= '02500' and dataBed.CR_COLUMN = '0100' \n";
   }
   
   if ($min_beds) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op dataBed.CR_VALUE >= (select 0 + ? )\n";
      push @sql_args, $min_beds;
   }

   if ($max_beds) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op dataBed.CR_VALUE <= (select 0 + ? )\n";
      push @sql_args, $max_beds;
   }  
   
   my $ftesLeftJoin = '';
   if (($min_ftes)||($max_ftes))
   {
   	$ftesLeftJoin = " left outer join CR_" . $defaultYear . "_DATA as dataFte on dataFte.CR_REC_NUM = rpt.RPT_REC_NUM ";
   	$n = @where_list;
	$op = ($n) ? 'AND' : 'WHERE';
      	push @where_list, "$op dataFte.CR_WORKSHEET = 'S300001' and dataFte.CR_LINE= '01200' and dataFte.CR_COLUMN = '1000' \n";
   }
   
   if ($min_ftes) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op dataFte.CR_VALUE >= (select 0 + ? )\n";
      push @sql_args, $min_ftes;
   }

   if ($max_ftes) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op dataFte.CR_VALUE <= (select 0 + ? )\n";
      push @sql_args, $max_ftes;
   }   
   
   my $incomeLeftJoin = '';
   if (($min_net_income)||($max_net_income))
   {
   	$incomeLeftJoin = " left outer join CR_" . $defaultYear . "_DATA as dataIncome on dataIncome.CR_REC_NUM = rpt.RPT_REC_NUM ";
   	$n = @where_list;
	$op = ($n) ? 'AND' : 'WHERE';
      	push @where_list, "$op dataIncome.CR_WORKSHEET = 'G300000' and dataIncome.CR_LINE= '03100' and dataIncome.CR_COLUMN = '0100' \n";
   }
   
   if ($min_net_income) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op dataIncome.CR_VALUE >= (select 0 + ? )\n";
      push @sql_args, $min_net_income;
   }

   if ($max_net_income) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op dataIncome.CR_VALUE <= (select 0 + ? )\n";
      push @sql_args, $max_net_income;
   }
   
   if ($tocText) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
     push @where_list, "$op h.HMPControl IN ($tocText)\n";     
   }   
   
   # if ($min_Zscore) {
      # $n = @where_list;
      # $op = ($n) ? 'AND' : 'WHERE';
      # push @where_list, "$op p.Z_Score >= (select 0 + ? )\n";
      # push @sql_args, $min_Zscore;   
   # }
   
   # if ($max_Zscore) {
         # $n = @where_list;
         # $op = ($n) ? 'AND' : 'WHERE';
         # push @where_list, "$op p.Z_Score <= (select 0 + ? )\n";
         # push @sql_args, $max_Zscore;   
   # }
   
   if ($min_Opr_Margin) {
	 $n = @where_list;
	 $op = ($n) ? 'AND' : 'WHERE';
	 push @where_list, "$op h.Operating_Margin >= (select 0 + ? )\n";
	 push @sql_args, $min_Opr_Margin;   
   }

   if ($max_Opr_Margin) {
	    $n = @where_list;
	    $op = ($n) ? 'AND' : 'WHERE';
	    push @where_list, "$op h.Operating_Margin <= (select 0 + ? )\n";
	    push @sql_args, $max_Opr_Margin;   
   }
   
   my $hTypeLeftJoin = '';
   if ($hosp_type) {
      #$hTypeLeftJoin = " left outer join CR_" . $defaultYear . "_DATA as dataHtype on dataHtype.CR_REC_NUM = rpt.RPT_REC_NUM ";
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      #push @where_list, "$op dataHtype.CR_WORKSHEET = 'S200000' and dataHtype.CR_LINE= '01900' and dataHtype.CR_COLUMN = '0100' and dataHtype.CR_VALUE IN ($hospText)\n";
      #push @sql_args, $hosp_type;    
		push @where_list, "$op h.HMPFacilityType IN ($hospText)\n"; 
   }	
   
   #need to add left join part for MSA. 
   
   my $leftJoinMSA = '';
   
   if ($msa) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op msa.MSACode in ($msa)\n";
      #push @sql_args, $msa;
      $leftJoinMSA = " left outer join zipcodes as zip on zip.zipcode = h.ShortZip left outer join msa on msa.MSACode = zip.msa ";
   }

   if ($system) {   
      if(substr($system,1,4) eq 'none'){
	  $n = @where_list;
	  $op = ($n) ? 'AND' : 'WHERE';
	  push @where_list, "$op h.SytemID IS NULL\n";    
      }else{
      	$n = @where_list;
      	$op = ($n) ? 'AND' : 'WHERE';
      	push @where_list, "$op h.SystemID IN($systemText) \n";
      	#push @sql_args, $system;
      } 
   }
  # else
  # {
  #    	$n = @where_list;
  #    	$op = ($n) ? 'AND' : 'WHERE';
  #    	push @where_list, "$op h.SYSTEM IS NOT NULL\n";      	
  # }

#------------New fields----------------------------------------

my $nfpLeftJoin = '';
if ($nfp){
   	$nfpLeftJoin = " left outer join CR_" . $defaultYear . "_DATA as dataNFP on dataNFP.CR_REC_NUM = rpt.RPT_REC_NUM inner join control_type on control_type.Control_Type_id = h.TypeofControl";
   	$n = @where_list;
	$op = ($n) ? 'AND' : 'WHERE';
      	push @where_list, "$op dataNFP.CR_WORKSHEET = 'S200000' and dataNFP.CR_LINE= '01800' and dataNFP.CR_COLUMN = '0100' and control_type.Not_For_Profit = '$nfp'\n";
      	
      	if ($nfp eq 'T') {
      	
      		if(($govt eq 'T') || ($govt eq 'F'))
      		{
      			push @where_list, "and control_type.Governmental = '$govt'\n";
      		}      		
      	
      	}
}

my $teachinghosp = '';
if ($teach_hosp){
   	$teachinghosp = " left outer join CR_" . $defaultYear . "_DATA as dataTeach on dataTeach.CR_REC_NUM = rpt.RPT_REC_NUM";
   	$n = @where_list;
	$op = ($n) ? 'AND' : 'WHERE';
      	push @where_list, "$op dataTeach.CR_WORKSHEET = 'S200000' and dataTeach.CR_LINE= '02500' and dataTeach.CR_COLUMN = '0100' and dataTeach.cr_value = '$teach_hosp' \n";
}

my $geoArLeftJoin = '';
if ($geoArea){
   	$geoArLeftJoin = " left outer join CR_" . $defaultYear . "_DATA as dataGeoAr on dataGeoAr.CR_REC_NUM = rpt.RPT_REC_NUM";
   	$n = @where_list;
	$op = ($n) ? 'AND' : 'WHERE';
      	push @where_list, "$op dataGeoAr.CR_WORKSHEET = 'S200000' and dataGeoAr.CR_LINE= '02103' and dataGeoAr.CR_COLUMN = '0100' and dataGeoAr.cr_value = '$geoArea'\n";
}
#------------End of new fields---------------------------------
  
  my ($sql, $sth);

   my $geosql = '';

   if ($location && $radius) {

      my ($zip_sth, $lat1, $lon1);

      my $zip_sql = qq{
         SELECT city, state, latitude, longitude
           FROM zipcodes
          WHERE zipcode = ?
      };
   
      unless ($zip_sth = $dbh->prepare($zip_sql)) {
         &display_error('ERROR preparing SQL', $zip_sth->errstr); 
         exit(1);
      }

      unless ($zip_sth->execute($location)) {
         &display_error('ERROR executing SQL', $zip_sth->errstr); 
         exit(1);
      }

      my ($z_city, $z_state, $lat1, $lon1) = $zip_sth->fetchrow_array;

      unless ($lat1) {
         &display_error("NO COORDINATES AVAILABLE FOR $location"); 
         exit(1);
      }
         
      $zip_sth->finish;

      #---------------------------------------------------------------------

      $geosql = qq{
    	  ,3963.0 * acos(sin(radians($lat1)) * sin(radians(z.latitude))
 	  + cos(radians($lat1)) * cos(radians(z.latitude))
 	  * cos(radians(z.longitude) - radians($lon1))) as distance
      };

      push @where_list, "HAVING distance < $radius\n";
      push @where_list, "ORDER BY distance ASC\n";
   }

   $sql = qq{
   SELECT distinct h.ProviderNo, h.TypeofControl, h.HospitalName,
          h.HospitalStreet, h.HospitalCity, h.HospitalState, h.HospitalZipCode,
			 h.HospitalCounty,z.latitude as LATITUDE,z.longitude as LONGITUDE,sys.SystemName as SYSTEM_NAME,h.SystemID
			 $geosql 
     FROM tblhospitalmaster as h 
	 LEFT OUTER JOIN zipcodes as z on h.ShortZip=z.zipcode
    LEFT JOIN tblsystemmaster as sys ON sys.SystemID = h.SystemID
     $leftJoinMSA 
     $workSheetLeftJoin
     $bedsLeftJoin
     $ftesLeftJoin
     $incomeLeftJoin
     $hTypeLeftJoin
     $nfpLeftJoin
     $geoArLeftJoin
     $teachinghosp
     @where_list
   };

#     LIMIT 800

   unless ($sth = $dbh->prepare($sql)) {
       &display_error('ERROR preparing SQL', $sth->errstr);
       $dbh->disconnect();
       exit(1);
   }


   unless ($sth->execute(@sql_args)) {
       &display_error('ERROR executing SQL', $sth->errstr, $sql);
       $dbh->disconnect();
       exit(1);
   }
  
   my $rows_returned = $sth->rows();
   
   unless ($rows_returned) {
      &display_error(" Sorry, no matches were found. Please change the search criteria and try again.");
      $dbh->disconnect();
      return(0);
   }

   my $aref = $sth->{'NAME'};

   my @column_headings = @$aref;

   

}

#----------------------------------------------------------------------------

sub display_error
{
   my @list = @_;

   my ($s, $string);

   foreach $s (@list) {
      $s =~ s/\n/<BR \/>/g;
      $string .= "$s<BR>\n";
   }

   print "Content-Type: text/html\n\n";

   print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon" />
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css" />
<link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />
</HEAD>
<BODY>
<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/cgi-bin/demo_index.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/demo_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>Standard Reports</div>">
			        <img src="/images/stardardreport_inner_icon.gif" onmouseover="src='/images/stardardreport_inner_icon_hover.gif';"
                                    onmouseout="src='/images/stardardreport_inner_icon.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 102px" align="Center">
                            <a href="" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
			                                    <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->
<CENTER>
<BR>
<FONT SIZE="5" COLOR="RED">
<B></B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR ALIGN="LEFT">
      <TD>$string</TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
   };
}


#---------------------------------------------------------------------------

sub display_form
{
   my ($sth, $key);

   my $sql = "SELECT SystemID as SYSTEM_ID, SystemName as SYSTEM_NAME FROM tblsystemmaster ORDER BY SystemName";

   unless ($sth = $dbh->prepare($sql)) {
       &display_error('ERROR preparing SQL', $sth->errstr);
       $dbh->disconnect();
       exit(1);
   }

   unless ($sth->execute()) {
       &display_error('ERROR executing SQL', $sth->errstr);
       $dbh->disconnect();
       exit(1);
   }
   
   #------------------------------------Starts------------------------------------------
   # Build html table of rows of names along with check boxes and insert into divisions.
   # Each row has one hidden textbox which contains the 'text' of the item. 
   # And each check box value is value of that item
   # for example : for 'system' field Text = 'Adventist Health' value ='6'  
   #-------------------------------------------------------------------------------------

   #-- System
   
   my ($system_id, $system_name);

   my $system_options  = "<table width=\"100%\"><tr><td colspan=2 align=\"center\">-- Select Systems --</td></tr>\n";
   
   $system_options .= "<tr><td align=\"left\" style=\"width:25px;\"><input type=\"checkbox\" name=\"chksys\" value=\"none\"><input type=\"hidden\" name=\"sysString\" value=\"none\"></td><td align=\"left\">none</td></tr>\n";
   while (($system_id, $system_name) = $sth->fetchrow_array) {
      $system_options .= "<tr><td align=\"left\" style=\"width:25px;\"><input type=\"checkbox\" name=\"chksys\" value=\"$system_id\"><input type=\"hidden\" name=\"sysString\" value=\"$system_name\"></td><td align=\"left\">$system_name</td></tr>\n";
   }
   $system_options .= "</table>\n";
   $sth->finish();

   #-- Type of control

   my $toc_options  = "<table width=\"100%\"><tr><td colspan=2 align=\"center\">-- Select Types --</td></tr>\n";
      
   my $ind = 0;
   foreach $key (keys %controlType) {
      $ind++;	
      $toc_options .= "<tr><td align=\"left\" style=\"width:25px;\"><input type=\"checkbox\" name=\"chktoc\" value=\"$key\"><input type=\"hidden\" name=\"tocString\" value=\"$controlType{$key}\"></td><td align=\"left\">$controlType{$key}</td></tr>\n";      
   }
   
   $toc_options = $toc_options."</table>\n"; 
    
   #-- States
   
   my $statehtml = "<table width=\"100%\"><tr><td colspan=2 align=\"center\">-- Select States --</td></tr>";
       
   foreach $key(sort {$statesDetails{$a} cmp $statesDetails{$b} } keys %statesDetails){ 
    		        $statehtml .= "<tr><td align=\"left\" style=\"width:25px;\"><input type=\"checkbox\" name=\"chkState\" value=\"$key\"><input type=\"hidden\" name=\"statString\" value=\"$statesDetails{$key}\"></td><td align=\"left\">$statesDetails{$key}</td></tr>\n";    
             	}
                	
   $statehtml = $statehtml."</table>\n";  
  
  #-- Hospital type  
   
  my $hospType ="<table width=\"100%\"><tr><td colspan=2 align=\"center\">-- Select Types --</td></tr>";
  
  foreach $key(sort keys %hospitalType){ 
 		$hospType .= "<tr><td align=\"left\" style=\"width:25px;\"><input type=\"checkbox\" name=\"chkhosptype\" value=\"$key\"><input type=\"hidden\" name=\"hosptypeString\" value=\"$hospitalType{$key}\"></td><td align=\"left\">$hospitalType{$key}</td></tr>\n";
  	}
  
  $hospType .= "</table>\n";
	
  #-- MSA
 # my $a = @restored_arry_msa;
 
  my $msahtml = "<table width=\"100%\"><tr><td colspan=2 align=\"center\">-- Select MSA --</td></tr>\n";
   foreach $key(sort keys %msaDetais){  
   	$msahtml .= "<tr><td align=\"left\" style=\"width:25px;\"><input type=\"checkbox\" name=\"chkMsa\" value=\"$key\"><input type=\"hidden\" name=\"msaString\" value=\"$msaDetais{$key}\"></td><td align=\"left\">$msaDetais{$key}</td></tr>\n";	           		
   }

  $msahtml .= "</table>\n";
  #------------------------------------------Ends----------------------------------------------
   print "Content-Type: text/html\n\n";

   print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<HEAD><TITLE>COST REPORTS</TITLE>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon" />
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css" />
<link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />
<style>
.checkDiv{
	position:static;
	height:130px;
	width:400px;
	border:1px solid gray;
	overflow:scroll;
	overflow-x:hidden;
	background-color:#FFFFFF;
}
</style>
<script>
	function PageSubmit(){		
		
		//-- States
		if(document.frmSearch.chkState!=null){	
			var stateCount = 0 ;
			for(var i=0;i<document.frmSearch.chkState.length;i++){								
			    if(document.frmSearch.chkState[i].checked == true){
				  stateCount++;					  
				if(document.frmSearch.chkState[i].value!=null){
					if(stateCount>1){					
						document.frmSearch.srcStateText.value = document.frmSearch.srcStateText.value + ",'" + document.frmSearch.chkState[i].value+"'";					
						document.frmSearch.state.value = document.frmSearch.state.value + ",'" + document.frmSearch.statString[i].value +"'";
					}else{
						if(document.frmSearch.chkState[i]!=null){
							document.frmSearch.srcStateText.value = "'"+document.frmSearch.chkState[i].value+"'";	
						}else{
							document.frmSearch.srcStateText.value = "'"+document.frmSearch.chkState.value+"'";
						}
						if(document.frmSearch.statString[i]!=null){
							document.frmSearch.state.value = "'"+document.frmSearch.statString[i].value+"'";
						}else{
							document.frmSearch.state.value = "'"+document.frmSearch.statString.value+"'";
						}	
					}
				}else{
					document.frmSearch.srcStateText.value ="";
					document.frmSearch.state.value ="";
				}	
			     }	
			}
		}
		
		//--System
				
		if(document.frmSearch.chksys!=null){
			var sysCount = 0 ;
			for(var i=0;i<document.frmSearch.chksys.length;i++){				
				if(document.frmSearch.chksys[i].checked == true){	
					sysCount++;
					if(sysCount>1){					
						document.frmSearch.srcSystemText.value = document.frmSearch.srcSystemText.value + ",'" + document.frmSearch.chksys[i].value+"'";					
						document.frmSearch.system.value = document.frmSearch.system.value + ",'" + document.frmSearch.sysString[i].value+"'";
					}else{
						if(document.frmSearch.chksys[i]!=null){
							document.frmSearch.srcSystemText.value = "'" + document.frmSearch.chksys[i].value + "'";	
						}else{
							document.frmSearch.srcSystemText.value = "'" + document.frmSearch.chksys.value + "'";
						}
						if(document.frmSearch.sysString[i]!=null){
							document.frmSearch.system.value = "'" + document.frmSearch.sysString[i].value + "'";
						}else{
							document.frmSearch.system.value = "'" + document.frmSearch.sysString.value + "'";
						}	
					}
				}	
			}
		}		
		
		//--Type of control
		
		if(document.frmSearch.chktoc!=null){	
			var tocCount = 0;
			for(var i=0;i<document.frmSearch.chktoc.length;i++){				
				if(document.frmSearch.chktoc[i].checked == true){	
					tocCount++;
					if(tocCount>1){					
						document.frmSearch.srcTocText.value = document.frmSearch.srcTocText.value + "," + document.frmSearch.chktoc[i].value;					
						document.frmSearch.toc.value = document.frmSearch.toc.value + ",'" + document.frmSearch.tocString[i].value + "'";
					}else{
						if(document.frmSearch.chktoc[i]!=null){
							document.frmSearch.srcTocText.value = document.frmSearch.chktoc[i].value;	
						}else{
							document.frmSearch.srcTocText.value = document.frmSearch.chktoc.value;
						}
						if(document.frmSearch.tocString[i]!=null){
							document.frmSearch.toc.value = "'" + document.frmSearch.tocString[i].value + "'";
						}else{
							document.frmSearch.toc.value = "'" + document.frmSearch.tocString.value + "'";
						}	
					}
				}	
			}
		}
		
		// Hospial Type
		
		if(document.frmSearch.chkhosptype!=null){
			var hosptypeCount = 0;			
			for(var i=0;i<document.frmSearch.chkhosptype.length;i++){								
				if(document.frmSearch.chkhosptype[i].checked == true){						
					hosptypeCount++;
					if(hosptypeCount>1){					
						document.frmSearch.srcHospText.value = document.frmSearch.srcHospText.value + "," + document.frmSearch.chkhosptype[i].value;					
						document.frmSearch.HospType.value = document.frmSearch.HospType.value + "," + document.frmSearch.hosptypeString[i].value;
					}else{
						if(document.frmSearch.chkhosptype[i]!=null){
							document.frmSearch.srcHospText.value = document.frmSearch.chkhosptype[i].value;	
						}else{
							document.frmSearch.srcHospText.value = document.frmSearch.chkhosptype.value;
						}
						if(document.frmSearch.hosptypeString[i]!=null){
							document.frmSearch.HospType.value = document.frmSearch.hosptypeString[i].value;
						}else{
							document.frmSearch.HospType.value = document.frmSearch.hosptypeString.value;
						}	
					}
				}	
			}
		}	
		
		// MSA
				
		if(document.frmSearch.chkMsa!=null){	
			var msaCount = 0;
			for(var i=0;i<document.frmSearch.chkMsa.length;i++){				
				if(document.frmSearch.chkMsa[i].checked == true){	
					msaCount++;
					if(msaCount>1){					
						document.frmSearch.srcMSAText.value = document.frmSearch.srcMSAText.value + ",'" + document.frmSearch.msaString[i].value+"'";					
						document.frmSearch.msa.value = document.frmSearch.msa.value + "," + document.frmSearch.chkMsa[i].value;
					}else{
						if(document.frmSearch.chkMsa[i]!=null){
							document.frmSearch.srcMSAText.value = "'" + document.frmSearch.msaString[i].value + "'";	
						}else{
							document.frmSearch.srcMSAText.value = "'" + document.frmSearch.msaString.value + "'";
						}
						if(document.frmSearch.msaString[i]!=null){
							document.frmSearch.msa.value = document.frmSearch.chkMsa[i].value;
						}else{
							document.frmSearch.msa.value = document.frmSearch.chkMsa.value;
						}	
					}
				}	
			}
		}		
		
		if(document.frmSearch.N_F_Profit.selectedIndex>0){
			document.frmSearch.srcNFPText.value = document.frmSearch.N_F_Profit.options[document.frmSearch.N_F_Profit.selectedIndex].text;
		}
		if(document.frmSearch.Geographical_Area.selectedIndex>0){
			document.frmSearch.srcGeoText.value = document.frmSearch.Geographical_Area.options[document.frmSearch.Geographical_Area.selectedIndex].text;
		}
		if(document.frmSearch.Teaching_Hosp.selectedIndex>0){
			document.frmSearch.srcTeachText.value = document.frmSearch.Teaching_Hosp.options[document.frmSearch.Teaching_Hosp.selectedIndex].text;
		}
		if(document.frmSearch.Governmental.selectedIndex>0){
			document.frmSearch.srcGovtText.value = document.frmSearch.Governmental.options[document.frmSearch.Governmental.selectedIndex].text;
		}		
		
		document.frmSearch.submit();
	}
        
        function setGovt(param){
        	
        	if(param == "T"){
        		document.getElementById('Governmental').disabled = false;
        		
        	}else{
        		document.getElementById('Governmental').disabled = true;
        	}	
        }
        
        // On page load clear values if its not refine search criteria.
        
</script>
</HEAD>
<BODY>
<BR>
<CENTER>
<FORM NAME="frmSearch" METHOD="POST" ACTION="#">
<!--header start-->
};
    &innerHeader();
    
print qq{    
    <!--header end-->
    <!--content start-->    
    <table cellpadding="0" cellspacing="0" width="885px">
    <tr><td align="left">Welcome <b>
        	};
        	
        	print $firstName . " " . $lastName;
        	print qq{
        	</b></td>  
        	<td align="right">
        	<a href="">Sign Out</a>
        	</td>
    	</tr>
    </table>
    <table cellpadding="0" cellspacing="0" width="885px">
        <tr>
            <td valign="top">
                <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;
                text-align: left; padding: 10px">
                <TABLE BORDER="0" cellpadding="2" cellspacing="2">   
   <tr><TD COLSPAN="3" style="padding:10px">
         <FONT SIZE="3">
         <B>HOSPITAL SEARCH </B>
         </FONT>
      </TD><td></td><td align="left"> <INPUT TYPE="hidden" NAME="action" VALUE="go">
          <span class="button">  <INPUT TYPE="button" VALUE=" SEARCH " onclick=""></span>&nbsp;<span class="button"><INPUT TYPE="reset"  VALUE=" CLEAR "></span>
           
        </td></tr>
   <TR>
      <TD ALIGN="RIGHT"><strong>Provider</strong></TD><TD>
      <TD ALIGN="LEFT">
          <INPUT TYPE="text" class="textboxstyle"  class="textboxstyle"  NAME="provider" value="" SIZE="6" MAXLENGTH="6">
      </TD>
   </TR>
   <TR>
      <TD ALIGN="RIGHT"><strong>Name</strong></TD><Td><img src="/images/star_indicator.png"></td>
      <TD ALIGN="LEFT">
          <INPUT TYPE="text" class="textboxstyle"  class="textboxstyle"  NAME="name" SIZE="40" value="">
      </TD>
   </TR>
   <TR>
      <TD ALIGN="RIGHT"><strong>Address</strong></TD><Td><img src="/images/star_indicator.png"></td>
      <TD ALIGN="LEFT">
          <INPUT TYPE="text" class="textboxstyle"  class="textboxstyle"  NAME="address" SIZE="30" value="">
      </TD>
   </TR>
   <TR>
      <TD ALIGN="RIGHT"><strong>City</strong></TD><Td><img src="/images/star_indicator.png"></td>
      <TD ALIGN="LEFT">
          <INPUT TYPE="text" class="textboxstyle"  class="textboxstyle"  NAME="city" SIZE="30" value="">
      </TD>
   </TR>
   <TR>
      <TD ALIGN="RIGHT"><strong>State(s)</strong></TD><td></td>
      <TD ALIGN="LEFT">
      	<div ID="state" class="checkDiv">	
      	   $statehtml
       	</div>  
      </TD>
   </TR>
   <TR>
      <TD ALIGN="RIGHT"><strong>Zip Code</strong></TD><td></td>
      <TD ALIGN="LEFT">
          <INPUT TYPE="text" class="textboxstyle"  class="textboxstyle"  NAME="zip" SIZE="10" MAXLENGTH="10" value="">
      </TD>
   </TR>
   <TR>
      <TD ALIGN="RIGHT"><strong>County</strong></TD><Td><img src="/images/star_indicator.png"></td>
      <TD ALIGN="LEFT">
          <INPUT TYPE="text" class="textboxstyle"  NAME="county" SIZE="20" MAXLENGTH="20" value="">
      </TD>
   </TR>
   <TR>
      <TD ALIGN="RIGHT"><strong>System</strong></TD><Td></td>
      <TD ALIGN="LEFT">
         <div ID="sys" class="checkDiv">
	 	$system_options
	</div> 
      </TD>
   </TR>
   <TR>
      <TD ALIGN="RIGHT"><strong>Type of Control</strong></TD><Td></td>
      <TD ALIGN="LEFT">
	<div ID="toc" class="checkDiv">
		$toc_options
	</div>         
      </TD>
   </TR>
   <TR>
      <TD ALIGN="RIGHT"><strong>Hospital Type</strong></TD><Td></td>
      <TD ALIGN="LEFT">
      <div ID="HospType" class="checkDiv">
        	$hospType
        </div> 
      </TD>
   </TR>

      <TD ALIGN="RIGHT"><strong>MSA</strong></TD><Td></td>
      <TD ALIGN="LEFT">
      <div ID="HospType" class="checkDiv">
      		$msahtml
      </div>		
      <div style="float:left; width:100px; padding-left:10px"></div>
      </TD>
   </TR>
   <!--<TR>   
   	<TD ALIGN="RIGHT" ><strong>N F Profits</strong></TD>
   	<TD colspan="2">
   	<TABLE><TR>   
   	 <TD style="padding-left:10px;"></TD>
         <TD ALIGN="LEFT">
           <SELECT NAME="N_F_Profit" ID="N_F_Profit" onchange="setGovt(this.value)" class="textboxstyle">
               <OPTION VALUE="" SELECTED>-- ALL --</OPTION>
               <OPTION VALUE="T">True</OPTION>          	
               <OPTION VALUE="F">False</OPTION>            	
           </SELECT>
         </TD>
         <TD ALIGN="RIGHT" style="padding-left:60px;"><strong>Governmental</strong></TD>
	          <TD ALIGN="LEFT">
	            <SELECT NAME="Governmental" ID="Governmental" class="textboxstyle" disabled>
	                <OPTION VALUE="" SELECTED>-- ALL --</OPTION>
	                <OPTION VALUE="T">True</OPTION>          	
	                <OPTION VALUE="F">False</OPTION>            	
	            </SELECT>
         </TD>
         </TR>
         </TABLE>
         </TD>
   </TR>-->
    <TR>
            <TD ALIGN="RIGHT"><strong>Teaching Hospital</strong></TD><Td></td>
            <TD ALIGN="LEFT">
              <SELECT NAME="Teaching_Hosp" ID="Teaching_Hosp"  class="textboxstyle">
                  <OPTION VALUE="" SELECTED>-- ALL --</OPTION>
              };
              	foreach $key(sort keys %teachingHospital){
      	    	  print "<OPTION VALUE=\"$key\">$teachingHospital{$key}</OPTION>\n";   
      	    	}
      	    	print qq{
              </SELECT>
            </TD>
   </TR>
   <TR>
            <TD ALIGN="RIGHT"><strong>Geographical Area</strong></TD><Td></td>
            <TD ALIGN="LEFT">
              <SELECT NAME="Geographical_Area" ID="Geographical_Area"  class="textboxstyle">
                  <OPTION VALUE="" SELECTED>-- ALL Geographical Area --</OPTION>
               };
               foreach $key(sort keys %geographicalArea){
                  print "<OPTION VALUE=\"$key\">$geographicalArea{$key}</OPTION>\n";
               }
               print qq{
              </SELECT>
            </TD>
   </TR>
   <TR>
      <TD ALIGN="RIGHT"><strong>Beds</strong></TD><Td></td>
      <TD ALIGN="LEFT">
          BETWEEN <INPUT TYPE="text" class="textboxstyle"  NAME="min_beds" SIZE="4" MAXLENGTH="4" value="">
          AND <INPUT TYPE="text" class="textboxstyle"  class="textboxstyle"  NAME="max_beds" SIZE="4" MAXLENGTH="4" value="">
      </TD>
   </TR>
   <TR>
      <TD ALIGN="RIGHT"><strong>FTEs</strong></TD><Td></td>
      <TD ALIGN="LEFT">
          BETWEEN <INPUT TYPE="text" class="textboxstyle"  NAME="min_ftes" SIZE="4" MAXLENGTH="4" value="">
          AND <INPUT TYPE="text" class="textboxstyle"  NAME="max_ftes" SIZE="4" MAXLENGTH="4" value="">
      </TD>
   </TR>
   <TR>
      <TD ALIGN="RIGHT"><strong>Net Income</strong></TD><Td></td>
      <TD ALIGN="LEFT">
          BETWEEN <INPUT TYPE="text" class="textboxstyle"  NAME="min_net_income" SIZE="9" MAXLENGTH="12" value="">
          AND <INPUT TYPE="text" class="textboxstyle"  NAME="max_net_income" SIZE="9" MAXLENGTH="12" value="">
      </TD>
   </TR>
   <TR>
      <TD ALIGN="RIGHT"><strong>Within</strong></TD><Td></td>
      <TD ALIGN="LEFT"><div style="float:left">
         <SELECT NAME="radius" class="textboxstyle">
	    <OPTION VALUE=''> </OPTION>
            <OPTION VALUE=15>15</OPTION>
            <OPTION VALUE=30>30</OPTION>
            <OPTION VALUE=60>60</OPTION>
            <OPTION VALUE=90>90</OPTION>
            <OPTION VALUE=120>120</OPTION>
          </SELECT></div><div style="float:left; padding-left:10px; padding-top:5px">
         <strong>Miles of Zip Code:</strong></div><div style="float:left; padding-left:10px">
             <INPUT TYPE="text" class="textboxstyle"  NAME="location" TITLE="5 DIGIT ZIPCODE TO CENTER SEARCH FROM" SIZE="5" MAXLENGTH="5" value=""></div>
            <div style="float:left; padding-left:10px"></div>
      </TD>
    </TR>    
 <!--  <TR>
         <TD ALIGN="RIGHT"><strong>Z Score</strong></TD><Td></td>
         <TD ALIGN="LEFT">
             BETWEEN <INPUT TYPE="text" class="textboxstyle"  NAME="min_Zscore" SIZE="9" MAXLENGTH="12" value="">
             AND <INPUT TYPE="text" class="textboxstyle"  NAME="max_Zscore" SIZE="9" MAXLENGTH="12" value="">
         </TD>
   </TR>-->
   <TR>
            <TD ALIGN="RIGHT"><strong>Operating Margin</strong></TD><Td></td>
            <TD ALIGN="LEFT">
                BETWEEN <INPUT TYPE="text" class="textboxstyle"  NAME="min_Opr_Margin" SIZE="9" MAXLENGTH="12" value="">
                AND <INPUT TYPE="text" class="textboxstyle"  NAME="max_Opr_Margin" SIZE="9" MAXLENGTH="12" value="">
            </TD>
   </TR>
   <tr><td style="padding-top:10px; padding-bottom:20px"></td></tr>
       <TR><td></td><td><img src="/images/star_indicator.png"></td>
          <TD align="left">
            <FONT COLOR="RED" SIZE="2">WILD CARD MATCHES SUPPORTED WITH THE "%" CHARACTER</FONT>
        </TD>
   </TR>
   <tr><td></td><td></td><td align="left"> <INPUT TYPE="hidden" NAME="action" VALUE="go">
       <span class="button">   <INPUT TYPE="button" VALUE=" SEARCH " onclick=""></span>&nbsp;<span class="button"><INPUT TYPE="reset"  VALUE=" CLEAR "></span>
        
        </td></tr>
</TABLE></td>
            <td valign="top">
                <img src="/images/content_right.gif" /></td>
        </tr>
    </table>
    <!--content end-->};
		&TDdoc;
		print qq{
<input type="hidden" name="srcStateText" >
<input type="hidden" name="srcSystemText" >
<input type="hidden" name="srcHospText" >
<input type="hidden" name="srcTocText" >
<input type="hidden" name="srcMSAText" >
<input type="hidden" name="srcNFPText" >
<input type="hidden" name="srcGeoText" >
<input type="hidden" name="srcTeachText" >
<input type="hidden" name="srcGovtText" >

<input type="hidden" name="toc"  >
<input type="hidden" name="HospType"  >
<input type="hidden" name="system"  >
<input type="hidden" name="state"  >
<input type="hidden" name="msa"  >
<input type="hidden" name="N_F_Profit"  >
<input type="hidden" name="Governmental"  >

<BR><script></script>
</FORM>
</CENTER>
</BODY>
</HTML>
   };
}


#------------------------------------------------------------------------------

sub comify
{
   local($_) = shift;
   1 while s/^(-?\d+)(\d{3})/$1,$2/;
   return($_);
}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#------------------------------------------------------------------------------

# - Get details from MSA table. 

sub getMSADetails
{
	my ($qry, $stmt, $record);
	
	$qry = "select * from MSA order by MSAName";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();   	
   	
   	my %msa;   	
   	
	while ($record = $stmt->fetchrow_hashref) {	
		$msa{$$record{MSACode}} = $$record{MSAName};		
  	}  	
  	return %msa;
}

#--------------------------------------------------------------------------------

# - Get details from States table. 

sub getStatesDetails
{
	my ($qry, $stmt, $record);
	
	$qry = "select * from states order by state_name";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();   	
   	
   	my %state;   	
   	
	while ($record = $stmt->fetchrow_hashref) {	
		$state{$$record{state}} = $$record{state_name};		
  	}  	
  	return %state;
}

#--------------------------------------------------------------------------------

# - Get details from Type of Control table. 

sub getControlTypeDetails
{
	my ($qry, $stmt, $record);
	
	$qry = "select * from control_type order by Control_Type_Name";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();   	
   	
   	my %cType;   	
   	
	while ($record = $stmt->fetchrow_hashref) {	
		$cType{$$record{Control_Type_id}} = $$record{Control_Type_Name};		
  	}
  	return %cType;
}

#--------------------------------------------------------------------------------

# - Get details from Hospital Type table. 

sub getHospTypeDetails
{
	my ($qry, $stmt, $record);
	
	$qry = "select * from hospital_type order by hospital_type_id";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();   	
   	
   	my %hType;   	
   	
	while ($record = $stmt->fetchrow_hashref) {	
		$hType{$$record{Hospital_Type_id}} = $$record{Hospital_Type_Name};				
  	}  	  	
  	return %hType;
}

#--------------------------------------------------------------------------------------
# - Get details from Geographical Area table. 

sub getGeoAreaDetails
{
	my ($qry, $stmt, $record);
	
	$qry = "select * from mastertables where MasterType = 'Geoagraphical Area'";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();   	
   	
   	my %gType;   	
   	
	while ($record = $stmt->fetchrow_hashref) {	
		$gType{$$record{Value}} = $$record{Name};				
  	}  	  	
  	return %gType;
}

#--------------------------------------------------------------------------------------
# - Get details from Teaching Hospital table. 

sub getTeachingHospDetails                        
{
	my ($qry, $stmt, $record);
	
	$qry = "select * from mastertables where MasterType = 'Teaching Hospital'";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();   	
   	
   	my %tType;   	
   	
	while ($record = $stmt->fetchrow_hashref) {	
		$tType{$$record{Value}} = $$record{Name};				
  	}  	  	
  	return %tType;
}

#--------------------------------------------------------------------------------------


#--------------------------------------------------------------------------------

#  Table: HOSPITALS  Wildcard: %
# +-----------------+--------------+-------------------+------+-----+
# | Field           | Type         | Collation         | Null | Key |
# +-----------------+--------------+-------------------+------+-----+
# | PROVIDER_NUMBER | char(6)      | latin1_swedish_ci | NO   | PRI |
# | FYB             | date         |                   | YES  |     |
# | FYE             | date         |                   | YES  |     |
# | STATUS          | varchar(15)  | latin1_swedish_ci | YES  |     |
# | CTRL_TYPE       | char(2)      | latin1_swedish_ci | YES  |     |
# | HOSP_NAME       | varchar(45)  | latin1_swedish_ci | YES  |     |
# | STREET_ADDR     | varchar(45)  | latin1_swedish_ci | YES  |     |
# | PO_BOX          | varchar(30)  | latin1_swedish_ci | YES  |     |
# | CITY            | varchar(38)  | latin1_swedish_ci | YES  |     |
# | STATE           | varchar(11)  | latin1_swedish_ci | YES  |     |
# | ZIP_CODE        | varchar(10)  | latin1_swedish_ci | YES  |     |
# | COUNTY          | varchar(35)  | latin1_swedish_ci | YES  |     |
# | URBAN1_RURAL    | char(2)      | latin1_swedish_ci | YES  |     |
# | LATITUDE        | decimal(8,5) |                   | YES  |     |
# | LONGITUDE       | decimal(8,5) |                   | YES  |     |
# | GEOACC          | char(1)      | latin1_swedish_ci | YES  |     |
# +-----------------+--------------+-------------------+------+-----+

# Table: HS2006_RPT  Wildcard: %
#+--------------------+---------------+-------------------+------+
#| Field              | Type          | Collation         | Null |
#+--------------------+---------------+-------------------+------+
#| RPT_REC_NUM        | int(11)       |                   | NO   |
#| PRVDR_CTRL_TYPE_CD | char(2)       | latin1_swedish_ci | YES  |
#| PRVDR_NUM          | char(6)       | latin1_swedish_ci | NO   
#| NPI                | decimal(10,0) |                   | YES  |
#| RPT_STUS_CD        | char(1)       | latin1_swedish_ci | NO   |
#| FY_BGN_DT          | date          |                   | YES  |
#| FY_END_DT          | date          |                   | YES  |
#| PROC_DT            | date          |                   | YES  |
#| INITL_RPT_SW       | char(1)       | latin1_swedish_ci | YES  |
#| LAST_RPT_SW        | char(1)       | latin1_swedish_ci | YES  |
#| TRNSMTL_NUM        | char(2)       | latin1_swedish_ci | YES  |
#| FI_NUM             | char(5)       | latin1_swedish_ci | YES  |
#| ADR_VNDR_CD        | char(1)       | latin1_swedish_ci | YES  |
#| FI_CREAT_DT        | date          |                   | YES  |
#| UTIL_CD            | char(1)       | latin1_swedish_ci | YES  |
#| NPR_DT             | date          |                   | YES  |
#| SPEC_IND           | char(1)       | latin1_swedish_ci | YES  |
#| FI_RCPT_DT         | date          |                   | YES  |
#+--------------------+---------------+-------------------+------+

