#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: Registration.pl
#  author: Jayanth Raj
#----------------------------------------------------------------------------

use strict;
use CGI;
use DBI;

use CGI::Session;
do "pagefooter.pl";
do "common_header.pl";
print "Content-Type: text/html\n\n";
do "audit.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $role = $session->param("role");
my $facility;
$session->clear(["narrow_search", "narrow_search_module", "providerid", "peersearch_sql", "peersearch_sql_args"]);
$session->param("narrow_search_module", "state");
$session->param("narrow_search", "0");
my $narrow_search = $session->param("narrow_search");
my $narrow_search_module = $session->param("narrow_search_module");

if(!$userID)
{
	&sessionExpire;
}
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>HMP Metrics</title>
	<link href="/css/main.css" rel="stylesheet" type="text/css" />
	<link href="/css/tabview.css" rel="stylesheet" type="text/css" />
	<script language="javascript" type="text/javascript" src="/JS/tabview.js"></script>
</head>
<body>
    <table cellpadding="0" cellspacing="0" width="740px" align="center">
        <tr>
            <td align="center" style="padding-top: 10px; padding-bottom: 10px; ">
                <a href="http://www.hcmpllc.com">
                    <img border="0" src="/images/logo.gif"></a>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" width="750px" align="center">
    	<tr><td style="font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px;">Welcome <b>
    	};

    	print $firstName . " " . $lastName;
    	print qq{

    	</b>
    	<a href="/cgi-bin/login.pl?saction=signOut">(Sign Out)</a>
    	</td>
		<td  align="right" style="padding-right:5px;" class="gridHlink">
    		<font size="2"><a href="/cgi-bin/changePassword.pl">Change Password</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="/cgi-bin/Profile.pl">Profile</a></font>&nbsp;&nbsp;&nbsp;
			};
			if($role eq '2')
    		{
    			print qq{
				<font size="2" color="#2e7738"> <a href="/cgi-bin/AdminPanel.pl">Admin Panel</a>
    			};
    		}
				print qq{
			&nbsp;&nbsp;&nbsp;<a href="http://www.hcmpllc.com" target="_new">About Us</a>
			</font></td>
    	</tr>
	</table>
	};
		&mainHeader();
	print qq{
 <br>
    <table cellpadding="0" cellspacing="0" width="750px" align="center">
    	<tr>
    	 		};

    		# old link to About Us was "team.html"
    		print qq{

    	</tr>
        <tr>
            <td valign="top">
                <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;
                text-align: center; padding: 10px">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td valign="top" align="left">
                            <form method="GET" action="/cgi-bin/choose1.pl">
                                <table border="0" cellspacing="0" cellpadding="2">
                                    <tr>
                                        <td>
                                            <img src="/icons/CMSLogo.gif" width="140" height="50"><br>
                                            <br>
                                            <font size="2" color="#2e7738"><b>COST REPORT</b><br>
                                            </font>
                                        </td>
                                    </tr>
									<tr>
                                        <td>
                                            <select name="facility" title="CHOOSE FACILITY" class="styled" style="width: 175px">
                                                <option value='hospital' selected>HOSPITAL</option>
                                                <option value='snf' >SNF</option>
                                                <option value='renal'>RENAL FACILITY</option>
                                                <option value='hospice'>HOSPICE</option>
                                                <option value='hha'>HHA</option>

                                            </select>
                                        </td>
									</tr>
                                    <tr>
                                        <td style="padding-top: 10px">
                                            <strong>Provider:</strong></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="text" name="provider" size="6" maxlength="6" title="ENTER 6 DIGIT PROVIDER NUMBER"
                                                style="width: 165px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="hidden" name="action" value="go">
                                            <span class="button" style="height:28px;width:16px;background-color:#ffffff">
                                                <input type="submit" value=" LIST REPORTS " style="width: 167px"></span>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            <form method="GET" action="">
                                <table border="0" cellspacing="0" cellpadding="0" width="200px">

                                    <tr>
				       <td valign="top">
						<br />
						<br />
				      <span valign="top" class="button" style="height:28px;width:16px;padding-top:0px;background-color:#ffffff"><input type="button" value="HMP Metrics " onClick="window.location.href='/cgi-bin/Milo2/index.pl'" style="width: 167px"></span>
				       </td>
                          </tr>
                                </table>
                            </form>
                        </td>
						<input type="hidden" name="narrow_search" value="$narrow_search" >
						<input type="hidden" name="narrow_search_module" value ="$narrow_search_module">
                        <td valign="top" align="center">
                             <div class="TabView" id="TabView">
                <div class="Tabs" style="overflow:hidden">
                    <a>Hospital</a><a>SNF</a><a>Renal Facility</a><a>Hospice</a><a>HHA</a>
                </div>
                <div class="Pages" style=" height:398px ; overflow:hidden">
                    <!-- *** Page1 Start *** -->
                    <div class="Page" style="overflow:hidden" id="page">
                        <div class="Pad" style="overflow:hidden">
                        <center>
						<table>
                                <tr>
                                    <td width="3%">
										<div style="font-size: 12pt; padding-top: 5px">
                               <font color=#0C4F7A> <strong>Medicare Inpatient Claim Data for Hospital</strong></font>
							   <br>
							    <br>

                            };
									&state_map('hospital');
									print qq{   </div></td>
                               </tr>
                        </table>
						</center>
                        </div>
                    </div>
                    <!-- *** Page1 end *** -->
                    <!-- *** Page2 Start *** -->

                    <div class="Page" style="overflow:hidden" id="page">
                        <div class="Pad" style="overflow:hidden">

                        <table>
                          <tr>
                                    <td width="3%">
										<div style="font-size: 12pt; padding-top: 5px">
                                 <font color=#0C4F7A><strong>Medicare Inpatient Claim Data for SNF</strong></font>
								 <br>
								  <br>


                            };
									&state_map('snf');
									print qq{   </div></td>

                          </tr>

                      </table>

                        </div>
                    </div>
                    <!-- *** Page2 End ***** -->
					<!-- *** Page3 Start *** -->
                    <div class="Page" style="overflow:hidden" id="page">
                        <div class="Pad" style="overflow:hidden">

                        <center>
						<table>
                                <tr>
                                    <td width="3%">
									<div style="font-size: 12pt; padding-top: 5px">
                                <font color=#0C4F7A><strong>Medicare Inpatient Claim Data for Renal Facility</strong></font>
								<br>
								 <br>

                            };
									&state_map('renal');
									print qq{   </div></td>

                                </tr>
                        </table>
						</center>

                        </div>
                    </div>
                    <!-- *** Page3 end *** -->
					 <!-- *** Page4 Start *** -->
                    <div class="Page" style="overflow:hidden" id="page">
                        <div class="Pad" style="overflow:hidden">
						<table>
                                <tr>
                                    <td width="3%">
									<div style="font-size: 12pt; padding-top: 5px">
                                 <font color=#0C4F7A><strong>Medicare Inpatient Claim Data for Hospice</strong></font>
								 <br>
								 <br>

                            };
									&state_map('hospice');
									print qq{   </div></td>

                                </tr>
                        </table>
						</center>

                        </div>
                    </div>
                    <!-- *** Page4 end *** -->
                    <!-- *** Page5 Start *** -->
                    <div class="Page" style="overflow:hidden" id="page" >
                        <div class="Pad" style="overflow:hidden">

                        <center>
						<table>
                                <tr>
                                    <td>
									<div style="font-size: 12pt; padding-top: 5px">
                                   <font color=#0C4F7A><strong>Medicare Inpatient Claim Data for HHA</strong></font>
								   <br>
								    <br>

                            };
									&state_map('hha');
									print qq{   </div></td>

                                </tr>
                        </table>
						</center>
                        </div>
                    </div>
                </div>
            </div>
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="top">
                <img src="/images/content_right.gif" /></td>
        </tr>
    </table>	};
	&TDdoc;
print qq{
 <script>
				tabview_initialize('TabView');

            </script>
</body>
</html>
};

#--------------------------------------------------------------------------------------
sub sessionExpire
{

print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

#----------------------------------------------------------------
sub state_map($_){
$facility=shift;
print qq{
<div>
<img src="/icons/states_map.gif" usemap="#map$facility" border="0">
		<map name="map$facility">
			<area shape="POLY" alt="Washington" coords="53,0,94,10,87,42,71,40,52,38,43,36,41,28,36,24,38,2,54,11"
				href="/cgi-bin/peersearch.pl?state='WA'&facility=$facility">
			<area shape="POLY" alt="Idaho" coords="100,12,94,10,88,42,88,49,77,87,125,95,130,66,128,64,115,67,110,52,106,52,106,47,109,40,106,39,99,22"
				href="/cgi-bin/peersearch.pl?state='ID'&facility=$facility">
			<area shape="POLY" alt="North Dakota" coords="240,28,245,62,187,59,190,24" href="/cgi-bin/peersearch.pl?state='ND'&facility=$facility">
			<area shape="POLY" alt="Minnesota" coords="296,36,270,32,259,30,256,24,255,28,239,29,246,87,287,86,275,74,273,63,277,59,278,50"
				href="/cgi-bin/peersearch.pl?state='MN'&facility=$facility">
			<area shape="POLY" alt="South Dakota" coords="242,97,244,98,246,86,243,61,185,59,183,91"
				href="/cgi-bin/peersearch.pl?state='SD'&facility=$facility">
			<area shape="POLY" alt="Wyoming" coords="181,112,184,69,131,62,124,106" href="/cgi-bin/peersearch.pl?state='WY'&facility=$facility">
			<area shape="POLY" alt="Nevada" coords="102,90,89,156,82,154,80,167,45,113,54,80"
				href="/cgi-bin/peersearch.pl?state='NV'&facility=$facility">
			<area shape="POLY" alt="Oregon" coords="34,28,42,29,44,36,63,39,87,42,90,47,78,86,20,71,21,59,27,54,35,33"
				href="/cgi-bin/peersearch.pl?state='OR'&facility=$facility">
			<area shape="POLY" alt="California" coords="74,192,78,192,83,175,80,166,44,112,54,81,20,71,14,88,29,161,44,171,54,183,54,189"
				href="/cgi-bin/peersearch.pl?state='CA'&facility=$facility">
			<area shape="POLY" alt="Arizona" coords="134,153,126,216,107,213,75,195,74,192,83,175,83,155,89,156,91,146"
				href="/cgi-bin/peersearch.pl?state='AZ'&facility=$facility">
			<area shape="POLY" alt="Colorado" coords="198,114,140,108,134,153,161,158,195,159"
				href="/cgi-bin/peersearch.pl?state='CO'&facility=$facility">
			<area shape="POLY" alt="Utah" coords="102,91,124,96,124,105,140,108,134,152,91,146"
				href="/cgi-bin/peersearch.pl?state='UT'&facility=$facility">
			<area shape="POLY" alt="Iowa" coords="286,86,288,93,295,102,295,108,289,112,286,122,250,121,249,112,244,98,246,88"
				href="/cgi-bin/peersearch.pl?state='IA'&facility=$facility">
			<area shape="POLY" alt="Wisconsin" coords="277,52,287,50,313,62,313,73,318,68,314,98,292,98,288,90,287,84,275,75,274,66,273,63,277,58"
				href="/cgi-bin/peersearch.pl?state='WI'&facility=$facility">
			<area shape="POLY" alt="Missouri" coords="286,121,288,130,294,136,298,138,298,147,305,152,306,158,308,160,305,170,299,170,300,166,261,167,259,135,256,132,257,128,252,125,251,121"
				href="/cgi-bin/peersearch.pl?state='MO'&facility=$facility">
			<area shape="POLY" alt="Nebraska" coords="182,91,182,112,198,115,197,127,254,127,248,108,243,97"
				href="/cgi-bin/peersearch.pl?state='NE'&facility=$facility">
			<area shape="POLY" alt="Kansas" coords="260,161,195,159,198,126,254,127,258,130,256,132,259,136"
				href="/cgi-bin/peersearch.pl?state='KS'&facility=$facility">
			<area shape="POLY" alt="Montana" coords="100,11,122,17,158,23,190,24,184,70,131,62,130,68,127,65,115,66,110,52,106,52,106,47,108,40,102,32,100,23"
				href="/cgi-bin/peersearch.pl?state='MT'&facility=$facility">
			<area shape="POLY" alt="New Mexico" coords="187,158,182,215,149,211,148,214,133,212,133,216,125,216,134,153,160,157"
				href="/cgi-bin/peersearch.pl?state='NM'&facility=$facility">
			<area shape="POLY" alt="Oklahoma" coords="187,164,212,166,212,186,221,193,235,196,244,197,257,197,262,199,264,177,260,161,186,159"
				href="/cgi-bin/peersearch.pl?state='OK'&facility=$facility">
			<area shape="POLY" alt="Illinois" coords="314,98,317,106,321,140,318,148,318,152,314,156,314,158,310,156,306,158,305,153,304,150,298,146,298,143,298,138,294,138,287,127,286,120,288,118,289,111,294,108,296,104,296,101,292,98"
				href="/cgi-bin/peersearch.pl?state='IL'&facility=$facility">
			<area shape="POLY" alt="Arkansas" coords="304,170,301,180,295,193,293,204,266,206,266,199,262,199,264,180,262,167,300,165,299,166,299,170"
				href="/cgi-bin/peersearch.pl?state='AR'&facility=$facility">
			<area shape="POLY" alt="Texas" coords="188,164,184,214,151,211,151,214,154,219,161,226,164,228,167,236,167,239,179,248,186,241,190,239,198,241,207,255,209,260,212,261,214,266,215,271,219,278,226,282,235,283,236,285,237,282,235,278,233,277,234,270,235,264,237,263,238,260,244,252,253,252,259,246,259,240,261,241,265,244,271,239,272,234,273,228,270,221,269,217,267,216,267,199,260,197,257,194,245,196,232,195,223,192,215,190,212,186,213,173,213,166"
				href="/cgi-bin/peersearch.pl?state='TX'&facility=$facility">
			<area shape="POLY" alt="Michigan" coords="292,52,303,48,308,42,307,46,306,50,310,48,314,50,318,52,322,51,331,49,333,51,338,51,339,54,335,57,336,59,346,62,348,68,348,73,344,80,345,81,347,81,350,77,352,75,357,84,358,88,355,92,354,96,350,102,341,104,341,102,325,104,327,102,328,95,328,89,326,86,325,77,326,75,326,70,327,67,329,67,329,70,332,69,332,64,333,63,334,58,334,56,329,55,327,56,324,57,322,59,319,59,318,58,315,62,313,69,312,62,308,59,298,56,293,54"
				href="/cgi-bin/peersearch.pl?state='MI'&facility=$facility">
			<area shape="POLY" alt="Indiana" coords="343,136,343,128,341,104,341,101,323,104,322,107,318,106,320,133,320,138,318,144,318,148,318,149,322,147,330,147,334,144,334,145,340,138"
				href="/cgi-bin/peersearch.pl?state='IN'&facility=$facility">
			<area shape="POLY" alt="Ohio" coords="374,95,362,103,357,104,350,101,340,104,344,130,347,133,350,135,360,136,364,136,366,137,369,131,372,126,374,125,377,118,378,115,378,112,375,111,377,108"
				href="/cgi-bin/peersearch.pl?state='OH'&facility=$facility">
			<area shape="POLY" alt="Kentucky" coords="370,146,365,142,365,137,360,135,352,136,349,134,348,132,344,134,344,136,340,138,337,142,335,145,332,146,327,148,321,150,318,151,314,157,309,158,309,163,307,165,318,164,320,162,360,157,367,152"
				href="/cgi-bin/peersearch.pl?state='KY'&facility=$facility">
			<area shape="POLY" alt="Tennessee" coords="375,156,342,160,319,163,318,164,306,166,301,181,305,181,353,176,357,171,375,159"
				href="/cgi-bin/peersearch.pl?state='TN'&facility=$facility">
			<area shape="POLY" alt="Louisiana" coords="307,226,291,227,290,221,296,210,292,204,267,206,267,216,270,223,271,228,272,233,270,239,274,240,281,242,285,242,287,239,291,240,292,242,296,244,300,246,301,244,303,244,305,246,305,244,307,242,308,243,314,246,310,242,308,240,313,241,310,237,306,236,301,236,304,234,308,233,308,231"
				href="/cgi-bin/peersearch.pl?state='LA'&facility=$facility">
			<area shape="POLY" alt="Mississippi" coords="320,180,319,200,319,219,321,230,309,234,306,225,290,226,290,219,294,213,295,209,293,205,294,201,294,196,296,191,302,180"
				href="/cgi-bin/peersearch.pl?state='MS'&facility=$facility">
			<area shape="POLY" alt="Alabama" coords="342,178,352,210,350,213,352,217,353,222,328,225,329,227,331,230,328,232,324,227,322,229,321,232,320,230,319,199,320,180"
				href="/cgi-bin/peersearch.pl?state='AL'&facility=$facility">
			<area shape="POLY" alt="Georgia" coords="364,176,341,178,352,210,350,214,352,217,353,224,356,227,380,223,382,226,382,225,382,221,384,220,385,221,388,202,382,196,373,186,370,184,368,182,363,179"
				href="/cgi-bin/peersearch.pl?state='GA'&facility=$facility">
			<area shape="POLY" alt="Florida" coords="384,222,388,228,392,234,396,240,398,246,400,251,403,255,406,259,407,263,406,270,406,276,402,280,398,276,395,272,392,272,389,267,385,262,382,261,382,255,381,251,380,250,378,243,376,239,370,236,367,233,366,231,358,231,355,235,351,236,350,234,346,230,339,230,332,230,330,228,328,226,328,224,352,222,355,226,380,223,382,225"
				href="/cgi-bin/peersearch.pl?state='FL'&facility=$facility">
			<area shape="POLY" alt="South Carolina" coords="406,181,404,185,402,190,399,195,394,199,389,199,388,204,383,196,379,193,376,190,374,187,371,186,369,184,367,181,365,180,363,180,364,176,369,172,376,172,382,171,385,172,386,174,391,174,394,172,397,174,403,179"
				href="/cgi-bin/peersearch.pl?state='SC'&facility=$facility">
			<area shape="POLY" alt="North Carolina" coords="424,148,425,152,419,156,428,155,426,160,422,160,419,160,421,164,422,167,420,171,416,174,406,182,396,174,388,175,382,171,377,172,368,173,364,176,354,177,358,171,371,162,375,156"
				href="/cgi-bin/peersearch.pl?state='NC'&facility=$facility">
			<area shape="POLY" alt="West Virginia" coords="402,119,397,125,392,132,388,134,384,143,384,147,374,149,370,148,365,143,366,137,366,132,372,125,376,120,378,117,380,120,388,118,389,124,396,118"
				href="/cgi-bin/peersearch.pl?state='WV'&facility=$facility">
			<area shape="POLY" alt="Washington, D.C." coords="404,119,403,122,406,124,407,121">
			<area shape="POLY" alt="Virginia" coords="425,147,422,144,419,142,417,139,418,135,415,131,410,128,408,125,402,121,398,121,396,125,390,131,388,134,386,142,384,147,375,149,370,148,362,157,372,157,391,154"
				href="/cgi-bin/peersearch.pl?state='VA'&facility=$facility">
			<area shape="POLY" alt="Pennsylvania" coords="382,90,374,96,377,109,376,112,379,120,422,110,424,108,425,106,424,104,422,100,423,95,424,93,420,91,419,89,416,87,382,94"
				href="/cgi-bin/peersearch.pl?state='PA'&facility=$facility">
			<area shape="POLY" alt="Vermont" coords="438,46,440,55,437,62,437,70,437,73,431,75,429,66,427,58,426,50"
				href="/cgi-bin/peersearch.pl?state='VT'&facility=$facility">
			<area shape="POLY" alt="New Hampshire" coords="437,73,450,70,451,68,447,62,442,44,439,44,439,54,437,63"
				href="/cgi-bin/peersearch.pl?state='NH'&facility=$facility">
			<area shape="POLY" alt="Massachusetts" coords="431,83,448,79,451,83,455,81,450,75,448,70,432,75"
				href="/cgi-bin/peersearch.pl?state='MA'&facility=$facility">
			<area shape="POLY" alt="Maryland" coords="418,111,421,122,425,129,424,132,420,127,415,119,415,124,418,132,416,134,407,124,406,120,402,118,396,118,390,121,390,118"
				href="/cgi-bin/peersearch.pl?state='MD'&facility=$facility">
			<area shape="POLY" alt="Delaware" coords="421,110,424,118,426,121,421,122,419,117"
				href="/cgi-bin/peersearch.pl?state='DE'&facility=$facility">
			<area shape="POLY" alt="New Jersey" coords="430,96,432,100,431,105,430,111,426,120,422,114,421,110,426,106,422,102,421,97,424,92"
				href="/cgi-bin/peersearch.pl?state='NJ'&facility=$facility">
			<area shape="POLY" alt="New York" coords="385,79,388,83,386,86,381,91,381,93,416,86,422,92,428,94,431,95,432,99,436,98,443,92,438,93,433,94,433,92,433,88,431,81,431,72,429,64,427,58,425,52,415,52,408,60,406,67,406,74,401,78,394,77,388,77"
				href="/cgi-bin/peersearch.pl?state='NY'&facility=$facility">
			<area shape="POLY" alt="Connecticut" coords="441,80,441,88,432,90,432,84" href="/cgi-bin/peersearch.pl?state='CT'&facility=$facility">
			<area shape="POLY" alt="Rhode Island" coords="445,80,446,83,446,86,442,89,441,80"
				href="/cgi-bin/peersearch.pl?state='RI'&facility=$facility">
			<area shape="POLY" alt="Maine" coords="450,67,452,59,458,52,460,46,462,47,464,47,470,41,472,39,467,33,464,28,462,23,460,18,456,15,454,13,450,17,446,24,446,30,446,34,444,41,442,44,445,55,446,60"
				href="/cgi-bin/peersearch.pl?state='ME'&facility=$facility">
			<area shape="POLY" alt="Hawaii" coords="146,259,144,262,152,261,163,268,170,268,174,274,175,274,180,281,181,286,186,286,190,284,186,278,184,277,181,272,176,270,172,267,168,268,167,265,164,264,162,266,152,261,152,258,149,258"
				href="/cgi-bin/peersearch.pl?state='HI'&facility=$facility">
			<area shape="POLY" alt="Alaska" coords="109,227,83,226,73,224,64,228,61,236,57,236,53,240,63,250,62,252,57,249,50,254,52,260,57,262,64,261,65,265,62,268,55,271,50,278,52,285,57,290,61,290,62,296,69,298,74,296,73,302,66,311,51,319,21,332,70,313,84,300,86,294,94,281,92,291,99,290,103,283,115,284,132,288,137,290,152,303,159,304,164,300,163,296,159,294,155,292,141,280,134,285,128,282,121,281"
				href="/cgi-bin/peersearch.pl?state='AK'&facility=$facility">
			<area shape="RECT" alt="Vermont" coords="409,36,425,47" href="/cgi-bin/peersearch.pl?state='VT'&facility=$facility">
			<area shape="RECT" alt="New Hampshire" coords="423,26,438,36" href="/cgi-bin/peersearch.pl?state='NH'&facility=$facility">
			<area shape="RECT" alt="Massachusetts" coords="465,68,482,78" href="/cgi-bin/peersearch.pl?state='MA'&facility=$facility">
			<area shape="RECT" alt="Rhode Island" coords="464,81,476,90" href="/cgi-bin/peersearch.pl?state='RI'&facility=$facility">
			<area shape="RECT" alt="New Jersey" coords="442,102,457,113" href="/cgi-bin/peersearch.pl?state='NJ'&facility=$facility">
			<area shape="RECT" alt="Delaware" coords="442,114,459,124" href="/cgi-bin/peersearch.pl?state='DE'&facility=$facility">
			<area shape="RECT" alt="Maryland" coords="442,124,461,134" href="/cgi-bin/peersearch.pl?state='MD'&facility=$facility">
			<area shape="RECT" alt="Washington, D.C." coords="443,136,459,145">
			<area shape="RECT" alt="Hawaii" coords="188,296,202,307" href="/cgi-bin/peersearch.pl?state='HI'&facility=$facility">
			<area shape="RECT" alt="Connecticut" coords="463,92,483,102" href="/cgi-bin/peersearch.pl?state='CT'&facility=$facility">
		</map>
		</div>
	};
}