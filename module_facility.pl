#!/usr/bin/perl

use strict;
use CGI;
use DBI;
my %ini_value;
my $path;
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $provider = $cgi->param("provider");
my $providerid = $session->param("providerid");
my $facility = $cgi->param("facility");
my $nrfac= $session->param("nr_facility");
unless (&read_inifileN('milo.ini')) {
   exit(1);
}
my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my $module;
my ($dbh, $sql, $sth, $row);
unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   exit(1);
}
sub header{
my $fac = ucfirst($facility);
$path = "$ENV{'SCRIPT_NAME'}";
$sql = "select module_name from module_facility where file_name='$path'";
$sth = $dbh->prepare($sql);
$sth->execute();  
 while ($row = $sth->fetchrow_hashref){
	$module = $$row{module_name};
 }
 print qq{	
<table bgColor=\"#b4cdcd\" width="885px" align=\"centre\">
<tr>
};
if(!$facility){
$fac=$nrfac
}
if(!$provider){
$provider=$providerid
}
print qq{
	<td><font color="#0C4F7A" size = "2"> <b>Module : $module </b> &nbsp; &nbsp; &nbsp;<b>Facility : $fac </b></td>
	  </tr>
          <tr>
              <td>
                  <ul class="myMenu">
	<li><a href="/cgi-bin/Milo2/profile_report.pl?provider=$provider&status=1&facility=$fac">Profile</a></li>
        <li><a href="/cgi-bin/Milo2/finantial_report.pl">Financial</a></li>
    <li><a href="/cgi-bin/Milo2/trend_report.pl">&nbsp; Trend </a></li>
    <li><a href="/cgi-bin/Milo2/trend_appendix_report.pl">Trend Appendix</a></li>
    <li><a href="#">Quartile Appendix</a>
    	<ul>
            <li><a href="/cgi-bin/Milo2/search.pl">Generate Peer Group</a></li>
            <li><a href="/cgi-bin/PeerMain.pl?facility=$fac">Custom Peer Group</a></li>
        </ul>
    </li>
    <li><a href="#">Patient Info</a>
       <ul>
            <li><a href="/cgi-bin/drg_mix.pl?provider=$provider&facility=$fac">DRG Mix</a></li>
            <li><a href="/cgi-bin/derek3.pl?provider=$provider&facility=$fac">Patient Source</a></li>
        </ul>
    </li>
    <li><a href="/cgi-bin/choose1.pl?provider=$provider&facility=$fac">Cost Reports</a></li>
</ul>
              </td>	

</tr>
	</table>
	<br>
	};
	}
1;	
#-------------------------------------------------------------------------------------
sub read_inifileN
{
   my ($fname) = @_;
   my ($line, $key, $value);
   open INIFILE, $fname or return 0;
   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }
   close INIFILE;
   return 1;
}
