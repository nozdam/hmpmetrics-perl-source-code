#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: cr_explore.pl
#  author: Steve Spicer
#  Written: Wed Oct 24 20:14:32 CDT 2007
#
#  Research Data Assistance Center
#  http://www.resdac.umn.edu/Tools/TBs/TN-005-revised.asp
#----------------------------------------------------------------------------

use strict;
use CGI;
use DBI;
do "pagefooter.pl";
do "common_header.pl";
print "Content-Type: text/html\n\n";
&headerScript();
use CGI::Session;
do "audit.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $prov = $session->param("str");
my $rpt_no = $session->param("sstr");
my $facility = $cgi->param("facility");
my ($name, $address, $city, $state, $zip,$sth1,$sth2);
if(!$userID)
{
	&sessionExpire;
}
&view("View","Cost report details","Cost Report");
my $action    = $cgi->param('action');
my $worksheet = $cgi->param('worksheet');
my $reportid  = $cgi->param('reportid');
my $provider = $cgi->param('provider');
my %ini_value;

my %status_lookup = (
 1 => 'As Submitted',
 2 => 'Settled w/o Audit',
 3 => 'Settled with Audit',
 4 => 'Reopened',
 5 => 'Amended',
);

unless ($reportid) {
   &display_error('REPORT ID NOT SPECIFIED');
   exit(0);
}

my $start_time = time;
my ($dbh, $sql, $sth,$sql_f,$facility_id,$master_table,$report_table,$data_table,$row);
unless (&read_inifile('milo.ini')) {
   &display_error('CAN\'T READ milo.ini');
   exit(1);
}
my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

$sql_f = "select facility_id ,master_table,report_table,data_table from tbl_facility_type where facility_name = '$facility'";
$sth = $dbh->prepare($sql_f);
$sth->execute();

while ($row = $sth->fetchrow_hashref) {
	$facility_id = $$row{facility_id};
	$master_table = $$row{master_table};
	$report_table = $$row{report_table};
	$data_table = $$row{data_table};
}

#----------------------------------------------------------------------------



my ($line, $key, $string, %hash,@dir);
  my %table_def;

my $wd_sql = qq{
Select cr_worksheet,definition from tbl_cost_report_definition where facility_type=$facility_id
};
 unless ($sth = $dbh->prepare($wd_sql)) {
       &display_error("Error preparing sql query", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   $sth->execute();

 while(($key,$string) = $sth->fetchrow_array){

             $hash{$key} = $string;

   }


my $w_sql = qq{
select distinct cr_worksheet from tbl_cost_report_definition as cr_dd
inner join tbl_cost_report_definition_detail as crd on
crd.definition_id = cr_dd.definition_id
where cr_dd.facility_type=$facility_id
};
 unless ($sth = $dbh->prepare($w_sql)) {
       &display_error("Error preparing sql query", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   $sth->execute();

 while(($string) = $sth->fetchrow_array){
         push(@dir,$string);
   }
foreach $string (@dir) {
   if ($string =~ m/^(\S{7})/) {   # look for def files
      $table_def{$1} = 'Y';
   }
}

#----------------------------------------------------------------------------

my ($rpt_status, $rpt_year, $rpt_provider, $rpt_stus_cd, $fi_num, $fy_bgn_dt, $fy_end_dt);

$sql = qq{
   SELECT year(FY_END_DT) as RPT_YEAR, PRVDR_NUM, RPT_STUS_CD, FI_NUM,
          DATE_FORMAT(FY_BGN_DT, '%m-%d-%Y'),
          DATE_FORMAT(FY_END_DT, '%m-%d-%Y')
     FROM $report_table
    WHERE RPT_REC_NUM = ?
};

unless ($sth = $dbh->prepare($sql)) {
   &display_error("Error preparing SQL query", $sth->errstr);
   $dbh->disconnect();
   exit(0);
}

unless ($sth->execute($reportid)) {
   &display_error("Error executing SQL query", $sth->errstr);
   $dbh->disconnect();
   exit(0);
}

($rpt_year, $rpt_provider, $rpt_stus_cd, $fi_num, $fy_bgn_dt, $fy_end_dt) = $sth->fetchrow_array;

$rpt_status = $status_lookup{$rpt_stus_cd};

$sql = qq{
  SELECT Name,address,
City ,State,Zip
 FROM $master_table
 WHERE ProviderNo in ($provider)
};

unless ($sth1 = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL query:", $sth1->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth1->execute()) {
   &display_error("ERROR executing SQL query:", $sth1->errstr);
   $dbh->disconnect;
   exit(1);
}



($name,$address,$city,$state,$zip) = $sth1->fetchrow_array;

if ($worksheet) {
   &explore_worksheet($reportid, $worksheet);
} else {
   &explore_report($reportid);
}

#$dbh->disconnect();

exit(0);

#----------------------------------------------------------------------------

sub explore_report
{
   my ($reportid) = @_;
#AND RPT_YEAR		= $rpt_year
   my ($worksheet, $value_count, $hover, $desc, $other, $link, $class);

   my $count = 0;

   my ($sth, $sql);

   $sql = qq/
    SELECT CR_WORKSHEET, COUNT(*)
      FROM $data_table BB
     WHERE CR_REC_NUM  = ?
  GROUP BY CR_WORKSHEET
   /;

   unless ($sth = $dbh->prepare($sql)) {
       &display_error('Error preparing SQL query: ', $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sth->execute($reportid)) {
       &display_error('Error executing SQL query: ', $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sth->rows()) {
     &display_error("NOTHING AVAILABLE FOR REPORTID: $reportid YEAR: $rpt_year");
     return(0);
   }
   &print_page_top();
   print qq{
<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0" WIDTH="885px" class="gridstyle">
   <TR CLASS="gridheader">
     <TD ALIGN="CENTER"><B>Worksheet</B></TD>
     <TD ALIGN="LEFT"><B>Description</B></TD>
     <TD ALIGN="RIGHT"><B>Values</B></TD>
     <TD ALIGN="CENTER"><B>Detailed<BR>View</B></TD>
	 <TD ALIGN="CENTER"><B>Download<br>Worksheet</B></TD>
   </TR>
   };

   my ($xlink, $xlsfile);

   while (($worksheet, $value_count) = $sth->fetchrow_array) {
		$sql = qq/
		select cost_report_form_link from
		tbl_cost_report_definition where
		cr_worksheet = ?
		and facility_type = $facility_id
		/;
		$sth2 = $dbh->prepare($sql);
		$sth2->execute($worksheet);
		($xlsfile) = $sth2->fetchrow_array;

      $desc = $hash{$worksheet};

      $xlink = "<A HREF=\"cr_explore_xls.pl?facility=$facility&reportid=$reportid&provider=$provider&cr_worksheet=$worksheet\" TITLE=\"Download In Excel\"><IMG BORDER=\"0\" SRC=\"/icons/excel_icon.gif\"></A>";

      $link = "<A HREF=\"/cgi-bin/cr_explore.pl?reportid=$reportid&worksheet=$worksheet&facility=$facility&provider=$provider\">$value_count</A>";

      $desc = '&nbsp;' unless ($desc);

      $class = (++$count % 2) ? 'gridrow' : 'gridalternate';

      if ($table_def{$worksheet} eq 'Y') {
         $other = "<A HREF=\"/cgi-bin/cr_table.pl?reportid=$reportid&worksheet=$worksheet&facility=$facility&provider=$provider\"><IMG BORDER=\"0\" SRC=\"/icons/table_icon.jpg\"></A>";
      } else {
         $other = '';
      }

      print "   <TR CLASS=\"$class\">\n";
      print "      <TD ALIGN=\"CENTER\">$worksheet</TD>\n";
      print "      <TD ALIGN=\"LEFT\">$desc</TD>\n";
      print "      <TD ALIGN=\"RIGHT\">$link</TD>\n";
      print "      <TD ALIGN=\"CENTER\">$other</TD>\n";
	  print "      <TD ALIGN=\"CENTER\">$xlink</TD>\n";
      print "   </TR>\n";
   }

   print qq{
</TABLE>
<FONT SIZE="1">
$count ROWS<BR>
</FONT>
};
&TDdoc;
print qq{
</CENTER>
</BODY>
</HTML>
   };

   return(0);
}

#----------------------------------------------------------------------------

sub explore_worksheet
{
   my ($reportid, $worksheet) = @_;
#AND RPT_YEAR		= $rpt_year
   my ($address, $row, $col, $value, $class, $align, $editlink);

   my $desc = $hash{$worksheet};

   my $count = 0;

   my ($sth, $sql);

   $sql = qq/
       SELECT CR_LINE, CR_COLUMN, CR_VALUE, KB_DESC
         FROM $data_table
    LEFT JOIN KB_TABLE ON KB_ADDRESS = CONCAT(CR_WORKSHEET, ':', CR_LINE, ':', CR_COLUMN)
        WHERE CR_REC_NUM    = ?
          AND CR_WORKSHEET  = ?
		   AND facility_type = $facility_id
    /;


   unless ($sth = $dbh->prepare($sql)) {
      &display_error("Error preparing sql query", $sth->errstr);
      $dbh->disconnect();
      exit(0);
   }

   unless ($sth->execute($reportid, $worksheet)) {
       &display_error("Error preparing sql query", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sth->rows()) {
     # &display_error("NOTHING AVAILABLE FOR REPORTID: $reportid WORKSHEET: $worksheet");
     # return(0);
   }

   &print_page_top();

   print qq{
<BR>
<TABLE BORDER="0" CELLPADDING="6" CELLSPACING="0" WIDTH="885px" class="gridstyle">
   <TR CLASS="gridheader">
     <TD ALIGN="CENTER"><B>ADDRESS</B></TD>
     <TD ALIGN="CENTER"><B>DESCRIPTION IF POPULATED</B></TD>
     <TD ALIGN="CENTER"><B>LINE</B></TD>
     <TD ALIGN="CENTER"><B>COLUMN</B></TD>
     <TD ALIGN="CENTER"><B>VALUE</B></TD>
   </TR>
   };

   while (($row, $col, $value, $desc) = $sth->fetchrow_array) {

      $address = "$worksheet:$row:$col";

      if ($row =~ m/^(\d\d\d)(\d\d)/) {
         $row = "$1.$2";
         $row =~ s/^0+//;
         $row =~ s/\.0+$//;
         $row = '0' if ($row eq '');
      }

      if ($col =~ m/^(\d\d)(\d\d)/) {
         $col = "$1.$2";
         $col =~ s/\.0+$//;
         $col =~ s/^0+//;
         $col = '0' if ($col eq '');
      }

      $align = 'LEFT';

      if ($value =~ m/^-?\d+$|-?\d*\.\d+$/) {
         $align = 'RIGHT';
#        $value = &comify($value);
      }

      $editlink = "<A HREF=\"#\" onClick=\"MyWindow=window.open('/cgi-bin/kb_update.pl?address=$address&facility=$facility&reportid=$reportid&worksheet=$worksheet','MyWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=600,height=450,left=25,top=25'); return false;\">$address</A>";

      $class = (++$count % 2) ? 'gridrow' : 'gridalternate';

      print "   <TR CLASS=\"$class\">\n";
      print "      <TD ALIGN=\"CENTER\">$editlink</TD>\n";
      print "      <TD ALIGN=\"LEFT\">$desc</TD>\n";
      print "      <TD ALIGN=\"CENTER\">$row</TD>\n";
      print "      <TD ALIGN=\"CENTER\">$col</TD>\n";
      print "      <TD ALIGN=\"$align\">$value</TD>\n";
      print "   </TR>\n";
   }

   print qq{
</TABLE>
<FONT SIZE="1">
$count ROWS<BR>
</FONT>
};
&TDdoc;
print qq{
</CENTER>
</BODY>
</HTML>
   };

   return(0);
}
#window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';
#----------------------------------------------------------------------------
sub CR_dropdown
{
#print "Content-Type: text/html\n\n";
my $rpt= substr $prov,1;
my @rptprov = split(/,/, $rpt);
my $rpt_list =	substr $rpt_no,1;
my @rpt_id = split(/,/, $rpt_list);
print qq{

<script>
function opt_select(){
var opt=document.frmcrexplorer.CR.options[document.frmcrexplorer.CR.selectedIndex].value;
window.parent.location.href="/cgi-bin/cr_explore.pl?reportid="+opt+"&action=go&facility=$facility&provider=$provider";
}
</script>


<form name="frmcrexplorer">
<table align="left">
<tr>
<td>
<strong>Cost Reports </strong>
	<select name="CR" title="CHOOSE A COST REPORT" onchange="opt_select()">
	<option >Select CR end date</option>
	};

		for(my $i=0;$i< scalar(@rptprov);$i++){
			print "<option name=\"Report\" value=".@rpt_id[$i].">".@rptprov[$i]."</option>";
		}



	print qq{
	</select>
	</td>
	</tr>
	</table>
	</form>
	};
}
#-------------------------------------------------------------------------------
sub print_page_top
{
   $worksheet = 'ALL' unless ($worksheet);
   print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
<HEAD><TITLE>COST REPORT EXPLORER</TITLE>
</HEAD>
<BODY>
<CENTER>
<BR>
};

&innerHeader();

print qq{
    <!--content start-->

        <table cellpadding="0" cellspacing="0" width="885px">
            <tr>
                <!--<td valign="top">
                    <img src="/images/content_left.gif" /></td>-->
            <td valign="top" style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px">
<TABLE BORDER="0" CELLPADDING="2" WIDTH="100%">
   <TR>
     <TD ALIGN="LEFT" VALIGN="TOP">

        <Div class="pageheader">$name</div>
      <span class="tahoma12">
        $address<BR>
        $city, $state  $zip<BR><BR>
        </B></span>

     </TD>
     <TD ALIGN="left" VALIGN="TOP" class="tahoma12" style="border-left:dotted 1px #ffffff; padding-left:10px;">
       WORKSHEET:<BR>
        PROVIDER:<BR>
        FY BEGIN:<BR>
          FY END:<BR>
          STATUS:<BR>
              FI:<BR>
     </TD>
     <TD ALIGN="LEFT" VALIGN="TOP" class="tahoma12" style="border-right:dotted 1px #ffffff; padding-right:10px">
        <B>
        <FONT COLOR="#EE0000">
        $worksheet<BR>
        $rpt_provider<BR>
        $fy_bgn_dt<BR>
        $fy_end_dt<BR>
        $rpt_status<BR>
        $fi_num<BR>
        </FONT>
        </B>
     </TD>
     <TD ALIGN="RIGHT" VALIGN="TOP">
        <IMG SRC="/icons/CMSLogo.gif">
     </TD>
   </TR>

   <tr>
   <td>};
   &CR_dropdown;
   print qq{
   </td>
   </tr>
</TABLE>
</td>
            <!--<td valign="top">
                <img src="/images/content_right.gif" /></td>-->
        </tr>
    </table>
    <!--content end-->
};
}

#----------------------------------------------------------------------------

sub get_value_by_addr
{
   my ($year, $reportid, $address) = @_;
# AND RPT_YEAR		= $year
   my ($worksheet, $row, $col) = split /[-:\|]/,$address;

   my ($sth, $sql);

   $sql = qq/
    SELECT CR_VALUE
      FROM $data_table BB
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?

   /;

   unless ($sth = $dbh->prepare($sql)) {
       &display_error("Error preparing sql query", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   $sth->execute($reportid, $worksheet, $row, $col);

   my ($value) = $sth->fetchrow_array;

   return($value);
}


#----------------------------------------------------------------------------

sub display_error
{
   my @list = @_;

   my ($string, $s);

   foreach $s (@list) {
      $string .= "$s<BR>\n";
   }

   print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
<HEAD><TITLE>ERROR</TITLE>

<link href="/css/cost_report/ergosign.styleform.css" media="all" type="text/css" rel="stylesheet">

    <script type="text/javascript" src="/JS/mootools-1.2-core.js"></script>

    <script type="text/javascript" src="/JS/cost_report/ergosign.styleform.js"></script>

    <script type="text/javascript" src="/JS/script.js"></script>
</HEAD>
<BODY><a style="display:scroll;position:fixed;left:5px;bottom:5px;" href="#" title="Feedback Please"><img border=0 src="/images/feedback.png"/></a>
<CENTER>
};
&innerHeader();
print qq{
<BR>

    <!--content start-->
    <table cellpadding="0" cellspacing="0" width="885px">
        <tr><td align="left">Welcome
            	};

            	print $firstName . " " . $lastName;
            	print qq{
            	</td>
            	<td align="right">
            	<a href="/cgi-bin/login.pl?saction=signOut">Sign Out</a>
            	</td>
        	</tr>
    </table>
        <table cellpadding="0" cellspacing="0" width="885px">
            <tr>
                <td valign="top">
                    <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px">
<FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<TABLE CLASS="error" WIDTH="600">
   <TR>
      <TD ALIGN="CENTER">
          <FONT SIZE="4">
          <BR>
          $string
          <BR>
          </FONT>
      </TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</td>
            <td valign="top">
                <img src="/images/content_right.gif" /></td>
        </tr>
    </table>
    <!--content end-->
	};

&TDdoc;
print qq{
</CENTER>
</BODY>
</HTML>
   };

   return(0);
}


#----------------------------------------------------------------------------

sub comify
{
  local($_) = shift;
  1 while s/^(-?\d+)(\d{3})/$1,$2/;
  return($_);
}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#--------------------------------------------------------------------------------------
sub sessionExpire
{


print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}


#------------------------------------------------------------------------------

