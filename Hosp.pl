#!/usr/bin/perl -X

#----------------------------------------------------------------------------
#  program: Registration.pl
#  author: Jayanth Raj
#----------------------------------------------------------------------------

use strict;
use CGI;
use DBI;

# use Session;

use CGI::Session;


my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $role = $session->param("role");

my $action = $cgi->param("upAct");

open (J,">data.txt");
print J $action;
close J;
my $msg = $cgi->param("msg");

#if(!$userID)
#{
#	&sessionExpire;
#}


my $dbh;
my ($sql, $sth, $key, $record, $valZ,$valOM, $i, @outer, $data);

my ($sqlU, $sthU, $recordU);

# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my $defaultYear= $ini_value{'DEFAULT_YEAR'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}


my %hospitalDetails = &getHospitalDetails();


print "Content-Type: text/html\n\n";

print qq{
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<title>PROJECT MILO Edit Profile...</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="/css/register/main.css" rel="stylesheet" type="text/css">
<link href="/css/register/qm.css" rel="stylesheet" type="text/css">
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css" />
<link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />

</head>
<body>
<FORM NAME="frmHospi" METHOD="POST" ACTION="/cgi-bin/Hosp.pl?upAct=update">
<center><br><!---->
<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/cgi-bin/index.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/cr_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="/cgi-bin/cr_batchx.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="/cgi-bin/snf_batch1.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="/cgi-bin/standard_reports.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Standard Reports</div>">
                                <img src="/images/stardardreport_inner_icon.gif" onmouseover="src='/images/stardardreport_inner_icon_hover.gif';"
                                    onmouseout="src='/images/stardardreport_inner_icon.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 102px" align="Center">
                            <a href="/cgi-bin/PeerMain.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
			                                    <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->
    
    <!--content start-->    
    <br>
    <div id='divData' style='height:400px;width:885px;overflow:auto;' > 
    <table cellpadding="0" cellspacing="0" width="860px" border="1" style="border:solid 1px #b1bed0;color:#0c4f7a;" > };
    	$sql = "select distinct h.PROVIDER_NUMBER as PROVIDER_NUMBER, h.HOSP_NAME as HOSP_NAME, h.Z_Score as Z_Score, h.Operating_Margin as Operating_Margin, rpt.RPT_REC_NUM as RPT_REC_NUM, rpt.RPT_YEAR as RPT_YEAR, rpt.FY_BGN_DT as BGN_DT, rpt.FY_END_DT as END_DT from Hospitals as h left outer join CR_ALL_RPT as rpt on rpt.PRVDR_NUM = h.PROVIDER_NUMBER and RPT_YEAR = 2008 order by h.HOSP_NAME";
	$sth = $dbh->prepare($sql);
	$sth->execute(); 
	$i = 1;		
	print "<tr> <td><B>Sl No</B></td> <td><B>Provider No</B></td> <td><B>Provider Name </B></td> <td align=right><B> Operating_Margin</B></td> <td align=right><B>Z Score</B></td> <td><B>FY_BGN</B></td>  <td><B>FY_END</B></td>  <td align=right><B>New Operating_Margin</B></td> <td  align=right><B>New Z Score</B></td> </tr> ";
	while ($record = $sth->fetchrow_hashref) {
		print "<tr> <td>";
		print $i;
		print "</td> <td>";
		print $$record{PROVIDER_NUMBER};
		print "</td> <td>";
		print $$record{HOSP_NAME};		
		print "</td> <td align=right>";
		print $$record{Operating_Margin};
		print "</td> <td align=right>";
		print $$record{Z_Score};		
		print "</td> <td>";
		print $$record{BGN_DT};
		print "</td> <td>";
		print $$record{END_DT};
		print "</td> <td align=right>";		
		if($$record{RPT_YEAR})
		{
			$valOM = &getFormulae($$record{RPT_REC_NUM}, $$record{RPT_YEAR}, "((G300000-00300-0100 - G300000-00400-0100) / G300000-00300-0100) * 100");
			print $valOM;
		}
		print "</td> <td  align=right>";
		if($$record{RPT_YEAR})
		{
			$valZ = &getFormulae($$record{RPT_REC_NUM}, $$record{RPT_YEAR}, "((G000000-01100-0100 - G000000-03600-0100) / G000000-02700-0100 * .717) + (((G000000-02700-0100 - G000000-04300-0100) / G000000-02700-0100) * .847) + (((G300000-03100-0100 + A000000-08800-0200) / G000000-02700-0100) * 3.107) + (((G000000-02700-0100 - G000000-04300-0100) / G000000-04300-0100) * .42) + ((G300000-00300-0100 / G000000-02700-0100) * .998)");
			print $valZ;
		}				
		print "</td> </tr> ";			 
		if($$record{RPT_YEAR})
		{
			if($valZ eq 'N/A')
			{
				$valZ="";
			}
			if($valOM eq 'N/A')
			{
				$valOM="";
			}			   			
   			$outer[$i] =  $valZ  . "|" . $valOM . "|" . $$record{PROVIDER_NUMBER} ;
		}		
		$i++;
	}
	
	print qq{
      </table>      
      </div>         
      };
      	my ($provider, $z, $om, $line);
	if ($action eq 'update')
	{
		
		print "<BR>";
		print "Updating, please wait......";		
		print "<div id='divupdate' style='height:100px;width:885px;overflow:auto;' align=\"left\" >";
		
		$sqlU = "Update Hospitals set Z_Score = ?, Operating_Margin = ? where PROVIDER_NUMBER = ?";
		unless ($sthU = $dbh->prepare($sqlU)) {
		      die "ERROR preparing SQL statement:" . $sthU->errstr;
		}	
		foreach $line (@outer) {
		    ($z, $om, $provider) = split /\|/,$line;
		    if($provider)
		    {
			    unless ($sthU->execute($z, $om, $provider)) {
			       print "ERROR executing UPDATE: $sthU->errstr\n";
			    }
			    print "$provider -> Completed";
		    	    print "<BR>";
		    }	    	    
		}
		print "</div>";
	}
	else
	{
		print "<span class=\"button\"><INPUT TYPE=\"SUBMIT\" VALUE=\"Update\"></span><br>";
	}
      
      print qq{
    <!--content end-->
    </center>
    </form>
   </body> 
  </html>
};

#--------------------------------------------------------------------------------------
sub sessionExpire
{

print "Content-Type: text/html\n\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}


#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}


#--------------------------------------------------------------------------------
# - Get details from Hospital table. 

sub getHospitalDetails
{
	my ($qry, $stmt, $record);
	
	$qry = "select * from Hospitals order by HOSP_NAME";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();   	
   	
   	my %state;   	
   	
	while ($record = $stmt->fetchrow_hashref) {	
		$state{$$record{PROVIDER_NUMBER}} = $$record{HOSP_NAME};		
  	}  	
  	#$state{"Other"} = "Other";
  	return %state;
}

#----------------------------------------------------------------------------

sub getFormulae
{
	my ($report_id, $yyyy, $formulae) = @_;
	my ($value);	
	
	$value = &getFormulaeValue($report_id,$formulae,$yyyy);
	
	#EBITDAR as a Percent of Interest Expense
	#Net Revenue per FTE
	
	return $value;
}

#--------------------------------------------------------------------------------

sub getFormulaeValue
{
	my ($report_id,$formulae,$yyyy) = @_;
	my ($value, $i, $elementVal, $operation);	
	
	for ($i=0;$i<length($formulae);$i++)
	{
		if(substr($formulae, $i,1) =~ /[A-Z]/ )
		{
			#Spliting elements from Address
			my ($worksheet, $line, $column) = split /[-:]/,substr($formulae, $i,18);
			if(&get_value($report_id, $worksheet, $line, $column, $yyyy))
			{	
				$elementVal = &get_value($report_id, $worksheet, $line, $column, $yyyy);
			}
			else
			{
				$elementVal = 0;
			}
				
			
			$value = $value.$elementVal;
			$i = $i+17;				
		}
		elsif(substr($formulae, $i,1) =~ /\s/ )
		{
		
		}
		else 
		{			
			
			$value = $value . substr($formulae, $i,1);			
		}		
	}
	$value = eval($value);
	if(!$value)
	{
		$value = "N/A";
	}
	else
	{
		$value = sprintf "%.2f", $value;
	}
	return $value;

}

#------------------------------------------------------------------------------

sub get_value
{
   my ($report_id, $worksheet, $line, $column, $fYear) = @_;

   my $yyyy = $fYear;

   my ($sthVal, $value);

   my $sql = qq(
    SELECT CR_VALUE         
      FROM CR_${yyyy}_DATA
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
   );

   unless ($sthVal = $dbh->prepare($sql)) {
       &display_error('Error preparing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sthVal->execute($report_id, $worksheet, $line, $column)) {
       &display_error('Error executing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }
   
   if ($sthVal->rows) {
      $value = $sthVal->fetchrow_array;
   }

   $sthVal->finish();

   return $value;
}


#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $a (@list) {
   $string .= "$a<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon" />
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css" />
<link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />
</HEAD>
<BODY>
<CENTER>
<BR>
<BR>
<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/cgi-bin/index.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/cr_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="/cgi-bin/cr_batchx.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="/cgi-bin/snf_batch1.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="/cgi-bin/standard_reports.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Standard Reports</div>">
			        <img src="/images/stardardreport_inner_icon.gif" onmouseover="src='/images/stardardreport_inner_icon_hover.gif';"
                                    onmouseout="src='/images/stardardreport_inner_icon.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 102px" align="Center">
                            <a href="/cgi-bin/PeerMain.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
			                                    <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->
    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</CENTER>
</BODY>
</HTML>
};

}
