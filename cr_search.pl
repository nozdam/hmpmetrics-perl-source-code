#!/usr/bin/perl


#----------------------------------------------------------------------------
#  program: exp1.pl
#  author: Steve Spicer
#  Written: Wed Oct 24 20:14:32 CDT 2007
#----------------------------------------------------------------------------

use strict;
use CGI;
use DBI;
do "pagefooter.pl";
do "common_header.pl";
print "Content-Type: text/html\n\n";
&headerScript();
use CGI::Session;
do "audit.pl";

my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $facility = $cgi->param("facility");

if(!$userID)
{
	&sessionExpire;
}

my $start_time = time;

$session->clear(["narrow_search", "narrow_search_module"]);
$session->param("narrow_search", "0");
$session->param("narrow_search_module", "search");
   
my $narrow_search = $session->param("narrow_search");
my $narrow_search_module = $session->param("narrow_search_module");


	open(J, ">data.txt");
						print J $narrow_search."pp".$narrow_search_module ;
						close J;
my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my $defaultYear= $ini_value{'DEFAULT_YEAR'};

my ($dbh,$facility_id,$sql_f,$sth,$sql_s,@search_elements,$size,$i,$row,$action,$sth1,$section_id);

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

$sql_f = "select facility_id from tbl_facility_type where facility_name = '$facility'";
$sth = $dbh->prepare($sql_f);
$sth->execute();

while ($row = $sth->fetchrow_hashref) {
	$facility_id = $$row{facility_id};
}

#jayanth 19Aug09 - Get MSA values from Database. 
my %msaDetais = &getMSADetails();
my %statesDetails = &getStatesDetails();
my %controlType = &getControlTypeDetails();
my %hospitalType = &getHospTypeDetails();
my %geographicalArea = &getGeoAreaDetails(); 
my %teachingHospital = &getTeachingHospDetails();   
 
my $sql_sec = "select section_id from tbl_section
where path = '/cgi-bin/cr_search.pl' and name = 'search elements'";
$sth1 = $dbh->prepare($sql_sec);
$sth1->execute();

while ($row = $sth1->fetchrow_hashref) {
	$section_id = $$row{section_id};
}
 
$sql_s = "select se.section_element_name as search_element from
tbl_section_element as se
inner join tbl_section_element_details as sd on
sd.section_element_id = se.section_element_id
where sd.facility_type = $facility_id
and se.section_id = $section_id
order by se.section_element_id";
$sth1 = $dbh->prepare($sql_s);
$sth1->execute();

while ($row = $sth1->fetchrow_hashref) {
	push(@search_elements,$$row{search_element});
}

unless ($action eq 'go') {
   &display_form();
   exit(0);
} 
$dbh->disconnect();
exit(0);

#----------------------------------------------------------------------------

sub display_error
{
   my @list = @_;

   my ($s, $string);

   foreach $s (@list) {
      $s =~ s/\n/<BR \/>/g;
      $string .= "$s<BR>\n";
   }

&innerHeader();
   print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>

<CENTER>
<BR>
<FONT SIZE="5" COLOR="RED">
<B></B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR ALIGN="LEFT">
      <TD>$string</TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
   };
}


#---------------------------------------------------------------------------

sub display_form
{
   my ($key);

   my $sql = "SELECT SystemID as SYSTEM_ID, SystemName as SYSTEM_NAME FROM tblsystemmaster ORDER BY SystemName";

   unless ($sth = $dbh->prepare($sql)) {
       &display_error('ERROR preparing SQL', $sth->errstr);
       $dbh->disconnect();
       exit(1);
   }

   unless ($sth->execute()) {
       &display_error('ERROR executing SQL', $sth->errstr);
       $dbh->disconnect();
       exit(1);
   }
   
   #------------------------------------Starts------------------------------------------
   # Build html table of rows of names along with check boxes and insert into divisions.
   # Each row has one hidden textbox which contains the 'text' of the item. 
   # And each check box value is value of that item
   # for example : for 'system' field Text = 'Adventist Health' value ='6'  
   #-------------------------------------------------------------------------------------

   #-- System
   
   my ($system_id, $system_name);

   my $system_options  = "<table width=\"100%\"><tr><td colspan=2 align=\"center\">-- Select Systems --</td></tr>\n";
   
   $system_options .= "<tr><td align=\"left\" style=\"width:25px;\"><input type=\"checkbox\" name=\"chksys\" value=\"none\"><input type=\"hidden\" name=\"sysString\" value=\"none\"></td><td align=\"left\">none</td></tr>\n";
   while (($system_id, $system_name) = $sth->fetchrow_array) {
      $system_options .= "<tr><td align=\"left\" style=\"width:25px;\"><input type=\"checkbox\" name=\"chksys\" value=\"$system_id\"><input type=\"hidden\" name=\"sysString\" value=\"$system_name\"></td><td align=\"left\">$system_name</td></tr>\n";
   }
   $system_options .= "</table>\n";
   $sth->finish();

   #-- Type of control

   my $toc_options  = "<table width=\"100%\"><tr><td colspan=2 align=\"center\">-- Select Types --</td></tr>\n";
      
   my $ind = 0;
   foreach $key (keys %controlType) {
      $ind++;	
      $toc_options .= "<tr><td align=\"left\" style=\"width:25px;\"><input type=\"checkbox\" name=\"chktoc\" value=\"$key\"><input type=\"hidden\" name=\"tocString\" value=\"$controlType{$key}\"></td><td align=\"left\">$controlType{$key}</td></tr>\n";      
   }
   
   $toc_options = $toc_options."</table>\n"; 
    
   #-- States
   
   my $statehtml = "<table width=\"100%\"><tr><td colspan=2 align=\"center\">-- Select States --</td></tr>";
       
   foreach $key(sort {$statesDetails{$a} cmp $statesDetails{$b} } keys %statesDetails){ 
    		        $statehtml .= "<tr><td align=\"left\" style=\"width:25px;\"><input type=\"checkbox\" name=\"chkState\" value=\"$key\"><input type=\"hidden\" name=\"statString\" value=\"$statesDetails{$key}\"></td><td align=\"left\">$statesDetails{$key}</td></tr>\n";    
   }
                	
   $statehtml = $statehtml."</table>\n";  
  
  #-- Hospital type  
   
  my $hospType ="<table width=\"100%\"><tr><td colspan=2 align=\"center\">-- Select Types --</td></tr>";
  
  foreach $key(sort keys %hospitalType){ 
 		$hospType .= "<tr><td align=\"left\" style=\"width:25px;\"><input type=\"checkbox\" name=\"chkhosptype\" value=\"$key\"><input type=\"hidden\" name=\"hosptypeString\" value=\"$hospitalType{$key}\"></td><td align=\"left\">$hospitalType{$key}</td></tr>\n";
  	}
  
  $hospType .= "</table>\n";
	
  #-- MSA
 # my $a = @restored_arry_msa;
 
  my $msahtml = "<table width=\"100%\"><tr><td colspan=2 align=\"center\">-- Select MSA --</td></tr>\n";
   foreach $key(sort keys %msaDetais){  
   	$msahtml .= "<tr><td align=\"left\" style=\"width:25px;\"><input type=\"checkbox\" name=\"chkMsa\" value=\"$key\"><input type=\"hidden\" name=\"msaString\" value=\"$msaDetais{$key}\"></td><td align=\"left\">$msaDetais{$key}</td></tr>\n";	           		
   }

  $msahtml .= "</table>\n";
  #------------------------------------------Ends----------------------------------------------

   print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<HEAD><TITLE>COST REPORTS</TITLE>
<link href="/css/search/cr_search.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/JS/search/cr_search.js"></script>
<SCRIPT SRC="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<SCRIPT SRC="/JS/dropmenu.js"></SCRIPT>
<link rel="stylesheet" href="/css/dropmenu.css" type="text/css" />
</HEAD>
<BODY>
<BR>
<CENTER>
};
&innerHeader();
print qq{
<FORM NAME="frmSearch" METHOD="POST" ACTION="/cgi-bin/peersearch.pl?facility=$facility">
    <!--content start-->      
    <table cellpadding="0" cellspacing="0" width="885px">
        <tr>
            <td valign="top">
                <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;
                text-align: left; padding: 10px">
                <TABLE BORDER="0" cellpadding="2" cellspacing="2">   
				 <tr>
				 <TD COLSPAN='3' style='padding:10px'>
					<FONT SIZE='3'>
		};
		print "<div><B>".uc($facility) ." SEARCH </B></div>";
		print qq{
			</FONT>
			</TD>
			<td></td><td align="left"> <INPUT TYPE="hidden" NAME="action" VALUE="go">
			<span class="button">  <INPUT TYPE="button" VALUE=" SEARCH " onclick="PageSubmit()"></span>&nbsp;<span class="button"><INPUT TYPE="reset"  VALUE=" CLEAR "></span> 
			</td></tr>
			};
			for($i=0;$i<scalar(@search_elements);$i++){		
			if(@search_elements[$i] eq 'Provider'){
				print "<div>	
					   <TR>
						  <TD ALIGN='RIGHT'><strong>@search_elements[$i]</strong></TD><TD>
						  <TD ALIGN='LEFT'>
							  <INPUT TYPE='text' class='textboxstyle'  class='textboxstyle'  NAME='provider' value=''SIZE='6' MAXLENGTH='6'>
						  </TD>
					   </TR>
					   </div>";
			}
			if(@search_elements[$i] eq 'Name'){	
			 print	"<div>	
					   <TR>
						  <TD ALIGN='RIGHT'><strong>@search_elements[$i]</strong></TD><Td><img src='/images/star_indicator.png'></td>
						  <TD ALIGN='LEFT'>
							  <INPUT TYPE='text' class='textboxstyle'  class='textboxstyle'  NAME='name' SIZE='40' value=''>
						  </TD>
					   </TR>
					</div>";
		   }   
		   if(@search_elements[$i] eq 'Address'){
			print "<div>	
				   <TR>
					  <TD ALIGN='RIGHT'><strong>@search_elements[$i]</strong></TD><Td><img src='/images/star_indicator.png'></td>
					  <TD ALIGN='LEFT'>
						  <INPUT TYPE='text' class='textboxstyle'  class='textboxstyle'  NAME='address' SIZE='30' value=''>
					  </TD>
				   </TR>
				</div>";
		   }
		   if(@search_elements[$i] eq 'City'){
		    print "<div>	
				   <TR>
					  <TD ALIGN='RIGHT'><strong>@search_elements[$i]</strong></TD><Td><img src='/images/star_indicator.png'></td>
					  <TD ALIGN='LEFT'>
						  <INPUT TYPE='text' class='textboxstyle'  class='textboxstyle'  NAME='city' SIZE='30' value=''>
					  </TD>
				   </TR>
				</div>";
		   }
		   if(@search_elements[$i] eq 'State(s)'){
		    print "<div>
				   <TR>
					  <TD ALIGN='RIGHT'><strong>@search_elements[$i]</strong></TD><td></td>
					  <TD ALIGN='LEFT'>
						<div ID='state' class='checkDiv'>	
						   $statehtml
						</div>  
					  </TD>
				   </TR>
				</div>";
		  }
		  if(@search_elements[$i] eq 'Zip Code'){
				print " <div>
			   <TR>
				  <TD ALIGN='RIGHT'><strong>@search_elements[$i]</strong></TD><td></td>
				  <TD ALIGN='LEFT'>
					  <INPUT TYPE='text' class='textboxstyle' class='textboxstyle'  NAME='zip' SIZE='10' MAXLENGTH='10' value=''>
				  </TD>
			   </TR>
			   </div>";
		   }
		   if(@search_elements[$i] eq 'County'){
				print"
			   <div>
			   <TR>
				  <TD ALIGN='RIGHT'><strong>@search_elements[$i]</strong></TD><Td><img src='/images/star_indicator.png'></td>
				  <TD ALIGN='LEFT'>
					  <INPUT TYPE='text' class='textboxstyle' NAME='county' SIZE='20' MAXLENGTH='20' value=''>
				  </TD>
			   </TR>
			   </div>";
		   }
			if(@search_elements[$i] eq 'System'){
				print "
			   <div>
			   <TR>
				  <TD ALIGN='RIGHT'><strong>@search_elements[$i]</strong></TD><Td></td>
				  <TD ALIGN='LEFT'>
					 <div ID='sys' class='checkDiv'>
					$system_options
				</div>
				  </TD>
			   </TR>
			   </div>";
		   }
		   if(@search_elements[$i] eq 'Type of Control'){
			   print "<div>
			   <TR>
				  <TD ALIGN='RIGHT'><strong>@search_elements[$i]</strong></TD><Td></td>
				  <TD ALIGN='LEFT'>
				<div ID='toc' class='checkDiv'>
					$toc_options
				</div>         
				  </TD>
			   </TR>
			   </div>";
		   }
		   if(@search_elements[$i] eq 'Hospital Type'){
				print "
			   <div>
			   <TR>
				  <TD ALIGN='RIGHT'><strong>@search_elements[$i]</strong></TD><Td></td>
				  <TD ALIGN='LEFT'>
				  <div ID='HospType' class='checkDiv'>
						$hospType
					</div> 
				  </TD>
					</TR>
				</div>";
		   }
		   if(@search_elements[$i] eq 'MSA'){
				print "
				 <div>
					<TR>
						<TD ALIGN='RIGHT'><strong>@search_elements[$i]</strong></TD><Td></td>
						<TD ALIGN='LEFT'>
							<div ID='HospType' class='checkDiv'>
								$msahtml
							</div>		
							<div style='float:left; width:100px; padding-left:10px'></div>
						</TD>
					</TR>
				  </div>";
		   }
		   if(@search_elements[$i] eq 'Teaching Hospital'){
			print "<div>
					<TR>
						<TD ALIGN='RIGHT'><strong>@search_elements[$i]</strong></TD><Td></td>
						<TD ALIGN='LEFT'>
						  <SELECT NAME='Teaching_Hosp' ID='Teaching_Hosp'  class='textboxstyle'>
							  <OPTION VALUE='' SELECTED>-- ALL --</OPTION>";
							foreach $key(sort keys %teachingHospital){
							  print "<OPTION VALUE=\'$teachingHospital{$key}\'>$teachingHospital{$key}</OPTION>\n";   
							}
							print "</SELECT>
								</TD>
							</TR>
				</div>";
		   }
		   if(@search_elements[$i] eq 'Geographical Area'){
		   print " <div>
		   <TR>
					<TD ALIGN='RIGHT'><strong>@search_elements[$i]</strong></TD><Td></td>
					<TD ALIGN='LEFT'>
					  <SELECT NAME='Geographical_Area' ID='Geographical_Area'  class='textboxstyle'>
						  <OPTION VALUE='' SELECTED>-- ALL Geographical Area --</OPTION>
					   ";
					   foreach $key(sort keys %geographicalArea){
						  print "<OPTION VALUE=\'$geographicalArea{$key}\'>$geographicalArea{$key}</OPTION>\n";
					   }
					   print "
					  </SELECT>
					</TD>
		   </TR>
		   </div>";
		  }
		  if(@search_elements[$i] eq 'Beds'){
		  print "<div>
			   <TR>
				  <TD ALIGN='RIGHT'><strong>@search_elements[$i]</strong></TD><Td></td>
				  <TD ALIGN='LEFT'>
					  BETWEEN <INPUT TYPE='text' class='textboxstyle'  NAME='min_beds' SIZE='4' MAXLENGTH='4' value=''>
					  AND <INPUT TYPE='text' class='textboxstyle'  class='textboxstyle'  NAME='max_beds' SIZE='4' MAXLENGTH='4' value=''>
				  </TD>
			   </TR>
			   </div>";
		   }
		   if(@search_elements[$i] eq 'FTEs'){
			   print "<div>
				   <TR>
					  <TD ALIGN='RIGHT'><strong>@search_elements[$i]</strong></TD><Td></td>
					  <TD ALIGN='LEFT'>
						  BETWEEN <INPUT TYPE='text' class='textboxstyle'  NAME='min_ftes' SIZE='4' MAXLENGTH='4' value=''>
						  AND <INPUT TYPE='text' class='textboxstyle'  NAME='max_ftes' SIZE='4' MAXLENGTH='4' value=''>
					  </TD>
				   </TR>
				   </div>";
		   }
		   if(@search_elements[$i] eq 'Net Income'){
			   print "<div>
				   <TR>
					  <TD ALIGN='RIGHT'><strong>@search_elements[$i]</strong></TD><Td></td>
					  <TD ALIGN='LEFT'>
						  BETWEEN <INPUT TYPE='text' class='textboxstyle'  NAME='min_net_income' SIZE='9' MAXLENGTH='12' value=''>
						  AND <INPUT TYPE='text' class='textboxstyle'  NAME='max_net_income' SIZE='9' MAXLENGTH='12' value=''>
					  </TD>
				   </TR>
				   </div>";
		   }
		   if(@search_elements[$i] eq 'Within'){
			   print "
				   <div>
				   <TR>
					  <TD ALIGN='RIGHT'><strong>@search_elements[$i]</strong></TD><Td></td>
					  <TD ALIGN='LEFT'><div style='float:left'>
						 <SELECT NAME='radius' class='textboxstyle'>
						<OPTION VALUE=''> </OPTION>
							<OPTION VALUE=15>15</OPTION>
							<OPTION VALUE=30>30</OPTION>
							<OPTION VALUE=60>60</OPTION>
							<OPTION VALUE=90>90</OPTION>
							<OPTION VALUE=120>120</OPTION>
						  </SELECT></div><div style='float:left; padding-left:10px; padding-top:5px'>
						 <strong>Miles of Zip Code:</strong></div><div style='float:left; padding-left:10px'>
							 <INPUT TYPE='text' class='textboxstyle'  NAME='location' TITLE='5 DIGIT ZIPCODE TO CENTER SEARCH FROM' SIZE='5' MAXLENGTH='5' value=''></div>
							<div style='float:left; padding-left:10px'></div>
					  </TD>
					</TR>
					</div>";
			} 
			 if(@search_elements[$i] eq 'Operating Margin'){
				 print "<div>
						   <TR>
							<TD ALIGN='RIGHT'><strong>@search_elements[$i]</strong></TD><Td></td>
							<TD ALIGN='LEFT'>
								BETWEEN <INPUT TYPE='text' class='textboxstyle'  NAME='min_Opr_Margin' SIZE='9' MAXLENGTH='12' value=''>
								AND <INPUT TYPE='text' class='textboxstyle'  NAME='max_Opr_Margin' SIZE='9' MAXLENGTH='12' value=''>
							</TD>
							</TR>
					   </div>";
			}
		}   
	print qq{
		   <div>
		   <tr><td style="padding-top:10px; padding-bottom:20px"></td></tr>
			   <TR><td></td><td><img src="/images/star_indicator.png"></td>
				  <TD align="left">
					<FONT COLOR="RED" SIZE="2">WILD CARD MATCHES SUPPORTED WITH THE "%" CHARACTER</FONT>
				</TD>
		   </TR>
		   </div>
 
			<tr><td></td><td></td><td align="left"> <INPUT TYPE="hidden" NAME="action" VALUE="go">
			<span class="button">   <INPUT TYPE="button" VALUE=" SEARCH " onclick="PageSubmit()"></span>&nbsp;<span class="button"><INPUT TYPE="reset"  VALUE=" CLEAR "></span>
			
			</td></tr>
			</TABLE></td>
				<td valign="top">
					<img src="/images/content_right.gif" /></td>
			</tr>
    </table>
    <!--content end-->
	};
		&TDdoc;
		print qq{
			<input type="hidden" name="srcStateText" >
			<input type="hidden" name="srcSystemText" >
			<input type="hidden" name="srcHospText" >
			<input type="hidden" name="srcTocText" >
			<input type="hidden" name="srcMSAText" >
			<input type="hidden" name="srcNFPText" >
			<input type="hidden" name="srcGeoText" >
			<input type="hidden" name="srcTeachText" >
			<input type="hidden" name="srcGovtText" >
			<input type="hidden" name="facility" value="$facility">
			<input type="hidden" name="toc"  >
			<input type="hidden" name="HospType"  >
			<input type="hidden" name="system"  >
			<input type="hidden" name="state"  >
			<input type="hidden" name="msa"  >
			<input type="hidden" name="N_F_Profit"  >
			<input type="hidden" name="Governmental"  >
			<input type="hidden" name="narrow_search" value ="$narrow_search" >
			<input type="hidden" name="narrow_search_module"  value = "$narrow_search_module">
<BR><script></script>
</FORM>
</CENTER>
</BODY>
</HTML>
   };
}


#------------------------------------------------------------------------------

sub comify
{
   local($_) = shift;
   1 while s/^(-?\d+)(\d{3})/$1,$2/;
   return($_);
}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#------------------------------------------------------------------------------

# - Get details from MSA table. 

sub getMSADetails
{
	my ($qry, $stmt, $record);
	
	$qry = "select * from MSA order by MSAName";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();   	
   	
   	my %msa;   	
   	
	while ($record = $stmt->fetchrow_hashref) {	
		$msa{$$record{MSACode}} = $$record{MSAName};		
  	}  	
  	return %msa;
}

#--------------------------------------------------------------------------------

# - Get details from States table. 

sub getStatesDetails
{
	my ($qry, $stmt, $record);
	
	$qry = "select * from states order by state_name";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();   	
   	
   	my %state;   	
   	
	while ($record = $stmt->fetchrow_hashref) {	
		$state{$$record{state}} = $$record{state_name};		
  	}  	
  	return %state;
}

#--------------------------------------------------------------------------------

# - Get details from Type of Control table. 

sub getControlTypeDetails
{
	my ($qry, $stmt, $record);
	
	$qry = "select * from control_type order by Control_Type_Name";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();   	
   	
   	my %cType;   	
   	
	while ($record = $stmt->fetchrow_hashref) {	
		$cType{$$record{Control_Type_id}} = $$record{Control_Type_Name};		
  	}
  	return %cType;
}

#--------------------------------------------------------------------------------

# - Get details from Hospital Type table. 

sub getHospTypeDetails
{
	my ($qry, $stmt, $record);
	
	$qry = "select * from hospital_type order by hospital_type_id";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();   	
   	
   	my %hType;   	
   	
	while ($record = $stmt->fetchrow_hashref) {	
		$hType{$$record{Hospital_Type_id}} = $$record{Hospital_Type_Name};				
  	}  	  	
  	return %hType;
}

#--------------------------------------------------------------------------------------
# - Get details from Geographical Area table. 

sub getGeoAreaDetails
{
	my ($qry, $stmt, $record);
	
	$qry = "select * from mastertables where MasterType = 'Geoagraphical Area'";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();   	
   	
   	my %gType;   	
   	
	while ($record = $stmt->fetchrow_hashref) {	
		$gType{$$record{ID}} = $$record{Name};				
  	}  	  	
  	return %gType;
}

#--------------------------------------------------------------------------------------
# - Get details from Teaching Hospital table. 

sub getTeachingHospDetails                        
{
	my ($qry, $stmt, $record);
	
	$qry = "select * from mastertables where MasterType = 'Teaching Hospital'";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();   	
   	
   	my %tType;   	
   	
	while ($record = $stmt->fetchrow_hashref) {	
		$tType{$$record{ID}} = $$record{Name};				
  	}  	  	
  	return %tType;
}

#--------------------------------------------------------------------------------------
sub sessionExpire
{

print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

