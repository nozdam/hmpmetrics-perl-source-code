#!/usr/bin/perl

use CGI;
use DBI;
use strict;
my $cgi = new CGI;
do "common_header.pl";
use CGI::Session;
print "Content-Type: text/html\n\n";
&headerScript();
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $rpt_mainid = $cgi->param("mainID");
my $reload =  $cgi->param("reload");
my $dtName = $cgi->param("dtName");
my $facility = $cgi->param("facility");
my ($sth1,$s,$row1,$row,$class1);
if(!$userID)
{
	&sessionExpire;
}

# Database

my $dbh;
my ($sql, $sth,$sql_f,$facility_id);
my($rpt_details_id,$rpt_details_element,$rpt_details_isFormulae,$rpt_details_Formulae,$rpt_details_summary,$summary_Info,$temp);
# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};
my $orderBy = $cgi->param("orderBy");
if(!$orderBy){
	$orderBy = "element";
}
unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error('ERROR connecting to database', $DBI::errstr);
   exit(1);
}

$sql_f = "select facility_id from tbl_facility_type where facility_name = '$facility'";
$sth1 = $dbh->prepare($sql_f);
$sth1->execute();

while ($row = $sth1->fetchrow_hashref) {
	$facility_id = $$row{facility_id};
}

$sql = "select sm.metrics_id,sm.element,sm.isFormulae,sm.Formulae,rm.Summary_Info from standard_metrics as sm
inner join standard_reports_metrics as rm
on sm.metrics_id = rm.metrics_id Where reports_id = '$rpt_mainid' and sm.facility_type = $facility_id order by $orderBy";
$sth = $dbh->prepare($sql);
$sth->execute();


print qq{
<HTML>
<HEAD><TITLE>Standard Reports</TITLE>
 <script type="text/javascript" src="/JS/AjaxScripts/prototype.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/effects.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/controls.js"></script>
 <script type="text/javascript" src="/JS/admin_panel/standard_report/ReportDetails.js"></script>
 </HEAD>
 <BODY onload="setRepName()">
<FORM NAME = "frmReportDetails" Action="" method="post">
<TABLE align="top" border=1 class="gridstyle" CELLPADDING="0" CELLSPACING="0" width="100%" height="100%">
};
			my $i = 0;
			my $isFormula = undef;
			while ($row1 = $sth->fetchrow_hashref) {
				$rpt_details_id = $$row1{metrics_id};
				$rpt_details_element = $$row1{element};
				$rpt_details_isFormulae = $$row1{isFormulae};
				$rpt_details_Formulae = $$row1{Formulae};
				$rpt_details_summary="";
				$summary_Info = $$row1{Summary_Info};
				$temp = my ($rpt_details_worksheet,$rpt_details_line_no,$rpt_details_column_no) = split(/-/, $rpt_details_Formulae);
				if($summary_Info eq 'Avg'){
					$rpt_details_summary = "Average";
				}elsif($summary_Info eq 'NA'){
					$rpt_details_summary = "No Summary";
				}else{
					$rpt_details_summary = $summary_Info;
				}

				if($rpt_details_isFormulae eq '1'){
					$isFormula = "True";
					$rpt_details_worksheet = "";
					$rpt_details_line_no = "" ;
					$rpt_details_column_no = "";
				}else{
					$isFormula = "False";
					$rpt_details_Formulae="";
					$temp;
				}
				$class1 = (++$i % 2) ? 'gridrow' : 'gridalternate';
				#print "<tr class=\"$class1\"><td><a name=\"main$i\">$i</a></td><td><a href=\"#main$i\">$rpt_details_element</a></td><td>$rpt_details_worksheet</td><td>$rpt_details_line_no</td><td>$rpt_details_column_no</td><td>$rpt_details_cra</td><td>$rpt_details_isFormulae</td><td nowrap>$rpt_details_Formulae</td></tr>\n";
				print "<tr ><td nowrap style=\"width:10%;color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">SlNo</td><td style=\"background-color:#edf2fb\">$i</td></tr>\n";
				print "<tr ><td nowrap style=\"width:10%;color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Element</td><td style=\"background-color:#edf2fb\">$rpt_details_element</td></tr>\n";
				print "<tr ><td nowrap style=\"width:10%;color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Worksheet</td><td style=\"background-color:#edf2fb\">$rpt_details_worksheet</td></tr>\n";
				print "<tr ><td nowrap style=\"width:10%;color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Line No.</td><td style=\"background-color:#edf2fb\">$rpt_details_line_no</td></tr>\n";
				print "<tr ><td nowrap style=\"width:10%;color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Column No.</td><td style=\"background-color:#edf2fb\">$rpt_details_column_no</td></tr>\n";
				print "<tr ><td nowrap style=\"width:10%;color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Add Formulae</td><td style=\"background-color:#edf2fb\">$isFormula</td></tr>\n";
				print "<tr ><td nowrap style=\"width:10%;color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Formulae</td><td nowrap style=\"background-color:#edf2fb\">$rpt_details_Formulae</td></tr>\n";
				print "<tr ><td nowrap style=\"width:10%;color:#FFFFFF;font-weight:bold;background-color:#B4CDCD\">Report Total</td><td style=\"width:50%;background-color:#edf2fb\">$rpt_details_summary</td></tr>\n";
				print "<tr bgcoloe=\"#307D7E\"><td></td><td colspan=\"6\"><table width=\"450px\"><tr><td></td><td></td><td width=\"80%\" align=\"left\"><a href=\"#\" onclick=\"delDetails('/cgi-bin/ReportActions.pl?saction=delDetails&txtRptName=$dtName&rpt_mainid=$rpt_mainid&repdetailsid=$rpt_details_id&facility=$facility\','$rpt_details_element')\" title=\"Delete '$rpt_details_element' element\"><img src=\"/images/btn_delete.gif\" border=\"0\"></a></td></tr></table></td></tr>\n";
			}
print qq{
	</TABLE>
<input type="hidden" name="orderBy" value="$orderBy">
<input type="hidden" name="dtName" value="$dtName">
<input type="hidden" name="mainID" value="$rpt_mainid">
<input type="hidden" name="recCount" value="$i">
<input type="hidden" name="facility" value="$facility">
</FORM>
</BODY>
</HTML>
};

#--------------------------------------------------------------------------------------
sub sessionExpire
{

print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}
#-----------------------------------------------------------------------------


sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}



print qq{
<HTML>
<HEAD><TITLE>Standard Reports-ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
<BR>
<BR>
<FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}