#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: Registration.pl
#  author: Jayanth Raj
#----------------------------------------------------------------------------

use strict;
use CGI;
use DBI;
do "common_header.pl";
use CGI::Session;
do "audit.pl";

my $cgi = new CGI;
my $dbh;
my ($sql, $sth, $key, @row);

# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my $defaultYear= $ini_value{'DEFAULT_YEAR'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

my $saction = $cgi->param('saction');
my $userName = $cgi->param('username');
my $password = $cgi->param('password');

my ($userID, $str, $firstName, $lastName, $middleName, $role);
if($saction eq 'login')
{
	$sql = "select UserID from Users where UserName = '$userName' and Password = AES_ENCRYPT('$password','bs') and IsActive = 1";
	$sth = $dbh->prepare($sql);
	$sth->execute();
	@row = $sth->fetchrow_array();
	$userID = @row[0];
	if($sth->rows()>0)
	{
		my $rec;
		$str = " ";
		$sql = "select ud.First_Name as First_Name, ud.Middle_Name as Middle_Name, ud.Last_Name as Last_Name, rl.RoleName as RoleName,rl.RoleID as roleID from userdetails as ud left outer join users as us on us.UserID = ud.UsersID left outer join role as rl on us.RoleID = rl.RoleID where UsersID = $userID ";
		$sth = $dbh->prepare($sql);
		$sth->execute();

		while ($rec = $sth->fetchrow_hashref) {
			$firstName = $$rec{First_Name};
			$lastName = $$rec{Last_Name};
			$middleName = $$rec{Middle_Name};
			$role = $$rec{roleID};
		}
		&loginSuccess;
		exit(0);
	}
	else
	{
		$str = "The username or password you entered is incorrect";
	}

}
elsif ($saction eq 'sessOut')
{
	$str = "Your session has expired. Please login again.";
}
elsif ($saction eq 'signOut')
{
	&signOut;

	$str = " ";
}

print "Content-Type: text/html\n\n";

print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style>

.label{
    font-family:Verdana, Arial, Helvetica, sans-serif;
    font-size:11px;
    color:#0066FF;
}
.tableBorder{
    border:solid 1px #0066FF;
    margin-top:100px;
}
.message{
    font-family:Verdana, Arial, Helvetica, sans-serif;
    font-size:14px;
    font-weight:bold;
    color:#0066FF;
}

</style>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Milo Login...</title>
<link href="/css/register/main.css" rel="stylesheet" type="text/css">
<link href="/css/register/qm.css" rel="stylesheet" type="text/css">
};
&headerScript();
print qq{
</head>
<body>
<table cellpadding="0" cellspacing="0" width="740px" align="center">
	<tr>
	    <td align="center" style="padding-top: 10px; padding-bottom: 10px; ">
		<a href="http://www.hcmpllc.com">
		    <img border="0" src="/images/logo.gif"></a>
	    </td>
	</tr>
</table>
<form action="/cgi-bin/login.pl" method="post" name="frmLogin" id="frmLogin" >
<table cellpadding="2px" cellspacing="1px" bgcolor="#F4F5F7" width="400px" class="tableBorder" align="center">
    <tr>
        <td colspan="2" class="Fnt11Pix" background="/images/register/grad_01_hover.gif">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2" class="label">&nbsp;</td>
    </tr>

    <tr>
        <td align="center" colspan="2">
            <span class="message">Login to Milo</span>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="label">&nbsp;</td>
    </tr>
    <tr>
        <td class="label" align="right" width="40%">Username:</td>
        <td align="left" width="60%"><input type="text" name="username" maxlength="100"/></td>
    </tr>
    <tr>
        <td class="label" align="right">Password:</td>
        <td align="left"><input type="password" name="password" maxlength="100" /></td>
    </tr>
    <tr>
        <td class="label" align="right">&nbsp;</td>
        <td align="left"><input type="hidden" name="saction" value="login"><input type="submit" value="Login" />&nbsp;<a href="/cgi-bin/forgotPassword.pl">Forgot Password ?</a></td>
    </tr>
 <tr>
         <td colspan="2" class="label">&nbsp;</td>
    </tr>
    <tr>
            <td class="label" align="right">&nbsp;</td>
            <td align="left"><a href="http://www.hcmpllc.com/tools/hmp_metrics/" target="_new">HMP Metrics complimentary reports</a></td>
    </tr>

</table>
<table align="center">
<tr>
<td style="color:#FF0000;">
};
	print $str;
print qq{
</td>
</tr>
</form>
</body>
</html>
};


#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#------------------------------------------------------------------------------

sub loginSuccess
{

my $session = new CGI::Session("driver:File", undef, {Directory=>"/tmp"});

my $sid = $session->id();

my $cookie = $cgi->cookie(CGISESSID => $session->id);

$session->param("firstName", $firstName);
$session->param("lastName", $lastName);
$session->param("userID", $userID);
$session->param("role", $role);
$session->expire('+1h');

print $cgi->header(-cookie=>$cookie);

#insert data in audit log

&viewLogin("Login",$userID,"");

print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
};
&headerScript();
print qq{
</head>
<body>
};
print "<script>window.parent.location.href='/cgi-bin/index.pl';</script>";
print qq{
</body>
</html>
};

}

#------------------------------------------------------------------------------

sub signOut
{
	my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
	$session->expire(1);
}

#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach my $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}

#------------------------------------------------------------------------------