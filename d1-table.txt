# TABLE D1
R	02500	Adults & Pediatrics (General Routine Care)
R	02600	Intensive Care Unit
R	02700	Coronary Care Unit
R	02800	Burn Intensive Care Unit
R	02900	Surgical Intensive Care Unit
R	03000	Other Special Care Unit (specify)
R	03100	Subprovider
R	03300	Nursery
R	10100	Total (lines 25-33)

C	0100	Old Capital - Capital Related Cost
C	0200	Old Capital - Swing Bed Adjustment
C	0300	Old Capital - Reduced Capital Related Cost 
C	0400	New Capital - Capital Related Cost
C	0500	New Capital - Swing Bed Adjustment
C	0600	New Capital - Reduced Capital Related Cost 
C	0700	Total Patient Days
C	0800	Inpatient Program Days
C	0900	Old Captial - Per Diem (c3 - c7)
C	1000	Old Captial - Inpatient Program Captial Cost (c9 * c8)
C	1100	New Captial - Per Diem (c6 - c7)
C	1200	New Captial - Inpatient Program Captial Cost (c11 * c8)
