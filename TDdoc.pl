#!/usr/bin/perl

use strict;
use CGI;
use DBI;
print "Content-Type: text/html\n\n";
my $cgi=new CGI;

print qq{
<html>
<head>
<title></title>
<body>
<h3 align="center">DISCLAIMER AND TERMS OF USE</h1>

<h4>Terms of Use:  General</h3>
<p>This website and others maintained by or on behalf of Healthcare Management Partners, LLC or its affiliates ("HMP") offer complimentary information as a service at no charge to users, with the express condition that your use of this HMP website implies your acceptance of all of the terms and conditions below (the "Terms of Use").  HMP reserves the right to modify the Terms of Use at any time, without notice.  HMP may discontinue maintaining this site or may impose a subscription fee or other charge for use of this site at any time, but you will not be charged any such fee or charge without your express agreement (which may be on a subscription, per-use, per-search or other basis at the discretion of HMP).</p>
<h4>Beta Site</h4>
<p>This website is currently in experimental, "Beta Site" form.  Accordingly, this website and its contents are still undergoing testing, and there is no guarantee that this website will be available, or offer full functionality, at all times or at any specific time.  Should you encounter any bugs, glitches, lack of functionality or other problems on this website, please let us know immediately by contacting HMP using the information at www.hcmpllc.com/hcmp/contactus.html.</p>
<h4>Third-Party Information</h4>
<p>Most of the information contained on this website is obtained from third-party sources, primarily the Centers for Medicare and Medicaid Services of the U.S. Department of Health and Human Services ("CMS").  The availability, form and content of information available from CMS or other third-party sources may change at any time and from time to time, and HMP makes no warranty as to the accuracy, completeness, correctness, reliability or availability of such information.  In providing such information from third-party sources, HMP has not made any independent investigation of the accuracy, completeness, correctness or reliability of such information.  HMP expressly disclaims any duty to correct, update or otherwise change information obtained from third-party sources, whether or not HMP becomes aware of any corrections, updates or changes made by such sources.</p>
<h4>Disclaimer of Warranty</h4>
<p>All information on this website is provided on an "as-is", "as available" basis.  HMP makes no warranties of any kind about the accuracy, completeness, timeliness or reliability of this website's content, or that this website will be error-free or virus-free.  Information on this website is general and may be out of date, may not be relevant to your specific situation, may be misinterpreted or may be incomplete or otherwise inaccurate.</p>
<h4>Limitation of Liability</h4>
<p>You agree to hold harmless HMP, its affiliates and their respective officers, directors, managers, employees and volunteers from all claims relating to this website and any website to which it is linked.  By using this website, you agree that neither HMP nor any of its affiliates shall be liable in any respect for claims or damages ensuing from or by reason of or in any way connected with the use of this site or any linked sites, including, but not limited to, consequential damages or damages for injury of any type, damage to computer hardware, software or systems, lost data, lost profits or loss of goodwill.</p>
<h4>Links to Third-Party Sites</h4>
<p>HMP may provide links to Internet sites maintained by third parties, over which HMP has no control. HMP and its affiliates do not endorse the content, operators, products or services of such sites, and they are not responsible or liable for the services or other materials on or available from such sites.</p>
<h4>Use of Content</h4>
<p>Unless noted otherwise, all information contained on this website, such as text, graphics, logos, icons and images, that is not publicly available data is copyrighted by and property of HMP or its licensors, and may not be copied, altered, stored or otherwise used in whole or part without the express consent of HMP.</p>
<h4>Confidentiality</h4>
<p>Please be aware that most information transmitted over the Internet is not secure; thus, confidentiality cannot be guaranteed. Information of a confidential, proprietary or privileged nature should not be sent electronically through this website. Any information transmitted to this website will be treated as non-confidential unless transmitted to a secure environment.</p>
<h4>CPrivacy Policy</h4>
<p>This website is subject to HMP's Privacy Policy, a copy of which may be reviewed at www.hcmpllc.com/hcmp/privacystatement.html, and which is incorporated by reference into these Terms of Use are posted at that URL or are otherwise made publicly available by HMP are incorporated by reference into these Terms of Use.</p>
</br>
</body>
</head>
</html>
};