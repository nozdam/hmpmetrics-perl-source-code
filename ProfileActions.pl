#!/usr/bin/perl

use CGI;
use DBI;

my $cgi = new CGI;

use CGI::Session;
do "common_header.pl";
print "Content-Type: text/html\n\n";
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");

my $saction = $cgi->param("saction");
my $title = $cgi->param("txtNameTitle");
my $suffix = $cgi->param("txtNameSuffix");
my $fname = $cgi->param("txtFirstName");
my $mname = $cgi->param("txtMiddleName");
my $lname = $cgi->param("txtLastName");
my $add1 = $cgi->param("txtAddress1");
my $add2 = $cgi->param("txtAddress2");
my $add3 = $cgi->param("txtAddress3");
my $city = $cgi->param("txtCity");
my $state = $cgi->param("txtState");
my $zip = $cgi->param("txtZip");
my $Telephone1 = $cgi->param("txtTelephone1");
my $Telephone2 = $cgi->param("txtTelephone2");
my $email = $cgi->param("txtEmail");
my $company_name = $cgi->param("txtCompany");
if(!$userID)
{
	&sessionExpire;
}

# Database 

my $dbh;
my ($sql, $sth);

# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error('ERROR connecting to database', $DBI::errstr);
   exit(1);
}


if(trim($saction) eq 'Profile'){	
	
	$sql = "update userdetails set First_Name = '$fname' ,Middle_Name = '$mname' , Last_Name = '$lname' , emailID = '$email' ,Address1 = '$add1' ,Address2 = '$add2',Address3 = '$add3',city = '$city',State = '$state',zip ='$zip',Title = '$title',Suffix_Title = '$suffix',Phone1 = '$Telephone1',Phone2 = '$Telephone2', ModifiedDate = CURRENT_DATE ,company_name = '$company_name' where userdetails.UsersID = '$userID'";
	$sth = $dbh->prepare($sql);
	$sth->execute();	
	
	$msg ="<script>alert('Profile updated successfully.');window.location.href='/cgi-bin/Profile.pl';</script>";	
}



print qq{
<HTML>
<HEAD><TITLE>Standard Reports</TITLE>
 <script type="text/javascript" src="/JS/AjaxScripts/prototype.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/effects.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/controls.js"></script> 
</HEAD>
<BODY onload="setCreate()">
<center><br>
};
&innerHeader();
print qq{
<FORM NAME = "frmReportActions" Action="/cgi-bin/ReportActions.pl" method="post">
<TABLE align="top" border=1 class="gridstyle" CELLPADDING="0" CELLSPACING="0" width="100%">
};	
print "$msg";
print qq{
</TABLE>
</FORM>
</BODY>
</HTML>
};
#--------------------------------------------------------------------------------------

sub sessionExpire
{

print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};
&headerScript();
print qq{
<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>
</body>
</html>
};

}
#-----------------------------------------------------------------------------


sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>
    
    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>};
	&TDdoc;
print qq{
</BODY>
</HTML>
};

}

#Perl trim function to remove whitespace from the start and end of the string

sub trim($)
{
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}
