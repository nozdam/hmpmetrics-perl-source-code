#!/usr/bin/perl 
#--------------------------------------------------------------------
# Program: 
# used by: 
#  Author: Steve Spicer
#--------------------------------------------------------------------

use strict;
use CGI;

my $cgi = new CGI;

#my $label_list = $cgi->param("labels");
#my $value_list = $cgi->param("values");
#my @labels = split /,/,$label_list;
#my @values = split /,/,$value_list;

my $chart_width  = 800;
my $chart_height = 600;

my @values = (.8, .5,  .3, .7,  .9,  .6, .65, .40);

my ($n, $i, $x, $y, $x1, $y1, $label, $sample);

$n = @values;

my $x_spacing = $chart_width / ($n + 1);

print "Content-Type: image/svg+xml\n\n";
#<svg width="16cm" height="12cm" viewBox="0 0 $chart_width $chart_height" xmlns="http://www.w3.org/2000/svg" version="1.1">

print <<EOF
<?xml version="1.0" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg viewBox="0 0 $chart_width $chart_height" xmlns="http://www.w3.org/2000/svg" version="1.1">

 <style type="text/css"><![CDATA[
    .Border { fill:none; stroke:blue; stroke-width:1 }
    .CtlPoint { fill:#888888; stroke:none }
    .AutoCtlPoint { fill:none; stroke:blue; stroke-width:4 }
    .Label { font-size:10pt; font-family:Verdana; font-weight: normal }
    .Point { fill:none; stroke:red; stroke-width:5 }
    .Grid { fill:none; stroke:lightblue; stroke-width:1 }
  ]]></style>

  <rect x="1" y="1" width="798" height="598" fill="none" stroke="grey" stroke-width="2" />
EOF
;


for $i (1 .. 9) {
   $y = $i * ($chart_height / 10);
   print "<path class=\"Grid\" d=\"M 0 $y L $chart_width $y\"></path>\n";
}

for $i (1 .. 9) {
   $y = $i * ($chart_height / 10) -4;
   $label = sprintf("%.02f", (10 - $i) / 10);
   print "<text class=\"Label\" x=\"3\" y=\"$y\">$label</text>\n";
}

$i = 0;
foreach $sample (@values) {
   $x = ++$i * $x_spacing;
   print "<path class=\"Grid\" d=\"M $x 0 L $x, $chart_height\"></path>\n";
}

$i = 0;
foreach $sample (@values) {
   $x = ++$i * $x_spacing + 4;
   $y = $chart_height - ($chart_height * $sample) -4;
   print "<text class=\"Label\" x=\"$x\" y=\"$y\">$sample</text>\n";
};

print "<polyline class=\"Border\" points=\"";

$i = 0;
foreach $sample (@values) {
   $x = ++$i * $x_spacing;
   $y = $chart_height - ($chart_height * $sample);
   print "$x,$y  ";
}

print "\" />\n";
print "</svg>\n";
exit 0;
