#!/usr/bin/perl

use strict;
use DBI;
use CGI;
use CGI::Session;

require "/var/www/cgi-bin/lib/req-milo.pl";

my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $facility = $cgi->param("facility");
my ($sql_f,$facility_id,$master_table,$report_table,$data_table,$row,$sth1,$view,$s,$section_id,$sql_sec,$sql_s,@search_elements,$i);
my $thpsql;
if(!$userID)
{
	&sessionExpire;
}



my $sql = $session->param("peersearch_sql");
my @sql_args = split(',,', $session->param("peersearch_sql_args"));
my $rs = &run_mysql($sql, 4, undef, undef, @sql_args);
my $file_name = "search_results.csv";

print "Content-type: text/csv\n";
print "Content-Disposition: attachment; filename=$file_name\n";
print "Cache-Control: no-cache\n";
print "Pragma: no-cache\n\n";
print "Provider No,Provider Name,City,State\n\n";
foreach my $k (sort {$a <=> $b} keys %{$rs}) {
	print '="' . $$rs{$k}{'provider_number'} ."\",$$rs{$k}{'name'},$$rs{$k}{'city'},$$rs{$k}{'state'}\n";
}

sub sessionExpire
{
print "Content-Type: text/html\n\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};
&headerScript();
print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};
}
#---------------------