#!/usr/bin/perl

#----------------------------------------------------------------------------
# Program: select.pl
#  Author: Jayanth Raj
# Request:
# Written:
# Revised:
#----------------------------------------------------------------------------


use DBI;
use DateTime;
use CGI::Session;

do "common_header.pl";
my $cgi = new CGI;
my($firstName, $lastName, $userID);
my $sid = $cgi->cookie('CGISESSID') || $cgi->param('CGISESSID') || undef;
if ($sid) {
	my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});

	$firstName = $session->param("firstName");
	$lastName = $session->param("lastName");
	$userID = $session->param("userID");
}

my $dbh;
my %ini_value;

unless (&read_inifileNew('milo.ini')) {
   &display_error_audit("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};
my $upload_dir = $ini_value{'PEER_IMPORT_FILE_PATH'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error_audit('ERROR connecting to database', $DBI::errstr);
   exit(1);
}

#-------------------------------------------
sub viewLogin
{
	my ($action,$loginUser,$description) = @_;
	my $dt = DateTime->now(time_zone=>'local');
	my ($sql,$sth);
	my $user_ip = $cgi->remote_host();
	$sql = "insert into audit(user_id,log_date,action,description,user_ip) values(?,?,?,?,?)";
	unless ($sth = $dbh->prepare($sql)) {
		&display_error_audit('ERROR preparing SQL', $sth->errstr);
		$dbh->disconnect();
		exit(1);
	}

	unless ($sth->execute($loginUser,$dt,$action,$description,$user_ip)) {
		&display_error_audit('ERROR executing SQL', $sth->errstr, $sql);
		$dbh->disconnect();
		exit(1);
   	}
}

#-------------------------------------------

sub view
{
	my ($action,$description,$module) = @_;
	my $dt = DateTime->now(time_zone=>'local');
	my ($sql,$sth);
	my $user_ip = $cgi->remote_host();

	$sql = "insert into audit(user_id,log_date,action,description,module,user_ip) values(?,?,?,?,?,?)";
	unless ($sth = $dbh->prepare($sql)) {
		&display_error_audit('ERROR preparing SQL', $sth->errstr);
		$dbh->disconnect();
		exit(1);
	}

	unless ($sth->execute($userID,$dt,$action,$description,$module,$user_ip)) {
		&display_error_audit('ERROR executing SQL', $sth->errstr, $sql);
		$dbh->disconnect();
		exit(1);
   	}
}

#-------------------------------------------
sub insert
{
	my ($action,$description,$newVal,$module) = @_;
	my $dt = DateTime->now(time_zone=>'local');
	my ($sql,$sth);
	my $user_ip = $cgi->remote_host();

	$sql = "insert into audit(user_id,log_date,action,description,new_value,module,user_ip) values(?,?,?,?,?,?,?)";
	unless ($sth = $dbh->prepare($sql)) {
		&display_error_audit('ERROR preparing SQL', $sth->errstr);
		$dbh->disconnect();
		exit(1);
	}

	unless ($sth->execute($userID,$dt,$action,$description,$newVal,$module,$user_ip)) {
		&display_error_audit('ERROR executing SQL', $sth->errstr, $sql);
		$dbh->disconnect();
		exit(1);
   	}
}

#-------------------------------------------
sub update
{
	my ($action,$description,$newVal,$oldVal,$module) = @_;
	my $dt = DateTime->now(time_zone=>'local');
	my ($sql,$sth);
	my $user_ip = $cgi->remote_host();

	$sql = "insert into audit(user_id,log_date,action,description,new_value,old_value,module,user_ip) values(?,?,?,?,?,?,?,?)";
	unless ($sth = $dbh->prepare($sql)) {
		&display_error_audit('ERROR preparing SQL', $sth->errstr);
		$dbh->disconnect();
		exit(1);
	}

	unless ($sth->execute($userID,$dt,$action,$description,$newVal,$oldVal,$module,$user_ip)) {
		&display_error_audit('ERROR executing SQL', $sth->errstr, $sql);
		$dbh->disconnect();
		exit(1);
   	}
}

#-------------------------------------------
sub delete
{
	my ($action,$description,$oldVal,$module) = @_;
	my $dt = DateTime->now(time_zone=>'local');
	my ($sql,$sth);
	my $user_ip = $cgi->remote_host();

	$sql = "insert into audit(user_id,log_date,action,description,old_value,module,user_ip) values(?,?,?,?,?,?,?)";
	unless ($sth = $dbh->prepare($sql)) {
		&display_error_audit('ERROR preparing SQL', $sth->errstr);
		$dbh->disconnect();
		exit(1);
	}

	unless ($sth->execute($userID,$dt,$action,$description,$oldVal,$module,$user_ip)) {
		&display_error_audit('ERROR executing SQL', $sth->errstr, $sql);
		$dbh->disconnect();
		exit(1);
   	}
}

#------------------------------------------------------
sub read_inifileNew
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#----------------------------------------------------------------------------

sub display_error_audit
{
my @list = @_;

my $string;

foreach my $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}

#------------------------------------------------------------------------------
