#!/usr/bin/perl -w

#----------------------------------------------------------------------------
#  program: snf_kb_update.pl
#   author: Steve Spicer
#  Written: Wed Sep 10 19:19:49 CDT 2008
#  Revised:
#  Research Data Assistance Center
#  http://www.resdac.umn.edu/Tools/TBs/TN-005-revised.asp
#----------------------------------------------------------------------------

use strict;
use CGI;
use DBI;

my $cgi = new CGI;

my $action    = $cgi->param('action');  
my $address   = $cgi->param('address');
my $desc      = $cgi->param('desc');
my $notes     = $cgi->param('notes');
my $fmt       = $cgi->param('fmt');
my $verified  = $cgi->param('verified');

$verified = ($verified eq 'on') ? 'Y' : 'N';

my $author    = $ENV{"REMOTE_USER"};

my %ini_value;

unless (&read_inifile("milo.ini")) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $ref_old;   # hash referece for old record

my $dbtype     = $ini_value{"DB_TYPE"};
my $dblogin    = $ini_value{"DB_LOGIN"};
my $dbpasswd   = $ini_value{"DB_PASSWORD"};
my $dbname     = $ini_value{"DB_NAME"};
my $dbserver   = $ini_value{"DB_SERVER"};

my $dbh;

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

if ($action eq 'UPDATE') {
   &update_kb_record($address, $desc, $notes, $fmt, $verified, $author);
   &display_success();
} elsif ($action eq 'INSERT') {
   &insert_kb_record($address, $desc, $notes, $fmt, $verified, $author);
   &display_success();
} else {
   &get_kb_record($address);
   &display_form();
}

$dbh->disconnect();

exit(0);

#----------------------------------------------------------------------------

sub get_kb_record
{

   my ($address) = @_;

   my ($sth);

   my $sql = "SELECT * FROM SNF_KB_TABLE WHERE KB_ADDRESS = ?";

   unless ($sth = $dbh->prepare($sql)) {
      &display_error("ERROR preparing sql query", $sth->errstr);
      $dbh->disconnect();
      exit(0);
   }

   unless ($sth->execute($address)) {
       &display_error("ERROR executing sql query", $sth->errstr);
       exit(0);
   }

   $ref_old = $sth->fetchrow_hashref;

   $sth->finish();

   return(0);
}

#----------------------------------------------------------------------------

sub update_kb_record
{
   my  ($address, $desc, $notes, $fmt, $verified, $author) = @_;

   my ($sth);

   my $sql = qq{
    UPDATE SNF_KB_TABLE
       SET KB_DESC     = ?,
           KB_NOTES    = ?,
           KB_FORMAT   = ?,
           KB_VERIFIED = ?,
           KB_AUTHOR   = ?
     WHERE KB_ADDRESS  = ?
   };

   unless ($sth = $dbh->prepare($sql)) {
       &display_error("ERROR preparing sql update", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sth->execute($desc, $notes, $fmt, $verified, $author, $address)) {
       &display_error("ERROR executing sql update", $sth->errstr);
       exit(0);
   }

   $sth->finish();

   return(0);
}

#----------------------------------------------------------------------------

sub insert_kb_record
{
   my  ($address, $desc, $notes, $fmt, $verified, $author) = @_;

   my ($sth);

   my $sql = qq{
    INSERT INTO SNF_KB_TABLE 
            (KB_ADDRESS, KB_DESC, KB_NOTES, KB_FORMAT, KB_VERIFIED, KB_AUTHOR)
     VALUES (?, ?, ?, ?, ?, ?)
   };
    
   unless ($sth = $dbh->prepare($sql)) {
       &display_error("ERROR preparing sql update", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sth->execute($address, $desc, $notes, $fmt, $verified, $author)) {
       &display_error("ERROR executing sql update", $sth->errstr);
       exit(0);
   }

   $sth->finish();

   return(0);
}
#----------------------------------------------------------------------------

sub display_error
{
   my @list = @_;

   my ($string, $s);

   foreach $s (@list) {
      $string .= "$s<BR>\n";
   }

   print "Content-Type: text/html\n\n";

   print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css" />
<link href="/JS/ergosign.styleform.css" media="all" type="text/css" rel="stylesheet">

    <script type="text/javascript" src="/JS/mootools-1.2-core.js"></script>

    <script type="text/javascript" src="/JS/ergosign.styleform.js"></script>

    <script type="text/javascript" src="/JS/script.js"></script>
    <link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />
</HEAD>
<BODY>
<CENTER>
<BR>
<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/index.html" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/cr_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="/cgi-bin/cr_batchx.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="/cgi-bin/snf_batch1.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="/cgi-bin/graph.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Graph Utility</div>">
                                <img src="/images/graph_icon_inner.gif" onmouseover="src='/images/graph_icon_inner_hover.gif';"
                                    onmouseout="src='/images/graph_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                      <td style="width: 102px" align="Center">
		                                  <a href="/cgi-bin/PeerMain.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
		                                      <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->

<FONT SIZE="5" COLOR="RED">
<B>ERROR:</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" VALUE=" CLOSE " onclick="window.close()"></span>
</FORM>
</CENTER>
</BODY>
</HTML>
   };

   return(0);
}


#----------------------------------------------------------------------------


sub display_form
{
   my ($selected, $key);

   my $desc     = $$ref_old{'KB_DESC'};
   my $notes    = $$ref_old{'KB_NOTES'};
   my $author   = $$ref_old{'KB_AUTHOR'};
   my $verified = $$ref_old{'KB_VERIFIED'};
   my $fmt      = $$ref_old{'KB_FORMAT'};

   my $checked = ($verified eq 'Y') ? 'CHECKED' : '';

   my $action = (defined $$ref_old{'KB_ADDRESS'}) ? 'UPDATE' : 'INSERT';

   my %lookup = (
     'S' => 'STRING',
     'M' => 'MONEY',
     'D' => 'DECIMAL',
     'N' => 'NUMBER',
   );

   my $option_list = '';
   
   foreach $key (keys %lookup) {
      $selected = ($key eq $fmt) ? 'SELECTED' : '';
      $option_list .= "<OPTION VALUE=\"$key\" $selected>$lookup{$key}</OPTION>\n";
   }

   print "Content-Type: text/html\n\n";
   
   print <<EOF
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
<HEAD>
<TITLE>KNOWLEDGE BASE UPDATE TOOL</TITLE>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon">
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css" />

<script type="text/javascript">
function setfocus() {
   document.forms[0].desc.focus();
}
</script>
<link href="/JS/ergosign.styleform.css" media="all" type="text/css" rel="stylesheet">

    <script type="text/javascript" src="/JS/mootools-1.2-core.js"></script>

    <script type="text/javascript" src="/JS/ergosign.styleform.js"></script>

    <script type="text/javascript" src="/JS/script.js"></script>
    <link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />
</HEAD>
<BODY onLoad="setfocus()">
<CENTER>
<BR>
<FORM NAME="form1" ID="form1" METHOD="POST" ACTION="/cgi-bin/snf_kb_update.pl">
<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/index.html" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/cr_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="/cgi-bin/cr_batchx.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="/cgi-bin/snf_batch1.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="/cgi-bin/graph.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Graph Utility</div>">
                                <img src="/images/graph_icon_inner.gif" onmouseover="src='/images/graph_icon_inner_hover.gif';"
                                    onmouseout="src='/images/graph_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    <td style="width: 102px" align="Center">
		                                <a href="/cgi-bin/PeerMain.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
		                                    <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->

<TABLE CLASS="dentry" CELLPADDING="4" WIDTH="400">
   <TR>
      <TD ALIGN="CENTER" COLSPAN="2">
         <FONT SIZE="5" COLOR="GREEN">
         <B>UPDATE KNOWLEDGE BASE</B>
         </FONT>
      </TD>
   </TR>
   <TR>
      <TD ALIGN="RIGHT">ADDRESS:</TD>
      <TD ALIGN="LEFT">
          <INPUT TYPE="text" NAME="address" SIZE="20" MAXLENGTH="18" VALUE="$address" READONLY>
      </TD>
   </TR>
   <TR>
      <TD ALIGN="RIGHT">DESCRIPTION:</TD>
      <TD ALIGN="LEFT">
          <INPUT TYPE="text" NAME="desc" ID="desc" SIZE="50" MAXLENGTH="255" VALUE="$desc" TITLE="ENTER NOTES">
      </TD>
   </TR>
   <TR>
      <TD ALIGN="RIGHT">NOTES:</TD>
      <TD ALIGN="LEFT">
        <TEXTAREA NAME="notes" ROWS="10" COLS="50">$notes</TEXTAREA>
      </TD>
   </TR>
   <TR>
      <TD ALIGN="RIGHT">FORMAT:</TD>
      <TD ALIGN="LEFT">
          <SELECT NAME="fmt">
             $option_list
           </SELECT>
           &nbsp; &nbsp; &nbsp; &nbsp;
           VERFIED: <INPUT TYPE="CHECKBOX" NAME="verified" $checked>
      </TD>
   </TR>
   <TR>
      <TD ALIGN="RIGHT">UPDATED BY:</TD>
      <TD ALIGN="LEFT">
          <INPUT TYPE="text" NAME="author" SIZE="12" MAXLENGTH="12" VALUE="$author">
      </TD>
   </TR>
   <TR>
      <TD ALIGN="CENTER" COLSPAN="2">
          <INPUT TYPE="button" VALUE=" CANCEL " onclick="window.close()">
          <INPUT TYPE="submit" VALUE="  $action RECORD  ">
          <BR>
      </TD>
</TABLE>
<INPUT TYPE="hidden" NAME="action" VALUE="$action">
</FORM>
</CENTER>
</BODY>
</HTML>
EOF
;
}

#----------------------------------------------------------------------------

sub display_success
{
   my $now = localtime();

   print "Content-Type: text/html\n\n";
   
   print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
<HEAD>
<TITLE>KNOWLEDGE BASE UPDATE TOOL</TITLE>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon" />
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css" />
<link href="/JS/ergosign.styleform.css" media="all" type="text/css" rel="stylesheet">

    <script type="text/javascript" src="/JS/mootools-1.2-core.js"></script>

    <script type="text/javascript" src="/JS/ergosign.styleform.js"></script>

    <script type="text/javascript" src="/JS/script.js"></script>
    <link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />
</HEAD>
<BODY>
<CENTER>
<BR>
<!--header start-->
<table cellpadding="0" cellspacing="0" width="885" align="center">
        <tr>
            <td>
                <img src="/images/inner_logo.gif" /></td>
            <td style="background-image: url(/images/inner_header_middle.gif); width: 100%">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 78px" align="Center">
                            <a href="/index.html" class="addToolTip" title="<div id='ToolTipTextWrap'>Home</div>">
                                <img src="/images/home_icon.gif" onmouseover="src='/images/home_icon_hover.gif';" onmouseout="src='/images/home_icon.gif';"
                                    style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 81px" align="Center">
                            <a href="/cgi-bin/cr_search.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Search</div>">
                                <img src="/images/h_search_icon_inner.gif" onmouseover="src='/images/h_search_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_search_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 85px" align="Center">
                            <a href="/cgi-bin/cr_batchx.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Hospital Spreadsheet Generator</div>">
                                <img src="/images/h_spreadsheet_icon_inner.gif" onmouseover="src='/images/h_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/h_spreadsheet_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                        <td style="width: 96px" align="Center">
                            <a href="/cgi-bin/snf_batch1.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>SNF Spreadsheet Generator</div>">
                                <img src="/images/snf_spreadsheet_icon_inner.gif" onmouseover="src='/images/snf_spreadsheet_icon_inner_hover.gif';"
                                    onmouseout="src='/images/snf_spreadsheet_icon_inner.gif';" style="border: 0px;
                                    cursor: pointer" /></a></td>
                        <td style="width: 79px" align="Center">
                            <a href="/cgi-bin/graph.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Graph Utility</div>">
                                <img src="/images/graph_icon_inner.gif" onmouseover="src='/images/graph_icon_inner_hover.gif';"
                                    onmouseout="src='/images/graph_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                      <td style="width: 102px" align="Center">
		                                  <a href="/cgi-bin/PeerMain.pl" class="addToolTip" title="<div id='ToolTipTextWrap'>Peer Group</div>">
		                                      <img src="/images/peer_group_icon_inner.gif" onmouseover="src='/images/peer_group_icon_inner_hover.gif';"
                                    onmouseout="src='/images/peer_group_icon_inner.gif';" style="border: 0px; cursor: pointer" /></a></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="/images/inner_header_right.gif" /></td>
        </tr>
    </table><script language="javascript" src="/JS/tooltip.js"></script>
    <!--header end-->

<TABLE BORDER="1" CELLSPACING="0" CELLPADDING="8" WIDTH="400">
   <TR>
      <TD ALIGN="CENTER">
         <BR>
         <FONT SIZE="5" COLOR="GREEN">
         OPERATION SUCCESSFUL<BR>
         </FONT>
         <FONT SIZE="1">$now</FONT><BR>
         <BR>
      </TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" VALUE=" CLOSE " onclick="window.close()"></span>
</FORM>
</CENTER>
</BODY>
</HTML>
   }
}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#------------------------------------------------------------------------------

