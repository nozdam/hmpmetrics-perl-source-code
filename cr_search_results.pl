#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: exp1.pl
#  author: Steve Spicer
#  Written: Wed Oct 24 20:14:32 CDT 2007
#----------------------------------------------------------------------------

use strict;
use CGI;
use DBI;
do "pagefooter.pl";
do "common_header.pl";
use CGI::Session;

my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $role = $session->param("role");

if(!$userID)
{
	&sessionExpire;
}

my $start_time = time;

my $providerIds ="";  # Building String of Provider IDs Peer Group
my $action    = $cgi->param('action');    
my $provider  = $cgi->param('provider'); 
my $searchProvider  = $cgi->param('provider');
my $name      = $cgi->param('name'); 
my $address   = $cgi->param('address'); 
my $city      = $cgi->param('city');
my $states    = $cgi->param('state'); 
my $zip       = $cgi->param('zip');
my $county    = $cgi->param('county');
my $location  = $cgi->param('location');
my $radius    = $cgi->param('radius');
my $min_ftes  = $cgi->param('min_ftes');  
my $max_ftes  = $cgi->param('max_ftes');
my $min_beds  = $cgi->param('min_beds');
my $max_beds  = $cgi->param('max_beds');
my $toc       = $cgi->param('toc');        # type of control 
my $hosp_type = $cgi->param('HospType');  
my $system    = $cgi->param('system');  
my $msa       = $cgi->param('msa');
my $nfp       = $cgi->param('N_F_Profit');
my $teach_hosp= $cgi->param('Teaching_Hosp');
my $geoArea   = $cgi->param('Geographical_Area');
my $govt      = $cgi->param('Governmental');
my $min_net_income = $cgi->param('min_net_income');
my $max_net_income = $cgi->param('max_net_income');
my $max_Zscore = $cgi->param('max_Zscore');  
my $min_Zscore = $cgi->param('min_Zscore');
my $max_Opr_Margin = $cgi->param('max_Opr_Margin');
my $min_Opr_Margin = $cgi->param('min_Opr_Margin');

# Display search criteria text instead values

my $stateText = $cgi->param('srcStateText');
my $systemText = $cgi->param('srcSystemText');
my $hospText = $cgi->param('srcHospText');
my $tocText = $cgi->param('srcTocText');
my $msaText = $cgi->param('srcMSAText');
my $nfpText = $cgi->param('srcNFPText');
my $teachText = $cgi->param('srcTeachText');
my $geoArText = $cgi->param('srcGeoText');
my $govtText  = $cgi->param('srcGovtText');

# Storing search criteria in session-jagadish .m

$session->param("systxt", $systemText);
$session->param("statetxt", $stateText);
$session->param("hosptxt", $hospText);
$session->param("toctxt", $tocText);
$session->param("msatxt", $msaText);
$session->param("nfptxt", $nfpText);
$session->param("teachtxt", $teachText);
$session->param("geotxt", $geoArText);
$session->param("govttxt", $govtText);

$session->param("providerno", $provider); 	
$session->param("name", $name); 
$session->param("address", $address); 
$session->param("city", $city);  
$session->param("zip", $zip); 
$session->param("county", $county); 
$session->param("location", $location); 
$session->param("radius", $radius); 
$session->param("min_ftes", $min_ftes); 
$session->param("max_ftes", $max_ftes); 
$session->param("min_beds", $min_beds); 
$session->param("max_beds", $max_beds); 
$session->param("toc", $toc); 
$session->param("hosp_type", $hosp_type); 
$session->param("system", $system); 
$session->param("states",$states);
$session->param("msa", $msa); 
$session->param("nfp", $nfp); 
$session->param("teach_hosp", $teach_hosp); 
$session->param("geoArea", $geoArea); 
$session->param("govt", $govt); 
$session->param("min_net_income", $min_net_income); 
$session->param("max_net_income", $max_net_income); 
$session->param("max_Zscore", $max_Zscore); 
$session->param("min_Zscore", $min_Zscore); 
$session->param("max_Opr_Margin", $max_Opr_Margin); 
$session->param("min_Opr_Margin", $min_Opr_Margin); 


my $lstrStore=''; 
my $strStates ='';
my $t = 0;

#foreach my $lstate (@states){if($t>0){$strStates = $strStates .":".$lstate;}else{$strStates = $lstate;}}

my $strToc = '';
my $y = 0 ;

#foreach my $ltoc (@toc){if($y>0){$strToc = $strToc .":".$ltoc;}else{$strToc = $ltoc;}}

# strDisp is used to display the Peer Group in Overlay and strStore is used to insert in database.
# Sequence of fields is maintained as in the interface.

my $lHeight = "100px";
my $tbHeight = 0;

my $lstrDisp = "<table class='gridstyle' style='color:white;' cellpadding=2 cellspacing=2 width='80%' border=1>";
if($provider){
	$lstrDisp = $lstrDisp."<tr><td nowrap>Provider No.</td><td>".$provider."</td></tr>";  
	$lstrStore = $lstrStore .$provider;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}


if ($name){
	$lstrDisp = $lstrDisp."<tr><td>HospName</td><td>".$name."</td></tr>";
	$lstrStore = $lstrStore.":".$name;
	$tbHeight = $tbHeight + 50;
}else{
	$lstrDisp = $lstrDisp."";
}

if ($address){
       $lstrDisp = $lstrDisp."<tr><td>Address </td><td>".$address."</td></tr>";
       $lstrStore = $lstrStore.":".$address;
       $tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}

if ($city){
	$lstrDisp = $lstrDisp."<tr><td>City</td><td>".$city."</td></tr>";
	$lstrStore = $lstrStore.":".$city;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}

if ($states){
	$lstrDisp = $lstrDisp."<tr><td>State(s)</td><td>".$states."</td></tr>";
	$lstrStore = $lstrStore.":".$stateText;

	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
	$lstrStore = $lstrStore.":"."";
}

if ($zip){
	$lstrDisp = $lstrDisp."<tr><td>zip</td><td>".$zip."</td></tr>";
	$lstrStore = $lstrStore.":".$zip;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}

if ($county){
	$lstrDisp = $lstrDisp."<tr><td>County</td><td>".$county."</td></tr>";
	$lstrStore = $lstrStore.":".$county;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}
if ($system){
	$lstrDisp = $lstrDisp."<tr><td>System</td><td>".$system."</td></tr>";
	$lstrStore = $lstrStore.":".$systemText;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
	$lstrStore = $lstrStore.":"."";
}
if ($toc){
	$lstrDisp = $lstrDisp."<tr><td>Type Of Control</td><td>".$toc."</td></tr>";
	$lstrStore = $lstrStore.":".$tocText;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
	$lstrStore = $lstrStore.":"."";
}
if ($hosp_type){
	$lstrDisp = $lstrDisp."<tr><td>Hosp Type</td><td>".$hosp_type."</td></tr>";
	$lstrStore = $lstrStore.":".$hospText;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
	$lstrStore = $lstrStore.":"."";
}
if ($msa){
	$lstrDisp = $lstrDisp."<tr><td>MSA</td><td>".$msaText."</td></tr>";
	$lstrStore = $lstrStore.":".$msa;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
	$lstrStore = $lstrStore.":"."";
}
if ($nfp){
	$lstrDisp = $lstrDisp."<tr><td>N F Profit</td><td>".$nfpText."</td></tr>";
	$lstrStore = $lstrStore.":".$nfp;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
	$lstrStore = $lstrStore.":"."";
}
if ($govt){
	$lstrDisp = $lstrDisp."<tr><td>Governmental</td><td>".$govtText."</td></tr>";
	$lstrStore = $lstrStore.":".$govt;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
	$lstrStore = $lstrStore.":"."";
}

if ($teach_hosp){
	$lstrDisp = $lstrDisp."<tr><td>Teaching Hospital</td><td>".$teachText."</td></tr>";
	$lstrStore = $lstrStore.":".$teach_hosp;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
	$lstrStore = $lstrStore.":"."";
}
if ($geoArea){
	$lstrDisp = $lstrDisp."<tr><td>Geographical Area</td><td>".$geoArText."</td></tr>";
	$lstrStore = $lstrStore.":".$geoArea;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
	$lstrStore = $lstrStore.":"."";
}

if ($min_beds){
	$lstrDisp = $lstrDisp."<tr><td>Min Beds</td><td>".$min_beds."</td></tr>";
	$lstrStore = $lstrStore.":".$min_beds;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}

if ($max_beds){
	$lstrDisp = $lstrDisp."<tr><td>Max Beds</td><td>".$max_beds."</td></tr>";
	$lstrStore = $lstrStore.":".$max_beds;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}

if ($min_ftes){
	$lstrDisp = $lstrDisp."<tr><td>Min FTEs</td><td>".$min_ftes."</td></tr>";
	$lstrStore = $lstrStore.":".$min_ftes;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}

if ($max_ftes){
	$lstrDisp = $lstrDisp."<tr><td>Max FTEs</td><td>".$max_ftes."</td></tr>";
	$lstrStore = $lstrStore.":".$max_ftes;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}
if ($min_net_income){
	$lstrDisp = $lstrDisp."<tr><td>Min Net Income</td><td>".$min_net_income."</td></tr>";
	$lstrStore = $lstrStore.":".$min_net_income;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}

if ($max_net_income){
	$lstrDisp = $lstrDisp."<tr><td>Max Net Income</td><td>".$max_net_income."</td></tr>";
	$lstrStore = $lstrStore.":".$max_net_income;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}
if ($radius){
	$lstrDisp = $lstrDisp."<tr><td>Distance(in miles)</td><td>".$radius."</td></tr>";
	$lstrStore = $lstrStore.":".$radius;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}
if ($location){
	$lstrDisp = $lstrDisp."<tr><td>Location</td><td>".$location."</td></tr>";
	$lstrStore = $lstrStore.":".$location;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}
 
if($min_Zscore){
	$lstrDisp = $lstrDisp."<tr><td>Min Zscore</td><td>".$min_Zscore."</td></tr>";
	$lstrStore = $lstrStore.":".$min_Zscore;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}
 
if($max_Zscore){
 	$lstrDisp = $lstrDisp."<tr><td>Max Zscore</td><td>".$max_Zscore."</td></tr>";
 	$lstrStore = $lstrStore.":".$max_Zscore;
 	$tbHeight = $tbHeight + 20;
}else{
 	$lstrDisp = $lstrDisp."";
}


if($min_Opr_Margin){
	$lstrDisp = $lstrDisp."<tr><td>Min Operating Margin</td><td>".$min_Opr_Margin."</td></tr>";
	$lstrStore = $lstrStore.":".$min_Opr_Margin;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}

if($max_Opr_Margin){
	$lstrDisp = $lstrDisp."<tr><td>Max Operating Margin</td><td>".$max_Opr_Margin."</td></tr>";
	$lstrStore = $lstrStore.":".$max_Opr_Margin;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}  

if($tbHeight<100){$lHeight=$tbHeight."px";}

$lstrDisp = $lstrDisp."</table>";

my $remote_user  = $ENV{'REMOTE_USER'};

my %toc_desc = (
  '1'  => 'Voluntary Nonprofit, Church',
  '2'  => 'Voluntary Nonprofit, Other',
  '3'  => 'Proprietary, Individual',
  '4'  => 'Proprietary, Corporation',
  '5'  => 'Proprietary, Partnership',
  '6'  => 'Proprietary, Other',
  '7'  => 'Governmental, Federal',
  '8'  => 'Governmental, City-County',
  '9'  => 'Governmental, County',
  '10' => 'Governmental, State',
  '11' => 'Governmental Hospital District',
  '12' => 'Governmental, City',
  '13' => 'Governmental, Other',
);

my %type_desc = (
  '1' => 'General Short Term ',
  '2' => 'General Long Term',
  '3' => 'Cancer',
  '4' => 'Psychiatric',
  '5' => 'Rehabilitation',
  '6' => 'Religious Non-medical Health Care Institution',
  '7' => 'Children',
  '8' => 'Alcohol and Drug',
  '9' => 'Other',
);


my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my $defaultYear= $ini_value{'DEFAULT_YEAR'};

my $dbh;

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}
 
unless ($action eq 'go') {
   &display_form();
   exit(0);
} else {
   &query_database();
}

$dbh->disconnect();
exit(0);

open(J, ">data.txt");
						print J $remove_system;
						close J;

#----------------------------------------------------------------------------

sub query_database
{
   my (@list, $row, $op, $n, $class, $string,$opr);
   my ($inlist, $i,$j);

   my @where_list = ();
   my @sql_args   = ();
   my @error_list;


   if ($min_ftes) {
      unless ($min_ftes =~ m/^\d+/) {
         push @error_list, "MINIMUM FTES MUST BE NUMERIC";
      }
   }

   if ($provider) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op h.ProviderNo = ?\n";
      push @sql_args, $provider;
   }

   if ($name) {
   
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
	   #$opr=$name;
      push @where_list, "$op h.HospitalName LIKE '$name%' \n";
      #push @sql_args, $name;
   }

   if ($address) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op h.HospitalStreet LIKE '$address%'\n";
      #push @sql_args, $address;
   }

   if ($city) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op h.HospitalCity LIKE '$city%'\n";
      #push @sql_args, $city;
   }

   $n  = @where_list;
   $op = ($n) ? 'AND' : 'WHERE';

   #$i = @states;

   #if ($i) {
   #   unless ($states[0] eq '') {
   #      $inlist = join "\", \"", @states;
   #      push @where_list, "$op h.STATE IN (\"$inlist\")\n";
   #   }
   #}

 if ($stateText) {
    $n = @where_list;
    $op = ($n) ? 'AND' : 'WHERE';
    push @where_list, "$op h.HospitalState IN($stateText)\n";
    #push @sql_args, $state;
 }

   if ($zip) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op h.HospitalZipCode = ?\n";
      push @sql_args, $zip;
   }

   if ($county) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op h.HospitalCounty LIKE '$county%'\n";
      #push @sql_args, $county;
   }

   my $workSheetLeftJoin = '';
   if (($min_beds)||($max_beds)||($min_ftes)||($max_ftes)||($min_net_income)||($max_net_income) || ($nfp) ||($geoArea)||($teach_hosp))
   {
   	$workSheetLeftJoin = " left outer join CR_" . $defaultYear . "_RPT as rpt on rpt.PRVDR_NUM = h.ProviderNo ";
   }
   
   my $bedsLeftJoin = '';
   if (($min_beds)||($max_beds))
   {
   	$bedsLeftJoin = " left outer join CR_" . $defaultYear . "_DATA as dataBed on dataBed.CR_REC_NUM = rpt.RPT_REC_NUM ";
   	$n = @where_list;
	$op = ($n) ? 'AND' : 'WHERE';
      	push @where_list, "$op dataBed.CR_WORKSHEET = 'S300001' and dataBed.CR_LINE= '02500' and dataBed.CR_COLUMN = '0100' \n";
   }
   
   if ($min_beds) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op dataBed.CR_VALUE >= (select 0 + ? )\n";
      push @sql_args, $min_beds;
   }

   if ($max_beds) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op dataBed.CR_VALUE <= (select 0 + ? )\n";
      push @sql_args, $max_beds;
   }  
   
   my $ftesLeftJoin = '';
   if (($min_ftes)||($max_ftes))
   {
   	$ftesLeftJoin = " left outer join CR_" . $defaultYear . "_DATA as dataFte on dataFte.CR_REC_NUM = rpt.RPT_REC_NUM ";
   	$n = @where_list;
	$op = ($n) ? 'AND' : 'WHERE';
      	push @where_list, "$op dataFte.CR_WORKSHEET = 'S300001' and dataFte.CR_LINE= '01200' and dataFte.CR_COLUMN = '1000' \n";
   }
   
   if ($min_ftes) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op dataFte.CR_VALUE >= (select 0 + ? )\n";
      push @sql_args, $min_ftes;
   }

   if ($max_ftes) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op dataFte.CR_VALUE <= (select 0 + ? )\n";
      push @sql_args, $max_ftes;
   }   
   
   my $incomeLeftJoin = '';
   if (($min_net_income)||($max_net_income))
   {
   	$incomeLeftJoin = " left outer join CR_" . $defaultYear . "_DATA as dataIncome on dataIncome.CR_REC_NUM = rpt.RPT_REC_NUM ";
   	$n = @where_list;
	$op = ($n) ? 'AND' : 'WHERE';
      	push @where_list, "$op dataIncome.CR_WORKSHEET = 'G300000' and dataIncome.CR_LINE= '03100' and dataIncome.CR_COLUMN = '0100' \n";
   }
   
   if ($min_net_income) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op dataIncome.CR_VALUE >= (select 0 + ? )\n";
      push @sql_args, $min_net_income;
   }

   if ($max_net_income) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op dataIncome.CR_VALUE <= (select 0 + ? )\n";
      push @sql_args, $max_net_income;
   }
   
   if ($tocText) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op h.HMPControl IN ($tocText)\n";    
   }   
   
   if ($min_Zscore) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op p.Z_Score >= (select 0 + ? )\n";
      push @sql_args, $min_Zscore;   
   }
   
   if ($max_Zscore) {
         $n = @where_list;
         $op = ($n) ? 'AND' : 'WHERE';
         push @where_list, "$op p.Z_Score <= (select 0 + ? )\n";
         push @sql_args, $max_Zscore;   
   }
   
   if ($min_Opr_Margin) {
	 $n = @where_list;
	 $op = ($n) ? 'AND' : 'WHERE';
	 push @where_list, "$op p.Operating_Margin >= (select 0 + ? )\n";
	 push @sql_args, $min_Opr_Margin;   
   }

   if ($max_Opr_Margin) {
	    $n = @where_list;
	    $op = ($n) ? 'AND' : 'WHERE';
	    push @where_list, "$op p.Operating_Margin <= (select 0 + ? )\n";
	    push @sql_args, $max_Opr_Margin;   
   }
   
   my $hTypeLeftJoin = '';
   if ($hosp_type) {
      #$hTypeLeftJoin = " left outer join CR_" . $defaultYear . "_DATA as dataHtype on dataHtype.CR_REC_NUM = rpt.RPT_REC_NUM ";
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      #push @where_list, "$op dataHtype.CR_WORKSHEET = 'S200000' and dataHtype.CR_LINE= '01900' and dataHtype.CR_COLUMN = '0100' and dataHtype.CR_VALUE IN ($hospText)\n";
      #push @sql_args, $hosp_type;  
		push @where_list, "$op h.HMPFacilityType IN ($hospText)\n";  
   }	
   
   #need to add left join part for MSA. 
   
   my $leftJoinMSA = '';
   
   if ($msa) {
      $n = @where_list;
      $op = ($n) ? 'AND' : 'WHERE';
      push @where_list, "$op msa.MSACode in ($msa)\n";
      #push @sql_args, $msa;
      $leftJoinMSA = " left outer join zipcodes as zip on zip.zipcode = h.ShortZip left outer join msa on msa.MSACode = zip.msa ";
   }

   if ($system) {   
      if(substr($system,1,4) eq 'none'){
	  $n = @where_list;
	  $op = ($n) ? 'AND' : 'WHERE';
	  push @where_list, "$op h.SystemID IS NULL\n";    
      }else{
      	$n = @where_list;
      	$op = ($n) ? 'AND' : 'WHERE';
      	push @where_list, "$op h.SystemID IN($systemText) \n";
      	#push @sql_args, $system;
      } 
   }
  # else
  # {
  #    	$n = @where_list;
  #    	$op = ($n) ? 'AND' : 'WHERE';
  #    	push @where_list, "$op h.SYSTEM IS NOT NULL\n";      	
  # }

#------------New fields----------------------------------------

my $nfpLeftJoin = '';
if ($nfp){
   	$nfpLeftJoin = " left outer join CR_" . $defaultYear . "_DATA as dataNFP on dataNFP.CR_REC_NUM = rpt.RPT_REC_NUM inner join control_type on control_type.Control_Type_id = h.TypeofControl";
   	$n = @where_list;
	$op = ($n) ? 'AND' : 'WHERE';
      	push @where_list, "$op dataNFP.CR_WORKSHEET = 'S200000' and dataNFP.CR_LINE= '01800' and dataNFP.CR_COLUMN = '0100' and control_type.Not_For_Profit = '$nfp'\n";
      	
      	if ($nfp eq 'T') {
      	
      		if(($govt eq 'T') || ($govt eq 'F'))
      		{
      			push @where_list, "and control_type.Governmental = '$govt'\n";
      		}      		
      	
      	}
}

my $teachinghosp = '';
if ($teach_hosp){
   	$teachinghosp = " left outer join CR_" . $defaultYear . "_DATA as dataTeach on dataTeach.CR_REC_NUM = rpt.RPT_REC_NUM";
   	$n = @where_list;
	$op = ($n) ? 'AND' : 'WHERE';
      	push @where_list, "$op dataTeach.CR_WORKSHEET = 'S200000' and dataTeach.CR_LINE= '02500' and dataTeach.CR_COLUMN = '0100' and dataTeach.cr_value = '$teach_hosp' \n";
}

my $geoArLeftJoin = '';
if ($geoArea){
   	$geoArLeftJoin = " left outer join CR_" . $defaultYear . "_DATA as dataGeoAr on dataGeoAr.CR_REC_NUM = rpt.RPT_REC_NUM";
   	$n = @where_list;
	$op = ($n) ? 'AND' : 'WHERE';
      	push @where_list, "$op dataGeoAr.CR_WORKSHEET = 'S200000' and dataGeoAr.CR_LINE= '02103' and dataGeoAr.CR_COLUMN = '0100' and dataGeoAr.cr_value = '$geoArea'\n";
}
#------------End of new fields---------------------------------
  
  my ($sql, $sth);

   my $geosql = '';

   if ($location && $radius) {

      my ($zip_sth, $lat1, $lon1);

      my $zip_sql = qq{
         SELECT city, state, latitude, longitude
           FROM zipcodes
          WHERE zipcode = ?
      };
   
      unless ($zip_sth = $dbh->prepare($zip_sql)) {
         &display_error('ERROR preparing SQL', $zip_sth->errstr); 
         exit(1);
      }

      unless ($zip_sth->execute($location)) {
         &display_error('ERROR executing SQL', $zip_sth->errstr); 
         exit(1);
      }

      my ($z_city, $z_state, $lat1, $lon1) = $zip_sth->fetchrow_array;

      unless ($lat1) {
         &display_error("NO COORDINATES AVAILABLE FOR $location"); 
         exit(1);
      }
         
      $zip_sth->finish;

      #---------------------------------------------------------------------

      $geosql = qq{
    	  ,3963.0 * acos(sin(radians($lat1)) * sin(radians(z.latitude))
 	  + cos(radians($lat1)) * cos(radians(z.latitude))
 	  * cos(radians(z.longitude) - radians($lon1))) as distance
      };

      push @where_list, "HAVING distance < $radius\n";
      push @where_list, "ORDER BY distance ASC\n";
   }

   $sql = qq{
SELECT distinct h.ProviderNo, h.TypeofControl, h.HospitalName,
          h.HospitalStreet, h.HospitalCity, h.HospitalState, h.HospitalZipCode,
		h.HospitalCounty,p.Z_Score,z.latitude as LATITUDE,z.longitude as LONGITUDE,sys.SystemName as SYSTEM_NAME,h.SystemID
			 $geosql 
     FROM tblhospitalmaster as h
	 LEFT OUTER JOIN zipcodes as z on h.ShortZip=z.zipcode
	 LEFT OUTER JOIN hospitals as p on h.ProviderNo=p.PROVIDER_NUMBER
      LEFT JOIN tblsystemmaster as sys ON sys.SystemID = h.SystemID
     $leftJoinMSA 
     $workSheetLeftJoin
     $bedsLeftJoin
     $ftesLeftJoin
     $incomeLeftJoin
     $hTypeLeftJoin
     $nfpLeftJoin
     $geoArLeftJoin
     $teachinghosp
     @where_list
   };
   
   open(J, ">data.txt");
				print J $sql;
				close J;

#     LIMIT 800

   unless ($sth = $dbh->prepare($sql)) {
       &display_error('ERROR preparing SQL', $sth->errstr);
       $dbh->disconnect();
       exit(1);
   }


   unless ($sth->execute(@sql_args)) {
       &display_error('ERROR executing SQL', $sth->errstr, $sql);
       $dbh->disconnect();
       exit(1);
   }
  
   my $rows_returned = $sth->rows();
   
   unless ($rows_returned) {
      &display_error(" Sorry, no matches were found. Please change the search criteria and try again.");
      $dbh->disconnect();
      return(0);
   }

   my $aref = $sth->{'NAME'};

   my @column_headings = @$aref;

   print "Content-Type: text/html\n\n";

   print <<EOF
<HTML xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<HEAD><TITLE>COST REPORTS SEARCH TOOL</TITLE>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon" />
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css" />
<link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />

<style>
/*--CSS for Peer Group Display Box--*/
#displaybox { 	
	 width:50%;
	 height:40%;
	 z-index:10000;
	}
</style>

<SCRIPT TYPE="text/javascript">
/*-- Function to check/uncheck on click of check box --*/
function CheckUncheck(obj){
	if(obj.checked){
		
		// function call
		CheckAll();
	}else{
	
		// function call
		UnCheckAll();
	}
}
/*--Function to select all providers--*/
function CheckAll() {
   var i;      
   var len = document.form1.SelectedHosps.length;
   if(len>1){
   	for (i = 0; i < len; i++) {         
         	document.form1.SelectedHosps[i].checked = true;
   	}
   }else{
   	document.form1.SelectedHosps.checked = true;
   }	
}

/*--Function to deselect all providers--*/
function UnCheckAll() {
   var i;

   var len = document.form1.SelectedHosps.length;
   if(len>1){
      for (i = 0; i < len; i++) {         
            document.form1.SelectedHosps[i].checked = false;    
            
            var loopid = "foi"+i
            if(document.getElementById(loopid)!=null)
            {
            	if(document.getElementById(loopid).style.visibility =='visible')
            	{
            		document.getElementById(loopid).style.visibility ='hidden';
            		document.peerFrom.foiId.value = ''; 			
			document.form1.foiName.value = '--Not Specified--';
            	}
            }
            
      }
   }else{
   	document.form1.SelectedHosps.checked = false;
   	document.getElementById("foi1").style.visibility ='hidden';
	document.peerFrom.foiId.value = ''; 			
	document.form1.foiName.value = '--Not Specified--';
   }
}

/*--Function to find peer sample size--*/
function smplSize(){
   var i;
   var count=0;
   var len = document.form1.SelectedHosps.length;   
      
   if(len>1){
      for (i = 0; i < len; i++) {         
          if(document.form1.SelectedHosps[i].checked){
        	count++;
          }
      }
   }else{   	    
   	if(document.form1.SelectedHosps.value!=""){
	      	if(document.form1.SelectedHosps.checked)
	      	 {count = 1;}else{count = 0;}
   	}
   }   
   
   return count;
}

/*--Function to Build String of 'all'/'only selected' provider number separated by ':' --*/
function callsubmit(){
	var i;

	var idStr;
	var nameStr;
	var lStr;
	var selected = false;
	var len = document.form1.SelectedHosps.length;		
	
	//Search and get provider number from selected checkboxes
	if(len>1){
		for (i = 0; i < len; i++) {         
		      if(document.form1.SelectedHosps[i].checked){
		      	selected = true;
		        	
		        	idStr = document.form1.SelectedHosps[i].value;
		           	nameStr = document.form1.providerName[i].value;
		           	
		        	// Dont add ':' in the beginbing of the string.
		        	if(i == 0){ 		           		
		           		lStr = idStr + " - " + nameStr;
		        	}else{   
			   				           		
			   		lStr = lStr + ":"+idStr + " - " + nameStr;
			   					   		
				}   
		      }		
   		}
   	}else{
   		// Find out the check boxes length.			
		if(document.form1.SelectedHosps.value!=""){
			if(document.form1.SelectedHosps.checked){
				 selected = true;
			  	 len = 1;
			  	 lStr = document.form1.SelectedHosps.value;
			}else{len=0;}
   		}
   	}	
   	
   	// Assign provider Numbers string to hidden textbox.
   	document.form1.Ids.value = lStr;
	//var ids = document.form1.Ids.value;
	

	// Check if atleast one provider is selected.	
	if(selected){
		//location.href="/cgi-bin/cr_batchx.pl?imp=Y&Ids="+ids;
		document.form1.action = "/cgi-bin/cr_batchx.pl";
		document.form1.submit();
	}else{
		alert("Please select the Provider(s)")

	}	
}
/*--Check duplicate peer group name--*/
function CheckDub(pname){				
	if(document.form1.peergrpname.value != ""){
		peerCheck.location.href="/cgi-bin/PeerActions.pl?saction=Check&peername="+pname;		
	}    
	return false;
}

function CheckDubOnSave(){	
	var pname = document.form1.peergrpname.value;
	if(document.form1.peergrpname.value != ""){
		peerCheck.location.href="/cgi-bin/PeerActions.pl?saction=CheckSave&peername="+pname;		
	}    
	return false;
}
/*--JavaScript Function to close Peer Group Display Box--*/

function clicker(){
	
	var thediv = document.getElementById('displaybox');	
	var bodyDiv = document.getElementById('bodyDiv');
	
	var js_lstrpids = '';
	var js_lstrpnames = '';
	
	var js_providerID;
	var js_providerName;	
	var len = document.form1.SelectedHosps.length;
	var i;
	
	// Find out count of selected providers.
	var sampleSize = smplSize();	
	
	var selected = false;
	var FOIName = document.form1.foiName.value;	
	
	//Display overlay only when atleast one provider is selected.
	if(sampleSize > 0){ 
	
		//Build the overlay.
		var strHTML = "<br><table cellpadding='0' cellspacing='0' bgColor='#87AEC5'  class='PeerGrouptbl' align='center' width='100%'><tr class='gridHeader'><td style='padding-left:10px;'>Create new peer group</td><td align='right' style='padding-right:10px;'><a href='#' onclick='return clicker();' class='gridHlink'>Close</a></td></tr>";
		strHTML = strHTML + "<tr class='gridalternate'><td colspan='2' class='gridstyle' align='center' nowrap>Facility Of Interest : " + FOIName + "</td></tr>";
		strHTML = strHTML + "<tr><td colspan='2' valign='center' style='padding-top:10px;'><table cellpadding='0' cellspacing='0' align='center' class='gridstyle' width='80%' ><tr><td class='PeerGrpfont' nowrap align='right'>Peer Group Name :</td><td nowrap><input type='text' name='peergrpname' style='width:268px;' maxLength='50' onblur='CheckDub(this.value)'>&nbsp;<font color=red>*</font></td></tr>";
		strHTML = strHTML + "<tr><td nowrap class='PeerGrpfont' align='right'>Search Criteria :</td><td><div style='position:static;overflow:auto;height:$lHeight;width:100%;'>$lstrDisp</div></td></tr>";
		strHTML = strHTML + "<tr><td nowrap class='PeerGrpfont' align='right'>Description :</td><td><input type='text' name='description' style='width:280px;' maxLength='200'></td></tr>";
		strHTML = strHTML + "<tr><td></td><td><span class='button'><input type='button' name='save' onclick='return CheckDubOnSave();' value='Save' ></span><span class='button'><input type='button' onclick='return clicker();' value=' Cancel '></span></td></tr></table></td></tr>";
		strHTML = strHTML + "<tr><td colspan='2' style='padding-left:10px;padding-top:10px;padding-bottom:10px;' valign='top'><div style='height:300px;width:100%;overflow:auto; class='PeerGroupOverlay'><table width='100%' class='gridstyle'><tr class='gridHeader'><td align='center' nowrap>Provider #</td><td>Provider Name<span style='padding-left:180px;'>Size : "+sampleSize+"</span></td></tr>";
	
		if(thediv.style.display == "none"){
			bodyDiv.style.display = "";
			thediv.style.display = "";
						
			if(len>1){
			
				// For multiple rows.
		  		for (i = 0; i < len; i++) {   		     
		     		if(document.form1.SelectedHosps[i].checked == true){		     	
				selected = true;
				js_providerID = document.form1.SelectedHosps[i].value;
			      	js_providerName = document.form1.providerName[i].value;	
			      	
			      	if(i==0){		      		
			      		js_lstrpids = js_lstrpids + js_providerID;
			      		js_lstrpnames = js_lstrpnames + js_providerName;
			      	     }else{		      	     	
			      	     	js_lstrpids = js_lstrpids +":"+js_providerID;
			      		js_lstrpnames = js_lstrpnames +":"+js_providerName;
			      	     }		
				      	
				// Add css to show alternative row colors      	
			      	if(i%2==0){
			      	  strHTML = strHTML+"<tr class='gridrow'><td align='center'>"+js_providerID+"</td><td>"+js_providerName+"</td></tr>";
			      	}else{
			      	  strHTML = strHTML+"<tr class='gridalternate'><td align='center'>"+js_providerID+"</td><td>"+js_providerName+"</td></tr>";
			      	}		      
			      }
			   }   
			 }else{     
			 
			 	//For single row.
			      if(document.form1.SelectedHosps.checked == true){
			    	if(document.form1.SelectedHosps.checked == true){
				   selected = true;
				   js_providerID = document.form1.SelectedHosps.value;
				   js_providerName = document.form1.providerName.value;	
						 
				   js_lstrpids = js_lstrpids + js_providerID;
				   js_lstrpnames = js_lstrpnames + js_providerName;
				    
				   // Add css to show alternative row colors  
			           if(i%2==0){
				      strHTML = strHTML+"<tr class='gridrow'><td align='center'>"+js_providerID+"</td><td>"+js_providerName+"</td></tr>";
				   }else{
				     strHTML = strHTML+"<tr class='gridalternate'><td align='center'>"+js_providerID+"</td><td>"+js_providerName+"</td></tr>";
				   }
			        }
			      }		    
	   		  }   		
			strHTML = strHTML +"</table></div></td></tr></table>";	
			
			//insert html string in the div.
			
			thediv.innerHTML = strHTML;
			document.form1.peergrpname.focus();
		}else{
			
			//Hide the Peer Group Overlay			
			bodyDiv.style.display = "none";
			thediv.style.display = "none";
			thediv.innerHTML = '';		
		}
		
		// Assign buil string of providerno. and name to hidden textboxes.
		document.peerFrom.strpids.value = js_lstrpids;
		document.peerFrom.strpnames.value = js_lstrpnames;	
	}else{
		//alert if no provider is selected
		alert("Please select the required provider(s) for Peer Group");	
	}	
	return false;
}

/*-- Function to Submitting the peer group details to database--*/
function PeerBuild(){	
	
	//charpattern = /^[a-zA-Z0-9\'\,\(\)\-\.\:\|\-\/\_\&\+\#\s]+$/
	var valid=true;
	var fnamestr = document.form1.peergrpname.value	
	
	//Validation for Peer Group Name
	if (document.form1.peergrpname.value == "") {
		alert("Please enter peer group name.");
		valid = false;
		document.form1.peergrpname.focus();		
	}else
	if (!isNaN(document.form1.peergrpname.value)) {
		alert("Only characters are allowed.");
		valid = false;
	        document.form1.peergrpname.value ="";
		document.form1.peergrpname.focus();		
	}/*else	
	if(!fnamestr.match(charpattern)){
                alert("Special characters are not allowed.");
                valid=false;
                document.form1.peergrpname.value="";
		document.form1.peergrpname.focus();		
        }*/
        if(valid){
		document.peerFrom.peername.value = document.form1.peergrpname.value;
		document.peerFrom.desc.value = document.form1.description.value;
		document.peerFrom.submit();			
	}	
}

/*-- Show hide reports menu --*/
function showMenu(id,rows){	
	
	var menuID = document.getElementById(id);
	
	// for more than one row.
	for (var i=1;i<=rows;i++){  
		if(i == id){ 
			// show current row menu
			document.getElementById(i).style.visibility ='visible';		
		}else{  
			//Hide all other rows menus
			document.getElementById(i).style.visibility ='hidden';
		}
	}
	
	// show menu when only one row in search result.
	if(id == 1){ 
		menuID.style.visibility = 'visible';//(menuID.style.visibility == 'visible') ? 'hidden' : 'visible';
	}
	

	// Hide menu when only one row in search result
	if(id == 0){ 
		document.getElementById('1').style.visibility = 'hidden';
	}   
		
}
/*-- Function to set Facility of Interest--*/
function setFOI(id,rows){
   
   var 	id ="foi"+id;
   var sampleSize = document.form1.SelectedHosps.length;   
   if(sampleSize>1){
   	for (var i=1;i<=rows;i++){  
   	     var j = i-1;
   	     var loopid = "foi"+i;
	     if(loopid == id){ 
		// show current row menu
		if(document.form1.SelectedHosps[j].checked == true)
		{
			document.getElementById(loopid).style.visibility ='visible';	
			document.form1.SelectedHosps[j].checked = true;
			document.peerFrom.foiId.value = document.form1.SelectedHosps[j].value; 			
			document.form1.foiName.value = document.form1.providerName[j].value;
		}
		else
		{
			alert("Please select the provider before marking as FOI");
			break;
		}
	      }else{  
		//Hide all other rows menus
		document.getElementById(loopid).style.visibility ='hidden';
		//document.form1.SelectedHosps[j].checked = false;
	       }
	}
    }else{    	
    	document.getElementById(id).style.visibility ='visible';	
	document.form1.SelectedHosps.checked = true;
	document.peerFrom.foiId.value = document.form1.SelectedHosps.value; 			
	document.form1.foiName.value = document.form1.providerName.value;
    }			
}

/*-- Function to uncheck the FOI for individual hospitals --*/
function UncheckFOI(id,rows)
{
	var 	id ="foi"+id;
   	var sampleSize = document.form1.SelectedHosps.length;   
   	if(sampleSize>1){
   		for (var i=1;i<=rows;i++){
   			var j = i-1;
   	     		var loopid = "foi"+i;
   	     		if(loopid == id)
   	     		{
   	     			if(document.form1.SelectedHosps[j].checked == false)
   	     			{
   	     				document.getElementById(loopid).style.visibility ='hidden';
   	     				document.peerFrom.foiId.value = ''; 			
					document.form1.foiName.value = '--Not Specified--';
   	     			}	
   	     		}
   		}   		
   	}
   	else
   	{
   		if(document.form1.SelectedHosps.checked == false)
   		{   			
   			document.getElementById(id).style.visibility ='hidden';
   			document.peerFrom.foiId.value = ''; 			
			document.form1.foiName.value = '--Not Specified--';
   		}	
   	}
}

</SCRIPT>
</HEAD>
<BODY>
<BR>

<FORM NAME="form1" METHOD="POST" ACTION="/cgi-bin/cr_search.pl">
<div align="center" id='bodyDiv' style="position:absolute;z-index:100;display:none;background-image:url(/images/images/background-trans.png);height:100%;width:100%;"><div align='center'  id="displaybox" style="display: none;"></div></div><input type='hidden' name='sess_clear' value=\"false\">
<CENTER>
EOF
;
print <<EOF

    <!--content start-->
    
        <table cellpadding="0" cellspacing="0" width="885px">
            <tr>
                <td valign="top">
                    <img src="/images/content_left.gif" /></td>
            <td valign="top" style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px">

<div style="float:left" class="pageheader">SEARCH RESULTS ( $rows_returned results )</div>
<div style="float:right; padding-bottom:5px">
 <table>
	<tr>		
		<td valign="center"><!--<span class="button"><INPUT TYPE="button" VALUE=" CHECK ALL"   onClick="CheckAll(document.form1.chosen)"></span>--></td>
      		<td valign="center"><!--<span class="button"><INPUT TYPE="button" VALUE=" UNCHECK ALL" onClick="UnCheckAll(document.form1.chosen)"></span>--></td>
      		<td valign="bottom"><span class="button"><INPUT TYPE="button" VALUE="REFINE SEARCH CRITERIA" style="width: 155px" onclick='javascript:document.form1.submit();'></span></td>
      		<td valign="bottom"><span class="button"><INPUT TYPE="button" VALUE="CREATE PEER GROUP" style="width: 135px" onclick='return clicker();'></span></td>
EOF
;      	
      	if($role eq '2')
   		{
    			print qq{
      		<td valign="bottom"><span class="button"><INPUT TYPE="button" VALUE="SPREADSHEET GENERATOR" style="width: 170" onclick="callsubmit()"></span></td>      		
      		};
      	    }else{
      	    	print qq{
		     <td valign="bottom"></td>   	    	
      	    	};
      	    }
print qq{      	    
      	</tr>
 </table>     	
</div>
  <TABLE NAME="TBL1" BORDER="0" CELLPADDING="0" align="center" width="100%" class="gridstyle">
  <TR class='gridHeader'>
  	<TD>
  		Provider #  
  	</TD>
  	<TD>
  		Provider Name 
  	</TD>	
  	<TD>
		Address
  	</TD><TD></TD>  	
  	<TD>
  		<a href="#" ><input type=\"checkbox\" name=\"Select\" onclick="CheckUncheck(this)"></a>
	  	
  	</TD>
  </TR>
};
 
   open OUTPUT,">userdata/$remote_user.txt";

   my $count = 0;
   my $year = 2006;
   
   
   my ($provider, $link, $lat, $lon, $hosp, $fye, $type, $maplink);
   my ($cr_link, $drg_link, $src_link, $calc_link, $chart_link);
   my $edit_net;

   while ($row = $sth->fetchrow_hashref) {

     $provider = $$row{ProviderNo};
     $lat = $$row{LATITUDE};   # yep mixed up
     $lon = $$row{LONGITUDE};     
     $type = $type_desc{$$row{HOSP_TYPE}};

     if($count > 0){
      $providerIds = $providerIds . ":".$provider;
     }else{
       $providerIds = $providerIds . $provider;
     }
      
     $cr_link  = "<A HREF=\"/cgi-bin/choose1.pl?provider=$provider\" class=\"gridHlink\" style=\"font-weight:bold;\">COST REPORTS</A>";
     $drg_link = "<A HREF=\"/cgi-bin/drg_mix.pl?provider=$provider\" TITLE=\"DRG MIX\" class=\"gridHlink\" style=\"font-weight:bold;\">DRG MIX</A>";
     $src_link = "<A HREF=\"/cgi-bin/derek3.pl?provider=$provider\" TITLE=\"WHERE PATIENTS COME FORM\" class=\"gridHlink\" style=\"font-weight:bold;\">PATIENT SOURCE</A>";
     $calc_link = "<A HREF=\"/cgi-bin/cr_calc.pl?provider=$provider\" TITLE=\"FINANCIAL CALCULATIONS\" class=\"gridHlink\" style=\"font-weight:bold;\">CALCULATIONS</A>";
     $chart_link = "<A HREF=\"/cgi-bin/graph.pl?providers=$provider\" class=\"gridHlink\" style=\"font-weight:bold;\">CHART</A>";
     
     #$fye = substr($$row{FYE},5,5);

     $hosp = $$row{HospitalName};

     if ($lat && $lon) {
        $maplink = "<A HREF=\"/cgi-bin/cr_map.pl?lat=$lat&lon=$lon&desc=$hosp\"><IMG BORDER=\"0\" SRC=\"/icons/gm_button.gif\"></A>";
     } else {
        $maplink = 'No Map';
     }

     #$edit_net = &comify($$row{NET_INCOME}); //commented 5 oct

     $class = (++$count % 2) ? 'gridrow' : 'gridalternate';
           
     print "  <TR CLASS=\"$class\" onclick=\"showMenu(0,$rows_returned)\">\n";
     print "     <TD ALIGN=\"LEFT\" VALIGN=\"TOP\" style=\"padding:5px\" >\n";
     print "         <a href=\"#\" onmouseover=\"showMenu($count ,$rows_returned)\" ><B>$provider</B></a><BR>\n";              
     print "         <input type=\"hidden\" name=\"providerName\" value=\"$$row{HospitalName}\">\n";
     print "	<div id=\"$count\" class=\"$class\" align=\"right\" style=\"border:2px solid #36648B;top;100px;left:75px;position:absolute;visibility:hidden;\"> <font face=\"verdana\"> <table> <tr bgcolor=\"#B9D3EE\"><td colspan=2 align=\"center\" style=\"height:30px;border-bottom:0px solid #FFFFFF;color:#000000;font-family:Helvetica;font-size:12px;font-weight:bold;\" nowrap >Reports</td></tr> <tr bgcolor=\"#B9D3EE\"> <td style=\"border-bottom:0px solid #FFFFFF;\" nowrap></td> <td align=\"left\" colspan=2 style=\"border-bottom:0px solid #FFFFFF;\"><A HREF=\"#\" onclick=\"setFOI($count ,$rows_returned)\" class=\"gridHlink\" style=\"font-weight:bold;\">Mark as FOI</A></td></tr><tr bgcolor=\"#B9D3EE\"><td style=\"border-bottom:0px solid #FFFFFF;\" nowrap><img src=\"/images/costreport_icon.gif\" /></td><td style=\"border-bottom:0px solid #FFFFFF;font-family:Verdana;font-size:11px\" nowrap>$cr_link</td></tr><tr bgcolor=\"#B9D3EE\"><td style=\"border-bottom:0px solid #FFFFFF;\" nowrap><img src=\"/images/drgmix_icon.gif\" /></td><td style=\"border-bottom:0px solid #FFFFFF;font-family:Verdana;font-size:11px\" nowrap>$drg_link</td></tr><tr bgcolor=\"#B9D3EE\"><td style=\"border-bottom:0px solid #FFFFFF;\" nowrap><img src=\"/images/patientsource_icon.gif\" /></td><td style=\"border-bottom:0px solid #FFFFFF;font-family:Verdana;font-size:11px\" nowrap>$src_link</td></tr><tr><td align=\"center\" colspan=2 style=\"border-bottom:0px solid #FFFFFF;\">$maplink</td></tr></table></div>\n";
     #<tr bgcolor=\"#B9D3EE\"><td style=\"border-bottom:0px solid #FFFFFF;\" nowrap><img src=\"/images/calculation_icon.gif\" /></td><td style=\"border-bottom:0px solid #FFFFFF;font-family:Verdana;font-size:11px\" nowrap>$calc_link</td></tr><tr bgcolor=\"#B9D3EE\"><td style=\"border-bottom:0px solid #FFFFFF;\" nowrap><img src=\"/images/chart_icon.gif\" /></td><td style=\"border-bottom:0px solid #FFFFFF;font-family:Verdana;font-size:11px\" nowrap>$chart_link</td></tr>
     print "     </TD>\n";
     print "	<TD ALIGN=\"LEFT\" VALIGN=\"TOP\" style=\"padding:5px\">\n";
     print "		<B>$$row{HospitalName}</B><BR>\n";     
     print "	</TD>\n";
     print "	<TD ALIGN=\"LEFT\" VALIGN=\"TOP\" style=\"padding:5px\">\n";
     print "         <B>$$row{HospitalStreet}</B><BR>\n";
     print "         <B>$$row{HospitalCity} $$row{HospitalState}  $$row{HospitalZipCode}</B><BR><BR>\n";   
     if($$row{SYSTEM_NAME}){
     	print "         <B><font style=\"color:darkred;\">System :</font></B><B>$$row{SYSTEM_NAME}</B>\n";
     }
     print "	</TD>\n";     
     print "<TD style=\"padding-right:10px\"><img id=\"foi$count\" src=\"/images/FOI.gif\" style=\"visibility:hidden;\" /></TD>\n";	
     print "     <TD valign=\"center\"><input type=\"checkbox\" VALUE=\"$provider\" name=\"SelectedHosps\" onclick=\"UncheckFOI($count, $rows_returned)\" checked='yes'></TD>\n";
     print "  </TR>\n";
     
     print OUTPUT "$provider\n";
   }

   close OUTPUT;

   $sth->finish();

   print qq{
   <input type=\"hidden\" name=\"Ids\" value=\"$providerIds\">
   <input type=\"hidden\" name=\"imp\" value=\"Y\">
<TR>
   <TD ALIGN="left" COLSPAN="4">
      $rows_returned ROWS
   </TD>   
</TR>
</TABLE>
<input type="hidden" name="foiName" value="--Not Specified--"> </CENTER>
</FORM>
<FORM name='peerFrom' action='/cgi-bin/PeerActions.pl' method='post'>
	<input type='hidden' name='saction' value=\"Create\">
	<input type='hidden' name='foiId' value=\"\">
	<input type='hidden' name='peername' value=\"\">
	<input type='hidden' name='desc' value=\"\">
	<input type='hidden' name='strpids' value=\"\">
	<input type='hidden' name='strpnames' value=\"\">
	<input type='hidden' name='js_lproviderNum' value=\"$searchProvider\">
	<input type='hidden' name='js_lproviderName' value=\"$name\">
	<input type='hidden' name='js_lAddress' value=\"$address\">
	<input type='hidden' name='js_lCity' value=\"$city\">
	<input type='hidden' name='js_lStates' value=\"$stateText\">
	<input type='hidden' name='js_lZip' value=\"$zip\">
	<input type='hidden' name='js_lCountry' value=\"$county\">
	<input type='hidden' name='js_lLocation' value=\"$location\">
	<input type='hidden' name='js_lRadius' value=\"$radius\">
	<input type='hidden' name='js_lMin_ftes' value=\"$min_ftes\">
	<input type='hidden' name='js_lMax_ftes' value=\"$max_ftes\">
	<input type='hidden' name='js_lMin_beds' value=\"$min_beds\">
	<input type='hidden' name='js_lMax_beds' value=\"$max_beds\">
	<input type='hidden' name='js_lToc' value=\"$tocText\">
	<input type='hidden' name='js_lHosp_Type' value=\"$hospText\">
	<input type='hidden' name='js_lSystem' value=\"$systemText\">
	<input type='hidden' name='js_lMsa' value=\"$msa\">
	<input type='hidden' name='js_lnfp' value=\"$nfp\">
	<input type='hidden' name='js_lgovt' value=\"$govt\">
	<input type='hidden' name='js_lTeachHosp' value=\"$teach_hosp\">
	<input type='hidden' name='js_lGeoArea' value=\"$geoArea\">
	<input type='hidden' name='js_lMin_net_income' value=\"$min_net_income\">
	<input type='hidden' name='js_lMax_net_income' value=\"$max_net_income\">
	<input type='hidden' name='js_lMin_zscore' value=\"$min_Zscore\"> 
	<input type='hidden' name='js_lMax_zscore' value=\"$max_Zscore\">
	<input type='hidden' name='js_lMin_Opr_Margin' value=\"$min_Opr_Margin\">
	<input type='hidden' name='js_lMax_Opr_Margin' value=\"$max_Opr_Margin\">	
	
</FORM>

<iframe name=\"peerCheck\" style="visibility:hidden;height:0px;width:0px;"></iframe>
</td>
            <td valign="top">
                <img src="/images/content_right.gif" /></td>
        </tr>
    </table>
    <!--content end--> 
};
		&TDdoc;
		print qq{	
   </BODY>
</HTML>
   };

}

#----------------------------------------------------------------------------

sub display_error
{
   my @list = @_;

   my ($s, $string);

   foreach $s (@list) {
      $s =~ s/\n/<BR \/>/g;
      $string .= "$s<BR>\n";
   }

   print "Content-Type: text/html\n\n";

   print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon" />
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css" />
<link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<FONT SIZE="5" COLOR="RED">
<B></B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR ALIGN="LEFT">
      <TD>$string</TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="javascript:window.location.href='/cgi-bin/cr_search.pl'" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
   };
}


#---------------------------------------------------------------------------

sub display_form
{
   my ($sth, $key);

   my $sql = "SELECT SYSTEM_ID, SYSTEM_NAME FROM SYSTEMS ORDER BY SYSTEM_NAME";

   unless ($sth = $dbh->prepare($sql)) {
       &display_error('ERROR preparing SQL', $sth->errstr);
       $dbh->disconnect();
       exit(1);
   }

   unless ($sth->execute()) {
       &display_error('ERROR executing SQL', $sth->errstr);
       $dbh->disconnect();
       exit(1);
   }
}


#------------------------------------------------------------------------------

sub comify
{
   local($_) = shift;
   1 while s/^(-?\d+)(\d{3})/$1,$2/;
   return($_);
}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#--------------------------------------------------------------------------------------
sub sessionExpire
{

print "Content-Type: text/html\n\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

#--------------------------------------------------------------------------------

#  Table: HOSPITALS  Wildcard: %
# +-----------------+--------------+-------------------+------+-----+
# | Field           | Type         | Collation         | Null | Key |
# +-----------------+--------------+-------------------+------+-----+
# | PROVIDER_NUMBER | char(6)      | latin1_swedish_ci | NO   | PRI |
# | FYB             | date         |                   | YES  |     |
# | FYE             | date         |                   | YES  |     |
# | STATUS          | varchar(15)  | latin1_swedish_ci | YES  |     |
# | CTRL_TYPE       | char(2)      | latin1_swedish_ci | YES  |     |
# | HOSP_NAME       | varchar(45)  | latin1_swedish_ci | YES  |     |
# | STREET_ADDR     | varchar(45)  | latin1_swedish_ci | YES  |     |
# | PO_BOX          | varchar(30)  | latin1_swedish_ci | YES  |     |
# | CITY            | varchar(38)  | latin1_swedish_ci | YES  |     |
# | STATE           | varchar(11)  | latin1_swedish_ci | YES  |     |
# | ZIP_CODE        | varchar(10)  | latin1_swedish_ci | YES  |     |
# | COUNTY          | varchar(35)  | latin1_swedish_ci | YES  |     |
# | URBAN1_RURAL    | char(2)      | latin1_swedish_ci | YES  |     |
# | LATITUDE        | decimal(8,5) |                   | YES  |     |
# | LONGITUDE       | decimal(8,5) |                   | YES  |     |
# | GEOACC          | char(1)      | latin1_swedish_ci | YES  |     |
# +-----------------+--------------+-------------------+------+-----+

# Table: HS2006_RPT  Wildcard: %
#+--------------------+---------------+-------------------+------+
#| Field              | Type          | Collation         | Null |
#+--------------------+---------------+-------------------+------+
#| RPT_REC_NUM        | int(11)       |                   | NO   |
#| PRVDR_CTRL_TYPE_CD | char(2)       | latin1_swedish_ci | YES  |
#| PRVDR_NUM          | char(6)       | latin1_swedish_ci | NO   
#| NPI                | decimal(10,0) |                   | YES  |
#| RPT_STUS_CD        | char(1)       | latin1_swedish_ci | NO   |
#| FY_BGN_DT          | date          |                   | YES  |
#| FY_END_DT          | date          |                   | YES  |
#| PROC_DT            | date          |                   | YES  |
#| INITL_RPT_SW       | char(1)       | latin1_swedish_ci | YES  |
#| LAST_RPT_SW        | char(1)       | latin1_swedish_ci | YES  |
#| TRNSMTL_NUM        | char(2)       | latin1_swedish_ci | YES  |
#| FI_NUM             | char(5)       | latin1_swedish_ci | YES  |
#| ADR_VNDR_CD        | char(1)       | latin1_swedish_ci | YES  |
#| FI_CREAT_DT        | date          |                   | YES  |
#| UTIL_CD            | char(1)       | latin1_swedish_ci | YES  |
#| NPR_DT             | date          |                   | YES  |
#| SPEC_IND           | char(1)       | latin1_swedish_ci | YES  |
#| FI_RCPT_DT         | date          |                   | YES  |
#+--------------------+---------------+-------------------+------+

