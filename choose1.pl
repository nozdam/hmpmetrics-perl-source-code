#!/usr/bin/perl

#--------------------------------------------------------------------
# Program: choose1.pl
#  Author: jagadish.m
# Written: Sun Apr  5 21:13:52 CDT 2009
# Revised: Sun Jun 14 18:21:56 CDT 2009
# Revised: Mon Jun 15 22:28:38 CDT 2009
#   Notes:
# Depends: DBI.pm, CGI.pm
#--------------------------------------------------------------------

require "/var/www/cgi-bin/lib/req-milo.pl";
use strict;
use DBI;
use CGI;
do "pagefooter.pl";
do "common_header.pl";
print "Content-Type: text/html\n\n";
&headerScript();
use CGI::Session;
do "audit.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $facility = $cgi->param("facility");
if(!$userID)
{
	&sessionExpire;
}
my $provider  = $cgi->param('provider');

&view("View","","Cost Report");
unless ($provider) {
   &display_error('NO PROVIDER NUMBER SUPPLIED');
   exit(1);
}

my %status_lookup = (
 1 => 'As Submitted',
 2 => 'Settled w/o Audit',
 3 => 'Settled with Audit',
 4 => 'Reopened',
 5 => 'Amended',
);

my $start_time = time;

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error('CAN\'T READ milo.ini');
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my ($sql, $dbh,$row, $sth,$rsql,$sth1,$htypename,$csql,$sth2,$ctrl_name,$sql_f,$facility_id,$master_table,$report_table,$data_table,$ctrl_id,$providerno);

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", DBI::errstr);
   exit(1);
}

$sql = "select control_type_id, control_type_name from control_type";
my $rs = &run_mysql($sql,4);
my %all_control;
foreach my $k (keys %{$rs}) {
	$all_control{$$rs{$k}{'control_type_id'}} = $$rs{$k}{'control_type_name'};
}

$sql_f = "select facility_id ,master_table,report_table,data_table from tbl_facility_type where facility_name = '$facility'";
$sth = $dbh->prepare($sql_f);
$sth->execute();

while ($row = $sth->fetchrow_hashref) {
	$facility_id = $$row{facility_id};
	$master_table = $$row{master_table};
	$report_table = $$row{report_table};
	$data_table = $$row{data_table};
}
my @rpt;
my @rpt_year;
my $prevprovider;
my @prev_prov;
my $row;
my ($name, $address, $city, $state, $zip, $county,$toc,$beds);
my %provider_control_types;
#----------------------------------------------------------------------------
$sql = qq{
  SELECT h.providerno,h.Name,h.address,
h.City ,h.State,h.Zip
,h.County, h.type_of_control, h.no_of_beds,h.call_type,tc.Control_Type_Name as ctrl_name,
ht.Hospital_Type_Name as htypename
 FROM $master_table as h
 left outer join control_type as tc on tc.Control_Type_id = h.type_of_control
 left outer join hospital_type as ht on ht.Hospital_Type_id = h.call_type
Where tc.Control_Type_Name is not null and h.providerno IN ($provider)
group by tc.Control_Type_Name, h.providerno

};

unless ($sth = $dbh->prepare($sql)) {
   &display_error("ERROR preparing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute()) {
   &display_error("ERROR executing SQL query:", $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

while ($row = $sth->fetchrow_hashref) {
	$name = $$row{Name};
	$address = $$row{address};
	$city = $$row{City};
	$state = $$row{State};
	$zip = $$row{Zip};
	$county = $$row{County};
	$toc = $$row{type_of_control};
	$beds = $$row{no_of_beds};
	$htypename=$$row{htypename};
	$ctrl_name = $$row{ctrl_name};
	$providerno = $$row{providerno};
	$provider_control_types{$providerno} = $ctrl_name;
 }
$zip =~ s/-$//;  # crappy data

$sth->finish();
#----------------------------------------------------------------------------

$prevprovider= qq{
Select PreviousProviderNo from tblpreviousprovider where CurrentProviderNo = ? and facility_type = $facility_id
};

unless ($sth = $dbh->prepare($prevprovider)) {
   &display_error('ERROR preparing SQL query:',  $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute($provider)) {
   &display_error('ERROR executing SQL query:',  $sth->errstr);
   $dbh->disconnect;
   exit(1);
}
while ($row = $sth->fetchrow_hashref){
	 push (@prev_prov, $$row{PreviousProviderNo});
	 }
	 foreach my $prev_prov(@prev_prov){
	 $provider=$provider.",".$prev_prov;
	}

$sql = qq{SELECT year(FY_END_DT) as RPT_YEAR, RPT_REC_NUM, RPT_STUS_CD, PRVDR_NUM, DATE_FORMAT(FY_BGN_DT, '%m-%d-%Y'), DATE_FORMAT(FY_END_DT, '%m-%d-%Y') FROM $report_table WHERE PRVDR_NUM in ($provider) ORDER BY FY_BGN_DT DESC};
unless ($sth = $dbh->prepare($sql)) {
   &display_error('ERROR preparing SQL query:',  $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

unless ($sth->execute()) {
   &display_error('ERROR executing SQL query:',  $sth->errstr);
   $dbh->disconnect;
   exit(1);
}

my $now = localtime();

print qq{
<HTML>
<HEAD><TITLE>CHOOSER</TITLE>
<SCRIPT SRC="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<SCRIPT SRC="/JS/dropmenu.js"></SCRIPT>
<link rel="stylesheet" href="/css/dropmenu.css" type="text/css" />
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<!--content start-->

    <table cellpadding="0" cellspacing="0" width="885px">
        <tr>
            <td valign="top">
                <img src="/images/content_left.gif" /></td>
            <td valign="top" style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px">
<Table cellpadding="0" cellspacing="0" width="100%">
	<Tr>
		<td>
			<tr><td>PROVIDER # <span class="pageheader">$provider</span></td>  <td align="right" class="tahoma12">Type of Control : $all_control{$toc}($toc)</td></tr>
			<tr><td><B>$name</B></td> &nbsp &nbsp <td align="right" class="tahoma12">Total number of beds : $beds </td></tr>
			<tr><td><span class="tahoma12">$address, $city, $state  $zip</td></tr>
			<tr>
		<td align="right" valign="bottom" class="tahoma12" width="">$now</td>
		</tr>
		</td>
</tr>
</table>
<table BORDER="0" align="center" CELLPADDING="2" WIDTH="100%" class="gridstyle">
<tr><td colspan="8" style="background-color:white; font-size:11px" align="right">&nbsp;Available cost reports, ordered with most recent on top</td></tr>
<TR class="gridheader">
    <TH>Cost Report #</TH><TH>Status Code</TH><TH>Fiscal Year Begins</TH><TH>Fiscal Year Ends</TH>
	<TH>Provider Number</TH><TH>Facility Type</TH><TH>Control Type</TH><TH>Download In Excel</TH>
</TR>
};
my ($rpt_year, $rpt_rec_num, $rpt_stus_cd, $actual_provider, $fy_bgn_dt, $fy_end_dt);

my ($class, $link);

my $count = 0;

#Hospital Type

while (($rpt_year, $rpt_rec_num, $rpt_stus_cd, $actual_provider, $fy_bgn_dt, $fy_end_dt) = $sth->fetchrow_array) {

   $class = (++$count % 2) ? 'gridrow' : 'gridalternate';

   $link = "<A HREF=\"/cgi-bin/cr_explore.pl?facility=$facility&reportid=$rpt_rec_num&provider=$provider&action=go\" TITLE=\"VIEW COST REPORT\">$rpt_rec_num</A>";
  push (@rpt, $rpt_rec_num);
  push(@rpt_year,$fy_end_dt);
  my $xlink = "<A HREF=\"cr_explore_xls.pl?facility=$facility&reportid=$rpt_rec_num&provider=$provider\" TITLE=\"Download entire cost report in Excel\"><IMG BORDER=\"0\" SRC=\"/icons/excel_icon.gif\"></A>";
   print "   <TR CLASS=\"$class\">\n";
   print "      <TD ALIGN=\"center\">$link</TD>\n";
   print "      <TD ALIGN=\"center\">$status_lookup{$rpt_stus_cd}</TD>\n";
   print "      <TD ALIGN=\"center\">$fy_bgn_dt</TD>\n";
   print "      <TD ALIGN=\"center\">$fy_end_dt</TD>\n";
   print "		 <TD ALIGN=\"center\">$actual_provider</TD>\n";
   print "		 <TD ALIGN=\"center\">$htypename</TD>\n";
   print "		 <TD ALIGN=\"center\">$provider_control_types{$actual_provider}</TD>\n";
   print "		 <TD ALIGN=\"center\">$xlink</TD>\n";
   print "   </TR>\n";

}
my $arraycount=0;
 my $str="";
 my $sstr="";
 my $rptyear=@rpt_year;
 my $i;

for($i=0;$i<$rptyear;$i++){
    $str= $str.",".$rpt_year[$i];
	$sstr= $sstr.",".$rpt[$i];
 }
$session->param("sstr", $sstr);
$session->param("str", $str);
my $elapsed_time = time - $start_time;
my $prov=$session->param("str");
print qq{
</TABLE>
<span class="tahoma12">
$count ROWS
</span>
		</td>
		<td valign="top">
		<img src="/images/content_right.gif" /></td>
	</tr>
	</table>
<!--content end-->
};
&TDdoc;
print qq{
</CENTER>
</BODY>
</HTML>
};
$sth->finish;
$dbh->disconnect;
exit(0);

#------------------------------------------------------------------------------

sub display_error
{
   my $s;
   print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>

<FONT SIZE="5" COLOR="RED">
<B>ERRORS</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" BORDER="1" CELLPADDING="20" WIDTH="600">
<TR><TD>
<UL>
};
   foreach $s (@_) {
      print "$s<BR>";
   }
   print qq{
</TD></TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</FONT>
</CENTER>
</BODY>
</HTML>
};
}

#----------------------------------------------------------------------------

sub comify
{
   local($_) = shift;
   1 while s/^(-?\d+)(\d{3})/$1,$2/;
   return($_);
}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }
   close INIFILE;
   return 1;
}

#--------------------------------------------------------------------------------------
sub sessionExpire
{
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

#------------------------------------------------------------------------------

