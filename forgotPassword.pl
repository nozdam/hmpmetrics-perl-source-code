#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: forgotPassword.pl
#  author: Jagadish.M
#----------------------------------------------------------------------------

use strict;
use CGI;
use DBI;
use Mail::Sendmail;
use CGI::Session;
do "common_header.pl";
print "Content-Type: text/html\n\n";
&headerScript();

my $cgi = new CGI;
my $dbh;
my ($sql, $sth, $key, @row);

# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my $defaultYear= $ini_value{'DEFAULT_YEAR'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

my $saction = $cgi->param('saction');
my $userName = $cgi->param('username');

# Email setting to send forgot password.

my ($to_list,$cc_list,$bcc_list,$email,$MailFrom,$subject,$message);



my ($str, $password1);

if($userName){
	$sql = "select AES_DECRYPT(Password,'bs') as password from Users where UserName = '$userName' and IsActive = 1";
	$sth = $dbh->prepare($sql);			
	$sth->execute();			
	while (my $rec = $sth->fetchrow_hashref) {		
		$password1 = $$rec{password};
	}
	@row = $sth->fetchrow_array();
		
	if($sth->rows()>0)
	{		
		$to_list = $userName;
		$cc_list = $userName;
		$bcc_list = $userName;
		$email = "\n\nThis email was generated automatically.\n";
		$MailFrom = "milo\@hcmpllc.com";
		$subject = "Confidential Mail - Milo";
		$message = "Please login with the password: '$password1'";			
		
		my %mail = (
		To => $to_list,
		From => $MailFrom,
		Bcc => $bcc_list,
		Cc => $cc_list,
		Subject => $subject,
		Message => $message
		);
		
		$mail{Smtp} = 'smtp.gmail.com';
		
		sendmail(%mail); 
		$str = "Password has been sent to your Email-id :'$userName'";				
		
	}
	else
	{
		$str = "The username you entered is incorrect";
	
	}
}	





print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="/css/forgotPassword.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Milo Login...</title>
<link href="/css/register/main.css" rel="stylesheet" type="text/css">
<link href="/css/register/qm.css" rel="stylesheet" type="text/css">


</head>
<body>
<table cellpadding="0" cellspacing="0" width="740px" align="center">
	<tr>
	    <td align="center" style="padding-top: 10px; padding-bottom: 10px; ">
		<a href="http://www.hcmpllc.com">
		    <img border="0" src="/images/logo.gif"></a>
	    </td>
	</tr>
</table>
<form action="/cgi-bin/forgotPassword.pl" method="post" name="frmForgotPassword" >
<table cellpadding="2px" cellspacing="1px" bgcolor="#F4F5F7" width="400px" class="tableBorder" align="center">
    <tr>
        <td colspan="2" class="Fnt11Pix" background="/images/register/grad_01_hover.gif">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2" class="label">&nbsp;</td>
    </tr>

    <tr>
        <td align="center" colspan="2">            
            <span class="message">Forgot Password</span>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="label">&nbsp;</td>
    </tr>
    <tr>
        <td class="label" align="right" width="40%">User name (Email):</td>
        <td align="left" width="60%"><input type="text" name="username" style="width:300px;" maxlength="300"/></td>
    </tr>    
    <tr>
        <td class="label" align="right">&nbsp;</td>
        <td align="left"><input type="submit" value="Submit" /><input type="button" value="Back" onclick="location.href='/cgi-bin/login.pl'"/></td>
    </tr>   
    <tr>
            <td class="label" align="right">&nbsp;</td>
            <td style="font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px;" align="left"><!--New to Milo? <a href="/cgi-bin/registration.pl">Create an account</a>--></td>
    </tr>   
    <tr>
         <td colspan="2" class="label">&nbsp;</td>
    </tr>
</table>
<table align="center">
<tr>
<td style="color:#FF0000;">
};
	print $str;
print qq{
</td>
</tr>
</form>
</body>
</html>
};


#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach my $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}

#------------------------------------------------------------------------------