#!/usr/bin/perl

use CGI;
use DBI;
use strict;
do "common_header.pl";
use CGI::Session;
my $cgi = new CGI;

my $peermainId    = $cgi->param('mainID');
my $ownerid = $cgi->param('createdBy');
my $reload = "false";
$reload = $cgi->param('reload');
my $peerName = undef;
my $peerSearchcriteria = "<table class='gridstyle' cellpadding=0 cellspacing=0 width='100%' border=1>";
my $peerDescription = undef;
my $count = 0;
my ($sql_f,$facility_id,$master_table,$row,$row2,$Data,$sysStr,$statRow,$sysRow,$toRow,$htRow,$jmsSql,$msRow,$row2);
my ($FOI,$class1,$status1,$providerid1,$peername1,$i,$s,$stateSql,$row1);
# Database

my $dbh;
my ($sql1, $sth1);
my ($sql, $sth);

# Session management

my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $userType = $session->param("role");


# Check permissions
# All permission to admin ie - 2
my $Accessible = "false";
if($ownerid == $userID){
	$Accessible = "true";
}
if($userType == 2){
	$Accessible = "true";
}

# Get Database connection
my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error('ERROR connecting to database', $DBI::errstr);
   exit(1);
}

my $facility = $cgi->param("facility");
$sql_f = "select facility_id,master_table from tbl_facility_type where facility_name = '$facility'";
$sth = $dbh->prepare($sql_f);
$sth->execute();
while ($row = $sth->fetchrow_hashref) {
	$facility_id = $$row{facility_id};
	$master_table= $$row{master_table};
}
my $lHeight = "100px";
my $tbHeight = 0;

# Get Peer Name,Criteria,Description to display.
if($peermainId != 0){
$sql1 = "select pm.PeerGroup_ID as PeerGroup_ID,pm.Name as Name,pm.isActive as isActive,pm.c_Provider_ID as c_ProviderID,pm.description as description,pm.c_provider_name as c_Provider_Name,pm.c_Provider_Address as c_Provider_Address,pm.c_Provider_City as c_Provider_City,pm.c_Provider_zip as c_Provider_zip,pm.c_Provider_country as c_Provider_country,st.state_name as c_Provider_States,sm.systemname as c_System,ct.control_type_name as c_Control,ht.hospital_type_name as c_Hospital_Type,m1.msaname as c_MSA,pm.C_Bed_Start as C_Bed_Start,pm.c_Bed_End as c_Bed_End,pm.c_FTE_Start as c_FTE_Start,pm.c_FTE_End as c_FTE_End,pm.c_Income_Start as c_Income_Start,pm.c_Income_End as c_Income_End,pm.c_Within as c_Within,pm.c_Miles_Of_zip as c_Miles_Of_zip,pm.C_NFP as c_nfp,pm.C_Teaching_Hospital as c_teach_hosp,pm.C_Geographical_Area as c_geo_area,C_Governmental as c_govt,pm.c_Zscore_min as c_Zscore_min,pm.c_Zscore_max as c_Zscore_max,pm.c_OprMargin_max as c_OprMargin_max,pm.c_OprMargin_min as c_OprMargin_min from peergroup_main as pm left outer join tblsystemmaster as sm on pm.c_System = sm.SYSTEMID left outer join states as st on pm.c_Provider_States = st.state left outer join msa as m1 on pm.c_MSA = m1.MSACode left outer join control_type as ct on pm.c_Control = ct.Control_Type_id left outer join hospital_type as ht on pm.c_Hospital_Type = ht.Hospital_Type_id where pm.PeerGroup_ID = $peermainId and pm.facility_type = $facility_id";
$sth1 = $dbh->prepare($sql1);
$sth1->execute();
if($sth1->rows()>0){
while($row2=$sth1->fetchrow_hashref)
	{
		my($stateSql,$stateSth,$stStr);
		my($jstSql,$jstSth,$jstStr);

		my($sysSql,$sysSth,$syStr);
		my($jsySql,$jsySth,$jsyStr);

		my($toSql,$toSth,$toStr);
		my($jtoSql,$jtoSth,$jtoStr);

		my($htSql,$htSth,$htStr);
		my($jhtSql,$jhtSth,$jhtStr);

		my($msSql,$msSth,$msStr);
		my($jmstSql,$jmsSth,$jmsStr);

		$peerName = $$row2{Name};

		if($$row2{c_Provider_ID}){
		 	$peerSearchcriteria = $peerSearchcriteria."<tr><td>providerno</td><td>".$$row2{c_Provider_ID}."</td></tr>";
		 	$tbHeight = $tbHeight + 20;
		}
		if($$row2{c_Provider_Name}){
			$peerSearchcriteria = $peerSearchcriteria."<tr><td>HospName</td><td>".$$row2{c_Provider_Name}."</td></tr>";
			$tbHeight = $tbHeight + 20;
		}
		if($$row2{c_Provider_Address}){
			$peerSearchcriteria = $peerSearchcriteria."<tr><td>Address </td><td>".$$row2{c_Provider_Address}."</td></tr>";
			$tbHeight = $tbHeight + 20;
		}
		if($$row2{c_Provider_City}){
			$peerSearchcriteria = $peerSearchcriteria."<tr><td>City</td><td>".$$row2{c_Provider_City}."</td></tr>";
			$tbHeight = $tbHeight + 20;
		}

		$jstSql ="SELECT c_Provider_States FROM peergroup_main where PeerGroup_ID = $peermainId";
		$jstSth = $dbh->prepare($jstSql);
		$jstSth->execute();

		my $jstStr = "";

		while ($Data = $jstSth->fetchrow_hashref){
			$jstStr = $$Data{c_Provider_States};
		}

	if($jstStr){
			$stateSql = "select group_concat(states.state_name) as c_Provider_States from states where states.state in ($jstStr)";
			$stateSth = $dbh->prepare($stateSql);
			$stateSth->execute();  
			if($stateSth->rows() > 0){
			while($statRow = $stateSth->fetchrow_hashref){
				$stStr = $$statRow{c_Provider_States};
			}
			}
			}
			$peerSearchcriteria = $peerSearchcriteria."<tr><td>State(s)</td><td>".$stStr."</td></tr>";
			$tbHeight = $tbHeight + 20;
		

		if($$row2{c_Provider_zip}){
			$peerSearchcriteria = $peerSearchcriteria."<tr><td>zip</td><td>".$$row2{c_Provider_zip}."</td></tr>";
			$tbHeight = $tbHeight + 20;
		}
		if($$row2{c_Provider_country}){
			$peerSearchcriteria = $peerSearchcriteria."<tr><td>Country</td><td>".$$row2{c_Provider_country}."</td></tr>";
			$tbHeight = $tbHeight + 20;
		}


		$jsySql ="SELECT c_System FROM peergroup_main where PeerGroup_ID = $peermainId";
		$jsySth = $dbh->prepare($jsySql);
		$jsySth->execute();

		my $jsyStr = "";

		while ($Data = $jsySth->fetchrow_hashref){
			$jsyStr = $$Data{c_System};
		}

		if($jsyStr){
			$sysSql = "select group_concat(tblsystemmaster.SYSTEMNAME) as c_System from tblsystemmaster where systems.SYSTEMID in($jsyStr)";
			$sysSth = $dbh->prepare($sysSql);
			$sysSth->execute();

			while($sysRow = $sysSth->fetchrow_hashref){
				$sysStr = $$sysRow{c_System};
			}
			if(substr($jsyStr,1,4) eq 'none'){
				$peerSearchcriteria = $peerSearchcriteria."<tr><td>System</td><td>".substr($jsyStr,1,4)."</td></tr>";
			}else{
				$peerSearchcriteria = $peerSearchcriteria."<tr><td>System</td><td>".$sysStr."</td></tr>";
			}
			$tbHeight = $tbHeight + 20;
		}

		$jtoSql ="SELECT c_Control FROM peergroup_main where PeerGroup_ID = $peermainId";
		$jtoSth = $dbh->prepare($jtoSql);
		$jtoSth->execute();

		my $jtoStr = "";

		while ($Data = $jtoSth->fetchrow_hashref){
			$jtoStr = $$Data{c_Control};
		}

		if($jtoStr){
			$toSql = "select group_concat('''',Control_Type.Control_Type_Name,'''') as c_Control from Control_Type where Control_Type.Control_Type_ID in($jtoStr)";
			$toSth = $dbh->prepare($toSql);
			$toSth->execute();

			while($toRow = $toSth->fetchrow_hashref){
				$toStr = $$toRow{c_Control};
			}

			$peerSearchcriteria = $peerSearchcriteria."<tr><td>Type Of Control</td><td>".$toStr."</td></tr>";
			$tbHeight = $tbHeight + 20;
		}

		$jhtSql ="SELECT c_Hospital_Type FROM peergroup_main where PeerGroup_ID = $peermainId";
		$jhtSth = $dbh->prepare($jhtSql);
		$jhtSth->execute();

		my $jhtStr = "";

		while ($Data = $jhtSth->fetchrow_hashref){
			$jhtStr = $$Data{c_Hospital_Type};
		}

		if($jhtStr){
			$htSql = "select group_concat(hospital_type.Hospital_Type_Name) as c_hospital_type from hospital_type where Hospital_Type.Hospital_Type_id in($jhtStr)";
			$htSth = $dbh->prepare($htSql);
			$htSth->execute();

			while($htRow = $htSth->fetchrow_hashref){
				$htStr = $$htRow{c_hospital_type};
			}
			$peerSearchcriteria = $peerSearchcriteria."<tr><td>Hosp Type</td><td>".$htStr."</td></tr>";
			$tbHeight = $tbHeight + 20;
		}

		$jmsSql ="SELECT c_MSA FROM peergroup_main where PeerGroup_ID = $peermainId";
		$jmsSth = $dbh->prepare($jmsSql);
		$jmsSth->execute();

		my $jmsStr = "";

		while ($Data = $jmsSth->fetchrow_hashref){
			$jmsStr = $$Data{c_MSA};
		}

		if($jmsStr){
			$msSql = "select group_concat('''',msa.MSAName,'''') as MSA from MSA where MSA.MSACode in($jmsStr)";
			$msSth = $dbh->prepare($msSql);
			$msSth->execute();

			while($msRow = $msSth->fetchrow_hashref){
				$msStr = $$msRow{MSA};
			}
			$peerSearchcriteria = $peerSearchcriteria."<tr><td>MSA</td><td>".$msStr."</td></tr>";
			$tbHeight = $tbHeight + 20;
		}
		if($$row2{c_nfp}){
			my $nfp_txt = '';
			if($$row2{c_nfp} eq 'T'){
			  $nfp_txt = 'True';
			}else{
			  $nfp_txt = 'False';
			}
			$peerSearchcriteria = $peerSearchcriteria."<tr><td>N F Profit</td><td>".$nfp_txt."</td></tr>";
			$tbHeight = $tbHeight + 20;
		}
		if($$row2{c_teach_hosp}){
			my $cth = '';
			if($$row2{c_teach_hosp} eq 'Y'){
				$cth = 'Yes';
			}else{
				$cth = 'No';
			}
			$peerSearchcriteria = $peerSearchcriteria."<tr><td>Teaching Hospital</td><td>".$cth."</td></tr>";
			$tbHeight = $tbHeight + 20;
		}
		if($$row2{c_geo_area}){
			my $cga = '';
			if($$row2{c_geo_area} eq '1'){
			   $cga = 'Urban';
			}else{
			   $cga = 'Rural';
			}
			$peerSearchcriteria = $peerSearchcriteria."<tr><td>Geographical Area</td><td>".$cga."</td></tr>";
			$tbHeight = $tbHeight + 20;
		}
		if($$row2{c_govt}){
			my $cgovt = '';
			if($$row2{c_govt} eq 'T'){
			   $cgovt = 'True';
			}else{
			   $cgovt = 'False';
			}
			$peerSearchcriteria = $peerSearchcriteria."<tr><td>Governmental</td><td>".$cgovt."</td></tr>";
			$tbHeight = $tbHeight + 20;
		}

		if($$row2{C_Bed_Start}){
			$peerSearchcriteria = $peerSearchcriteria."<tr><td>Min Beds</td><td>".$$row2{C_Bed_Start}."</td></tr>";
			$tbHeight = $tbHeight + 20;
		}
		if($$row2{c_Bed_End}){
			$peerSearchcriteria = $peerSearchcriteria."<tr><td>Max Beds</td><td>".$$row2{c_Bed_End}."</td></tr>";
			$tbHeight = $tbHeight + 20;
		}
		if($$row2{c_FTE_Start}){
			$peerSearchcriteria = $peerSearchcriteria."<tr><td>Min FTEs</td><td>".$$row2{c_FTE_Start}."</td></tr>";
			$tbHeight = $tbHeight + 20;
		}
		if($$row2{c_FTE_End}){
			$peerSearchcriteria = $peerSearchcriteria."<tr><td>Max FTEs</td><td>".$$row2{c_FTE_End}."</td></tr>";
			$tbHeight = $tbHeight + 20;
		}
		if($$row2{c_Income_Start}){
			$peerSearchcriteria = $peerSearchcriteria."<tr><td>Min Net Income</td><td>".$$row2{c_Income_Start}."</td></tr>";
			$tbHeight = $tbHeight + 20;
		}
		if($$row2{c_Income_End}){
			$peerSearchcriteria = $peerSearchcriteria."<tr><td>Max Net Income</td><td>".$$row2{c_Income_End}."</td></tr>";
			$tbHeight = $tbHeight + 20;
		}
		if($$row2{c_Within}){
			$peerSearchcriteria = $peerSearchcriteria."<tr><td>Distance(in miles)</td><td>".$$row2{c_Within}."</td></tr>";
			$tbHeight = $tbHeight + 20;
		}
		if($$row2{c_Miles_Of_zip}){
			$peerSearchcriteria = $peerSearchcriteria."<tr><td>Location</td><td>".$$row2{c_Miles_Of_zip}."</td></tr>";
			$tbHeight = $tbHeight + 20;
		}

		if($$row2{c_Zscore_min}){
			$peerSearchcriteria = $peerSearchcriteria."<tr><td>Min Zscore</td><td>".$$row2{c_Zscore_min}."</td></tr>";
			$tbHeight = $tbHeight + 20;
		}
		if($$row2{c_Zscore_max}){
			$peerSearchcriteria = $peerSearchcriteria."<tr><td>Max Zscore</td><td>".$$row2{c_Zscore_max}."</td></tr>";
			$tbHeight = $tbHeight + 20;
		}
		if($$row2{c_OprMargin_min}){
					$peerSearchcriteria = $peerSearchcriteria."<tr><td>Min Operating Margin</td><td>".$$row2{c_OprMargin_min}."</td></tr>";
					$tbHeight = $tbHeight + 20;
		}
		if($$row2{c_OprMargin_max}){
					$peerSearchcriteria = $peerSearchcriteria."<tr><td>Max Operating Margin</td><td>".$$row2{c_OprMargin_max}."</td></tr>";
					$tbHeight = $tbHeight + 20;
		}
		$peerSearchcriteria = $peerSearchcriteria."</table>";
		$peerDescription = $$row2{description};
	}
	if($tbHeight<100){$lHeight=$tbHeight."px";}
$sth1->finish();
}

}
# Get All the provider Number and name from Peer Group Details.

#$sql = "select * from peergroup_details where PeerGroup_Main_ID = $peermainId ORDER BY FOI desc";

$sql = "select pd.PeerGroup_Main_ID as PeerGroup_Main_ID, pd.Provider_number as Provider_number,pd.ProviderName, pd.FOI as FOI, pd.isActive as isActive from peergroup_details as pd left outer join $master_table as h on h.providerno = pd.Provider_number where pd.PeerGroup_Main_ID = $peermainId ORDER BY pd.FOI desc";

$sth = $dbh->prepare($sql);
$sth->execute();
$count = $sth->rows();

# Display the Details using html.

print "Content-Type: text/html\n\n";
&headerScript();
print <<EOF
<HTML>
<HEAD><TITLE>PEER GROUP DETAILS</TITLE>
<script type="text/javascript" src="/JS/peer_group/peer_main.js"></script>
<script type="text/javascript" src="/JS/peer_group/PeerDetails.js"></script>
<SCRIPT SRC="/JS/sorttable.js"></SCRIPT>
</HEAD>
<BODY onload="LoadPeerDetails();">
<FORM name="frmPeerDetails">
<input type="hidden" name="hdnPeerID" value="$peermainId">
<input type="hidden" name="reload" value="$reload">
<input type="hidden" name="facility" value="$facility">
<input type="hidden" name="lHeight" value="$lHeight">
<input type="hidden" name="hdnPeerName" value="$peerName">
<input type="hidden" name="hdnSearchCriteria" value="$peerSearchcriteria">
<input type="hidden" name="hdnDescription" value="$peerDescription">
<input type="hidden" name="hdnOwnerID" value="$ownerid">
<TABLE  class="sortable" border=1 style="border:solid 1px #b1bed0;color:#0c4f7a;" align="left" CELLPADDING="0" CELLSPACING="0" width="100%">
	<tr class='gridHeader'>
		<td>SlNo.$stateSql</td><td>Provider #</td><td>Provider Name</td><td>F.O.I</td><td>Delete</td>
	</tr>
EOF
;
	$i = 0;
		if($sth->rows()>0){
	while ($row1 = $sth->fetchrow_hashref) {
			$peermainId = $$row1{PeerGroup_Main_ID};
     		$providerid1 = $$row1{Provider_number};
     		$peername1 = $$row1{ProviderName};
			$FOI =	$$row1{FOI};
     		$status1 = $$row1{isActive};
     		$class1 = (++$i % 2) ? 'gridrow' : 'gridrow';
  	if($FOI == 0){
  		if($Accessible eq 'false'){
  			print "<tr class=\"$class1\"><td>$i</td><td>$providerid1</td><td>$peername1</td><td></td><td><a href=\"#\" onClick=\"AccessDenied();\">Delete</a></td></tr>\n";
  		}else{
  			print "<tr class=\"$class1\"><td>$i</td><td>$providerid1</td><td>$peername1</td><td></td><td><a href=\"#\" onClick=\"callDelDetails($peermainId,'$providerid1','$peername1',$count);\">Delete</a></td></tr>\n";
  		}
  	}else{
  		if($Accessible eq 'false'){
  			print "<tr class=\"$class1\"><td>$i</td><td>$providerid1</td><td>$peername1</td><td><img src=\"/images/FOI_small.gif\" /></td><td><a href=\"#\" onClick=\"AccessDenied();\">Delete</a></td></tr>\n";
  		}else{
  			print "<tr class=\"$class1\"><td>$i</td><td>$providerid1</td><td>$peername1</td><td><img src=\"/images/FOI_small.gif\" /></td><td><a href=\"#\" onClick=\"callDelDetails($peermainId,'$providerid1','$peername1',$count);\">Delete</a></td></tr>\n";
  		}
  	}
  }
}
  $sth->finish();
  $dbh->disconnect();
qq{
</table>
</FORM>
</BODY>
</HTML>
};

#-----------------------------------------------------------------------------


sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}