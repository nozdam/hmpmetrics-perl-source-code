#!/usr/bin/perl

use CGI;
use DBI;
use strict;
do "common_header.pl";
my $cgi = new CGI;
my $param = $cgi->param('search');
my $facility = $cgi->param("facility");

# Database
my ($sth1,$row,$row1,$s);
my $dbh;

my ($sql, $sth,$sql_f,$facility_id,$master_table);

# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my @fYears     = split(/,/,$ini_value{'HCRIS_YEARS'});
my $defaultYear= $ini_value{'DEFAULT_YEAR'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error('ERROR connecting to database', $DBI::errstr);
   exit(1);
}
$sql_f = "select master_table,facility_id from tbl_facility_type where facility_name = '$facility'";
$sth1= $dbh->prepare($sql_f);
$sth1->execute();

while ($row = $sth1->fetchrow_hashref) {
	$facility_id = $$row{facility_id};
	$master_table = $$row{master_table};
}
$sql = "SELECT mt.City as CITY,mt.State as STATE,
mt.Name as NAME,
mt.ProviderNo as PROVIDER_NUMBER
FROM $master_table as mt
inner join states as s on mt.State = s.state
WHERE mt.Name LIKE '$param%'
union SELECT mst.City as CITY,mst.State as STATE,
mst.Name as NAME,
mst.ProviderNo as PROVIDER_NUMBER
FROM $master_table as mst
inner join states as s on mst.State = s.state
WHERE mst.ProviderNo LIKE '$param%'";


$sth = $dbh->prepare($sql);
$sth->execute();

print "Content-Type: text/html\n\n";
print <<EOF
<ul>
EOF
;
while ($row1 = $sth->fetchrow_hashref) {
print "<li nowrap>$$row1{NAME} ~ $$row1{PROVIDER_NUMBER} - $$row1{CITY} , $$row1{STATE}</li>";
}
qq{
</ul>
};

#-----------------------------------------------------------------------------


sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}
#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}
print "Content-Type: text/html\n\n";
print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};
&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}