#!/usr/bin/perl

use strict;

my $scalar = 100/5/2;
print "$scalar\n";
exit;

my %hash;
$hash{"1"} = "abc";
$hash{"2"} = "xyz";
$hash{"3"} = "pqr";

my @arr = keys %hash;

print $arr[0] . " " . $arr[1] . " " . $arr[2] . "\n";

@arr = values %hash;

print "@arr\n";


