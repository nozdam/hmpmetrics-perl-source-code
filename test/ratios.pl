#!/usr/bin/perl

require "/var/www/cgi-bin/lib/req-adhoc-reports.pl";


my %metrics;

$metrics{"lifecare_margin"}{"lower"} = "-1000";
$metrics{"lifecare_margin"}{"upper"} = "1000";
$metrics{"lifecare_margin"}{"sort"} = "D";
$metrics{"lifecare_margin"}{"name"} = "TotalOverallMargin";

$metrics{"lifecare_operating_margin"}{"lower"} = "-1000";
$metrics{"lifecare_operating_margin"}{"upper"} = "1000";
$metrics{"lifecare_operating_margin"}{"sort"} = "D";
$metrics{"lifecare_operating_margin"}{"name"} = "TotalOperatingMargin";

$metrics{"lifecare_labor_cost"}{"lower"} = "20";
$metrics{"lifecare_labor_cost"}{"upper"} = "100";
$metrics{"lifecare_labor_cost"}{"sort"} = "A";
$metrics{"lifecare_labor_cost"}{"name"} = "LaborCostAsPerRev";

$metrics{"lifecare_ftes_per_aob"}{"lower"} = "0.01";
$metrics{"lifecare_ftes_per_aob"}{"upper"} = "24";
$metrics{"lifecare_ftes_per_aob"}{"sort"} = "A";
$metrics{"lifecare_ftes_per_aob"}{"name"} = "FTEsPerAOB";

$metrics{"lifecare_avg_days_in_ar"}{"lower"} = "1";
$metrics{"lifecare_avg_days_in_ar"}{"upper"} = "500";
$metrics{"lifecare_avg_days_in_ar"}{"sort"} = "A";
$metrics{"lifecare_avg_days_in_ar"}{"name"} = "AvgDays";

$metrics{"lifecare_avg_age_of_plant"}{"lower"} = "0";
$metrics{"lifecare_avg_age_of_plant"}{"upper"} = "100";
$metrics{"lifecare_avg_age_of_plant"}{"sort"} = "A";
$metrics{"lifecare_avg_age_of_plant"}{"name"} = "AvgAgePlant";


&run_report(\%metrics);

