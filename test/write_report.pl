#!/usr/bin/perl -w

use strict;
use Spreadsheet::ParseExcel;
use Spreadsheet::WriteExcel;
use Data::Dumper;
use Scalar::Util qw(looks_like_number);

require "/var/www/cgi-bin/lib/req-costreports.pl";

my $path = "/var/www/html";
my $cr_rec_num = 242866;
#my $cr_worksheet = "D00A191";
my $cr_worksheet = "G300000";
my $facility_type = 1;
my $spreadsheet_new = $cr_rec_num . "-" . $cr_worksheet . "_filled.xls";

#Write the values to a new spreadsheet.
my $dest_book  = Spreadsheet::WriteExcel->new("$spreadsheet_new") or die "Could not create a new Excel file in $spreadsheet_new: $!";

my %worksheet;
my $sql;
if ($cr_worksheet) {
	$sql = "SELECT cr_worksheet, cr_line, cr_column, cr_value FROM cr_all_data WHERE cr_rec_num = $cr_rec_num AND cr_worksheet = '$cr_worksheet' and cr_type = 'N'";
}
else {
	$sql = "SELECT cr_worksheet, cr_line, cr_column, cr_value FROM cr_all_data WHERE cr_rec_num = $cr_rec_num and cr_type = 'N'";
}
print "$sql\n";
my $rs = &run_mysql($sql,4);
foreach my $k (keys %{$rs}) {
	my $line = $$rs{$k}{"cr_line"};
	my $column = $$rs{$k}{"cr_column"};
	my $value = $$rs{$k}{"cr_value"};
	my $cr_worksheet2 = $$rs{$k}{"cr_worksheet"};
	$worksheet{$cr_worksheet2}{$line}{$column} = $value;
}


foreach my $cr_worksheet (sort {$a cmp $b} keys %worksheet) {

	#Get the spreadsheet path.
	$sql = "SELECT cost_report_form_link, definition FROM tbl_cost_report_definition WHERE facility_type = $facility_type AND cr_worksheet = '$cr_worksheet'";
	#print "$sql\n";
	$rs = &run_mysql($sql,4);
	my $spreadsheet_path = $path . $$rs{0}{'cost_report_form_link'};
	my $spreadsheet_name = uc($cr_worksheet);

	&create_work_sheet($spreadsheet_name, $spreadsheet_path, $dest_book, \%{$worksheet{$cr_worksheet}});
}

#&create_work_sheet($spreadsheet_name, $spreadsheet_path, $dest_book, \%worksheet);



$dest_book->close();

#`gzip $spreadsheet_new`;

