#!/usr/bin/perl

use CGI;
use DBI;
use Scalar::Util qw(looks_like_number);

my $cgi = new CGI;
do "common_header.pl";
do "pagefooter.pl";
use CGI::Session;
do "audit.pl";
require "/var/www/cgi-bin/lib/req-milo.pl";
my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $facility = $cgi->param("facility");
if(!$userID)
{
	&sessionExpire;
}
&view("View","Standard report generate page","Standard Report");

my $orderBy = $cgi->param('orderBy');
my $text = $cgi->param('providers');
my $checkedYear = $cgi->param('yearCheck');

my $datasetIds = $cgi->param('datasetIds');
my $dataElementIds = $cgi->param('dataElementIds');
my $dispOrder = $cgi->param('dispOrder');

# Used to assign to hidden textboxes.

my $tmpdatasetIds = $datasetIds;
my $tmpdataElements = $dataElementIds;
my $tmpdispOrder = $dispOrder;

if(!$orderBy){
	$orderBy = " FY_BGN_DT ";
}

my $peerID = $cgi->param('peerGroupSelect');
my $foiID = $cgi->param('foistatistics');
my $rptDirection = $cgi->param('rptDirection');

my $foiNumber;
my $foiCompare = "false";

# Remove first comma from the string.

$datasetIds = substr($datasetIds, 1);
$dataElementIds = substr($dataElementIds, 1);
$dispOrder = substr($dispOrder,1);

$session->param("stdRptProviders", $text);
$session->param("stdRptElements", $dataElementIds);
$session->param("stdRptDataset", $datasetIds);
$session->param("stdRptFy", $checkedYear);
$session->param("stdRptFoi", $foiID);
$session->param("stdRptDirection", $rptDirection);
$session->param("stdRepDispOrder", $tmpdispOrder);

# Database

my $dbh;
my ($sql, $sth,$row2,$sql_c,$loook_up,$sth3);


# Get Database connection

my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};

my $defaultYear= $ini_value{'DEFAULT_YEAR'};

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}
my ($sql,$sth,$facility_id,$row,$sql1,$sth1,$report_table,$data_table,$sql2,$sth2,$master_table);
$sql = "select facility_id,data_table,report_table,master_table from tbl_facility_type where facility_name = '$facility'";
$sth = $dbh->prepare($sql);
$sth->execute();
	  while ($row = $sth->fetchrow_hashref) {
	$facility_id = $$row{facility_id};
	$report_table = $$row{report_table};
	$data_table = $$row{data_table};
	$master_table = $$row{master_table};
}

my @provider_list;

foreach $s (split /\n/,$text) {
   $s =~ s/[ \r\n]//g;


   $len = length($s);

   if ($s =~ m/^\S{6}[\:\/](\d{4}|ALL)$/) {
      push @provider_list, $s;
   } elsif ($s =~ m/(^\S{6})[\s-\s]/) {
      push @provider_list, $1;
   } elsif ($s =~ m/^\S{6}$/) {
      push @provider_list, $s;
   }
}

$n = @provider_list;

unless ($n) {
   &display_error('YOU MUST PROVIDE ONE OR MORE 6 CHARACTER PROVIDER IDS');
   exit(0);
}


#get FOI name.
my $recSet;
$sql = "select Provider_number as Number from peergroup_details where PeerGroup_Main_ID = $peerID and FOI = 1";
$sth = $dbh->prepare($sql);
$sth->execute();
while ($recSet = $sth->fetchrow_hashref)
{
	$foiNumber = $$recSet{Number};
}

my ($key, $yyyy, $rpt_num, $rpt_year, $rpt_stus_cd, $fy_bgn_dt, $fy_end_dt, @arrSummary, $recCount, @arrSummaryType, $chkSummary, $summaryHTML, $k, $avgCount);

my ($sth1, $row1, $value, $strHead, $strData, $rowCount, $i, $expResult,$strHeadFY,$strDataFY);
my ($spreadHead, $spreadData);
$spreadData = "NoVal";

$rowCount = 0;
my $i = 0;
$recCount = 0;
$avgCount = 0;
$chkSummary = "false";
my ($colCountFY);
my $align = "right";
my $acrossDown = $rptDirection;

my @elementIds = split(",",$dataElementIds);
my @dispOrds = split(",",$dispOrder);
my $size = $#elementIds;

# Set default display order
if($dispOrder)
{}else{
    for (my $t =0;$t<$size+2;$t++){
    	@dispOrds[$t] = $t;
    }
}
my($newWorkSheet,$newLineNo,$newColNo);

my @strArrayHead = ();
my @strArrayData = ();
my @strArraySpreadHead = ();
my @strArraySpreadData = ();
my $prevprovider;
my $providerid;
my $wild_count = 0;
my $w_count= 0;

if($acrossDown eq "down")
{

foreach $providerid (@provider_list) {

my $prov_size=scalar(@provider_list);

	if ($providerid =~ m/^(\S{6})$/) {
		my @prev_prov;
		$prevprovider="Select PreviousProviderNo from tblpreviousprovider where CurrentProviderNo = '$1' and facility_type = $facility_id";#($providerid,$prevprovider)

	$sth = $dbh->prepare($prevprovider);
	$sth->execute();
	 $providerid=$1;
	 while ($row = $sth->fetchrow_hashref){
	 push (@prev_prov, $$row{PreviousProviderNo});
	 }

	 foreach my $prev_prov(@prev_prov){
	$providerid=$providerid.",".$prev_prov;
		}

		$sql = "SELECT c.PRVDR_NUM,c.RPT_REC_NUM, year(c.FY_END_DT) as RPT_YEAR, c.RPT_STUS_CD, c.FI_NUM, c.FY_BGN_DT, c.FY_END_DT,h.Name as providername FROM $report_table as c LEFT JOIN $master_table as h on h.providerno = c.PRVDR_NUM WHERE PRVDR_NUM in ($providerid) and year(c.FY_END_DT) in ($checkedYear) ORDER BY  $orderBy"; #RPT_REC_NUM

		$sth = $dbh->prepare($sql);
		$sth->execute();

		if(!($foiNumber eq $1))
		{
			$foiCompare = "true";
		}
		my $rowcounts = $sth->rows();
	if($rowcounts>0){
		while ($row = $sth->fetchrow_hashref) {

			$sql = "SELECT sm.metrics_id as metrics_id, element, isFormulae, Formulae, Summary_Info,sm.facility_lookup as look_up FROM standard_metrics as sm inner join standard_reports_metrics as srm on srm.metrics_id = sm.metrics_id WHERE sm.metrics_id in ($dataElementIds) AND srm.reports_id in ($datasetIds) and sm.facility_type = $facility_id ORDER BY sm.metrics_id";

			$sth1 = $dbh->prepare($sql);
			$sth1->execute();

			if($strHead)
			{
				$strHead = "";
				$spreadHead = "NoVal";

			}
			$class1 = (++$i % 2) ? 'alt' : '';
			$strHead = $strHead . "<tr class='gridHeader'>";
			$strData = $strData . "<tr>";

			$k = 0;

			$colCountFY = 1;
				$rowcounts = $sth1->rows();
	if($rowcounts>0){
			while ($row1 = $sth1->fetchrow_hashref) {
				if($$row1{isFormulae} == 0)
				{
					$temp = ($newWorkSheet,$newLineNo,$newColNo) = split(/-/, $$row1{Formulae});
				}
				#$loook_up=$$row1{look_up};
				# - FY_BGN_DT and FY_END_DT are pushing into the second and third column.

				if($colCountFY == 2)
				{

					$strHead = $strHead . "<td nowrap align=left style=\"padding-left:10px;-moz-user-select: none;\"  class=\"sortable-date-dmy fd-column-1\"><a href=\"#\">" . "Provider Number" . "</a></td>";
					$spreadHead = $spreadHead ."~". "Provider Number";
					$strHead = $strHead . "<td nowrap align=left style=\"padding-left:10px;-moz-user-select: none;\"  class=\"sortable-date-dmy fd-column-2\"><a href=\"#\" >" . "Provider Name" . "</a></td>";
					$spreadHead = $spreadHead ."~". "Provider Name";

					$strData = $strData . "<td nowrap align=left style=\"padding-left:10px;\">" . $$row{PRVDR_NUM} . "</td>";
					$spreadData = $spreadData ."~". $$row{PRVDR_NUM};
					$strData = $strData . "<td nowrap align=left style=\"padding-left:10px;\">" . $$row{providername} . "</td>";
					$spreadData = $spreadData ."~". $$row{providername};

					$strHead = $strHead . "<td nowrap align=left style=\"padding-left:10px;-moz-user-select: none;\"  class=\"sortable-date-dmy fd-column-1\"><a href=\"#\">" . "FY_BGN_DT" . "</a></td>";
					$spreadHead = $spreadHead ."~". "FY_BGN_DT";
					$strHead = $strHead . "<td nowrap align=left style=\"padding-left:10px;-moz-user-select: none;\"  class=\"sortable-date-dmy fd-column-2\"><a href=\"#\" >" . "FY_END_DT" . "</a></td>";
					$spreadHead = $spreadHead ."~". "FY_END_DT";

					$strData = $strData . "<td nowrap align=left style=\"padding-left:10px;\">" . $$row{FY_BGN_DT} . "</td>";
					$spreadData = $spreadData ."~". $$row{FY_BGN_DT};
					$strData = $strData . "<td nowrap align=left style=\"padding-left:10px;\">" . $$row{FY_END_DT} . "</td>";
					$spreadData = $spreadData ."~". $$row{FY_END_DT};
				}
				# - starting normal procedure.

					if ($$row1{isFormulae} == 0) # Checking for formulae.
					{
						if ((&check_Alpha_Numeric($$row{RPT_REC_NUM}, $newWorkSheet, $newLineNo, $newColNo, $$row{RPT_YEAR})) eq 'A')
						{
								$align = "left";
						}
						elsif($$row1{look_up} ne ""){
								$align = "left";
						}
					for (my $jmm =0;$jmm<$size+2;$jmm++){
					    if($$row1{metrics_id} == @elementIds[$jmm]){
						      my $dispIndex = @dispOrds[$jmm];
						      @strArrayHead[$dispIndex] = "<td nowrap align=\"$align\" style=\"padding-left:10px;-moz-user-select: none;\" class=\"sortable-keep fd-column-$colCountFY\"><a href=\"#\">" . $$row1{element} . "</a></td>";
						      my $spreadIndex = $dispIndex;
						      if($dispOrder){
						      	$spreadIndex = $spreadIndex-1;
            				}
						      @strArraySpreadHead[$spreadIndex] = $$row1{element};
						}
					}
				}
				else{
						if($$row1{look_up} ne ""){
							$align = "left";
						}
					for (my $jmm =0;$jmm<$size+2;$jmm++){
						if($$row1{metrics_id}==@elementIds[$jmm]){
						      my $dispIndex = @dispOrds[$jmm];
						      @strArrayHead[$dispIndex] = "<td nowrap align=\"$align\" style=\"padding-left:10px;-moz-user-select: none;\" class=\"sortable-keep fd-column-$colCountFY\"><a href=\"#\">" . $$row1{element} . "</a></td>";
						      my $spreadIndex = $dispIndex;
						      if($dispOrder){
							$spreadIndex = $spreadIndex-1;
            					      }
						      @strArraySpreadHead[$spreadIndex] = $$row1{element};
						}
					}
				}
				 my $rec_no = $$row{RPT_REC_NUM};
				if($$row1{look_up} ne ""){
					$sql_c = " select  v.$$row1{look_up} from view_hospital as v where v.rpt_rec_no = $rec_no";
					$sth3 = $dbh->prepare($sql_c);
					$sth3->execute();
						$rowcounts = $sth3->rows();
					if($rowcounts>0){
					while ($row2 = $sth3->fetchrow_hashref) {
						$value = $$row2{$$row1{look_up}};
					}
					}
				 }
				else
				{
					# Checking for exception by calling 'getException' method.

					$expResult = &getException($$row{RPT_REC_NUM}, $$row1{element}, $$row{RPT_YEAR}, $$row1{Formulae});

					if(($expResult) || ($expResult eq "N/A"))
					{
						$value = "N/A";
						$align = "left";
					}
					else
					{
						$value = &getFormulae($$row{RPT_REC_NUM}, $$row1{element}, $$row{RPT_YEAR}, $$row1{Formulae});
						if (looks_like_number($value)) {
							$align = "right";
						}
						else {
							$align = "left";
						}
					}
				}

				for (my $jmm =0;$jmm<$size+2;$jmm++){
					if($$row1{metrics_id}==@elementIds[$jmm]){

						my $dispIndex = @dispOrds[$jmm];
						if($dispOrder)
						{
							$dispIndex = $dispIndex-1;
						}

						@strArrayData[$dispIndex] = "<td nowrap align=\"$align\" style=\"padding-left:10px;\">" . $value . "</td>";
						@strArraySpreadData[$dispIndex] = $value;

						if(!@arrSummary){
							@arrSummary[$dispIndex] = $value;
							@arrSummaryType[$dispIndex] = $$row1{Summary_Info};
						}
						else{
							if(($$row1{Summary_Info} eq "Avg") || ($$row1{Summary_Info} eq "Sum")){
								if($foiID eq "true"){
									$arrSummary[$dispIndex] = $arrSummary[$dispIndex] + $value;
								}
								elsif(!($foiNumber eq $1)){
									$arrSummary[$dispIndex] = $arrSummary[$dispIndex] + $value;
								}
								$chkSummary = "true";
								@arrSummaryType[$dispIndex] = $$row1{Summary_Info};
							}
						}
					}
				}

				$colCountFY++;

				# if only one standard report column selected for displaying.
				# FY_BGN_DT and FY_END_DT are pushing into the second and third column.
				if ($sth1->rows() == 1)
				{

					$strHead = $strHead . "<td nowrap align=left style=\"padding-left:10px;-moz-user-select: none;\"  class=\"sortable-date-dmy fd-column-1\"><a href=\"#\">" . "Provider Number" . "</a></td>";
					$spreadHead = $spreadHead ."~". "Provider Number";
					$strHead = $strHead . "<td nowrap align=left style=\"padding-left:10px;-moz-user-select: none;\"  class=\"sortable-date-dmy fd-column-2\"><a href=\"#\" >" . "Provider Name" . "</a></td>";
					$spreadHead = $spreadHead ."~". "Provider Name";

					$strData = $strData . "<td nowrap align=left style=\"padding-left:10px;\">" . $$row{PRVDR_NUM}. "</td>";
					$spreadData = $spreadData ."~". $$row{PRVDR_NUM};
					$strData = $strData . "<td nowrap align=left style=\"padding-left:10px;\">" . $$row{providername} . "</td>";
					$spreadData = $spreadData ."~". $$row{providername};

					$strHead = $strHead . "<td nowrap align=left style=\"padding-left:10px;-moz-user-select: none;\" class=\"sortable-keep fd-column-1\" >" . "FY_BGN_DT" . "</td>";
					$spreadHead = $spreadHead ."~". "FY_BGN_DT";
					$strHead = $strHead . "<td nowrap align=left style=\"padding-left:10px;-moz-user-select: none;\" class=\"sortable-keep fd-column-2\" >" . "FY_END_DT" . "</td>";
					$spreadHead = $spreadHead ."~". "FY_END_DT";

					$strData = $strData . "<td nowrap align=left style=\"padding-left:10px;\">" . $$row{FY_BGN_DT} . "</td>";
					$spreadData = $spreadData ."~". $$row{FY_BGN_DT};
					$strData = $strData . "<td nowrap align=left style=\"padding-left:10px;\">" . $$row{FY_END_DT} . "</td>";
					$spreadData = $spreadData ."~". $$row{FY_END_DT};
				}
			}
	}
			for (my $jmm =0;$jmm<$size+2;$jmm++){
				$strHead = $strHead.@strArrayHead[$jmm];
				$strData = $strData.@strArrayData[$jmm];
				$spreadHead = $spreadHead."~".@strArraySpreadHead[$jmm];
				$spreadData = $spreadData."~".@strArraySpreadData[$jmm];
			}

			$recCount++;
			if($foiID eq "true")
			{
				$avgCount++;
			}
			elsif(!($foiNumber eq $1))
			{
				$avgCount++;
			}

			$spreadData = $spreadData .":" . "NoVal";
			$strData = $strData . "</tr>";
			$strHead = $strHead . "</tr>";
			$summaryHTML = $summaryHTML . "</tfoot>";#</tr>

			#finding row count for setting division hight
			$rowCount = $rowCount + 1;
  		}
		}
	}

}

}

else
{
	#First Outer loop to print the report elements.


	$sql = "SELECT element, isFormulae, Formulae, srm.Summary_Info,sm.facility_lookup as look_up FROM standard_metrics as sm inner join standard_reports_metrics as srm on srm.metrics_id = sm.metrics_id WHERE sm.metrics_id in ($dataElementIds) AND srm.reports_id in ($datasetIds) and sm.facility_type = $facility_id ORDER BY sm.metrics_id";
	$sth1 = $dbh->prepare($sql);
	$sth1->execute();


	$colCountFY = 1;
	my ($FY_BGN, $FY_END, $PROV_NUM,$PROV_NAME,$spreadPROV_NUM,$spreadPROV_NAME,$spreadFY_BGN, $spreadFY_END, $summaryVal, $summaryType, $summaryAvgCount, $summaryDisplay);

	while ($row1 = $sth1->fetchrow_hashref) {
		if($$row1{isFormulae} == 0)
		{
			$temp = ($newWorkSheet,$newLineNo,$newColNo) = split(/-/, $$row1{Formulae});
		}

		#Adding FY BGN and END scripts to main script.
		if($colCountFY == 2)
		{
			$strData = $strData . $PROV_NUM;
			$strData = $strData . "<td nowrap align=\"$align\" style=\"padding-left:10px;\"><B> </B></td>";
			$spreadData = $spreadData ."~". "Provider Number" ."~". $spreadPROV_NUM;
			$spreadData = $spreadData .":" . "NoVal";

			$strData = $strData . $PROV_NAME;
			$strData = $strData . "<td nowrap align=\"$align\" style=\"padding-left:10px;\"><B> </B></td>";
			$spreadData = $spreadData ."~". "Provider Name" ."~". $spreadPROV_NAME;
			$spreadData = $spreadData .":" . "NoVal";

			$strData = $strData . $FY_BGN;
			$strData = $strData . "<td nowrap align=\"$align\" style=\"padding-left:10px;\"><B> </B></td>";
			$spreadData = $spreadData ."~". "FY_BGN_DT" ."~". $spreadFY_BGN;
			$spreadData = $spreadData .":" . "NoVal";

			$strData = $strData . $FY_END;
			$strData = $strData . "<td nowrap align=\"$align\" style=\"padding-left:10px;\"><B> </B></td>";

			$spreadData = $spreadData ."~". "FY_END_DT" ."~". $spreadFY_END;
			$spreadData = $spreadData .":" . "NoVal";

		}

		$class1 = (++$i % 2) ? 'gridrow' : 'gridalternate';
		$strData = $strData . "<tr class='gridalternate'>";
		$strData = $strData . "<td nowrap align=left style=\"padding-left:10px;\"><B>" . $$row1{element} . "</B></td>";
		$spreadData = $spreadData ."~". $$row1{element};

		#Generating the FY BGN and END scripts.
		if($colCountFY == 1)
		{
			$PROV_NUM = "<tr class='gridalternate'>";
			$PROV_NUM = $PROV_NUM . "<td nowrap align=left style=\"padding-left:10px;\"><B> Provider Number </B></td>";

			$PROV_NAME = "<tr class='gridalternate'>";
			$PROV_NAME = $PROV_NAME . "<td nowrap align=left style=\"padding-left:10px;\"><B> Provider Name </B></td>";

			$FY_BGN = "<tr class='gridalternate'>";
			$FY_BGN = $FY_BGN . "<td nowrap align=left style=\"padding-left:10px;\"><B> FY_BGN_DT </B></td>";

			$FY_END = "<tr class='gridalternate'>";
			$FY_END = $FY_END . "<td nowrap align=left style=\"padding-left:10px;\"><B> FY_END_DT </B></td>";
		}
		$summaryVal = 0;
		$summaryType = "";
		$summaryAvgCount = 1;
		#Second Inner loopto print the provider info.
		foreach $string (@provider_list) {
			if ($string =~ m/^(\S{6})$/) {

				$sql = "SELECT c.PRVDR_NUM,c.RPT_REC_NUM, year(c.FY_END_DT) as RPT_YEAR, c.RPT_STUS_CD, c.FI_NUM, c.FY_BGN_DT, c.FY_END_DT,h.Name as providername FROM $report_table as c LEFT JOIN $master_table as h on h.providerno = c.PRVDR_NUM WHERE PRVDR_NUM = '$1' and year(c.FY_END_DT) in ($checkedYear) ORDER BY c.RPT_REC_NUM ";
				$sth = $dbh->prepare($sql);
				$sth->execute();
				while ($row = $sth->fetchrow_hashref) {

					#Generating the FY BGN and END scripts.
					if($colCountFY == 1)
					{
						$PROV_NUM = $PROV_NUM . "<td nowrap align=\"$align\" style=\"padding-left:10px;\">" . $$row{PRVDR_NUM} . "</td>";
						if($spreadPROV_NUM)
						{
							$spreadPROV_NUM = $spreadPROV_NUM ."~". $$row{PRVDR_NUM};
						}
						else
						{
							$spreadPROV_NUM = $$row{PRVDR_NUM};
						}

						$PROV_NAME = $PROV_NAME . "<td nowrap align=\"$align\" style=\"padding-left:10px;\">" . $$row{providername} . "</td>";
						if($spreadPROV_NAME)
						{
							$spreadPROV_NAME = $spreadPROV_NAME ."~". $$row{providername} ;
						}
						else
						{
							$spreadPROV_NAME = $$row{providername};
						}

						$FY_BGN = $FY_BGN . "<td nowrap align=\"$align\" style=\"padding-left:10px;\">" . $$row{FY_BGN_DT} . "</td>";
						if($spreadFY_BGN)
						{
							$spreadFY_BGN = $spreadFY_BGN ."~". $$row{FY_BGN_DT};
						}
						else
						{
							$spreadFY_BGN = $$row{FY_BGN_DT};
						}

						$FY_END = $FY_END . "<td nowrap align=\"$align\" style=\"padding-left:10px;\">" . $$row{FY_END_DT} . "</td>";
						if($spreadFY_END)
						{
							$spreadFY_END = $spreadFY_END ."~". $$row{FY_END_DT};
						}
						else
						{
							$spreadFY_END = $$row{FY_END_DT};
						}
					}

					my $rec_no = $$row{RPT_REC_NUM};
					if($$row1{look_up} ne ""){
					$sql_c = " select  v.$$row1{look_up} from view_hospital as v where v.rpt_rec_no = $rec_no";
					$sth3 = $dbh->prepare($sql_c);
					$sth3->execute();
					$rowcounts = $sth3->rows();
					if($rowcounts>0){
						while ($row2 = $sth3->fetchrow_hashref) {
							$value = $$row2{$$row1{look_up}};
						}
						}
					}
					else
					{
						# Checking for exception by calling 'getException' method.

						$expResult = &getException($$row{RPT_REC_NUM}, $$row1{element}, $$row{RPT_YEAR}, $$row1{Formulae});

						if(($expResult) || ($expResult eq "N/A"))
						{
							$value = "N/A";
							$align = "left";
						}
						else
						{
							$value = &getFormulae($$row{RPT_REC_NUM}, $$row1{element}, $$row{RPT_YEAR}, $$row1{Formulae});
							if (looks_like_number($value)) {
								$align = "right";
							}
							else {
								$align = "left";
							}
						}
					}
					#Adding Summary Information.
					if(($$row1{Summary_Info} eq "Avg") || ($$row1{Summary_Info} eq "Sum"))
					{
						if($foiID eq "true")
						{
							$summaryVal = $summaryVal + $value;
							$summaryAvgCount++;
						}
						elsif(!($foiNumber eq $1))
						{
							$summaryVal = $summaryVal + $value;
							$summaryAvgCount;
						}
						$summaryType = $$row1{Summary_Info};
					}

					$strData = $strData . "<td nowrap align=\"$align\" style=\"padding-left:10px;\">" . $value . "</td>";
					$spreadData = $spreadData ."~". $value;
				}
			}
		}

		#Calculating and Adding summary information.
		if($summaryType eq "Avg")
		{
			$summaryDisplay = sprintf "%.2f", $summaryVal / $summaryAvgCount;
			$strData = $strData . "<td nowrap align=\"$align\" style=\"padding-left:10px;\"><B>" . $summaryDisplay . "</B></td>";
		}
		elsif($summaryType eq "Sum")
		{
			$summaryDisplay = sprintf "%.2f", $summaryVal;
			$strData = $strData . "<td nowrap align=\"$align\" style=\"padding-left:10px;\"><B>" . $summaryDisplay . "</B></td>";
		}
		else
		{
			$strData = $strData . "<td nowrap align=\"$align\" style=\"padding-left:10px;\"><B> </B></td>";
		}
		$summaryVal = 0;
		$summaryType = "";
		$summaryAvgCount = 1;

		$spreadData = $spreadData ."~". $summaryDisplay;
		$spreadData = $spreadData .":" . "NoVal";

		#Handling when only one record
		if ($sth1->rows() == 1)
		{
			$strData = $strData . $PROV_NUM;
			$strData = $strData . "<td nowrap align=\"$align\" style=\"padding-left:10px;\"><B> </B></td>";
			$spreadData = $spreadData ."~". "Provider Number" ."~". $spreadPROV_NUM;
			$spreadData = $spreadData .":" . "NoVal";

			$strData = $strData . $PROV_NAME;
			$strData = $strData . "<td nowrap align=\"$align\" style=\"padding-left:10px;\"><B> </B></td>";
			$spreadData = $spreadData ."~". "Provider Name" ."~". $spreadPROV_NAME;
			$spreadData = $spreadData .":" . "NoVal";

			$strData = $strData . $FY_BGN;
			$strData = $strData . "<td nowrap align=\"$align\" style=\"padding-left:10px;\"><B> </B></td>";
			$spreadData = $spreadData ."~". "FY_BGN_DT" ."~". $spreadFY_BGN;
			$spreadData = $spreadData .":" . "NoVal";

			$strData = $strData . $FY_END;
			$strData = $strData . "<td nowrap align=\"$align\" style=\"padding-left:10px;\"><B> </B></td>";
			$spreadData = $spreadData ."~". "FY_END_DT" ."~". $spreadFY_END;
			$spreadData = $spreadData .":" . "NoVal";
		}

		$strData = $strData . "</tr>";
		$colCountFY++;

		#Generating the FY BGN and END scripts.
		if($colCountFY == 1)
		{
			$PROV_NUM=$PROV_NUM."</tr>";
			$PROV_NAME=$PROV_NAME."</tr>";
			$FY_BGN = $FY_BGN . "</tr>";
			$FY_END = $FY_END . "</tr>";
		}
	}
}

$spreadData = $spreadData .":" . "NoVal";
$summaryHTML = "<tfoot class='gridHeader'>";
for (my $j=0; $j<scalar(@arrSummaryType);$j++)
{
	# - FY_BGN_DT and FY_END_DT summary part are pushing into the second and third column.
	if($j == 0)
	{
		$summaryHTML = $summaryHTML . "<td nowrap style=\"padding-left:10px;\"> </td>";
		$spreadData = $spreadData ."~". " ";
		$summaryHTML = $summaryHTML . "<td nowrap style=\"padding-left:10px;\"> </td>";
		$spreadData = $spreadData ."~". " ";
		$summaryHTML = $summaryHTML . "<td nowrap style=\"padding-left:10px;\"> </td>";
		$spreadData = $spreadData ."~". " ";
		$summaryHTML = $summaryHTML . "<td nowrap style=\"padding-left:10px;\"> </td>";
		$spreadData = $spreadData ."~". " ";
	}
	if($arrSummaryType[$j] eq "Avg")
	{
		if($foiID eq "true")
		{
			$arrSummary[$j] = sprintf "%.2f", $arrSummary[$j] / $avgCount;
			$spreadData = $spreadData ."~". $arrSummary[$j];
		}
		elsif($foiCompare)
		{
			if($avgCount)
			{
				$arrSummary[$j] = sprintf "%.2f", $arrSummary[$j] / ($avgCount);
				$spreadData = $spreadData ."~". $arrSummary[$j];
			}
		}
		$summaryHTML = $summaryHTML . "<td nowrap align=right style=\"padding-left:10px;\">"  . $arrSummary[$j] . "</td>";
	}
	elsif($arrSummaryType[$j] eq "Sum")
	{
		$arrSummary[$j] = sprintf "%.2f", $arrSummary[$j] ;
		$summaryHTML = $summaryHTML . "<td nowrap align=right style=\"padding-left:10px;\">"  . $arrSummary[$j] . "</td>";
		$spreadData = $spreadData ."~". $arrSummary[$j];
	}
	else
	{
		$summaryHTML = $summaryHTML . "<td nowrap style=\"padding-left:10px;\"> </td>";
		$spreadData = $spreadData ."~". " ";
	}
}
$rowCount = 380;

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>Standard Reports</TITLE>
<link href="/css/standard_report/std_report_display.css" rel="stylesheet" type="text/css" />
 <script type="text/javascript" src="/JS/standard_report/standard_reports_display.js"></script>
<script type="text/javascript" src="/JS/tablesort.js"></script>
<!-- <SCRIPT SRC="/JS/sorttable.js"></SCRIPT> -->
</HEAD>
<BODY>
<FORM NAME = "frmStandardReports" Action="/cgi-bin/std_reports_spreadsheet.pl" method="post">
<CENTER>
<div id="overlay" style="position:absolute;z-index:10000;visibility:hidden;background-image:url(/images/background-trans.png);height:96%;width:96%;" ></div>
};

&innerHeader();
print qq{
    <!--content start-->

    <table cellpadding="0" cellspacing="0" width="885px">
        <tr>
            <td valign="top">
                <img src="/images/content_left.gif" /></td>
            <td valign="top" style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; ">


             <div id='divData' style='height:*}; #print $rowCount;
             print qq{px;width:885px;overflow:auto;' >
            <table width=100% cellspacing="0" cellpadding="0">
            	<tr>
            		<td>
            			<table id="test1" class="sortable-onload-3-reverse rowstyle-alt no-arrow" bgcolor="#ffffff" style="border:solid 1px #b1bed0;color:#0c4f7a;" width=100% cellspacing="1" cellpadding="1"  >
	    		    	        };
	    		    	        print $strHead;
	    		    	        print $strData;
	    		    	        print $summaryHTML;
	    		    	        print qq{
            			</table>
            		</td>
            	</tr>
            	<tr>
            		<td>
            			<table class="sortable" width=100% style="border:solid 1px #b1bed0;color:#0c4f7a;" cellspacing="0" cellpadding="0"> };

					print qq{
            			</table>
            		</td>
            	</tr>
            </table>
            </div>
            <br>
            <span class="button"><INPUT TYPE="button" VALUE="GENERATE SPREADSHEET" onclick='generateSpreadSheet()' ></span>
};
	#if($acrossDown eq "down"){
	#	print "<span class=\"button\"><INPUT TYPE=\"button\" VALUE=\"GENERATE PDF\" onclick=\"overlay()\" ></span>\n";
	#}else{
	#	print "<span class=\"button\"><INPUT TYPE=\"button\" VALUE=\"GENERATE PDF\" onclick=\"overlay()\" disabled></span>\n";
	#}

print qq{
            <span class=\"button\"><INPUT TYPE="button" VALUE="BACK"  onclick='callBackStdReport()'> </span>
            <input type="hidden" name="spreadHead" value="$spreadHead">
            <input type="hidden" name="spreadData" value="$spreadData">
            </td>
            <td valign="top">
	                    <img src="/images/content_right.gif" />
	    </td>
        </tr>
    </table>
    <!--content end-->
	};
&TDdoc;
	print qq{
   <input type="hidden" name="providers" value="$text">
   <input type="hidden" name="yearCheck" value="$checkedYear">
   <input type="hidden" name="datasetIds" value="$tmpdatasetIds">
   <input type="hidden" name="dataElementIds" value="$tmpdataElements">
   <input type="hidden" name="dispOrder" value="$tmpdispOrder">
   <input type="hidden" name="peerGroupSelect" value="$peerID">
   <input type="hidden" name="foistatistics" value="$foiID">
   <input type="hidden" name="rptDirection" value="$rptDirection">
   <input type="hidden" name="orderBy" value="$orderBy">
   <input type="hidden" name="facility" value="$facility">
</FORM>
</BODY>
</HTML>
};

#--------------------------------------------------------------------------------------
sub sessionExpire
{

print "Content-Type: text/html\n\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};
&headerScript();
print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#------------------------------------------------------------------------------

sub get_value
{
   my ($report_id, $worksheet, $line, $column, $fYear) = @_;

   my $yyyy = $fYear;
	my $count=0;
   my ($sthVal, $value);
   my  $wildcard = substr( $line, 3, 5 );

   if($wildcard eq '**')
 {
 $line = substr( $line, 0, 3 );
 my $sql = "

    SELECT sum(CR_VALUE)
      FROM $data_table
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE    LIKE '".$line."__'
       AND CR_COLUMN     = ?

 ";

  unless ($sthVal = $dbh->prepare($sql)) {
       &display_error('Error preparing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sthVal->execute($report_id, $worksheet, $column)) {
       &display_error('Error executing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

 }
 else
 {

   my $sql = qq(
    SELECT CR_VALUE
      FROM $data_table
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?

   );

   unless ($sthVal = $dbh->prepare($sql)) {
       &display_error('Error preparing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sthVal->execute($report_id, $worksheet, $line, $column)) {
       &display_error('Error executing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }
   }
   if ($sthVal->rows) {
      $value = $sthVal->fetchrow_array;
   }

   $sthVal->finish();

   return $value;
}

#------------------------------------------------------------------------------

sub check_Alpha_Numeric
{
   my ($report_id, $worksheet, $line, $column, $fYear) = @_;

   my $yyyy = $fYear;

   my ($sthVal, $value);
	my  $wildcard = substr( $line, 3, 5 );

 if($wildcard eq '**')
 {
 $line = substr( $line, 0, 3 );
 my $sql = "

    SELECT CR_TYPE
      FROM $data_table
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE  LIKE '".$line."__'
       AND CR_COLUMN     = ?

 ";
  unless ($sthVal = $dbh->prepare($sql)) {
       &display_error('Error preparing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sthVal->execute($report_id, $worksheet, $column)) {
       &display_error('Error executing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

 }
 else
 {
   my $sql = qq(
    SELECT CR_TYPE
      FROM $data_table
     WHERE CR_REC_NUM    = ?
       AND CR_WORKSHEET  = ?
       AND CR_LINE       = ?
       AND CR_COLUMN     = ?
   );

   unless ($sthVal = $dbh->prepare($sql)) {
       &display_error('Error preparing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }

   unless ($sthVal->execute($report_id, $worksheet, $line, $column)) {
       &display_error('Error executing SQL: ', $sthVal->errstr);
       $dbh->disconnect();
       exit(0);
   }
   }
   if ($sthVal->rows) {
      $value = $sthVal->fetchrow_array;
   }

   $sthVal->finish();

   return $value;
}

#----------------------------------------------------------------------------

sub getFormulae
{
	my ($report_id, $element, $yyyy, $formulae) = @_;
	my ($value);

	$value = &getFormulaeValue($report_id,$formulae,$yyyy);

	#EBITDAR as a Percent of Interest Expense
	#Net Revenue per FTE

	return $value;
}

#----------------------------------------------------------------------------
sub getAddressValOld
{
    my ($report_id,$address,$yyyy) = @_;

    my ($worksheet, $line, $column) = split /-/,$address;

    return(&get_value($report_id, $worksheet, $line, $column, $yyyy));
}
#----------------------------------------------------------------------------

sub getFormulaeValue
{
	my ($report_id,$formulae,$yyyy) = @_;
	my ($value, $i, $elementVal, $operation);

	my $is_formulae = 1;
	my $pattern = &get_pattern_orig_address();
	if ( length($formulae) == 18 && ($formulae =~ /$pattern/) ) {
		$is_formulae = 0;
	}

	for ($i=0;$i<length($formulae);$i++)
	{
		if(substr($formulae, $i,1) =~ /[A-Z]/ )
		{
			#Spliting elements from Address
			my ($worksheet, $line, $column) = split /[-:]/,substr($formulae, $i,18);
			if(&get_value($report_id, $worksheet, $line, $column, $yyyy))
			{
				$elementVal = &get_value($report_id, $worksheet, $line, $column, $yyyy);
			}
			else
			{
				$elementVal = 0;
			}


			$value = $value.$elementVal;
			$i = $i+17;
		}
		elsif(substr($formulae, $i,1) =~ /\s/ )
		{

		}
		else
		{
			$value = $value . substr($formulae, $i,1);
		}
	}
	if ($is_formulae) {
		$value = eval($value);
	}
	if(!$value)
	{
		$value = "N/A";
	}
	else
	{
		if (looks_like_number($value)) {
			$value = sprintf "%.2f", $value;
		}
	}
	return $value;

}


#----------------------------------------------------------------------------

# - Get details from Type of Control table.

sub getControlTypeDetails
{
	my ($id) = @_;

	my ($qry, $stmt, $record, $value);

	$qry = "select Control_Type_Name from control_type where Control_Type_id = $id";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();

	while ($record = $stmt->fetchrow_hashref) {
		$value = $$record{Control_Type_Name};
  	}
  	return $value
}

#----------------------------------------------------------------------------

# - Get Exception values

sub getException
{
	my ($report_id, $element, $yyyy, $formulae) = @_;

	my ($qry, $stmt, $record, $value, $result);

	$qry = "select * from exception_details where '$formulae' like CONCAT('%',Formula,'%')";
	$stmt = $dbh->prepare($qry);
	$stmt->execute();
	while ($record = $stmt->fetchrow_hashref) {
		$value = &getFormulaeValue($report_id,$$record{Formula},$yyyy);
		if( $value != "N/A" )
		{
			if($$record{Sign} eq "+/-")
			{
				$result = abs($value) . " " . $$record{Operator} . " " . $$record{Value};
				$result = eval($result);
				if($result)
				{
					last;
				}
			}
			else
			{
				$result = $value . " " . $$record{Operator} . " " . $$record{Sign} . $$record{Value};
				$result = eval($result);
				if($result)
				{
					last;
				}
			}
		}
		else
		{
			$result = $value
		}
  	}

  	return $result
}


#--------------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
</HEAD>
<BODY>
<CENTER>
};

&innerHeader();
print qq{
<BR>
<BR>

    <FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>
</CENTER>
</BODY>
</HTML>
};

}
