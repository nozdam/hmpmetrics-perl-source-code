#!/usr/bin/perl

use strict;
use DBI;
use CGI;
do "pagefooter.pl";
do "common_header.pl";
do "audit.pl";
use CGI::Session;

my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");
my $facility = $cgi->param("facility");
my ($sql_f,$facility_id,$master_table,$report_table,$data_table,$row,$sth1,$view,$s,$section_id,$sql_sec,$sql_s,@search_elements,$i);
my $thpsql;
if(!$userID)
{
	&sessionExpire;
}
my $orderBy = $cgi->param('orderBy');
my $acsdesc = $cgi->param('acsdesc');
my $tmpOrder = $orderBy;
if(!$orderBy){
$orderBy= "Provider_number";
}
if(!$acsdesc){
	$acsdesc = "true";
}
my $peermainId    = $cgi->param('mainID');
my($prov,$zip,$county,$name,$address,$city,$p_peergroupname,$p_state,$p_ctype,$p_msa,$p_system,$p_htype,$p_thosp,$p_geoarea,$p_bedstart,$p_bedend,$p_ftestart,$p_fteend,$p_incomestart,$p_incomeend,$p_Zscoremin,$p_Zscoremax,$p_OprMarginmin,$p_OprMarginmax,$p_Within,$p_c_Miles_Of_zip,$row);

my $narrow_search = $cgi->param("narrow_search");
my $narrow_search_module = $cgi->param("narrow_search_module");
my $sess_peermainId=$session->param("sess_peermainId");
my $lHeight ;
my $tbHeight ;
my $lstrDisp ;
my $lstrStore='';

if($narrow_search_module eq ""){
$narrow_search = $session->param("narrow_search");
$narrow_search_module = $session->param("narrow_search_module");
}
#$session->param("narrow_search_module", "state");

if($narrow_search_module eq "state" && $narrow_search == "0"){
my $state_map=$cgi->param('state');
if($state_map){
$session->param("nr_state",$state_map);
$session->param("nr_facility",$facility);
	$session->param("nr_zip","");
	$session->param("nr_county","");
	$session->param("nr_name","");
	$session->param("nr_address","");
	$session->param("nr_city","");
	$session->param("nr_provider","");
	$session->param("nr_ctype","");
	$session->param("nr_msa","");
	$session->param("nr_system","");
	$session->param("nr_htype","");
	$session->param("nr_thosp","");
	$session->param("nr_geoarea","");
	$session->param("nr_bedstart","");
	$session->param("nr_bedend","");
	$session->param("nr_ftestart","");
	$session->param("nr_fteend","");
	$session->param("nr_incomestart","");
	$session->param("nr_incomeend","");
	$session->param("nr_Zscoremin","");
	$session->param("nr_Zscoremax","");
	$session->param("nr_OprMarginmin","");
	$session->param("nr_OprMarginmax","");
	$session->param("nr_Within","");
	$session->param("nr_p_c_Miles_Of_zip","");
	$session->param("narrow_search", "1");

#assign the session values to search criteria parameters
	$zip=$session->param("nr_zip");
	$county=$session->param("nr_county");
	$prov=$session->param("nr_provider");
	$name=$session->param("nr_name");
	$address=$session->param("nr_address");
	$city=$session->param("nr_city");
	$p_peergroupname=$session->param("nr_peername");
	$p_state=$session->param("nr_state");
	$p_ctype=$session->param("nr_ctype");
	$p_msa=	$session->param("nr_msa");
	$p_system=$session->param("nr_system");
	$p_htype=$session->param("nr_htype");
	$p_thosp=$session->param("nr_thosp");
	$p_geoarea=$session->param("nr_geoarea");
	$p_bedstart=$session->param("nr_bedstart");
	$p_bedend=$session->param("nr_bedend");
	$p_ftestart=$session->param("nr_ftestart");
	$p_fteend=$session->param("nr_fteend");
	$p_incomestart=$session->param("nr_incomestart");
	$p_incomeend=$session->param("nr_incomeend");
	$p_Zscoremin=$session->param("nr_Zscoremin");
	$p_Zscoremax=$session->param("nr_Zscoremax");
	$p_OprMarginmin=$session->param("nr_OprMarginmin");
	$p_OprMarginmax=$session->param("nr_OprMarginmax");
	$p_Within=$session->param("nr_Within");
	$facility=$session->param("nr_facility");
	$p_c_Miles_Of_zip=$session->param("nr_p_c_Miles_Of_zip");

#$narrow_search = "1";
}
}
if($peermainId){
	$session->param("sess_peermainId", $peermainId);
}

if(!$peermainId )
{
	$peermainId    = $session->param("sess_peermainId");
}
my $check;
#from search page
if($narrow_search_module eq "search" && $narrow_search == "0"){
#get the data from search page parameters
	my $provider  = $cgi->param('provider');
	my $searchProvider  = $cgi->param('provider');
	my $pr_name      = $cgi->param('name');
	my $pr_address   = $cgi->param('address');
	my $pr_city      = $cgi->param('city');
	my $pr_zip       = $cgi->param('zip');
	my $pr_county    = $cgi->param('county');
	my $location  = $cgi->param('location');
	my $radius    = $cgi->param('radius');
	my $min_ftes  = $cgi->param('min_ftes');
	my $max_ftes  = $cgi->param('max_ftes');
	my $min_beds  = $cgi->param('min_beds');
	my $max_beds  = $cgi->param('max_beds');
	my $min_net_income = $cgi->param('min_net_income');
	my $max_net_income = $cgi->param('max_net_income');
	my $max_Zscore = $cgi->param('max_Zscore');
	my $min_Zscore = $cgi->param('min_Zscore');
	my $max_Opr_Margin = $cgi->param('max_Opr_Margin');
	my $min_Opr_Margin = $cgi->param('min_Opr_Margin');
	my $stateText = $cgi->param('srcStateText');
	my $states    = $cgi->param('state');
	my $toc       = $cgi->param('toc');        # type of control
	my $hosp_type = $cgi->param('HospType');
	my $system    = $cgi->param('system');
	my $msa       = $cgi->param('msa');
	my $teach_hosp= $cgi->param('Teaching_Hosp');
	my $geoArea   = $cgi->param('Geographical_Area');
	my $systemText = $cgi->param('srcSystemText');
	my $hospText = $cgi->param('srcHospText');
	my $tocText = $cgi->param('srcTocText');
	my $msaText = $cgi->param('srcMSAText');
	my $teachText = $cgi->param('srcTeachText');
	my $geoArText = $cgi->param('srcGeoText');

#storing the value in session
	$session->param("nr_zip",$pr_zip);
	$session->param("nr_county",$pr_county);
	$session->param("nr_name",$pr_name);
	$session->param("nr_address",$pr_address);
	$session->param("nr_city",$pr_city);
	$session->param("nr_provider",$provider);
	$session->param("nr_state",$stateText);
	$session->param("nr_ctype",$tocText);
	$session->param("nr_msa",$msa);
	$session->param("nr_system",$systemText);
	$session->param("nr_htype",$hospText);
	$session->param("nr_thosp",$teach_hosp);
	$session->param("nr_geoarea",$geoArText);
	$session->param("nr_bedstart",$min_beds);
	$session->param("nr_bedend",$max_beds);
	$session->param("nr_ftestart",$min_ftes);
	$session->param("nr_fteend",$max_ftes);
	$session->param("nr_incomestart",$min_net_income);
	$session->param("nr_incomeend",$max_net_income);
	$session->param("nr_Zscoremin",$max_Zscore);
	$session->param("nr_Zscoremax",$min_Zscore);
	$session->param("nr_OprMarginmin",$min_Opr_Margin);
	$session->param("nr_OprMarginmax",$max_Opr_Margin);
	$session->param("nr_Within",$radius);
	$session->param("nr_p_c_Miles_Of_zip",$location);
	$session->param("nr_facility",$facility);
	$session->param("narrow_search", "1");
	$session->param("narrow_search_module","search");
#assign the session values to search criteria parameters
	$zip=$session->param("nr_zip");
	$county=$session->param("nr_county");
	$prov=$session->param("nr_provider");
	$name=$session->param("nr_name");
	$address=$session->param("nr_address");
	$city=$session->param("nr_city");
	$p_peergroupname=$session->param("nr_peername");
	$p_state=$session->param("nr_state");
	$p_ctype=$session->param("nr_ctype");
	$p_msa=	$session->param("nr_msa");
	$p_system=$session->param("nr_system");
	$p_htype=$session->param("nr_htype");
	$p_thosp=$session->param("nr_thosp");
	$p_geoarea=$session->param("nr_geoarea");
	$p_bedstart=$session->param("nr_bedstart");
	$p_bedend=$session->param("nr_bedend");
	$p_ftestart=$session->param("nr_ftestart");
	$p_fteend=$session->param("nr_fteend");
	$p_incomestart=$session->param("nr_incomestart");
	$p_incomeend=$session->param("nr_incomeend");
	$p_Zscoremin=$session->param("nr_Zscoremin");
	$p_Zscoremax=$session->param("nr_Zscoremax");
	$p_OprMarginmin=$session->param("nr_OprMarginmin");
	$p_OprMarginmax=$session->param("nr_OprMarginmax");
	$p_Within=$session->param("nr_Within");
	$p_c_Miles_Of_zip=$session->param("nr_p_c_Miles_Of_zip");
	$facility=$session->param("nr_facility");
	$narrow_search = $session->param("narrow_search");
	$narrow_search_module = $session->param("narrow_search_module");
}


my %ini_value;

unless (&read_inifile('milo.ini')) {
   &display_error('CAN\'T READ milo.ini');
   exit(1);
}

my $dbtype     = $ini_value{'DB_TYPE'};
my $dblogin    = $ini_value{'DB_LOGIN'};
my $dbpasswd   = $ini_value{'DB_PASSWORD'};
my $dbname     = $ini_value{'DB_NAME'};
my $dbserver   = $ini_value{'DB_SERVER'};
my $defaultYear= $ini_value{'DEFAULT_YEAR'};
my ($sql, $dbh, $sth);
my($sql_l,$sth_l,$dbh_l);

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", DBI::errstr);
   exit(1);
}
#To get tables based on the facility type
$sql_f = "select facility_id ,master_table,report_table,data_table,view_facility from tbl_facility_type where facility_name = '$facility'";
$sth = $dbh->prepare($sql_f);
$sth->execute();

while ($row = $sth->fetchrow_hashref) {
	$facility_id = $$row{facility_id};
	$master_table = $$row{master_table};
	$report_table = $$row{report_table};
	$data_table = $$row{data_table};
	$view= $$row{view_facility};
}

$sql_sec = "select section_id from tbl_section
where path = '/cgi-bin/peersearch.pl' and name = 'search result reports'";
$sth = $dbh->prepare($sql_sec);
$sth->execute();
while ($row = $sth->fetchrow_hashref) {
	$section_id = $$row{section_id};
}

$sql_s = "select se.section_element_name as search_element from
tbl_section_element as se
inner join tbl_section_element_details as sd on
sd.section_element_id = se.section_element_id
where sd.facility_type = $facility_id
and se.section_id = $section_id
order by se.section_element_id";
$sth = $dbh->prepare($sql_s);
$sth->execute();

while ($row = $sth->fetchrow_hashref) {
	push(@search_elements,$$row{search_element});
}

#for narrow search
my $toc_id = $cgi->param('Type_cntrl');
my $hty_id = $cgi->param('Hosp_type');
my $msa_id = $cgi->param('Msa_type_code');
my $sys_id = $cgi->param('System_type_Id');
my $tch_id = $cgi->param('Teach_type_value');
my $st_id= $cgi->param('State_name_id');
my $geo_id=$cgi->param('geo_type_name');
my $bed_id=$cgi->param('bed_code');
my $fte_id=$cgi->param('fte_code');
my $income_id=$cgi->param('income_code');

#to remove search criteria getting data from hidden textbox
my $remove_toc=$cgi->param('rm_toc');
my $remove_hosp=$cgi->param('rm_hosp');
my $remove_msa=$cgi->param('rm_msa');
my $remove_state=$cgi->param('rm_state');
my $remove_system=$cgi->param('rm_system');
my $remove_city=$cgi->param('rm_city');
my $remove_county=$cgi->param('rm_county');
my $remove_address=$cgi->param('rm_address');
my $remove_zip=$cgi->param('rm_zip');
my $remove_provname=$cgi->param('rm_name');
my $remove_prov=$cgi->param('rm_prov');
my $remove_thosp=$cgi->param('rm_thosp');
my $remove_geoar=$cgi->param('rm_geoar');
my $remove_bedst=$cgi->param('rm_bedstart');
my $remove_bedend=$cgi->param('rm_bedend');
my $remove_ftest=$cgi->param('rm_ftestart');
my $remove_fteend=$cgi->param('rm_fteend');
my $remove_incomest=$cgi->param('rm_incomestart');
my $remove_incomeend=$cgi->param('rm_incomeend');
my $remove_zscorest=$cgi->param('rm_zscoremin');
my $remove_zscoreend=$cgi->param('rm_zscoremax');
my $remove_OprMarginst=$cgi->param('rm_OprMarginmin');
my $remove_OprMarginend=$cgi->param('rm_OprMarginmax');

#to remove search criteria
if ($remove_zip) {
	if($remove_zip eq 'BIN_EMPTY'){
		$zip = "";
		$session->param("nr_name", $zip);
	}else{
		$zip = $remove_zip;
		$session->param("nr_name", $zip);
	}
}
if ($remove_county) {
	if($remove_county eq 'BIN_EMPTY'){
		$county = "";
		$session->param("nr_county", $county);
	}else{
		$county = $remove_county;
		$session->param("nr_county", $county);
	}
}
if ($remove_address) {
	if($remove_address eq 'BIN_EMPTY'){
		$address = "";
		$session->param("nr_address", $address);
	}else{
		$address = $remove_address;
		$session->param("nr_address", $address);
	}
}
if ($remove_city) {
	if($remove_city eq 'BIN_EMPTY'){
		$city = "";
		$session->param("nr_city", $city);
	}else{
		$city = $remove_city;
		$session->param("nr_city", $city);
	}
}
if ($remove_provname) {
	if($remove_provname eq 'BIN_EMPTY'){
		$name = "";
		$session->param("nr_name", $name);
	}else{
		$name = $remove_provname;
		$session->param("nr_name", $name);
	}
}
if ($remove_prov) {
	if($remove_prov eq 'BIN_EMPTY'){
		$prov = "";
		$session->param("nr_prov", $prov);
	}else{
		$prov = $remove_prov;
		$session->param("nr_prov", $prov);
	}
}
if ($remove_incomest) {
	if($remove_incomest eq 'BIN_EMPTY'){
		$p_incomestart = "";
		$session->param("nr_incomestart", $p_incomestart);
	}else{
		$p_incomestart = $remove_incomest;
		$session->param("nr_incomestart", $p_incomestart);
	}
}
if ($remove_incomeend) {
	if($remove_incomeend eq 'BIN_EMPTY'){
		$p_incomeend = "";
		$session->param("nr_incomeend", $p_incomeend );
	}else{
		$p_incomeend  = $remove_incomeend;
		$session->param("nr_incomeend", $p_incomeend);
	}
}
if ($remove_zscorest) {
	if($remove_zscorest eq 'BIN_EMPTY'){
		$p_Zscoremin = "";
		$session->param("nr_Zscoremin", $p_Zscoremin);
	}else{
		$p_Zscoremin = $remove_zscorest;
		$session->param("nr_Zscoremin", $p_Zscoremin);
	}
}
if ($remove_zscoreend) {
	if($remove_zscoreend eq 'BIN_EMPTY'){
		$p_Zscoremax = "";
		$session->param("nr_Zscoremax", $p_Zscoremax );
	}else{
		$p_Zscoremax  = $remove_zscoreend;
		$session->param("nr_Zscoremax", $p_Zscoremax );
	}
}
if ($remove_OprMarginst) {
	if($remove_OprMarginst eq 'BIN_EMPTY'){
		$p_OprMarginmin = "";
		$session->param("nr_OprMarginmin", $p_OprMarginmin);
	}else{
		$p_OprMarginmin = $remove_OprMarginst;
		$session->param("nr_OprMarginmin", $p_OprMarginmin);
	}
}
if ($remove_OprMarginend) {
	if($remove_OprMarginend eq 'BIN_EMPTY'){
		$p_OprMarginmax = "";
		$session->param("nr_OprMarginmax", $p_OprMarginmax );
	}else{
		$p_OprMarginmax = $remove_OprMarginend;
		$session->param("nr_OprMarginmax", $p_OprMarginmax );
	}
}
if ($remove_ftest) {
	if($remove_ftest eq 'BIN_EMPTY'){
		$p_ftestart = "";
		$session->param("nr_ftestart", $p_ftestart);
	}else{
		$p_ftestart = $remove_ftest;
		$session->param("nr_ftestart", $p_ftestart);
	}
}
if ($remove_fteend) {
	if($remove_fteend eq 'BIN_EMPTY'){
		$p_fteend = "";
		$session->param("nr_fteend", $p_fteend );
	}else{
		$p_fteend  = $remove_fteend;
		$session->param("nr_fteend", $p_fteend );
	}
}
if ($remove_bedst) {
	if($remove_bedst eq 'BIN_EMPTY'){
		$p_bedstart = "";
		$session->param("nr_bedstart", $p_bedstart);
	}else{
		$p_bedstart = $remove_bedst;
		$session->param("nr_bedstart", $p_bedstart);
	}
}
if ($remove_bedend) {
	if($remove_bedend eq 'BIN_EMPTY'){
		$p_bedend = "";
		$session->param("nr_bedend", $p_bedend);
	}else{
		$p_bedend = $remove_bedend;
		$session->param("nr_bedend", $p_bedend);
	}
}
if ($remove_state) {
	if($remove_state eq 'BIN_EMPTY'){
		$p_state = "";
		$session->param("nr_state", $p_state);
	}else{
		$p_state = $remove_state;
		$session->param("nr_state", $p_state);
	}
}
if ($remove_system) {

	if($remove_system eq 'BIN_EMPTY'){
		$p_system = "";
		$session->param("nr_system", $p_system);
	}else{

		$p_system = $remove_system;
		$session->param("nr_system", $p_system);
	}
}
if ($remove_thosp) {
	if($remove_thosp eq 'BIN_EMPTY'){
		$p_thosp = "";
		$session->param("nr_thosp", $p_thosp);
	}else{
		$p_thosp = $remove_thosp;
		$session->param("nr_thosp", $p_thosp);
	}
}
if ($remove_geoar) {
	if($remove_geoar eq 'BIN_EMPTY'){
		$p_geoarea= "";
		$session->param("nr_geoarea", $p_geoarea);
	}else{
		$p_geoarea= $remove_geoar;
		$session->param("nr_geoarea", $p_geoarea);
	}
}
if ($remove_msa) {
	if($remove_msa eq 'BIN_EMPTY'){
		$p_msa = "";
		$session->param("nr_msa", $p_msa);
	}else{
		$p_msa = $remove_msa;
		$session->param("nr_msa", $p_msa);
	}
}
if ($remove_hosp) {
	if($remove_hosp eq 'BIN_EMPTY'){
		$p_htype = "";
		$session->param("nr_htype", $p_htype);
	}else{
		$p_htype = $remove_hosp;
		$session->param("nr_htype", $p_htype);
	}
}
if ($remove_toc) {
	if($remove_toc eq 'BIN_EMPTY'){
		$p_ctype = "";
		$session->param("nr_ctype", $p_ctype);
	}else{
		$p_ctype = $remove_toc;
		$session->param("nr_ctype", $p_ctype);
	}
}

#to add search criteria
#BEDS
if ($bed_id) {
	my @arrayOfbeds = split(/~/,$bed_id);

		$p_bedstart =@arrayOfbeds[0];
		$p_bedend=@arrayOfbeds[1];

	$session->param("nr_bedstart", $p_bedstart);
	$session->param("nr_bedend", $p_bedend);

}
#FTES
if ($fte_id) {
	my @arrayOfbeds = split(/~/,$fte_id);

		$p_ftestart =@arrayOfbeds[0];
		$p_fteend=@arrayOfbeds[1];
	$session->param("nr_ftestart", $p_ftestart);
	$session->param("nr_fteend", $p_fteend);
}
#Net Income
if ($income_id) {
	my @arrayOfbeds = split(/~/,$income_id);
		$p_incomestart =@arrayOfbeds[0];
		$p_incomeend=@arrayOfbeds[1];
	$session->param("nr_incomestart", $p_incomestart);
	$session->param("nr_incomeend", $p_incomeend);
}
#Geographical Area
if ($geo_id) {
	if($p_geoarea){
		$p_geoarea = $p_geoarea.",".$geo_id;
	}else{
		$p_geoarea = $geo_id;
	}
	$session->param("nr_geoarea", $p_geoarea);
}
#State
if ($st_id) {
	if($p_state){
		$p_state = $p_state.",".$st_id;
	}else{
		$p_state = $st_id;
	}
	$session->param("nr_state", $p_state);
}
#Teaching Hospital
if ($tch_id) {
	if($p_thosp){
		$p_thosp = $p_thosp.",".$tch_id;
	}else{
		$p_thosp = $tch_id;
	}
	$session->param("nr_thosp", $p_thosp);
}
#Type of Control
if ($toc_id) {
	if($p_ctype){
		$p_ctype = $p_ctype.",".$toc_id;
	}else{
		$p_ctype = $toc_id;
	}
	$session->param("nr_ctype", $p_ctype);
}
#Hospital Type
if ($hty_id) {
	if($p_htype){
		$p_htype = $p_htype.",".$hty_id;
	}else{
		$p_htype = $hty_id;
	}
	$session->param("nr_htype", $p_htype);
}
#MSA
if ($msa_id) {
	if($p_msa){
		$p_msa = $p_msa.",".$msa_id;
	}else{
		$p_msa = $msa_id;
	}
	$session->param("nr_msa", $p_msa);
}
#System
if ($sys_id) {
	if($p_system){
		$p_system = $p_system.",".$sys_id;
	}else{
		$p_system = $sys_id;
	}
	$session->param("nr_system", $p_system);
}
#when page executes second time

if($narrow_search == "1"){

	$zip=$session->param("nr_zip");
	$county=$session->param("nr_county");
	$name=$session->param("nr_name");
	$address=$session->param("nr_address");
	$city=$session->param("nr_city");
	$prov=$session->param("nr_provider");
	$p_peergroupname=$session->param("nr_peername");
	$p_state=$session->param("nr_state");
	$p_ctype=$session->param("nr_ctype");
	$p_msa=	$session->param("nr_msa");
	$p_system=$session->param("nr_system");
	$p_htype=$session->param("nr_htype");
	$p_thosp=$session->param("nr_thosp");
	$p_geoarea=$session->param("nr_geoarea");
	$p_bedstart=$session->param("nr_bedstart");
	$p_bedend=$session->param("nr_bedend");
	$p_ftestart=$session->param("nr_ftestart");
	$p_fteend=$session->param("nr_fteend");
	$p_incomestart=$session->param("nr_incomestart");
	$p_incomeend=$session->param("nr_incomeend");
	$p_Zscoremin=$session->param("nr_Zscoremin");
	$p_Zscoremax=$session->param("nr_Zscoremax");
	$p_OprMarginmin=$session->param("nr_OprMarginmin");
	$p_OprMarginmax=$session->param("nr_OprMarginmax");
	$p_Within=$session->param("nr_Within");
	$p_c_Miles_Of_zip=$session->param("nr_p_c_Miles_Of_zip");
	$facility = $session->param("nr_facility");

}

my ( $op, $n);
my @where_list = ();

#To display search criteria in narrow search display box.
$lHeight = "100px";
$tbHeight = 0;
$lstrDisp = "<table class='gridstyle' style='color:white;' cellpadding=2 cellspacing=2 width='80%' border=1>";

if ($p_state){
	$lstrDisp = $lstrDisp."<tr nowrap><td>State(s)</td><td>".$p_state."</td></tr>";
	$lstrStore = $lstrStore.":".$p_state;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
	$lstrStore = $lstrStore.":"."";
}

if($prov){
	$lstrDisp = $lstrDisp."<tr><td nowrap>Provider No.</td><td>".$prov."</td></tr>";
	$lstrStore = $lstrStore .$prov;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}

if ($zip){
	$lstrDisp = $lstrDisp."<tr><td>Zip Code</td><td>".$zip."</td></tr>";
	$lstrStore = $lstrStore.":".$zip;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}

if ($county){
	$lstrDisp = $lstrDisp."<tr><td>County</td><td>".$county."</td></tr>";
	$lstrStore = $lstrStore.":".$county;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}
if ($name){
	$lstrDisp = $lstrDisp."<tr><td>HospName</td><td>".$name."</td></tr>";
	$lstrStore = $lstrStore.":".$name;
	$tbHeight = $tbHeight + 50;
}else{
	$lstrDisp = $lstrDisp."";
}

if ($address){
       $lstrDisp = $lstrDisp."<tr><td>Address </td><td>".$address."</td></tr>";
       $lstrStore = $lstrStore.":".$address;
       $tbHeight = $tbHeight + 20;
}else{
		$lstrDisp = $lstrDisp."";
}

if ($city){
	$lstrDisp = $lstrDisp."<tr><td>City</td><td>".$city."</td></tr>";
	$lstrStore = $lstrStore.":".$city;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}

if ($p_system){
	$lstrDisp = $lstrDisp."<tr><td>System</td><td>".$p_system."</td></tr>";
	$lstrStore = $lstrStore.":".$p_system;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
	$lstrStore = $lstrStore.":"."";
}

if ($p_ctype){
	$lstrDisp = $lstrDisp."<tr><td>Type Of Control</td><td>".$p_ctype."</td></tr>";
	$lstrStore = $lstrStore.":".$p_ctype;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
	$lstrStore = $lstrStore.":"."";
}

if ($p_htype){
	$lstrDisp = $lstrDisp."<tr><td>Hosp Type</td><td>".$p_htype."</td></tr>";
	$lstrStore = $lstrStore.":".$p_htype;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
	$lstrStore = $lstrStore.":"."";
}

if ($p_msa){
	$lstrDisp = $lstrDisp."<tr><td>MSA</td><td>".$p_msa."</td></tr>";
	$lstrStore = $lstrStore.":".$p_msa;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
	$lstrStore = $lstrStore.":"."";
}

if ($p_thosp){
	$lstrDisp = $lstrDisp."<tr><td>Teaching Hospital</td><td>".$p_thosp."</td></tr>";
	$lstrStore = $lstrStore.":".$p_thosp;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
	$lstrStore = $lstrStore.":"."";
}
if ($p_geoarea){
	$lstrDisp = $lstrDisp."<tr><td>Geographical Area</td><td>".$p_geoarea."</td></tr>";
	$lstrStore = $lstrStore.":".$p_geoarea;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
	$lstrStore = $lstrStore.":"."";
}

if ($p_bedstart){
	$lstrDisp = $lstrDisp."<tr><td>Min Beds</td><td>".$p_bedstart."</td></tr>";
	$lstrStore = $lstrStore.":".$p_bedstart;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}

if ($p_bedend){
	$lstrDisp = $lstrDisp."<tr><td>Max Beds</td><td>".$p_bedend."</td></tr>";
	$lstrStore = $lstrStore.":".$p_bedend;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}

if ($p_ftestart){
	$lstrDisp = $lstrDisp."<tr><td>Min FTEs</td><td>".$p_ftestart."</td></tr>";
	$lstrStore = $lstrStore.":".$p_ftestart;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}

if ($p_fteend){
	$lstrDisp = $lstrDisp."<tr><td>Max FTEs</td><td>".$p_fteend."</td></tr>";
	$lstrStore = $lstrStore.":".$p_fteend;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}

if ($p_incomestart){
	$lstrDisp = $lstrDisp."<tr><td>Min Net Income</td><td>".$p_incomestart."</td></tr>";
	$lstrStore = $lstrStore.":".$p_incomestart;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}

if ($p_incomeend){
	$lstrDisp = $lstrDisp."<tr><td>Max Net Income</td><td>".$p_incomeend."</td></tr>";
	$lstrStore = $lstrStore.":".$p_incomeend;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}
if ($p_Within){
	$lstrDisp = $lstrDisp."<tr><td>Distance(in miles)</td><td>".$p_Within."</td></tr>";
	$lstrStore = $lstrStore.":".$p_Within;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}
if ($p_c_Miles_Of_zip){
	$lstrDisp = $lstrDisp."<tr><td>Location</td><td>".$p_c_Miles_Of_zip."</td></tr>";
	$lstrStore = $lstrStore.":".$p_c_Miles_Of_zip;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}

if($p_Zscoremin){
	$lstrDisp = $lstrDisp."<tr><td>Min Zscore</td><td>".$p_Zscoremin."</td></tr>";
	$lstrStore = $lstrStore.":".$p_Zscoremin;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}

if($p_Zscoremax){
 	$lstrDisp = $lstrDisp."<tr><td>Max Zscore</td><td>".$p_Zscoremax."</td></tr>";
 	$lstrStore = $lstrStore.":".$p_Zscoremax;
 	$tbHeight = $tbHeight + 20;
}else{
 	$lstrDisp = $lstrDisp."";
}

if($p_OprMarginmin){
	$lstrDisp = $lstrDisp."<tr><td>Min Operating Margin</td><td>".$p_OprMarginmin."</td></tr>";
	$lstrStore = $lstrStore.":".$p_OprMarginmin;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}

if($p_OprMarginmax){
	$lstrDisp = $lstrDisp."<tr><td>Max Operating Margin</td><td>".$p_OprMarginmax."</td></tr>";
	$lstrStore = $lstrStore.":".$p_OprMarginmax;
	$tbHeight = $tbHeight + 20;
}else{
	$lstrDisp = $lstrDisp."";
}

if($tbHeight<100){$lHeight=$tbHeight."px";}

$lstrDisp = $lstrDisp."</table>";
print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>CHOOSER</TITLE>
<link href="/css/peer_group/peersearch.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/JS/AjaxScripts/prototype.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/effects.js"></script>
 <script type="text/javascript" src="/JS/AjaxScripts/controls.js"></script>
 <script type="text/javascript" src="/JS/tablesort.js"></script>
 <script type="text/javascript" src="/JS/peer_group/peersearch.js"></script>
 <SCRIPT SRC="/JS/sorttable.js"></SCRIPT>
</HEAD>
<BODY onload="setArrow();">
<CENTER>
<FORM NAME="peersearch" Method="post" action="" >
<input type='hidden' name='facility' value=\"$facility\">
<div id='bodyDiv' style="position:absolute;z-index:100;display:none;background-image:url(/images/background-trans.png);height:96%;width:96%;"><div align='center'  id="displaybox" style="display: none;"></div></div>
};
&innerHeader();
print qq{
<!--content start-->

    <table cellpadding="0" cellspacing="0" width="885px">
        <tr>
            <td valign="top">
                <img src="/images/content_left.gif" /></td>
            <td valign="top" style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;   text-align: left; padding: 10px">};

			if($narrow_search == "0" && $narrow_search_module eq "peer"){
					my $gsql = qq{
						   select * from peergroup_main as pm where pm.PeerGroup_ID = ? and facility_type = $facility_id
						};
					unless ($sth = $dbh->prepare($gsql)) {
						   &display_error("ERROR preparing SQL query:", $sth->errstr);
						   $dbh->disconnect;
						   exit(1);
						}

					unless ($sth->execute($peermainId)) {
						   &display_error("ERROR executing SQL query:", $sth->errstr);
						   $dbh->disconnect;
						   exit(1);
						}

					while ($row = $sth->fetchrow_hashref) {
						$zip=$$row{c_Provider_zip};
						$county=$$row{c_Provider_country};
						$name=$$row{c_Provider_Name};
						$address=$$row{c_Provider_Address};
						$city=$$row{c_Provider_City};
						$prov=$$row{c_Provider_ID};
						$p_peergroupname=$$row{Name};
						$p_state=$$row{c_Provider_States};
						$p_ctype=$$row{c_Control};
						$p_msa=$$row{c_MSA};
						$p_system=$$row{c_System};
						$p_htype=$$row{c_Hospital_Type};
						$p_thosp=$$row{C_Teaching_Hospital};
						$p_geoarea=$$row{C_Geographical_Area};
						$p_bedstart=$$row{C_Bed_Start};
						$p_bedend=$$row{c_Bed_End};
						$p_ftestart=$$row{c_FTE_Start};
						$p_fteend=$$row{c_FTE_End};
						$p_incomestart=$$row{c_Income_Start};
						$p_incomeend=$$row{c_Income_End};
						$p_Zscoremin=$$row{c_Zscore_min};
						$p_Zscoremax=$$row{c_Zscore_max};
						$p_OprMarginmin=$$row{c_OprMargin_min};
						$p_OprMarginmax=$$row{c_OprMargin_max};
						$p_Within=$$row{c_Within};
						$p_c_Miles_Of_zip=$$row{c_Miles_Of_zip};

						$session->param("narrow_search", "1");
						$session->param("nr_zip", $zip);
						$session->param("nr_county", $county);
						$session->param("nr_name", $name);
						$session->param("nr_address", $address);
						$session->param("nr_city", $city);
						$session->param("nr_provider", $prov);
						$session->param("nr_peername", $p_peergroupname);
						$session->param("nr_state", $p_state);
						$session->param("nr_ctype", $p_ctype);
						$session->param("nr_msa", $p_msa);
						$session->param("nr_system", $p_system);
						$session->param("nr_htype", $p_htype);
						$session->param("nr_thosp", $p_thosp);
						$session->param("nr_geoarea", $p_geoarea);
						$session->param("nr_bedstart", $p_bedstart);
						$session->param("nr_bedend", $p_bedend);
						$session->param("nr_ftestart", $p_ftestart);
						$session->param("nr_fteend", $p_fteend);
						$session->param("nr_incomestart", $p_incomestart);
						$session->param("nr_incomeend", $p_incomeend);
						$session->param("nr_Zscoremin", $p_Zscoremin);
						$session->param("nr_Zscoremax", $p_Zscoremax);
						$session->param("nr_OprMarginmin", $p_OprMarginmin);
						$session->param("nr_OprMarginmax", $p_OprMarginmax);
						$session->param("nr_Within", $p_Within);
						$session->param("nr_p_c_Miles_Of_zip", $p_c_Miles_Of_zip);
						$session->param("nr_facility", $facility);

					}
				}
			print qq{
			<table WIDTH="100%" >
			};
			if($narrow_search_module eq "peer"){
				print "<TR><TH align='left'>Peer Group Name: $p_peergroupname</TH>";
				print "<th align='right'><div align='right' id=\"facilityCount\" ></th></TR>";
			}
			if($narrow_search_module eq "search"){
				print "<TR><th align='left'><span class=\"button\"><input type=button name='create' value='Save' onClick=\"return clicker()\"></span> &nbsp;&nbsp; <a href='peersearch_xls.pl'>Download Results To Excel</a></th><th align='right'><div align='right' id=\"facilityCount\" ></th></TR>";
			}
			if($narrow_search_module eq "state"){
				print "<tr><th align='right'><div align='right' id=\"facilityCount\" ></th></TR>";
			}
			print qq{
			</table>
			<TABLE BORDER="0" align="right" CELLPADDING="2" WIDTH="100%" class="gridstyle">

			<tr valign="top">
			<td WIDTH="30%">

				<table width="100%">

				<TR class="gridheader" >
				<TH align="left">Searched Criteria</TH>
				</TR>
				};

				my @sql_args=();
				if ($prov == 0) {
					print "   <TR>\n";
					#print "      <TD ALIGN=\"left\">NO</TD>\n";
					print "   </TR>\n";
				}else{
					$n = @where_list;
					$op = ($n) ? 'AND' : 'WHERE';
					push @where_list, "$op provider_no = ?\n";
					push @sql_args, $prov;
					print "   <TR>\n";
					print "  <TD ALIGN=\"left\"><b>Provider Number</b></br><input type='checkbox' name='src_provider' value='$prov'><a href=\"#\" onClick=\"\"> $prov</a></br> </TD>\n";
					print "   </TR>\n";

				}
				if ($name eq "") {
				}else{
					$n = @where_list;
					$op = ($n) ? 'AND' : 'WHERE';
					push @where_list, "$op name LIKE '$name%' \n";
					print "   <TR>\n";
					print "  <TD ALIGN=\"left\"><b>Provider Name</b></br><input type='checkbox' name='src_provname' value='$name'><a href=\"#\" onClick=\"\"> $name</a></br> </TD>\n";
					print "   </TR>\n";
				}

				if ($address eq "") {
				}else{
				  $n = @where_list;
				  $op = ($n) ? 'AND' : 'WHERE';
				  push @where_list, "$op address LIKE '$address%'\n";

				  print "   <TR>\n";
				  print "  <TD ALIGN=\"left\"><b>Address</b></br><input type='checkbox' name='src_address' value='$address'><a href=\"#\" onClick=\"\"> $address</a></br> </TD>\n";
				  print "   </TR>\n";
				}

			   if ($city eq "") {
			   }else{
					$n = @where_list;
					$op = ($n) ? 'AND' : 'WHERE';
					push @where_list, "$op city LIKE '$city%'\n";
					print "   <TR>\n";
					print "  <TD ALIGN=\"left\"><b>City</b></br><input type='checkbox' name='src_city' value='$city'><a href=\"#\" onClick=\"\"> $city</a></br> </TD>\n";
					print "   </TR>\n";
			    }

				if ($zip eq "") {
				}else{
					$n = @where_list;
					$op = ($n) ? 'AND' : 'WHERE';
					push @where_list, "$op zipcode = ?\n";
					push @sql_args, $zip;
					print "   <TR>\n";
					print "  <TD ALIGN=\"left\"><b>Zip Code</b></br><input type='checkbox' name='src_zip' value='$zip'><a href=\"#\" onClick=\"\"> $zip</a></br> </TD>\n";
					print "   </TR>\n";
			    }

				if ($county eq "") {
				}else{
				  $n = @where_list;
				  $op = ($n) ? 'AND' : 'WHERE';
				  push @where_list, "$op county LIKE '$county%'\n";
				  print "   <TR>\n";
				  print "  <TD ALIGN=\"left\"><b>County</b></br><input type='checkbox' name='src_county' value='$county'><a href=\"#\" onClick=\"\"> $county</a></br> </TD>\n";
				  print "   </TR>\n";
				}


#State display  in searched criteria
				if($p_state eq "")
				{
					print "   <TR>\n";
					#print "      <TD ALIGN=\"left\">NO</TD>\n";
					print "   </TR>\n";
				}
				else
				{

					$n = @where_list;
					$op = ($n) ? 'AND' : 'WHERE';
					push @where_list, "$op state IN(".$p_state.")\n";
					my $stsql="select * from states as st where st.state in (".$p_state.")";
					unless ($sth = $dbh->prepare($stsql)) {
					&display_error("ERROR preparing SQL query:", $sth->errstr);
					$dbh->disconnect;
					exit(1);
				}

			unless ($sth->execute()) {
				   &display_error("ERROR executing SQL query:", $sth->errstr);
				   $dbh->disconnect;
				   exit(1);
				}
				my ($statename,$st_state);
				print "   <TR>\n";
				print qq{
					<TD ALIGN=\"left\"><b>State</b></br>
					};
				while ($row = $sth->fetchrow_hashref) {
					$statename=$$row{state_name};
					$st_state=$$row{state};
					print qq{<input type='checkbox' name='src_state' value='$st_state'><a href=\"#\" onClick=\"\"> $statename</a></br>};
				}

					print "   </TD>\n";
					print "   </TR>\n";
				}

#Beds display  in searched criteria

					if($p_bedstart == 0)
					{
						print "   <TR>\n";
						#print "      <TD ALIGN=\"left\">NO</TD>\n";
						print "   </TR>\n";
					}
					else
					{
						$n = @where_list;
						$op = ($n) ? 'AND' : 'WHERE';
						push @where_list, "$op beds >= (select 0 + ? )\n";
						push @sql_args,$p_bedstart;
							print "   <TR>\n";
						print "  <TD ALIGN=\"left\"><b>Min Beds</b></br><input type='checkbox' name='src_bedstart' value='$p_bedstart'><a href=\"#\" onClick=\"\"> $p_bedstart</a></br> </TD>\n";
						print "   </TR>\n";
					}
					if($p_bedend == 0)
					{
						print "   <TR>\n";
						#print "      <TD ALIGN=\"left\">NO</TD>\n";
						print "   </TR>\n";
					}
					else
					{
						$n = @where_list;
						$op = ($n) ? 'AND' : 'WHERE';
						push @where_list, "$op beds <= (select 0 + ? )\n";
						push @sql_args,$p_bedend ;
							print "   <TR>\n";
						print "  <TD ALIGN=\"left\"><b>Max Beds</b></br><input type='checkbox' name='src_bedend' value='$p_bedend'><a href=\"#\" onClick=\"\"> $p_bedend</a></br> </TD>\n";
						print "   </TR>\n";
					}

#FTEs display  in searched criteria

					if($p_ftestart == 0)
					{

						print "   <TR>\n";
						#print "      <TD ALIGN=\"left\">No".$row."</TD>\n";
						print "   </TR>\n";
					}
					else
					{
						$n = @where_list;
						$op = ($n) ? 'AND' : 'WHERE';
						push @where_list, "$op ftes >= (select 0 + ? )\n";
						push @sql_args,$p_ftestart;
							print "   <TR>\n";
						print "  <TD ALIGN=\"left\"><b>Min FTEs</b></br><input type='checkbox' name='src_ftestart' value='$p_ftestart'><a href=\"#\" onClick=\"\"> $p_ftestart</a></br> </TD>\n";
						print "   </TR>\n";
					}
					if($p_fteend == 0)
					{
						print "   <TR>\n";
						#print "      <TD ALIGN=\"left\">NO</TD>\n";
						print "   </TR>\n";
					}
					else
					{
						$n = @where_list;
						$op = ($n) ? 'AND' : 'WHERE';
						push @where_list, "$op ftes <= (select 0 + ? )\n";
						push @sql_args,$p_fteend ;
						print "   <TR>\n";
						print "  <TD ALIGN=\"left\"><b>Max FTEs</b></br><input type='checkbox' name='src_fteend' value='$p_fteend'><a href=\"#\" onClick=\"\"> $p_fteend</a></br> </TD>\n";
						print "   </TR>\n";
					}

#Netincome display  in searched criteria

					if($p_incomestart == 0)
					{
						print "   <TR>\n";
						#print "      <TD ALIGN=\"left\">NO</TD>\n";
						print "   </TR>\n";
					}
					else
					{
						$n = @where_list;
						$op = ($n) ? 'AND' : 'WHERE';
						push @where_list, "$op net_income >= (select 0 + ? )\n";
						push @sql_args,$p_incomestart;
						print "   <TR>\n";
						print "  <TD ALIGN=\"left\"><b>Min Net Income</b></br><input type='checkbox' name='src_incomestart' value='$p_incomestart'><a href=\"#\" onClick=\"\">$p_incomestart</a></br> </TD>\n";
						print "   </TR>\n";
					}
					if($p_incomeend== 0)
					{
						print "   <TR>\n";
						#print "      <TD ALIGN=\"left\">NO</TD>\n";
						print "   </TR>\n";
					}
					else
					{
						$n = @where_list;
						$op = ($n) ? 'AND' : 'WHERE';
						push @where_list, "$op net_income <= (select 0 + ? )\n";
						push @sql_args,$p_incomeend;

						print "   <TR>\n";
						print "  <TD ALIGN=\"left\"><b>Max Net Income</b></br><input type='checkbox' name='src_incomeend' value='$p_incomeend'><a href=\"#\" onClick=\"\"> $p_incomeend</a></br> </TD>\n";
						print "   </TR>\n";
					}

#Z_score display  in searched criteria
					if($p_Zscoremin == 0)
					{
						print "   <TR>\n";
						#print "      <TD ALIGN=\"left\">NO</TD>\n";
						print "   </TR>\n";
					}
					else
					{
						$n = @where_list;
						$op = ($n) ? 'AND' : 'WHERE';
						#push @where_list, "$op p.Z_Score >= (select 0 + ? )\n";
						#push @sql_args,$p_Zscoremin;
						print "   <TR>\n";
						print "  <TD ALIGN=\"left\"><b>Min Z_Score</b></br><input type='hidden' name='src_Zscoremin' value='$p_Zscoremin'><a href=\"#\" onClick=\"\">$p_Zscoremin</a></br> </TD>\n";
						print "   </TR>\n";
					}
					if($p_Zscoremax== 0)
					{
						print "   <TR>\n";
						#print "      <TD ALIGN=\"left\">NO</TD>\n";
						print "   </TR>\n";
					}
					else
					{
						$n = @where_list;
						$op = ($n) ? 'AND' : 'WHERE';
						#push @where_list, "$op p.Z_Score <= (select 0 + ? )\n";
						#push @sql_args,$p_Zscoremax;
						print "   <TR>\n";
						print "  <TD ALIGN=\"left\"><b>Max Z_Score</b></br><input type='hidden' name='src_Zscoremax' value='$p_Zscoremax'><a href=\"#\" onClick=\"\"> $p_Zscoremax</a></br> </TD>\n";
						print "   </TR>\n";
					}

#Operating margin display  in searched criteria
					if($p_OprMarginmin == 0)
					{
						print "   <TR>\n";
						#print "      <TD ALIGN=\"left\">NO</TD>\n";
						print "   </TR>\n";
					}
					else
					{
						$n = @where_list;
						$op = ($n) ? 'AND' : 'WHERE';
						push @where_list, "$op operating_margin >= (select 0 + ? )\n";
						push @sql_args,$p_OprMarginmin;
						print "   <TR>\n";
						print "  <TD ALIGN=\"left\"><b>Min Operating Margin</b></br><input type='hidden' name='src_OprMarginmin' value='$p_OprMarginmin'><a href=\"#\" onClick=\"\">$p_OprMarginmin</a></br> </TD>\n";
						print "   </TR>\n";
					}
					if($p_OprMarginmax== 0)
					{
						print "   <TR>\n";
						#print "      <TD ALIGN=\"left\">NO</TD>\n";
						print "   </TR>\n";
					}
					else
					{
						$n = @where_list;
						$op = ($n) ? 'AND' : 'WHERE';
						push @where_list, "$op operating_margin <= (select 0 + ? )\n";
						push @sql_args,$p_OprMarginmax;
						print "   <TR>\n";
						print "  <TD ALIGN=\"left\"><b>Max Operating Margin</b></br><input type='hidden' name='src_OprMarginmax' value='$p_OprMarginmax'><a href=\"#\" onClick=\"\"> $p_OprMarginmax</a></br> </TD>\n";
						print "   </TR>\n";
					}

#Within display  in searched criteria
					if($p_Within == 0)
					{
						print "   <TR>\n";
						#print "      <TD ALIGN=\"left\">NO</TD>\n";
						print "   </TR>\n";
					}
					else
					{

						print "   <TR>\n";
						print "  <TD ALIGN=\"left\"><b>Distance(in miles)</b></br><input type='hidden' name='src_Within' value='$p_Within'><a href=\"#\" onClick=\"\">$p_Within</a></br> </TD>\n";
						print "   </TR>\n";
					}

#Miles of zipcode display  in searched criteria
					if($p_c_Miles_Of_zip== 0)
					{
						print "   <TR>\n";
						#print "      <TD ALIGN=\"left\">NO</TD>\n";
						print "   </TR>\n";
					}
					else
					{
						print "   <TR>\n";
						print "  <TD ALIGN=\"left\"><b>Miles of Zip Code </b></br><input type='hidden' name='src_c_Miles_Of_zip' value='$p_c_Miles_Of_zip'><a href=\"#\" onClick=\"\"> $p_c_Miles_Of_zip</a></br> </TD>\n";
						print "   </TR>\n";
					}

#control type display in searched criteria
					if($p_ctype eq "")
					{
						print "   <TR>\n";
						#print "      <TD ALIGN=\"left\">NO</TD>\n";
						print "   </TR>\n";
					}
					else
					{
						$n = @where_list;
						$op = ($n) ? 'AND' : 'WHERE';
						push @where_list, "$op type_of_control IN ($p_ctype)\n";
						my $ctysql="select * from control_type where Control_Type_id in (".$p_ctype.")";
						unless ($sth = $dbh->prepare($ctysql)) {
					   &display_error("ERROR preparing SQL query:", $sth->errstr);
					   $dbh->disconnect;
					   exit(1);
					}

						unless ($sth->execute()) {
							&display_error("ERROR executing SQL query:", $sth->errstr);
							$dbh->disconnect;
							exit(1);
						}
							print "   <TR>\n";
							print qq{
								<TD ALIGN=\"left\"><b>Control Type</b></br>
								};
							my ($ctypename,$Control_Type_id);
					while ($row = $sth->fetchrow_hashref) {
						$ctypename=$$row{Control_Type_Name};
						$Control_Type_id = $$row{Control_Type_id};
						print qq{ <input type="checkbox" name="src_ctype" value="$Control_Type_id"><a href=\"#\" onClick=\"\"> $ctypename</a></br>};
					}
						print " </TD>\n";
						print "   </TR>\n";
					}

#MSA display  in searched criteria
					my ($msaname,$MSACode);
					if($p_msa == 0||$p_msa eq "")
					{
						print "   <TR>\n";
						#print "      <TD ALIGN=\"left\">NO</TD>\n";
						print "   </TR>\n";
					}
					else
					{

						$n = @where_list;
						$op = ($n) ? 'AND' : 'WHERE';
						push @where_list, "$op msa_code in (".$p_msa.")\n";
						my $ctysql="select * from msa where MSACode in  (".$p_msa.")";
						unless ($sth = $dbh->prepare($ctysql)) {
								&display_error("ERROR preparing SQL query:", $sth->errstr);
								$dbh->disconnect;
								exit(1);
						}

						unless ($sth->execute()) {
							&display_error("ERROR executing SQL query:", $sth->errstr);
							$dbh->disconnect;
							exit(1);
						}
						print "   <TR>\n";
						print qq{
						<TD ALIGN=\"left\"><b>MSA</b></br>
						};

						while ($row = $sth->fetchrow_hashref) {
						$msaname=$$row{MSAName};
						$MSACode=$$row{MSACode};
						print qq{ <input type='checkbox' name='src_msa' value='$MSACode' onclick='msa_store()'><a href=\"#\" onClick=\"\"> $msaname</a></br>};

						}

						print "   </TD>\n";
						print "   </TR>\n";
					}

#Teaching Hospital display  in searched criteria

					if($p_thosp eq "")
					{
						print "   <TR>\n";
						#print "      <TD ALIGN=\"left\">NO</TD>\n";
						print "   </TR>\n";
					}
					else
					{

						$n = @where_list;
						$op = ($n) ? 'AND' : 'WHERE';
						if($narrow_search == "1" ){
						push @where_list, "$op teach in ('".$p_thosp."')\n";
								}elsif($narrow_search == "0"){
						push @where_list, "$op teach ='$p_thosp'\n";

						}

						print "   <TR>\n";
						print qq{
						<TD ALIGN=\"left\"><b>Teaching Hospital</b></br>
						};

						print qq{<input type='radio' name='src_thosp' value='$p_thosp'> <a href=\"#\" onClick=\"\"> $p_thosp</a></br>};

						print "   <TR>\n";
						print "     </TD>\n";
						print "   </TR>\n";
					}

#Geographical Area display  in searched criteria

					if($p_geoarea eq "")
					{
						print "   <TR>\n";
						#print "      <TD ALIGN=\"left\">NO</TD>\n";
						print "   </TR>\n";
					}
					else
					{

						$n = @where_list;
						$op = ($n) ? 'AND' : 'WHERE';
						push @where_list, "$op geographic IN ('".$p_geoarea."')\n";

						print "   <TR>\n";
						 print qq{
						 <TD ALIGN=\"left\"><b>Geographical Area</b></br>
						 };
						print qq{<input type='radio' name='src_geoarea' value='$p_geoarea'> <a href=\"#\" onClick=\"\"> $p_geoarea</a></br>};
						print "      </TD>\n";
						print "   </TR>\n";
					}

#System display  in searched criteria
					if($p_system eq "")
					{
						print "   <TR>\n";
						#print "      <TD ALIGN=\"left\">NO</TD>\n";
						print "   </TR>\n";
					}
					else
					{
						if(substr($p_system,1,4) eq "none"){
						$n = @where_list;
						$op = ($n) ? 'AND' : 'WHERE';
						push @where_list, "$op system_id IS NULL\n";
						}else{

						$n = @where_list;
						$op = ($n) ? 'AND' : 'WHERE';
						push @where_list, "$op system_id IN (".$p_system.") \n";

						}
						my $sysql="select SystemID as SYSTEM_ID,SystemName as SYSTEM_NAME from tblsystemmaster where SystemID in (".$p_system.")";
						unless ($sth = $dbh->prepare($sysql)) {
					   &display_error("ERROR preparing SQL query:", $sth->errstr);
					   $dbh->disconnect;
					   exit(1);
						}

						unless ($sth->execute()) {
					   &display_error("ERROR executing SQL query:", $sth->errstr);
					   $dbh->disconnect;
					   exit(1);
						}
						print "   <TR>\n";
						print qq{
						<TD ALIGN=\"left\"><b>System</b></br>
						};
						my ($systname,$SYSTEM_ID);
						if(substr($p_system,1,4) eq "none"){
							print qq{<input type='checkbox' name='src_system' value='$SYSTEM_ID'> <a href=\"#\" onClick=\"\">none</a></br>};
						}else{
						while ($row = $sth->fetchrow_hashref) {
						$systname=$$row{SYSTEM_NAME};
						$SYSTEM_ID=$$row{SYSTEM_ID};
						print qq{<input type='checkbox' name='src_system' value='$SYSTEM_ID'> <a href=\"#\" onClick=\"\"> $systname</a></br>};
						}
						}

						print "     </TD>\n";
						print "   </TR>\n";
					}

#Hospital Type display  in searched criteria

					if($p_htype eq "")
					{
						print "   <TR>\n";
						#print "      <TD ALIGN=\"left\">NO</TD>\n";
						print "   </TR>\n";
					}
					else
					{
						$n = @where_list;
						$op = ($n) ? 'AND' : 'WHERE';
						push @where_list, "$op facility_type IN (".$p_htype.")\n";
						my $htsql="select * from hospital_type where Hospital_Type_id in(".$p_htype.")";

						unless ($sth = $dbh->prepare($htsql)) {
						&display_error("ERROR preparing SQL query:", $sth->errstr);
						$dbh->disconnect;
						exit(1);
						}

						unless ($sth->execute()) {
						&display_error("ERROR executing SQL query:", $sth->errstr);
						$dbh->disconnect;
						exit(1);
						}
						my ($hname,$Hospital_Type_id);
						print "   <TR>\n";
						print qq{
						<TD ALIGN=\"left\"><b>Hospital Type</b></br>
						};
						while ($row = $sth->fetchrow_hashref) {
						$hname=$$row{Hospital_Type_Name};
						$Hospital_Type_id=$$row{Hospital_Type_id};
						print qq{<input type='checkbox' name='src_htype' value='$Hospital_Type_id'><a href=\"#\" onClick=\"\">$hname</a></br>};
						}

						print "     </TD>\n";
						print "   </TR>\n";
					}

		print qq{
				  <TR><td><input type='button' value='Remove' onClick='callRemove()'></td></TR>
				};


		my $temp = @where_list;
		my $tempWhere = ($temp) ? 'AND' : 'WHERE';

#display of Narrow your search
#Type of control
		print qq{
		</table>
		<table width="100%">
		<TR class="gridheader">
		<TH align="left">Narrow Your Search</TH>
		</TR>
		};

		my $c_sql = qq{select control_name, count(distinct provider_no) as ctrl_count,type_of_control
		from $view
		@where_list
		$tempWhere
		control_name is not null
		group by control_name
		};

		unless ($sth = $dbh->prepare($c_sql)) {
			   &display_error("ERROR preparing SQL query:", $sth->errstr);
			   $dbh->disconnect;
			   exit(1);
			}

		unless ($sth->execute(@sql_args)) {
			   &display_error("ERROR executing SQL query:", $sth->errstr);
			   $dbh->disconnect;
			   exit(1);
			}

		my $rows =  $sth->rows;
		if($rows > 0){
			print qq{
		<TR>
		<td><input type="button" value="Search" onclick="peer_submit()"></td>
		</TR>
		<TR>
		<td><b>Type of Control</b></td>
		</TR>
		};
		}
		my($ctrl_name,$ctrl_count,$cntrl_id);
			while (($ctrl_name,$ctrl_count,$cntrl_id) = $sth->fetchrow_array) {

				print "   <TR>\n";
				print "      <TD ALIGN=\"left\"><input type=\"checkbox\"  name=\"cntrl_type_id\" value=\"$cntrl_id\"><a href=\"#\" onClick=\"\">$ctrl_name ($ctrl_count)</a></TD>\n";
				print "   </TR>\n";
			}

#Hospital Type

	 my $hsql = "select hospital_name, count(distinct provider_no) as htype_count,hospital_id as htype_id
					from ".$view."
					@where_list
					".$tempWhere."
					hospital_name is not null
					group by hospital_name";

		unless ($sth = $dbh->prepare($hsql)) {
			   &display_error("ERROR preparing SQL query:", $sth->errstr);
			   $dbh->disconnect;
			   exit(1);
			}

		unless ($sth->execute(@sql_args)) {
			   &display_error("ERROR executing SQL query:", $sth->errstr);
			   $dbh->disconnect;
			   exit(1);
			}

			$rows =  $sth->rows;
		$s=0;
		my($htypename,$htype_count,$htype_id);
		while (($htypename,$htype_count,$htype_id) = $sth->fetchrow_array) {
				my $tmp = $htypename;
				$htypename =~s/ /%/g;	#to remove spaces
					if($htypename ne "" ){
					if($s == 0){
					$s = 1;
					print qq{
					<tr>
					<td><b>Hospital Type</b></td>
					</tr>
				};
				}
				print "   <TR>\n";
				print "      <TD ALIGN=\"left\"><input type='checkbox' name='hosp_type_id' value='$htype_id' ><a href=\"\" >$tmp ($htype_count)</a></TD>\n";
				print "   </TR>\n";
				}
		}

#Beds
	my $bed_sql="";#&& $narrow_search == "0"
	$bed_sql = "select CAST(beds AS SIGNED), count(distinct provider_no) as bed_count
				from ".$view."
				@where_list
				group by beds
				order by beds";

		unless ($sth = $dbh->prepare($bed_sql)) {
			   &display_error("ERROR preparing SQL query:", $sth->errstr);
			   $dbh->disconnect;
			   exit(1);
			}

		unless ($sth->execute(@sql_args)) {
			   &display_error("ERROR executing SQL query:", $sth->errstr);
			   $dbh->disconnect;
			   exit(1);
			}
			$rows =  $sth->rows;

		my($beds,$bed_count);
		my $bed_upper_limit = 100;
		my $bed_total = 0;
		my $bed_lower_limit = 1;
		$s=0;
		while (($beds,$bed_count) = $sth->fetchrow_array) {
			if($beds >= $bed_upper_limit)
				{
				if($s == 0){
				print qq{
			<tr>
		<td><b>BED</b></td>
		</tr>
			};
				$s=1;
				}
					if($bed_total!=0 && $bed_total>=1 && $p_bedstart == 0){
					print "   <TR>\n";
					print "      <TD ALIGN=\"left\"><input type='checkbox' name='beds_id' value='$bed_lower_limit' ><input type='hidden' name='beds_val' value='$bed_upper_limit'><a href=\"\" >$bed_lower_limit - $bed_upper_limit ($bed_total)</a></TD>\n";
					print "   </TR>\n";
					}
					$bed_lower_limit = $bed_upper_limit + 1;
					$bed_upper_limit = $bed_upper_limit + 100;
					$bed_total = 0;
						while($beds >= $bed_upper_limit)
						{
							$bed_upper_limit = $bed_upper_limit + 100;
						}
					$bed_total = $bed_count;
				}
				else
				{
					$bed_total = $bed_total+$bed_count;
				}
			}
			if(!$p_bedstart && $bed_upper_limit != 0 && $beds != 0){
			if($s == 0){
				print qq{
			<tr>
		<td><b>BED</b></td>
		</tr>
			};
				$s=1;
				}
			print "   <TR>\n";
			print "      <TD ALIGN=\"left\"><input type='checkbox' name='beds_id' value='$bed_lower_limit' ><input type='hidden' name='beds_val' value='$bed_upper_limit'><a href=\"\" >$bed_lower_limit - $bed_upper_limit ($bed_total)</a></TD>\n";
			print "   </TR>\n";
				}elsif($p_bedstart && $p_bedend){
					my $row;
				$bed_sql = "select count(distinct provider_no) as bed_count
				from ".$view."
				@where_list";

		unless ($sth = $dbh->prepare($bed_sql)) {
			   &display_error("ERROR preparing SQL query:", $sth->errstr);
			   $dbh->disconnect;
			   exit(1);
			}

		unless ($sth->execute(@sql_args)) {
			   &display_error("ERROR executing SQL query:", $sth->errstr);
			   $dbh->disconnect;
			   exit(1);
			}
			while ($row = $sth->fetchrow_hashref) {
			$bed_count = $$row{bed_count};
			}
		if($s == 0){
				print qq{
			<tr>
		<td><b>BED</b></td>
		</tr>
			};
				$s=1;

				}

				print "   <TR>\n";
			print "      <TD ALIGN=\"left\"><input type='checkbox' name='beds_id' value='$p_bedstart' ><input type='hidden' name='beds_val' value='$p_bedend'><a href=\"\" >$p_bedstart - $p_bedend($bed_count)</a></TD>\n";
			print "   </TR>\n";
		}
##FTES
#			my $fte_sql="";
#			$fte_sql =
#			  "select CAST(ftes AS SIGNED), count(distinct provider_no) as fte_count
#				from ".$view."
#				@where_list
#				group by ftes order by ftes";
#
#			unless ($sth = $dbh->prepare($fte_sql)) {
#			   &display_error("ERROR preparing SQL query:", $sth->errstr);
#			   $dbh->disconnect;
#			   exit(1);
#			}
#
#			unless ($sth->execute(@sql_args)) {
#			   &display_error("ERROR executing SQL query:", $sth->errstr);
#			   $dbh->disconnect;
#			   exit(1);
#			}
#
#			$rows =  $sth->rows;
#
#		my($ftes,$fte_count);
#		my $fte_upper_limit = 100;
#		my $fte_total = 0;
#		my $fte_lower_limit = 1;
#		$s=0;
#			while (($ftes,$fte_count) = $sth->fetchrow_array) {
#				if($ftes >= $fte_upper_limit)
#				{
#					if($s == 0){
#						print qq{
#								<tr>
#								<td><b>FTE</b></td>
#								</tr>
#								};
#								$s=1;
#					}
#
#					if($fte_total!=0 && $fte_total>=1 && $p_ftestart == 0){
#
#					print "   <TR>\n";
#					print "      <TD ALIGN=\"left\"><input type='checkbox' name='ftes_id' value='$fte_lower_limit' ><input type='hidden' name='ftes_val' value='$fte_upper_limit'><a href=\"\" >$fte_lower_limit - $fte_upper_limit ($fte_total)</a></TD>\n";
#					print "   </TR>\n";
#					}
#					$fte_lower_limit = $fte_upper_limit + 1;
#					$fte_upper_limit = $fte_upper_limit + 100;
#					$fte_total = 0;
#						while($ftes >= $fte_upper_limit)
#						{
#							$fte_upper_limit = $fte_upper_limit + 100;
#						}
#					$fte_total = $fte_count;
#				}
#				else
#				{
#					$fte_total = $fte_total+$fte_count;
#				}
#
#			}
#				if(!$p_ftestart && $fte_upper_limit != 0 && $ftes != 0){
#				if($s == 0){
#						print qq{
#								<tr>
#								<td><b>FTE</b></td>
#								</tr>
#								};
#								$s=1;
#					}
#
#			print "   <TR>\n";
#			print "      <TD ALIGN=\"left\"><input type='checkbox' name='ftes_id' value='$fte_lower_limit' ><input type='hidden' name='ftes_val' value='$fte_upper_limit'><a href=\"\" >$fte_lower_limit - $fte_upper_limit ($fte_total)</a></TD>\n";
#			print "   </TR>\n";
#			}	elsif($p_ftestart && $p_fteend){
#			if($s == 0){
#						print qq{
#								<tr>
#								<td><b>FTE</b></td>
#								</tr>
#								};
#								$s=1;
#					}
#			my $row;
#			$fte_sql =
#			  "select count(distinct provider_no) as fte_count
#				from ".$view."
#				@where_list
#				order by ftes";
#
#			unless ($sth = $dbh->prepare($fte_sql)) {
#			   &display_error("ERROR preparing SQL query:", $sth->errstr);
#			   $dbh->disconnect;
#			   exit(1);
#			}
#
#			unless ($sth->execute(@sql_args)) {
#			   &display_error("ERROR executing SQL query:", $sth->errstr);
#			   $dbh->disconnect;
#			   exit(1);
#			}
#			while ($row = $sth->fetchrow_hashref) {
#			$fte_count = $$row{fte_count};
#			}
#			print "   <TR>\n";
#			print "      <TD ALIGN=\"left\"><input type='checkbox' name='ftes_id' value='$p_ftestart' ><input type='hidden' name='ftes_val' value='$p_fteend'><a href=\"\" >$p_ftestart - $p_fteend ($fte_count)</a></TD>\n";
#			print "   </TR>\n";
#
#			}

##Net Income
#		my $income_sql="";
#		$income_sql = "select CAST(net_income AS SIGNED) as income, count(distinct provider_no) as income_count
#						from ".$view."
#						@where_list
#						group by net_income order by net_income";
#
#		unless ($sth = $dbh->prepare($income_sql)) {
#			   &display_error("ERROR preparing SQL query:", $sth->errstr);
#			   $dbh->disconnect;
#			   exit(1);
#			}
#
#		unless ($sth->execute(@sql_args)) {
#			   &display_error("ERROR executing SQL query:", $sth->errstr);
#			   $dbh->disconnect;
#			   exit(1);
#			}
#			$rows =  $sth->rows;
#
#		my($Income,$Income_count);
#		my $Income_upper_limit = 100000;
#		my $Income_total = 0;
#		my $Income_lower_limit = 1;
#		$s=0;
#			while (($Income,$Income_count) = $sth->fetchrow_array) {
#				if($Income >= $Income_upper_limit)
#				{
#					if($s == 0){
#						print qq{
#								<tr>
#								<td><b>Net Income</b></td>
#								</tr>
#								};
#								$s=1;
#					}
#					if($Income_total!=0 && $Income_total >=1 && $p_incomestart == 0){
#					print "   <TR>\n";
#					print "      <TD ALIGN=\"left\"><input type='checkbox' name='Income_id' value='$Income_lower_limit' ><input type='hidden' name='Income_val' value='$Income_upper_limit'><a href=\"\" >$Income_lower_limit - $Income_upper_limit ($Income_total)</a></TD>\n";
#					print "   </TR>\n";
#					}
#					$Income_lower_limit = $Income_upper_limit + 1;
#					$Income_upper_limit = $Income_upper_limit + 100000;
#					$Income_total = 0;
#						while($Income >= $Income_upper_limit)
#						{
#							$Income_upper_limit = $Income_upper_limit + 100000;
#						}
#					$Income_total = $Income_count;
#				}
#				else
#				{
#					$Income_total = $Income_total+$Income_count;
#				}
#			}
#
#			if(!$p_incomestart && $Income_upper_limit!=0 && $Income != 0){
#			if($s == 0){
#						print qq{
#								<tr>
#								<td><b>Net Income</b></td>
#								</tr>
#								};
#								$s=1;
#					}
#			print "   <TR>\n";
#			print "      <TD ALIGN=\"left\"><input type='checkbox' name='Income_id' value='$Income_lower_limit' ><input type='hidden' name='Income_val' value='$Income_upper_limit'><a href=\"\" >$Income_lower_limit - $Income_upper_limit ($Income_total)</a></TD>\n";
#			print "   </TR>\n";
#			}elsif($p_incomestart && $p_incomeend){
#			my $row;
#			$income_sql = "select CAST(net_income AS SIGNED) as income, count(distinct provider_no) as income_count
#						from ".$view."
#						@where_list
#						order by net_income";
#
#		unless ($sth = $dbh->prepare($income_sql)) {
#			   &display_error("ERROR preparing SQL query:", $sth->errstr);
#			   $dbh->disconnect;
#			   exit(1);
#			}
#
#		unless ($sth->execute(@sql_args)) {
#			   &display_error("ERROR executing SQL query:", $sth->errstr);
#			   $dbh->disconnect;
#			   exit(1);
#			}
#			while ($row = $sth->fetchrow_hashref) {
#			$Income_count = $$row{income_count};
#			}
#			if($s == 0){
#						print qq{
#								<tr>
#								<td><b>Net Income</b></td>
#								</tr>
#								};
#								$s=1;
#					}
#			print "   <TR>\n";
#			print "      <TD ALIGN=\"left\"><input type='checkbox' name='Income_id' value='$p_incomestart' ><input type='hidden' name='Income_val' value='$p_incomeend'><a href=\"\" >$p_incomestart - $p_incomeend ($Income_count)</a></TD>\n";
#			print "   </TR>\n";
#
#			}
#MSA type
		my $msql="";
		$msql = "select msa_name , count(distinct provider_no),msa_code
		from $view
		@where_list
		".$tempWhere."
		msa_name is not null
		group by msa_name";

		unless ($sth = $dbh->prepare($msql)) {
			   &display_error("ERROR preparing SQL query:", $sth->errstr);
			   $dbh->disconnect;
			   exit(1);
		}

		unless ($sth->execute(@sql_args)) {
			   &display_error("ERROR executing SQL query:", $sth->errstr);
			   $dbh->disconnect;
			   exit(1);
		}

		$rows =  $sth->rows;
		if($rows > 0){
		print qq{
				<tr>
				<td><b>MSA</b></td>
				</tr>
			};
		}

		my($MSAName,$msa_count,$msa_code,@msa_code);
			while (($MSAName,$msa_count,$msa_code) = $sth->fetchrow_array) {
				print "   <TR>\n";
				print "      <TD ALIGN=\"left\"><input type='checkbox' name='msa_type' value='$msa_code'><a href=\"#\" onClick=\"\">$MSAName ($msa_count)</a></TD>\n";
				print "   </TR>\n";
			}
		# System

		my $systemsql = "select system_name, count(distinct provider_no) as sys_count,system_id
		from ".$view."
		@where_list
		".$tempWhere."
		system_name is not null
		group by system_name";

		unless ($sth = $dbh->prepare($systemsql)) {
			   &display_error("ERROR preparing SQL query:", $sth->errstr);
			   $dbh->disconnect;
			   exit(1);
			}

		unless ($sth->execute(@sql_args)) {
			   &display_error("ERROR executing SQL query:", $sth->errstr);
			   $dbh->disconnect;
			   exit(1);
			}
		$rows =  $sth->rows;
		if($rows > 0){
		print qq{
				<tr>
				<td><b>System</b></td>
				</tr>
			};
		}
		my($sysname,$sys_count,$sys_sysid);
		while (($sysname,$sys_count,$sys_sysid) = $sth->fetchrow_array) {
			print "   <TR>\n";
			print "      <TD ALIGN=\"left\"><input type='checkbox' name='system_type' value='$sys_sysid'><a href=\"#\" onClick=\"\">$sysname ($sys_count)</a></TD>\n";
			print "   </TR>\n";
		}

#Teaching Hospital
		my $tsql = "select teach as tname,count(distinct provider_no) as count_tname
					from ".$view."
					@where_list
					".$tempWhere."
					teach is not null
					group by teach";

		unless ($sth = $dbh->prepare($tsql)) {
			   &display_error("ERROR preparing SQL query:", $sth->errstr);
			   $dbh->disconnect;
			   exit(1);
			}

		unless ($sth->execute(@sql_args)) {
			   &display_error("ERROR executing SQL query:", $sth->errstr);
			   $dbh->disconnect;
			   exit(1);
			}
		$rows =  $sth->rows;
		$s=0;
		my($tname,$count_tname,$teach_value);
		while (($tname,$count_tname) = $sth->fetchrow_array) {
					if($tname ne "" ){
					if($s == 0){
					$s=1;
					print qq{
					<tr>
					<td><b>Teaching Hospital</b></td>
					</tr>
				};
				}
					print "   <TR>\n";
					print "      <TD ALIGN=\"left\"><input type='radio' name='Teach_type' value='$tname'><a href=\"#\" onClick=\"\">$tname ($count_tname)</a></TD>\n";
					print "   </TR>\n";

				}
}
#State
			my $statesql = "select state_name,count(distinct provider_no) as count_statename,state
			from ".$view."
			@where_list
			".$tempWhere."
			state_name is not null
			group by state_name";

		unless ($sth = $dbh->prepare($statesql)) {
		   &display_error("ERROR preparing SQL query:", $sth->errstr);
		   $dbh->disconnect;
		   exit(1);
		}

		unless ($sth->execute(@sql_args)) {
		   &display_error("ERROR executing SQL query:", $sth->errstr);
		   $dbh->disconnect;
		   exit(1);
		}
		$rows =  $sth->rows;
		if($rows > 0){
			print qq{
				<tr>
				<td><b>State</b></td>
				</tr>
			};
		}
		my($statename,$count_statename,$state_id );
			while (($statename,$count_statename,$state_id ) = $sth->fetchrow_array) {
			print "   <TR>\n";
			print "      <TD ALIGN=\"left\"><input type='checkbox' name='state_name' value='$state_id '><a href=\"#\" onClick=\"\">$statename ($count_statename)</a></TD>\n";
			print "   </TR>\n";
		}

#Geographical Area
		my $ursql = "select geographic,count(distinct provider_no) as count_urname,msa_code as geo_url
					from ".$view."
					@where_list
					".$tempWhere."
					geographic is not null
					group by geographic";

		unless ($sth = $dbh->prepare($ursql)) {
			   &display_error("ERROR preparing SQL query:", $sth->errstr);
			   $dbh->disconnect;
			   exit(1);
			}

		unless ($sth->execute(@sql_args)) {
			   &display_error("ERROR executing SQL query:", $sth->errstr);
			   $dbh->disconnect;
			   exit(1);
			}

			$rows =  $sth->rows;
			if($rows > 0){
			print qq{
					<tr>
					<td><b>Geographical Area</b></td>
					</tr>
				};
			}
		my($urname,$count_urname,$geo_url);
		while (($urname,$count_urname,$geo_url) = $sth->fetchrow_array) {
				print "   <TR>\n";
				print "      <TD ALIGN=\"left\"><input type='radio' name='Geo_type' value='$urname'><a href=\"#\" onClick=\"\">$urname ($count_urname)</a></TD>\n";
				print "   </TR>\n";
		}
		my ($row,@provider_Ids,@provider_Names,$p_id,$p_no);
		if($narrow_search_module eq "peer"){
		my $f_sql = qq{
						   select PeerGroup_Main_ID as peerid,Provider_number as provno from peergroup_details as pd where pd.PeerGroup_Main_ID = ? and FOI=1
						};

					unless ($sth = $dbh->prepare($f_sql)) {
						   &display_error("ERROR preparing SQL query:", $sth->errstr);
						   $dbh->disconnect;
						   exit(1);
						}

					unless ($sth->execute($peermainId)) {
						   &display_error("ERROR executing SQL query:", $sth->errstr);
						   $dbh->disconnect;
						   exit(1);
						}

			while ($row = $sth->fetchrow_hashref) {
				$p_id=$$row{peerid};
				$p_no=$$row{provno};
			}
			}
		print qq{
				</table>
				</td>
				<td WIDTH="70%">
				<table width="100%">
				<TR class="gridheader">
				};


	$sql = qq{
	SELECT distinct hm.ProviderNo as Provider_number,hm.Name as Name,hm.City as City,hm.State as State from $master_table as hm WHERE  ProviderNo in (select provider_no from $view @where_list)
	order by $orderBy
						};


				unless ($sth = $dbh->prepare($sql)) {
				   &display_error("ERROR preparing SQL query:", $sth->errstr);
				   $dbh->disconnect;
				   exit(1);
				}

				unless ($sth->execute(@sql_args)) {
				   &display_error("ERROR executing SQL query:", $sth->errstr);
				   $dbh->disconnect;
				   exit(1);
				}


				 $sql_l = qq{
				select latitude,longitude from $view @where_list
				};

				unless ($sth_l = $dbh->prepare($sql_l)) {
				   &display_error("ERROR preparing SQL query:", $sth_l->errstr);
				   $dbh->disconnect;
				   exit(1);
				}
				unless ($sth_l->execute(@sql_args)) {
				   &display_error("ERROR executing SQL query:", $sth_l->errstr);
				   $dbh->disconnect;
				   exit(1);
				}

				my $rows_returned = $sth->rows();
				#open (J, ">data.txt");
				#print J "SQL: $sql\n";
				#print J "ROWS RETURNED: $rows_returned\n";
				#close J;
				$session->param("peersearch_sql", $sql);
				$session->param("peersearch_sql_args", join(',,', @sql_args));

				print "<input type=\"hidden\" name=\"facility_Count\" value=\"$rows_returned\"  >";
				print "<script>setFacilityCount('$rows_returned');</script>";
				unless ($rows_returned) {
				&display_error(" Sorry, no matches were found. Please change the search criteria and try again.");
				$dbh->disconnect();
				return(0);
				}

				print qq{
					<td align="left"><a href='#' onclick=\"callSort('Provider_number',1,'$facility')\">Provider #<span id='pnumber' class='arrow'></span></a></TH><TH align="left"><a href='#' onclick="callSort('Name',1,'$facility')">Provider Name<span id='pname'></span></a></TH><TH align="left"><a href='#' onclick=\"callSort('City',1,'$facility')\">City<span id='pcity'></span></a></TH><TH align="left"><a href='#'>FOI</a></TH><TH align="left"><a href='#' onclick=\"callSort('State',1,'$facility')\"> State<span id='pstate'></span></a></TH><TH align="left"><input type=\"checkbox\" name=\"Select\" onclick="CheckUncheck(this)"></TH>
					</TR>
				};
				my $count = 0;
				my($name,$number,$class,$city,$states);
				my ($cr_link, $drg_link, $src_link,$profile_link ,$chart_link,$lat, $lon,$maplink);

					while ($row = $sth_l->fetchrow_hashref) {
					$lat=$$row{latitude};
					$lon=$$row{longitude};
					}

				while ($row = $sth->fetchrow_hashref) {
					$number=$$row{Provider_number};
					$name=$$row{Name};
					$city=$$row{City};
					$states=$$row{State};

						for($i=0;$i<scalar(@search_elements);$i++){
					if(@search_elements[$i] eq 'Cost Reports'){
					$cr_link  = "<tr bgcolor=\"#B9D3EE\"><td style=\"border-bottom:0px solid #FFFFFF;\" nowrap><img src=\"/images/costreport_icon.gif\" /></td><td style=\"border-bottom:0px solid #FFFFFF;font-family:Verdana;font-size:11px\" nowrap><A HREF=\"/cgi-bin/choose1.pl?provider=$number&facility=$facility\" class=\"gridHlink\" style=\"font-weight:bold;\">COST REPORTS</A></td></tr>";
					}
					if(@search_elements[$i] eq 'Drg Mix'){
					$drg_link = "<tr bgcolor=\"#B9D3EE\"><td style=\"border-bottom:0px solid #FFFFFF;\" nowrap><img src=\"/images/drgmix_icon.gif\" /></td><td style=\"border-bottom:0px solid #FFFFFF;font-family:Verdana;font-size:11px\" nowrap><A HREF=\"/cgi-bin/drg_mix.pl?provider=$number&facility=$facility\" TITLE=\"DRG MIX\" class=\"gridHlink\" style=\"font-weight:bold;\">DRG MIX</A></td></tr>";
					}
					if(@search_elements[$i] eq 'Patient Source'){
					$src_link = "<tr bgcolor=\"#B9D3EE\"><td style=\"border-bottom:0px solid #FFFFFF;\" nowrap><img src=\"/images/patientsource_icon.gif\" /></td><td style=\"border-bottom:0px solid #FFFFFF;font-family:Verdana;font-size:11px\" nowrap><A HREF=\"/cgi-bin/derek3.pl?provider=$number&facility=$facility\" TITLE=\"WHERE PATIENTS COME FORM\" class=\"gridHlink\" style=\"font-weight:bold;\">PATIENT SOURCE</A></td></tr>";
					}
					if(@search_elements[$i] eq 'Profile Report'){
					$profile_link = "<tr bgcolor=\"#B9D3EE\"><td style=\"border-bottom:0px solid #FFFFFF;\" nowrap></td><td style=\"border-bottom:0px solid #FFFFFF;font-family:Verdana;font-size:11px\" nowrap><A HREF=\"/cgi-bin/Milo2/profile_report.pl?provider=$number&status=1&facility=$facility\" class=\"gridHlink\" style=\"font-weight:bold;\">PROFILE REPORT</A></td></tr>";
					}
					}

					if ($lat && $lon) {
						$maplink = "<tr><td align=\"center\" colspan=2 style=\"border-bottom:0px solid #FFFFFF;\"><A HREF=\"/cgi-bin/cr_map.pl?lat=$lat&lon=$lon&desc=$name&facility=$facility\"><IMG BORDER=\"0\" SRC=\"/icons/gm_button.gif\"></A></td></tr>";
					} else {
						$maplink = 'No Map';
					}

					$class = (++$count % 2) ? 'gridrow' : 'gridalternate';
					print "   <TR CLASS=\"$class\" onclick=\"showMenu(0,$rows_returned)\">\n";

					print "      <TD ALIGN=\"left\"  VALIGN=\"TOP\" style=\"padding:5px\"><a href=\"#\" onmouseover=\"showMenu($count ,$rows_returned)\" ><B>$number</B></a><input type=\"hidden\" name=\"providerName\" value=\"$name\"><div id=\"$count\" class=\"$class\" align=\"right\" style=\"border:2px solid #36648B;top;100px;position:absolute;visibility:hidden;\"> <font face=\"verdana\"> <table> <tr bgcolor=\"#B9D3EE\"><td colspan=2 align=\"center\" style=\"height:30px;border-bottom:0px solid #FFFFFF;color:#000000;font-family:Helvetica;font-size:12px;font-weight:bold;\" nowrap >Reports</td></tr> <tr bgcolor=\"#B9D3EE\"> <td style=\"border-bottom:0px solid #FFFFFF;\" nowrap></td> <td align=\"left\" colspan=2 style=\"border-bottom:0px solid #FFFFFF;\"><A HREF=\"#\" onclick=\"setFOI($count ,$rows_returned)\" class=\"gridHlink\" style=\"font-weight:bold;\">Mark as FOI</A></td></tr>$profile_link $cr_link $drg_link $src_link $maplink </table></div></TD>\n";

					print "      <TD ALIGN=\"left\" VALIGN=\"TOP\" style=\"padding:5px\">$name</TD>\n";
					print "      <TD ALIGN=\"left\" VALIGN=\"TOP\" style=\"padding:5px\">$city</TD>\n";
					print "	<TD style=\"padding-right:10px\">";
					if($p_no==$number && $p_id==$peermainId){
						print "<img id=\"foi$count\" src=\"/images/FOI.gif\" style=\"\" /></TD>\n";
					}else{
						print "<img id=\"foi$count\" src=\"/images/FOI.gif\" style=\"visibility:hidden;\" /></TD>\n";
					}

					print "      <TD ALIGN=\"left\" VALIGN=\"TOP\" style=\"padding:5px\">$states</TD>\n";
					print "<TD valign=\"center\"><input type=\"checkbox\" VALUE=\"$number\" name=\"SelectedHosps\" style=\"\" ></TD>\n";
					print "   </TR>\n";
				}

			print "   </table>\n";
			print "   </TR>\n";

	print qq{
	</TABLE>
	</td>
	<td valign="top">
	<img src="/images/content_right.gif" /></td>
	</tr>
	</table>
	<H1 align="left"></H1>
	<TR>
	</TR>
	};

	if($narrow_search_module eq "peer"){
		&view("View","From peer group","Narrow Search");
		print "<TR><TD align='center'><input type=button value='Save' onClick='search_save();'></TD></TR>";
	}

	if($narrow_search_module eq "search"){
	&view("View","From search page","Narrow Search");
		print "<TR><TD align='center'><span class=\"button\"><input type=button name='create' value='Save' onClick=\"return clicker()\"></span> &nbsp;&nbsp; <a href='peersearch_xls.pl'>Download Results To Excel</a><br/><br/></TD></TR>";
	}
	#To display footer
	&TDdoc;

	print qq{
		<iframe name=\"peerCheck\" style="visibility:hidden;height:0px;width:0px;"></iframe>
		<input type="hidden" name="peer_id" value="$peermainId" >
		<input type="hidden" name="saction" value="" >
		<input type="hidden" name="prov_Names" value="" >
		<input type="hidden" name="prov_Id" value="" >
		<input type="hidden" name="peer_name" value="$p_peergroupname" >
		<input type="hidden" name="Type_cntrl" >
		<input type="hidden" name="Hosp_type" >
		<input type="hidden" name="Msa_type_code" >
		<input type="hidden" name="Msa_code" >
		<input type="hidden" name="bed_code" >
		<input type="hidden" name="fte_code" >
		<input type="hidden" name="income_code" >
		<input type="hidden" name="System_type_Id" >
		<input type="hidden" name="Teach_type_value" >
		<input type="hidden" name="State_name_id" >
		<input type="hidden" name="geo_type_name" >
		<input type="hidden" name="rm_toc" >
		<input type="hidden" name="rm_hosp">
		<input type="hidden" name="rm_msa">
		<input type="hidden" name="rm_system">
		<input type="hidden" name="rm_thosp">
		<input type="hidden" name="rm_geoar">
		<input type="hidden" name="rm_state">
		<input type="hidden" name="rm_bedstart">
		<input type="hidden" name="rm_bedend">
		<input type="hidden" name="rm_ftestart">
		<input type="hidden" name="rm_fteend">
		<input type="hidden" name="rm_incomestart">
		<input type="hidden" name="rm_incomeend">
		<input type="hidden" name="rm_zscoremin">
		<input type="hidden" name="rm_zscoremax">
		<input type="hidden" name="rm_OprMarginmin">
		<input type="hidden" name="rm_OprMarginmax">
		<input type="hidden" name="rm_prov">
		<input type="hidden" name="rm_name">
		<input type="hidden" name="rm_address">
		<input type="hidden" name="rm_city">
		<input type="hidden" name="rm_zip">
		<input type="hidden" name="rm_county">
		<input type="hidden" name="foi">
		<input type='hidden' name='tmpOrder' value=\"$tmpOrder\">
		<input type='hidden' name='lHeight' value=\"$lHeight\">
		<input type='hidden' name='lstrDisp' value=\"$lstrDisp\">
		<input type='hidden' name='acsdesc' value=\"$acsdesc\">
		<input type="hidden" name="foiName" value="--Not Specified--">
		</form>

		<FORM name='peerFrom' action='/cgi-bin/PeerActions.pl?facility=$facility' method='post'>
		<input type='hidden' name='saction' value=\"\">
		<input type='hidden' name='foiId' value=\"\">
		<input type='hidden' name='facility' value=\"$facility\">
		<input type='hidden' name='peername' value=\"\">
		<input type='hidden' name='desc' value=\"\">
		<input type='hidden' name='strpids' value=\"\">
		<input type='hidden' name='strpnames' value=\"\">
		</FORM>
	};

#---------------------------------------------------------------------------------------------------------------------
sub display_error
{
   #my $s;
  #print "Content-Type: text/html\n\n";
   print qq{
			<HTML>
			<HEAD><TITLE>ERROR</TITLE>
			</HEAD>
			<BODY>
			<!-- I AM HERE!! -->
			<CENTER>
			<FONT SIZE="5" COLOR="RED">
			<B>ERRORS</B><BR>
			</FONT>
			<BR>
			<TABLE CLASS="error" BORDER="1" CELLPADDING="20" WIDTH="600">
			<TR><TD>
			<UL>
	};
	&headerScript();
	   foreach $s (@_) {
		  print "$s<BR>";
	   }
	    $session->param("narrow_search", "0");
	    $session->param("nr_facility",$facility );
	    $session->param("nr_zip","");
		$session->param("nr_county","");
		$session->param("nr_name","");
		$session->param("nr_address","");
		$session->param("nr_city","");
		$session->param("nr_provider","");
		$session->param("nr_ctype","");
		$session->param("nr_msa","");
		$session->param("nr_system","");
		$session->param("nr_htype","");
		$session->param("nr_thosp","");
		$session->param("nr_geoarea","");
		$session->param("nr_bedstart","");
		$session->param("nr_bedend","");
		$session->param("nr_ftestart","");
		$session->param("nr_fteend","");
		$session->param("nr_incomestart","");
		$session->param("nr_incomeend","");
		$session->param("nr_Zscoremin","");
		$session->param("nr_Zscoremax","");
		$session->param("nr_OprMarginmin","");
		$session->param("nr_OprMarginmax","");
		$session->param("nr_Within","");
		$session->param("nr_p_c_Miles_Of_zip","");

	   print qq{
				</TD></TR>
				</TABLE>
				<BR>
				<FORM>
				};
				if($narrow_search_module eq "state" && $narrow_search == "0"){
	  print qq{
				<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)"/></span>
				};
				}else{
	  print qq{
				<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="window.location.href='/cgi-bin/cr_search.pl?facility=$facility'"/></span>
				};
			}
	  print qq{
				<input type='hidden' name='facility' value=\"$facility\">
				</FORM>
				</FONT>
				</CENTER>
				</BODY>
				</HTML>
				};
		}


#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }
   close INIFILE;
   return 1;
}

#--------------------------------------------------------------------------------------
sub sessionExpire
{
print "Content-Type: text/html\n\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
};
&headerScript();
print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};
}
#---------------------
