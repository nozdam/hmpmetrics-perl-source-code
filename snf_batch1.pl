#!/usr/bin/perl

#----------------------------------------------------------------------------
#  program: snf_batch1.pl
#  author: Steve Spicer
#  Written: Wed Oct 24 20:14:32 CDT 2007
#----------------------------------------------------------------------------

use strict;
use Spreadsheet::WriteExcel;
use CGI;
use DBI;
do "common_header.pl";
do "pagefooter.pl";
use CGI::Session;

my $start_time = time;

my $cgi = new CGI;
my $session = new CGI::Session(undef, $cgi , {Directory=>"/tmp"});
my $firstName = $session->param("firstName");
my $lastName = $session->param("lastName");
my $userID = $session->param("userID");

if(!$userID)
{
	&sessionExpire;
}

my $action    = $cgi->param('action');    
my $project   = $cgi->param('project');   # project name
my $text      = $cgi->param('providers'); # aaa.txt bbb.txt
my $show_keys = $cgi->param('showkeys');
my $dir       = $cgi->param('direction');
my $year      = $cgi->param('year');
my @datasets  = $cgi->param('datasets');

my %ini_value;

unless (&read_inifile("milo.ini")) {
   &display_error("CAN'T READ milo.ini");
   exit(1);
}

unless ($action eq 'go') {
   &display_form();
   exit(0);
}

$year = $ini_value{'DEFAULT_YEAR'} unless ($year =~ m/^\d\d\d\d$/);  # avoid any sql injection possibilities

my ($line, @list, $s, $len, $string, $provider, $n, $width, $value, $row, $column, $desc);

my @provider_list;

foreach $s (split /\n/,$text) {
   $s =~ s/[ \r\n]//g;
   $len = length($s);
   if ($len == 6 || $len == 11) {
      push @provider_list, $s;
   }
}

$n = @provider_list;

unless ($n) {
   &display_error("YOU MUST PROVIDE ONE OR MORE 6 CHARACTER PROVIDER ID'S");
   exit(0);
}

my $dbtype     = $ini_value{"DB_TYPE"};
my $dblogin    = $ini_value{"DB_LOGIN"};
my $dbpasswd   = $ini_value{"DB_PASSWORD"};
my $dbname     = $ini_value{"DB_NAME"};
my $dbserver   = $ini_value{"DB_SERVER"};

my $dbh;

unless ($dbh = DBI->connect("DBI:$dbtype:$dbname:$dbserver",$dblogin,$dbpasswd)) {
   &display_error("ERROR connecting to database", $DBI::errstr);
   exit(1);
}

my @report_definition;

my $fname;

foreach $fname (@datasets) {
   open(INPUT, "./snf_packages/$fname");
   while ($line = <INPUT>) {
     chomp($line);
     push @report_definition, $line;
   }
   close INPUT;
}

my $workbook = Spreadsheet::WriteExcel->new("-");
#   $workbook->set_tempdir('/tmp');

my $sheet1 = $workbook->addworksheet("main");

my $amount_format = $workbook->addformat();
   $amount_format->set_num_format(0x2b);      #  (#,##0.00_); (#,##0.00)

my $money_format = $workbook->addformat();
   $money_format->set_num_format(0x2c);      #  (#,##0.00_); (#,##0.00)

my $amount_bold_format = $workbook->addformat();
   $amount_bold_format->set_num_format(0x2c);      #  (#,##0.00_); (#,##0.00)
   $amount_bold_format->set_bold();

my $date_format = $workbook->addformat();
   $date_format->set_num_format(0x0e);        #  m/d/yy
   $date_format->set_align("center");

my $number_format = $workbook->addformat();
   $number_format->set_num_format(0x01);      #  0 integer

my $comma_format = $workbook->addformat();
   $comma_format->set_num_format(0x26);   #  (#,###) red

my $format_bold = $workbook->addformat();
   $format_bold->set_bold();

my $big_format_bold = $workbook->addformat();
   $big_format_bold -> set_bold();
   $big_format_bold -> set_size(14);

my $format_bold_rotated = $workbook->addformat();
   $format_bold_rotated->set_bold();
   $format_bold_rotated->set_rotation(80);

my $format_bold_right  = $workbook->addformat();
   $format_bold_right->set_bold();
   $format_bold_right->set_align("right");

my $format = $workbook->addformat();
   $format->set_size(10);

my $col = 0;
my $row = 0;


$n = @provider_list;

if ($dir eq 'across') {
   if ($show_keys eq 'on') {
       $sheet1->set_column($col, $col, 24);
       ++$col;
   }
   $sheet1->set_column($col, $col, 40);
   ++$col;
   $sheet1->set_column($col, $col + $n, 15);
}

my $now = localtime();

$sheet1->write_string($row,   0, 'Healthcare Management Partners', $big_format_bold);
$sheet1->write_string(++$row, 0, 'Project Milo HCRIS Data Demonstration Material',$big_format_bold);
$sheet1->write_string(++$row, 0, "$project", $big_format_bold);
$sheet1->write_string(++$row, 0, "GENERATED: $now", $format_bold);

&process_report();

my $elapsed_time = time - $start_time;

# $sheet1->write_number($row, 0, $elapsed_time, $number_format);

$dbh->disconnect();

print "Content-type: application/vnd.ms-excel\n";
print "Content-Disposition: attachment; filename=cost_report_batch.xls\n";
print "Cache-Control: no-cache\n";
print "Pragma: no-cache\n\n";

$workbook->close();

exit(0);


#=============================< SUBROUTINES >================================

# across ... the hospitals left to right columns
# down  .... the hospitals are down the page

sub process_report
{
   $row += 2;
   $col  = 0;

   my $format = ($dir eq 'across') ? $format_bold : $format_bold_rotated;
   
   if ($show_keys eq 'on') {
      @list = ('KEY', 'DESCRIPTION');
   } else {
      @list = ('DESCRIPTION');
   }

   my $top_row  = $row;

   foreach $string (@list) {
      $sheet1->write_string($row, $col, $string, $format);
      if ($dir eq 'across') { ++$col; } else { ++$row;}
   }
   
   foreach $string (@provider_list) {
      $sheet1->write_string($row, $col, $string, $format_bold_right);
      if ($dir eq 'across') { ++$col; } else { ++$row;}
   }
   
   if ($dir eq 'across') { ++$row; } else { ++$col;}

   my ($yyyy, $worksheet, $wrow, $wcol, $fmt);
   
   foreach $line (@report_definition) {

      next if ($line =~ m/^\#/);
   
      ($worksheet, $wrow, $wcol, $fmt, $desc) = split /\|/,$line,-1;
   
      if ($dir eq 'across') { $col = 0;} else {$row = $top_row;}
   
      if ($show_keys eq 'on') {
         $sheet1->write_string($row, $col, "$worksheet,$wrow,$wcol,$fmt", $format);
         if ($dir eq 'across') { ++$col; } else { ++$row;}
      }

      $sheet1->write_string($row, $col, "$desc", $format);
      if ($dir eq 'across') { ++$col; } else { ++$row;}

      foreach $line (@provider_list) {

         $provider = $line;

         if ($provider =~ m/(\S{6})[:\/\t](\d{4})/)  {
            ($provider, $yyyy) = ($1, $2);
         } else {
            $yyyy = $year;
         }

         print STDERR "$provider,$yyyy,$row,$col\n";

         $value = &get_value($yyyy, $worksheet, $wrow, $wcol, $provider);
   
         if ($value =~ m/^\-??[\d\.]+$/) {
             if ($fmt eq '$') {
                $sheet1->write_number($row, $col, $value, $money_format);
             } elsif ($fmt eq 'M') {
                $sheet1->write_number($row, $col, $value, $amount_format);
             } elsif ($fmt eq 'C') {
                $sheet1->write_number($row, $col, $value, $comma_format);
             } elsif ($fmt eq 'S') {
                $sheet1->write_string($row, $col, $value);
             } else {
                $sheet1->write_number($row, $col, $value);
             }
         } else {
             $sheet1->write_string($row, $col, $value);
         }
   
         if ($dir eq 'across') { ++$col } else { ++$row; }
      }
    
      if ($dir eq 'across') { ++$row; } else { ++$col; }
   }
}



#----------------------------------------------------------------------------

sub display_error
{
my @list = @_;

my $string;

foreach $s (@list) {
   $string .= "$s<BR>\n";
}

print "Content-Type: text/html\n\n";

print qq{
<HTML>
<HEAD><TITLE>ERROR</TITLE>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon" />
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css" />
<link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />
</HEAD>
<BODY>
};
&innerHeader();
print qq{
<CENTER>
<BR>
<BR>
<FONT SIZE="5" COLOR="RED">
<B>ERROR</B><BR>
</FONT>
<BR>
<TABLE CLASS="error" CELLPADDING="20" WIDTH="600">
   <TR>
      <TD><FONT SIZE="3">$string</FONT></TD>
   </TR>
</TABLE>
<BR>
<FORM ACTION="#">
<span class="button"><INPUT TYPE="button" value="  BACK  " onClick="history.go(-1)" /></span>
</FORM>

</CENTER>
</BODY>
</HTML>
};

}

#------------------------------------------------------------------------------
#

sub get_value
{
   my ($yyyy, $worksheet, $line, $column, $provider) = @_;

   my ($sth, $value);

   my $sql = qq(
    SELECT SNF_VALUE         
      FROM SNF_${yyyy}_RPT AA
 LEFT JOIN SNF_${yyyy}_DATA BB
	ON AA.RPT_REC_NUM = BB.SNF_REC_NUM
       AND SNF_WORKSHEET  = ?
       AND SNF_LINE       = ?
       AND SNF_COLUMN     = ?
     WHERE PRVDR_NUM     = ?
   );

   unless ($sth = $dbh->prepare($sql)) {
       &display_error("Error preparing SQL query", $sth->errstr);
       $dbh->disconnect();
       exit(0);
   }

   $sth->execute($worksheet, $line, $column, $provider);
   
   if ($sth->rows) {
      $value = $sth->fetchrow_array;
   }

   $sth->finish();

   return $value;
}

#---------------------------------------------------------------------------

sub display_form
{
   print "Content-Type: text/html\n\n";

   my ($year, $year_options, $selected);


   my $default_year = $ini_value{'DEFAULT_YEAR'};

   foreach $year (split /,/,$ini_value{'HCRIS_YEARS'}) {
      $selected = ($year == $default_year) ? 'SELECTED' : '';
      $year_options .= "               <OPTION VALUE=\"$year\" $selected>$year</OPTION>\n";
   }

   my $package_list = '';

   opendir(DIR,"./snf_packages");
   my @list = readdir(DIR);
   closedir(DIR);

   my ($fname, $url, $string); 

   foreach $fname (sort @list) {
      if ($fname =~ m/\.txt$/) {
         $string = uc $fname;
         $string =~ s/^\d+//;  
         $string =~ s/\.txt//i;
         $url = "<A HREF=\"/cgi-bin/snf_setedit.pl?set=$fname\">$string</A>\n";
         $package_list .= "<INPUT NAME=\"datasets\" TYPE=\"checkbox\" VALUE=\"$fname\">$url<BR>\n";
      }
   }

   print qq{
<HTML>
<HEAD><TITLE>Milo</TITLE></HEAD>
<LINK REL="shortcut icon" HREF="/favicon.ico" TYPE="image/x-icon" />
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css" />
<link href="/JS/rounded_button.css" rel="stylesheet" type="text/css" />
</HEAD>
<BODY><center>
<BR>
<CENTER>
};
&innerHeader();
print qq{
<FORM METHOD="POST" ACTION="/cgi-bin/snf_batch1.pl">

    <!--content start-->
    
    <table cellpadding="0" cellspacing="0" width="885px">
        <tr>
            <td valign="top">
                <img src="/images/content_left.gif" /></td>
            <td style="background-image: url(/images/content_middle.gif); width: 100%; background-repeat: repeat-x;
                text-align: left; padding: 10px">
<!--<Div class="pageheader" style="padding-bottom:0px">SNF COST REPORT SPREADSHEET GENERATOR</div>
<div style="padding-bottom:10px"> PROTOTYPE / PROOF OF CONCEPT</div>-->
<TABLE CELLPADDING="0" cellspacing="0" WIDTH="100%" class="gridstyle" style="background-color:white">
	<Tr>
		<Td valign="top" style="padding:10px">
			<TABLE CELLPADDING="2" cellspacing="2">
				<Tr>
					<td class="style1"><B>PROJECT IDENTIFIER</B></td>
					<td class="style1"><B>DEFAULT YEAR</B></td>
					
				</tr>
				<Tr>
					<td><INPUT NAME="project" class="textboxstyle" TYPE="TEXT" SIZE="30"></td>
					<td><SELECT  NAME="year" ><BR>
               $year_options
            </SELECT>
</td>
					
				</tr>
<tr>
<td colspan="2">&nbsp;</td>
</tr>

				<Tr>
					<td class="style1"><B>REPORT DIRECTION</B></td><td class="style1"><B>OTHER OPTIONS</B></td>

										
				</tr>
<tr>
<td colspan="2">&nbsp;</td>
</tr>

				<Tr>
					<Td valign="bottom"  class="style1"><INPUT NAME="direction" TYPE="radio" VALUE="across" CHECKED> ACROSS<BR>
            <INPUT NAME="direction" TYPE="radio" VALUE="down"> DOWN</td><td valign="bottom" class="style1"><INPUT NAME="showkeys" TYPE="checkbox"> INCLUDE REFERENCE CODES
</td>

</tr>
<tr>
<td colspan="2">&nbsp;</td>
</tr>
				<Tr>
					<td class="style1"><b>SELECT DATASETS</b></td>
					<Td></td>
				<tr>
<tr>
<td colspan="2">&nbsp;</td>
</tr>

				<Tr>
					<td valign="top" class="gridHlink" >$package_list</td>
					<Td></td>
				<tr>


			</table>
		</td>
		<td valign="top" style="padding:10px">
			<B>PEER GROUP SELECTION</B><BR>
         Cut and paste from provider numbers into area below use a single column 
<BR><BR>

         
        <Span style="color:red;font-size:10;">(Now supports Provider/Year)</span><BR><BR>
<TEXTAREA NAME="providers" COLS="20" ROWS="10" WRAP="HARD" style="width:40%" ></TEXTAREA>


		</td>
	</tr>
</table>
<div style="padding-top:5px"> <INPUT TYPE="hidden" NAME="action" VALUE="go">
         <span class="button"><INPUT TYPE="reset"  VALUE=" CLEAR "></span>
         <span class="button"><INPUT TYPE="submit" VALUE="  GENERATE SPREADSHEET  "></span></div>

</td>
            <td valign="top">
                <img src="/images/content_right.gif" /></td>
        </tr>
    </table>
    <!--content end-->
	};
	
	&TDdoc;
	print qq{
	
</FORM>
</CENTER>
</BODY>
</HTML>
   };
}


#------------------------------------------------------------------------------

sub read_inifile
{
   my ($fname) = @_;

   my ($line, $key, $value);

   open INIFILE, $fname or return 0;

   while ($line = <INIFILE>) {
      chomp($line);
      next if ($line =~ m/^#/);
      $line =~ s/\s+#.*$//;  # strip comments and right spaces
      ($key, $value) = split /=/,$line;
      $ini_value{$key} = $value;
   }

   close INIFILE;
   return 1;
}

#--------------------------------------------------------------------------------------
sub sessionExpire
{

print "Content-Type: text/html\n\n";
print qq{
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<LINK REL="stylesheet" TYPE="text/css" HREF="/milo.css">
</head>
<body>
};

print "<script>window.parent.location.href='/cgi-bin/login.pl?saction=sessOut';</script>";

print qq{
</body>
</html>
};

}

#------------------------------------------------------------------------------


#Database: HCRISDB  Table: HS2006_RPT  Wildcard: %
#+--------------------+---------------+-------------------+------+-----+---------+-------+---------------------------------+---------+
#| Field              | Type          | Collation         | Null | Key | Default | Extra | Privileges                      | Comment |
#+--------------------+---------------+-------------------+------+-----+---------+-------+---------------------------------+---------+
#| RPT_REC_NUM        | int(11)       |                   | NO   |     |         |       | select,insert,update,references |         |
#| PRVDR_CTRL_TYPE_CD | char(2)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| PRVDR_NUM          | char(6)       | latin1_swedish_ci | NO   | MUL |         |       | select,insert,update,references |         |
#| NPI                | decimal(10,0) |                   | YES  |     |         |       | select,insert,update,references |         |
#| RPT_STUS_CD        | char(1)       | latin1_swedish_ci | NO   |     |         |       | select,insert,update,references |         |
#| FY_BGN_DT          | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| FY_END_DT          | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| PROC_DT            | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| INITL_RPT_SW       | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| LAST_RPT_SW        | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| TRNSMTL_NUM        | char(2)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| FI_NUM             | char(5)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| ADR_VNDR_CD        | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| FI_CREAT_DT        | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| UTIL_CD            | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| NPR_DT             | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#| SPEC_IND           | char(1)       | latin1_swedish_ci | YES  |     |         |       | select,insert,update,references |         |
#| FI_RCPT_DT         | date          |                   | YES  |     |         |       | select,insert,update,references |         |
#+--------------------+---------------+-------------------+------+-----+---------+-------+---------------------------------+---------+
